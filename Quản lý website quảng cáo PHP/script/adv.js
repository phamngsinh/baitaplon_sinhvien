var slideTime = 2000;
var floatAtBottom = false;

function pepsi_floating_init()
{
	xMoveTo('floating_banner_right', 1280 - (1200-screen.width), 0);

	winOnResize(); // set initial position
	xAddEventListener(window, 'resize', winOnResize, false);
	xAddEventListener(window, 'scroll', winOnScroll, false);
}
function winOnResize() {
	checkScreenWidth();
	winOnScroll(); // initial slide
}
function winOnScroll() {
  var y = xScrollTop();
  if (floatAtBottom) {
    y += xClientHeight() - xHeight('floating_banner_left');
  }

	if( screen.width <= 1200 )
	{
		xSlideTo('floating_banner_left', (screen.width - (1024-1000) - 1000)/2, y, slideTime);
  		xSlideTo('floating_banner_right', (screen.width - (1200-1200) + 1000)/2, y, slideTime);
	}
	else
	{
		xSlideTo('floating_banner_left', (screen.width - (1024-1000) - 1000)/2-110-3 , y+0, slideTime);
  		xSlideTo('floating_banner_right', (screen.width - (1200-1200) + 1000)/2, y+0, slideTime);
	}
}
function checkScreenWidth()
{
	if( screen.width <= 1024 )
	{
		document.getElementById('floating_banner_left').style.display = 'none';
	}

	if(screen.width < 1024)
	{
		
		document.getElementById('floating_banner_right').style.display = 'none';
	}
}
