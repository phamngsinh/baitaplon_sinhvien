<?
define('_FIRST_PAGE'      ,'FIRST PAGE');
define('_HOME'            ,'Home');
define('_INTRO'           ,'About us');
define('_CONTACT'         ,'Contact');
define('_CONTACTINFO'     ,'CONTACT INFOMATION');
define('_CONTACTCOMPANY'  ,'Company Information');
define('_ABOUT'           ,'ABOUT US');
define('_HISTORY'         ,'HISTORY');
define('_DETAIL'		  ,'Detail');
define('_CART'		      ,'Cart');
define('_LANGUAGE'		  ,'Language');
define('_QUANTITY'		  ,'Quantity');
define('_TOTALS'		  ,'Total');
define('_INFOCART'		  ,'INFORMATION CART');
define('_PRODUCT'        ,'Product');
define('_PRICE'           ,'Price');
define('_GO'           ,'Continue to buy');
define('_PICTURE'           ,'Images');
define('_FUNCTION'           ,'Function');
define('_SUPPORT'           ,'Support');
define('_INFOPUT'           ,'Enter information');
define('_PRODUCTNEW'           ,'Product new');

define('_NEWSEVENT'       ,'News & Event');
define('_NEWS'            ,'News');
define('_NEWS_'           ,'Salient news');
define('_EVENT'           ,'EVENT');
define('_DOC'             ,'DOCUMENT');
define('_PROJECT'         ,'PROJECT');
define('_PRODUCT_CATEGORY','Product Category');
define('_PRODUCT'         ,'Product');
define('_PRODUCT_NEW'     ,'New Product');
define('_PRODUCT_SPECIAL' ,'SPECIAL PRODUCT');
define('_PRODUCT_BUY' 	  ,'Product best buy');
define('_MEMBER'          ,'Member');
define('_SERVICE'         ,'Service');
define('_SITEMAP'         ,'SITEMAP');
define('_SPECIAL'         ,'LIMITED SPECIALITY');
define('_RECRUIT'         ,'Recruit');
define('_PARTNERCUSTOMER' ,'Partner & Customer');
define('_SOLUTION'		  ,'Solution');
define('_HARDWARE'		  ,'Hardware');
define('_SOFTWARE'		  ,'Software');

define('_LINK'            ,'Link Website');
define('_WEATHER'         ,'WEATHER');
define('_GOLD'            ,'GOLD');
define('_FOREX'           ,'FOREX');
define('_STOCK'           ,'STOCK');
define('_SUPPORTONLINE'   ,'Support Online');
define('_YAHOO'           ,'Support Online');
define('_SKYPE'           ,'SUPPORT ONLINE (SKYPE)');
define('_SUPPORTTECHNIQUE','Support Technique');
define('_SEARCH'          ,'Search');
define('_GOOGLE'          ,'SEARCH BY GOOGLE');
define('_SEARCH_ADVANCE'  ,'Advance search');
define('_TOTAL'           ,'Statistic');
define('_ADVERTISEMENT'   ,'ADVERTISEMENT');
define('_IDEA'            ,'IDEA');
define('_FORUM'           ,'FORUM');
define('_ORTHERINFO'      ,'ORTHER INFOMATION');

define('_UID'             ,'Username');
define('_PWD'             ,'Password');
define('_REGISTRY'        ,'Registry');
define('_LOGIN'           ,'Login');
define('_LOGOUT'          ,'Logout');
define('_FORGOTPASS'      ,'Forgot password');
define('_VN'			  ,'Việt Nam');
define('_EN'			  ,'English');

define('_MEMBER'          ,'Member');
define('_GUEST'           ,'Guest');
?>