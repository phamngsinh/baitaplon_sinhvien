<?php
//generated at 17.03.2007

$lang['help_db']="Veritabanlarının listesi";
$lang['help_praefix']="Tablo ön eki, tabloların daha kolay filtrelenebilmesini sağlamak için tablo isimlerinin önüne eklenir.";
$lang['help_zip']="Gzip ile sıkıştırma - aktiv olması önerilir'";
$lang['help_memorylimit']="Byte olarak belirlenir, Scriptin kullanabileceği azami hafızayı belirler
0 = Devre dışı";
$lang['memory_limit']="Hafıza sınırı";
$lang['help_mail1']="Aktiv olması durumunda, yedekleme işleminin sonunda mail gönderilir.";
$lang['help_mail2']="Mailin gönderileceği adres.";
$lang['help_mail3']="Maili gönderenin adresi.";
$lang['help_mail4']="Maileklentisinin asgari ebatı, 0 yazıldığında limit kullanılmaz.";
$lang['help_mail5']="Yedekleme dosyasının maile eklenip eklenmeyeceğini burada belirleyebilirsiniz.";
$lang['help_ad1']="Aktiv olduğunda yedekleme dosyaları otomatik olarak silinir.";
$lang['help_ad2']="Yedekleme dosyalarının maximum yaşı (Otomatik silme için) 
0 = kullanılmaz";
$lang['help_ad3']="Yedekleme dosyalarının maximum sayısı (Otomatik silme için)
0 = kullanılmaz";
$lang['help_lang']="Dil seçeneği";
$lang['help_empty_db_before_restore']="Gereksiz kayıtların silinmesi için, geri dönüıümden önce Veritabanlarının boşaltılmasını sağlayabilirsiniz.";
$lang['help_cronmail']="Cronscriptin yedeklemeyi mail eklentisi olarak göndermesini sağlar.";
$lang['help_cronmailprg']="Mailproğramının veri yolu, standart olarak sendmail kullanılır.";
$lang['help_cronftp']="Cronscriptin yedeklemeyi FTP ile göndermesini sağlar";
$lang['help_cronzip']="GZip ile sıkıştırma - Tavsiyemiz 'aktiv olması' (Kompressions-Lib eklentilerinin yüklenmiş olması gerekir!).";
$lang['help_cronextender']="Perlscriptinin dosyabitimi, Standard: '.pl'.";
$lang['help_cronsavepath']="Perlskriptinin ayardosyasının adı.";
$lang['help_cronprintout']="Textçıktısı kapalı olursa, sonuçlar belirtilmez.
Bu funksyon protokol dosyasına bağlı değildir.";
$lang['help_cronsamedb']="Cronscript için ayarlardaki Veritabanı kullanılsınmı?";
$lang['help_crondbindex']="Cronscript için kullanılacak Veritabanını seçiniz.";
$lang['help_cronmail_dump']="Cronjob tarafından gönderilecek maile yedekleme dosyasının ekleneceğini belirler.";
$lang['help_ftptransfer']="Seçildiğinde yedekleme den sonra dosya FTP ile gönderilir.";
$lang['help_ftpserver']="FTP-Sunucusunun adresi.";
$lang['help_ftpport']="FTP-Sunucusunun Portnumarası, Standart: 21.";
$lang['help_ftpuser']="FTP-Kullanıcısının adı";
$lang['help_ftppass']="FTP-Kullanıcısının şifresi.";
$lang['help_ftpdir']="Dosyanın gönderileceği yer?";
$lang['help_speed']="asgari ve azami hız, Standart: 50 den 5000 e kadar
(daha yüksek hız ayarı çalışmayabilir!).";
$lang['speed']="Hız";
$lang['help_cronexecpath']="Perlskriptin bulunduğu alan.
HTTP-Adressinden yola çıkarak (Tarayıcıda).";
$lang['cron_execpath']="Perlskript in veriyolu";
$lang['help_croncompletelog']="Aktiv olması durumunda çıktının komplesi complete_log dosyasına kaydedilir. 
Textçıktısı ayarlarına bağlı değildir.";
$lang['help_ftp_mode']="Eğer FTP-Transfer esnasında hata oluşursa,lütfen Pasif-Modus yöntemi ile deneyin.";


?>