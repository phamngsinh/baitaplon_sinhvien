<?php
//generated at 17.03.2007

$lang['dump_headline']="yedekleme oluşturuluyor...";
$lang['gzip_compression']="GZip-sıkıştırma ";
$lang['saving_table']="Tablolar kayıt ediliyor ";
$lang['of']="tarihinde";
$lang['actual_table']="şuanki tablo";
$lang['progress_table']="Tablo işlem durumu";
$lang['progress_over_all']="Genel işlem durumu";
$lang['entry']="Kayıt";
$lang['done']="Tamamlandı!";
$lang['dump_successful']=" Başarıyla tamamlandı.";
$lang['upto']="kadar";
$lang['email_was_send']="Email gönderilen alıcı:";
$lang['back_to_control']="Devam";
$lang['back_to_overview']="Veritabanı listesi";
$lang['dump_filename']="Yedeklenen dosyanın ismi: ";
$lang['withpraefix']="Önekli";
$lang['dump_notables']="`<b>%s</b>` Veritabanında tablo bulunamadı.";
$lang['dump_endergebnis']=" <b>%s</b> tabloda <b>%s</b> kayıt yedeklendi.<br>";
$lang['mailerror']="Mail göderiminde hata oluştu!";
$lang['emailbody_attach']="Ekte veritabanıyın yedeklemesi bulunuyor.<br>yedeklenen Veritabanı `%s`
<br><br>Oluşturulan dosya:<br><br>%s <br><br>Sevgilerler<br><br>MySQLDumper<br><a href=\"http://www.mysqldumper.de/\">www.mysqldumper.de</a>";
$lang['emailbody_mp_noattach']="Çok parçalı yedekleme oluşturuldu.<br> Dosyalar eklenti olarak gönderilmiyor!<br>yedeklenen Veritabanı `%s`
<br><br>oluşturulan dosyalar:<br><br>%s<br><br><br>Sevgilerle<br><br>MySQLDumper<br><a href=\"http://www.mysqldumper.de/\">www.mysqldumper.de</a>";
$lang['emailbody_mp_attach']="Çok parçalı yedekleme oluşturuldu.<br>Dosyalar eklenti olarak gönderilmiyor!Dosyalar ayrı bir mail ile gönderiliyor!<br>Yedeklenen Veritabanı `%s`
<br><br>oluşturulan dosyalar:<br><br>%s<br><br><br>Sevgilerle<br><br>MySQLDumper<br><a href=\"http://www.mysqldumper.de/\">www.mysqldumper.de</a>";
$lang['emailbody_footer']="<br><br><br>Sevgiler<br><br>MySQLDumper<br><a href=\"http://www.mysqldumper.de/\">www.mysqldumper.de</a>";
$lang['emailbody_toobig']="Yedekleme boyutu maximumu boyut olan %s aştıgından dolayı eklenti olarak gönderilemiyor.<br>Yedeklenen Veritabanı `%s`
<br><br>oluşturulan dosyalar:<br><br>%s
<br><br>Saygılarla<br><br>MySQLDumper<br><a href=\"http://www.mysqldumper.de/\">www.mysqldumper.de</a>";
$lang['emailbody_noattach']="Yedekleme dosyaları maalesef eklenememiştir.<br>yedeklenen Veritabanı `%s`
<br><br>Oluşturulan Dosyalar:<br><br>%s
<br><br>Sevgilerle<br><br>MySQLDumper<br><a href=\"http://www.mysqldumper.de/\">www.mysqldumper.de</a>";
$lang['email_only_attachment']=" ... sadece eklentiler";
$lang['tableselection']="Tablo seçimi";
$lang['selectall']="hiçbirini seçme";
$lang['deselectall']="hepsini seç";
$lang['startdump']="Yedeklemeyi başlat";
$lang['datawith']="kayıtlar";
$lang['lastbufrom']="son yedekleme tarihi";
$lang['not_supported']="Bu yedekleme istenilen funksyonu desteklemiyor.";
$lang['multidump']="Parçalı yedekleme:<b>%d</b> Veritabanları yedeklendi.";
$lang['filesendftp']="Dosya FTP ile gönderiliyor... lütfen biraz bekleyiniz. ";
$lang['ftpconnerror']="FTP-Bağlantısı kurulmadı! Bağlantı  ";
$lang['ftpconnerror1']=" Kullanıcı";
$lang['ftpconnerror2']=" mümkün değil";
$lang['ftpconnerror3']="FTP-Yüklemesi başarısız! ";
$lang['ftpconnected1']="Kullanılan bağlantı";
$lang['ftpconnected2']=" ile ";
$lang['ftpconnected3']=" kayıt edilen";
$lang['nr_tables_selected']="-  %s seçilmiş tablolar";
$lang['nr_tables_optimized']="<span class=\"small\">%s Tablolar arındırıldı.</span>";
$lang['dump_errors']="<p class=\"uyarı\">Hata oluştu: <a href=\"log.php?r=3\">göster</a></p>";


?>