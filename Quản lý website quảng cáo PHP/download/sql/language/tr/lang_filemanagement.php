<?php
//generated at 17.03.2007

$lang['convert_start']="Çeviriyi başlat";
$lang['convert_title']="MSD-Formatına çevir";
$lang['convert_wrong_parameters']="Yanlış Parametre! Çeviri mümkün değil.";
$lang['fm_uploadfilerequest']="Dosya adını giriniz.";
$lang['fm_uploadnotallowed1']="Bu dosya tipi geçerli değil.";
$lang['fm_uploadnotallowed2']="Geçerli dosya tipleri: *.gz und *.sql-Dosyaları";
$lang['fm_uploadmoveerror']="Yüklenen dosya yerine sürülemedi.";
$lang['fm_uploadfailed']="Yükleme yapılamadı!";
$lang['fm_uploadfileexists']="Bu isimde bir dosya zaten bulunmakta!";
$lang['fm_nofile']="Dosya seçmediniz!";
$lang['fm_delete1']="Dosya ";
$lang['fm_delete2']=" silindi.";
$lang['fm_delete3']=" silinemedi!";
$lang['fm_choose_file']="Seçilmiş dosya:";
$lang['fm_filesize']="Dosya boyutu";
$lang['fm_filedate']="Tarih";
$lang['fm_nofilesfound']="Dosya bulunamadı.";
$lang['fm_tables']="Tablolar";
$lang['fm_records']="Kayıtlar";
$lang['fm_all_bu']="Tüm yedeklemeler";
$lang['fm_anz_bu']="Yedeklemeler";
$lang['fm_last_bu']="Son yedekleme";
$lang['fm_totalsize']="Toplam boyut";
$lang['fm_selecttables']="Tablo seçimi";
$lang['fm_comment']="Not düş";
$lang['fm_restore']="Dönüştür";
$lang['fm_alertrestore1']="Veritabanı";
$lang['fm_alertrestore2']="Dosyanın içeriği ile";
$lang['fm_alertrestore3']="dönüştürülsünmü?";
$lang['fm_delete']="Seçilen dosyaları sil";
$lang['fm_askdelete1']="Seçilen dosya ";
$lang['fm_askdelete2']=" gerçekten silinsinmi?";
$lang['fm_askdelete3']="Otomatik dosya temizlemesi belirlenmiş ayarlara göre şimdi uygulansınmı?";
$lang['fm_askdelete4']="Tüm yedeklemeleri şimdi silmek istiyormusun?";
$lang['fm_askdelete5']="tüm yedeklemeleri (... ile)";
$lang['fm_askdelete5_2']="_* özelliğine sahip tüm yedeklemeler silinsinmi?";
$lang['fm_deleteauto']="Otomatik dosya temizliği";
$lang['fm_deleteall']="Hepsini sil ";
$lang['fm_deleteallfilter']="Sil: ";
$lang['fm_deleteallfilter2']="_*";
$lang['fm_startdump']="Yedeklemeyi başlat";
$lang['fm_fileupload']="Dosya yükle";
$lang['fm_dbname']="Veritabanının ismi";
$lang['fm_files1']="Veritabanı yedeklemeleri";
$lang['fm_files2']="Veritabanı-yapısı";
$lang['fm_autodel1']="Otomatik temizleme: Maximim dosya sayısı aştığı için silinen dosyalar:";
$lang['fm_autodel2']="Otomatik temizleme: Zaman aşımından dolayı silinen dosyalar:";
$lang['fm_dumpsettings']="Yedekleme ayarları";
$lang['fm_dumpsettings_cron']="Perl-Cronscript ayarları";
$lang['fm_oldbackup']="(bilinmiyor)";
$lang['fm_restore_header']="Dünüştürüm <strong>\"";
$lang['fm_restore_header2']="\"</strong>";
$lang['fm_dump_header']="Yedekleme";
$lang['DoCronButton']="Perl-Cronscript'i çalıştır";
$lang['DoPerlTest']="Perl-Modulerini dene";
$lang['DoSimpleTest']="Perli dene";
$lang['perloutput1']="crondump.pl de kayıtlı adres absolute_path_of_configdir";
$lang['perloutput2']="Tarayıcı veya dışarıdan çağrışım ile çalışan Cronjob";
$lang['perloutput3']="Shell den veya Crontab dan çalışması için";
$lang['converter']="Yedekleme dönüştürücüsü";
$lang['convert_file']="dönüştürülecek dosya";
$lang['convert_filename']="Yeni dosya adı (eksiz)";
$lang['converting']="Dönüştürüm";
$lang['convert_fileread']="Dosya '%s' okunuyor";
$lang['convert_finished']="Dönüştürme tamamlandı, '%s' oluşturuldu.";
$lang['no_msd_backupfile']="Başka yazılımların dosyaları:";
$lang['max_upload_size']="Maximum dosya büyüklüğü";
$lang['max_upload_size_info']="If your Dumpfile is bigger than the above mentioned limit, you must upload it via FTP into the directory \"work/backup\". 
After that you can choose it to begin a restore progress. ";
$lang['encoding']="encoding";
$lang['fm_choose_encoding']="Choose encoding of backupfile";
$lang['choose_charset']="MySQLDumper couldn't detect the encoding of the backupfile automatically.
<br>You must choose the charset with which this backup was saved.
<br>If you discover any problems with some characters after restoring, you can repeat the backup-progress and then choose another chracter set.
<br>Good luck. ;)

";


?>