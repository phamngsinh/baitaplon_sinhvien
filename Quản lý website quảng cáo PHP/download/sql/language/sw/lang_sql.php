<?php
//generated at 17.03.2007

$lang['command']="Kommando";
$lang['sql_view_normal']="Display: normal";
$lang['sql_view_compact']="Display: compact";
$lang['import_notable']="Es ist keine Tabelle für den Import ausgewählt!";
$lang['sql_warning']="Die Ausführung von SQL-Befehlen kann Daten manipulieren! Der Autor übernimmt keine Haftung bei Datenverlusten.";
$lang['sql_exec']="SQL-Befehl ausführen";
$lang['sql_dataview']="Daten-Ansicht";
$lang['sql_tableview']="Tabellen-Ansicht";
$lang['sql_vonins']="von insgesamt";
$lang['sql_nodata']="keine Datensätze";
$lang['sql_recordupdated']="Datensatz wurde geändert";
$lang['sql_recordinserted']="Datensatz wurde gespeichert";
$lang['sql_backdboverview']="zurück zur Datenbankübersicht";
$lang['sql_recorddeleted']="Datensatz wurde gelöscht";
$lang['asktableempty']="Skall tabellen `%s` verkligen tömmas?";
$lang['sql_recordedit']="editiere Datensatz";
$lang['sql_recordnew']="Datensatz einfügen";
$lang['askdeleterecord']="Skall dataposten verkligen raderas?";
$lang['askdeletetable']="Skall tabellen `%s` verkligen raderas?";
$lang['sql_befehle']="SQL-Befehle";
$lang['sql_befehlneu']="neuer Befehl";
$lang['sql_befehlsaved1']="SQL-Befehl";
$lang['sql_befehlsaved2']="wurde hinzugefügt";
$lang['sql_befehlsaved3']="wurde gespeichert";
$lang['sql_befehlsaved4']="wurde nach oben gebracht";
$lang['sql_befehlsaved5']="wurde gelöscht";
$lang['sql_queryentry']="Die Abfrage enthält";
$lang['sql_columns']="Spalten";
$lang['askdbdelete']="Vill du verkligen radera databasen `%s` samt dess innehåll?";
$lang['askdbempty']="Vill du verkligen tömma databasen `%s`?";
$lang['askdbcopy']="Vill du kopiera innehållet i databasen `%s` till databasen `%s`?";
$lang['sql_tablenew']="Tabellen bearbeiten";
$lang['sql_output']="SQL-Ausgabe";
$lang['do_now']="utför nu";
$lang['sql_namedest_missing']="Name für die Zieldatenbank fehlt!";
$lang['askdeletefield']="Skall fältet verkligen raderas?";
$lang['sql_commands_in']=" Zeilen in ";
$lang['sql_commands_in2']="  Sekunde(n) abgearbeitet.";
$lang['sql_out1']="Es wurden ";
$lang['sql_out2']="Befehle ausgeführt";
$lang['sql_out3']="Es gab ";
$lang['sql_out4']="Kommentare";
$lang['sql_out5']="Da die Ausgabe über 5000 Zeilen enthält, wird sie nicht angezeigt.";
$lang['sql_selecdb']="Datenbank auswählen";
$lang['sql_tablesofdb']="Tabellen der Datenbank";
$lang['sql_edit']="bearbeiten";
$lang['sql_nofielddelete']="Löschen nicht möglich, da eine Tabelle mindestens 1 Feld haben muss.";
$lang['sql_fielddelete1']="Das Feld";
$lang['sql_deleted']="wurde gelöscht.";
$lang['sql_changed']="wurde geändert.";
$lang['sql_created']="wurde angelegt.";
$lang['sql_nodest_copy']="Ohne Ziel kann nicht kopiert werden!";
$lang['sql_desttable_exists']="Zieltabelle existiert schon!";
$lang['sql_scopy']="Tabellenstruktur von `%s` wurde in Tabelle `%s` kopiert.";
$lang['sql_tcopy']="Tabelle `%s` wurde mit Daten in Tabelle `%s` kopiert.";
$lang['sql_tablenoname']="Tabelle braucht einen Namen!";
$lang['sql_tblnameempty']="Tabellenname darf nicht leer sein!";
$lang['sql_collatenotmatch']="Zeichensatz und Sortierung passen nicht zueinander!";
$lang['sql_fieldnamenotvalid']="Fehler: Kein gültiger Feldname";
$lang['sql_createtable']="Tabelle anlegen";
$lang['sql_copytable']="Tabelle kopieren";
$lang['sql_structureonly']="nur Struktur";
$lang['sql_structuredata']="Struktur und Daten";
$lang['sql_notablesindb']="Es befinden sich keine Tabellen in der Datenbank";
$lang['sql_selecttable']="Tabelle auswählen";
$lang['sql_showdatatable']="Daten der Tabelle anzeigen";
$lang['sql_tblpropsof']="Tabelleneigenschaften  von";
$lang['sql_editfield']="Editiere Feld";
$lang['sql_newfield']="Neues Feld";
$lang['sql_indexes']="Indizes";
$lang['sql_atposition']="an Position einfügen";
$lang['sql_first']="zuerst";
$lang['sql_after']="nach";
$lang['sql_changefield']="Feld ändern";
$lang['sql_insertfield']="Feld einfügen";
$lang['sql_insertnewfield']="neues Feld einfügen";
$lang['sql_tableindexes']="Indizes der Tabelle";
$lang['sql_allowdups']="Duplikate erlaubt";
$lang['sql_cardinality']="Kardinalität";
$lang['sql_tablenoindexes']="Die Tabelle enthält keine Indizes";
$lang['sql_createindex']="neuen Index erzeugen";
$lang['sql_wasemptied']="wurde geleert";
$lang['sql_renamedto']="wurde umbenannt in";
$lang['sql_dbcopy']="Der Inhalt der Datenbank `%s` wurde in die Datenbank `%s` kopiert.";
$lang['sql_dbscopy']="Die Struktur der Datenbank `%s` wurde in die Datenbank `%s` kopiert.";
$lang['sql_wascreated']="wurde erzeugt";
$lang['sql_renamedb']="Datenbank umbenennen";
$lang['sql_actions']="Aktionen";
$lang['sql_chooseaction']="Aktion wählen";
$lang['sql_deletedb']="Datenbank löschen";
$lang['sql_emptydb']="Datenbank leeren";
$lang['sql_copydatadb']="Inhalt in Datenbank kopieren";
$lang['sql_copysdb']="Struktur in Datenbank kopieren";
$lang['sql_imexport']="Im-/Export";
$lang['info_records']="Datensätze";
$lang['asktableemptykeys']="Skall tabellen `%s` tömmas och indexen återställas?";
$lang['edit']="redigera";
$lang['delete']="radera";
$lang['empty']="leeren";
$lang['emptykeys']="leeren und Indizes zurücksetzen";
$lang['sql_tableemptied']="Tabelle `%s` wurde geleert.";
$lang['sql_tableemptiedkeys']="Tabelle `%s` wurde geleert und die Indizes wurden zurückgesetzt.";
$lang['sql_library']="SQL-Bibliothek";
$lang['sql_attributes']="Attribute";
$lang['sql_enumsethelp']="Bei Feldtypen ENUM und SET bitte bei Size die Werteliste eingeben.

Die Werte müssen in Hochkommas und mit Kommas getrennt sein.

Bei Benutzung von Sonderzeichen müssen diese mit \ (Backslash) maskiert werden.


Beispiele:

'a','b','c'

'ja','nein'

'x','y'";
$lang['sql_uploadedfile']="geladenes File: ";
$lang['sql_import']="Import in Datenbank `%s`";
$lang['export']="Export";
$lang['import']="Import";
$lang['importoptions']="Import-Optionen";
$lang['csvoptions']="CSV-optioner";
$lang['importtable']="Import in Tabelle";
$lang['newtable']="neue Tabelle";
$lang['importsource']="Import-Quelle";
$lang['fromtextbox']="aus Textfeld";
$lang['fromfile']="aus Datei";
$lang['emptytablebefore']="Tabelle vorher leeren";
$lang['createautoindex']="Skapa auto-index";
$lang['csv_namefirstline']="Fältnamn i första raden";
$lang['csv_fieldseperate']="Fält separerade med";
$lang['csv_fieldsenclosed']="Fält inneslutna av";
$lang['csv_fieldsescape']="Fält escaped från";
$lang['csv_eol']="Raderna separerade med";
$lang['csv_null']="Ersätt NULL med";
$lang['csv_fileopen']="Öppna CSV-fil";
$lang['importieren']="importieren";
$lang['sql_export']="Export aus Datenbank `%s`";
$lang['exportoptions']="Export-Optionen";
$lang['excel2003']="Excel ab 2003";
$lang['showresult']="Ergebnis anzeigen";
$lang['sendresultasfile']="Ergebnis als Datei senden";
$lang['exportlines']="<strong>%s</strong> Zeilen exportiert";
$lang['csv_fieldcount_nomatch']="Antalet tabell-fält stämmer ej överens med antalet som skall importeras (%d istället för %d).";
$lang['csv_fieldslines']="%d fält fastställda, totalt %d rader";
$lang['csv_errorcreatetable']="Fel när tabellen `%s` skulle skapas!";
$lang['fm_uploadfilerequest']="Bitte gib eine Datei an.";
$lang['csv_nodata']="Ingen data kunde hittas för import!";
$lang['sqllib_generalfunctions']="allgemeine Funktionen";
$lang['sqllib_resetauto']="Auto-Wert zurücksetzen";
$lang['sqllib_boards']="Boards";
$lang['sqllib_deactivateboard']="Board deaktivieren";
$lang['sqllib_activateboard']="Board aktivieren";
$lang['sql_notablesselected']="Es sind keine Tabellen selektiert!";
$lang['tools']="Tools";
$lang['tools_toolbox']="Datenbank auswählen / Datenbankfunktionen / Im- und Export ";
$lang['sqllib_boardoffline']="Forum offline";
$lang['sql_openfile']="Open SQL-File";
$lang['sql_openfile_button']="Upload";
$lang['max_upload_size']="Maximum filesize";
$lang['sql_search']="Search";
$lang['sql_searchwords']="Searchword(s)";
$lang['start_sql_search']="start search";
$lang['reset_searchwords']="reset searchwords";
$lang['search_explain']="Emnter one or more searchwords and start searching by clicking \"start search\".<br>
You can enter more than one searchword by separating them with a space.";
$lang['search_options']="Searchoptions";
$lang['search_results']="The search for \"<b>%s</b>\" in table \"<b>%s</b>\" brings the following results";
$lang['search_no_results']="The search for \"<b>%s</b>\" in table \"<b>%s</b>\" doesn't bring any hits!";
$lang['no_entries']="Table \"<b>%s</b>\" is empty and doesn't have any entry.";
$lang['search_access_keys']="Browse: forward=ALT+V, backwards=ALT+C";
$lang['search_options_or']="a column must have one of the searchwords (OR-search)";
$lang['search_options_concat']="a row must contain all of the searchwords but they can be in any column (could take some time)";
$lang['search_options_and']="a column must contain all searchwords (AND-search)";
$lang['search_in_table']="Search in table";
$lang['sql_edit_tablestructure']="Edit tablestructure";
$lang['default_charset']="Default character set";


?>