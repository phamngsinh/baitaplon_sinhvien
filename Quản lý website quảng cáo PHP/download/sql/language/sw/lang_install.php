<?php
//generated at 17.03.2007

$lang['installfinished']="<br>die Installation ist abgeschlossen   --> <a href=\"index.php\">starte MySQLDumper</a><br>";
$lang['install_forcescript']="MySQLDumper ohne Installation starten";
$lang['install_tomenu']="zum Hauptmenü";
$lang['installmenu']="Hauptmenü";
$lang['step']="Schritt";
$lang['install']="Installation";
$lang['uninstall']="Deinstallation";
$lang['tools']="Tools";
$lang['editconf']="Redigera konfigureringen";
$lang['osweiter']="ohne Speichern weiter";
$lang['errorman']="<strong>Fehler beim Schreiben der Konfiguration!</strong><br>Bitte editieren Sie das File ";
$lang['manuell']="manuell";
$lang['createdirs']="skapar mappar";
$lang['install_continue']="mit der Installation fortfahren";
$lang['connecttomysql']=" förbind med mysql ";
$lang['dbparameter']="Databas-parametrar";
$lang['confignotwritable']="I cannot write to file \"config.php\".
Please use your FTP-Programm and chmod this file to 0777.";
$lang['dbconnection']="Databas-förbindelse";
$lang['connectionerror']="Fel: förbindelse kunde ej upprättas.";
$lang['connection_ok']="Databas-förbindelse har upprättats.";
$lang['saveandcontinue']="speichern und Installation fortsetzen";
$lang['confbasic']="Grundinställningar";
$lang['install_step2finished']="Die Einstellungen wurden erfolgreich gesichert.";
$lang['install_step2_1']="Installation mit Standardkonfiguration fortsetzen";
$lang['laststep']="Abschluss der Installation";
$lang['ftpmode']="Verzeichnisse per FTP erzeugen (safe_mode)";
$lang['idomanual']="Ich erstelle die Verzeichnisse manuell";
$lang['dofrom']="utgående ifrån";
$lang['ftpmode2']="Erstelle die Verzeichnisse per FTP:";
$lang['connect']="förbind";
$lang['dirs_created']="Alla mappar har skapats.";
$lang['connect_to']="förbind med";
$lang['changedir']="Hoppa till mapp";
$lang['changedirerror']="Hopp till mapp ej möjligt";
$lang['ftp_ok']="FTP-Parameter sind ok";
$lang['createdirs2']="Skapa mappar";
$lang['ftp_notconnected']="FTP-Verbindung nicht hergestellt!";
$lang['connwith']="Förbindelse med";
$lang['asuser']="som användare";
$lang['notpossible']="nicht möglich";
$lang['dircr1']="skapar arbetsmapp";
$lang['dircr2']="skapar backupmapp";
$lang['dircr3']="skapar strukturmapp";
$lang['dircr4']="skapar loggmapp";
$lang['dircr5']="skapar konfigureringsmapp";
$lang['indir']="bin im Verzeichnis";
$lang['check']="kontrollera mina mappar";
$lang['disabledfunctions']="Deaktiverade funktioner";
$lang['noftppossible']="Es stehen keine FTP-Funktionen zur Verfügung!";
$lang['nogzpossible']="Es stehen keine Kompressions-Funktionen zur Verfügung!";
$lang['ui1']="Es werden alle Arbeitsverzeichnisse incl. den darin enthaltenen Backups gelöscht.";
$lang['ui2']="Sind Sie sicher, dass Sie das möchten ?";
$lang['ui3']="Nein, sofort abbrechen";
$lang['ui4']="ja, bitte fortfahren";
$lang['ui5']="lösche Arbeitsverzeichnis";
$lang['ui6']="alles wurde erfolgreich gelöscht.";
$lang['ui7']="Bitte löschen Sie das Skriptverzeichnis";
$lang['ui8']="eine Ebene nach oben";
$lang['ui9']="Ein Fehler trat auf, löschen war nicht möglich</p>Fehler bei Verzeichnis ";
$lang['import']="Konfiguration importieren";
$lang['import1']="Einstellungen aus ";
$lang['import2']="Einstellungen hochladen und importieren";
$lang['import3']="Die Konfiguration wurde geladen...";
$lang['import4']="Die Konfiguration wurde gesichert.";
$lang['import5']="MySQLDumper starten";
$lang['import6']="Installations-Menü";
$lang['import7']="Konfiguration uploaden";
$lang['import8']="zurück zum Upload";
$lang['import9']="Dies ist keine Konfigurationssicherung!";
$lang['import10']="Die Konfiguration wurde erfolgreich hochgeladen...";
$lang['import11']="<strong>Fehler: </strong>Es gab Probleme beim Schreiben der sql_statements";
$lang['import12']="<strong>Fehler: </strong>Es gab Probleme beim Schreiben der config.php";
$lang['install_help_port']="(leer = Standardport)";
$lang['install_help_socket']="(leer = Standardsocket)";
$lang['tryagain']="nochmal versuchen";
$lang['socket']="Socket";
$lang['port']="Port";
$lang['found_no_db']="FEHLER: nicht gefundene Datenbank:";
$lang['found_db']="gefundene DB: ";
$lang['fm_fileupload']="Datei hochladen";
$lang['pass']="Passwort";
$lang['no_db_found_info']="The connection to the database was successfully established.<br>
Your userdata is valid and was accepted by the MySQL-Server.<br>
But MySQLDumper was not able to find any database.<br>
The automatic detection via script is blocked on some server.<br>
You must enter your databasename manually after the installation is finished.
Click on \"configuration\" \"Connection Parameter - display\" and enter the databasename there.";


?>