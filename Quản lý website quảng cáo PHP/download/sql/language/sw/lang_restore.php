<?php
//generated at 17.03.2007

$lang['restore_tables_completed0']="Es wurden bisher <b>%d</b> Tabellen angelegt.";
$lang['file_missing']="konnte Datei nicht finden";
$lang['restore_db']="Datenbank '<b>%s</b>' auf Server '<b>%s</b>'.";
$lang['restore_complete']="<b>%s</b> Tabellen wurden angelegt.";
$lang['restore_run1']="<br>Es wurden bisher <b>%s</b> von <b>%s</b> Datensätzen erfolgreich eingetragen.";
$lang['restore_run2']="<br>Momentan wird die Tabelle '<b>%s</b>' mit Datensätzen gefüllt.<br><br>";
$lang['restore_complete2']="<b>%s</b> Datensätze wurden eingetragen.";
$lang['restore_tables_completed']="Es wurden bisher <b>%d</b> von <b>%d</b> Tabellen angelegt.";
$lang['restore_total_complete']="<br><b>Herzlichen Glückwunsch.</b><br><br>Die Datenbank wurde komplett wiederhergestellt.<br>Alle Daten aus der Backupdatei wurden erfolgreich in die Datenbank eingetragen.<br><br>Alles fertig. :-)";
$lang['db_select_error']="<br>Fel:<br> val av databasen '<b>";
$lang['db_select_error2']="</b>' slog fel!";
$lang['file_open_error']="Fehler: Die Datei konnte nicht geöffnet werden.";
$lang['progress_over_all']="Fortschritt gesamt";
$lang['back_to_overview']="Databasöversikt";
$lang['restore_run0']="<br>Es wurden bisher <b>%s</b> Datensätze erfolgreich eingetragen.";
$lang['unknown_sqlcommand']="unknown SQL-Command";
$lang['notices']="Hinweise";


?>