<?php
//generated at 17.03.2007

$lang['installfinished']="<br>die Installation ist abgeschlossen   --> <a href=\"index.php\">starte MySQLDumper</a><br>";
$lang['install_forcescript']="MySQLDumper ohne Installation starten";
$lang['install_tomenu']="zum Hauptmenü";
$lang['installmenu']="Hauptmenü";
$lang['step']="Schritt";
$lang['install']="Installation";
$lang['uninstall']="Deinstallation";
$lang['tools']="Tools";
$lang['editconf']="Konfiguration bearbeiten";
$lang['osweiter']="ohne Speichern weiter";
$lang['errorman']="<strong>Fehler beim Schreiben der Konfiguration!</strong><br>Bitte editieren Sie die Datei ";
$lang['manuell']="manuell";
$lang['createdirs']="erstelle Verzeichnisse";
$lang['install_continue']="mit der Installation fortfahren";
$lang['connecttomysql']=" zu MySQL verbinden ";
$lang['dbparameter']="Datenbank-Parameter";
$lang['confignotwritable']="Die Datei \"config.php\" ist nicht beschreibbar.
Geben Sie ihr mit einem FTP-Programm entsprechende Rechte, z. B. den CHMod-Wert 0777.";
$lang['dbconnection']="Datenbank-Verbindung";
$lang['connectionerror']="Fehler: Es konnte keine Verbindung herstellt werden.";
$lang['connection_ok']="Datenbank-Verbindung wurde hergestellt.";
$lang['saveandcontinue']="speichern und Installation fortsetzen";
$lang['confbasic']="Grundeinstellungen";
$lang['install_step2finished']="Die Einstellungen wurden erfolgreich gesichert.";
$lang['install_step2_1']="Installation mit Standardkonfiguration fortsetzen";
$lang['laststep']="Abschluss der Installation";
$lang['ftpmode']="Verzeichnisse per FTP erzeugen (safe_mode)";
$lang['idomanual']="Ich erstelle die Verzeichnisse manuell";
$lang['dofrom']="ausgehend von";
$lang['ftpmode2']="Erstelle die Verzeichnisse per FTP:";
$lang['connect']="verbinden";
$lang['dirs_created']="Die Verzeichnisse wurden ordnungsgemäß erstellt.";
$lang['connect_to']="verbinde zu";
$lang['changedir']="Wechsel ins Verzeichnis";
$lang['changedirerror']="Wechsel ins Verzeichnis nicht möglich";
$lang['ftp_ok']="FTP-Parameter sind ok";
$lang['createdirs2']="Verzeichnisse erstellen";
$lang['ftp_notconnected']="FTP-Verbindung nicht hergestellt!";
$lang['connwith']="Verbindung mit";
$lang['asuser']="als Benutzer";
$lang['notpossible']="nicht möglich";
$lang['dircr1']="erstelle Arbeitsverzeichnis";
$lang['dircr2']="erstelle Backup-Verzeichnis";
$lang['dircr3']="erstelle Strukturverzeichnis";
$lang['dircr4']="erstelle Log-Verzeichnis";
$lang['dircr5']="erstelle Konfigurationsverzeichnis";
$lang['indir']="bin im Verzeichnis";
$lang['check']="überprüfen";
$lang['disabledfunctions']="Abgeschaltete Funktionen";
$lang['noftppossible']="Es stehen keine FTP-Funktionen zur Verfügung!";
$lang['nogzpossible']="Es stehen keine Kompressions-Funktionen zur Verfügung!";
$lang['ui1']="Es werden alle Arbeitsverzeichnisse incl. den darin enthaltenen Backups gelöscht.";
$lang['ui2']="Sind Sie sicher, dass Sie das möchten?";
$lang['ui3']="Nein, sofort abbrechen";
$lang['ui4']="ja, bitte fortfahren";
$lang['ui5']="lösche Arbeitsverzeichnis";
$lang['ui6']="alles wurde erfolgreich gelöscht.";
$lang['ui7']="Bitte löschen Sie das Skriptverzeichnis";
$lang['ui8']="eine Ebene nach oben";
$lang['ui9']="Ein Fehler trat auf, löschen war nicht möglich</p>Fehler bei Verzeichnis ";
$lang['import']="Konfiguration importieren";
$lang['import1']="Einstellungen aus ";
$lang['import2']="Einstellungen hochladen und importieren";
$lang['import3']="Die Konfiguration wurde geladen...";
$lang['import4']="Die Konfiguration wurde gesichert.";
$lang['import5']="MySQLDumper starten";
$lang['import6']="Installations-Menü";
$lang['import7']="Konfiguration hochladen";
$lang['import8']="zurück zum Upload";
$lang['import9']="Dies ist keine Konfigurationssicherung!";
$lang['import10']="Die Konfiguration wurde erfolgreich hochgeladen...";
$lang['import11']="<strong>Fehler: </strong>Es gab Probleme beim Schreiben der sql_statements.";
$lang['import12']="<strong>Fehler: </strong>Es gab Probleme beim Schreiben der config.php.";
$lang['install_help_port']="(leer = Standardport)";
$lang['install_help_socket']="(leer = Standardsocket)";
$lang['tryagain']="noch einmal versuchen";
$lang['socket']="Socket";
$lang['port']="Port";
$lang['found_no_db']="Fehler: nicht gefundene Datenbank:";
$lang['found_db']="gefundene DB: ";
$lang['fm_fileupload']="Datei hochladen";
$lang['pass']="Passwort";
$lang['no_db_found_info']="Die Verbindung zur Datenbank konnte erfolgreich hergestellt werden.<br>
Ihre Zugangsdaten sind gültig und wurden vom MySQL-Server akzeptiert.<br>
Leider konnte MySQLDumper keine Datenbank finden.<br>
Die automatische Erkennung per Programm ist bei manchen Hostern gesperrt.<br>
Sie müssen Ihre Datenbank nach dem Abschluß der Installation unter dem Menüpunkt \"Konfiguration\" \"Verbindungsparameter einblenden\" angeben.<br>
Bitte begeben Sie sich nach Abschluß der Installation umgehend dort hin und tragen den Namen Ihrer Datenbank dort ein.";


?>