<?php
//generated at 17.03.2007

$lang['dump_headline']="erzeuge Backup...";
$lang['gzip_compression']="GZip-Kompression";
$lang['saving_table']="Speichere Tabelle ";
$lang['of']="von";
$lang['actual_table']="Aktuelle Tabelle";
$lang['progress_table']="Fortschritt Tabelle";
$lang['progress_over_all']="Fortschritt gesamt";
$lang['entry']="Eintrag";
$lang['done']="Fertig!";
$lang['dump_successful']=" wurde erfolgreich erstellt.";
$lang['upto']="bis";
$lang['email_was_send']="Die E-Mail wurde erfolgreich verschickt an ";
$lang['back_to_control']="weiter";
$lang['back_to_overview']="Datenbank-Übersicht";
$lang['dump_filename']="Backup-Datei: ";
$lang['withpraefix']="mit Praefix";
$lang['dump_notables']="Es konnten keine Tabellen in der Datenbank `<b>%s</b>` gefunden werden.";
$lang['dump_endergebnis']="Es wurden <b>%s</b> Tabellen mit insgesamt <b>%s</b> Datensätzen gesichert.<br>";
$lang['mailerror']="Leider ist beim Verschicken der E-Mail ein Fehler aufgetreten!";
$lang['emailbody_attach']="In der Anlage finden Sie die Sicherung Ihrer MySQL-Datenbank.<br>Sicherung der Datenbank `%s`
<br><br>Folgende Datei wurde erzeugt:<br><br>%s <br><br>Viele Grüße<br><br>MySQLDumper<br>";
$lang['emailbody_mp_noattach']="Es wurde eine Multipart-Sicherung erstellt.<br>Die Sicherungen werden nicht als Anhang mitgeliefert!<br>Sicherung der Datenbank `%s`
<br><br>Folgende Dateien wurden erzeugt:<br><br>%s<br><br><br>Viele Grüße<br><br>MySQLDumper<br>";
$lang['emailbody_mp_attach']="Es wurde eine Multipart-Sicherung erstellt.<br>Die Sicherungen werden in separaten E-Mails als Anhang geliefert!<br>Sicherung der Datenbank `%s`
<br><br>Folgende Dateien wurden erzeugt:<br><br>%s<br><br><br>Viele Grüße<br><br>MySQLDumper<br>";
$lang['emailbody_footer']="<br><br><br>Viele Grüße<br><br>MySQLDumper<br>";
$lang['emailbody_toobig']="Die Sicherung überschreitet die Maximalgröße von %s und wurde daher nicht angehängt.<br>Sicherung der Datenbank `%s`
<br><br>Folgende Datei wurde erzeugt:<br><br>%s
<br><br>Viele Grüße<br><br>MySQLDumper<br>";
$lang['emailbody_noattach']="Das Backup wurde nicht angehängt.<br>Sicherung der Datenbank `%s`
<br><br>Folgende Datei wurde erzeugt:<br><br>%s
<br><br>Viele Grüße<br><br>MySQLDumper<br>";
$lang['email_only_attachment']=" ... nur der Anhang";
$lang['tableselection']="Tabellenauswahl";
$lang['selectall']="alle auswählen";
$lang['deselectall']="Auswahl aufheben";
$lang['startdump']="Backup starten";
$lang['datawith']="Datensätze mit";
$lang['lastbufrom']="letztes Update vom";
$lang['not_supported']="Dieses Backup unterstützt diese Funktion nicht.";
$lang['multidump']="Multidump: Es wurden <b>%d</b> Datenbanken gesichert.";
$lang['filesendftp']="versende File via FTP... bitte habe etwas Geduld. ";
$lang['ftpconnerror']="FTP-Verbindung nicht hergestellt! Verbindung mit ";
$lang['ftpconnerror1']=" als Benutzer ";
$lang['ftpconnerror2']=" nicht möglich";
$lang['ftpconnerror3']="FTP-Upload war fehlerhaft! ";
$lang['ftpconnected1']="Verbunden mit ";
$lang['ftpconnected2']=" auf ";
$lang['ftpconnected3']=" geschrieben";
$lang['nr_tables_selected']="- mit %s gewählten Tabellen";
$lang['nr_tables_optimized']="<span class=\"small\">%s Tabellen wurden optimiert.</span>";
$lang['dump_errors']="<p class=\"error\">%s Fehler aufgetreten: <a href=\"log.php?r=3\">anzeigen</a></p>";


?>