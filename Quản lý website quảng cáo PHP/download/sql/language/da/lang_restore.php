<?php
//generated at 17.03.2007

$lang['restore_tables_completed0']="Foreløbigt er der oprettet <b>%d</b> tabeller.";
$lang['file_missing']="kunne ikke finde fil";
$lang['restore_db']="Database '<b>%s</b>' på '<b>%s</b>'.";
$lang['restore_complete']="<b>%s</b> tabeller oprettet.";
$lang['restore_run1']="<br>Foreløbigt er der korrekt tilføjet  <b>%s</b> af <b>%s</b> poster.";
$lang['restore_run2']="<br>Tabellen '<b>%s</b>' er under genetablering.<br><br>";
$lang['restore_complete2']="<b>%s</b> poster indsat.";
$lang['restore_tables_completed']="Foreløbigt er der oprettet <b>%d</b> af <b>%d</b> tabeller.";
$lang['restore_total_complete']="<br><b>Tillykke.</b><br><br>Genetableringen af databasen er færdig.<br>Alle data fra Backupfilen er blevet genetableret.<br><br>Processen er færdig. :-)";
$lang['db_select_error']="<br>Fejl:<br>Valg af database <b>";
$lang['db_select_error2']="</b> fejlede!";
$lang['file_open_error']="Fejl: kunne ikke åbne fil.";
$lang['progress_over_all']="Samlede fremskridt";
$lang['back_to_overview']="Database-oversigt";
$lang['restore_run0']="<br>foreløbigt er der korrekt tilføjet <b>%s</b> poster.";
$lang['unknown_sqlcommand']="ukendt SQL-kommando";
$lang['notices']="Bemærkninger";


?>