<?php
//generated at 17.03.2007

$lang['noftppossible']="Du har ingen FTP-funktioner til rådighed!";
$lang['info_location']="Din lokation er ";
$lang['info_databases']="Følgende database(r) er tilgængelige på din server:";
$lang['info_nodb']="database findes ikke.";
$lang['info_table1']="Tabel";
$lang['info_table2']="ler";
$lang['info_dbdetail']="Detaljeret info om database ";
$lang['info_dbempty']="Databasen er tom !";
$lang['info_records']="Poster";
$lang['info_size']="Størrelse";
$lang['info_lastupdate']="Sidst opdateret";
$lang['info_sum']="total";
$lang['info_optimized']="optimeret";
$lang['optimize_databases']="Optimér tabeller";
$lang['check_tables']="Check Tabeller";
$lang['clear_database']="Tøm database";
$lang['delete_database']="Slet database";
$lang['info_cleared']="blev tømt";
$lang['info_deleted']="blev slettet";
$lang['info_emptydb1']="Skal databasen";
$lang['info_emptydb2']=" virkelig tømmes? (Bemærk: Alle data vil være permanent tabt!)";
$lang['info_killdb']=" virkelig slettes? (Bemærk: Alle data vil være permanent tabt!)";
$lang['processkill1']="Scriptet forsøger at dræbe proces ";
$lang['processkill2']="at dræbe.";
$lang['processkill3']="Scriptet har forsøgt i   ";
$lang['processkill4']=" sek. at dræbe processen ";
$lang['htaccess1']="Opret folderbeskyttelse";
$lang['htaccess2']="Kodeord:";
$lang['htaccess3']="Kodeord (gentag):";
$lang['htaccess4']="Krypteringsmetode:";
$lang['htaccess5']="Crypt (Linux og Unix-systemer)";
$lang['htaccess6']="Linux og Unix-systemer (MD5)";
$lang['htaccess7']="plain text, ingen kryptering (Windows)";
$lang['htaccess8']="Der findes allerede en folderbeskyttelse. Hvis du opretter en ny, vil den tidligere blive overskrevet!";
$lang['htaccess9']="Du skal indtaste et navn!<br>";
$lang['htaccess10']="Kodeordene er ikke identiske eller tomme!<br>";
$lang['htaccess11']="Skal folderbeskyttelsen gemmes nu?";
$lang['htaccess12']="Folderbeskyttelsen blev oprettet.";
$lang['htaccess13']="Indhold af fil:";
$lang['htaccess14']="Der opstod en fejl ved oprettelse af folderbeskyttelsen!<br>Opret venligst de 2 filer manuelt med følgende indhold:";
$lang['htaccess15']="Stærkt anbefalet!";
$lang['htaccess16']="Rediger .htaccess";
$lang['htaccess17']="Opret og rediger .htaccess";
$lang['htaccess18']="Opret .htaccess i ";
$lang['htaccess19']="Genindlæs";
$lang['htaccess20']="Udfør script";
$lang['htaccess21']="Tilføj handler";
$lang['htaccess22']="Lav til eksekverbar";
$lang['htaccess23']="Folder-indholdslistning";
$lang['htaccess24']="Fejl-dokument";
$lang['htaccess25']="Aktivér rewrite";
$lang['htaccess26']="Deny / Allow";
$lang['htaccess27']="Redirect";
$lang['htaccess28']="Error Log";
$lang['htaccess29']="Flere eksempler og dokumentation";
$lang['htaccess30']="Leverandør";
$lang['htaccess31']="Generelt";
$lang['htaccess32']="Bemærk! .htaccess påvirker dirkte browserens opførsel.<br>Med forkert indhold kan disse sider blive utilgængelige.";
$lang['phpbug']="Fejl i zlib ! Ingen komprimering mulig!";
$lang['disabledfunctions']="Funktioner slået fra";
$lang['nogzpossible']="Da Zlib ikke er installeret/tilgængeligt, kan du ikke bruge GZip-funktionerne.


";
$lang['delete_htaccess']="Fjern folderbeskyttelse (slet .htaccess)";
$lang['wrong_rights']="Kan ikke skrive til filen eller folderen '%s'.<br> Fil-rettighederne (chmod) er ikke sat korrekt eller har den forkerte ejer.<br> Sæt venligst de korrekte attributter via din FTP-klient.<br> Filen eller mappen skal være sat til %s.<br>";
$lang['cant_create_dir']="Kunne ikke oprette folderen '%s'. Opret den venligst med en FTP-klient.";


?>