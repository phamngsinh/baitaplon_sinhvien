<?php
//generated at 17.03.2007

$lang['help_db']="Dette er listen over alle Databaser";
$lang['help_praefix']="præfikset er en streng i begyndelsen af et tabelnavn, hvilket kan bruges som et filter.";
$lang['help_zip']="Komprimering med GZip - anbefalet tilstand er 'aktiveret'";
$lang['help_memorylimit']="Maksimal mængde hukommelse i Bytes for scriptet
0 = deaktiveret";
$lang['memory_limit']="Hukommelsesgrænse";
$lang['help_mail1']="Hvis aktiveret, sendes der efter backupprocessen en email med backuppen som vedhæftet fil.";
$lang['help_mail2']="Dette er emailens modtager.";
$lang['help_mail3']="Dette er emailens afsender.";
$lang['help_mail4']="Maksimal størrelse for vedhæftede på email, hvis 0 ignoreres indstillingen";
$lang['help_mail5']="Her kan du vælge hvorvidt backup skal sendes som email-vedhæftet";
$lang['help_ad1']="Hvis aktiveret slettes backupfiler automatisk.";
$lang['help_ad2']="maksimum antal dage hvori backupfilen bevares (for autoslet)
0 = deaktiveret";
$lang['help_ad3']="maksimum antal backupfilet (for autoslet)
0 = deaktiveret";
$lang['help_lang']="vælg dit sprog";
$lang['help_empty_db_before_restore']="For at eliminere nytteløse eller fejlagtige data kan du tømme databasen før genetablering";
$lang['help_cronmail']="Vælg om Cron jobbet skal sende Backup'en via Mail";
$lang['help_cronmailprg']="sti til dit mailprogram, standard er sendmail med given sti";
$lang['help_cronftp']="Vælg om Cron jobbet skal sende Backup'en via FTP";
$lang['help_cronzip']="Komprimering med GZip i Cron job- anbefalet tilstand er 'aktiveret' (dette kræver Compression-Lib installeret!)";
$lang['help_cronextender']="Filtype for Perl scripts, standard er '.pl'";
$lang['help_cronsavepath']="Navn på konfigurationsfilen for Perl scriptet";
$lang['help_cronprintout']="Hvis deaktiveret skrives output ikke på skærmen.
Dette er uafhængigt af skrivningen til logfilen.";
$lang['help_cronsamedb']="Brug samme database i Cron job som konfigureret under Database?";
$lang['help_crondbindex']="vælg databasen for Cron job";
$lang['help_cronmail_dump']="Vælg om Cron jobbet skal sende backuppen vedhæftet emailen.";
$lang['help_ftptransfer']="hvis aktiveret sendes filen via FTP.";
$lang['help_ftpserver']="Adresse på FTP-Serveren";
$lang['help_ftpport']="Port på FTP-Serveren, standard: 21";
$lang['help_ftpuser']="indtast brugernavn for FTP";
$lang['help_ftppass']="indtast kodeord for FTP";
$lang['help_ftpdir']="hvor er upload-folderen? indtast sti!";
$lang['help_speed']="Minimum og maksimum hastighed, standard er 50 til 5000 (for høje eller lave hastigheder kan forårsage timeouts!)";
$lang['speed']="Hastighedskontrol";
$lang['help_cronexecpath']="Placering af Perl scripts.
Startpunkt er HTTP-Adressen (som Adresser i Browseren)
Tilladt er absolutte eller relative angivelser.";
$lang['cron_execpath']="Sti til Perl scripts";
$lang['help_croncompletelog']="Når aktiveret, skrives det fuldstændige output til complete_log-filen.
Dette er uafhængigt af tekstudskrifter";
$lang['help_ftp_mode']="Hvis du oplever problemer med FTP-overførsel, brug passiv tilstand.";


?>