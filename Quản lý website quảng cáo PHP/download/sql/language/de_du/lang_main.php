<?php
//generated at 17.03.2007

$lang['noftppossible']="Es stehen keine FTP-Funktionen zur Verfügung!";
$lang['info_location']="Du befindest dich auf ";
$lang['info_databases']="Folgende Datenbank(en) befinden sich auf dem MySql-Server:";
$lang['info_nodb']="Datenbank existiert nicht";
$lang['info_table1']="Tabelle";
$lang['info_table2']="n";
$lang['info_dbdetail']="Detail-Information von Datenbank ";
$lang['info_dbempty']="Die Datenbank ist leer!";
$lang['info_records']="Datensätze";
$lang['info_size']="Größe";
$lang['info_lastupdate']="letztes Update";
$lang['info_sum']="insgesamt";
$lang['info_optimized']="optimiert";
$lang['optimize_databases']="Tabellen optimieren";
$lang['check_tables']="Tabellen überprüfen";
$lang['clear_database']="Datenbank leeren";
$lang['delete_database']="Datenbank löschen";
$lang['info_cleared']="wurde geleert";
$lang['info_deleted']="wurde gelöscht";
$lang['info_emptydb1']="Soll die Datenbank";
$lang['info_emptydb2']=" wirklich geleert werden? (Achtung! Alle Daten gehen unwiderruflich verloren)";
$lang['info_killdb']=" wirklich gelöscht werden? (Achtung! Alle Daten gehen unwiderruflich verloren)";
$lang['processkill1']="Es wird versucht, Prozess ";
$lang['processkill2']="zu beenden.";
$lang['processkill3']="Es wird seit ";
$lang['processkill4']=" Sekunde(n) versucht, Prozess ";
$lang['htaccess1']="Verzeichnisschutz erstellen";
$lang['htaccess2']="Passwort:";
$lang['htaccess3']="Passwort (Wiederholung):";
$lang['htaccess4']="Verschlüsselungsart:";
$lang['htaccess5']="Linux und Unix-Systeme (Crypt)";
$lang['htaccess6']="Linux and Unix-Systeme (MD5)";
$lang['htaccess7']="unverschlüsselt (Windows)";
$lang['htaccess8']="Es besteht bereits ein Verzeichnisschutz. Wenn Du einen neuen erstellst, wird der alte überschrieben!";
$lang['htaccess9']="Du musst einen Namen eingeben!<br>";
$lang['htaccess10']="Die Passwörter sind nicht identisch oder leer!<br>";
$lang['htaccess11']="Soll der Verzeichnisschutz jetzt erstellt werden?";
$lang['htaccess12']="Der Verzeichnisschutz wurde erstellt.";
$lang['htaccess13']="Inhalt der Datei";
$lang['htaccess14']="Es ist ein Fehler bei der Erstellung des Verzeichnisschutzes aufgetreten!<br>Bitte erzeuge die Dateien manuell mit folgendem Inhalt:";
$lang['htaccess15']="Dringend empfohlen!";
$lang['htaccess16']=".htaccess editieren";
$lang['htaccess17']=".htaccess erstellen und editieren";
$lang['htaccess18']=".htaccess erstellen in ";
$lang['htaccess19']=" neu laden ";
$lang['htaccess20']="Skript ausführen";
$lang['htaccess21']="Handler zufügen";
$lang['htaccess22']="Ausführbar machen";
$lang['htaccess23']="Verzeichnis-Listing";
$lang['htaccess24']="Error-Dokument";
$lang['htaccess25']="Rewrite aktivieren";
$lang['htaccess26']="Deny / Allow";
$lang['htaccess27']="Redirect";
$lang['htaccess28']="Error-Log";
$lang['htaccess29']="weitere Beispiele und Dokumentation";
$lang['htaccess30']="Provider";
$lang['htaccess31']="allgemein";
$lang['htaccess32']="Achtung! Die .htaccess hat eine direkte Auswirkung auf den Browser.<br>Bei falscher Anwendung sind die Seiten nicht mehr erreichbar.";
$lang['phpbug']="Bug in zlib! Keine Kompression möglich";
$lang['disabledfunctions']="Abgeschaltete Funktionen";
$lang['nogzpossible']="Da zlib nicht installiert ist, stehen keine GZip-Funktionen zur Verfügung.";
$lang['delete_htaccess']="Verzeichnisschutz entfernen (.htaccess löschen)";
$lang['wrong_rights']="Die Datei oder das Verzeichnis '%s' ist für mich nicht beschreibbar.<br>
Entweder hat sie/es den falschen Besitzer (Owner) oder die falschen Rechte (Chmod).<br> 
Bitte setze die richtigen Attribute mit Deinem FTP-Programm. <br>
Die Datei oder das Verzeichnis benötigt die Rechte %s.<br>";
$lang['cant_create_dir']="Ich konntes das Verzeichnis '%s' nicht erstellen.
Bitte erstelle es mit Deinem FTP-Programm.";


?>