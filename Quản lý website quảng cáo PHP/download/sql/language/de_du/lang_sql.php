<?php
//generated at 17.03.2007

$lang['command']="Befehl";
$lang['sql_view_normal']="Ansicht: normal";
$lang['sql_view_compact']="Ansicht: kompakt";
$lang['import_notable']="Es ist keine Tabelle für den Import ausgewählt!";
$lang['sql_warning']="Die Ausführung von SQL-Befehlen kann Daten manipulieren! Der Autor übernimmt keine Haftung bei Datenverlusten.";
$lang['sql_exec']="SQL-Befehl ausführen";
$lang['sql_dataview']="Daten-Ansicht";
$lang['sql_tableview']="Tabellen-Ansicht";
$lang['sql_vonins']="von insgesamt";
$lang['sql_nodata']="keine Datensätze";
$lang['sql_recordupdated']="Datensatz wurde geändert";
$lang['sql_recordinserted']="Datensatz wurde gespeichert";
$lang['sql_backdboverview']="zurück zur Datenbank-Übersicht";
$lang['sql_recorddeleted']="Datensatz wurde gelöscht";
$lang['asktableempty']="Soll die Tabelle `%s` geleert werden?";
$lang['sql_recordedit']="editiere Datensatz";
$lang['sql_recordnew']="Datensatz einfügen";
$lang['askdeleterecord']="Soll der Datensatz gelöscht werden?";
$lang['askdeletetable']="Soll die Tabelle `%s` gelöscht werden?";
$lang['sql_befehle']="SQL-Befehle";
$lang['sql_befehlneu']="neuer Befehl";
$lang['sql_befehlsaved1']="SQL-Befehl";
$lang['sql_befehlsaved2']="wurde hinzugefügt";
$lang['sql_befehlsaved3']="wurde gespeichert";
$lang['sql_befehlsaved4']="wurde nach oben gebracht";
$lang['sql_befehlsaved5']="wurde gelöscht";
$lang['sql_queryentry']="Die Abfrage enthält";
$lang['sql_columns']="Spalten";
$lang['askdbdelete']="Soll die Datenbank `%s` samt Inhalt wirklich gelöscht werden?";
$lang['askdbempty']="Soll die Datenbank `%s` wirklich geleert werden?";
$lang['askdbcopy']="Soll der Inhalt der Datenbank `%s` in die Datenbank `%s` kopiert werden?";
$lang['sql_tablenew']="Tabellen bearbeiten";
$lang['sql_output']="SQL-Ausgabe";
$lang['do_now']="jetzt ausführen";
$lang['sql_namedest_missing']="Name für die Zieldatenbank fehlt!";
$lang['askdeletefield']="Soll das Feld gelöscht werden?";
$lang['sql_commands_in']=" Zeilen in ";
$lang['sql_commands_in2']="  Sekunde(n) abgearbeitet.";
$lang['sql_out1']="Es wurden ";
$lang['sql_out2']="Befehle ausgeführt";
$lang['sql_out3']="Es gab ";
$lang['sql_out4']="Kommentare";
$lang['sql_out5']="Da die Ausgabe über 5000 Zeilen enthält, wird sie nicht angezeigt.";
$lang['sql_selecdb']="Datenbank auswählen";
$lang['sql_tablesofdb']="Tabellen der Datenbank";
$lang['sql_edit']="bearbeiten";
$lang['sql_nofielddelete']="Löschen nicht möglich, da eine Tabelle mindestens 1 Feld haben muss.";
$lang['sql_fielddelete1']="Das Feld";
$lang['sql_deleted']="wurde gelöscht.";
$lang['sql_changed']="wurde geändert.";
$lang['sql_created']="wurde angelegt.";
$lang['sql_nodest_copy']="Ohne Ziel kann nicht kopiert werden!";
$lang['sql_desttable_exists']="Zieltabelle existiert schon!";
$lang['sql_scopy']="Tabellenstruktur von `%s` wurde in Tabelle `%s` kopiert.";
$lang['sql_tcopy']="Tabelle `%s` wurde mit Daten in Tabelle `%s` kopiert.";
$lang['sql_tablenoname']="Tabelle braucht einen Namen!";
$lang['sql_tblnameempty']="Tabellenname darf nicht leer sein!";
$lang['sql_collatenotmatch']="Zeichensatz und Sortierung passen nicht zueinander!";
$lang['sql_fieldnamenotvalid']="Fehler: Kein gültiger Feldname";
$lang['sql_createtable']="Tabelle anlegen";
$lang['sql_copytable']="Tabelle kopieren";
$lang['sql_structureonly']="nur Struktur";
$lang['sql_structuredata']="Struktur und Daten";
$lang['sql_notablesindb']="Es befinden sich keine Tabellen in der Datenbank";
$lang['sql_selecttable']="Tabelle auswählen";
$lang['sql_showdatatable']="Daten der Tabelle anzeigen";
$lang['sql_tblpropsof']="Tabelleneigenschaften  von";
$lang['sql_editfield']="Editiere Feld";
$lang['sql_newfield']="Neues Feld";
$lang['sql_indexes']="Indizes";
$lang['sql_atposition']="an Position einfügen";
$lang['sql_first']="zuerst";
$lang['sql_after']="nach";
$lang['sql_changefield']="Feld ändern";
$lang['sql_insertfield']="Feld einfügen";
$lang['sql_insertnewfield']="neues Feld einfügen";
$lang['sql_tableindexes']="Indizes der Tabelle";
$lang['sql_allowdups']="Duplikate erlaubt";
$lang['sql_cardinality']="Kardinalität";
$lang['sql_tablenoindexes']="Die Tabelle enthält keine Indizes";
$lang['sql_createindex']="neuen Index erzeugen";
$lang['sql_wasemptied']="wurde geleert";
$lang['sql_renamedto']="wurde umbenannt in";
$lang['sql_dbcopy']="Der Inhalt der Datenbank `%s` wurde in die Datenbank `%s` kopiert.";
$lang['sql_dbscopy']="Die Struktur der Datenbank `%s` wurde in die Datenbank `%s` kopiert.";
$lang['sql_wascreated']="wurde erzeugt";
$lang['sql_renamedb']="Datenbank umbenennen";
$lang['sql_actions']="Aktionen";
$lang['sql_chooseaction']="Aktion wählen";
$lang['sql_deletedb']="Datenbank löschen";
$lang['sql_emptydb']="Datenbank leeren";
$lang['sql_copydatadb']="Inhalt in Datenbank kopieren";
$lang['sql_copysdb']="Struktur in Datenbank kopieren";
$lang['sql_imexport']="Im-/Export";
$lang['info_records']="Datensätze";
$lang['asktableemptykeys']="Sollen die Tabelle `%s` geleert und die Indizes zurückgesetzt werden?";
$lang['edit']="editieren";
$lang['delete']="löschen";
$lang['empty']="leeren";
$lang['emptykeys']="leeren und Indizes zurücksetzen";
$lang['sql_tableemptied']="Tabelle `%s` wurde geleert.";
$lang['sql_tableemptiedkeys']="Tabelle `%s` wurde geleert, und die Indizes wurden zurückgesetzt.";
$lang['sql_library']="SQL-Bibliothek";
$lang['sql_attributes']="Attribute";
$lang['sql_enumsethelp']="Bei Feldtypen ENUM und SET bitte bei Size die Werteliste eingeben.

Die Werte müssen in Hochkommas und mit Kommas getrennt sein.

Bei Benutzung von Sonderzeichen müssen diese mit \ (Backslash) maskiert werden.


Beispiele:

'a','b','c'

'ja','nein'

'x','y'";
$lang['sql_uploadedfile']="geladene Datei: ";
$lang['sql_import']="Import in Datenbank `%s`";
$lang['export']="Export";
$lang['import']="Import";
$lang['importoptions']="Import-Optionen";
$lang['csvoptions']="CSV-Optionen";
$lang['importtable']="Import in Tabelle";
$lang['newtable']="neue Tabelle";
$lang['importsource']="Import-Quelle";
$lang['fromtextbox']="aus Textfeld";
$lang['fromfile']="aus Datei";
$lang['emptytablebefore']="Tabelle vorher leeren";
$lang['createautoindex']="Auto-Index erzeugen";
$lang['csv_namefirstline']="Feldnamen in die erste Zeile";
$lang['csv_fieldseperate']="Felder getrennt mit";
$lang['csv_fieldsenclosed']="Felder eingeschlossen von";
$lang['csv_fieldsescape']="Felder escaped von";
$lang['csv_eol']="Zeilen getrennt mit";
$lang['csv_null']="Ersetze NULL durch";
$lang['csv_fileopen']="CSV-Datei öffnen";
$lang['importieren']="importieren";
$lang['sql_export']="Export aus Datenbank `%s`";
$lang['exportoptions']="Export-Optionen";
$lang['excel2003']="Excel ab 2003";
$lang['showresult']="Ergebnis anzeigen";
$lang['sendresultasfile']="Ergebnis als Datei senden";
$lang['exportlines']="<strong>%s</strong> Zeilen exportiert";
$lang['csv_fieldcount_nomatch']="Die Anzahl der Tabellenfelder stimmen nicht mit den zu importierenden Daten überein (%d statt %d).";
$lang['csv_fieldslines']="%d Felder ermittelt, insgesamt %d Zeilen";
$lang['csv_errorcreatetable']="Fehler beim Erstellen der Tabelle `%s`!";
$lang['fm_uploadfilerequest']="Bitte gib eine Datei an.";
$lang['csv_nodata']="Keine Daten zum Importieren gefunden!";
$lang['sqllib_generalfunctions']="allgemeine Funktionen";
$lang['sqllib_resetauto']="Auto-Wert zurücksetzen";
$lang['sqllib_boards']="Boards";
$lang['sqllib_deactivateboard']="Board deaktivieren";
$lang['sqllib_activateboard']="Board aktivieren";
$lang['sql_notablesselected']="Es sind keine Tabellen ausgewählt!";
$lang['tools']="Tools";
$lang['tools_toolbox']="Datenbank auswählen / Datenbankfunktionen / Im- und Export ";
$lang['sqllib_boardoffline']="Board offline";
$lang['sql_openfile']="SQL-Datei öffnen";
$lang['sql_openfile_button']="Hochaden";
$lang['max_upload_size']="Maximale Dateigröße";
$lang['sql_search']="Suche";
$lang['sql_searchwords']="Suchbegriff(e)";
$lang['start_sql_search']="Suche starten";
$lang['reset_searchwords']="Eingabe zurücksetzen";
$lang['search_explain']="Gib einen oder mehrere Suchbegriffe ein und starte die Suche durch einen Klick auf \"Suche starten\".
<br>Du kannst mehrere Suchbegriffe durch ein Leerzeichen getrennt eingeben.";
$lang['search_options']="Suchoptionen";
$lang['search_results']="Die Suche nach \"<b>%s</b>\" in der Tabelle \"<b>%s</b>\" lieferte folgende Treffer";
$lang['search_no_results']="Die Suche nach \"<b>%s</b>\" in der Tabelle \"<b>%s</b>\" liefert keine Ergebnisse!";
$lang['no_entries']="Die Tabelle \"<b>%s</b>\" ist leer und hat keine Einträge.";
$lang['search_access_keys']="Blättern: vor=ALT+V, zurück=ALT+C";
$lang['search_options_or']="eine Spalte muss mindestens einen Suchbegriff enthalten (ODER-Suche)";
$lang['search_options_concat']="ein Datensatz muss alle Suchbegriffe enthalten, diese können aber in beliebigen Spalten sein (Rechenintensiv!)";
$lang['search_options_and']="eine Spalte muss alle Suchbegriffe enthalten (UND-Suche)";
$lang['search_in_table']="Suche in Tabelle";
$lang['sql_edit_tablestructure']="Tabellenstruktur bearbeiten";
$lang['default_charset']="Standardzeichensatz";


?>