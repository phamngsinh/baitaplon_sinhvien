<?php
//generated at 17.03.2007

$lang['yes']="si";
$lang['to']="hasta";
$lang['activated']="activo";
$lang['not_activated']="inactivo";
$lang['error']="error";
$lang['of']=" de ";
$lang['added']="añadido";
$lang['db']="Base de datos";
$lang['dbs']="Bases de datos";
$lang['tables']="Tablas";
$lang['table']="Tabla";
$lang['records']="registros";
$lang['compressed']="comprimido (gz)";
$lang['notcompressed']="normal (sin comprimir)";
$lang['general']="general";
$lang['comment']="comentario";
$lang['filesize']="Tamaño de archivo";
$lang['all']="todos";
$lang['none']="ninguno";
$lang['with']=" con ";
$lang['dir']="directorio";
$lang['rechte']="derechos";
$lang['status']="Estado";
$lang['finished']="finalizado";
$lang['file']="archivo";
$lang['fields']="campos";
$lang['new']="nuevo";
$lang['charset']="juego de caracteres";
$lang['collation']="orden";
$lang['change']="cambiar";
$lang['in']="en";
$lang['do']="iniciar";
$lang['view']="ver";
$lang['existing']="existentes";
$lang['authors']="Autores";
$lang['back']="atrás";
$lang['normal']="normal";
$lang['db_host']="Host";
$lang['db_user']="Usuario";
$lang['db_pass']="Password";
$lang['info_scriptdir']="Directorio de MySQLDumper";
$lang['info_actdb']="Base de datos actual";
$lang['useconnection']="Usar conexión";
$lang['wrongconnectionpars']="Conexión errónea o sin parámetros !";
$lang['conn_not_possible']="Conexión imposible !";
$lang['servercaption']="Nombre del servidor";
$lang['help_servercaption']="Al utilizar varios sistemas, puede ser útil mostrar la dirección del servidor actual";
$lang['activate_multidump']="activar volcados múltiples (MultiDump)";
$lang['save']="Guardar";
$lang['reset']="Volver";
$lang['praefix']="prefijo de las tablas";
$lang['autodelete']="eliminación automática de las copias de seguridad";
$lang['max_backup_files_each1']="para todas las bases de datos";
$lang['max_backup_files_each2']="para cada base de datos";
$lang['saving_db_form']="Base de datos";
$lang['testconnection']="Probar conexión";
$lang['back_to_minisql']="editar base de datos";
$lang['critical_safemode']="<h3>ATENCIÓN: no se puede seguir de forma segura!</h3>Los siguientes directorios deben ser creados manualmente en el directorio del Script:<br><div style=";
$lang['create']="crear";
$lang['mailabsendererror']="El email no puede enviarse sin el campo 'de' !";
$lang['Ausgabe']="Entrega";
$lang['Zusatz']="Añadido";
$lang['Variabeln']="Variables";
$lang['berichtsent']="El informe de errores se ha enviado con éxito.";
$lang['autobericht']="Informe de errores generado automáticamente por";
$lang['berichtman1']="Por favor, envíe manualmente este email a";
$lang['Statusinformationen']="Estado";
$lang['Versionsinformationen']="Versión";
$lang['MySQL Dumper Informationen']="Información sobre MySQL Dumper";
$lang['Fehlerbericht']="Informe de errores";
$lang['backupfilesanzahl']="En el directorio de copias de seguridad se encuentran";
$lang['lastbackup']="Última copia de seguridad";
$lang['notavail']="<em>no disponible</em>";
$lang['vom']="de";
$lang['mysqlvars']="Variables de MySQL";
$lang['mysqlsys']="Datos de MySQL";
$lang['Status']="Estado";
$lang['Prozesse']="Proceso";
$lang['info_novars']="no hay variables disponibles";
$lang['Inhalt']="Contenido";
$lang['info_nostatus']="no hay estados disponibles";
$lang['info_noprocesses']="no hay procesos corriendo";
$lang['fm_freespace']="Espacio libre en el servidor";
$lang['load_database']="Refrescar la lista de BdD";
$lang['home']="Inicio";
$lang['config']="Configuración";
$lang['dump']="Copia de seguridad";
$lang['restore']="Restaurar";
$lang['file_manage']="Archivos";
$lang['log']="Log";
$lang['choose_db']="elegir base de datos";
$lang['credits']="Créditos / Ayuda";
$lang['multi_part']="Copia de seguridad en múltiples archivos";
$lang['logfilenotwritable']="No se puede escribir en el fichero de historial (log)!";
$lang['sql_error1']="Error de ejecución:";
$lang['sql_error2']="MySQL informa:";
$lang['unknown']="desconocido";
$lang['unknown_number_of_records']="desconocido";
$lang['cron_completelog']="Registrar todas las operaciones";
$lang['no']="no";
$lang['MySQLErrorDoc']="Documentación de errores MySQL";
$lang['downgrade']="Degradar (MySQL 4.x => 3.x)";
$lang['create_database']="crear nueva base de datos";
$lang['exportfinished']="Exportación finalizada.";
$lang['sql_browser']="Navegador-SQL";
$lang['server']="Servidor";
$lang['browse']="View";
$lang['mysql_connection_encoding']="Standard encoding of MySQL-Server";


?>