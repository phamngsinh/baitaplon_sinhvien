<?php
//generated at 17.03.2007

$lang['config_headline']="Configuración";
$lang['save_success']="La configuración se ha guardado con éxito.";
$lang['save_error']="¡La configuración no ha podido ser guardada!";
$lang['config_email']="Notificación por email";
$lang['config_autodelete']="Eliminación automática";
$lang['config_interface']="Interfaz";
$lang['multi_part_groesse']="tamaño máximo del archivo";
$lang['help_multipart']="Si selecciona archivos múltiples, se crearán varios archivos de copia de seguridad, el tamaño máximo de los cuales quedará determinado por las propiedades elegidas debajo";
$lang['help_multipartgroesse']="El tamaño máximo de los archivos individuales de copia de seguridad en caso de activar la copia de seguridad en archivos múltiples, queda determinado por este valor";
$lang['empty_db_before_restore']="Vaciar la base de datos antes de recuperar los valores";
$lang['allpars']="todos los parámetros";
$lang['cron_extender']="Extensión de los scripts";
$lang['cron_savepath']="Archivo de configuración";
$lang['cron_printout']="Salida de texto";
$lang['config_cronperl']="Propiedades de Crondump como script perl";
$lang['cron_mailprg']="Programa de email";
$lang['cron_ftp']="Enviar archivo de backup por FTP";
$lang['optimize']="Optimizar las tablas antes del backup";
$lang['help_optimize']="Si esta opción está activada, se optimizarán todas las tablas antes de cada copia de seguridad";
$lang['help_ftptimeout']="Tiempo de inactividad que tarda en cancelarse la conexión, 90 segundos por defecto.";
$lang['ftp_timeout']="Cancelación de la conexión";
$lang['help_ftpssl']="Choose if the connection will be established via SSL.";
$lang['config_askload']="Desea realmente sobreescribir la configuración actual con la configuración inicial?";
$lang['load']="Cargar config. inicial.\n";
$lang['load_success']="La configuración inicial ha sido cargada.";
$lang['cron_samedb']="Usar la base de datos actual";
$lang['cron_crondbindex']="Base de datos y prefijos de tablas<br>      para Cronjob";
$lang['withattach']=" con fichero adjunto";
$lang['withoutattach']=" sin fichero adjunto";
$lang['multidumpconf']="=Propiedades de Multidump=";
$lang['multidumpall']="=Todas las bases de datos=";
$lang['gzip']="Compresión GZip";
$lang['send_mail_form']="enviar un email";
$lang['send_mail_dump']="Adjuntar copia de seguridad";
$lang['email_adress']="Dirección de email";
$lang['email_subject']="Email desde donde se envía";
$lang['email_maxsize']="tamaño máximo del fichero adjunto";
$lang['age_of_files']="Antigüedad del archivo (en días)";
$lang['number_of_files_form']="Cantidad de archivos de copia de seguridad";
$lang['language']="Idioma";
$lang['list_db']="Bases de datos configuradas:";
$lang['config_ftp']="Transferencia por FTP de los backups";
$lang['ftp_transfer']="Transferencia FTP";
$lang['ftp_server']="Servidor";
$lang['ftp_port']="Puerto";
$lang['ftp_user']="Usuario";
$lang['ftp_pass']="Password";
$lang['ftp_dir']="Directorio de subida";
$lang['ftp_ssl']="Conexión segura mediante SSL-FTP";
$lang['ftp_useSSL']="conexión SSL usada";
$lang['sqlboxheight']="Altura del cuadro de SQL";
$lang['sqllimit']="Cantidad de registros por página";
$lang['bbparams']="Propiedades del código BB";
$lang['bbtextcolor']="Color de texto";
$lang['help_commands']="Antes y después de la copia de seguridad, puede hacer que se ejecute algún comando.\n
Puede tratarse de una sentencia SQL o de un archivo de comandos de sistema (por ejemplo, un script)";
$lang['command']="Comando";
$lang['wrong_connectionpars']="¡Parámetros de conexión incorrectos!";
$lang['connectionpars']="Parámetros de conexión";
$lang['extendedpars']="Parámetros extendidos";
$lang['fade_in_out']="mostrar/ocultar";
$lang['db_backuppars']="Propiedades de la copia de seguridad de la base de datos";
$lang['general']="Genéricas";
$lang['maxsize']="tamaño máx.";
$lang['backup_format']="Formato de copia de seguridad";
$lang['inserts_complete']="Inserts completos";
$lang['inserts_extended']="Inserts extendidos";
$lang['inserts_delayed']="Inserts con retraso";
$lang['inserts_ignore']="Inserts que ignoren los errores";
$lang['lock_tables']="bloquear tablas";
$lang['errorhandling_restore']="Tratamiento de los errores en la recuperación de datos";
$lang['ehrestore_continue']="informar de los errores y seguir";
$lang['ehrestore_stop']="detenerse";
$lang['in_mainframe']="en ventana principal";
$lang['in_leftframe']="en ventana de menu";
$lang['width']="ancho";
$lang['sql_befehle']="Comandos SQL";
$lang['download_languages']="Descargar otros idiomas";
$lang['download_styles']="Descargar otros temas";
$lang['connect_to']="Conectarse a ";
$lang['changedir']="Cambiando al directorio";
$lang['changedirerror']="No se ha podido cambiar el directorio";
$lang['ftp_ok']="La conexión se ha realizado correctamente";
$lang['install']="Instalación";
$lang['noftppossible']="Las funciones de FTP no están disponibles!";
$lang['found_db']="Encontrada BdD:";
$lang['ftp_choose_mode']="FTP Transfer Mode";
$lang['ftp_passive']="use passive mode";
$lang['help_ftp_mode']="Choose the passive mode when you discover problems while using the active mode.";
$lang['db_in_list']="The database '%s' couldn't be added because it is allready existing. ";
$lang['add_db_manually']="Add database manually";
$lang['db_manual_error']="Sorry, couldn't connect to database '%s'!";
$lang['db_manual_file_error']="Fileerror: couldn't insert database '%s'!";
$lang['no_db_found']="I couldn't find any database automatically!
Please blend in the connection paramter and enter the name of your database manually.";
$lang['connect_utf8']="Set database connection to utf8";


?>