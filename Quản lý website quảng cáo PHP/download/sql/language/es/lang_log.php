<?php
//generated at 17.03.2007

$lang['log_delete']="Eliminar fichero de historial (log)";
$lang['logfileformat']="Formato del fichero de historial (log)";
$lang['logfilenotwritable']="No se puede escribir en el fichero de historial (log)!";
$lang['noreverse']="Mostrar las entradas más antiguas primero";
$lang['reverse']="Mostrar las entradas más nuevas primero";


?>