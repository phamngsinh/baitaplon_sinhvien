<?php
//generated at 17.03.2007

$lang['command']="comando";
$lang['sql_view_normal']="Vista normal";
$lang['sql_view_compact']="Vista compacta";
$lang['import_notable']="¡No ha seleccionado ninguna tabla para importar!";
$lang['sql_warning']="La ejecución de comandos SQL sirve para manipular directamente los datos de la base de datos. Los autores no se responsabilizarán de la pérdida de datos ocurrida debido al uso de esta utilidad.";
$lang['sql_exec']="ejecutar comando SQL";
$lang['sql_dataview']="Vista de datos";
$lang['sql_tableview']="Vista de tablas";
$lang['sql_vonins']="de un total de";
$lang['sql_nodata']="No hay registros que mostrar";
$lang['sql_recordupdated']="Registro actualizado";
$lang['sql_recordinserted']="Registro insertado";
$lang['sql_backdboverview']="volver a la vista de bases de datos";
$lang['sql_recorddeleted']="Registro eliminado";
$lang['asktableempty']="¿Desea vaciar la tabla `%s`?";
$lang['sql_recordedit']="editar registro";
$lang['sql_recordnew']="insertar registro";
$lang['askdeleterecord']="¿Desea eliminar el registro?";
$lang['askdeletetable']="Desea eliminar la tabla `%s`?";
$lang['sql_befehle']="Comandos SQL";
$lang['sql_befehlneu']="nuevo comando";
$lang['sql_befehlsaved1']="El comando SQL";
$lang['sql_befehlsaved2']="ha sido insertado";
$lang['sql_befehlsaved3']="ha sido guardado";
$lang['sql_befehlsaved4']="ha sido desplazado hacia arriba";
$lang['sql_befehlsaved5']="ha sido eliminado";
$lang['sql_queryentry']="La consulta contiene";
$lang['sql_columns']="columnas";
$lang['askdbdelete']="¿Desea realmente eliminar la base de datos `%s` así como todos sus contenidos?";
$lang['askdbempty']="¿Desea realmente vaciar la base de datos `%s` ?";
$lang['askdbcopy']="¿Desea copiar el contenido de la base de datos `%s` a la base de datos `%s`?";
$lang['sql_tablenew']="Modificar tabla";
$lang['sql_output']="Salida de SQL";
$lang['do_now']="ejecutar ahora";
$lang['sql_namedest_missing']="¡Falta el nombre de destino!";
$lang['askdeletefield']="¿Desea eliminar el campo?";
$lang['sql_commands_in']=" líneas en ";
$lang['sql_commands_in2']=" registros modificados por segundo.";
$lang['sql_out1']="Se han ejecutado ";
$lang['sql_out2']="comandos";
$lang['sql_out3']="Hubo ";
$lang['sql_out4']="comentarios";
$lang['sql_out5']="Dado que el comando afecta más de 5000 registros, no se mostrarán los resultados.";
$lang['sql_selecdb']="Elija la base de datos";
$lang['sql_tablesofdb']="Tablas de la base de datos";
$lang['sql_edit']="editar";
$lang['sql_nofielddelete']="Eliminación imposible, ya que la tabla debe contener al menos un campo.";
$lang['sql_fielddelete1']="El campo";
$lang['sql_deleted']="ha sido eliminado";
$lang['sql_changed']="ha sido modificado.";
$lang['sql_created']="ha sido insertado.";
$lang['sql_nodest_copy']="¡Sin destino, no se puede copiar nada!";
$lang['sql_desttable_exists']="¡La tabla de destino ya existe!";
$lang['sql_scopy']="La estructura de tabla de `%s` ha sido copiada en la tabla `%s`.";
$lang['sql_tcopy']="La tabla `%s` ha sido copiada (con sus datos), en la tabla `%s`.";
$lang['sql_tablenoname']="¡La tabla necesita un nombre!";
$lang['sql_tblnameempty']="¡El nombre de la tabla no puede estar vacío!";
$lang['sql_collatenotmatch']="¡Este juego de caracteres y el orden solicitado no pueden funcionar juntos!";
$lang['sql_fieldnamenotvalid']="ERROR: nombre de campo inválido";
$lang['sql_createtable']="Crear tabla";
$lang['sql_copytable']="Copiar tabla";
$lang['sql_structureonly']="solamente estructura";
$lang['sql_structuredata']="estructura y datos";
$lang['sql_notablesindb']="No hay ninguna tabla en la base de datos";
$lang['sql_selecttable']="elegir tabla";
$lang['sql_showdatatable']="mostrar los datos de la tabla";
$lang['sql_tblpropsof']="Propiedades de tabla de";
$lang['sql_editfield']="editar campo";
$lang['sql_newfield']="nuevo campo";
$lang['sql_indexes']="índices";
$lang['sql_atposition']="insertar en la posición";
$lang['sql_first']="primero";
$lang['sql_after']="siguiente";
$lang['sql_changefield']="modificar campo";
$lang['sql_insertfield']="insertar campo";
$lang['sql_insertnewfield']="insertar nuevo campo";
$lang['sql_tableindexes']="Índices de la tabla";
$lang['sql_allowdups']="Se permiten duplicados";
$lang['sql_cardinality']="Cardinalidad";
$lang['sql_tablenoindexes']="La tabla no contiene ningún índice";
$lang['sql_createindex']="crear nuevo índice";
$lang['sql_wasemptied']="ha sido vaciada";
$lang['sql_renamedto']="ha sido renombrada a";
$lang['sql_dbcopy']="El contenido de la base de datos `%s` ha sido copiado a la base de datos `%s`.";
$lang['sql_dbscopy']="La estructura de la base de datos `%s` ha sido copiado a la base de datos `%s`.";
$lang['sql_wascreated']="ha sido creada con éxito";
$lang['sql_renamedb']="renombrar base de datos";
$lang['sql_actions']="Acciones";
$lang['sql_chooseaction']="Elija una acción";
$lang['sql_deletedb']="eliminar base de datos";
$lang['sql_emptydb']="vaciar base de datos";
$lang['sql_copydatadb']="Copiar contenido de la base de datos";
$lang['sql_copysdb']="Copiar estructura en la base de datos";
$lang['sql_imexport']="Im-/Exportar";
$lang['info_records']="Registros";
$lang['asktableemptykeys']="¿Desea vaciar la tabla `%s` y resetear sus índices?";
$lang['edit']="editar";
$lang['delete']="eliminar";
$lang['empty']="vaciar";
$lang['emptykeys']="vaciar y resetear los índices";
$lang['sql_tableemptied']="La tabla `%s` ha sido vaciada.";
$lang['sql_tableemptiedkeys']="La tabla `%s` ha sido eliminada, y los índices reinicializados.";
$lang['sql_library']="Librería SQL";
$lang['sql_attributes']="Atributos";
$lang['sql_enumsethelp']="En los campos tipo ENUM y SET, en vez del tamaño, dar el valor.
Los valores deben estar entrecomillados y separados por comas.
Si utiliza símbolos especiales, deben estar precedidos por \ (Backslash).
Ejemplos:
'a','b','c'
'sí','no'
'x','y'";
$lang['sql_uploadedfile']="Fichero cargado: ";
$lang['sql_import']="Importar a la base de datos `%s`";
$lang['export']="Exportar";
$lang['import']="Importar";
$lang['importoptions']="Opciones de importación";
$lang['csvoptions']="Opciones CSV";
$lang['importtable']="Importar a tabla";
$lang['newtable']="nueva tabla";
$lang['importsource']="Origen de la importación";
$lang['fromtextbox']="del campo de texto";
$lang['fromfile']="de un fichero";
$lang['emptytablebefore']="Vaciar la tabla antes de la operación";
$lang['createautoindex']="Crear índice automático";
$lang['csv_namefirstline']="Nombres de campo en la primera línea";
$lang['csv_fieldseperate']="Campos separados por";
$lang['csv_fieldsenclosed']="Campos delimitados por";
$lang['csv_fieldsescape']="Campos 'escapeados' con";
$lang['csv_eol']="separar líneas con";
$lang['csv_null']="reemplazar NULL con";
$lang['csv_fileopen']="abrir fichero CSV";
$lang['importieren']="Importar";
$lang['sql_export']="Exportar la base de datos `%s`";
$lang['exportoptions']="Opciones de exportación";
$lang['excel2003']="Excel a partir de la versión 2003";
$lang['showresult']="Mostrar resultados";
$lang['sendresultasfile']="Enviar resultados como archivo";
$lang['exportlines']="<strong>%s</strong> líneas exportadas";
$lang['csv_fieldcount_nomatch']="El número de campos no coincide con el de los datos a importar (%d en vez de  %d).";
$lang['csv_fieldslines']="%d campos reconocidos, totalizando %d líneas";
$lang['csv_errorcreatetable']="¡Error al crear la tabla `%s`!";
$lang['fm_uploadfilerequest']="Por favor, elija un archivo.";
$lang['csv_nodata']="¡No se han encontrado datos que importar!";
$lang['sqllib_generalfunctions']="funciones generales";
$lang['sqllib_resetauto']="reinicializar autoincremento";
$lang['sqllib_boards']="Foros";
$lang['sqllib_deactivateboard']="desactivar foro";
$lang['sqllib_activateboard']="activar foro";
$lang['sql_notablesselected']="¡No se han seleccionado tablas!";
$lang['tools']="Herramientas";
$lang['tools_toolbox']="Elección de base de datos / Funciones de base de datos / Im- y Exportar ";
$lang['sqllib_boardoffline']="Forum offline";
$lang['sql_openfile']="Open SQL-File";
$lang['sql_openfile_button']="Upload";
$lang['max_upload_size']="Tamaño máximo del fichero";
$lang['sql_search']="Search";
$lang['sql_searchwords']="Searchword(s)";
$lang['start_sql_search']="start search";
$lang['reset_searchwords']="reset searchwords";
$lang['search_explain']="Emnter one or more searchwords and start searching by clicking \"start search\".<br>
You can enter more than one searchword by separating them with a space.";
$lang['search_options']="Searchoptions";
$lang['search_results']="The search for \"<b>%s</b>\" in table \"<b>%s</b>\" brings the following results";
$lang['search_no_results']="The search for \"<b>%s</b>\" in table \"<b>%s</b>\" doesn't bring any hits!";
$lang['no_entries']="Table \"<b>%s</b>\" is empty and doesn't have any entry.";
$lang['search_access_keys']="Browse: forward=ALT+V, backwards=ALT+C";
$lang['search_options_or']="a column must have one of the searchwords (OR-search)";
$lang['search_options_concat']="a row must contain all of the searchwords but they can be in any column (could take some time)";
$lang['search_options_and']="a column must contain all searchwords (AND-search)";
$lang['search_in_table']="Search in table";
$lang['sql_edit_tablestructure']="Edit tablestructure";
$lang['default_charset']="Default character set";


?>