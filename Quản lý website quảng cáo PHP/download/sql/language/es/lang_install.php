<?php
//generated at 17.03.2007

$lang['installfinished']="<br>La Instalación ha terminado --> <a href=\"index.php\">MySQLDumper</a><br>";
$lang['install_forcescript']="Iniciar MySQLDumper sin Instalación";
$lang['install_tomenu']="al menú principal";
$lang['installmenu']="Menú principal";
$lang['step']="Paso";
$lang['install']="Instalación";
$lang['uninstall']="Desinstalación";
$lang['tools']="Herramientas";
$lang['editconf']="modificar configuración";
$lang['osweiter']="seguir sin guardar";
$lang['errorman']="<strong>Error al escribir la configuración!</b><br>Por favor, edite Vd. el fichero ";
$lang['manuell']="manualmente";
$lang['createdirs']="Directorios creados";
$lang['install_continue']="seguir con la instalación";
$lang['connecttomysql']=" conectarse a MySQL ";
$lang['dbparameter']="Parámetros de la base de datos";
$lang['confignotwritable']="No se ha podido modificar el fichero \"config.php\".
Por favor, utilice su programa de FTP y ejecute chmod de dicho fichero a 0777.";
$lang['dbconnection']="Conexión a la base de datos";
$lang['connectionerror']="Error: no se pudo realizar la conexión.";
$lang['connection_ok']="La conexión a la base de datos ha sido realizada con éxito.";
$lang['saveandcontinue']="guardar y seguir con la instalación";
$lang['confbasic']="Propiedades básicas";
$lang['install_step2finished']="Los datos de acceso a la base de datos han sido guardados.";
$lang['install_step2_1']="seguir con propiedades estándar";
$lang['laststep']="Fin de la instalación";
$lang['ftpmode']="Crear directorios por FTP (en modo seguro)";
$lang['idomanual']="Crearé los directorios manualmente";
$lang['dofrom']="hacer desde";
$lang['ftpmode2']="Creación de los directorios por FTP:";
$lang['connect']="conectar";
$lang['dirs_created']="Los directorios han sido correctamente creados.";
$lang['connect_to']="conectar a";
$lang['changedir']="cambiar al directorio";
$lang['changedirerror']="No ha sido posible realizar el cambio de directorio";
$lang['ftp_ok']="Los parámetros de FTP son correctos";
$lang['createdirs2']="Crear directorios";
$lang['ftp_notconnected']="Conexión por FTP no realizada!";
$lang['connwith']="Conectar con";
$lang['asuser']="como usuario";
$lang['notpossible']="imposible";
$lang['dircr1']="cree el directorio de trabajo ";
$lang['dircr2']="cree el directorio de copias de seguridad ";
$lang['dircr3']="cree el directorio de estructuras ";
$lang['dircr4']="cree el directorio de informes ";
$lang['dircr5']="cree el directorio de configuración ";
$lang['indir']="está en el directorio";
$lang['check']="comprobar";
$lang['disabledfunctions']="Funciones deshabilitadas";
$lang['noftppossible']="Las funciones de FTP no están disponibles!";
$lang['nogzpossible']="Las funciones de compressión no están disponibles!";
$lang['ui1']="Se van a eliminar todos los directorios de trabajo, incluídos aquellos que contengan copias de seguridad.";
$lang['ui2']="Está seguro de que desea realizar la operación ?";
$lang['ui3']="no, abortar inmediatamente";
$lang['ui4']="si, deseo continuar";
$lang['ui5']="eliminar directorio de trabajo";
$lang['ui6']="todo ha sido eliminado satisfactoriamente.";
$lang['ui7']="Por favor, elimine el directorio de los scripts";
$lang['ui8']="subir un directorio";
$lang['ui9']="Se ha producido un error, no ha sido posible eliminarlo</p>Error en el directorio ";
$lang['import']="importar configuración";
$lang['import1']="Importar propiedades de ";
$lang['import2']="Cargar configuración e importar datos";
$lang['import3']="La configuración ha sido cargada ...";
$lang['import4']="La configuración ha sido guardada.";
$lang['import5']="Iniciar MySQLDumper";
$lang['import6']="Menú de instalación";
$lang['import7']="subir configuración";
$lang['import8']="volver a subir";
$lang['import9']="Este no es ningun fichero de configuración !";
$lang['import10']="La configuración ha sido cargada con éxito ...";
$lang['import11']="<strong>ERROR: </strong>Ha habido problemas al guardar sql_statements";
$lang['import12']="<strong>ERROR: </strong>Ha habido problemas al guardar config.php";
$lang['install_help_port']="(vacío = Puerto estándar)";
$lang['install_help_socket']="(vacío = Socket estándar)";
$lang['tryagain']="intentar nuevamente";
$lang['socket']="Socket";
$lang['port']="Puerto";
$lang['found_no_db']="ERROR: No se ha encontrado la BdD:";
$lang['found_db']="Encontrada BdD:";
$lang['fm_fileupload']="Subir archivo";
$lang['pass']="Password";
$lang['no_db_found_info']="The connection to the database was successfully established.<br>
Your userdata is valid and was accepted by the MySQL-Server.<br>
But MySQLDumper was not able to find any database.<br>
The automatic detection via script is blocked on some server.<br>
You must enter your databasename manually after the installation is finished.
Click on \"configuration\" \"Connection Parameter - display\" and enter the databasename there.";


?>