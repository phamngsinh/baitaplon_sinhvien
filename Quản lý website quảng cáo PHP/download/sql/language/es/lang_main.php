<?php
//generated at 17.03.2007

$lang['noftppossible']="Las funciones de FTP no están disponibles!";
$lang['info_location']="Se encuentra en ";
$lang['info_databases']="Las siguentes bases de datos se encuentran en el servidor de MySQL:";
$lang['info_nodb']="Base de datos inexistente";
$lang['info_table1']="Tabla";
$lang['info_table2']="s";
$lang['info_dbdetail']="Vista detallada de la base de datos ";
$lang['info_dbempty']="La base de datos está vacía !";
$lang['info_records']="Registros";
$lang['info_size']="Tamaño";
$lang['info_lastupdate']="última actualización";
$lang['info_sum']="total";
$lang['info_optimized']="optimizado";
$lang['optimize_databases']="Optimizar tablas";
$lang['check_tables']="Comprobar tablas";
$lang['clear_database']="Vaciar base de datos";
$lang['delete_database']="Eliminar base de datos";
$lang['info_cleared']="ha sido vaciada";
$lang['info_deleted']="ha sido eliminada";
$lang['info_emptydb1']="Debe ser la base de datos";
$lang['info_emptydb2']=" verdaderamente vaciada? (ALERTA: los datos se perderán irremisiblemente)";
$lang['info_killdb']=" verdaderamente eliminada? (ALERTA: los datos se perderán irremisiblemente)";
$lang['processkill1']="Se intentará, terminar el proceso ";
$lang['processkill2']=".";
$lang['processkill3']="Se ha intentado desde hace ";
$lang['processkill4']=" seg. para eliminar el proceso";
$lang['htaccess1']="Crear protección de directorio";
$lang['htaccess2']="Password:";
$lang['htaccess3']="Password (Repetir):";
$lang['htaccess4']="Tipo de encriptación:";
$lang['htaccess5']="Linux y sistemas Unix (Crypt)";
$lang['htaccess6']="Linux y sistemas Unix (MD5)";
$lang['htaccess7']="sin encriptación (Windows)";
$lang['htaccess8']="Ya existe actualmente una protección del directorio. ¡Si crea una nueva, la antigua será sobreescrita!";
$lang['htaccess9']="Debe darle un nombre!<br>";
$lang['htaccess10']="¡Los passwords no son idénticos o están vacíos! ";
$lang['htaccess11']="¿Desea crear ahora la protección del directorio?";
$lang['htaccess12']="La protección del directorio ha sido creada.";
$lang['htaccess13']="Contenido del archivo";
$lang['htaccess14']="Se ha producido un error al crear la protección del directorio!<br>Por favor, coloque en él el siguiente archivo, con el contenido especificado:";
$lang['htaccess15']="¡Altamente recomendado!";
$lang['htaccess16']="editar .htaccess";
$lang['htaccess17']="crear y editar .htaccess";
$lang['htaccess18']="crear .htaccess en ";
$lang['htaccess19']=" cargar de nuevo ";
$lang['htaccess20']="Ejecutar script";
$lang['htaccess21']="Escriba el proveedor";
$lang['htaccess22']="Permitir ejecución";
$lang['htaccess23']="Listado de directorios";
$lang['htaccess24']="Documentos de error";
$lang['htaccess25']="Activar la reescritura";
$lang['htaccess26']="Denegar / Permitir";
$lang['htaccess27']="Redirecccionar";
$lang['htaccess28']="Historial de errores";
$lang['htaccess29']="otros ejemplos y documentación";
$lang['htaccess30']="Proveedor";
$lang['htaccess31']="conjunto";
$lang['htaccess32']="Alerta! El fichero .htaccess influye directamente el comportamiento de los navegadores.<br>Si lo crea de forma incorrecta, estas páginas no serán accesibles.";
$lang['phpbug']="¡Bug en zlib! No es posible comprimir archivos";
$lang['disabledfunctions']="Funciones deshabilitadas";
$lang['nogzpossible']="Dado que Zlib no está instalado, no puede usar las funciones de compresión GZip.";
$lang['delete_htaccess']="Remove directory protection (delete .htaccess)";
$lang['wrong_rights']="The file or the directory '%s' is not writable for me.<br>
The rights (chmod) are not set properly or it has the wrong owner.<br>
Pleae set the correct attributes using your FTP-Programm.<br>
The file or the directory needs to be set to %s.<br>";
$lang['cant_create_dir']="Couldn' t create dir '%s'. 
Please create it using your FTP-Programm.";


?>