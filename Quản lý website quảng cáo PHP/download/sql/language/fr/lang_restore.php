<?php
//generated at 17.03.2007

$lang['restore_tables_completed0']="Jusqu'à présent <b>%d</b> tables ont été créés.";
$lang['file_missing']="le fichier n'a pas été trouvé";
$lang['restore_db']="Base de données '<b>%s</b>' sur le serveur '<b>%s</b>'.";
$lang['restore_complete']="<b>%s</b> tables ont été créés.";
$lang['restore_run1']="<br>Jusqu'à présent <b>%s</b> de <b>%s</b> enregistrements ont été enregistrés.";
$lang['restore_run2']="<br>En ce moment la table '<b>%s</b>' avec ces enregistrements est en cours de traitement.<br><br>";
$lang['restore_complete2']="<b>%s</b> enregistrements ont été enregistrés.";
$lang['restore_tables_completed']="Jusqu'à présent <b>%d</b> de <b>%d</b> tables ont été créés.";
$lang['restore_total_complete']="<br><b>Toutes nos félicitations.</b><br><br>La base de données a été restaurée complètement.<br>Tous les fichiers de la copie de sauvegarde ont été enregistrés avec succès dans la base de données.<br><br>Restauration terminée. :-)";
$lang['db_select_error']="<br>Erreur:<br>Choix de la base de données '<b>";
$lang['db_select_error2']="</b>' échoué!";
$lang['file_open_error']="Erreur: Le fichier n'a pas pu être ouvert.";
$lang['progress_over_all']="Progression totale";
$lang['back_to_overview']="Aperçu général des bases de données";
$lang['restore_run0']="<br>Jusqu'à présent <b>%s</b> enregistrements ont été enregistrés avec succès.";
$lang['unknown_sqlcommand']="Commande SQL inconnue";
$lang['notices']="Notices";


?>