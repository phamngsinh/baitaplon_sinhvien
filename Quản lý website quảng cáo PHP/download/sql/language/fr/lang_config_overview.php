<?php
//generated at 17.03.2007

$lang['config_headline']="Configuration";
$lang['save_success']="Les configurations ont été sauvegardées avec succès.";
$lang['save_error']="Les configurations n'ont pas pu être sauvegardées !";
$lang['config_email']="Envoyer courriel";
$lang['config_autodelete']="Suppression automatique";
$lang['config_interface']="Interface";
$lang['multi_part_groesse']="Taille maximale par fichier";
$lang['help_multipart']="Lors d'une sauvegarde en plusieurs parties plusieurs fichiers seront créés, dont la taille maximale s'oriente aux configurations ci-dessous";
$lang['help_multipartgroesse']="La taille maximale par fichiers de sauvergarde lors d'une sauvegarde en plusieurs parties peut être défini ici";
$lang['empty_db_before_restore']="Supprimer la base de données avant la restauration";
$lang['allpars']="Tous les paramètres";
$lang['cron_extender']="Extension du nom de fichier";
$lang['cron_savepath']="Fichier de configuration";
$lang['cron_printout']="Format d'impression";
$lang['config_cronperl']="Configuration Cron pour le script Perl";
$lang['cron_mailprg']="Programme du courriel";
$lang['cron_ftp']="Envoyer une copie de sauvegarde par FTP";
$lang['optimize']="Optimiser les tables avant la sauvegarde";
$lang['help_optimize']="Si cette option est activée, avant chaque sauvegarde les tables seront optimisées";
$lang['help_ftptimeout']="Temps avant temporisation si aucun transfert n'est effectué, par défaut 90 sec.";
$lang['ftp_timeout']="Erreur de temporisation";
$lang['help_ftpssl']="Choisir si la connexion doit être établie via SSL.";
$lang['config_askload']="Voulez-vous vraiment écraser les configurations actuelles avec les configurations par défaut?";
$lang['load']="Charger configurations\npar défaut";
$lang['load_success']="Les configurations par défaut ont été chargées.";
$lang['cron_samedb']="Utiliser la base de données actuelle";
$lang['cron_crondbindex']="Base de données et<br>préfixe des tables<br>pour la requête Cron";
$lang['withattach']=" avec fichier joint";
$lang['withoutattach']=" sans fichier joint";
$lang['multidumpconf']="=Configuration de la sauvegarde en plusieurs parties=";
$lang['multidumpall']="=toutes les bases de données=";
$lang['gzip']="Compression GZip des données";
$lang['send_mail_form']="Envoyer un courriel";
$lang['send_mail_dump']="Joindre le fichier de sauvegarde";
$lang['email_adress']="Adresse électronique";
$lang['email_subject']="Expéditeur du courriel";
$lang['email_maxsize']="Taille maximale du fichier joint";
$lang['age_of_files']="Âge du fichier (en jours)";
$lang['number_of_files_form']="Nombre de fichier de sauvegarde";
$lang['language']="Langue";
$lang['list_db']="Base de données configurée:";
$lang['config_ftp']="Tranfert FTP du fichier de sauvegarde";
$lang['ftp_transfer']="Transfert FTP";
$lang['ftp_server']="Serveur";
$lang['ftp_port']="Port";
$lang['ftp_user']="Utilisateur";
$lang['ftp_pass']="Mot de passe";
$lang['ftp_dir']="Répertoire de téléchargement vers le serveur";
$lang['ftp_ssl']="Connexion SSL vers le serveur FTP";
$lang['ftp_useSSL']="utiliser une connexion SSL";
$lang['sqlboxheight']="Hauteur du cadre SQL";
$lang['sqllimit']="Nombre d'enregistrements par page";
$lang['bbparams']="Configuration pour le code BB";
$lang['bbtextcolor']="Couleur du texte";
$lang['help_commands']="On peut avant et après chaque sauvegarde exécuter une requête.
Ceci peut-être un ordre SQL ou bien une requête de système (p. ex. un script)";
$lang['command']="Requête";
$lang['wrong_connectionpars']="Les paramètres de connexion sont faux !";
$lang['connectionpars']="Paramètres de connexion";
$lang['extendedpars']="Paramètres avancés";
$lang['fade_in_out']="afficher / masquer";
$lang['db_backuppars']="Configuration des bases de données sauvegardées";
$lang['general']="Général";
$lang['maxsize']="Taille maximale";
$lang['backup_format']="Format de sauvegarde";
$lang['inserts_complete']="Insertions complètes";
$lang['inserts_extended']="Insertions étendues";
$lang['inserts_delayed']="Insertions avec délais";
$lang['inserts_ignore']="Ignorer les erreurs de doublons";
$lang['lock_tables']="Protéger les tables";
$lang['errorhandling_restore']="Traitement des erreurs lors d'une restauration";
$lang['ehrestore_continue']="continuer et consigner par écrit les erreurs";
$lang['ehrestore_stop']="arrêter";
$lang['in_mainframe']="dans le cadre principal";
$lang['in_leftframe']="dans le cadre de gauche";
$lang['width']="Largeur";
$lang['sql_befehle']="Requête SQL";
$lang['download_languages']="télécharger d'autres langues";
$lang['download_styles']="télécharger d'autres thèmes";
$lang['connect_to']="Connecter à";
$lang['changedir']="Changer vers répertoire";
$lang['changedirerror']="N'a pas pu changer vers le répertoire!";
$lang['ftp_ok']="La connexion a été établie avec succès.";
$lang['install']="Installation";
$lang['noftppossible']="Il n'y a pas de fonction FTP à disposition!";
$lang['found_db']="Base de données trouvée";
$lang['ftp_choose_mode']="Mode de transfert FTP";
$lang['ftp_passive']="utiliser le mode passif";
$lang['help_ftp_mode']="Choisissez le mode passif quand vous avez des problèmes en utilisant le mode actif.";
$lang['db_in_list']="la base de données '%s' ne peut pas être ajoutée car elle existe déjà. ";
$lang['add_db_manually']="Ajouter une base de données manuellement";
$lang['db_manual_error']="Désolé, impossible de se connecter à la base de données '%s'!";
$lang['db_manual_file_error']="Erreur fichier: Impossible à insérer à la base de données '%s'!";
$lang['no_db_found']="Impossible de trouver une base de données automatiquement!
Veuillez vérifier les paramètres de connexion et entrer le nom de votre base de données manuellement.";
$lang['connect_utf8']="Régler la connexion à la base de données en utf8";


?>