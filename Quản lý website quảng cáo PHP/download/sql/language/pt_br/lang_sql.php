<?php
//generated at 17.03.2007

$lang['command']="Comando";
$lang['sql_view_normal']="Exibir: normal";
$lang['sql_view_compact']="Exibir: compacto";
$lang['import_notable']="Nenhuma tabela foi selecionada para importação!";
$lang['sql_warning']="A execução do comandos SQL pode manipular os dados. CUIDADO! Os autores não aceitam qualquer responsabilidade por danos ou perda de dados.";
$lang['sql_exec']="Executar comando SQL";
$lang['sql_dataview']="Exibir dados";
$lang['sql_tableview']="Exibir tabelas";
$lang['sql_vonins']="do total";
$lang['sql_nodata']="nenhum registro";
$lang['sql_recordupdated']="O registro foi atualizado";
$lang['sql_recordinserted']="O registro foi adicionado";
$lang['sql_backdboverview']="Voltar para Visão geral";
$lang['sql_recorddeleted']="O registro foi excluido";
$lang['asktableempty']="Deve a tabela `%s` ser esvaziada?";
$lang['sql_recordedit']="editar registro";
$lang['sql_recordnew']="novo registro";
$lang['askdeleterecord']="Você tem certeza em apagar este registro?";
$lang['askdeletetable']="Deve a tabela `%s` ser excluida?";
$lang['sql_befehle']="Comandos SQL";
$lang['sql_befehlneu']="Novo comando";
$lang['sql_befehlsaved1']="Comando SQL";
$lang['sql_befehlsaved2']="foi adicionada";
$lang['sql_befehlsaved3']="foi salva";
$lang['sql_befehlsaved4']="foi movida acima";
$lang['sql_befehlsaved5']="foi excluida";
$lang['sql_queryentry']="A consulta contém";
$lang['sql_columns']="Colunas";
$lang['askdbdelete']="Você quer excluir o banco de dados `%s` com seu conteúdo?";
$lang['askdbempty']="Você quer esvaziar o banco de dados `%s` ?";
$lang['askdbcopy']="Você quer copiar o banco de dados `%s` para o banco de dados `%s`?";
$lang['sql_tablenew']="Editar tabelas";
$lang['sql_output']="Saída SQL";
$lang['do_now']="executar agora";
$lang['sql_namedest_missing']="O nome da destinação está faltando !";
$lang['askdeletefield']="Você quer excluir o campo?";
$lang['sql_commands_in']=" linhas em ";
$lang['sql_commands_in2']="  sec. parsed.";
$lang['sql_out1']="Executado ";
$lang['sql_out2']="Comandos";
$lang['sql_out3']="Haviam ";
$lang['sql_out4']="Comentários";
$lang['sql_out5']="Devido à saída conter mais de 5000 linhas ela não será exibida.";
$lang['sql_selecdb']="Selecionar banco de dados";
$lang['sql_tablesofdb']="Tabelas do banco de dados";
$lang['sql_edit']="editar";
$lang['sql_nofielddelete']="A exclusão não é possível porque as tabelas devem ter pelo menos um campo.";
$lang['sql_fielddelete1']="O campo";
$lang['sql_deleted']="foi excluido";
$lang['sql_changed']="foi modificado.";
$lang['sql_created']="foi criado.";
$lang['sql_nodest_copy']="Nenhuma cópia sem destinação !";
$lang['sql_desttable_exists']="tabelas de destinação existem !";
$lang['sql_scopy']="A estrutura da tabela `%s` foi copiada para a tabela `%s`.";
$lang['sql_tcopy']="A tabela `%s` foi copiada com os dados da tabela `%s`.";
$lang['sql_tablenoname']="A tabela requer um nome!";
$lang['sql_tblnameempty']="O nome da tabela não pode ser nulo!";
$lang['sql_collatenotmatch']="O conjunto de caracteres e intercalação não combinam juntos!";
$lang['sql_fieldnamenotvalid']="Erro: Nenhum nome de campo válido";
$lang['sql_createtable']="crair tabela";
$lang['sql_copytable']="copiar tabela";
$lang['sql_structureonly']="Somente a estrutura";
$lang['sql_structuredata']="Estrutura e dados";
$lang['sql_notablesindb']="Nenhuma tabela encontrada no banco de dados";
$lang['sql_selecttable']="selecionar tabela";
$lang['sql_showdatatable']="Exibir dados da tabela";
$lang['sql_tblpropsof']="Propriedades da tabela";
$lang['sql_editfield']="Editar campo";
$lang['sql_newfield']="Novo campo";
$lang['sql_indexes']="Índices";
$lang['sql_atposition']="inserir na posição";
$lang['sql_first']="primeiro";
$lang['sql_after']="após";
$lang['sql_changefield']="modificar campo";
$lang['sql_insertfield']="inserir campo";
$lang['sql_insertnewfield']="inserir novo campo";
$lang['sql_tableindexes']="Índices da tabela";
$lang['sql_allowdups']="Duplicidade permitida";
$lang['sql_cardinality']="Cardinalmente";
$lang['sql_tablenoindexes']="Nenhum índices na tabela";
$lang['sql_createindex']="criar novo índice";
$lang['sql_wasemptied']="foi esvaziada";
$lang['sql_renamedto']="foi renomeado para";
$lang['sql_dbcopy']="O conteúdo do banco de dados `%s` foi copiado no banco de dados `%s`.";
$lang['sql_dbscopy']="A estrutura do banco de dados `%s` foi copiada no banco de dados `%s`.";
$lang['sql_wascreated']="foi criada";
$lang['sql_renamedb']="Renomear banco de dados";
$lang['sql_actions']="Ações";
$lang['sql_chooseaction']="Escolher ação";
$lang['sql_deletedb']="Excluir banco de dados";
$lang['sql_emptydb']="Esvaziar banco de dados";
$lang['sql_copydatadb']="Copiar todo o banco de dados para";
$lang['sql_copysdb']="Copiar a estrutura do banco de dados";
$lang['sql_imexport']="Importar-Exportar";
$lang['info_records']="registros";
$lang['asktableemptykeys']="Deve a tabela `%s` ser esvaziada e os índices reiniciados?";
$lang['edit']="editar";
$lang['delete']="excluir";
$lang['empty']="esvaziar";
$lang['emptykeys']="esvaziar e reiniciar os índices";
$lang['sql_tableemptied']="A tabela `%s` foi excluida.";
$lang['sql_tableemptiedkeys']="A tabela `%s` foi excluida e os índices reiniciados.";
$lang['sql_library']="Biblioteca SQL";
$lang['sql_attributes']="Atributos";
$lang['sql_enumsethelp']="com os tipos de campo ENUM e SET favor usar o tamanho para entrar a lista de valores.
Os valores devem estar entre aspas simples ' e separados por vírgulas.
Se usar caracteres especiais, estes devem ser precedidos por  (barra normal)
Exemplos:
'a','b','c'
'sim','não'
'x','y'";
$lang['sql_uploadedfile']="arquivo carregado: ";
$lang['sql_import']="Importar no banco de dados `%s`";
$lang['export']="Exportar";
$lang['import']="Importar";
$lang['importoptions']="Opções de importação";
$lang['csvoptions']="Opções de CSV";
$lang['importtable']="Importar para a tabela";
$lang['newtable']="nova tabela";
$lang['importsource']="Fonte da importação";
$lang['fromtextbox']="da caixa de texto";
$lang['fromfile']="do arquivo";
$lang['emptytablebefore']="Esvaziar a tabela antes";
$lang['createautoindex']="Criar um Auto-índice";
$lang['csv_namefirstline']="Nome dos campos na primeira linha";
$lang['csv_fieldseperate']="Campos separados por";
$lang['csv_fieldsenclosed']="Campos fechados por";
$lang['csv_fieldsescape']="Campos escapados com";
$lang['csv_eol']="Separar linhas com";
$lang['csv_null']="Substituir NULL com";
$lang['csv_fileopen']="Abrir arquivo CSV";
$lang['importieren']="Importar";
$lang['sql_export']="Exportar from banco de dados `%s`";
$lang['exportoptions']="Opções de exportação";
$lang['excel2003']="Excel de 2003";
$lang['showresult']="exibir resultado";
$lang['sendresultasfile']="enviar resultado como arquivo";
$lang['exportlines']="<strong>%s</strong> linhas exportadas";
$lang['csv_fieldcount_nomatch']="A contagem de campos não confere com a dos dados a importar (%d ao invés de %d).";
$lang['csv_fieldslines']="%d campos reconhecidos, totalizando %d linhas";
$lang['csv_errorcreatetable']="Erro durante a criação da tabela `%s` !";
$lang['fm_uploadfilerequest']="favor escolher um arquivo.";
$lang['csv_nodata']="Nenhum dado encontrado para importação!";
$lang['sqllib_generalfunctions']="funções gerais";
$lang['sqllib_resetauto']="reiniciar o auto-incremento";
$lang['sqllib_boards']="Quadros";
$lang['sqllib_deactivateboard']="desativar quadro";
$lang['sqllib_activateboard']="ativar quadro";
$lang['sql_notablesselected']="Nenhuma tabela selecionada !";
$lang['tools']="Ferramentas";
$lang['tools_toolbox']="Selecionar banco de dados / Funções de banco de dados / Importar - Exportar ";
$lang['sqllib_boardoffline']="Fórum offline";
$lang['sql_openfile']="Abrir arquivo SQL";
$lang['sql_openfile_button']="Upload";
$lang['max_upload_size']="Tamanho máximo de arquivo";
$lang['sql_search']="Pesquisar";
$lang['sql_searchwords']="Pesquisar palavra(s)";
$lang['start_sql_search']="iniciar pesquisa";
$lang['reset_searchwords']="reiniciar pesquisa de palavras";
$lang['search_explain']="Digite uma ou mais palavras para pesquisa e inicie a pesquisa clicando \"iniciar pesquisa\".<br>
Você pode digitar mais de uma palavra para pesquisa separando-as com um espaço.";
$lang['search_options']="Opções de pesquisa";
$lang['search_results']="A pesquisa por \"<b>%s</b>\" na tabela \"<b>%s</b>\" levou aos seguintes resultados";
$lang['search_no_results']="A pesquisa por \"<b>%s</b>\" na tabela \"<b>%s</b>\" não trouxe nenhum resultado!";
$lang['no_entries']="A tabela \"<b>%s</b>\" está vazia e não contêm nenhuma entrada.";
$lang['search_access_keys']="Navegação: para frente=ALT+V, para trás=ALT+C";
$lang['search_options_or']="a coluna deve conter uma das palavras a pesquisar (OU-pesquisar)";
$lang['search_options_concat']="a linha deve conter todas as palavras a pesquisar, mas elas podem estar em qualquer coluna (pode levar algum tempo)";
$lang['search_options_and']="a coluna deve conter todas as palavras a pesquisar (E-pesquisar)";
$lang['search_in_table']="Pesquisar na tabela";
$lang['sql_edit_tablestructure']="Edit tablestructure";
$lang['default_charset']="Default character set";


?>