<?php
//generated at 17.03.2007

$lang['dump_headline']="Criar backup...";
$lang['gzip_compression']="Compressão gzip";
$lang['saving_table']="Salvando a tabela ";
$lang['of']="of";
$lang['actual_table']="Tabela atual";
$lang['progress_table']="Progresso da tabela";
$lang['progress_over_all']="Progresso do todo";
$lang['entry']="Entrada";
$lang['done']="Pronto!";
$lang['dump_successful']=" foi criado com sucesso.";
$lang['upto']="até";
$lang['email_was_send']="Um email foi enviado com sucesso para ";
$lang['back_to_control']="Continuar";
$lang['back_to_overview']="Visão geral do banco de dados";
$lang['dump_filename']="Arquivo de backup: ";
$lang['withpraefix']="com o prefixo";
$lang['dump_notables']="Nenhuma tabela foi encontrada no banco de dados `<b>%s</b>` ";
$lang['dump_endergebnis']="O arquivo contém <b>%s</b> tabela(s) com <b>%s</b> registro(s).<br>";
$lang['mailerror']="O envio do email falhou!";
$lang['emailbody_attach']="O anexo contém o backup do seu banco de dados MySQL.<br>Backup do banco de dados `%s`
<br><br>O seguinte arquivo foi criado:<br><br>%s <br><br>Atenciosamente<br><br>MySQLDumper<br>";
$lang['emailbody_mp_noattach']="Um backup Multi-parte foi criad.<br>Os arquivos não estão anexados a este email!<br>Backup do banco de dados `%s`
<br><br>Os seguintes arquivos foram criados:<br><br>%s
<br><br>Atenciosamente<br><br>MySQLDumper<br>";
$lang['emailbody_mp_attach']="Um backup Multi-parte foi criado.<br>Os arquivos de backup estão anexados em emails separados.<br>Backup do banco de dados `%s`
<br><br>Os seguintes arquivos foram criados:<br><br>%s <br><br>Atenciosamente<br><br>MySQLDumper<br>";
$lang['emailbody_footer']="`<br><br>Atenciosamente<br><br>MySQLDumper<br>";
$lang['emailbody_toobig']="O arquivo de backup excedeu o tamanho máximo de %s e não foi anexado a este email.<br>Backup do banco de dados `%s`
<br><br>O seguinte arquivo foi criado:<br><br>%s
<br><br>Atenciosamente<br><br>MySQLDumper<br>";
$lang['emailbody_noattach']="Os arquivos não estão anexados a este email!<br>Backup do banco de dados `%s`
<br><br>O seguinte arquivo foi criado:<br><br>%s
<br><br>Atenciosamente<br><br>MySQLDumper<br>";
$lang['email_only_attachment']=" ... somente anexos.";
$lang['tableselection']="Seleção de tabela";
$lang['selectall']="Desselecionar tudo";
$lang['deselectall']="Desselecionar tudo";
$lang['startdump']="Iniciar backup";
$lang['datawith']="Registros com";
$lang['lastbufrom']="última atualização de";
$lang['not_supported']="Este backup não suporta esta função.";
$lang['multidump']="Multidump: Backup do(s) <b>%d</b> banco(s) de dados pronto.";
$lang['filesendftp']="enviando o arquivo via FTP... favor ter paciente. ";
$lang['ftpconnerror']="Conexão de FTP não estabelecida! Conexão com ";
$lang['ftpconnerror1']=" com o usuário ";
$lang['ftpconnerror2']=" impossível";
$lang['ftpconnerror3']="Envio por FTP falhou! ";
$lang['ftpconnected1']="Conectado com ";
$lang['ftpconnected2']=" em ";
$lang['ftpconnected3']=" trasnferido com sucesso";
$lang['nr_tables_selected']="- com %s tabelas selecionadas";
$lang['nr_tables_optimized']="<span class=\"small\">%s tabelas foram otimizadas.</span>";
$lang['dump_errors']="<p class=\"error\">%s erros ocorreram: <a href=\"log.php?r=3\">verdere</a></p>";


?>