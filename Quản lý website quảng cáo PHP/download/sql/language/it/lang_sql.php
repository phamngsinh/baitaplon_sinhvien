<?php
//generated at 17.03.2007

$lang['command']="comando";
$lang['sql_view_normal']="Visibiltà: normale";
$lang['sql_view_compact']="Visibiltà: compatta";
$lang['import_notable']="Non è stato scelta nessuna tabella per l`importo!";
$lang['sql_warning']="L`esucuzione di ordini SQL possono manipolare dati. L`autore non si prende nessuna responsabilità riguarda la perdita di questi dati.";
$lang['sql_exec']="Eseguire comandi SQL";
$lang['sql_dataview']="Visualizzare-dati";
$lang['sql_tableview']="Sommario-tabelle";
$lang['sql_vonins']="di totale";
$lang['sql_nodata']="nessun registro";
$lang['sql_recordupdated']="Registro è stato cambiato";
$lang['sql_recordinserted']="Registro è stato salvato";
$lang['sql_backdboverview']="torna dal Sommario-banca dati";
$lang['sql_recorddeleted']="Registro è stato cancellato";
$lang['asktableempty']="Vuoi svuotare la tabella `%s` ?";
$lang['sql_recordedit']="edit registro";
$lang['sql_recordnew']="inserire nuovo registro";
$lang['askdeleterecord']="Vuoi cancellare questo registro?";
$lang['askdeletetable']="Vuoi cancellare la tabella `%s` ?";
$lang['sql_befehle']="Comandi-SQL";
$lang['sql_befehlneu']="nuovo comando";
$lang['sql_befehlsaved1']="Comando-SQL";
$lang['sql_befehlsaved2']="e stato aggiunto";
$lang['sql_befehlsaved3']="e stato salvato";
$lang['sql_befehlsaved4']="e stato spostato sopra";
$lang['sql_befehlsaved5']="e stato cancellato";
$lang['sql_queryentry']="La richiesta contiene";
$lang['sql_columns']="Colonne";
$lang['askdbdelete']="Vuoi cancellare questa banca dati `%s` con il contenuto?";
$lang['askdbempty']="Vuoi svuotare veramente la banca dati `%s` ?";
$lang['askdbcopy']="Vuoi copiare il contenuto di questa banca dati `%s` in questa banca dati `%s`?";
$lang['sql_tablenew']="Edit tabella";
$lang['sql_output']="Distribuzione-SQL";
$lang['do_now']="esegui ora";
$lang['sql_namedest_missing']="Nome della banca dati di destinazione è mancante!";
$lang['askdeletefield']="Vuoi cancellare questo campo?";
$lang['sql_commands_in']=" riga in ";
$lang['sql_commands_in2']="  sec. parsed.";
$lang['sql_out1']="Eseguito";
$lang['sql_out2']="Comandi";
$lang['sql_out3']="C'erano";
$lang['sql_out4']="Commenti";
$lang['sql_out5']="Poiché l`uscita contiene piu di 5000 righe, non sono visibili.";
$lang['sql_selecdb']="Seleziona banca dati";
$lang['sql_tablesofdb']="Tabelle della banca dati";
$lang['sql_edit']="edit";
$lang['sql_nofielddelete']="Cancellare non è possibile , perche una tabella deve avere almeno un campo.";
$lang['sql_fielddelete1']="Il campo";
$lang['sql_deleted']="è stato cancellato";
$lang['sql_changed']="e stato cambiato.";
$lang['sql_created']="è stato creato.";
$lang['sql_nodest_copy']="Senza destinazione non si può copiare!";
$lang['sql_desttable_exists']="Tabella di destinazione esiste gia !";
$lang['sql_scopy']="La struttura della tabella `%s` e stata copiata in `%s`.";
$lang['sql_tcopy']="Tabella `%s` è stata copiata con dati in tabella `%s`.";
$lang['sql_tablenoname']="La tabella ha bisogno di un nome!";
$lang['sql_tblnameempty']="Il nome della tabella non deve essere vuoto!";
$lang['sql_collatenotmatch']="Serie di simboli(Charset) ed assortimento(Collation) sono incompatibili!";
$lang['sql_fieldnamenotvalid']="Errore: nome del campo non valido";
$lang['sql_createtable']="crea tabella";
$lang['sql_copytable']="copia tabelle";
$lang['sql_structureonly']="solo struttura";
$lang['sql_structuredata']="Struttura e Dati";
$lang['sql_notablesindb']="Non ci sono tabelle nella banca dati";
$lang['sql_selecttable']="Seleziona tabella";
$lang['sql_showdatatable']="Guarda dati della tabella";
$lang['sql_tblpropsof']="Proprietà della tabella da";
$lang['sql_editfield']="Edit campo";
$lang['sql_newfield']="Nuovo campo";
$lang['sql_indexes']="Indice";
$lang['sql_atposition']="inserire nella posizione";
$lang['sql_first']="primo";
$lang['sql_after']="dopo";
$lang['sql_changefield']="cambia campo";
$lang['sql_insertfield']="inserisci campo";
$lang['sql_insertnewfield']="inserisci nuovo campo";
$lang['sql_tableindexes']="Indice della tabella";
$lang['sql_allowdups']="Duplicati permessi";
$lang['sql_cardinality']="Cardinalità";
$lang['sql_tablenoindexes']="La tabella non contiene l`indice";
$lang['sql_createindex']="crea nuovo indice";
$lang['sql_wasemptied']="stato svuotato";
$lang['sql_renamedto']="è stato rinominato in";
$lang['sql_dbcopy']="Il contenuto della banca dati `%s` è stato copiato nella banca dati `%s`.";
$lang['sql_dbscopy']="La struttura della banca dati `%s` è stata copiata nella banca dati `%s`.";
$lang['sql_wascreated']="stato creato";
$lang['sql_renamedb']="Rinomina banca dati";
$lang['sql_actions']="Azioni";
$lang['sql_chooseaction']="Seleziona azione";
$lang['sql_deletedb']="Cancella banca dati";
$lang['sql_emptydb']="Svuota banca dati";
$lang['sql_copydatadb']="Copia completamento banca dati in";
$lang['sql_copysdb']="Copia struttura in banca dati";
$lang['sql_imexport']="Importo / Esporta";
$lang['info_records']="Registri";
$lang['asktableemptykeys']="Vuoi svuotare la tabella `%s` e resettare l`indice?";
$lang['edit']="edit";
$lang['delete']="elimina";
$lang['empty']="svuota";
$lang['emptykeys']="svuota e resetta l`indice";
$lang['sql_tableemptied']="Tabella `%s` e stata svuotata.";
$lang['sql_tableemptiedkeys']="La tabella `%s` è stata svuotata e l`ìndice è stato spostato indietro .";
$lang['sql_library']="Biblioteca-SQL";
$lang['sql_attributes']="Attributi";
$lang['sql_enumsethelp']="Nei campi tipo ENUM e SET mettere sotto Size il valore per favore.
I valori devono essere seperati con commata e commata in alto.
Se usate dei caratteri speciali devono essere \\ mascherati cosi.
Esempio:
'a','b','c'
'si','no'
'\x','\y'";
$lang['sql_uploadedfile']="Dato(file) caricato: ";
$lang['sql_import']="Importo della banca dati `%s`";
$lang['export']="Esporto";
$lang['import']="Importa";
$lang['importoptions']="Opzioni-importo";
$lang['csvoptions']="Opzioni-CSV";
$lang['importtable']="Importa nella tabella";
$lang['newtable']="tabella nuova";
$lang['importsource']="Sorgente-importo";
$lang['fromtextbox']="dal campo testo";
$lang['fromfile']="dal dato(file)";
$lang['emptytablebefore']="Svuota prima la tabella";
$lang['createautoindex']="Creare index automatico";
$lang['csv_namefirstline']="nome del campo nella prima riga";
$lang['csv_fieldseperate']="Campi seperati con";
$lang['csv_fieldsenclosed']="Campi inclusi in";
$lang['csv_fieldsescape']="Campi sfuggiti da";
$lang['csv_eol']="Linee seperate con";
$lang['csv_null']="Cambia ZERO con";
$lang['csv_fileopen']="aprire CSV-File";
$lang['importieren']="importare";
$lang['sql_export']="Esporto della banca dati `%s`";
$lang['exportoptions']="Opzioni-esporto";
$lang['excel2003']="Excel dal 2003";
$lang['showresult']="Visualizzare il risultato";
$lang['sendresultasfile']="Spedire risultato come dato(file)";
$lang['exportlines']="<strong>%s</strong> righe esportare";
$lang['csv_fieldcount_nomatch']="La quantità dei campi della tabella non coincidono con i dati da importare (%d al posti di %d).";
$lang['csv_fieldslines']="%d campi trovati, totale %d righe";
$lang['csv_errorcreatetable']="Errore nella creazione della tabella `%s` !";
$lang['fm_uploadfilerequest']="Scegli un dato(file), per favore.";
$lang['csv_nodata']="Non ho trovato nessun dato(file) da importare!";
$lang['sqllib_generalfunctions']="funzioni generali";
$lang['sqllib_resetauto']="ricollocare autoincremento";
$lang['sqllib_boards']="Boards";
$lang['sqllib_deactivateboard']="disattiva Board";
$lang['sqllib_activateboard']="attivare Board";
$lang['sql_notablesselected']="Non ci sono tabelle selezionate !";
$lang['tools']="Tools";
$lang['tools_toolbox']="Scegliere banca dati / Funzione banca dati / Importo e Esporto ";
$lang['sqllib_boardoffline']="Board disattivato";
$lang['sql_openfile']="aprire dato - SQL";
$lang['sql_openfile_button']="Caricare";
$lang['max_upload_size']="Grandezza massima del dato (file)";
$lang['sql_search']="Ricerca";
$lang['sql_searchwords']="Parola(e) di ricerca";
$lang['start_sql_search']="esegui ricerca";
$lang['reset_searchwords']="resetta parola di ricerca";
$lang['search_explain']="Inserisci una o piu parole e fai partire la ricerca cliccando sul pulsante \"iniziare ricerca\". Mettendo piu parole di ricerca bisogna lasciare lo spazzio tra una parola e l`altra.";
$lang['search_options']="Opzione di ricerca";
$lang['search_results']="La ricera di \"<b>%s</b>\" nella tabella \"<b>%s</b>\" ha portato al seguente risultato";
$lang['search_no_results']="La ricera \"<b>%s</b>\" nella tabella \"<b>%s</b>\" non ha risultati!";
$lang['no_entries']="La tabella \"<b>%s</b>\" è vuota e non ha nessuna iscrizione.";
$lang['search_access_keys']="Sfogliare: avanti=ALT+V, indietro=ALT+C";
$lang['search_options_or']="una colonna deve contenere almeno una parola di ricera (o-ricreca)";
$lang['search_options_concat']="un registro deve contenere tutte le parole di ricerca, pero possono essere in colonne diverse (Calcolo intensivo!)";
$lang['search_options_and']="una colonna deve contenere tutte le parole di ricerca ( e ricerca )";
$lang['search_in_table']="cerca nelle tabelle";
$lang['sql_edit_tablestructure']="Edit struttura tabelare";
$lang['default_charset']="Set di caretteri standard";


?>