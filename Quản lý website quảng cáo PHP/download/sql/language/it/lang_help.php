<?php
//generated at 17.03.2007

$lang['help_db']="Questa è la lista delle banche dati presenti";
$lang['help_praefix']="Il prefix è un continuo di lettere per l`inizio di una tabella che funziona come un filtro.";
$lang['help_zip']="Compressione con GZip - stato consigliato è 'attivato'";
$lang['help_memorylimit']="Questa è la grandezza massima di Bytes come memoria che può avere lo script\n0 = disattivato";
$lang['memory_limit']="Limite di memoria";
$lang['help_mail1']="Se attivato viene spedito una email con il backup in allegato. ";
$lang['help_mail2']="Questo è il destinatario  della email.";
$lang['help_mail3']="Questo è il mittente della email.";
$lang['help_mail4']="La grandezza massima per un allegato email, se è 0, l`inserito viene ignorato.";
$lang['help_mail5']="Qui puoi decidere se il backup deve essere spedito come allegato tramite email.";
$lang['help_ad1']="Se attivato, vengono cancellati automaticamente i dati (file) backup.";
$lang['help_ad2']="Il massimimo di giorni che puo avere un dato(file) backup (per la cancellazione in automatico)
0 = disattivato";
$lang['help_ad3']="Il numero massimo di dati che possono stare nell`elenco dei dati(file)backup (per la cancellazione in automatico)
0 = disattivato";
$lang['help_lang']="Seleziona lingua desiderata";
$lang['help_empty_db_before_restore']="Per eliminare dati inutili puoi impostare lo svuotamento totale della banca dati prima del ripristino .";
$lang['help_cronmail']="Decide, se nel Cronjob il backup deve essere spedito tramite email";
$lang['help_cronmailprg']="Il percorso verso il programma email, default è manda_email nel percorso specificato.";
$lang['help_cronftp']="Decide, se nel Cronjob il backup deve essere spedito tramite FTP";
$lang['help_cronzip']="Compressione con GZip - racomandato è 'attivato' (la compressione-Lib deve essere installata!)";
$lang['help_cronextender']="L`estenzione del perlscripts, standard è '.pl'";
$lang['help_cronsavepath']="Il nome del dato di configurazine per il perlscript";
$lang['help_cronprintout']="Quanda l`uscita del testo viene disattivato non viene piu distribuito il testo. Questa funzione è indipendente dalla distribuzione del log.";
$lang['help_cronsamedb']="Vuoi usare la stessa banca dati per il conjob come specificato nella tua configurazione?";
$lang['help_crondbindex']="scegli la banca dati per il cronjob";
$lang['help_cronmail_dump']="Decide, se l`email del cronjob contiene il backup in allegato. ";
$lang['help_ftptransfer']="Se attivato, il backup dato(file) dopo sarà spedito tramite FTP.";
$lang['help_ftpserver']="Indirizzo del FTP-Server";
$lang['help_ftpport']="Port del FTP-Server, standard: 21";
$lang['help_ftpuser']="Inserisci nome utente per FTP";
$lang['help_ftppass']="Inserisci parola d`ordine (password) per FTP";
$lang['help_ftpdir']="Dove devo spedire l`archivio dati? (file)!";
$lang['help_speed']="Minimo è massimo della connessione, standard è da 50 a 5000 (Velocità troppo alte possono causare un timeout!)";
$lang['speed']="Controllo di velocità";
$lang['help_cronexecpath']="Il posto in cui si trovano i perlscripts.\nPunto di partenza iniziale è l`indirizzo HTTP (allora nel Browser)\nSono permessi dei assoluti e relativi cammini.";
$lang['cron_execpath']="Percorso dei Perlscripts";
$lang['help_croncompletelog']="Se questa funzione è attivata, allora viene scritta la completa uscita in complete_log. 
Questa funzione non dipende dalla uscita del testo.";
$lang['help_ftp_mode']="Se si averano dei problemi con il trasferimento tramite FTP, provate di usare il modo passivo.";


?>