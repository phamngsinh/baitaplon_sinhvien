<?php

/** *****************************************
 * 
 * NSK-Backuper h&#7895; tr&#7907; chuy&#7875;n d&#7919; li&#7879;u tr&#7921;c ti&#7871;p gi&#7919;a 2 m&#225;y ch&#7911;, gi&#250;p sao l&#432;u d&#7919; li&#7879;u d&#7877; d&#224;ng v&#224; nhanh ch&#243;ng h&#417;n.
 *
 * Ch&#432;&#417;ng tr&#236;nh n&#224;y l&#224; ph&#7847;n m&#7873;m t&#7921; do, b&#7841;n c&#243; th&#7875; cung c&#7845;p l&#7841;i v&#224;/ho&#7863;c ch&#7881;nh s&#7917;a n&#243; theo nh&#7919;ng &#273;i&#7873;u kho&#7843;n c&#7911;a Gi&#7845;y ph&#233;p C&#244;ng c&#7897;ng c&#7911;a GNU do T&#7893; ch&#7913;c Ph&#7847;n m&#7873;m T&#7921; do c&#244;ng b&#7889;; phi&#234;n b&#7843;n 2 c&#7911;a Gi&#7845;y ph&#233;p, ho&#7863;c b&#7845;t k&#7923; m&#7897;t phi&#234;n b&#7843;n sau &#273;&#243; (tu&#7923; s&#7921; l&#7921;a ch&#7885;n c&#7911;a b&#7841;n).
 * Ch&#432;&#417;ng tr&#236;nh n&#224;y &#273;&#432;&#7907;c cung c&#7845;p v&#7899;i hy v&#7885;ng n&#243; s&#7869; h&#7919;u &#237;ch, tuy nhi&#234;n KH&#212;NG C&#211; B&#7844;T K&#7922; M&#7896;T B&#7842;O H&#192;NH N&#192;O; th&#7853;m ch&#237; k&#7875; c&#7843; b&#7843;o h&#224;nh v&#7873; KH&#7842; N&#258;NG TH&#431;&#416;NG M&#7840;I ho&#7863;c T&#205;NH TH&#205;CH H&#7906;P CHO M&#7896;T M&#7908;C &#272;&#205;CH C&#7908; TH&#7874;. Xin xem Gi&#7845;y ph&#233;p C&#244;ng c&#7897;ng c&#7911;a GNU &#273;&#7875; bi&#7871;t th&#234;m chi ti&#7871;t.
 * B&#7841;n ph&#7843;i nh&#7853;n &#273;&#432;&#7907;c m&#7897;t b&#7843;n sao c&#7911;a Gi&#7845;y ph&#233;p C&#244;ng c&#7897;ng c&#7911;a GNU k&#232;m theo ch&#432;&#417;ng tr&#236;nh n&#224;y; n&#7871;u b&#7841;n ch&#432;a nh&#7853;n, xin g&#7917;i th&#432; v&#7873; T&#7893; ch&#7913;c Ph&#7847;n m&#7873;m T&#7921; do, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 *   
 * NSK-Backuper that help evrery one to backup and restore their data fast and easy via ftp account.
 *  
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 *
 * HTTP://nhatsongkiem.com
 *
 * @author Original nhatsongkiem <admin@nhatsongkiem.com>
 * @copyright 2008
 ********************************************
*/

define('PHIENBAN',1.0);

session_start();
header('Content-type:text/html; charset=UTF-8');
set_time_limit(0);

//Ki&#7875;m tra phi&#234;n b&#7843;n m&#7899;i
$KetNoi=@file_get_contents('http://nhatsongkiem.com/backuper/backuper.ver')."\n";
$PhienBan=substr($KetNoi,0,strpos($KetNoi,"\n"));
$CapNhat=substr($KetNoi,strpos($KetNoi,"\n"));
if ($PhienBan>PHIENBAN)
{
	echo "<i>&#272;&#227; c&#243; phi&#234;n b&#7843;n m&#7899;i $PhienBan thay cho phi&#234;n b&#7843;n hi&#7879;n t&#7841;i ".PHIENBAN."<br />
		B&#7841;n c&#243; th&#7875; c&#7853;p nh&#7853;t t&#7841;i &#273;&#7883;a ch&#7881; <a href=\"http://nhatsongkiem.com/backuper/\">http://nhatsongkiem.com/backuper</a><br /><br />
		Thay &#273;&#7893;i m&#7899;i:<br /></i>
		<div style=\"width:80%;border:inset;background:#DFDFDF\">$CapNhat</div>";
	exit;
}

$ChuyenDi=new ChuyenDi;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<style>
	html
	{
		scrollbar-track-color: #EAEAEA;
		scrollbar-face-color: #FFFFFF;
		scrollbar-highlight-color: #F0F0F0;
		scrollbar-darkshadow-Color: #F0F0F0;
		scrollbar-3dlight-color:#F0F0F0;
	}
	body
	{
		font-family: Verdana, Tahoma, sans-serif;
		font-size: 12px;
	}
	a
	{
		text-decoration: none;
	}
	p
	{
		margin: 2px;
	}
	td
	{
		background: #F1F1F1;
		border: 1px outset;
	}
	td.On
	{
		border: 1px inset;
	}
	input
	{
		border: 1px dashed #7F9DB9;
	}
	.OK
	{
		border: 1px outset;
		background: #F1F1F1;
	}
</style>
<title>Nhatsongkiem Backuper</title>
<body>
<?

switch($_GET['Mo']) //M&#7903; m&#7897;t th&#432; m&#7909;c n&#224;o &#273;&#243;
{
	case 'ThuMuc': //Chuy&#7875;n get th&#224;nh POST &#273;&#7875; POST x&#7917; l&#253;
		$_POST['Mo']='ThuMuc';
		$_POST['ThuMuc']=$_GET['ThuMuc'];
		break;
}

switch($_POST['Mo']) //Nh&#7853;n d&#7841;ng y&#234;u c&#7847;u
{
	default: //M&#7903; trang ch&#7911;
		$_POST['ThuMuc']='.';
		
	case 'ThuMuc': //V&#224;o th&#432; m&#7909;c
		//L&#7845;y danh s&#225;ch
		$DanhSach=$ChuyenDi->DanhSach($_POST['ThuMuc']);
		?>
	<form method="post" action="<?=basename(__FILE__)?>" onsubmit="var KetQua='';var Tam=document.getElementsByName('LuaChon');for(i=0;i<Tam.length;i++){if(Tam[i].checked){KetQua+=Tam[i].value+';'}}this.TapTin.value=KetQua;if(!confirm('X&#225;c nh&#7853;n chuy&#7875;n')){return false}" target="_blank">
		Chuy&#7875;n &#273;&#7871;n: <input type="text" name="DenThuMuc" size="10" value="./" />
		<input type="hidden" name="Mo" value="ChuyenDi" />
		<input type="hidden" name="TapTin" />
		<input type="hidden" name="TuThuMuc" value="<?=$_POST['ThuMuc']?>" />
		M&#225;y ch&#7911;: <input type="text" name="DiaChi" size="10" value="<?=$_SESSION['DiaChi']?>" />
		T&#234;n &#273;&#259;n nh&#7853;p: <input type="text" name="TenDangNhap" size="15" value="<?=$_SESSION['TenDangNhap']?>" />
		M&#7853;t kh&#7849;u: <input type="password" name="MatKhau" size="10" value="<?=$_SESSION['MatKhau']?>" />
		<input class="OK" type="submit" value="Th&#7921;c thi" />
	</form>
	<div style="width:100%;height:100%;height:expression(document.documentElement.clientHeight-100);overflow:auto">
		<table width="96%">
		<?
		//Li&#7879;t k&#234; nh&#432;ng th&#432; m&#7909;c &#273;&#7847;u ti&#234;n
		foreach($DanhSach as $GiaTri)
		{
			if (is_dir($_POST['ThuMuc'].'/'.$GiaTri))
			{
				?>
			<tr onmouseover="var Tam=this.childNodes;for(i=0;i<Tam.length;i++)Tam[i].className='On'" onmouseout="var Tam=this.childNodes;for(i=0;i<Tam.length;i++)Tam[i].className=''">
				<td width="20px"><input type="checkbox" name="LuaChon" value="<?=$GiaTri?>" /></td>
				<td width="40%"><a style="color:#B1942E" href="?Mo=ThuMuc&ThuMuc=<?=$_POST['ThuMuc'].'/'.$GiaTri?>">&#9658;<?=$GiaTri?></a></td>
				<td width="10%" align="center" style="color:#B1942E">Th&#432; m&#7909;c</td>
				<td align="right" width="10%">0</td>
				<td width="10%">KB</td>
				<td width="10%" align="center"><?=substr(sprintf('%o', fileperms($_POST['ThuMuc'].'/'.$GiaTri)), -4)?></td>
				<td width="20%"><?=date('D d Y - H:i:s',filemtime($_POST['ThuMuc'].'/'.$GiaTri))?></td>
			</tr>
				<?
			}
		}
		//Li&#7879;t k&#234; nh&#7919;ng t&#7853;p tin kh&#225;c
		foreach($DanhSach as $GiaTri)
		{
			if (!is_dir($_POST['ThuMuc'].'/'.$GiaTri))
			{
				?>
			<tr onmouseover="var Tam=this.childNodes;for(i=0;i<Tam.length;i++)Tam[i].className='On'" onmouseout="var Tam=this.childNodes;for(i=0;i<Tam.length;i++)Tam[i].className=''">
				<td width="20px"><input type="checkbox" name="LuaChon" value="<?=$GiaTri?>"></td>
				<td width="40%"><a style="color:#B1942E" target="_blank" href="<?=$_POST['ThuMuc'].'/'.$GiaTri?>"><?=$GiaTri?></a></td>
				<td width="10%" align="center" style="color:#B1942E">T&#7853;p tin</td>
				<td align="right" width="10%"><?=round(filesize($_POST['ThuMuc'].'/'.$GiaTri)/1024,2)?></td>
				<td width="10%">KB</td>
				<td width="10%" align="center"><?=substr(sprintf('%o', fileperms($_POST['ThuMuc'].'/'.$GiaTri)), -4)?></td>
				<td width="20%"><?=date('D d Y - H:i:s',filemtime($_POST['ThuMuc'].'/'.$GiaTri))?></td>
			</tr>
				<?
			}
		}
		?>
		</table>
	</div>
	<center>
		<i>Copyright 2008 <a href="http://nhatsongkiem.com/backuper">A5 IT Class</a> - GNU license</i><br />
		T&#7843;i ChuyenTinBackuper m&#7899;i nh&#7845;t t&#7841;i <a href="http://nhatsongkiem.com/backuper">http://nhatsongkiem.com/backuper</a>
	</center>
</body>
</html>
		<?
		break;
	
	case 'ChuyenDi': //Chuy&#7875;n t&#7853;p tin ho&#7863;c th&#432; m&#7909;c sang &#273;&#7883;a ch&#7881; kh&#225;c
		$_SESSION['DiaChi']=$_POST['DiaChi'];
		$_SESSION['TenDangNhap']=$_POST['TenDangNhap'];
		$_SESSION['MatKhau']=$_POST['MatKhau'];
		
		$ChuyenDi->Chuyen($_POST['DiaChi'],$_POST['TenDangNhap'],$_POST['MatKhau'],$_POST['TapTin'],$_POST['TuThuMuc'],$_POST['DenThuMuc']);
		break;
}

/** Ph&#7847;n th&#7921;c thi ch&#237;nh */

class ChuyenDi
{
	function DanhSach($ThuMuc) //L&#7845;y danh s&#225;ch ph&#7847;n t&#7917; c&#7911;a m&#7897;t th&#432; m&#7909;c tr&#7843; v&#7873; dang m&#7843;ng
	{
		$ThuMuc=opendir($ThuMuc);
		while($ThanhPhan=readdir($ThuMuc))
		{
			$KetQua[]=$ThanhPhan;
		}
		sort($KetQua);
		return $KetQua;
	}
	
	//Chuy&#7875;n t&#7899;i &#273;&#7883;a ch&#7881; m&#7899;i v&#224;o th&#432; m&#7909;c $DenThuMuc
	function Chuyen($DiaChi,$TenDangNhap,$MatKhau,$TapTin,$TuThuMuc,$DenThuMuc)
	{
		//K&#7871;t n&#7889;i v&#224; ki&#7875;m tra &#273;&#259;ng nh&#7853;p
		$this->KetNoi=@ftp_connect($DiaChi);
		if (!@ftp_login($this->KetNoi,$TenDangNhap,$MatKhau))
		{
			echo 'Kh&#244;ng &#273;&#259;ng nh&#7853;p &#273;&#432;&#7907;c v&#224;o t&#224;i kho&#7843;n FTP!';
			return false;
		}
		//Duy&#7879;t qua c&#225;c th&#432; m&#7909;c t&#7853;p tin
		$TapTin=explode(';',$TapTin);
		array_pop($TapTin);
		foreach($TapTin as $GiaTri)
		{
			//N&#7871;u l&#224; th&#432; m&#7909;c th&#236; chuy&#7875;n h&#7871;t to&#224;n b&#7897; nh&#7919;ng g&#236; trong th&#432; m&#7909;c &#273;&#243;
			if (is_dir($TuThuMuc.'/'.stripslashes($GiaTri)))
			{
				echo '<br /><br />T&#7841;o th&#432; m&#7909;c <b style="color:green">',$GiaTri,'</b>';
				$this->_Chuyen($TuThuMuc.'/'.$GiaTri,$DenThuMuc.'/'.$GiaTri);
			}
			//N&#7871;u l&#224; t&#7853;p tin th&#236; chuy&#7875;n th&#244;ng th&#432;&#7901;ng
			else
			{
				echo '<br>&#272;ang chuy&#7875;n t&#7853;p tin <b>',stripslashes($GiaTri).'</b> - ';
				ob_flush();
				flush();
				
				if (@ftp_fput($this->KetNoi,$DenThuMuc.'/'.stripslashes($GiaTri),fopen($TuThuMuc.'/'.stripslashes($GiaTri),'r'),FTP_BINARY))
				{
					echo '<b style="color:orange">&#272;&#227; chuy&#7875;n</b> ',stripslashes($GiaTri),' - ',filesize($TuThuMuc.'/'.stripslashes($GiaTri))==0 ? 100 : ftp_size($this->KetNoi,$DenThuMuc.'/'.stripslashes($GiaTri))/filesize($TuThuMuc.'/'.stripslashes($GiaTri))*100,'%';
				}
				else
				{
					echo '<b style="color:red">Kh&#244;ng th&#7875; chuy&#7875;n</b>';
				}
			}
			echo '<script>document.body.scrollTop=document.body.scrollHeight</script>';
			ob_flush();
			flush();
		}
	}
	
	function _Chuyen($ThuMuc,$ChuyenDen) //Chuy&#7875;n &#273;&#7879; quy th&#432; m&#7909;c
	{
		//T&#7841;o th&#432; m&#7909;c tr&#234;n m&#225;y ch&#7911; m&#7899;i
		@ftp_mkdir($this->KetNoi,$ChuyenDen);
		$DanhSach=$this->DanhSach($ThuMuc);
		//Duy&#7879;t qua th&#432; m&#7909;c $ThuMuc
		foreach($DanhSach as $GiaTri)
		{
			//N&#7871;u g&#7863;p th&#432; m&#7909;c th&#236; chuy&#7875;n ti&#7871;p
			if (is_dir($ThuMuc.'/'.$GiaTri) && $GiaTri!='..' && $GiaTri!='.')
			{
				echo '<br /><br />T&#7841;o th&#432; m&#7909;c <b style="color:green">',$GiaTri,'</b>';
				$this->_Chuyen($ThuMuc.'/'.$GiaTri,$ChuyenDen.'/'.$GiaTri);
			}
			//Chuy&#7875;n t&#7853;p tin th&#244;ng th&#432;&#7901;ng
			elseif ($GiaTri!='..' && $GiaTri!='.')
			{
				echo '<br>&#272;ang chuy&#7875;n t&#7853;p tin <b>',stripslashes($GiaTri).'</b> - ';
				ob_flush();
				flush();
				
				if (@ftp_fput($this->KetNoi,$ChuyenDen.'/'.stripslashes($GiaTri),fopen($ThuMuc.'/'.stripslashes($GiaTri),'r'),FTP_BINARY))
				{
					echo '<b style="color:orange">&#272;&#227; chuy&#7875;n</b> ',stripslashes($GiaTri),' - ',filesize($ThuMuc.'/'.stripslashes($GiaTri))>0 ? ftp_size($this->KetNoi,$ChuyenDen.'/'.stripslashes($GiaTri))/filesize($ThuMuc.'/'.stripslashes($GiaTri))*100 : 100,'%';
				}
				else
				{
					echo '<b style="color:red">Kh&#244;ng th&#7875; chuy&#7875;n!</b>';
				}
			}
			echo '<script>document.body.scrollTop=document.body.scrollHeight</script>';
			ob_flush();
			flush();
		}
	}
}

?>