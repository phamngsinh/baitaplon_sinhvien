<?php
	include 'php/classes/pagerank.class.php';
	$options = array(
		'pagerank' => true,
		'alexaRank' => true,
		'dmoz' => true,
		'backlinksGoogle' => true,
		'backlinksYahoo' => true,
		'backlinksMSN' => true,
		'resultsAltaVista' => true,
		'resultsAllTheWeb' => true,
		'thumbnail' => true
	);
	new pagerank(urldecode($_GET['url']), $options);
?>