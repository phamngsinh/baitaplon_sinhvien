function check_date(){	
	if(document.frmMain.tungay.value == ""){
		alert('Hãy chọn ngày mở thưởng!');
		document.frmMain.tungay.focus();
		return false;
	}
	return true;
}
var httpObject;

function unix_timestamp() {
	return parseInt(new Date().getTime().toString().substring(0, 10))
}

function ghiKetqua() {
	
	httpObject = GetXmlHttpObject();

	if (httpObject == null) {
	  	alert ("Browser does not support HTTP Request");
	  	return;
	}
	url1 = "ghiketqua.php";
	httpObject.open("GET", url1, true);
	httpObject.send(null);
}

function updateKetqua(type, ngay) {
	
	httpObject = GetXmlHttpObject();

	if (httpObject == null) {
	  	alert ("Browser does not support HTTP Request");
	  	return;
	}
	var randomnumber = unix_timestamp();
	url = "kq" + type + ".htm" + "?t=" + randomnumber;

	httpObject.onreadystatechange = setOutput;
	httpObject.open("GET", url, true);
	httpObject.send(null);

}

function updateKetquaMT(type, ngay) {
	
	httpObject1 = GetXmlHttpObject();

	if (httpObject1 == null) {
	  	alert ("Browser does not support HTTP Request");
	  	return;
	}
	var randomnumber = unix_timestamp();

	url2 = "kq" + type + ".php" + "?t=" + randomnumber;
	
	httpObject1.onreadystatechange = setOutputMT;
	httpObject1.open("GET", url2, true);
	httpObject1.send(null);

}

function updateKetquaMN(type, ngay) {
	
	httpObject2 = GetXmlHttpObject();

	if (httpObject2 == null) {
	  	alert ("Browser does not support HTTP Request");
	  	return;
	}
	var randomnumber = unix_timestamp();

	url3 = "kq" + type + ".php" + "?t=" + randomnumber;
	
	httpObject2.onreadystatechange = setOutputMN;
	httpObject2.open("GET", url3, true);
	httpObject2.send(null);

}

function GetXmlHttpObject() {
	
	if (window.XMLHttpRequest) {
	  	// code for IE7+, Firefox, Chrome, Opera, Safari
	  	return new XMLHttpRequest();
	}

	if (window.ActiveXObject) {
	  // code for IE6, IE5
	  	return new ActiveXObject("Microsoft.XMLHTTP");
	}

	return null;
}

function setOutput() {
	if(httpObject.readyState == 4) {
		if (httpObject.responseText != '') {
			document.getElementById('ketqua').innerHTML = httpObject.responseText;
		}
	}
}

function setOutputMT() {
	if(httpObject1.readyState == 4) {
		if (httpObject1.responseText != '') {
			document.getElementById('ketquamt').innerHTML = httpObject1.responseText;
		}
	}
}

function setOutputMN() {
	if(httpObject2.readyState == 4) {
		if (httpObject2.responseText != '') {
			document.getElementById('ketquamn').innerHTML = httpObject2.responseText;
		}
	}
}