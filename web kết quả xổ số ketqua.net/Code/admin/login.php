<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Login</title>
<link href="style.css"  rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body id="login">
<div id="login-wrapper">
	<div id="login-top">
		<div id="logo">	
			<div id="logoText"> Soi Cầu Admin Login </div>
		</div>
	</div>
	<div id="login-content">
		<form name="frmLogin" method="post" action="check_login.php" id="form-login" style="clear: both;">
			<p id="form-login-username">
				<label for="modlgn_username">Username</label>
				<input name="username" id="modlgn_username" type="text" class="inputbox" size="20">
			</p>
			<p id="form-login-password">
				<label for="modlgn_passwd">Password</label>
				<input name="password" id="modlgn_passwd" type="password" class="inputbox" size="20">
			</p>			
			<div class="button_holder">
				<div class="button1">
					<div class="next">
						<input class="button" type="submit" name="login" value="Login" style="width:80px; float:left; margin-right:20px;" />
						<input class="button" type="reset" name="reset" value="Reset" style="width:80px; float:left;" />
					</div>
				</div>
			</div>
			<div class="clr"></div>
		</form>


		<div class="notification information"><div>Sử dụng đúng tên và password để đăng nhập vào Admin của Soicau.co</div></div>
		<p class="home-page">
		<a href="http://soicau.co/">Quay Lại Trang Chủ</a>
		</p>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript">
document.frmLogin.modlgn_username.focus();
</script>
</body>
</html>