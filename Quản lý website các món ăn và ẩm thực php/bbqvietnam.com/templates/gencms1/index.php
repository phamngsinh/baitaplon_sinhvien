<?php
/**
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<jdoc:include type="head" />

<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/gencms/css/template.css" type="text/css" />
<!--[if lte IE 6]>
<link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/ieonly.css" rel="stylesheet" type="text/css" />
<![endif]-->
<?php if($this->direction == 'rtl') : ?>
	<link href="<?php echo $this->baseurl ?>/templates/gencms/css/template_rtl.css" rel="stylesheet" type="text/css" />
<?php endif; ?>
<script language="javascript" type="text/javascript"src="<?php echo $this->baseurl ?>/templates/gencms/js/swfobject.js"></script>


</head>
<body id="page_bg">
	<div id="wapper">
        <div id="me_lo">
            <div id="logo" align="center">
         	   <a href="/index.php"><img src="<?php echo $this->baseurl ?>/templates/gencms/images/logo.png" /></a>
            </div>
            <div id="menu_top">
            	<div id="user1">
            	<jdoc:include type="modules" name="user1" />   
                <div class="clr"></div>         
                </div>
             	<div id="user2">
            	<jdoc:include type="modules" name="user2" />            
                </div>
            </div>
            <div class="clr"></div>
        </div>
        <?php if( JRequest::getVar( 'view' ) == 'frontpage' ) { ?>

            <div id="frontpage">
            	<div id="flash">
                	<jdoc:include type="modules" name="user3" /> 
                </div>
                <div id="right_font">
                	<div id="user4">
	                	<jdoc:include type="modules" name="user4" style="xhtml" />                     
                    </div>
                    <div id="user5">
                    	<div id="user5_left">
	                	<jdoc:include type="modules" name="user5" style="xhtml" />                     
                        </div>
						<div id="user5_right">
	                	<jdoc:include type="modules" name="user55" style="xhtml" />                     
                        </div>
                        <div class="clr"></div>
                    </div>
                    <div id="user6">
	                	<jdoc:include type="modules" name="user6" style="xhtml" />                     
                    </div>
                </div>
                <div class="clr"></div>
            </div>
            <div class="clr"></div>
		<?php } else { ?>
        	<div id="left_bbq">
	               <div id="left_bbq_1">
                        <jdoc:include type="modules" name="left" />   
                   </div>
                   <div>     
                        <jdoc:include type="modules" name="user9"/>                              
                   </div>     
            </div>
            <div id="right_bbq">
            <div id="body_bbq">
					<jdoc:include type="modules" name="breadcrumb" />
                	<jdoc:include type="modules" name="user7" style="xhtml" />                                 
	       		     <jdoc:include type="component" />
                   	<jdoc:include type="modules" name="user8" style="xhtml"/>                                 
            </div>        
            </div>
              <div class="clr"></div>
              
        <?php } ?>
   		<div id="footer">
        	<jdoc:include type="modules" name="footer" /> 
        </div>
        
   </div>
   <jdoc:include type="modules" name="debug" /> 
</body>
</html>