<?php 
defined('_JEXEC') or die('Can not access this');
$flashfile=$params->get('flashfile');
$width=$params->get('width');
$height=$params->get('height');
?>
<div id="my-flash<?php echo $module->id?>">
	<script type="text/javascript">
			var flashvars = {};
			var params = {};
			params.play = "true";
			params.loop = "true";
			params.menu = "false";
			params.quality = "high";
			params.wmode = "transparent";
			params.allowscriptaccess = "no";
			var attributes = {};
			attributes.name = "myindex";
			attributes.align = "right";
			swfobject.embedSWF("<?php echo JURI::base().$flashfile?>", "my-flash<?php echo $module->id?>", "<?php echo $width?>", "<?php echo $height?>", "9.0.0", false, flashvars, params, attributes);
	</script>
	<a href="http://www.adobe.com/go/getflashplayer">
						<img src="images/get_flash_player.gif" alt="Get Adobe Flash player" />
	</a>
</div>
