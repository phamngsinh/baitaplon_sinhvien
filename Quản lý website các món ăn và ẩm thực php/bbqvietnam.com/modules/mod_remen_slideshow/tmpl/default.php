<?php defined('_JEXEC') or die('Restricted access'); // no direct access ?>
<script type="text/javascript" language="javascript">
jQuery(document).ready(function() {
	jQuery(".carousel .jCarouselLite").jCarouselLite({
	    btnNext: ".carousel .next",
	    btnPrev: ".carousel .prev",
	    auto: 5000,
	    speed: 1000
	});
});
</script>

<div id="jCarouselLiteWrap">
<div class="carousel clearfix">
    <div class="prev"></div>
    <div class="jCarouselLite">
        <ul>
            <?php 
            foreach($slideShow as $item){?>
            	<li><?php if($item->content_image){?><div class="slide-prj-image"><?php echo '<a href="'.JRoute::_(ContentHelperRoute::getArticleRoute($item->slug)).'">'.$item->content_image.'</a>'?></div><?php }?><div class="slide-prj-title"><?php echo '<a href="'.JRoute::_(ContentHelperRoute::getArticleRoute($item->slug)).'">'.$item->title.'</a>';?></div></li>
            <?php }?>
        </ul>
    </div>
    <div class="next"></div>
    <div class="clear"></div>   
</div>
</div>

