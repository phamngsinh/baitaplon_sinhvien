<?php
/**
 * @version		$Id: example.php 10714 2008-08-21 10:10:14Z eddieajau $
 * @package		Joomla
 * @subpackage	Content
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

/**
 * Example Content Plugin
 *
 * @package		Joomla
 * @subpackage	Content
 * @since 		1.5
 */
class plgContentThuthuy extends JPlugin
{

	/**
	 * Constructor
	 *
	 * For php4 compatability we must not use the __constructor as a constructor for plugins
	 * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
	 * This causes problems with cross-referencing necessary for the observer design pattern.
	 *
	 * @param object $subject The object to observe
	 * @param object $params  The object that holds the plugin parameters
	 * @since 1.5
	 */
	function plgContentThuthuy( &$subject, $params )
	{
		parent::__construct( $subject, $params );
	}

	/**
	 * Example prepare content method
	 *
	 * Method is called by the view
	 *
	 * @param 	object		The article object.  Note $article->text is also available
	 * @param 	object		The article params
	 * @param 	int			The 'page' number
	 */
	function onPrepareContent( &$article, &$params, $limitstart )
	{
		global $mainframe;

	}

	/**
	 * Example after display title method
	 *
	 * Method is called by the view and the results are imploded and displayed in a placeholder
	 *
	 * @param 	object		The article object.  Note $article->text is also available
	 * @param 	object		The article params
	 * @param 	int			The 'page' number
	 * @return	string
	 */
	function onAfterDisplayTitle( &$article, &$params, $limitstart )
	{
		global $mainframe;

		return '';
	}

	/**
	 * Example before display content method
	 *
	 * Method is called by the view and the results are imploded and displayed in a placeholder
	 *
	 * @param 	object		The article object.  Note $article->text is also available
	 * @param 	object		The article params
	 * @param 	int			The 'page' number
	 * @return	string
	 */
	function onBeforeDisplayContent( &$article, &$params, $limitstart )
	{
		global $mainframe;
		if($params->get('category_allow_switch_image') == 'Yes'){
			$image = ThuthuyHelper::replaceImage($article);
			return $image;
		}
		else{
			return '';
		}
	}

	/**
	 * Example after display content method
	 *
	 * Method is called by the view and the results are imploded and displayed in a placeholder
	 *
	 * @param 	object		The article object.  Note $article->text is also available
	 * @param 	object		The article params
	 * @param 	int			The 'page' number
	 * @return	string
	 */
	function onAfterDisplayContent( &$article, &$params, $limitstart )
	{
		global $mainframe;

		return '';
	}

	/**
	 * Example before save content method
	 *
	 * Method is called right before content is saved into the database.
	 * Article object is passed by reference, so any changes will be saved!
	 * NOTE:  Returning false will abort the save with an error.
	 * 	You can set the error by calling $article->setError($message)
	 *
	 * @param 	object		A JTableContent object
	 * @param 	bool		If the content is just about to be created
	 * @return	bool		If false, abort the save
	 */
	function onBeforeContentSave( &$article, $isNew )
	{
		global $mainframe;

		return true;
	}

	/**
	 * Example after save content method
	 * Article is passed by reference, but after the save, so no changes will be saved.
	 * Method is called right after the content is saved
	 *
	 *
	 * @param 	object		A JTableContent object
	 * @param 	bool		If the content is just about to be created
	 * @return	void
	 */
	function onAfterContentSave( &$article, $isNew )
	{
		global $mainframe;

		return true;
	}

}
class ThuthuyHelper{
	function replaceImage( &$row) {
		$regex = '#<\s*img [^\>]*src\s*=\s*(["\'])(.*?)\1#im';
		
		preg_match ($regex, $row->introtext, $matches);
		//if(!count($matches)) preg_match ($regex, $row->fulltext, $matches);
		$images = (count($matches)) ? $matches : array();
		$image = '';
		if (count($images)) $image = trim($images[2]);
		$width = 100;
		$height = 100;
		if ($image) {
				$image1 = ThuthuyHelper::processImage ( $image, $width, $height );
				$image = "<img src=\"".$image1."\" alt=\"{$row->title}\" />";
		} else $image = '';

		$regex1 = "/\<img[^\>]*>/";
		$row->text = preg_replace( $regex1, '', $row->introtext,1 );		
		$row->text = trim($row->text);
		// clean up globals
		return $image;
	}
	
	function processImage ( $img, $width, $height ) {
			if(!$img) return '';
			$img = str_replace(JURI::base(),'',$img);
			$img = rawurldecode($img);
			$imagesurl = (file_exists(JPATH_SITE .'/'.$img)) ? ThuthuyHelper::jaResize($img,$width,$height) :  '' ;
			return $imagesurl;
		}


		
		function jaResize($image,$max_width,$max_height){
			$path =JPATH_SITE; 
			$imgInfo = getimagesize($path.'/'.$image);
			$width = $imgInfo[0];
			$height = $imgInfo[1];
			if(!$max_width && !$max_height) {
		        $max_width = $width;
		        $max_height = $height;
		    }else{
		        if(!$max_width) $max_width = 1000;
		        if(!$max_height) $max_height = 1000;
		    }
			$x_ratio = $max_width / $width;
			$y_ratio = $max_height / $height;
			if (($width <= $max_width) && ($height <= $max_height) ) {
				$tn_width = $width;
				$tn_height = $height;
			} else if (($x_ratio * $height) < $max_height) {
				$tn_height = ceil($x_ratio * $height);
				$tn_width = $max_width;
			} else {
				$tn_width = ceil($y_ratio * $width);
				$tn_height = $max_height;
			}

			$ext = strtolower(substr(strrchr($image, '.'), 1)); // get the file extension
			$rzname = strtolower(substr($image, 0, strpos($image,'.')))."_{$tn_width}_{$tn_height}.{$ext}"; // get the file extension
			$resized = $path.'/images/resized/'.$rzname; 
			if(file_exists($resized)){
				$smallImg = getimagesize($resized);
				if (($smallImg[0] <= $tn_width && $smallImg[1] == $tn_height) ||
					($smallImg[1] <= $tn_height && $smallImg[0] == $tn_width)) {
						return "images/resized/".$rzname;
				}
			}
			if(!file_exists($path.'/images/resized/') && !mkdir($path.'/images/resized/',0755)) return '';
			$folders = explode('/',strtolower($image));
			$tmppath = $path.'/images/resized/';
			for($i=0;$i < count($folders)-1; $i++){
				if(!file_exists($tmppath.$folders[$i]) && !mkdir($tmppath.$folders[$i],0755)) return '';
				$tmppath = $tmppath.$folders[$i].'/';
			}	

					
		 switch ($imgInfo[2]) {
		  case 1: $im = imagecreatefromgif($path.'/'.$image); break;
		  case 2: $im = imagecreatefromjpeg($path.'/'.$image);  break;
		  case 3: $im = imagecreatefrompng($path.'/'.$image); break;
		  default: return '';  break;
		 }
					
		 $newImg = imagecreatetruecolor($tn_width, $tn_height);
		 
		 /* Check if this image is PNG or GIF, then set if Transparent*/  
		 if(($imgInfo[2] == 1) OR ($imgInfo[2]==3)){
		  imagealphablending($newImg, false);
		  imagesavealpha($newImg,true);
		  $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
		  imagefilledrectangle($newImg, 0, 0, $tn_width, $tn_height, $transparent);
		 }
		 imagecopyresampled($newImg, $im, 0, 0, 0, 0, $tn_width, $tn_height, $imgInfo[0], $imgInfo[1]);

			//Generate the file, and rename it to $newfilename
			jimport('joomla.filesystem.file');
		 switch ($imgInfo[2]) {
		  case 1: imagegif($newImg,$resized); break;
		  case 2: imagejpeg($newImg,$resized, 90);  break;
		  case 3: imagepng($newImg,$resized); break;
		  default: return '';  break;
		 }
		 return "images/resized/".$rzname;

		}
}
