# Nguyen Van Binh CMS MySQL-Dump
# Generation Time: Nov 16, 2007 at 10:05
# Server version: 5.0.45-community-nt-log
# PHP Version: 5.2.3
# Database : `kylin`
# --------------------------------------------------------
#
# Table structure for table `carphoto`
#
DROP table IF EXISTS carphoto;
CREATE TABLE `carphoto` (
  `id_carphoto` bigint(20) unsigned NOT NULL auto_increment,
  `id_product` bigint(20) NOT NULL default '0',
  `name` varchar(255) NOT NULL,
  `small_image` varchar(255) NOT NULL,
  `normal_image` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id_carphoto`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
#
# Dumping data for table `carphoto`
#
INSERT INTO carphoto VALUES (2,3,'a','thumb_carphoto_1195114449.jpeg','normal_carphoto_1195114449.jpeg','carphoto_1195114449.jpg',0,1);
INSERT INTO carphoto VALUES (3,3,'abv','thumb_carphoto_1195114460.jpeg','normal_carphoto_1195114460.jpeg','carphoto_1195114460.jpg',0,1);
INSERT INTO carphoto VALUES (4,3,'4','thumb_carphoto_1195114641.jpeg','normal_carphoto_1195114641.jpeg','carphoto_1195114641.jpg',0,1);
INSERT INTO carphoto VALUES (5,2,'d','thumb_carphoto_1195114859.jpeg','normal_carphoto_1195114859.jpeg','carphoto_1195114859.jpg',0,1);
INSERT INTO carphoto VALUES (6,1,'34','thumb_carphoto_1195114972.jpeg','normal_carphoto_1195114972.jpeg','carphoto_1195114972.jpg',0,1);
INSERT INTO carphoto VALUES (7,1,'dsf','thumb_carphoto_1195114979.jpeg','','carphoto_1195114979.jpg',0,1);
INSERT INTO carphoto VALUES (8,1,'d','thumb_carphoto_1195114984.jpeg','normal_carphoto_1195114984.jpeg','carphoto_1195114984.jpg',0,1);
INSERT INTO carphoto VALUES (9,2,'dfsdf','thumb_carphoto_1195118909.jpeg','normal_carphoto_1195118909.jpeg','carphoto_1195118909.jpg',2,1);
INSERT INTO carphoto VALUES (10,2,'dfsdfsdfsd','thumb_carphoto_1195119013.jpeg','normal_carphoto_1195119013.jpeg','carphoto_1195119013.jpg',3,1);
INSERT INTO carphoto VALUES (11,2,'dfsđf fd d','thumb_carphoto_1195119061.jpeg','normal_carphoto_1195119061.jpeg','carphoto_1195119061.jpg',76,1);
INSERT INTO carphoto VALUES (18,6,'2','thumb_carphoto_1195120250.jpeg','normal_carphoto_1195120250.jpeg','carphoto_1195120250.jpg',0,1);
INSERT INTO carphoto VALUES (19,6,'4','thumb_carphoto_1195120255.jpeg','normal_carphoto_1195120255.jpeg','carphoto_1195120255.jpg',0,1);
#
# Table structure for table `cart`
#
DROP table IF EXISTS cart;
CREATE TABLE `cart` (
  `id_cart` bigint(20) unsigned NOT NULL auto_increment,
  `session` tinytext NOT NULL,
  `quantity` int(10) unsigned NOT NULL default '0',
  `id_prod` bigint(20) unsigned NOT NULL default '0',
  `prod_cat` varchar(10) NOT NULL default '',
  `time` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id_cart`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
#
# Dumping data for table `cart`
#
INSERT INTO cart VALUES (1,'9c9801f8ca314a633ed87c2583b4a174',1,5,'product',1193220120);
INSERT INTO cart VALUES (2,'7a450107be6723564b49276d205d643b',100000,5,'product',1193220518);
INSERT INTO cart VALUES (3,'9c9801f8ca314a633ed87c2583b4a174',2,6,'product',1193221158);
INSERT INTO cart VALUES (4,'ca78c20533147d2c7533723021f99607',4,6,'product',1193221322);
INSERT INTO cart VALUES (5,'ca78c20533147d2c7533723021f99607',3,7,'product',1193221541);
INSERT INTO cart VALUES (6,'ca78c20533147d2c7533723021f99607',1,3,'product',1193221656);
INSERT INTO cart VALUES (8,'ca78c20533147d2c7533723021f99607',1,4,'product',1193222017);
INSERT INTO cart VALUES (9,'b4fab4ad2d8f4068b815b074cb247d7d',2,6,'product',1193295151);
INSERT INTO cart VALUES (10,'b4fab4ad2d8f4068b815b074cb247d7d',1,5,'product',1193298203);
INSERT INTO cart VALUES (11,'b4fab4ad2d8f4068b815b074cb247d7d',1,3,'product',1193298514);
INSERT INTO cart VALUES (12,'b4fab4ad2d8f4068b815b074cb247d7d',1,7,'product',1193300701);
INSERT INTO cart VALUES (18,'5ecee0d9632c323eaa4bd69c1a9c76f2',2,4,'product',1193996381);
INSERT INTO cart VALUES (17,'b4bb2bbc95f1920b32248c4db50043d8',1,5,'product',1193643879);
INSERT INTO cart VALUES (19,'666aa922f15ebadf18c91f59cc93fd1f',1,6,'product',1193998197);
INSERT INTO cart VALUES (20,'4b511812c5810b2ec358604a973d0597',1,3,'product',1194061852);
INSERT INTO cart VALUES (21,'b1f4b9ebae9c0926863ee043388a474d',4,1,'product',1195121179);
INSERT INTO cart VALUES (22,'fdbdf1a8f6b73318d5f462275987088e',2,2,'product',1195121235);
INSERT INTO cart VALUES (23,'cdea65f31222f9922ab617f5950c7ed4',1,2,'product',1195175493);
INSERT INTO cart VALUES (24,'176981f6598ef072b54631cae49f1134',1,1,'product',1195175806);
INSERT INTO cart VALUES (29,'f38448a8112977386bbd9f585963fa72',12323,2,'product',1195179898);
#
# Table structure for table `cat`
#
DROP table IF EXISTS cat;
CREATE TABLE `cat` (
  `id_cat` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(50) character set latin1 collate latin1_general_ci NOT NULL default '',
  `parentid` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`id_cat`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
#
# Dumping data for table `cat`
#
INSERT INTO cat VALUES (1,'TIN THỊ TRƯỜNG OTO',0,0,1);
INSERT INTO cat VALUES (3,'TIN TỨC SỰ KIỆN',0,1,1);
#
# Table structure for table `catif`
#
DROP table IF EXISTS catif;
CREATE TABLE `catif` (
  `id_catif` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(50) character set latin1 collate latin1_general_ci NOT NULL default '',
  `parentid` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`id_catif`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
#
# Dumping data for table `catif`
#
INSERT INTO catif VALUES (1,'Gi&#7899;i thi&#7879;u',0,0,1);
INSERT INTO catif VALUES (3,'B&#7843;o h&#224;nh',0,0,1);
INSERT INTO catif VALUES (4,'Liên hệ',0,0,1);
#
# Table structure for table `catlg`
#
DROP table IF EXISTS catlg;
CREATE TABLE `catlg` (
  `id_catlg` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(50) character set latin1 collate latin1_general_ci NOT NULL default '',
  `parentid` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`id_catlg`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
#
# Dumping data for table `catlg`
#
INSERT INTO catlg VALUES (1,'Banner trang ch&#7911;',0,0,1);
INSERT INTO catlg VALUES (2,'Li&#234;n k&#7871;t website',0,0,1);
INSERT INTO catlg VALUES (4,'Logo tr&#432;&#7907;t',0,0,1);
INSERT INTO catlg VALUES (5,'Tr&#225;i',4,0,1);
INSERT INTO catlg VALUES (6,'Ph&#7843;i',4,0,1);
#
# Table structure for table `catpd`
#
DROP table IF EXISTS catpd;
CREATE TABLE `catpd` (
  `id_catpd` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(50) character set latin1 collate latin1_general_ci NOT NULL default '',
  `parentid` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  `content` text character set latin1 collate latin1_general_ci NOT NULL,
  `image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  PRIMARY KEY  (`id_catpd`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
#
# Dumping data for table `catpd`
#
INSERT INTO catpd VALUES (1,'Xe hơi Đức',21,0,1,'','');
INSERT INTO catpd VALUES (2,'Xe hơi Nhật',21,1,1,'','');
INSERT INTO catpd VALUES (3,'Xe hơi Mỹ',21,2,1,'','');
INSERT INTO catpd VALUES (4,'Xe hơi Italia',21,3,1,'','');
INSERT INTO catpd VALUES (6,'Cadillac',1,0,1,'','product_1194945866.gif');
INSERT INTO catpd VALUES (7,'Chevrolet',1,0,1,'','product_1195009728.jpg');
INSERT INTO catpd VALUES (8,'Xe nhật 1',2,0,1,'','product_1195012095.jpg');
INSERT INTO catpd VALUES (9,'Xe nhật 3',2,0,1,'','product_1195012089.jpg');
INSERT INTO catpd VALUES (10,'Xe nhật 2',2,0,1,'','product_1195012082.jpg');
INSERT INTO catpd VALUES (11,'Xe Mỹ 1',3,0,1,'','product_1195012157.gif');
INSERT INTO catpd VALUES (12,'Xe Mỹ 2',3,0,1,'','product_1195012253.jpg');
INSERT INTO catpd VALUES (13,'Xe Mỹ 3',3,0,1,'','product_1195012260.jpg');
INSERT INTO catpd VALUES (14,'Xe Italia 1',4,0,1,'','product_1195012200.jpg');
INSERT INTO catpd VALUES (15,'Xe Italia 2',4,0,1,'','product_1195012206.jpg');
INSERT INTO catpd VALUES (16,'Xe Italia 3',4,0,1,'','');
INSERT INTO catpd VALUES (18,'Xe hơi khác',21,4,1,'','');
INSERT INTO catpd VALUES (19,'fdf sđfsf ',18,0,1,'','');
INSERT INTO catpd VALUES (20,'dfsfsdf',18,0,1,'','product_1195012216.jpg');
INSERT INTO catpd VALUES (21,'Xe ',0,0,1,'','');
#
# Table structure for table `catpt`
#
DROP table IF EXISTS catpt;
CREATE TABLE `catpt` (
  `id_catpt` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(50) character set latin1 collate latin1_general_ci NOT NULL default '',
  `parentid` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`id_catpt`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
#
# Dumping data for table `catpt`
#
INSERT INTO catpt VALUES (1,'Thư viện xe',0,0,1);
INSERT INTO catpt VALUES (2,'Người đẹp và xe',0,0,1);
#
# Table structure for table `contact`
#
DROP table IF EXISTS contact;
CREATE TABLE `contact` (
  `id_contact` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `content` longtext character set latin1 collate latin1_general_ci NOT NULL,
  `xem` tinyint(4) NOT NULL default '0',
  `ngay` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id_contact`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
#
# Dumping data for table `contact`
#
INSERT INTO contact VALUES (1,'From: nguyen van binh date: 14/11/2007','H&#7885; t&#234;n: nguyen van binh<br>&#272;&#7883;a ch&#7881;:\r\nha noi<br>&#272;i&#7879;n tho&#7841;i: 34534389<br>fax. 43985349<Br>Email. suongmumc@yahoo.com<br>N&#7897;i dung: yeu cau cua toi',1,'0000-00-00');
INSERT INTO contact VALUES (2,'From: nguyen van binh date: 14/11/2007','H&#7885; t&#234;n: nguyen van binh<br>&#272;&#7883;a ch&#7881;:\r\nha noi<br>&#272;i&#7879;n tho&#7841;i: 34534389<br>fax. 43985349<Br>Email. suongmumc@yahoo.com<br>N&#7897;i dung: yeu cau cua toi',1,'0000-00-00');
#
# Table structure for table `count`
#
DROP table IF EXISTS count;
CREATE TABLE `count` (
  `value` bigint(20) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
#
# Dumping data for table `count`
#
INSERT INTO count VALUES (3526);
#
# Table structure for table `country`
#
DROP table IF EXISTS country;
CREATE TABLE `country` (
  `id_country` int(11) NOT NULL auto_increment,
  `name` varchar(100) character set latin1 collate latin1_general_ci default NULL,
  `currency` varchar(50) character set latin1 collate latin1_general_ci default NULL,
  `code` varchar(50) character set latin1 collate latin1_general_ci default NULL,
  PRIMARY KEY  (`id_country`),
  UNIQUE KEY `countryID` (`id_country`),
  KEY `countryID_2` (`id_country`)
) ENGINE=MyISAM AUTO_INCREMENT=214 DEFAULT CHARSET=utf8;
#
# Dumping data for table `country`
#
INSERT INTO country VALUES (1,'United States of America','USD','US');
INSERT INTO country VALUES (2,'Canada','CAD','CA');
INSERT INTO country VALUES (3,'Afghanistan','AFA','AF');
INSERT INTO country VALUES (4,'Albania','ALL','AL');
INSERT INTO country VALUES (5,'Algeria','DZD','DZ');
INSERT INTO country VALUES (6,'Andorra','EUR','AD');
INSERT INTO country VALUES (7,'Angola','AOA','AO');
INSERT INTO country VALUES (8,'Anguilla','XCD','AI');
INSERT INTO country VALUES (10,'Antigua and Barbuda','XCD','AG');
INSERT INTO country VALUES (11,'Argentina','ARS','AR');
INSERT INTO country VALUES (12,'Armenia','AMD','AM');
INSERT INTO country VALUES (13,'Aruba','AWG','AW');
INSERT INTO country VALUES (14,'Australia','AUD','AU');
INSERT INTO country VALUES (15,'Austria','EUR','AT');
INSERT INTO country VALUES (16,'Azerbaijan','AZM','AZ');
INSERT INTO country VALUES (17,'Bahamas','BSD','BS');
INSERT INTO country VALUES (18,'Bahrain','BHD','BH');
INSERT INTO country VALUES (19,'Bangladesh','BDT','BD');
INSERT INTO country VALUES (20,'Barbados','BBD','BB');
INSERT INTO country VALUES (21,'Belarus','BYR','BY');
INSERT INTO country VALUES (22,'Belgium','EUR','BE');
INSERT INTO country VALUES (23,'Belize','BZD','BZ');
INSERT INTO country VALUES (24,'Benin','XOF','BJ');
INSERT INTO country VALUES (25,'Bermuda','BMD','BM');
INSERT INTO country VALUES (26,'Bhutan','BTN','BT');
INSERT INTO country VALUES (27,'Bolivia','BOB','BO');
INSERT INTO country VALUES (28,'Bosnia-Herzegovina','BAM','BA');
INSERT INTO country VALUES (29,'Botswana','BWP','BW');
INSERT INTO country VALUES (30,'Brazil','BRL','BR');
INSERT INTO country VALUES (31,'Brunei Darussalam','BND','BN');
INSERT INTO country VALUES (32,'Bulgaria','BGL','BG');
INSERT INTO country VALUES (33,'Burkina Faso','XOF','BF');
INSERT INTO country VALUES (34,'Burundi','BIF','BI');
INSERT INTO country VALUES (35,'Cambodia','KHR','KH');
INSERT INTO country VALUES (36,'Cameroon','XAF','CM');
INSERT INTO country VALUES (37,'Cape Verde','CVE','CV');
INSERT INTO country VALUES (38,'Cayman Islands','KYD','KY');
INSERT INTO country VALUES (39,'Central African Republic','XAF','CF');
INSERT INTO country VALUES (40,'Chad','XAF','TD');
INSERT INTO country VALUES (41,'Chile','CHL','CL');
INSERT INTO country VALUES (42,'China','CNY','CN');
INSERT INTO country VALUES (43,'Colombia','COP','CO');
INSERT INTO country VALUES (44,'Comoros','KMF','KM');
INSERT INTO country VALUES (45,'Costa Rica','CRI','CR');
INSERT INTO country VALUES (46,'Croatia','HRK','HR');
INSERT INTO country VALUES (47,'Cuba','CUP','CU');
INSERT INTO country VALUES (48,'Cyprus','CYP','CY');
INSERT INTO country VALUES (49,'Czech Republic','CZK','CZ');
INSERT INTO country VALUES (50,'Denmark','DKK','DK');
INSERT INTO country VALUES (51,'Djibouti','DJF','DJ');
INSERT INTO country VALUES (52,'Dominica','XCD','DM');
INSERT INTO country VALUES (53,'Dominican Republic','DOP','DO');
INSERT INTO country VALUES (54,'East Timor','IDR','TP');
INSERT INTO country VALUES (55,'Ecuador','USD','EC');
INSERT INTO country VALUES (56,'Egypt','EGP','EG');
INSERT INTO country VALUES (57,'El Salvador','SVC','SV');
INSERT INTO country VALUES (58,'Equatorial Guinea','XAF','GQ');
INSERT INTO country VALUES (59,'Estonia','EEK','EE');
INSERT INTO country VALUES (60,'Ethiopia','ETB','ET');
INSERT INTO country VALUES (61,'Falkland Islands','FKP','FK');
INSERT INTO country VALUES (62,'Faroe Islands','DKK','FO');
INSERT INTO country VALUES (63,'Fiji','FJD','FJ');
INSERT INTO country VALUES (64,'Finland','EUR','FI');
INSERT INTO country VALUES (65,'France','EUR','FR');
INSERT INTO country VALUES (66,'French Guiana','EUR','GF');
INSERT INTO country VALUES (67,'French Polynesia','XPF','PF');
INSERT INTO country VALUES (68,'Gabon','XAF','GA');
INSERT INTO country VALUES (69,'Gambia','GMD','GM');
INSERT INTO country VALUES (70,'Georgia, Republic of','GEL','GE');
INSERT INTO country VALUES (71,'Germany','EUR','DE');
INSERT INTO country VALUES (72,'Ghana','GHC','GH');
INSERT INTO country VALUES (73,'Gibraltar','GBP','GI');
INSERT INTO country VALUES (74,'Greece','EUR','GR');
INSERT INTO country VALUES (75,'Greenland','DKK','GL');
INSERT INTO country VALUES (76,'Grenada','XCD','GD');
INSERT INTO country VALUES (77,'Guadeloupe','EUR','GP');
INSERT INTO country VALUES (78,'Guam','USD','GU');
INSERT INTO country VALUES (79,'Guatemala','GTQ','GT');
INSERT INTO country VALUES (80,'Guinea','GNF','GN');
INSERT INTO country VALUES (81,'Guinea-Bissau','XOF','GW');
INSERT INTO country VALUES (82,'Guyana','GYD','GY');
INSERT INTO country VALUES (83,'Haiti','USD','HT');
INSERT INTO country VALUES (84,'Honduras','HNL','HN');
INSERT INTO country VALUES (85,'Hong Kong','HKD','HK');
INSERT INTO country VALUES (86,'Hungary','HUF','HU');
INSERT INTO country VALUES (87,'Iceland','ISK','IS');
INSERT INTO country VALUES (88,'India','INR','IN');
INSERT INTO country VALUES (89,'Indonesia','IDR','ID');
INSERT INTO country VALUES (90,'Iraq','IQD','IQ');
INSERT INTO country VALUES (91,'Ireland','EUR','IE');
INSERT INTO country VALUES (92,'Israel','ILS','IL');
INSERT INTO country VALUES (93,'Italy','EUR','IT');
INSERT INTO country VALUES (94,'Jamaica','JMD','JM');
INSERT INTO country VALUES (95,'Japan','JPY','JP');
INSERT INTO country VALUES (96,'Jordan','JOD','JO');
INSERT INTO country VALUES (97,'Kazakhstan','KZT','KZ');
INSERT INTO country VALUES (98,'Kenya','KES','KE');
INSERT INTO country VALUES (99,'Kiribati','AUD','KI');
INSERT INTO country VALUES (100,'North Korea','KPW','KP');
INSERT INTO country VALUES (101,'South Korea','KRW','KR');
INSERT INTO country VALUES (102,'Kuwait','KWD','KW');
INSERT INTO country VALUES (103,'Latvia','LVL','LV');
INSERT INTO country VALUES (104,'Lebanon','LBP','LB');
INSERT INTO country VALUES (105,'Lesotho','LSL','LS');
INSERT INTO country VALUES (106,'Liberia','LRD','LR');
INSERT INTO country VALUES (107,'England','GBP','EN');
INSERT INTO country VALUES (108,'Liechtenstein','CHF','LI');
INSERT INTO country VALUES (109,'Lithuania','LTL','LT');
INSERT INTO country VALUES (110,'Luxembourg','EUR','LU');
INSERT INTO country VALUES (111,'Macao','MOP','MO');
INSERT INTO country VALUES (112,'Macedonia, Republic of','MKD','MK');
INSERT INTO country VALUES (113,'Madagascar','MGF','MG');
INSERT INTO country VALUES (114,'Malawi','MWK','MW');
INSERT INTO country VALUES (115,'Malaysia','MYR','MY');
INSERT INTO country VALUES (116,'Maldives','MVR','MV');
INSERT INTO country VALUES (117,'Mali','XOF','ML');
INSERT INTO country VALUES (118,'Malta','MTL','MT');
INSERT INTO country VALUES (119,'Martinique','EUR','MQ');
INSERT INTO country VALUES (120,'Mauritania','MRO','MR');
INSERT INTO country VALUES (121,'Mauritius','MUR','MU');
INSERT INTO country VALUES (122,'Mexico','MXN','MX');
INSERT INTO country VALUES (123,'Moldova','MDL','MD');
INSERT INTO country VALUES (124,'Monaco','FRF','MC');
INSERT INTO country VALUES (125,'Mongolia','MNT','MN');
INSERT INTO country VALUES (126,'Montserrat','XCD','MS');
INSERT INTO country VALUES (127,'Morocco','MAD','MA');
INSERT INTO country VALUES (128,'Mozambique','MZM','MZ');
INSERT INTO country VALUES (129,'Myanmar','MMK','MM');
INSERT INTO country VALUES (130,'Namibia','NAD','NA');
INSERT INTO country VALUES (131,'Nauru','AUD','NR');
INSERT INTO country VALUES (132,'Nepal','NPR','NP');
INSERT INTO country VALUES (133,'Netherlands','EUR','NL');
INSERT INTO country VALUES (134,'Netherlands Antilles','ANG','AN');
INSERT INTO country VALUES (135,'New Caledonia','XPF','NC');
INSERT INTO country VALUES (136,'New Zealand','NZD','NZ');
INSERT INTO country VALUES (137,'Nicaragua','NIO','NI');
INSERT INTO country VALUES (138,'Niger','XOF','NE');
INSERT INTO country VALUES (139,'Nigeria','NGN','NG');
INSERT INTO country VALUES (140,'Niue','NZD','NU');
INSERT INTO country VALUES (141,'Norfolk Island','AUD','NF');
INSERT INTO country VALUES (142,'Northern Ireland','GBP','NB');
INSERT INTO country VALUES (143,'Norway','NOK','NO');
INSERT INTO country VALUES (144,'Oman','OMR','OM');
INSERT INTO country VALUES (145,'Pakistan','PKR','PK');
INSERT INTO country VALUES (146,'Panama','PAB','PA');
INSERT INTO country VALUES (147,'Papua New Guinea','PGK','PG');
INSERT INTO country VALUES (148,'Paraguay','PYG','PY');
INSERT INTO country VALUES (149,'Peru','PEN','PE');
INSERT INTO country VALUES (150,'Philippines','PHP','PH');
INSERT INTO country VALUES (151,'Pitcairn Island','NZD','PN');
INSERT INTO country VALUES (152,'Poland','PLN','PL');
INSERT INTO country VALUES (153,'Portugal','EUR','PT');
INSERT INTO country VALUES (154,'Qatar','QAR','QA');
INSERT INTO country VALUES (155,'Reunion','EUR','RE');
INSERT INTO country VALUES (156,'Romania','ROL','RO');
INSERT INTO country VALUES (157,'Russia','RUR','RU');
INSERT INTO country VALUES (158,'Rwanda','RWF','RW');
INSERT INTO country VALUES (159,'Saint Kitts','XCD','KN');
INSERT INTO country VALUES (160,'Saint Lucia','XCD','LC');
INSERT INTO country VALUES (161,'Saint Vincent and the Grenadines','XCD','VC');
INSERT INTO country VALUES (162,'Western Samoa','WST','WS');
INSERT INTO country VALUES (163,'San Marino','EUR','SM');
INSERT INTO country VALUES (164,'Sao Tome and Principe','STD','ST');
INSERT INTO country VALUES (165,'Saudi Arabia','SAR','SA');
INSERT INTO country VALUES (166,'Senegal','XOF','SN');
INSERT INTO country VALUES (167,'Seychelles','SCR','SC');
INSERT INTO country VALUES (168,'Sierra Leone','SLL','SL');
INSERT INTO country VALUES (169,'Singapore','SGD','SG');
INSERT INTO country VALUES (170,'Slovak Republic','SKK','SK');
INSERT INTO country VALUES (171,'Slovenia','SIT','SI');
INSERT INTO country VALUES (172,'Solomon Islands','SBD','SB');
INSERT INTO country VALUES (173,'Somalia','SOS','SO');
INSERT INTO country VALUES (174,'South Africa','ZAR','ZA');
INSERT INTO country VALUES (175,'Spain','EUR','ES');
INSERT INTO country VALUES (176,'Sri Lanka','LKR','LK');
INSERT INTO country VALUES (177,'Saint Helena','SHP','SH');
INSERT INTO country VALUES (178,'Saint Pierre and Miquelon','EUR','PM');
INSERT INTO country VALUES (179,'Sudan','SDD','SD');
INSERT INTO country VALUES (180,'Suriname','SRG','SR');
INSERT INTO country VALUES (181,'Swaziland','SZL','SZ');
INSERT INTO country VALUES (182,'Sweden','SEK','SE');
INSERT INTO country VALUES (183,'Switzerland','CHF','CH');
INSERT INTO country VALUES (184,'Syrian Arab Republic','SYP','SY');
INSERT INTO country VALUES (185,'Taiwan','TWD','TW');
INSERT INTO country VALUES (186,'Tajikistan','TJS','TJ');
INSERT INTO country VALUES (187,'Tanzania','TZS','TZ');
INSERT INTO country VALUES (188,'Thailand','THB','TH');
INSERT INTO country VALUES (189,'Togo','XOF','TG');
INSERT INTO country VALUES (190,'Tokelau','NZD','TK');
INSERT INTO country VALUES (191,'Tonga','TOP','TO');
INSERT INTO country VALUES (192,'Trinidad and Tobago','TTD','TT');
INSERT INTO country VALUES (193,'Tunisia','TND','TN');
INSERT INTO country VALUES (194,'Turkey','TRL','TR');
INSERT INTO country VALUES (195,'Turkmenistan','TMM','TM');
INSERT INTO country VALUES (196,'Turks and Caicos Islands','USD','TC');
INSERT INTO country VALUES (197,'Tuvalu','TVD','TV');
INSERT INTO country VALUES (198,'Uganda','UGX','UG');
INSERT INTO country VALUES (199,'Ukraine','UAH','UA');
INSERT INTO country VALUES (200,'United Arab Emirates','AED','AE');
INSERT INTO country VALUES (201,'Great Britain and Northern Ireland','GBP','GB');
INSERT INTO country VALUES (202,'Uruguay','UYU','UY');
INSERT INTO country VALUES (203,'Uzbekistan','UZS','UZ');
INSERT INTO country VALUES (204,'Vanuatu','VUV','VU');
INSERT INTO country VALUES (205,'Vatican City','ITL','VA');
INSERT INTO country VALUES (206,'Venezuela','VEB','VE');
INSERT INTO country VALUES (207,'Vietnam','VND','VN');
INSERT INTO country VALUES (208,'British Virgin Islands','USD','VG');
INSERT INTO country VALUES (209,'Wallis and Futuna Islands','XPF','WF');
INSERT INTO country VALUES (210,'Yemen','YER','YE');
INSERT INTO country VALUES (211,'Zambia','ZMK','ZM');
INSERT INTO country VALUES (212,'Zimbabwe','ZWD','ZW');
INSERT INTO country VALUES (213,'Iran','IRR','IR');
#
# Table structure for table `feedback`
#
DROP table IF EXISTS feedback;
CREATE TABLE `feedback` (
  `id_feedback` bigint(20) unsigned NOT NULL auto_increment,
  `id_user` bigint(20) unsigned NOT NULL,
  `id_product` bigint(20) unsigned NOT NULL,
  `comment` longtext NOT NULL,
  PRIMARY KEY  (`id_feedback`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
#
# Dumping data for table `feedback`
#
INSERT INTO feedback VALUES (1,3,1,'dsfsdafsdfsadfsdafsdfsdf');
INSERT INTO feedback VALUES (2,3,1,'sdfsdfsdfsdfsdafdsafsdafsddsfsd');
INSERT INTO feedback VALUES (3,3,1,'sadsadsadsadsadsadsa');
INSERT INTO feedback VALUES (4,3,1,'sadsadsadsadsadsadsa');
INSERT INTO feedback VALUES (5,3,1,'sadsadsadsadsadsadsa');
INSERT INTO feedback VALUES (6,3,1,'fxdfgdfgdfgdf');
INSERT INTO feedback VALUES (7,3,2,'xczxCxZCxz');
INSERT INTO feedback VALUES (8,3,2,'xczxCxZCxz');
INSERT INTO feedback VALUES (9,3,2,'xczxCxZCxz');
INSERT INTO feedback VALUES (10,3,2,'xczxCxZCxz');
INSERT INTO feedback VALUES (11,3,2,'xczxCxZCxz');
INSERT INTO feedback VALUES (12,3,2,'xczxCxZCxz');
INSERT INTO feedback VALUES (13,3,2,'xczxCxZCxz');
INSERT INTO feedback VALUES (14,3,2,'xczxCxZCxz');
INSERT INTO feedback VALUES (15,3,2,'xczxCxZCxz');
INSERT INTO feedback VALUES (16,3,2,'xczxCxZCxz');
INSERT INTO feedback VALUES (17,3,2,'xczxCxZCxz');
INSERT INTO feedback VALUES (18,3,2,'xczxCxZCxz');
INSERT INTO feedback VALUES (19,3,2,'xczxCxZCxz');
INSERT INTO feedback VALUES (20,3,2,'xczxCxZCxz');
#
# Table structure for table `info`
#
DROP table IF EXISTS info;
CREATE TABLE `info` (
  `id_info` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `content` longtext character set latin1 collate latin1_general_ci NOT NULL,
  `thu_tu` bigint(20) default '0',
  `id_catif` bigint(11) NOT NULL default '0',
  `id_user` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '0',
  `ngay_dang` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`id_info`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
#
# Dumping data for table `info`
#
INSERT INTO info VALUES (1,'Gi&#7899;i thi&#7879;u v&#7873; KyLin','<font face=\"Arial\"><font face=\"Arial\"><font face=\"Arial\">C&ocirc;ng ty Cổ phần &Ocirc;t&ocirc; KyLin gửi lời ch&agrave;o tr&acirc;n trọng v&agrave; lời ch&uacute;c sức khoẻ tới Qu&yacute; kh&aacute;ch h&agrave;ng c&ugrave;ng gia đ&igrave;nh !<br />\r\n<br />\r\nVinh dự c&oacute; mặt với tư c&aacute;ch l&agrave; nh&agrave; cung cấp h&agrave;ng đầu c&aacute;c loại xe &ocirc;t&ocirc; nhập khẩu tại thị trường Việt Nam, l&agrave; cầu nối giữa thị trường &Ocirc;t&ocirc; trong nước v&agrave; thế giới, KyLin mang đến cho Qu&yacute; kh&aacute;ch h&agrave;ng th&ecirc;m nhiều sự lựa chọn mới, đ&oacute; l&agrave; những chiếc MERCEDES, BMW, LEXUS h&agrave;o nho&aacute;ng, sang trọng; đ&oacute; l&agrave; những chiếc INFINITI với thiết kế mạnh mẽ đầy quyến rũ; những chiếc NISSAN việt d&atilde; thể thao ấn tượng; TOYOTA c&aacute; t&iacute;nh v&agrave; nhiều kh&aacute;m ph&aacute;; hay với những MATIZ, KIA MORNING nhỏ gọn m&agrave; tao nh&atilde;... ph&ugrave; hợp với c&aacute;c c&aacute; nh&acirc;n v&agrave; doanh nghiệp th&agrave;nh đạt. Tại đ&acirc;y Qu&yacute; kh&aacute;ch h&agrave;ng sẽ được đảm bảo to&agrave;n bộ c&aacute;c ti&ecirc;u ch&iacute;:<br />\r\n<ul>\r\n    <li>Chất lượng của sản phẩm đảm bảo đ&uacute;ng theo những g&igrave; m&agrave; KyLin đ&atilde; ch&agrave;o b&aacute;n. </li>\r\n    <li>Gi&aacute; cả của sản phẩm được đảm bảo trong to&agrave;n l&atilde;nh thổ Việt Nam. </li>\r\n    <li>Sản phẩm được bảo h&agrave;nh v&agrave; bảo tr&igrave; theo đ&uacute;ng ti&ecirc;u chuẩn của nh&agrave; sản xuất. </li>\r\n</ul>\r\nS&aacute;t c&aacute;nh c&ugrave;ng Ban l&atilde;nh đạo giỏi, d&agrave;y dạn kinh nghiệm trong ng&agrave;nh kinh doanh &ocirc;t&ocirc; l&agrave; một đội ngũ c&aacute;n bộ, nh&acirc;n vi&ecirc;n trẻ, năng động, lịch thiệp v&agrave; đầy nhiệt huyết, b&ecirc;n cạnh đ&oacute; l&agrave; một m&ocirc;i trường l&agrave;m việc chuy&ecirc;n nghiệp, một cơ sở vật chất trang bị tiện nghi, hiện đại, KyLin mong muốn mang đến cho Qu&yacute; kh&aacute;ch h&agrave;ng những sản phẩm đa dạng, đảm bảo chất lượng k&egrave;m theo những dịch vụ ho&agrave;n hảo:<br />\r\n<ul>\r\n    <li>Dịch vụ cung cấp v&agrave; nhập khẩu uỷ th&aacute;c c&aacute;c d&ograve;ng xe &ocirc; t&ocirc; được ph&eacute;p nhập khẩu về Việt Nam. </li>\r\n    <li>Dịch vụ đổi xe, sang t&ecirc;n đổi biển số c&aacute;c d&ograve;ng xe đang lưu h&agrave;nh tại Việt Nam. </li>\r\n    <li>Dịch vụ đăng tin b&aacute;n xe miễn ph&iacute; cho mọi đối tượng kh&aacute;ch h&agrave;ng tr&ecirc;n Website của KyLin. </li>\r\n    <li>Dịch vụ tư vấn, lập phương &aacute;n ho&agrave;n thiện thủ tục vay vốn trả g&oacute;p, nộp thuế v&agrave; đăng kiểm xe trong thời gian nhanh nhất. </li>\r\n</ul>\r\nTr&acirc;n trọng k&iacute;nh ch&agrave;o v&agrave; hẹn gặp lại !<br />\r\nKyLin</font></font></font>',0,1,7,1,1194949079);
INSERT INTO info VALUES (4,'Thông tin trên trang liên hệ','<p align=\"center\"><strong>C&Ocirc;NG TY CỔ PHẦN THƯƠNG MẠI KYLIN-GX668</strong></p>\r\n<p align=\"center\">ĐC: Số 8 Đường v&agrave;nh đai I - Kim Li&ecirc;n - &Ocirc; Chợ Dừa - Đống Đa - H&agrave; Nội<br />\r\nĐT: 04. 573 7743&nbsp; -&nbsp; FAX: 04. 573 7745<br />\r\nEmail: kylin_gx668@yahoo.com&nbsp; -&nbsp; Website: kylingx668.com.vn</p>',0,4,7,1,1195031593);
INSERT INTO info VALUES (3,'Bảo hành ','<font face=\"Arial\"><font face=\"Arial\"><font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh </font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font>Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh <font face=\"Arial\">Bảo h&agrave;nh </font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font>',0,3,7,1,1194949202);
#
# Table structure for table `logo`
#
DROP table IF EXISTS logo;
CREATE TABLE `logo` (
  `id_logo` bigint(20) unsigned NOT NULL auto_increment,
  `id_catlg` bigint(20) unsigned NOT NULL default '0',
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `link` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `ngay_dang` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  `small_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `normal_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  PRIMARY KEY  (`id_logo`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
#
# Dumping data for table `logo`
#
INSERT INTO logo VALUES (1,2,'Ho&#224;ng gia auto','','http://hoanggiaauto.com',1195030069,0,1,'','');
INSERT INTO logo VALUES (2,2,'Vnexpress.net','','http://vnexpress.net',1195030089,0,1,'','');
INSERT INTO logo VALUES (3,1,'Anh trang ch&#7911;','logo_1195030146.gif','',1195030146,0,1,'thumb_logo_1195030146.gif','normal_logo_1195030146.gif');
INSERT INTO logo VALUES (4,1,'flash','logo_1195033523.swf','',1195033453,0,1,'','');
INSERT INTO logo VALUES (5,6,'dfsfd','logo_1195181494.jpg','http://vnexpress.net',1195181400,0,1,'thumb_logo_1195181494.jpeg','normal_logo_1195181494.jpeg');
INSERT INTO logo VALUES (6,6,'fđf','logo_1195181589.jpg','',1195181590,0,1,'thumb_logo_1195181589.jpeg','normal_logo_1195181589.jpeg');
INSERT INTO logo VALUES (7,5,'sd','logo_1195181598.jpg','',1195181599,0,1,'thumb_logo_1195181598.jpeg','normal_logo_1195181598.jpeg');
#
# Table structure for table `module`
#
DROP table IF EXISTS module;
CREATE TABLE `module` (
  `id_module` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `gia_tri` varchar(255) character set latin7 NOT NULL default '',
  `thu_tu` bigint(20) unsigned NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id_module`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;
#
# Dumping data for table `module`
#
INSERT INTO module VALUES (5,'Module manager','module.php',0,1);
INSERT INTO module VALUES (9,'C&#7845;u h&#236;nh h&#7879; th&#7889;ng','settings.php',0,1);
INSERT INTO module VALUES (45,'Qu&#7843;n l&#253; database','dbadmin.php',0,1);
INSERT INTO module VALUES (26,'Th&#244;ng tin li&#234;n h&#7879; th&#7915; kh&#225;ch h&#224;ng','contact.php',0,1);
INSERT INTO module VALUES (27,'Qu&#7843;n l&#253; s&#7843;n ph&#7849;m','catpd.php',0,1);
INSERT INTO module VALUES (48,'Qu?n l� tin t?c','cat.php',0,1);
INSERT INTO module VALUES (36,'Logo qu?ng c�o','catlg.php',0,1);
INSERT INTO module VALUES (53,'H&#7895; tr&#7907; tr&#7921;c tuy&#7871;n','yahoo.php',0,1);
INSERT INTO module VALUES (52,'Qu&#7843;n l&#253; trang n&#7897;i dung','catif.php',0,1);
INSERT INTO module VALUES (42,'Qu&#7843;n l&#253; ng&#432;&#7901;i s&#7917; d&#7909;ng','users.php',0,1);
INSERT INTO module VALUES (43,'Th&#244;ng tin c&#225; nh&#226;n','myadmin.php',0,1);
INSERT INTO module VALUES (56,'shortcut','short.php',0,1);
INSERT INTO module VALUES (57,'B&#7897; s&#432;u t&#7853;p &#7843;nh','catpt.php',0,1);
INSERT INTO module VALUES (65,'Quản lý thành viên','users_post.php',0,1);
INSERT INTO module VALUES (64,'Th&#244;ng tin &#273;&#7863;t h&#224;ng','order.php',0,1);
INSERT INTO module VALUES (66,'Ảnh sản phẩm','carphoto.php',0,1);
#
# Table structure for table `news`
#
DROP table IF EXISTS news;
CREATE TABLE `news` (
  `id_news` bigint(20) unsigned NOT NULL auto_increment,
  `id_cat` bigint(20) unsigned NOT NULL default '0',
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `title` varchar(255) NOT NULL,
  `image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `intro` mediumtext character set latin1 collate latin1_general_ci NOT NULL,
  `content` text character set latin1 collate latin1_general_ci NOT NULL,
  `ngay_dang` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  `small_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `normal_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `id_user` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`id_news`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
#
# Dumping data for table `news`
#
INSERT INTO news VALUES (1,1,'Thị trường ôtô tháng 2 giảm gần một nửa','','news_1194941163.jpg','Lượng &ocirc;t&ocirc; 11 li&ecirc;n doanh ti&ecirc;u thụ trong th&aacute;ng 2 đạt 1.397 chiếc, chỉ bằng 59% của th&aacute;ng đầu năm v&agrave; bằng 70% nếu so với c&ugrave;ng thời điểm năm ngo&aacute;i.','<p class=\"Normal\">Tuy nhi&ecirc;n, t&iacute;nh tổng cả 2 th&aacute;ng đầu năm, con số n&agrave;y l&agrave; 3.767 xe, cao hơn c&ugrave;ng kỳ của năm ngo&aacute;i (3.389 xe).</p>\r\n<p class=\"Normal\">Dẫn đầu vẫn l&agrave; Toyota. Th&aacute;ng qua, h&atilde;ng xe c&oacute; thị phần lớn nhất Việt Nam xuất xưởng 526 chiếc xe, chỉ bằng 74% của th&aacute;ng trước, nhưng cũng đủ để chiếm gần 38% thị trường. Đứng thứ hai l&agrave; Vidamco, b&aacute;n được 206 xe.</p>\r\n<p class=\"Normal\">Ford Việt Nam l&ecirc;n thứ ba thay cho Vinastar. Trong tổng số 180 xe m&agrave; h&atilde;ng b&aacute;n ra, c&oacute; tới 73 chiếc l&agrave; xe pick-up đa dụng Ford Ranger. </p>\r\n<p class=\"Normal\">Cũng trong th&aacute;ng qua, Vidamco v&agrave; Vindaco đ&atilde; th&ocirc;ng b&aacute;o gi&aacute; b&aacute;n mới cho xe năm 2005. Đắt nhất hiện nay của Vidamco l&agrave; Daewoo Magnus L6, gi&aacute; 36.100 USD, tức l&agrave; tăng th&ecirc;m 3.200 USD so với khi n&oacute; tr&igrave;nh l&agrave;ng v&agrave;o đầu th&aacute;ng 1 năm ngo&aacute;i.</p>\r\n<p class=\"Normal\">Chiếc xe đắt nhất v&agrave; rất được thị trường ưa chuộng của Vindaco, chiếc Daihatsu Terios hiện c&oacute; gi&aacute; mới l&agrave; 24.000 USD. </p>\r\n<table cellspacing=\"2\" cellpadding=\"2\" width=\"331\" align=\"center\" border=\"1\">\r\n    <tbody>\r\n        <tr>\r\n            <td class=\"BoxLink\" valign=\"top\" align=\"center\" width=\"327\" bgcolor=\"#00005e\" colspan=\"4\"><strong><font color=\"#ffff00\">Lượng xe &ocirc;t&ocirc; VAMA ti&ecirc;u thụ th&aacute;ng 2/2005</font></strong></td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"BoxLink\" valign=\"top\" align=\"center\" width=\"31\">TT</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">H&atilde;ng</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">Số lượng (xe)</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">Thị phần(%)</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"BoxLink\" valign=\"top\" align=\"center\" width=\"31\">1</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">Toyota</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">526</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">37,7%</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"BoxLink\" valign=\"top\" align=\"center\" width=\"31\">2</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">Vidamco</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\" u1:num=\"\">206</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\" u1:num=\"0.18126022913256956\" u1:fmla=\"=C5/C$24\">14,7%</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"BoxLink\" valign=\"top\" align=\"center\" width=\"31\">3</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">Ford</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">180</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">12,9%</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"BoxLink\" valign=\"top\" align=\"center\" width=\"31\">4</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">Suzuki</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\" u1:num=\"\">165</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\" u1:num=\"0.13707037643207856\" u1:fmla=\"=C7/C$24\">11,8% </td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"BoxLink\" valign=\"top\" align=\"center\" width=\"31\">5</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">Mekong</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\" u1:num=\"\" u1:fmla=\"=SUM(C8:C11)\">77</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\" u1:num=\"8.5924713584288048E-2\" u1:fmla=\"=C12/C$24\">5,5%</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"BoxLink\" valign=\"top\" align=\"center\" width=\"31\">6</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">Vinastar</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">73</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">5,2%</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"BoxLink\" valign=\"top\" align=\"center\" width=\"31\">7</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">Mercedes</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">50</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">3,6%</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"BoxLink\" valign=\"top\" align=\"center\" width=\"31\">8</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">Isuzu</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">47</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">3,4%</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"BoxLink\" valign=\"top\" align=\"center\" width=\"31\">9</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">Hino</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\" u1:num=\"\">40</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\" u1:num=\"2.0458265139116204E-2\" u1:fmla=\"=C14/C$24\">3,9%</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"BoxLink\" valign=\"top\" align=\"center\" width=\"31\">10</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">VMC</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\" u1:num=\"\" u1:fmla=\"=SUM(C16:C18)\">40</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\" u1:num=\"2.0458265139116204E-2\" u1:fmla=\"=C19/C$24\">1,7%</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"BoxLink\" valign=\"top\" align=\"center\" width=\"31\">11</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">Vindaco</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\" u1:num=\"\">12</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\" u1:num=\"9.0016366612111296E-3\" u1:fmla=\"=C22/C$24\">0,9%</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"BoxLink\" valign=\"top\" align=\"center\" width=\"31\" colspan=\"2\">&nbsp;Tổng</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">1.397</td>\r\n            <td class=\"BoxLink\" align=\"center\" width=\"97\">100%</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table cellspacing=\"3\" cellpadding=\"3\" width=\"400\" bgcolor=\"#f4f4f4\" border=\"0\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" border=\"0\">\r\n                <tbody>\r\n                    <tr>\r\n                        <td class=\"OtherTitle\" valign=\"middle\" height=\"40\">Theo d&ograve;ng sự kiện:</td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            <table cellspacing=\"0\" cellpadding=\"2\" width=\"96%\" border=\"0\">\r\n                <tbody>\r\n                    <tr>\r\n                        <td><a class=\"Other\" href=\"http://vnexpress.net/Vietnam/Oto-Xe-may/2005/02/3B9DB767/\">Kh&ocirc;ng kh&aacute;ch h&agrave;ng mới, nh&agrave; m&aacute;y vẫn sống khoẻ</a><span class=\"ShowDate\">&nbsp;(17/02/2005)</span></td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td><a class=\"Other\" href=\"http://vnexpress.net/Vietnam/Oto-Xe-may/2005/01/3B9DA6E6/\">Toyota vẫn đứng đầu thị trường &ocirc;t&ocirc; Việt Nam</a><span class=\"ShowDate\">&nbsp;(10/01/2005)</span></td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td><a class=\"Other\" href=\"http://vnexpress.net/Vietnam/Oto-Xe-may/2004/12/3B9D9585/\">Ford dẫn đầu thị trường th&aacute;ng 11</a><span class=\"ShowDate\">&nbsp;(07/12/2004)</span></td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td align=\"right\"><a class=\"Other\" href=\"http://vnexpress.net/Topic/?ID=2711\">Xem tiếp&raquo;</a></td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<p class=\"Normal\" align=\"right\"><strong>X.O.</strong></p>',1194941163,0,1,'thumb_news_1194941163.jpeg','',7);
INSERT INTO news VALUES (2,1,'Vindaco hỗ trợ thuế trước bạ cho khách hàng','','news_1194941199.jpg','<p class=\"Lead\">Theo Daihatsu Vietindo (Vindaco), cho tới hết th&aacute;ng 4 tới, kh&aacute;ch h&agrave;ng mua bất kỳ mẫu xe n&agrave;o của c&ocirc;ng ty tại c&aacute;c đại l&yacute; uỷ quyền ch&iacute;nh thức đều được tặng 100% thuế trước bạ. </p>','<p class=\"Normal\">Hiện sản phẩm của Vindaco c&oacute; 4 loại, gồm Hijet Jumbo 1.6 c&oacute; mức gi&aacute; 9.800 USD, thuế trước bạ 2%; Citivan gi&aacute; 19.000 USD, thuế trước bạ 5%; Devan gi&aacute; 12.500 USD, thuế trước bạ 2%; v&agrave; Terios gi&aacute; 24.000 USD, thuế trước bạ 5%.</p>\r\n<p class=\"Normal\">Trong năm 2004, Vindaco đ&atilde; cung cấp cho thị trường 813 xe, n&acirc;ng tổng số &ocirc;t&ocirc; b&aacute;n ra kể từ khi c&oacute; mặt tại Việt Nam l&ecirc;n con số khoảng 5.500 chiếc. Mẫu xe đa dụng hạng nhỏ <a class=\"Normal\" href=\"http://sg.vnexpress.net/Vietnam/Oto-Xe-may/2003/01/3B9C40A0/\">Terios</a> rất được thị trường ưa chuộng từ l&uacute;c ra mắt đầu năm 2003, hiện mới chỉ ti&ecirc;u thụ 370 chiếc. Theo c&ocirc;ng ty, nguy&ecirc;n nh&acirc;n l&agrave; do số lượng linh kiện nhập hạn chế. </p>\r\n<p class=\"Normal\" align=\"right\"><strong>X.O.</strong></p>',1194941199,0,1,'thumb_news_1194941199.jpeg','',7);
INSERT INTO news VALUES (3,3,'Người nước ngoài chưa được biết đầy đủ về bệnh tả ở VN','','news_1194941251.jpg','Việc &iacute;t nhất một người nước ngo&agrave;i nhiễm tả ở Việt Nam đ&atilde; đặt ra vấn đề ph&ograve;ng tr&aacute;nh cho du kh&aacute;ch ngoại quốc, cũng như c&aacute;c tổ chức nước ngo&agrave;i. Cục trưởng Cục Y tế dự ph&ograve;ng Nguyễn Huy Nga cho biết, Bộ Y tế chưa c&oacute; khuyến c&aacute;o ri&ecirc;ng n&agrave;o cho những người n&agrave;y.<br />','&nbsp;&Ocirc;ng đ&aacute;nh gi&aacute; thế n&agrave;o về nguy cơ nhiễm tả của người nước ngo&agrave;i, nhất l&agrave; những người c&oacute; th&oacute;i quen ăn thực phẩm đường phố?\r\n<div class=\"Normal\">\r\n<p class=\"Normal\"><em>\r\n<table cellspacing=\"0\" cellpadding=\"3\" width=\"167\" align=\"right\" bgcolor=\"#f4faff\" border=\"1\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <p class=\"Normal\">S&aacute;ng 12/11 &ocirc;ng Chu Đồng, B&iacute; thư thứ nhất Ph&ograve;ng Ch&iacute;nh trị, Đại sứ qu&aacute;n Trung Quốc cho biết, chưa nhận được th&ocirc;ng b&aacute;o n&agrave;o của WHO hay Bộ Y tế về dịch ti&ecirc;u chảy cấp. Tuy nhi&ecirc;n, họ đ&atilde; biết tin n&agrave;y v&agrave; th&ocirc;ng b&aacute;o tr&ecirc;n mạng của Bộ Ngoại giao, y&ecirc;u cầu kh&aacute;ch TQ lưu &yacute; đến vệ sinh an to&agrave;n thực phẩm. </p>\r\n            <p class=\"Normal\">&Ocirc;ng Anton Golubev, Tuỳ vi&ecirc;n Đại sứ Nga cũng khẳng định chưa nhận được th&ocirc;ng b&aacute;o n&agrave;o từ WHO hoặc Bộ Y tế về dịch. Cơ quan n&agrave;y biết tin qua một tờ b&aacute;o Việt Nam v&agrave; đ&atilde; th&ocirc;ng b&aacute;o cho những người Nga đang sống tại đ&acirc;y. </p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n&nbsp;</em>- Cơ chế l&acirc;y bệnh th&igrave; với người n&agrave;o cũng như nhau. N&oacute;i chung người nước ngo&agrave;i nguy cơ thấp hơn v&igrave; họ c&oacute; tập qu&aacute;n ăn uống vệ sinh. Tuy nhi&ecirc;n, thực tế cũng c&oacute; một số người sử dụng thức ăn đường phố như người bản địa, thậm ch&iacute; ăn thịt ch&oacute;, mắm t&ocirc;m, rau sống. Do đ&oacute;, nguy cơ nhiễm bệnh ở họ cũng giống như với người Việt.</p>\r\n</div>\r\n<p class=\"Normal\"><em designtimesp=\"23000\">-<font color=\"#ff0000\"> </font><font color=\"#000000\">Vậy ng&agrave;nh y tế đ&atilde; c&oacute; những khuyến c&aacute;o n&agrave;o với với họ?</font></em></p>\r\n<p class=\"Normal\">- Ch&uacute;ng t&ocirc;i đ&atilde; th&ocirc;ng b&aacute;o t&igrave;nh h&igrave;nh dịch với Tổ chức Y tế Thế giới (WHO) rằng Việt Nam đang c&oacute; dịch ti&ecirc;u chảy cấp, trong đ&oacute; c&oacute; những ca bệnh tả v&agrave; tổ chức n&agrave;y đ&atilde; b&aacute;o cho c&aacute;c đại sứ qu&aacute;n. Ngo&agrave;i ra, tin tức, c&aacute;ch ph&ograve;ng chống dịch đ&atilde; được c&ocirc;ng bố rộng r&atilde;i tr&ecirc;n b&aacute;o ch&iacute; v&agrave; nhiều k&ecirc;nh th&ocirc;ng tin kh&aacute;c. C&ocirc;ng điện của Thủ tướng đ&atilde; y&ecirc;u cầu huy động cả hệ thống ch&iacute;nh trị v&agrave;o việc tuy&ecirc;n truyền, do đ&oacute; c&aacute;c kh&aacute;ch sạn, c&ocirc;ng ty lữ h&agrave;nh cũng phải th&ocirc;ng tin cho kh&aacute;ch. </p>\r\n<p class=\"Normal\"><em designtimesp=\"23008\">- Tuy nhi&ecirc;n, c&oacute; những kh&aacute;ch du lịch tự do kh&ocirc;ng li&ecirc;n hệ với sứ qu&aacute;n hay c&ocirc;ng ty lữ h&agrave;nh, ăn ngo&agrave;i phố, ở nh&agrave; nghỉ b&igrave;nh thường v&agrave; kh&ocirc;ng hiểu tiếng Việt. L&agrave;m sao họ biết tin được?</em></p>\r\n<p class=\"Normal\">- Th&igrave; họ cũng phải đọc b&aacute;o chứ! Ở Việt Nam cũng c&oacute; một số tờ b&aacute;o tiếng Anh. V&agrave; để bảo vệ họ cũng như những người sử dụng thực phẩm đường phố kh&aacute;c, Bộ Y tế đang c&oacute; chiến dịch kiểm so&aacute;t chặt chẽ chất lượng vệ sinh của c&aacute;c h&agrave;ng qu&aacute;n. </p>\r\n<p class=\"Normal\"><em>&nbsp;- Sau khi ph&aacute;t hiện người nước ngo&agrave;i nhiễm tả, Bộ Y tế c&oacute; tăng cường việc th&ocirc;ng tin cho đối tượng n&agrave;y?</em></p>\r\n<p class=\"Normal\">- Ngay từ đầu dịch, Bộ Y tế đ&atilde; c&oacute; khuyến c&aacute;o về c&aacute;c biện ph&aacute;p ph&ograve;ng ngừa, đ&acirc;y l&agrave; khuyến c&aacute;o chung chứ kh&ocirc;ng ph&acirc;n biệt người nước ngo&agrave;i hay Việt Nam. Hiện Bộ cũng kh&ocirc;ng c&oacute; th&ocirc;ng b&aacute;o n&agrave;o th&ecirc;m cho người nước ngo&agrave;i. </p>\r\n<p class=\"Normal\"><em designtimesp=\"23012\">- Việc xuất hiện người nước ngo&agrave;i nhiễm tả c&oacute; l&agrave;m tăng nguy cơ lan dịch ra nước kh&aacute;c, nhất l&agrave; với trường hợp kh&ocirc;ng triệu chứng? </em></p>\r\n<p class=\"Normal\">- Để ngăn dịch lan ra ngo&agrave;i bi&ecirc;n giới, những người nước ngo&agrave;i mắc tả sẽ kh&ocirc;ng được xuất cảnh cho đến khi c&oacute; x&eacute;t nghiệm &acirc;m t&iacute;nh 3 lần với phẩy khuẩn tả. </p>\r\n<p class=\"Normal\">C&ograve;n với những người nhiễm kh&ocirc;ng triệu chứng, hiện chưa c&oacute; c&aacute;ch g&igrave; kiểm so&aacute;t, v&agrave; ngay cả người Việt cũng c&oacute; thể đưa mầm bệnh ra nước ngo&agrave;i. Tuy nhi&ecirc;n, dịch c&oacute; lan ra c&aacute;c nước khi họ xuất cảnh hay kh&ocirc;ng t&ugrave;y thuộc v&agrave;o điều kiện nơi đến. Ở những nước ti&ecirc;n tiến, chắc chắn vẫn c&oacute; những người mang mầm bệnh tả từ nhiều nơi nhập cảnh nhưng dịch bệnh vẫn kh&ocirc;ng xảy ra do hệ thống xử l&yacute; m&ocirc;i trường ho&agrave;n hảo v&agrave; th&oacute;i quen vệ sinh của người d&acirc;n rất tốt.</p>\r\n<p class=\"Normal\"><em>- Khi c&oacute; triệu chứng tả, người nước ngo&agrave;i cần đến đ&acirc;u để được gi&uacute;p đỡ?</em></p>\r\n<p class=\"Normal\">- Khi thấy c&aacute;c triệu chứng như ti&ecirc;u chảy, n&ocirc;n..., cần đến ngay cơ sở y tế gần nhất, v&igrave; mọi cơ sở đều đ&atilde; được hướng dẫn c&aacute;ch xử l&yacute; đối với ti&ecirc;u chảp cấp n&oacute;i chung v&agrave; tả n&oacute;i ri&ecirc;ng. </p>\r\n<table cellspacing=\"0\" cellpadding=\"3\" width=\"100%\" bgcolor=\"#f4f4ff\" border=\"1\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <p class=\"Normal\">Trả lời ph&oacute;ng vi&ecirc;n <em>VnExpess</em> chiều qua, 3 trong số 10 kh&aacute;ch du lịch nước ngo&agrave;i tại H&agrave; Nội (phần lớn l&agrave; sinh vi&ecirc;n v&agrave; kh&aacute;ch tự do) kh&ocirc;ng biết l&agrave; Việt Nam đang c&oacute; bệnh tả. Những người c&ograve;n lại biết tin qua b&aacute;o v&agrave; bạn b&egrave; ở Việt Nam, hoặc hỏi đại sứ qu&aacute;n. </p>\r\n            <p class=\"Normal\">Chị Jessica, người Thụy Điển, đang dạy tại một c&ocirc;ng ty tin học Việt Nam, cho biết c&oacute; nghe l&aacute;ng m&aacute;ng về việc Việt Nam đang c&oacute; dịch ti&ecirc;u chảy cấp: &quot;Đ&acirc;y l&agrave; lần đầu ti&ecirc;n t&ocirc;i nghe về dịch ti&ecirc;u chảy cấp, t&ocirc;i cho n&oacute; rất b&igrave;nh thường. Hằng ng&agrave;y, t&ocirc;i vẫn ra phố đi dạo, v&agrave; ăn đồ của kh&aacute;ch sạn&quot;. </p>\r\n            <p class=\"Normal\">Phần lớn những kh&aacute;ch nước ngo&agrave;i được hỏi kh&aacute;c đều cho biết kh&ocirc;ng sợ h&atilde;i khi nghe tin về bệnh tả, v&igrave; cho rằng chỉ cần cẩn thận với thức ăn v&agrave; đồ uống l&agrave; kh&ocirc;ng sao. Ngay khi chưa c&oacute; th&ocirc;ng tin g&igrave; về dịch bệnh n&agrave;y, họ đ&atilde; được khuyến c&aacute;o l&agrave; kh&ocirc;ng ăn uống ngo&agrave;i đường phố, nhất l&agrave; với đồ uống.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>',1194941251,0,1,'thumb_news_1194941251.jpeg','',7);
INSERT INTO news VALUES (4,1,'Lamborghini tung ra Murcielago &#39;hạng nhẹ&#39;','','news_1194945178.jpg','Nh&agrave; sản xuất Italy đang cố gắng chiếm lĩnh ph&acirc;n kh&uacute;c si&ecirc;u xe khi tiếp tục tr&igrave;nh l&agrave;ng Murcielago SV, với trọng lượng nhẹ hơn &quot;quỷ dữ&quot; LP640 130 kg nhưng c&ocirc;ng suất cao hơn khoảng 40 m&atilde; lực.','<p class=\"Normal\">Sau khi <a class=\"Normal\" href=\"http://www.vnexpress.net/Vietnam/Oto-Xe-may/2007/09/3B9FA2CE/\">Revent&oacute;n</a>&nbsp;l&agrave;m kh&aacute;ch tham quan tại triển l&atilde;m Frankfurt, diễn ra v&agrave;o th&aacute;ng 9, say như điếu đổ với danh hiệu &quot;Xe đẹp nhất&quot;, nh&agrave; sản xuất si&ecirc;u xe Italy sẽ tung ra Murcielago SV, một trong những con b&agrave;i quan trọng đ&aacute;nh v&agrave;o niềm đam m&ecirc; của người h&acirc;m mộ.</p>\r\n<p class=\"Normal\">Tạp ch&iacute; <em designtimesp=\"17527\">Autocar </em>của Anh tiết lộ SV c&oacute; trọng lượng nhẹ hơn <a class=\"Normal\" href=\"http://www.vnexpress.net/Vietnam/Oto-Xe-may/2006/02/3B9E7218/\">Murcielago LP640</a> &iacute;t nhất l&agrave; 100 kg, ở mức khoảng 1.560 kg. Một v&agrave;i nguồn tin nội bộ cho biết SV nhẹ hơn ch&iacute;nh x&aacute;c l&agrave; 130 kg. Trong đ&oacute;, 40 kg giảm bớt l&agrave; do Lamborghini thay hệ dẫn động 4 b&aacute;nh th&agrave;nh dẫn động 2 b&aacute;nh. </p>\r\n<table cellspacing=\"0\" cellpadding=\"3\" width=\"1\" align=\"center\" border=\"0\">\r\n    <tbody>\r\n        <tr>\r\n            <td><img height=\"253\" alt=\"Lamborghini SV, bản đồ họa của trang Autoblog. Ảnh: Autoblog.\" width=\"380\" border=\"1\" src=\"http://www.vnexpress.net/Vietnam/Oto-Xe-may/2007/11/3B9FC37C/Lam.jpg\" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"Image\">Lamborghini SV, bản đồ họa của trang <em designtimesp=\"17536\">Autoblog</em>. Ảnh: <em designtimesp=\"17537\">Autoblog.</em></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<p class=\"Normal\">Murcielago SV được cải tiến theo c&aacute;ch tương tự như phi&ecirc;n bản Superleggera. Những nơi c&aacute;c kỹ sư tập trung nhiều nhất l&agrave; nội thất, ngoại nhất v&agrave; c&aacute;c th&agrave;nh phần cơ học.</p>\r\n<p class=\"Normal\">Nội thất v&agrave; phần th&acirc;n l&agrave; nơi được quan t&acirc;m đầu ti&ecirc;n, sau đ&oacute; đến bảng điều khiển trung t&acirc;m, cửa v&agrave; ghế. Da bọc cửa được thay bằng tấm carbon đ&uacute;c gi&uacute;p SV tiết kiệm 2 kg mỗi b&ecirc;n. Lamborghini bỏ d&agrave;n &acirc;m thanh Kenwood từ dạng ti&ecirc;u chuẩn sang thiết bị t&ugrave;y chọn. C&oacute; nghĩa l&agrave; kh&aacute;ch h&agrave;ng phải trả th&ecirc;m tiền nếu muốn lắp.</p>\r\n<p class=\"Normal\">V&agrave;nh đ&uacute;c hợp kim tr&ecirc;n LP640 cũng được thay bằng loại mới với trọng lượng giảm &iacute;t nhất 6 kg mỗi chiếc. V&agrave;nh ph&iacute;a sau tiết kiệm được nhiều hơn do lớn hơn.</p>\r\n<p class=\"Normal\">Murcielago SV sử dụng hệ dẫn động E-Gear nhẹ hơn 2 kg. C&ocirc;ng cuộc &quot;giảm c&acirc;n&quot; được Lamborghini thực hiện kh&aacute; quyết liệt khi thay nh&ocirc;m bằng ma-gi&ecirc; ở một số thiết bị. Những nơi kh&ocirc;ng chịu nhiều &aacute;p lực cũng được chế t&aacute;c bằng vật liệu nhẹ.</p>\r\n<p class=\"Normal\">Vừa giảm c&acirc;n, Lamborghini vừa n&acirc;ng cao c&ocirc;ng suất của SV. <em designtimesp=\"17544\">Autocar</em> cho biết động cơ 6,5 l&iacute;t V12 của SV sinh ra c&ocirc;ng suất 680 m&atilde; lực, cao hơn Murcielago v&agrave; Murcielago LP640. M&ocirc;-men xoắn cực đại của SV l&agrave; 700 Nm, hơn LP640 khoảng 40 Nm.</p>\r\n<p class=\"Normal\">Như vậy, c&aacute;c giới h&acirc;m mộ Lamborghini c&oacute; thể biết trước t&iacute;nh năng vận h&agrave;nh của SV. Khả năng tăng tốc chắc chắn sẽ cao LP640 nhưng so về độ ổn định tr&ecirc;n đường cũng như khi ở tốc độ cao, những tay l&aacute;i SV sẽ phải d&egrave; chừng với con ngựa bất kham n&agrave;y.</p>\r\n<p class=\"Normal\" align=\"right\"><strong designtimesp=\"17547\">Nguyễn Nghĩa</strong></p>',1194945179,0,1,'thumb_news_1194945178.jpeg','normal_news_1194945178.jpeg',7);
INSERT INTO news VALUES (5,1,'BMW vẫn là hãng xe hạng sang số 1 thế giới','','news_1194945251.jpg','Sự th&agrave;nh c&ocirc;ng của X5, X3 gi&uacute;p BMW giữ khoảng c&aacute;ch an to&agrave;n với hai k&igrave;nh địch đồng hương Mercedes v&agrave; Audi đang cố gắng b&aacute;m đuổi ph&iacute;a sau.','<p class=\"Normal\">Doanh số to&agrave;n cầu của BMW trong th&aacute;ng 10 tăng 13,2% so với c&ugrave;ng kỳ năm ngo&aacute;i, đạt 123.304 xe. Doanh số lũy kế 10 th&aacute;ng l&agrave; 1,2 triệu chiếc, tăng 10%.</p>\r\n<p class=\"Normal\">Trong khoảng thời 10 th&aacute;ng, lượng X5 ti&ecirc;u thụ đạt 92.770 chiếc, cao hơn tới 48% so với c&ugrave;ng kỳ 2006. Sự tăng trưởng vượt bậc n&agrave;y một phần nhờ những điểm mới tr&ecirc;n chiếc X5 2007 m&agrave; BMW tung ra hồi th&aacute;ng 8 năm ngo&aacute;i. X5 thế hệ thứ hai c&oacute; nội thất rộng hơn v&agrave; l&agrave; mẫu xe 7 chỗ đầu ti&ecirc;n của BMW. Động cơ cũng nhẹ hơn 10 kg v&agrave; th&acirc;n xe sử dụng nhiều vật liệu cao cấp.</p>\r\n<table cellspacing=\"0\" cellpadding=\"3\" width=\"1\" align=\"center\" border=\"0\">\r\n    <tbody>\r\n        <tr>\r\n            <td><img height=\"261\" alt=\"X5 2007 chính thức sản xuất từ ngày 28/9/2006. Ảnh: BMW.\" width=\"350\" border=\"1\" src=\"http://www.vnexpress.net/Vietnam/Oto-Xe-may/2007/11/3B9FC29F/X5.jpg\" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"Image\">X5 2007 ch&iacute;nh thức sản xuất từ ng&agrave;y 28/9/2006. Ảnh: <em designtimesp=\"2689\">BMW.</em></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<p class=\"Normal\">C&ugrave;ng với sự th&agrave;nh c&ocirc;ng của X5, người em X3 cũng g&oacute;p phần d&ugrave; kh&ocirc;ng lớn bằng. Doanh số 10 th&aacute;ng của X3 đạt 91.868 chiếc trong khi của năm ngo&aacute;i l&agrave; 89.159 xe. Những kết quả đ&aacute;ng mừng của X5 v&agrave; X3 khiến BMW c&agrave;ng tự tin v&agrave;o kế hoạch tung ra X6 trong năm tới.</p>\r\n<table cellspacing=\"7\" cellpadding=\"7\" width=\"196\" align=\"right\" border=\"0\">\r\n    <tbody>\r\n        <tr>\r\n            <td><a class=\"\" href=\"http://www.vnexpress.net/Vietnam/Oto-Xe-may/2007/05/3B9F6971/\">*Mercedes v&agrave; cuộc chiến với BMW</a></td>\r\n        </tr>\r\n        <tr>\r\n            <td><a class=\"\" href=\"http://www.vnexpress.net/Vietnam/Oto-Xe-may/2007/09/3B9FA253/\">*BMW X6 tr&igrave;nh diễn tại Frankfurt 2007</a></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<p class=\"Normal\">Với X6, những đặc t&iacute;nh của d&ograve;ng SUV kh&ocirc;ng r&otilde; r&agrave;ng bằng X5 nhưng b&ugrave; lại, n&oacute; sở hữu nhiều t&iacute;nh năng độc đ&aacute;o m&agrave; BMW gọi l&agrave; sự kết hợp giữa d&ograve;ng coupe (xe thể thao 2 cửa) với crossover.</p>\r\n<p class=\"Normal\">Nếu BMW tăng trưởng ấn tượng th&igrave; Mercedes đang gặt h&aacute;i th&agrave;nh c&ocirc;ng với C-class mới. Trong th&aacute;ng 10, Mercedes b&aacute;n 114.600 xe, tăng 11,8% so với c&ugrave;ng kỳ. Audi tăng 5,3%, l&ecirc;n mức 76.900 chiếc.</p>\r\n<p class=\"Normal\">Tuy nhi&ecirc;n, cả hai thương hiệu n&agrave;y vẫn c&ograve;n ở khoảng c&aacute;ch kh&aacute; xa so với BMW.</p>\r\n<p class=\"Normal\">Năm 2005, do vướng phải h&agrave;ng loạt vấn đề về sản phẩm, Mercedes đ&aacute;nh mất ngai v&agrave;ng v&agrave;o tay BMW bằng c&aacute;i c&aacute;ch m&agrave; kh&oacute; ai tưởng tượng ra trước đ&oacute;. </p>\r\n<p class=\"Normal\">H&atilde;ng xe nổi tiếng với chất lượng cao cấp phải ra lệnh thu hồi 680.000 chiếc E-class do lỗi kiểm so&aacute;t phanh điện tử th&aacute;ng 5/2004. Tồi tệ hơn, 10 th&aacute;ng sau, th&ecirc;m 1,3 triệu chiếc đến đại l&yacute; Mercedes bảo h&agrave;nh v&igrave; lỗi bơm xăng v&agrave; nh&agrave; sản xuất ki&ecirc;u h&atilde;nh n&agrave;y bỏ ra tới 600 triệu USD để sửa chữa sai lầm.</p>\r\n<p class=\"Normal\" nd=\"3\">Adam Jonas, chuy&ecirc;n gia ph&acirc;n t&iacute;ch của Morgan Stanley, Mỹ ước t&iacute;nh BMW sẽ b&aacute;n được 1,236 triệu xe trong năm nay trong khi của Mercedes l&agrave; 1,165 triệu chiếc. &quot;Mercedes kh&oacute; vượt qua BMW v&agrave;o 2007. Nhưng sang 2008 sẽ c&oacute; nhiều thay đổi&quot;, Jonas nhận định.</p>\r\n<p class=\"Normal\" align=\"right\" nd=\"3\"><strong designtimesp=\"2706\">Trọng Nghiệp</strong> (theo<em designtimesp=\"2707\"> AP, AMS</em>)</p>',1194945252,0,1,'thumb_news_1194945251.jpeg','normal_news_1194945251.jpeg',7);
INSERT INTO news VALUES (6,3,'Một người Việt bị giam nhầm ở Australia 5 năm','','news_1194945399.jpg','Một người đ&agrave;n &ocirc;ng Việt Nam đang khiếu nại l&ecirc;n ch&iacute;nh phủ Australia v&agrave; đ&ograve;i bồi thường h&agrave;ng trăm ngh&igrave;n USD v&igrave; bị giam 5 năm tại một trung t&acirc;m di tr&uacute; do nhầm lẫn.','<p class=\"Normal\" align=\"left\">C&aacute;c quan chức di tr&uacute; đ&atilde; bắt Tony Tran năm 1999 v&agrave; kh&ocirc;ng để anh n&oacute;i lời từ biệt với người vợ H&agrave;n Quốc v&agrave; cậu con trai khi đ&oacute; hai tuổi. </p>\r\n<p class=\"Normal\" align=\"left\">Tran đ&atilde; bị c&aacute;c bạn t&ugrave; tấn c&ocirc;ng trong thời gian bị giam giữ v&agrave; bị chấn thương n&atilde;o. Anh được ph&oacute;ng th&iacute;ch năm 2005 sau đợt xem x&eacute;t h&agrave;ng loạt nhầm lẫn của cơ quan di tr&uacute;. </p>\r\n<p class=\"Normal\" align=\"left\">Cố vấn di tr&uacute; cho Tran l&agrave; Libby Hogarth h&ocirc;m nay cho biết, anh đ&atilde; kiện ch&iacute;nh phủ Australia để đ&ograve;i bồi thường cho qu&atilde;ng thời gian phải chịu đựng của Tran. &quot;T&ocirc;i mong rằng số tiền đ&oacute; sẽ l&ecirc;n đến h&agrave;ng trăm ngh&igrave;n đ&ocirc;la bởi anh ấy kh&ocirc;ng những phải chia ly với cậu con trai m&agrave; c&ograve;n bị tấn c&ocirc;ng trong t&ugrave; v&agrave; vết thương ở n&atilde;o tiếp tục ảnh hưởng&quot;, Hogarth n&oacute;i. </p>\r\n<p class=\"Normal\" align=\"left\">Mọi chuyện bắt đầu khi Tran, khi đ&oacute; đ&atilde; cư tr&uacute; hợp ph&aacute;p ở Australia được 7 năm, y&ecirc;u cầu cơ quan di tr&uacute; cấp visa cho người vợ H&agrave;n Quốc của anh. C&aacute;c quan chức di tr&uacute; cho rằng visa của anh đ&atilde; hết hạn v&agrave; ngay lập tức tống giam Tran nhưng sau đ&oacute; thừa nhận rằng anh c&oacute; visa hợp ph&aacute;p từ năm 1993. </p>\r\n<p class=\"Normal\" align=\"left\">Vợ của Tran đ&atilde; về H&agrave;n Quốc để tr&aacute;nh bị bắt v&agrave; từ đ&oacute; đến nay, đ&ocirc;i vợ chồng n&agrave;y bặt tin nhau. Cậu con trai của Tran l&uacute;c đầu đi theo mẹ về H&agrave;n Quốc nhưng sau đ&oacute; được đưa trở lại Australia v&agrave; nu&ocirc;i dưỡng trong trại trẻ mồ c&ocirc;i. </p>\r\n<p class=\"Normal\" align=\"left\">Hogath cho rằng c&aacute;c quan chức di tr&uacute; đ&atilde; từ chối chuyển thư v&agrave; ảnh m&agrave; Tran gửi cho con trai v&agrave; c&aacute;o buộc đ&oacute; l&agrave; h&agrave;nh động &quot;nhẫn t&acirc;m v&agrave; xấu xa&quot;.</p>\r\n<p class=\"Normal\" align=\"left\">Ch&iacute;nh phủ Australia đ&atilde; thực hiện những ch&iacute;nh s&aacute;ch nhập cư cứng rắn nhằm ngăn chặn t&igrave;nh trạng bu&ocirc;n người v&agrave; bảo vệ bi&ecirc;n giới của nước n&agrave;y. Tuy nhi&ecirc;n, họ đ&atilde; nhẹ tay hơn v&agrave;o năm 2005 sau một loạt sai phạm trong qu&aacute; tr&igrave;nh thực hiện c&aacute;c quy định đ&oacute;. </p>\r\n<p class=\"Normal\" align=\"right\"><strong designtimesp=\"3791\">Ngọc Sơn</strong> (theo <em designtimesp=\"3792\">AFP</em>)</p>',1194945399,0,1,'thumb_news_1194945399.jpeg','',7);
#
# Table structure for table `orders`
#
DROP table IF EXISTS orders;
CREATE TABLE `orders` (
  `id_order` bigint(20) unsigned NOT NULL auto_increment,
  `session` tinytext NOT NULL,
  `time` bigint(20) unsigned NOT NULL default '0',
  `name` varchar(16) NOT NULL default '',
  `tel` varchar(16) NOT NULL default '',
  `email` varchar(16) NOT NULL default '',
  `address` tinytext NOT NULL,
  `addinfo` text,
  PRIMARY KEY  (`id_order`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
#
# Dumping data for table `orders`
#
INSERT INTO orders VALUES (8,'f38448a8112977386bbd9f585963fa72',1195179932,'Nguyễn Văn B�','091-251-2449','binh_mczk@yahoo.','Ha noi','');
#
# Table structure for table `photo`
#
DROP table IF EXISTS photo;
CREATE TABLE `photo` (
  `id_photo` bigint(20) unsigned NOT NULL auto_increment,
  `id_catpt` bigint(20) unsigned NOT NULL default '0',
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `intro` mediumtext character set latin1 collate latin1_general_ci NOT NULL,
  `ngay_dang` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  `small_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `normal_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `id_user` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id_photo`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
#
# Dumping data for table `photo`
#
INSERT INTO photo VALUES (1,1,'Ô tô đẹp','photo_1195026578.jpg','',1195026578,0,1,'thumb_photo_1195026578.jpeg','',7);
INSERT INTO photo VALUES (2,1,'Lixus','photo_1195026729.jpg','',1195026730,0,1,'thumb_photo_1195026729.jpeg','normal_photo_1195026729.jpeg',7);
INSERT INTO photo VALUES (3,1,'csa;lj','photo_1195026752.jpg','',1195026752,0,1,'thumb_photo_1195026752.jpeg','normal_photo_1195026752.jpeg',7);
INSERT INTO photo VALUES (4,2,'01','photo_1195089317.jpg','',1195089318,0,1,'thumb_photo_1195089317.jpeg','normal_photo_1195089317.jpeg',7);
INSERT INTO photo VALUES (5,2,'02','photo_1195089324.jpg','',1195089325,0,1,'thumb_photo_1195089324.jpeg','normal_photo_1195089324.jpeg',7);
INSERT INTO photo VALUES (6,2,'03','photo_1195089331.jpg','',1195089331,0,1,'thumb_photo_1195089331.jpeg','normal_photo_1195089331.jpeg',7);
INSERT INTO photo VALUES (7,2,'04','photo_1195089339.jpg','',1195089340,0,1,'thumb_photo_1195089339.jpeg','normal_photo_1195089339.jpeg',7);
INSERT INTO photo VALUES (8,2,'05','photo_1195089347.jpg','',1195089347,0,1,'thumb_photo_1195089347.jpeg','normal_photo_1195089347.jpeg',7);
INSERT INTO photo VALUES (9,2,'06','photo_1195089354.jpg','',1195089354,0,1,'thumb_photo_1195089354.jpeg','normal_photo_1195089354.jpeg',7);
INSERT INTO photo VALUES (10,2,'07','photo_1195089364.jpg','',1195089364,0,1,'thumb_photo_1195089364.jpeg','normal_photo_1195089364.jpeg',7);
INSERT INTO photo VALUES (11,2,'08','photo_1195089371.jpg','',1195089371,0,1,'thumb_photo_1195089371.jpeg','normal_photo_1195089371.jpeg',7);
INSERT INTO photo VALUES (12,2,'09','photo_1195089378.jpg','',1195089378,0,1,'thumb_photo_1195089378.jpeg','normal_photo_1195089378.jpeg',7);
INSERT INTO photo VALUES (13,2,'10','photo_1195089831.jpg','',1195089567,0,1,'thumb_photo_1195089831.jpeg','normal_photo_1195089831.jpeg',7);
INSERT INTO photo VALUES (14,2,'11','photo_1195089824.jpg','',1195089574,0,1,'thumb_photo_1195089824.jpeg','normal_photo_1195089824.jpeg',7);
INSERT INTO photo VALUES (15,2,'12','photo_1195089818.jpg','',1195089583,0,1,'thumb_photo_1195089818.jpeg','normal_photo_1195089818.jpeg',7);
INSERT INTO photo VALUES (16,2,'13','photo_1195089811.jpg','',1195089588,0,1,'thumb_photo_1195089811.jpeg','normal_photo_1195089811.jpeg',7);
INSERT INTO photo VALUES (17,2,'14','photo_1195089805.jpg','',1195089594,0,1,'thumb_photo_1195089805.jpeg','normal_photo_1195089805.jpeg',7);
INSERT INTO photo VALUES (18,2,'15','photo_1195089798.jpg','',1195089598,0,1,'thumb_photo_1195089798.jpeg','normal_photo_1195089798.jpeg',7);
INSERT INTO photo VALUES (19,2,'16','photo_1195089790.jpg','',1195089607,0,1,'thumb_photo_1195089790.jpeg','normal_photo_1195089790.jpeg',7);
INSERT INTO photo VALUES (25,2,'22','photo_1195090270.jpg','',1195090270,0,1,'thumb_photo_1195090270.jpeg','normal_photo_1195090270.jpeg',7);
INSERT INTO photo VALUES (21,2,'18','photo_1195089782.jpg','',1195089622,0,1,'thumb_photo_1195089782.jpeg','normal_photo_1195089782.jpeg',7);
INSERT INTO photo VALUES (22,2,'19','photo_1195089774.jpg','',1195089629,0,1,'thumb_photo_1195089774.jpeg','normal_photo_1195089774.jpeg',7);
INSERT INTO photo VALUES (23,2,'20','photo_1195089765.jpg','',1195089637,0,1,'thumb_photo_1195089765.jpeg','normal_photo_1195089765.jpeg',7);
INSERT INTO photo VALUES (24,2,'21','photo_1195089750.jpg','',1195089643,0,1,'thumb_photo_1195089750.jpeg','normal_photo_1195089750.jpeg',7);
INSERT INTO photo VALUES (26,2,'23','photo_1195090277.jpg','',1195090278,0,1,'thumb_photo_1195090277.jpeg','normal_photo_1195090277.jpeg',7);
INSERT INTO photo VALUES (27,2,'24','photo_1195090282.jpg','',1195090283,0,1,'thumb_photo_1195090282.jpeg','normal_photo_1195090282.jpeg',7);
INSERT INTO photo VALUES (28,2,'25','photo_1195090288.jpg','',1195090288,0,1,'thumb_photo_1195090288.jpeg','normal_photo_1195090288.jpeg',7);
INSERT INTO photo VALUES (29,2,'26','photo_1195090294.jpg','',1195090295,0,1,'thumb_photo_1195090294.jpeg','normal_photo_1195090294.jpeg',7);
INSERT INTO photo VALUES (30,2,'27','photo_1195090301.jpg','',1195090301,0,1,'thumb_photo_1195090301.jpeg','normal_photo_1195090301.jpeg',7);
INSERT INTO photo VALUES (31,2,'28','photo_1195090310.jpg','',1195090311,0,1,'thumb_photo_1195090310.jpeg','normal_photo_1195090310.jpeg',7);
INSERT INTO photo VALUES (32,2,'29','photo_1195090318.jpg','',1195090318,0,1,'thumb_photo_1195090318.jpeg','normal_photo_1195090318.jpeg',7);
INSERT INTO photo VALUES (33,2,'30','photo_1195090326.jpg','',1195090327,0,1,'thumb_photo_1195090326.jpeg','normal_photo_1195090326.jpeg',7);
INSERT INTO photo VALUES (34,2,'31','photo_1195090332.jpg','',1195090333,0,1,'thumb_photo_1195090332.jpeg','normal_photo_1195090332.jpeg',7);
INSERT INTO photo VALUES (35,2,'32','photo_1195090340.jpg','',1195090340,0,1,'thumb_photo_1195090340.jpeg','normal_photo_1195090340.jpeg',7);
INSERT INTO photo VALUES (36,2,'33','photo_1195090348.jpg','',1195090349,0,1,'thumb_photo_1195090348.jpeg','normal_photo_1195090348.jpeg',7);
#
# Table structure for table `poll_answer`
#
DROP table IF EXISTS poll_answer;
CREATE TABLE `poll_answer` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `answer` text NOT NULL,
  `image` varchar(255) NOT NULL default '',
  `votes` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
#
# Dumping data for table `poll_answer`
#
INSERT INTO poll_answer VALUES (43,'Qu� x?u','gfx/grey.gif',1);
INSERT INTO poll_answer VALUES (42,'C� nhi?u ng??i mua','gfx/aqua.gif',10);
INSERT INTO poll_answer VALUES (40,'Ki?u d�ng ??p','gfx/blue.gif',16);
INSERT INTO poll_answer VALUES (41,'Gi� r?','gfx/green.gif',51);
#
# Table structure for table `poll_ip`
#
DROP table IF EXISTS poll_ip;
CREATE TABLE `poll_ip` (
  `ip` varchar(50) NOT NULL default '',
  `time` bigint(20) unsigned NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
#
# Dumping data for table `poll_ip`
#
INSERT INTO poll_ip VALUES ('127.0.0.1',1041441880);
INSERT INTO poll_ip VALUES ('127.0.0.1',1041441884);
INSERT INTO poll_ip VALUES ('127.0.0.1',1041441910);
INSERT INTO poll_ip VALUES ('127.0.0.1',1041442518);
INSERT INTO poll_ip VALUES ('127.0.0.1',1041442547);
INSERT INTO poll_ip VALUES ('127.0.0.1',1041442626);
INSERT INTO poll_ip VALUES ('127.0.0.1',1041442679);
INSERT INTO poll_ip VALUES ('127.0.0.1',1041442683);
INSERT INTO poll_ip VALUES ('192.168.0.40',1041447771);
INSERT INTO poll_ip VALUES ('127.0.0.1',1193363393);
INSERT INTO poll_ip VALUES ('192.168.1.19',1193371767);
#
# Table structure for table `poll_question`
#
DROP table IF EXISTS poll_question;
CREATE TABLE `poll_question` (
  `question` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
#
# Dumping data for table `poll_question`
#
INSERT INTO poll_question VALUES ('B?n th?y s?n ph?m c?a ch�ng t�i?');
#
# Table structure for table `product`
#
DROP table IF EXISTS product;
CREATE TABLE `product` (
  `id_product` bigint(20) unsigned NOT NULL auto_increment,
  `id_catpd` bigint(20) unsigned NOT NULL default '0',
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `price` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `don_vi` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `intro` mediumtext character set latin1 collate latin1_general_ci NOT NULL,
  `content` longtext character set latin1 collate latin1_general_ci NOT NULL,
  `ngay_dang` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  `small_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `normal_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `id_user` tinyint(3) unsigned NOT NULL default '0',
  `moi` tinyint(4) NOT NULL default '0',
  `sologan` text NOT NULL,
  PRIMARY KEY  (`id_product`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
#
# Dumping data for table `product`
#
INSERT INTO product VALUES (1,6,'Kia New Morning','product_1195014175.jpg','','','<font face=\"Arial\">\r\n<table cellspacing=\"2\" cellpadding=\"2\" width=\"100%\">\r\n    <tbody>\r\n        <tr>\r\n            <td class=\"link_title\" width=\"30%\">M&atilde; xe</td>\r\n            <td class=\"link_title\" width=\"70%\">HGB-SEC-08</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"content\" width=\"30%\">Số km</td>\r\n            <td class=\"content\">6,968 miles&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"content\" width=\"30%\">Đời xe</td>\r\n            <td class=\"content\">2007</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"content\" width=\"30%\">Xuất xứ</td>\r\n            <td class=\"content\">Mỹ</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"content\" width=\"30%\">Nơi b&aacute;n</td>\r\n            <td class=\"content\">H&agrave; Nội</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"content\" width=\"30%\">T&igrave;nh trạng</td>\r\n            <td class=\"content\">Xe c&oacute; tại Showroom (KH đ&atilde; đặt)</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n</font>','<font face=\"Arial\"><font face=\"Arial\"><font face=\"Arial\">\r\n<table cellspacing=\"8\" cellpadding=\"0\" width=\"100%\" border=\"0\">\r\n    <tbody>\r\n        <tr>\r\n            <td class=\"link_title\" colspan=\"3\">Th&ocirc;ng số cơ bản</td>\r\n        </tr>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"19%\">&nbsp;</td>\r\n            <td class=\"content\" width=\"30%\">H&atilde;ng xe</td>\r\n            <td class=\"content\" width=\"65%\">BMW</td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"19%\">&nbsp;</td>\r\n            <td class=\"content\" width=\"30%\">Mẫu xe</td>\r\n            <td class=\"content\" width=\"65%\">X5</td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"10%\">&nbsp;</td>\r\n            <td class=\"content\" nowrap=\"nowrap\" width=\"30%\">Năm sxuất</td>\r\n            <td class=\"content\" width=\"55%\">2006</td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"10%\">&nbsp;</td>\r\n            <td class=\"content\" nowrap=\"nowrap\" width=\"30%\">M&agrave;u ngoại thất</td>\r\n            <td class=\"content\" width=\"55%\">M&agrave;u bạc</td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"10%\">&nbsp;</td>\r\n            <td class=\"content\" nowrap=\"nowrap\" width=\"30%\">M&agrave;u nội thất</td>\r\n            <td class=\"content\" width=\"55%\">M&agrave;u ghi s&aacute;ng</td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"10%\">&nbsp;</td>\r\n            <td class=\"content\" nowrap=\"nowrap\" width=\"30%\">Loại nhi&ecirc;n liệu</td>\r\n            <td class=\"content\" width=\"55%\">Xăng</td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"10%\">&nbsp;</td>\r\n            <td class=\"content\" nowrap=\"nowrap\" width=\"30%\">Số chỗ ngồi</td>\r\n            <td class=\"content\" width=\"55%\">7 chỗ</td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"10%\">&nbsp;</td>\r\n            <td class=\"content\" nowrap=\"nowrap\" width=\"30%\">Dung t&iacute;ch xi lanh</td>\r\n            <td class=\"content\" width=\"55%\">3.0 L</td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"10%\">&nbsp;</td>\r\n            <td class=\"content\" nowrap=\"nowrap\" width=\"30%\">Hộp số</td>\r\n            <td class=\"content\" width=\"55%\">Số tự động</td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"10%\">&nbsp;</td>\r\n            <td class=\"content\" nowrap=\"nowrap\" width=\"30%\">La răng</td>\r\n            <td class=\"content\" width=\"55%\">255/55 R18</td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"10%\">&nbsp;</td>\r\n            <td class=\"content\" nowrap=\"nowrap\" width=\"30%\">Số VIN</td>\r\n            <td class=\"content\" width=\"55%\">4USFE43567LY76293</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n</font></font></font>',1194917367,0,1,'thumb_product_1195014175.jpeg','normal_product_1195014175.jpeg',7,1,'Trường tồn với thời gian');
INSERT INTO product VALUES (2,6,'Innova','product_1195003493.gif','','','fd sfsdfsd fsd fsd fsdf sdfsdf','df sdfsd',1194919298,0,1,'thumb_product_1195003493.gif','normal_product_1195003493.gif',7,1,'Lướt cùng thời đại');
INSERT INTO product VALUES (3,6,'Messedes Ben','product_1195014145.jpg','','','<table cellspacing=\"2\" cellpadding=\"2\" width=\"100%\">\r\n    <tbody>\r\n        <tr>\r\n            <td class=\"link_title\" width=\"30%\">M&atilde; xe</td>\r\n            <td class=\"link_title\" width=\"70%\">HGB-SEC-08</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"content\" width=\"30%\">Số km</td>\r\n            <td class=\"content\">6,968 miles&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"content\" width=\"30%\">Đời xe</td>\r\n            <td class=\"content\">2007</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"content\" width=\"30%\">Xuất xứ</td>\r\n            <td class=\"content\">Mỹ</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"content\" width=\"30%\">Nơi b&aacute;n</td>\r\n            <td class=\"content\">H&agrave; Nội</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"content\" width=\"30%\">T&igrave;nh trạng</td>\r\n            <td class=\"content\">Xe c&oacute; tại Showroom (KH đ&atilde; đặt)</td>\r\n        </tr>\r\n    </tbody>\r\n</table>','df sdfsd',1194919310,0,1,'thumb_product_1195014145.jpeg','',7,1,'Đỉnh cao của chất lượng');
INSERT INTO product VALUES (5,7,'ád fsđfs','product_1195023222.jpg','','USD','df sdf','df sd',1195023222,0,1,'thumb_product_1195023222.jpeg','',7,1,'Đi tán gái rất hợp');
INSERT INTO product VALUES (6,7,'123445','product_1195027790.jpg','','USD','','',1195027790,0,1,'thumb_product_1195027790.jpeg','',7,1,'');
#
# Table structure for table `settings`
#
DROP table IF EXISTS settings;
CREATE TABLE `settings` (
  `setting_name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `setting_value` mediumtext character set latin1 collate latin1_general_ci NOT NULL,
  PRIMARY KEY  (`setting_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
#
# Dumping data for table `settings`
#
INSERT INTO settings VALUES ('site_name','Kylin');
INSERT INTO settings VALUES ('dir_path','/kylin');
INSERT INTO settings VALUES ('site_email','binh_mczk@yahoo.com');
INSERT INTO settings VALUES ('use_smtp','0');
INSERT INTO settings VALUES ('smtp_host','');
INSERT INTO settings VALUES ('smtp_username','');
INSERT INTO settings VALUES ('smtp_password','');
INSERT INTO settings VALUES ('template_dir','default');
INSERT INTO settings VALUES ('language_dir','english');
INSERT INTO settings VALUES ('date_format','d/m/Y');
INSERT INTO settings VALUES ('time_format','H:i');
INSERT INTO settings VALUES ('convert_tool','gd');
INSERT INTO settings VALUES ('convert_tool_path','');
INSERT INTO settings VALUES ('gz_compress','1');
INSERT INTO settings VALUES ('gz_compress_level','');
INSERT INTO settings VALUES ('upload_mode','2');
INSERT INTO settings VALUES ('allowed_mediatypes','jpg,gif,png,bmp,aif,au,avi,mid,mov,mp3,mpg,swf,wav,rar,ra,rm,zip,pdf,txt,xls,doc,swf');
INSERT INTO settings VALUES ('max_thumb_width','200');
INSERT INTO settings VALUES ('max_thumb_height','300');
INSERT INTO settings VALUES ('max_image_height','1024');
INSERT INTO settings VALUES ('max_media_size','5000');
INSERT INTO settings VALUES ('upload_notify','0');
INSERT INTO settings VALUES ('upload_emails','');
INSERT INTO settings VALUES ('auto_thumbnail','1');
INSERT INTO settings VALUES ('auto_thumbnail_dimension','135');
INSERT INTO settings VALUES ('auto_thumbnail_resize_type','1');
INSERT INTO settings VALUES ('auto_thumbnail_quality','100');
INSERT INTO settings VALUES ('id_country','207');
INSERT INTO settings VALUES ('paging_range','5');
INSERT INTO settings VALUES ('watermark_text','');
INSERT INTO settings VALUES ('upload_media_path','upload/medias/');
INSERT INTO settings VALUES ('upload_image_path','upload/images/');
INSERT INTO settings VALUES ('session_timeout','15');
INSERT INTO settings VALUES ('max_image_width','200');
INSERT INTO settings VALUES ('time_offset','0');
INSERT INTO settings VALUES ('http_host','localhost:801');
INSERT INTO settings VALUES ('document_root','D:/AppServ/www');
INSERT INTO settings VALUES ('site_keywords','Nguyen Binh');
INSERT INTO settings VALUES ('site_description','Ha Noi');
INSERT INTO settings VALUES ('bao_gia','upload/medias/bao_gia_1154146521.xls');
INSERT INTO settings VALUES ('diachi_cty','<font color=\"#ff9900\" size=\"1\"><strong><font color=\"#8a8587\">C&Ocirc;NG TY CỔ PHẦN THƯƠNG MẠI KYLIN-GX668<br />\r\nĐC: Số 8 Đường v&agrave;nh đai I - Kim Li&ecirc;n - &Ocirc; Chợ Dừa - Đống Đa - H&agrave; Nội<br />\r\nĐT: 04. 573 7743&nbsp; -&nbsp; FAX: 04. 573 7745<br />\r\nEmail: kylin_gx668@yahoo.com&nbsp; -&nbsp; Website: kylingx668.com.vn</font></strong></font><span class=\"trang\"></span>');
INSERT INTO settings VALUES ('active_site','1');
INSERT INTO settings VALUES ('content_active_site','<p><span class=\"style1\"><font color=\"#ff0000\">Th<font face=\"Arial\">&ocirc;ng b<font face=\"Arial\">&aacute;o khi <font face=\"Arial\">web ng<font face=\"Arial\">?ng ho<font face=\"Arial\">?t <font face=\"Arial\">?<font face=\"Arial\">?ng</font></font></font></font></font></font></font></font></span></p>');
INSERT INTO settings VALUES ('sl_image','10');
#
# Table structure for table `shortcut`
#
DROP table IF EXISTS shortcut;
CREATE TABLE `shortcut` (
  `id_shortcut` tinyint(4) NOT NULL auto_increment,
  `title` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `link` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `active` tinyint(4) NOT NULL default '0',
  `thu_tu` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id_shortcut`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
#
# Dumping data for table `shortcut`
#
INSERT INTO shortcut VALUES (2,'S&#7843;n ph&#7849;m','shortcut_1184358087.gif','?act=catpd&code=00',1,5);
INSERT INTO shortcut VALUES (1,'Th&#244;ng tin li&#234;n h&#7879;','shortcut_1184358104.png','?act=contact&code=00',1,7);
INSERT INTO shortcut VALUES (8,'Tin t&#7913;c','shortcut_1184358095.gif','?act=cat&code=00',1,6);
INSERT INTO shortcut VALUES (9,'Database','shortcut_1184358064.gif','?act=dbadmin',1,2);
INSERT INTO shortcut VALUES (10,'Th&#244;ng tin c&#225; nh&#226;n','shortcut_1184358071.png','?act=myadmin',1,4);
INSERT INTO shortcut VALUES (11,'QL ng&#432;&#7901;i s&#7917; d&#7909;ng','shortcut_1184358078.png','?act=users&code=00',1,3);
INSERT INTO shortcut VALUES (12,'C&#7845;u h&#236;nh h&#7879; th&#7889;ng','shortcut_1184358054.gif','?act=settings&code=00',0,1);
INSERT INTO shortcut VALUES (14,'Logo qu?ng c�o','shortcut_1184358118.gif','?act=catlg&code=00',1,8);
INSERT INTO shortcut VALUES (15,'Trang n?i dung','shortcut_1184358149.gif','?act=catif&code=00',1,10);
INSERT INTO shortcut VALUES (17,'Qu&#7843;n l&#253; n&#7897;i dung','shortcut_1184803969.png','?act=catif&code=00',1,11);
#
# Table structure for table `user_module`
#
DROP table IF EXISTS user_module;
CREATE TABLE `user_module` (
  `id_user_module` bigint(20) unsigned NOT NULL auto_increment,
  `id_user` bigint(20) unsigned NOT NULL default '0',
  `id_module` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id_user_module`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
#
# Dumping data for table `user_module`
#
#
# Table structure for table `user_post`
#
DROP table IF EXISTS user_post;
CREATE TABLE `user_post` (
  `id_user` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `username` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `password` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `email` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `telephone` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `lastvisit` bigint(20) unsigned NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '0',
  `address` text character set latin1 collate latin1_general_ci NOT NULL,
  `other` text character set latin1 collate latin1_general_ci NOT NULL,
  `area` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  PRIMARY KEY  (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
#
# Dumping data for table `user_post`
#
INSERT INTO user_post VALUES (3,'Nguyễn Văn Bình','admin','21232f297a57a5a743894a0e4a801fc3','binh_mczk@yahoo.com','091-251-2449',0,1,'Ha noi','khong co gi','0');
INSERT INTO user_post VALUES (4,'nguyen van binh','nguyenbinh','a745715b2ade66190a65e1710b7830c7','suongmumc@yahoo.com','091-251-2449',0,1,'cau giay ha noi','chang co thong tin gi them','0');
#
# Table structure for table `useronline`
#
DROP table IF EXISTS useronline;
CREATE TABLE `useronline` (
  `id` int(10) NOT NULL auto_increment,
  `ip` varchar(15) NOT NULL default '',
  `timestamp` varchar(15) NOT NULL default '',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8755 DEFAULT CHARSET=utf8;
#
# Dumping data for table `useronline`
#
INSERT INTO useronline VALUES (8746,'192.168.1.18','1195181487');
INSERT INTO useronline VALUES (8750,'192.168.1.18','1195181655');
INSERT INTO useronline VALUES (8751,'192.168.1.18','1195181684');
INSERT INTO useronline VALUES (8752,'192.168.1.18','1195181719');
INSERT INTO useronline VALUES (8753,'192.168.1.18','1195181730');
INSERT INTO useronline VALUES (8754,'192.168.1.18','1195181876');
INSERT INTO useronline VALUES (8745,'192.168.1.18','1195181426');
INSERT INTO useronline VALUES (8749,'192.168.1.18','1195181602');
INSERT INTO useronline VALUES (8748,'192.168.1.18','1195181576');
INSERT INTO useronline VALUES (8743,'192.168.1.19','1195181334');
INSERT INTO useronline VALUES (8744,'192.168.1.18','1195181385');
INSERT INTO useronline VALUES (8747,'192.168.1.18','1195181498');
#
# Table structure for table `users`
#
DROP table IF EXISTS users;
CREATE TABLE `users` (
  `id_users` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `username` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `password` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `email` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `telephone` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `lastvisit` bigint(20) unsigned NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '0',
  `super` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id_users`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
#
# Dumping data for table `users`
#
INSERT INTO users VALUES (7,'Nguy&#7877;n V&#259;n B&#236;nh','admin','21232f297a57a5a743894a0e4a801fc3','binh_mczk@yahoo.com','0912512449',1195179969,1,1);
#
# Table structure for table `yahoo`
#
DROP table IF EXISTS yahoo;
CREATE TABLE `yahoo` (
  `id_yahoo` tinyint(4) NOT NULL auto_increment,
  `nic` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `thu_tu` tinyint(4) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '0',
  `sky` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id_yahoo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
#
# Dumping data for table `yahoo`
#
