<?php
session_name( 'NVBadmin' );
session_start();
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
//error_reporting (E_ALL); //report all error and notice

define('_VALID_NVB','1');
defined( '_VALID_NVB' ) or die( 'Direct Access to this location is not allowed.' );
// Disable DOS Attacks
if (($_SERVER['HTTP_USER_AGENT'] == '' || $_SERVER['HTTP_USER_AGENT'] == '-') && !defined('XMLFEED')) {
    exit;
}

$start_time = get_microtime();
set_magic_quotes_runtime(0); // Disable magic_quotes_runtime
umask(0);

define('CAN_MOD_INI', !ereg('ini_set', ini_get('disable_functions')));

// Remove GET/POST/Cookie variables from the global scope
if (intval(ini_get('register_globals')) != 0) {
    // since IIS can't turn off register_globals as Apache thru .ht* we destroy them here
    foreach ($_REQUEST AS $key => $val) {
        if (isset($$key)) unset($$key);
//        $_REQUEST[$key] = $val;
    }
}
function StripAllSlashes(&$value, $key) {
    if (is_array($value)) array_walk($value, 'StripAllSlashes');
    else $value = stripslashes($value);
    $_REQUEST[$key] =& $value;
}
if (get_magic_quotes_gpc() || ini_get('magic_quotes_sybase')) {
    if (is_array($_GET) )   { array_walk($_GET,    'StripAllSlashes'); }
    if (is_array($_POST))   { array_walk($_POST,   'StripAllSlashes'); }
    if (is_array($_COOKIE)) { array_walk($_COOKIE, 'StripAllSlashes'); }
}

if (!defined('BASEDIR')) {
    $root_path = dirname(dirname(__FILE__));
    if (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') {
        $root_path = str_replace('\\', '/', $root_path); //Damn' windows
    }
    if (strlen($root_path) > 2) define('BASEDIR', $root_path.'/');
    else define('BASEDIR', '');
}
//if (CAN_MOD_INI) ini_set('include_path', BASEDIR);
$phpver = explode('.', phpversion());
$phpver = "$phpver[0]$phpver[1]";
define('PHPVERS', $phpver);


ini_set('magic_quotes_sybase', 0);


ob_start();
//include some files
require("conf.php");
require( "lib/link_pass.php" );
require( "lib/mySQL.php" );
require( "lib/topost.php" );
require( "lib/functions.php" );
require( "lib/class.Template.inc.php" );
//Database
$DB = new db_driver;
//Database
$DB->obj['sql_database']     = $config['dbname'];
//Username
$DB->obj['sql_user']         = $config['username'];
//Password
$DB->obj['sql_pass']         = $config['password'];
//Host. VD: localhost
$DB->obj['sql_host']         = $config['host'];
// Get a DB connection
$DB->connect();

//Load settings
	$CONFIG=array();
	$sql="select * from settings";
	$a=$DB->query($sql);
	while ($b=mysql_fetch_array($a))
	{
		$CONFIG[$b['setting_name']]=$b['setting_value'];
	}
	$CONFIG['allowed_mediatypes'] = str_replace(" ", "", $CONFIG['allowed_mediatypes']);
	$CONFIG['allowed_mediatypes_array'] = explode(",", $CONFIG['allowed_mediatypes']);
	$CONFIG['allowed_mediatypes_match'] = str_replace(",", "|", $CONFIG['allowed_mediatypes']);	
	$CONFIG['time_offset']=intval($CONFIG['time_offset'])*60*60;
	if (!$CONFIG['root_path'])
	{
		$CONFIG['root_path']=$_SERVER['DOCUMENT_ROOT']."/";
	}
	$CONFIG['root_path_dev']=substr($CONFIG['root_path'],0,-1);
	$sql="select * from country where id_country=".intval($CONFIG['id_country']);
	if ($country=mysql_fetch_array($DB->query($sql)))
	{
		$CONFIG['currency']=$country['currency'];
	}
//dat active=1 vao bien de don gian 
$sqlactive="active=1";
//load modules


if (PHPVERS >= 41) { $PHP_SELF = $_SERVER['PHP_SELF']; }


function get_microtime() {
    list($usec, $sec) = explode(' ', microtime());
    return ($usec + $sec);
}

// Function which removes \015\012 which causes linebreaks in SMTP email
function removecrlf($str) {
    return strtr($str, "\015\012", ' ');
}


// must start the session before we create the mainframe object
if ($_SESSION["session_username"] && $_SESSION["session_usertype"] && $_SESSION["session_user_id"] && $_SESSION["session_logintime"])
{
//START

	$username=clean_value($_SESSION["session_username"]);
	$id_users=intval($_SESSION["session_user_id"]);
	$super=0;
	if ($_SESSION["session_usertype"]=='super')
	{
		$super=1;
	}
	if ($_SESSION["session_usertype"]=='normal')
	{
		$super=0;
	}
	
	$sql="select * from users where id_users=".$id_users." and username='".$username."' and super=".$super;
	$uu=$DB->query($sql);
	if ($vv=mysql_fetch_array($uu))
	{
	
		$my['username']=$_SESSION["session_username"];
		$my['id']=$_SESSION["session_user_id"];
		$my['usertype']=$_SESSION["session_usertype"];
		$my['session_logintime']=$_SESSION["session_logintime"];
		$my['name']=$vv['name'];
		
		//check permission
		$modules_permission=array();
		if ($_SESSION["session_usertype"]=='super')
		{
			$sql="select * from module";
			$a=$DB->query($sql);
			$i=0;
			while ($b=mysql_fetch_array($a))
			{
				$modules_permission[$i]=$b['gia_tri'];
				$i++;
			}
			$modules_permission[$i]='source/users.php';
			$i++;
			$modules_permission[$i]='source/myadmin.php';
			$i++;
			
		}
		if ($_SESSION["session_usertype"]=='normal')
		{
			$sql="select m.gia_tri from user_module as um inner join module as m on (um.id_module=m.id_module) where um.id_user=".intval($_SESSION["session_user_id"]);
			$a=$DB->query($sql);
			$i=0;
			while ($b=mysql_fetch_array($a))
			{
				$modules_permission[$i]=$b['gia_tri'];
				$i++;
			}
			$modules_permission[$i]='source/myadmin.php';
			$i++;
			
		}
		
		require "skin/skin.php";
		
		//show_admin_header();	
		$act=$_GET['act'];
		if (!$act)
		{
			include('source/welcome.php');
		}
		else
		{
			if (@$act=='logout')
			{
				session_unset();
				session_destroy();
				redir("admin.php");
			}
			else
			{	
				$path="$act.php";
				//xac dinh quyen truy cap voi tung module
				if (in_array($path,$modules_permission))
				{	
					if (file_exists("source/".$path)) 
					{
						include("source/".$path);
					}
				}
				else
				{
					show_message("B&#7841;n kh&#244;ng &#273;&#7911; th&#7849;m quy&#7873;n &#273;&#7875; truy nh&#7853;p v&#224;o ph&#7847;n n&#224;y !<br>H&#227;y li&#234;n h&#7879; v&#7899;i ng&#432;&#7901;i qu&#7843;n tr&#7883; !");
				}
			}
		}	
		//show_admin_footer();
	}
	else
	{
		session_unset();
		@session_destroy();
		redir("admin.php");
	}
}
else
{
	session_unset();
 	@session_destroy();
   	redir("");
}
mysql_close();
?>