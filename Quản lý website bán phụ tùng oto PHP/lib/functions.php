<?php
defined( '_VALID_NVB' ) or die( 'Direct Access to this location is not allowed.' );
function referbox($url,$msg,$s=3)
{ 
	header("Location: index3.php?url=".urlencode($url)."&msg=".urlencode($msg)."&s=$s"); 
}
//Check file
function is_remote($file_name) {
  return strpos($file_name, '://') > 0 ? 1 : 0;
}

function is_remote_file($file_name) {
  return is_remote($file_name) && preg_match("#\.[a-zA-Z0-9]{1,4}$#", $file_name) ? 1 : 0;
}

function is_local_file($file_name) {
  return !is_remote($file_name) && strpos($file_name, '/') !== false && preg_match("#\.[a-zA-Z0-9]{1,4}$#", $file_name) ? 1 : 0;
}

function check_remote_media($remote_media_file) {
  global $config;
  return is_remote($remote_media_file) && preg_match("#\.[".$CONFIG['allowed_mediatypes_match']."]+$#i", $remote_media_file) ? 1 : 0;
}
function check_local_media($local_media_file) {
  global $config;
  return !is_remote($local_media_file) && strpos($local_media_file, '/') !== false && preg_match("#\.[".$CONFIG['allowed_mediatypes_match']."]+$#i", $local_media_file) ? 1 : 0;
}

function check_remote_thumb($remote_thumb_file) {
  return is_remote($remote_thumb_file) && preg_match("#\.[gif|jpg|jpeg|png|bmp]+$#is", $remote_thumb_file) ? 1 : 0;
}

function check_local_thumb($remote_thumb_file) {
  return !is_remote($local_thumb_file) && strpos($local_thumb_file, '/') !== false && preg_match("#\.[gif|jpg|jpeg|png]+$#i", $local_thumb_file) ? 1 : 0;
}

function get_file_extension($file_name) {
  ereg("(.+)\.(.+)", basename($file_name), $regs);
  return strtolower($regs[2]);
}

function get_file_name($file_name) {
  ereg("(.+)\.(.+)", basename($file_name), $regs);
  return $regs[1];
}

function check_media_type($file_name) {
  global $config;
  return (in_array(get_file_extension($file_name), $CONFIG['allowed_mediatypes_array'])) ? 1 : 0;
}

function check_thumb_type($file_name) {
  return (preg_match("#(gif|jpg|jpeg|png)$#is", $file_name)) ? 1 : 0;
}

function check_executable($file_name) {
  if (substr(PHP_OS, 0, 3) == "WIN" && !eregi("\.exe$", $file_name)) {
    $file_name .= ".exe";
  }
  elseif (substr(PHP_OS, 0, 3) != "WIN") {
    $file_name = eregi_replace("\.exe$", "", $file_name);
  }
  return $file_name;
}
function remote_file_exists($url) { // similar to file_exists(), checks existence of remote files
  $url = trim($url);
  if (!preg_match("=://=", $url)) $url = "http://$url";
  if (!($url = @parse_url($url))) {
    return false;
  }
  if (!eregi("http", $url['scheme'])) {
    return false;
  }
  $url['port'] = (!isset($url['port'])) ? 80 : $url['port'];
  $url['path'] = (!isset($url['path'])) ? "/" : $url['path'];
  $fp = fsockopen($url['host'], $url['port'], $errno, $errstr, 30);
  if (!$fp) {
    return false;
  }
  else {
    $head = "";
    $httpRequest = "HEAD ".$url['path']." HTTP/1.1\r\n"
                  ."HOST: ".$url['host']."\r\n"
                  ."Connection: close\r\n\r\n";
    fputs($fp, $httpRequest);
    while (!feof($fp)) {
      $head .= fgets($fp, 1024);
    }
    fclose($fp);

    preg_match("=^(HTTP/\d+\.\d+) (\d{3}) ([^\r\n]*)=", $head, $matches);
    if ($matches[2] == 200) {
      return true;
    }
  }
}
//stren lenght
function trimlen($str,$max){
	$i=0;
	for($i=0;$i<=$max;$i++){
		$ar.=$str[$i]	;
	}
	if(strlen($str)>$max){
		$ar.="...";
	}
	return $ar;
}
//lay chieu dai va chieu rong cua file anh va file flash
function get_width_height($file_name)
{
    if ($image_info = @getimagesize($file_name)) 
	{
      $width_height = " ".$image_info[3];
      $width = $image_info[0];
      $height = $image_info[1];
	 	return $width_height;	
	 }
	 else
	 {
	 	return false;
	 }

}
// $p=trang so,$url duong link,$sql,$maxpage=so trang duoc hien thi, $maxitem= so ban ghi/trang
function phan_trang($p,$url,$sql,$maxpage,$maxitem,$get='p') 
{ 
	global $DB, $tpl,$lang;
	if($lang=='' || !isset($lang)){
		$trang="Trang";
	}else{
		$trang="Page";
	}
	$p=isset($p) ? intval($p):1;
	$db=$DB->query($sql);
	$cac_trang=array();
	$total=mysql_num_rows($db);
	$total=ceil($total/$maxitem);
	if ($p>$maxpage) { 
		$num_page=ceil($p/$maxpage); 
		$showpage=($num_page-1)*$maxpage; 
		$end=$showpage+$maxpage; 
		$showpage++; 
	}else  	{ 
		$thispage=1; 
		$showpage=1; 
		$end=$maxpage; 
	} 
	$startpage=$showpage; 
	for ($showpage;$showpage<$end+1;$showpage++) 
	{ 
		if ($showpage<=$total) { 
			if ($p==$showpage) { 
				$list_page.="&nbsp;<span class='clicked'>[".$showpage."]&nbsp;</span>"; 
			}else { 
				$list_page.="<a href='$url&$get=$showpage' class='cactrang' >[".$showpage."]</a> "; 
			} 
		} 
	} 
	if ($num_page>1) { 
		$back=$startpage-1; 
		if ($num_page>2) { 
			$list_page1="<a href='$url&$get=1' class='cactrang' ><img src=CSS/phantrang/first.gif border='0' width='10px' height='10px'  align='center'/></a> "; 
		} 
		$list_page1.="<a href='$url&$get=$back' class='cactrang' > <img src=CSS/phantrang/prev.gif border='0' width='14px' height='10px' align='center'/> &nbsp;</a> "; 
	} 
	if ($num_page<ceil($total/$maxpage)&&($total>$maxpage)) { 
		$next=$showpage; 
		$list_page2.=" <a href='$url&$get=$next'  class='cactrang'><img src=CSS/phantrang/next.gif border='0' width='14px' height='10px'align='center'/> </a>"; 
		$list_page2.=" <a href='$url&$get=$total'  class='cactrang'> <img src=CSS/phantrang/last.gif border='0' width='10px' height='10px'align='center'/> </a>"; 
	} 
	$list_page=$list_page1.$list_page.$list_page2; 
	if($total > 1)
	{
		if($list_page){
			$cac_trang['cac_trang']=$trang.": ".$p."/".$total." &nbsp;&nbsp;".$list_page;
		 }
	}
	$a=$maxitem*$p-$maxitem;
	$sql=$sql." limit ".$a.",".$maxitem;
	$cac_trang['db']=$DB->query($sql);
	return $cac_trang;
}

?>