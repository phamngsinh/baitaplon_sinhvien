<?php 
////////////////// default .////////////////////////////////////////////////
function news_default($idcat){
	global $DB,$CONFIG;
	
	$sql="SELECT * FROM cat WHERE active=1 AND parentid=$idcat ORDER BY thu_tu ASC, name ASC";
	$db=$DB->query($sql);
	$count=mysql_num_rows($db);
	if($count>1){
		$tpl=new TemplatePower("template/news.htm");
		$tpl->prepare();
		while($rs=mysql_fetch_array($db)){
			$tpl->newBlock("cat");
			$tpl->assign("cat_name",$rs['name']);
			$tpl->assign("link_cat","?page=news&idc=$rs[id_cat]&code=show_cat");
			$sql1="SELECT * FROM news WHERE active=1 AND id_cat=$rs[id_cat] ORDER BY thu_tu ASC, id_news DESC LIMIT 0,4";
			$db1=$DB->query($sql1);
			$i=0;
			while($rs1=mysql_fetch_array($db1)){
				$i++;
				if($i<=2){
					$tpl->newBlock("news");
					$tpl->assign("news_name",$rs1['name']);
					$tpl->assign("intro",$rs1['intro']);
					$tpl->assign("link_detail","?page=news&id=$rs1[id_news]&idc=$rs1[id_cat]&code=detail");
					if ($rs1['small_image']){ 
						if ($rs1['small_image'] && file_exists($CONFIG['upload_image_path'].$rs1['small_image'])){
							$tpl->assign("image","<img hspace='5' vspace='5' src='".$CONFIG['upload_image_path'].$rs1['small_image']."' align='left'  border='0'>");
						}
					}
				} else {
					$tpl->newBlock("other");
					$tpl->assign("other_name",$rs1['name']);
					$tpl->assign("creat_date",date('d/m/Y',$rs1['ngay_dang']));
					$tpl->assign("link_detail","?page=news&code=detail&id=$rs1[id_news]&idc=$rs1[id_cat]");
				}
			}
		}
		$tpl->printToScreen();
	} else {
		$sql="SELECT * FROM cat WHERE active=1 AND parentid=$idcat";
		$db=$DB->query($sql);
		if($rs=mysql_fetch_array($db)){
			Show_cat($rs['id_cat']);
			echo $rs['id_cat'];
		}else {
			Show_cat($idcat);
		}
	}
}

///////////////// Show cat //////////////////////////////////////////////////
function Show_cat($idc){
	global $DB,$CONFIG;
	$tpl=new TemplatePower("template/news_cat.htm");
		$tpl->prepare();
	$sql="SELECT * FROM news WHERE active=1 AND id_cat=$idc ORDER BY thu_tu ASC, id_news DESC";
	$link="?page=news&idc=$idc&code=show_cat";
	$maxp=8;
	$db1=phan_trang($_GET['p'],$link,$sql,8,8,'p');
	//include("lib/mos_nava.php");
	$count=mysql_num_rows($db1['db']);
	$sql="SELECT * FROM cat WHERE active=1 AND id_cat=$idc";
	$db=$DB->query($sql);
	if($rs=mysql_fetch_array($db)){
		$tpl->assignGlobal("cat_name",$rs['name']);
	}
	while($rs=mysql_fetch_array($db1['db'])){
		$tpl->newBlock("show_cat");
		$tpl->assign("news_name",$rs['name']);
		$tpl->assign("intro",$rs['intro']);

		if ($rs['small_image']){ 
			if ($rs['small_image'] && file_exists($CONFIG['upload_image_path'].$rs['small_image'])){
				$tpl->assign("image","<img hspace='5' vspace='5' src='".$CONFIG['upload_image_path'].$rs['small_image']."' align='left' border='0' class='news_image'>");
			}
		}
		$tpl->assign("link_detail","?page=news&code=detail&id=$rs[id_news]&idc=$rs[id_cat]");
	}
	$tpl->newBlock("phan_trang"); 
	$tpl->assign("cac_trang",$db1['cac_trang']);
	$tpl->printToScreen();
	
	
}

///////////////// News detail ////////////////////////////////////////////////
function Detail($id,$idc,$limit=5,$show=1){
	$tpl=new TemplatePower("template/news_detail.htm");
	$tpl->prepare();
	global $DB,$CONFIG;
	$sql="SELECT * FROM news WHERE active=1 AND id_news=$id";
	$db=$DB->query($sql);
	if($rs=mysql_fetch_array($db)){
		$tpl->newBlock("detail");
		$tpl->assign("news_name",$rs['name']);
		$tpl->assign("intro",$rs['intro']);
		$tpl->assign("content",$rs['content']);
		if ($rs['normal_image']){ 
			if ($rs['normal_image'] && file_exists($CONFIG['upload_image_path'].$rs['normal_image'])){
				$tpl->assign("image","<img hspace='5' vspace='5' src='".$CONFIG['upload_image_path'].$rs['normal_image']."' align='left'  border='0'>");
			}
		}
		elseif ($rs['small_image']){ 
			if ($rs['small_image'] && file_exists($CONFIG['upload_image_path'].$rs['small_image'])){
				$tpl->assign("image","<img hspace='5' vspace='5' src='".$CONFIG['upload_image_path'].$rs['small_image']."' align='left'  border='0'>");
			}
		}
	}
	if($show==1){
		$sql1="SELECT * FROM news WHERE active=1 AND id_news<>$id AND id_cat=$idc ORDER BY thu_tu ASC, id_news DESC LIMIT 0,$limit";
		$db1=$DB->query($sql1);
		while($rs1=mysql_fetch_array($db1)){
			$tpl->newBlock("other");
			$tpl->assign("other_name",$rs1['name']);
			$tpl->assign("creat_date"," (".date('d/m/Y',$rs1['ngay_dang']).")");
			$tpl->assign("link_detail","?page=news&code=detail&id=$rs1[id_news]&idc=$rs1[id_cat]");
		}
	}
	$tpl->printToScreen();
}

?>