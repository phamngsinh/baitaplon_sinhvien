<?php
session_start();
    class Cart { 
		/*
		Input: $table: ten bang
				$session: gia tri session (cua cookie) vua tao o tren
				$product:	san pham
		Process:	Kiem tra trong bang $table, dk: session='$session' AND product='$product'
			
		Output: ko co record thoa man dk: return 0
				co record thoa man dk: return gia tri field "quantity"
		*/
        function check_item($session, $id_prod, $prod_cat) { 
            $query = "SELECT * FROM cart WHERE session='$session' AND id_prod=$id_prod AND prod_cat='$prod_cat' "; 
			
            $result = mysql_query($query); 
             
            if(!$result) { 
                return 0; 
            } 

            $numRows = mysql_num_rows($result); 

            if($numRows == 0) { 
                return 0; 
            } else { 
                $row = mysql_fetch_object($result); 
                return $row->quantity; 
            } 
        } 

		/*
		Input:	$table: ten bang
				$session: gia tri session (cua cookie) vua tao
				$product: ten san pham them vao
				$quantity: so luong san pham them vao
		Process:	- goi check_item kiem tra trong $table
					- ko co sp truoc: Them moi
						co sp truoc: update (cong them vao gia tri $quantity)
		
		*/
        function add_item($session, $id_prod, $prod_cat, $quantity) { 
            $qty = $this->check_item($session, $id_prod, $prod_cat); 
            if($qty == 0) { 
                $query = "INSERT INTO cart (session, id_prod, prod_cat, quantity, time) VALUES "; 
                $query .= "('$session', '$id_prod', '$prod_cat', '$quantity', '".time()."') "; 
                mysql_query($query); 
            } else { 
                $quantity += $qty; 
                $query = "UPDATE cart SET quantity='$quantity' WHERE session='$session' AND "; 
                $query .= "id_prod='$id_prod' AND prod_cat='$prod_cat' "; 
                mysql_query($query); 
            } 
        } 
         /*
		 	Input:	$table
					$session
					$product
			Process: delete record voi dk session='$session' AND product='$product'
		 */
        function delete_item($session, $id_cart) { 
            $query = "DELETE FROM cart WHERE session='$session' AND id_cart='$id_cart' "; 
            mysql_query($query); 
        } 
         
		 /*
		 	Input:	$table
					$session
					$product
					$quantity
			Output: Update bang $table, gan quantity='$quantity', voi dk session='$session' AND product='$product'
		 
		 */
        function modify_quantity($session, $id_cart, $quantity) { 
            $query = "UPDATE cart SET quantity='$quantity' WHERE session='$session' "; 
            $query .= "AND id_cart='$id_cart' "; 
            mysql_query($query); 
        } 
         /*
		 Input:	$table
		 		$session
		  Process:	delete khoi $table , dk: session='$session'
		 
		 */
        function clear_cart($session) { 
            $query = "DELETE FROM cart WHERE session='$session' "; 
            mysql_query($query); 
        } 
         /*
		 Input:	$table
		 		$session
		Process:	ket noi bang $table va bang inventory (ds product hien co) de tinh tong gia tien
		Output:	tong gia tien
				
		 
		 */
        function cart_total($session) { 
            $query = "SELECT * FROM cart WHERE session='$session' "; 
            $result = mysql_query($query); 
            if(mysql_num_rows($result) > 0) { 
                while($row = mysql_fetch_object($result)) { 
					$prod_cat=$row->prod_cat;
					$id_prod=$row->id_prod;
                    $query = "SELECT price FROM $prod_cat WHERE id_$prod_cat='$id_prod' "; 
                    $invResult = mysql_query($query); 
                    $row_price = mysql_fetch_object($invResult); 
                    $total += ($row_price->price * $row->quantity); 
                } 
            } 
            return $total; 
        }
		
		/*
		Input:	$table
				$session
		Process:	Lien ket bang $table va inventory de dua ra ds hang da dat va tong gia tien
		
		Output:
				Mang 2 chieu $contents[][0..] bat dau tu 0
				$contents['product'][]:	ten hang 
				$contents['price'][]: gia tien tuong ung
				$contents['quantity'][]: so luong
				$contents['total'][]: tong tien cho loai hang nay
				$contents['description'][]: mo ta ve loai hang nay
				$contents['final']: toan bo tong so tien
		
		*/ 
         
        function display_contents($session) { 
			global $DB;
            $count = 0; 
            $query = "SELECT * FROM cart WHERE session='$session' ORDER BY id_cart "; 
            $a = $DB->query($query); 
            while ($row = mysql_fetch_array($a)) 
			{ 
				$prod_cat=$row['prod_cat'];
				$id_prod=$row['id_prod'];			
                $query = "SELECT * FROM $prod_cat WHERE id_$prod_cat='$id_prod' "; 
                $result_inv = $DB->query($query); 
                $row_inventory = mysql_fetch_object($result_inv); 
				$contents["id_cart"][$count]=$row['id_cart'];
                $contents["name"][$count] = $row_inventory->name; 
                $contents["price"][$count] = $row_inventory->price; 
                $contents["quantity"][$count] = $row['quantity']; 
                $contents["total"][$count] = ($row_inventory->price * $row['quantity']); 
                $count++; 
            } 
            $total = $this->cart_total($session, $id_prod, $prod_cat); 
            $contents["final"] = $total; 
			$contents["count"]=$count-1;
            return $contents; 
        } 
        /*
		Input:	$table
				$session
		Output: so loai hang chon mua
		
		*/
        function num_items($session) { 
            $query = "SELECT * FROM cart WHERE session='$session' "; 
            $result = mysql_query($query); 
            $num_rows = mysql_num_rows($result); 
            return $num_rows; 
        } 
         /*
		 Input:	$table,$session
		 Ouput: Tong luong hang hoa chon mua
		 */
        function quant_items($session) { 
            $quant = 0; 
            $query = "SELECT * FROM cart WHERE session='$session' "; 
            $result = mysql_query($query); 
            while($row = mysql_fetch_object($result)) { 
                $quant += $row->quantity; 
            } 
            return $quant; 
        } 
    } 
	
	class Order
	{
		function add_order($session,$name,$tel,$email,$address,$addinfo,$miss)
		{
			if ($session and $name and $tel and $email and $address)
			{
				$sql="INSERT INTO orders (session,name,tel,email,address,addinfo,time) VALUES ('$session','$name','$tel','$email','$address','$addinfo','".time()."')";
				mysql_query($sql);
				if ($miss)
				{
					$save['name']=clean_value($name);
					$save['tel']=$tel;
					$save['email']=$email;
					$save['address']=$address;
					$save['addinfo']=$addinfo;

				}
				$info=array();
				$info['name']=clean_value($name);
				$info['tel']=$tel;
				$info['email']=$email;
				$info['address']=$address;
				$info['addinfo']=$addinfo;
				return $info;
			}
			else
			{
				echo "Qu&#253; kh&#225;ch &#273;&#227; nh&#7853;p thi&#7871;u th&#244;ng tin c&#7847;n thi&#7871;t !";
				return "";
			
			}
		
		}
		function delete_order($id_order)
		{
			global $DB;
			$sql="SELECT session FROM orders WHERE id_order=".$id_order;
			$a=$DB->query($sql);
			if ($b=mysql_fetch_array($a))
			{
				$session=$b['session'];
				$sql="DELETE FROM cart WHERE session='".$session."'";
				mysql_query($sql);	
			}
			$sql="DELETE FROM orders WHERE id_order='".$id_order."'";
			mysql_query($sql);
		}
		function delete_cart_not_order()
		{
			$timelimit=3600*24*10; //10 days
			$sql="DELETE FROM cart HAVING (time+".$timelimit.")<".time." AND session NOT IN (SELECT session FROM orders)";
			mysql_query($sql);
		}
	
	}


$cart=new Cart();

$scid=$_SESSION['scid'];
if (!$scid)
{
	$session = md5(uniqid(rand()));
	$_SESSION['scid']=$session;
	$scid=$session;
}

$tpl = new TemplatePower("template/cart.htm");

$tpl->prepare();
$prod_cat=str_replace("\\'","",$_GET['prod_cat']);
$prod_cat=str_replace('\\"',"",$prod_cat);
$id=intval($_GET['id']);

$avalcat=array("product");

if ($prod_cat and (!in_array($prod_cat,$avalcat)))
{
	exit();
}

if ($_GET['code']=='add') 
{
	if ($id and $prod_cat)
	{
		$cart->add_item($scid,$id,$prod_cat,1);
		
	}
	$tpl->assign("page_name", "Shopping Cart");
	$cont=$cart->display_contents($scid);
	$tpl->newBlock("giohang");
	$info['grandtotal']=$cont['final'];
	$tpl->assign("giohang_tongcong", number_format($info['grandtotal']));	
	
	for ($i=0;$i<=$cont['count'];$i++)
	{
		$tpl->newBlock("giohang_row");
		$info['prod_name']=$cont['name'][$i];
		$info['price']=$cont['price'][$i];
		$info['donvitinh']=$cont['donvitinh'][$i];
		$info['quantity']=$cont['quantity'][$i];
		$info['total']=$cont['total'][$i];
		$info['id']=$cont['id_cart'][$i];
		$tpl->assign("giohang_name", $info['prod_name']);
		$tpl->assign("giohang_price", number_format($info['price']));
		$tpl->assign("donvitinh", $info['donvitinh']);
		$tpl->assign("giohang_soluong", $info['quantity']);
		$tpl->assign("giohang_tongtien", number_format($info['total']));
		$tpl->assign("giohang_id", $info['id']);
	}

	
}


if ($_GET['code']=='report') 
{
$time = date("d/m/Y") ;
$tpl = new TemplatePower("template/report.htm");
$tpl->prepare();
$tpl-> assign("time",$time) ;

	if ($id_prod and $prod_cat)
	{
		$cart->add_item($scid,$id_prod,$prod_cat,1);
		
	}
	
	$tpl->assign("page_name", "Shopping Cart");
	$cont=$cart->display_contents($scid);
	$tpl->newBlock("report");
	$info['grandtotal']=$cont['final'];
	$tpl->assign("report_tongcong", number_format($info['grandtotal']));	
	for ($i=0;$i<=$cont['count'];$i++)
	{
		$tpl->newBlock("report_row");
		$info['prod_name']=$cont['name'][$i];
		$info['price']=$cont['price'][$i];
		$info['report_name']=$cont['name'][$i];
		//$info['report_noi_dung']=$cont['noi_dung'][$i];
		
		$info['quantity']=$cont['quantity'][$i];
		$info['total']=$cont['total'][$i];
		$info['id']=$cont['id_cart'][$i];
		$tpl->assign("giohang_name", $info['prod_name']);
		$tpl->assign("report_price", number_format($info['price']));
		
		$tpl->assign("report_name", $info['report_name']);
		//$tpl->assign("report_noi_dung", $info['report_noi_dung']);
		$tpl->assign("report_soluong", $info['quantity']);
		$he = $info['quantity'] * $info['price'] ;
		$tpl->assign("thanhtien",$he) ;
		$tpl->assign("report_tongtien", number_format($info['total']));
		$tpl->assign("report_id", $info['id']);
	}

	
}




if ($_GET['code']=='proadd')
{
	$tpl->assign("page_name", "Shopping Cart");
	$sql="select * from cart where session='".$scid."'";
	$b=$DB->query($sql);
	while ($a=mysql_fetch_array($b))
	{
		$id=$a['id_cart'];
		$quantname='quant'.$id;
		if ($_POST[$quantname]!=$a['quantity'])
		{
			$cart->modify_quantity($scid, $id, $_POST[$quantname]);
		}
	}
	$d=$DB->query($sql);
	while ($c=mysql_fetch_array($d))
	{
		$id=$c['id_cart'];	
		$delname='del'.$id;
		if ($_POST[$delname])
		{
			$cart->delete_item($scid, $id);
		}
	}
	$cont=$cart->display_contents($scid);
	$tpl->newBlock("giohang");
	$info['grandtotal']=$cont['final'];
	$tpl->assign("giohang_tongcong", number_format($info['grandtotal']));	
	for ($i=0;$i<=$cont['count'];$i++)
	{
		
		$tpl->newBlock("giohang_row");
		$info['prod_name']=$cont['name'][$i];
		$info['price']=$cont['price'][$i];
		$info['donvitinh']=$cont['donvitinh'][$i];
		$info['quantity']=$cont['quantity'][$i];
		$info['total']=$cont['total'][$i];
		$info['id']=$cont['id_cart'][$i];
		$tpl->assign("giohang_name", $info['prod_name']);
		$tpl->assign("giohang_price", number_format($info['price']));
		$tpl->assign("donvitinh", $info['donvitinh']);
		$tpl->assign("giohang_soluong", $info['quantity']);
		$tpl->assign("giohang_tongtien", number_format($info['total']));
		$tpl->assign("giohang_id", $info['id']);
				
	}
	$info['grandtotal']=$cont['final'];
	$tpl->assign("giohang_tongcong", number_format($info['grandtotal']));
	$tpl->newBlock("message");
	$msg="Update successfully !<br>";
	$tpl->assign("msg", $msg);

}



if ($_GET['code']=='checkout0')
{
	$tpl->assign("page_name", "Check out information");
	$tpl->newBlock("formuserorder");
	$sql="SELECT * FROM user_post WHERE id_user='".intval($_SESSION['id_user'])."'";
	$db=$DB->query($sql);
	if($rs=mysql_fetch_array($db)){
		$name=$rs['name'];
		$email=$rs['email'];
		$address=$rs['address'];
		$tel=$rs['telephone'];
	}
	$tpl->assign("name",$name);
	$tpl->assign("address",$address);
	$tpl->assign("email",$email);
	$tpl->assign("tel",$tel);
}
if ($_GET['code']=='checkout1')
{
	$tpl->assign("page_name", "Information of Client");
	$name=str_replace("'","",$_POST['name']);
	$password=md5($_POST['password']);
	$sql="select * from userorder where name='".$name."' and password='".$password."'";
	$a=$DB->query($sql);
	if (mysql_num_rows($a))
	{
		if ($b=mysql_fetch_array($a))
		{
			$info['id_userorder']=$b['id_userorder'];
			$info['name']=$b['name'];
			$info['password']=$b['password'];
			$info['email']=$b['email'];
			$info['tel']=$b['tel'];
			$info['address']=$b['address'];
			$tpl->newBlock("formorderreg");
			$tpl->assign("name", $info['name']);
			$tpl->assign("email", $info['email']);
			$tpl->assign("tel", $info['tel']);
			$tpl->assign("address", $info['address']);
			
		}
	}
	else
	{
		$tpl->newBlock("formuserorder");
		$msg="Name or Password 's incorrect !";
		$tpl->assign("msg", $msg);
	}
	

}

if ($_GET['code']=='checkout')
{
	if ($saved)
	{
		$info['name']=$saved['name'];
		$info['tel']=$saved['tel'];
		$info['email']=$saved['email'];
		$info['address']=$saved['address'];
	}
	$info['main']=form_order($info);
}

if ($_GET['code']=='procheckout')
{
	$tpl->assign("page_name", "Information of Client");

	$order=new Order;

		$info['cusname']=$_POST['name'];
		$info['custel']=$_POST['tel'];
		$info['cusemail']=$_POST['email'];
		$info['cusaddress']=$_POST['address'];
		$info['cusaddinfo']=$_POST['addinfo'];
		$tpl->newBlock("orderuser");
		$tpl->assign("cusname", $info['cusname']);
		$tpl->assign("custel", $info['custel']);
		$tpl->assign("cusemail", $info['cusemail']);
		$tpl->assign("cusaddress", $info['cusaddress']);
		$tpl->assign("cusaddinfo", $info['cusaddinfo']);
		$cont=$cart->display_contents($scid);
		$tpl->newBlock("orderprod");
		for ($i=0;$i<=$cont['count'];$i++)
		{
			$info['prod_name']=$cont['name'][$i];
			$info['price']=$cont['price'][$i];
			$info['quantity']=$cont['quantity'][$i];
			$info['donvitinh']=$cont['donvitinh'][$i];
			$info['total']=$cont['total'][$i];
			$info['id']=$cont['id_cart'][$i];
			//$info['cart']=$info['cart'].show_cart_row_order($info);
			$tpl->newBlock("orderprod_row");
			$tpl->assign("giohang_name", $info['prod_name']);
			$tpl->assign("giohang_price", number_format($info['price']));
			$tpl->assign("giohang_soluong", $info['quantity']);
			$tpl->assign("donvitinh", $info['donvitinh']);
			$tpl->assign("giohang_tongtien", number_format($info['total']));
			$tpl->assign("giohang_id", $info['id']);		
			
		}
		$tpl->newBlock("tongcong");
		$info['grandtotal']=$cont['final'];
		$tpl->assign("giohang_tongcong", number_format($info['grandtotal']));
		$tpl->newBlock("chuaxacnhan");
		$tpl->assign("frm_name", $_POST['name']);
		$tpl->assign("frm_password", $_POST['password']);
		$tpl->assign("frm_tel", $_POST['tel']);
		$tpl->assign("frm_email", $_POST['email']);
		$tpl->assign("frm_address", $_POST['address']);
		$tpl->assign("frm_addinfo", $_POST['addinfo']);
		$tpl->assign("frm_miss", $_POST['miss']);
}




if ($_GET['code']=='procheckout1')
{
	$tpl->assign("page_name", "Information of Client");
	
	$order=new Order;
	if ($cusinfo=$order->add_order($scid,$_POST['name'],$_POST['tel'],$_POST['email'],$_POST['address'],$_POST['addinfo'],$_POST['miss']))
	{

		$info['cusname']=$cusinfo['name'];
		$info['custel']=$cusinfo['tel'];
		$info['cusemail']=$cusinfo['email'];
		$info['cusaddress']=$cusinfo['address'];
		$info['cusaddinfo']=$cusinfo['addinfo'];
		$tpl->newBlock("orderuser");
		$tpl->assign("cusname", $info['cusname']);
		$tpl->assign("custel", $info['custel']);
		$tpl->assign("cusemail", $info['cusemail']);
		$tpl->assign("cusaddress", $info['cusaddress']);
		$tpl->assign("cusaddinfo", $info['cusaddinfo']);
		if ($_POST['password'])
		{
			$r['password']=md5($_POST['password']);
			$r['name']=$info['cusname'];
			$r['tel']=$info['custel'];
			$r['email']=$info['cusemail'];
			$r['address']=$info['cusaddress'];
			$t=$DB->compile_db_insert_string($r);
			$sql="INSERT INTO userorder (".$t['FIELD_NAMES'].") VALUES (".$t['FIELD_VALUES'].")";
			$DB->query($sql);		
		}		
		$cont=$cart->display_contents($scid);
		$tpl->newBlock("orderprod");
		for ($i=0;$i<=$cont['count'];$i++)
		{
			$info['prod_name']=$cont['name'][$i];
			$info['price']=$cont['price'][$i];
			$info['quantity']=$cont['quantity'][$i];
			$info['donvitinh']=$cont['donvitinh'][$i];
			$info['total']=$cont['total'][$i];
			$info['id']=$cont['id_cart'][$i];
			//$info['cart']=$info['cart'].show_cart_row_order($info);
			$tpl->newBlock("orderprod_row");
			$tpl->assign("giohang_name", $info['prod_name']);
			$tpl->assign("giohang_price", number_format($info['price']));
			$tpl->assign("giohang_soluong", $info['quantity']);
			$tpl->assign("donvitinh", $info['donvitinh']);
			$tpl->assign("giohang_tongtien", number_format($info['total']));
			$tpl->assign("giohang_id", $info['id']);		
			
		}
		$tpl->newBlock("tongcong");
		$info['grandtotal']=$cont['final'];
		$tpl->assign("giohang_tongcong", number_format($info['grandtotal']));
		$tpl->newBlock("daxacnhan");
	}
	
}

$tpl->printToScreen();
?>