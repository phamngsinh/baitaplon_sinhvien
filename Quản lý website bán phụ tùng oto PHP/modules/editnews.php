<?php
if($_SESSION['id_user']) {
	$tpl=new TemplatePower("template/editnews.htm");
	$tpl->prepare();
	require ("lib/upload.php");
	require ("lib/imaging.php");
	
	if($_GET['id']){
		// load database
		$id=intval($_GET['id']);
		$sql="SELECT * FROM raovat WHERE id_raovat=$id";
		$db=$DB->query($sql);
		if($rs=mysql_fetch_array($db)){
			$tpl->assign("name",$rs['name']);
			$tpl->assign("price",$rs['price']);
			$tpl->assign("don_vi",$rs['don_vi']);
			$tpl->assign("title",$rs['title']);
			$tpl->assign("intro",$rs['intro']);
			$tpl->assign("content",$rs['content']);
			if ($rs['small_image']){ 
				if ($rs['small_image'] && file_exists($CONFIG['upload_image_path'].$rs['small_image'])){
					$tpl->assign("image","<img hspace='5' vspace='5' src='".$CONFIG['upload_image_path'].$rs['small_image']."'  border='0' ><br><input type='checkbox' name='xoa_anh' value='1' class='noborder'>&nbsp;X&#243;a &#7843;nh<br>");
				}
			}
			if($_SESSION['vip']==1){
				$tpl->newBlock("vip");
				if ($rs['small_image4']){ 
					if ($rs['small_image4'] && file_exists($CONFIG['upload_image_path'].$rs['small_image4'])){
						$tpl->assign("image4","<img hspace='5' vspace='5' src='".$CONFIG['upload_image_path'].$rs['small_image4']."'  border='0' ><br><input type='checkbox' name='xoa_anh4' value='1' class='noborder'>&nbsp;X&#243;a &#7843;nh<br>");
					}
				}
				if ($rs['small_image1']){ 
					if ($rs['small_image1'] && file_exists($CONFIG['upload_image_path'].$rs['small_image1'])){
						$tpl->assign("image1","<img hspace='5' vspace='5' src='".$CONFIG['upload_image_path'].$rs['small_image1']."'  border='0' ><br><input type='checkbox' name='xoa_anh1' value='1' class='noborder'>&nbsp;X&#243;a &#7843;nh<br>");
					}
				}
				if ($rs['small_image2']){ 
					if ($rs['small_image2'] && file_exists($CONFIG['upload_image_path'].$rs['small_image2'])){
						$tpl->assign("image2","<img hspace='5' vspace='5' src='".$CONFIG['upload_image_path'].$rs['small_image2']."'  border='0' ><br><input type='checkbox' name='xoa_anh2' value='1' class='noborder'>&nbsp;X&#243;a &#7843;nh<br>");
					}
				}
				if ($rs['small_image3']){ 
					if ($rs['small_image3'] && file_exists($CONFIG['upload_image_path'].$rs['small_image3'])){
						$tpl->assign("image3","<img hspace='5' vspace='5' src='".$CONFIG['upload_image_path'].$rs['small_image3']."'  border='0' ><br><input type='checkbox' name='xoa_anh3' value='1' class='noborder'>&nbsp;X&#243;a &#7843;nh<br>");
					}
				}
			}
			$sql1="SELECT * FROM catrv WHERE active=1 ORDER BY thu_tu ASC, name ASC";
			$db1=$DB->query($sql1);
			while($rs1=mysql_fetch_array($db1)){
				$tpl->newBlock("category");
				if($rs1['id_catrv']==$rs['id_catrv']) $tpl->assign("selected","selected");
				$tpl->assign("id_cat",$rs1['id_catrv']);
				$tpl->assign("cat_name",$rs1['name']);
			}
			
			$sql1="SELECT * FROM ttlienhe WHERE id_raovat='".$rs['id_raovat']."'";
			$db1=$DB->query($sql1);
			if($rs1=mysql_fetch_array($db1)){
				$tpl->newBlock("thongtinlienhe");
				$tpl->assign("thongtinkhac",$rs1['ttlienhe']);
				
			}
		}
		// update database
		if($_POST['name']!='' && $_POST['gone']==1){
			$a=array();
			$a['name']		=compile_post('name');
			$a['price']		=compile_post('price');
			$a['don_vi']	=compile_post('don_vi');
			$a['title']		=compile_post('title');
			$a['intro']		=compile_post('intro');
			$a['id_catrv']		=compile_post('id_catrv');
		
			$a['content']	=compile_post('content');
			$a['id_user']	=0;
			$a['id_user_post']=intval($_SESSION['id_user']);
			if($_POST['xoa_anh']==1){
				$sql="select * from raovat where id_raovat=".$id;
				$x=$DB->query($sql);
				if ($y=mysql_fetch_array($x))
				{
					$lastfile=$y['image'];
					$lastnormal=$y['normal_image'];
					$lastsmall=$y['small_image'];
					if ($lastfile||$lastnormal||$lastsmall)
					{
						if ($lastfile && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile))
						{
							unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile);
						}
						if ($lastnormal && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastnormal))
						{
							unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastnormal);
						}
						if ($lastsmall && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall))
						{
							unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall);
						}						
					}					
				}
				$a['image']='';
				$a['normal_image']='';
				$a['small_image']='';
			}
			if($_POST['xoa_anh1']==1){
				$sql="select * from raovat where id_raovat=".$id;
				$x=$DB->query($sql);
				if ($y=mysql_fetch_array($x))
				{
					$lastfile=$y['image1'];
					$lastsmall=$y['small_image1'];
					if ($lastfile||$lastsmall)
					{
						if ($lastfile && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile))
						{
							unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile);
						}
						if ($lastsmall && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall))
						{
							unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall);
						}						
					}					
				}
				$a['image1']='';
				$a['small_image1']='';
			}
			if($_POST['xoa_anh2']==1){
				$sql="select * from raovat where id_raovat=".$id;
				$x=$DB->query($sql);
				if ($y=mysql_fetch_array($x))
				{
					$lastfile=$y['image2'];
					$lastsmall=$y['small_image2'];
					if ($lastfile||$lastsmall)
					{
						if ($lastfile && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile))
						{
							unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile);
						}
						if ($lastsmall && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall))
						{
							unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall);
						}						
					}					
				}
				$a['image2']='';
				$a['small_image2']='';
			}
			if($_POST['xoa_anh3']==1){
				$sql="select * from raovat where id_raovat=".$id;
				$x=$DB->query($sql);
				if ($y=mysql_fetch_array($x))
				{
					$lastfile=$y['image3'];
					$lastsmall=$y['small_image3'];
					if ($lastfile||$lastsmall)
					{
						if ($lastfile && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile))
						{
							unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile);
						}
						if ($lastsmall && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall))
						{
							unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall);
						}						
					}					
				}
				$a['image3']='';
				$a['small_image3']='';
			}
			if($_POST['xoa_anh4']==1){
				$sql="select * from raovat where id_raovat=".$id;
				$x=$DB->query($sql);
				if ($y=mysql_fetch_array($x))
				{
					$lastfile=$y['image4'];
					$lastsmall=$y['small_image4'];
					if ($lastfile||$lastsmall)
					{
						if ($lastfile && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile))
						{
							unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile);
						}
						if ($lastsmall && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall))
						{
							unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall);
						}						
					}					
				}
				$a['image4']='';
				$a['small_image4']='';
			}
			$sql="select * from raovat where id_raovat=".$id;
				$x=$DB->query($sql);
				if ($y=mysql_fetch_array($x)){
					$lastfile=$y['image'];
					$lastnormal=$y['normal_image'];
					$lastsmall=$y['small_image'];

					$lastfile1=$y['image1'];
					$lastsmall1=$y['small_image1'];
					
					$lastfile2=$y['image2'];
					$lastsmall2=$y['small_image2'];
					
					$lastfile3=$y['image3'];
					$lastsmall3=$y['small_image3'];
					
					$lastfile4=$y['image4'];
					$lastsmall4=$y['small_image4'];
					
				}
				
			if ($_FILES['image']['size'])
			{
				if ($lastfile||$lastnormal||$lastsmall)
				{
					if ($lastfile && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile))
					{
						unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile);
					}
					if ($lastnormal && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastnormal))
					{
						unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastnormal);
					}
					if ($lastsmall && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall))
					{
						unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall);
					}						
				}	
				$in_image=get_new_file_name($_FILES['image']['name'],"raovat_");
				$file_upload=new Upload($CONFIG['root_path'].$CONFIG['upload_image_path'],'jpg,gif,png,bmp');
				if ($file_upload->upload_file('image',2,$in_image))
				{
					$a['image']=$file_upload->file_name;
					$thumbnail=create_thumb($CONFIG['root_path'].$CONFIG['upload_image_path'], $file_upload->file_name);
					if ($thumbnail){
						$a['small_image']=$thumbnail['thumb'];
						$a['normal_image']=$thumbnail['normal'];
					}else{
						$msg.="Kh&#244;ng t&#7841;o &#273;&#432;&#7907;c &#7843;nh thumbnail ! Xem l&#7841;i &#273;&#7883;nh d&#7841;ng file !<br>";
					}
				}else{
					$msg.=$file_upload->get_upload_errors()."<br>";
				}
			}
			if ($_FILES['image1']['size'])
			{
				if ($lastfile1||$lastsmall1)
				{
					if ($lastfile1 && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile1))
					{
						unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile1);
					}
					if ($lastsmall1 && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall1))
					{
						unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall1);
					}						
				}	
				$in_image=get_new_file_name($_FILES['image1']['name'],"raovat_");
				$file_upload=new Upload($CONFIG['root_path'].$CONFIG['upload_image_path'],'jpg,gif,png,bmp');
				if ($file_upload->upload_file('image1',2,$in_image))
				{
					$a['image1']=$file_upload->file_name;
					$thumbnail=create_thumb($CONFIG['root_path'].$CONFIG['upload_image_path'], $file_upload->file_name);
					if ($thumbnail){
						$a['small_image1']=$thumbnail['thumb'];
					}else{
						$msg.="Kh&#244;ng t&#7841;o &#273;&#432;&#7907;c &#7843;nh thumbnail ! Xem l&#7841;i &#273;&#7883;nh d&#7841;ng file !<br>";
					}
				}else{
					$msg.=$file_upload->get_upload_errors()."<br>";
				}
			}
			if ($_FILES['image2']['size'])
			{
				if ($lastfile2||$lastsmall2)
				{
					if ($lastfile2 && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile2))
					{
						unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile2);
					}
					if ($lastsmall2 && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall2))
					{
						unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall2);
					}						
				}	
				$in_image=get_new_file_name($_FILES['image2']['name'],"raovat_");
				$file_upload=new Upload($CONFIG['root_path'].$CONFIG['upload_image_path'],'jpg,gif,png,bmp');
				if ($file_upload->upload_file('image2',2,$in_image))
				{
					$a['image2']=$file_upload->file_name;
					$thumbnail=create_thumb($CONFIG['root_path'].$CONFIG['upload_image_path'], $file_upload->file_name);
					if ($thumbnail){
						$a['small_image2']=$thumbnail['thumb'];
					}else{
						$msg.="Kh&#244;ng t&#7841;o &#273;&#432;&#7907;c &#7843;nh thumbnail ! Xem l&#7841;i &#273;&#7883;nh d&#7841;ng file !<br>";
					}
				}else{
					$msg.=$file_upload->get_upload_errors()."<br>";
				}
			}
			if ($_FILES['image3']['size'])
			{
				if ($lastfile3||$lastsmall3)
				{
					if ($lastfile3 && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile3))
					{
						unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile3);
					}
					if ($lastsmall3 && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall3))
					{
						unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall3);
					}						
				}	
				$in_image=get_new_file_name($_FILES['image3']['name'],"raovat_");
				$file_upload=new Upload($CONFIG['root_path'].$CONFIG['upload_image_path'],'jpg,gif,png,bmp');
				if ($file_upload->upload_file('image3',2,$in_image))
				{
					$a['image3']=$file_upload->file_name;
					$thumbnail=create_thumb($CONFIG['root_path'].$CONFIG['upload_image_path'], $file_upload->file_name);
					if ($thumbnail){
						$a['small_image3']=$thumbnail['thumb'];
					}else{
						$msg.="Kh&#244;ng t&#7841;o &#273;&#432;&#7907;c &#7843;nh thumbnail ! Xem l&#7841;i &#273;&#7883;nh d&#7841;ng file !<br>";
					}
				}else{
					$msg.=$file_upload->get_upload_errors()."<br>";
				}
			}
			if ($_FILES['image4']['size'])
			{
				if ($lastfile4||$lastsmall4)
				{
					if ($lastfile4 && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile4))
					{
						unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastfile4);
					}
					if ($lastsmall4 && file_exists($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall4))
					{
						unlink($CONFIG['root_path'].$CONFIG['upload_image_path'].$lastsmall4);
					}						
				}	
				$in_image=get_new_file_name($_FILES['image4']['name'],"raovat_");
				$file_upload=new Upload($CONFIG['root_path'].$CONFIG['upload_image_path'],'jpg,gif,png,bmp');
				if ($file_upload->upload_file('image4',2,$in_image))
				{
					$a['image4']=$file_upload->file_name;
					$thumbnail=create_thumb($CONFIG['root_path'].$CONFIG['upload_image_path'], $file_upload->file_name);
					if ($thumbnail){
						$a['small_image4']=$thumbnail['thumb'];
					}else{
						$msg.="Kh&#244;ng t&#7841;o &#273;&#432;&#7907;c &#7843;nh thumbnail ! Xem l&#7841;i &#273;&#7883;nh d&#7841;ng file !<br>";
					}
				}else{
					$msg.=$file_upload->get_upload_errors()."<br>";
				}
			}
			$b=$DB->compile_db_update_string($a);
			$sql="UPDATE raovat SET ".$b." WHERE id_raovat=".$id;
			$DB->query($sql);
			$msg="&#272;&#227; &#273;&#259;ng tin th&#224;nh c&#244;ng!";
			if($_POST['radioother']==1){
				$sqlx="SELECT * FROM ttlienhe WHERE id_raovat=".$id;
				$dbx=$DB->query($sqlx);
				if($rsx=mysql_fetch_array($dbx)){
					$c=array();
					$idinsert=mysql_insert_id();
					$c['ttlienhe']		=compile_post('thongtinkhac');
				//	$c['id_raovat']	=$idinsert;
					$d=$DB->compile_db_update_string($c);
					$sql="UPDATE ttlienhe SET ".$d." WHERE id_raovat=".$id;
					$DB->query($sql);
				}else {
					$c=array();
					$idinsert=mysql_insert_id();
					$c['ttlienhe']		=compile_post('thongtinkhac');
					$c['id_raovat']	=$id;
					$b=$DB->compile_db_insert_string($c);
					$sql="INSERT INTO ttlienhe (".$b['FIELD_NAMES'].") VALUES (".$b['FIELD_VALUES'].")";
					$DB->query($sql);
				}
				
			}
			$referbox("?page=list_news","&#272;&#227; s&#7917;a th&#224;nh c&#244;ng !");
		}
		
	}
	if($msg!=''){
		$tpl->newBlock("msg");
		$tpl->assign("msg",$msg);
	}
}
$tpl->printToScreen();
?>