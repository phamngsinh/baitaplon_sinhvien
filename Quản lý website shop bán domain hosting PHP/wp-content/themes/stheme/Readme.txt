=== STheme ===
Author: Sivan
Tags: three-columns, fixed-width, light,white
Requires at least: Wordpress 2.7

== Description ==
STheme inspired by Songtaste. It's simple, widget supported and doesn't require any plugin.
Tested on WordPress 2.7, with IE 6/7, Firefox 3, Safari4, Opera 9 and Chrome.
Valid XHTML 1.1 and CSS 3!
Best view under 1024��768 or greater.

== Suggested Languages ==
US English/en_US (default)
��������/zh_CN (translate by Sivan)

== Demo ==
http://demo.sivan.in/index.php?wptheme=STheme
http://wp-themes.com/stheme/

== Download ==
http://sivan.in/blog/stheme/
http://wordpress.org/extend/themes/stheme

== Display twitter in index ==
Go to "STheme Option" in dashboard,open "Show Notice".Input your twitter code and give it an ID.
  Example:
		<ul id="twitter_update_list">
			<li style="display:none;">More</li>	//for  valid W3C XHTML
		</ul>
		</div>
		<script type="text/javascript" src="http://twitter.com/javascripts/blogger.js"></script>
	<!-- change "username" to your twitter username, "count=3" to display 3 tweets. -->
		<script type="text/javascript" src="http://twitter.com/statuses/user_timeline/username.json?callback=twitterCallback2&count=3"></script>