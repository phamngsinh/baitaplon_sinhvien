<?php
if ( function_exists('register_sidebars') )
    register_sidebars(2);
?>
<?php
add_action ('init','my_theme_init');
function my_theme_init ()
{
load_theme_textdomain ('stheme');
}
?>
<?php
function mytheme_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
	 <div id="comment-<?php comment_ID(); ?>">
	  <div class="comment-author vcard">
		 <?php printf(__('<cite class="fn">%s</cite>'), get_comment_author_link()) ?>
	  </div>
	  <?php if ($comment->comment_approved == '0') : ?>
		 <em><?php _e('Your comment is awaiting moderation.') ?></em>
		 <br />
	  <?php endif; ?>
	  <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s', 'stheme'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)', 'stheme'),'  ','') ?></div>
<div class="clear"></div>
		 <?php echo get_avatar($comment,$size='40',$default='<path_to_url>' ); ?>
	  <?php comment_text() ?>
	  <div class="reply">
		 <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	  </div>
	 </div>
<div class="clear"></div>
<?php
		}
?>
<?php
class ClassicOptions {
	function getOptions() {

		$options = get_option('classic_options');
		if (!is_array($options)) {
			$options['notice'] = false;
			$options['notice_content'] = '';
			$options['roundborder'] = false;
			$options['logo'] = false;
			update_option('classic_options', $options);
		}
		return $options;
	}
 
	function init() {
		if(isset($_POST['classic_save'])) {
			$options = ClassicOptions::getOptions();
			if ($_POST['notice']) {
				$options['notice'] = (bool)true;
			} else {
				$options['notice'] = (bool)false;
			}
			if ($_POST['roundborder']) {
				$options['roundborder'] = (bool)true;
			} else {
				$options['roundborder'] = (bool)false;
			}
			if ($_POST['logo']) {
				$options['logo'] = (bool)true;
			} else {
				$options['logo'] = (bool)false;
			}
			$options['notice_content'] = stripslashes($_POST['notice_content']);
			update_option('classic_options', $options);
 
		} else {
			ClassicOptions::getOptions();
		}
 
		add_theme_page("STheme Options", "STheme Options", 'edit_themes', basename(__FILE__), array('ClassicOptions', 'display'));
	}
 
	function display() {
		$options = ClassicOptions::getOptions();
?>
 
<form action="#" method="post" enctype="multipart/form-data" name="classic_form" id="classic_form">
	<div class="wrap">
		<h2><?php _e('STheme Options', 'stheme'); ?></h2>
 		<table class="form-table">
			<tbody>
				<tr valign="top">
					<th scope="row">
						<?php _e('Notice', 'stheme'); ?>
						<br/>
						<small style="font-weight:normal;"><?php _e('HTML enabled', 'stheme') ?></small>
					</th>
					<td>
						<label>
							<input name="notice" type="checkbox" value="checkbox" <?php if($options['notice']) echo "checked='checked'"; ?> />
							 <?php _e('Show Notice', 'stheme'); ?>
						</label>
						<br/>
						<label>
							<textarea name="notice_content" cols="50" rows="10" id="notice_content" style="width:98%;font-size:12px;" class="code"><?php echo($options['notice_content']); ?></textarea>
						</label>
						<br/>
						<label>
							<input name="roundborder" type="checkbox" value="checkbox" <?php if($options['roundborder']) echo "checked='checked'"; ?> />
							 <?php _e('Use roundborder(Effective except in IE, it doesn\'t valid CSS3)', 'stheme'); ?>
						</label>
						<br/>
						<label>
							<input name="logo" type="checkbox" value="checkbox" <?php if($options['logo']) echo "checked='checked'"; ?> />
							 <?php _e('Use logo image instead of text(The logo image is in the theme image folder)', 'stheme'); ?>
						</label>
					</td>
				</tr>
			</tbody>
		</table>
		<p class="submit">
			<input type="submit" name="classic_save" value="<?php _e('Update Options &raquo;', 'stheme'); ?>" />
		</p>
		<p class="update">
			<iframe allowTransparency="true" width="300px" height="100px" frameborder="0" src="http://sivan.in/themes/stheme/version090510.html" scroll="no"></iframe>
		</p>
	</div>
</form>
 
<?php
	}
}
 
add_action('admin_menu', array('ClassicOptions', 'init'));
 
?>