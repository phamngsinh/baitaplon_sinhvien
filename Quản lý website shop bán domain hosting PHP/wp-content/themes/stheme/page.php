<?php get_header(); ?>

<div id="content_top">
<?php include (TEMPLATEPATH . '/lsidebar.php'); ?>

<div id="blog">
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
<div class="box">
<div class="entry" id="post-<?php the_ID(); ?>">
<div class="posttitle"><h2><?php the_title(); ?></h2></div>
		<div class="post"><?php the_content(__('More &raquo;' ,'stheme'))?></div>
		<p class="tags"><?php the_tags(' ', ', ', ' '); ?></p>
</div>
</div>
		<?php endwhile; ?>
<div class="clear"></div>
<div class="box">
	<div id="comments"><?php comments_template('', true); ?>
</div>
</div>
	<?php if (function_exists('wp_pagenavi')) : ?>
		<div id="pagenavi">
			<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
		</div>
	<?php else : ?>
		<div class="navigation">
			<div class="alignleft"><?php next_posts_link(__('&laquo; Older Entries' ,'stheme') )?></div>
			<div class="alignright"><?php previous_posts_link(__('Newer Entries &raquo;' ,'stheme') )?></div>
		</div>
	<?php endif; ?>

	<?php else : ?>

		<h2 class="center"><?php _e('Not Found', 'stheme') ?></h2>
		<p class="center"><?php _e('Sorry, but you are looking for something that is not here.', 'stheme') ?></p>
		<?php include (TEMPLATEPATH . "/searchform.php"); ?>
	<?php endif; ?>
</div>

<?php get_sidebar(); ?>

		</div>
<div id="content_foot"></div>
	</div>	
<div class="clear"></div>
</div>

<?php get_footer(); ?>