<?php

// helper functions
// ----------------

function bm_input( $var, $type, $description = "", $value = "", $selected="", $onchange="" ) {

	// ------------------------
	// add a form input control
	// ------------------------
	
 	echo "\n";
 	
	switch( $type ){
	
	    case "text":

	 		echo "<input name=\"$var\" id=\"$var\" type=\"$type\" style=\"width: 60%\" class=\"code\" value=\"$value\" onchange=\"$onchange\"/>";
	 		echo "<p style=\"font-size:0.9em; color:#999; margin:0;\">$description</p>";
			
			break;
			
		case "submit":
		
	 		echo "<p class=\"submit\"><input name=\"$var\" type=\"$type\" value=\"$value\" /></p>";

			break;

		case "option":
		
			if( $selected == $value ) { $extra = "selected=\"true\""; }

			echo "<option value=\"$value\" $extra >$description</option>";
		
		    break;
		    
  		case "radio":
  		
			if( $selected == $value ) { $extra = "checked=\"true\""; }
  		
  			echo "<label><input name=\"$var\" id=\"$var\" type=\"$type\" value=\"$value\" $extra /> $description</label><br/>";
  			
  			break;
  			
		case "checkbox":
		
			if( $selected == $value ) { $extra = "checked=\"true\""; }

  			echo "<label><input name=\"$var\" id=\"$var\" type=\"$type\" value=\"$value\" $extra /> $description</label><br/>";

  			break;

		case "textarea":
		
		    echo "<textarea name=\"$var\" id=\"$var\" style=\"width: 60%; height: 10em;\" class=\"code\">$value</textarea>";
		
		    break;
	}

}

function bm_select( $var, $arrValues, $selected, $label ) {

	if( $label != "" ) {
		echo "<label for=\"$var\">$label</label>";
	}
	
	echo "<select name=\"$var\" id=\"$var\">\n";

	forEach( $arrValues as $arr ) {
		
		$extra = "";	
		if( $selected == $arr[ 0 ] ) { $extra = " selected=\"true\""; }

		echo "<option value=\"" . $arr[ 0 ] . "\"$extra>" . $arr[ 1 ] . "</option>\n";

	}
	
	echo "</select>";
	
}

function bm_multiSelect( $var, $arrValues, $arrSelected, $label, $description ) {

	if( $label != "" ) {
		echo "<label for=\"$var\">$label</label>";
	}
	
	echo "<select multiple=\"true\" size=\"7\" name=\"$var\" id=\"$var\" style=\"height:150px;\">\n";

	forEach( $arrValues as $arr ) {
		
		$extra = "";	
		if( in_array( $arr[ 0 ], $arrSelected ) ) { $extra = " selected=\"true\""; }

		echo "<option value=\"" . $arr[ 0 ] . "\"$extra>" . $arr[ 1 ] . "</option>\n";

	}
	
	echo "</select>";
	
	echo "<p style=\"font-size:0.9em; color:#999; margin:0;\">$description</p>";
	
}

function bm_th( $title ) {

	// ------------------
	// add a table header
	// ------------------

   	echo "<tr valign=\"top\">";
	echo "<th width=\"33%\" scope=\"row\">$title</th>";
	echo "<td>";

}

function bm_cth() {

	echo "</td>";
	echo "</tr>";
	
}

?>
