<?php
/*
Template Name: Image Gallery
*/
?>

<?php get_header(); ?>
<div id="breadcrumbs"></div>

<div class="clearfloat stripes">
<?php get_sidebar(); ?>
	
		<div id="content">

		<h2>Image Gallery</h2>
		<br />

<?php
//Gets the last 16 images and paginates them
$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
query_posts("showposts=12&paged=$page");
?>

<?php
//Wraps all the floats in a clearfloat
$posts = get_posts('');
foreach($posts as $post) :
    if(get_post_meta($post->ID, 'Image', TRUE)) {
?>


<div class="clearfloat">       
<?php
        $ifpics = TRUE;
        break;
    }
endforeach;
?> 


<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php 
			//This grabs the custom_field image if there is one
			$values = get_post_custom_values("Image");
	if (isset($values[0])) {						
	?>
	
	<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><img class="gallery" src="<?php echo bloginfo('template_url'); ?>/scripts/timthumb.php?src=/<?php
			$values = get_post_custom_values("Image"); echo $values[0]; ?>&amp;w=70&amp;h=70&amp;zc=1" alt="<?php the_title(); ?>" /></a>
<?php } ?>

		<?php endwhile; ?>
		
		<?php if($ifpics == TRUE) : ?></div><?php endif; ?> 

		<div class="clearfloat pagination">
			<div class="left"><?php next_posts_link('&laquo; Older Entries') ?></div>
			<div class="right"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
		</div>

		<?php else : endif; ?>
	
		</div>

		
</div>
	

<?php get_footer(); ?>