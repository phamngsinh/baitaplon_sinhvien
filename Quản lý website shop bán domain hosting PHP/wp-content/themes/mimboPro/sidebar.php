	<div id="sidebar">
		
	<?php
	
	// grab the contents of the sidebar
	ob_start();
	dynamic_sidebar();
	$sidebarContent = ob_get_contents();
	ob_end_clean();
	
					
	if ( $sidebarContent != "" ) { 
	
		echo "<ul>";
		echo $sidebarContent;
		echo "</ul>";
		
	} else {
	
	?>
		
		<h3 id="pages">Pages</h3>
		<ul class="sidenav">
		<?php wp_list_pages('title_li=&depth=1'); ?>
		</ul>
		
		<h3>Authors</h3>
		<ul class="sidenav">
		<?php wp_list_authors('exclude_admin=0&show_fullname=1&hide_empty=1'); ?> 
		</ul>
		
		<h3>Popular Tags</h3>
		<div class="bluebox">
		<?php wp_tag_cloud('smallest=1&largest=1.5&orderby=count&number=18&unit=em'); ?>	
		</div>
		
			
		<!--<h3>Archives</h3>
				
		<form id="archiveform" action="">
			<select name="archive_chrono" onchange="window.location = (document.forms.archiveform.archive_chrono[document.forms.archiveform.archive_chrono.selectedIndex].value);">
			<ul class="sidenav">
				<?php get_archives('monthly','','option'); ?>
			</ul>
			</select>
		</form>-->
	

		<?php } ?>
		
			</div>

