<?php // Do not delete these lines
	if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if (!empty($post->post_password)) { // if there's a password
		if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie
			?>

			<p class="nocomments">This post is password protected. Enter the password to view comments.</p>

			<?php
			return;
		}
	}

	/* This variable is for alternating comment background */
	$oddcomment = 'alt';
?>

<!-- You can start editing here. -->

<div class="domtab">

	<!--<h4><?php comments_number('No Responses', 'One Response', '% Responses' );?> to &#8220;<?php the_title(); ?>&#8221;</h4>-->

	<?php
	
	if ($comments) : 
	
		bm_splitComments( $comments );
		global $bm_trackbacks, $bm_comments;
		
	?>

	<ul class="domtabs clearfloat" id="discussion">			
		<li><a href="#comments-count">Comments (<?php echo count( $bm_comments ); ?>)</a></li>
		<li><a href="#trackbacks-count">Trackbacks &amp; Pingbacks (<?php echo count( $bm_trackbacks ); ?>)</a></li>
	</ul>

	<div>
	
<h4 id="comments-count">Comments</h4>

	<ul class="commentlist">
		
		<?php foreach ( $bm_comments as $comment) { ?>

			<li <?php if ($comment->user_id) echo ' class="my_comment" '; ?>id="comment-<?php comment_ID() ?>">

			<cite><strong><?php comment_author_link() ?> <?php if ($comment->user_id) echo '(Author)'; ?></strong> said: </cite>
			
			<div class="clearfloat">
			<?php 
			//Gravatar check
			if(!empty($comment -> comment_author_email)) {
				$md5 = md5( strtolower( $comment -> comment_author_email) );
				$default = urlencode( get_bloginfo('template_url') . '/images/nopic.gif'); 
				echo "<img src='http://www.gravatar.com/avatar.php?gravatar_id=$md5&amp;size=45&amp;default=$default' alt='' class='gravatar' />";
			}
			?>

			<?php if ( $comment->comment_approved == '0' ) { ?>
				<em><?php _e('Your comment is awaiting moderation.'); ?></em>
			<?php } ?>
			
			<div class="commenttext"><?php comment_text(); ?></div>
			
			</div>
			
			
			<div class="commentmetadata">-<a href="#comment-<?php comment_ID() ?>" title=""><?php comment_date('F jS, Y') ?> at <?php comment_time() ?></a> <?php edit_comment_link('edit','&nbsp;&nbsp;',''); ?></div>

		</li>

		<?php /* Changes every other comment to a different class */
		
			if ('alt' == $oddcomment) {
				$oddcomment = ''; 
			} else {
				$oddcomment = 'alt';
			}

		} /* end for each comment */ ?>
	</ul>
</div>


<div>

	<h4 id="trackbacks-count">Trackbacks</h4>

	<?php if( count( $bm_trackbacks ) > 0 ) { ?>
	<ul class="commentlist">
	<li class="close-trackbacks">&#160;</li>
		<?php foreach ( $bm_trackbacks as $comment ) : ?>
			<li><?php comment_author_link() ?> <?php edit_comment_link('e','',''); ?></li>
		<?php endforeach; ?>
	</ul>
	<?php } else { ?>
		<h4>There are no trackbacks</h4>
	<?php } ?>
	
</div>
			
		
		

<?php else : // this is displayed if there are no comments so far ?>

	<?php if ('open' == $post->comment_status) : ?>

	 <?php else : // comments are closed ?>

		<p class="nocomments">Comments are closed.</p>

	<?php endif; ?>

<?php endif; ?>

</div>

<?php if ('open' == $post->comment_status) : ?>

<div>

<?php if ( get_option('comment_registration') && !$user_ID ) : ?>


</div><!-- #respond -->

<?php else : ?>

<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform" name="commentform">

<?php if ( $user_ID ) : ?>

<hr />

<p><?php _e('Logged in as'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="<?php _e('Log out of this account'); ?>"><?php _e('Logout'); ?> &raquo;</a></p>

<hr />
<?php else : ?>

<br />
<label for="commenter"><?php _e('Name'); ?> <?php if ($req) { ?><span class="required"><?php _e('(required)'); ?></span><?php } ?></label><input class="field" type="text" name="author" id="commenter" value="<?php echo $comment_author; ?>" size="35" tabindex="1" />

<label for="email"><?php _e('Mail (will not be published)'); ?> <?php if ($req) { ?><span class="required"><?php _e('(required)'); ?></span><?php } ?></label><input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="35" tabindex="2" class="field" />

<label for="url"><?php _e('Website'); ?></label><input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="35" tabindex="3" class="field" />


<?php endif; ?>


<label for="comment"><?php _e('Message'); ?></label>
<div id="wysiwyg" class="clearfloat"><script type="text/javascript">edWrite( "commentform", "comment" );</script></div>
<textarea name="comment" id="comment" cols="60" rows="15" tabindex="4" class="field"></textarea>
<br />
<input name="submit" type="submit" id="submit" tabindex="5" value="<?php _e('Submit Comment'); ?>" />
<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />

<?php do_action('comment_form', $post->ID); ?>


</form>

</div><!-- #respond -->

<?php endif; // If registration required and not logged in ?>

<?php endif; // if you delete this the sky will fall on your head ?>
