<?php get_header(); ?>

<?php if (have_posts()) { ?>
<div id="breadcrumbs"></div>

<div class="clearfloat stripes">
<?php get_sidebar(); ?>

	<div id="content">
	 <?php $post = $posts[0]; ?>
 	  <?php if (is_category()) { ?>
		<h2><?php single_cat_title(); ?> <a href="<?php bloginfo('home'); ?>/category/<?php single_cat_title(); ?>/feed/" title="Subscribe to this category"><img src="<?php bloginfo('template_url'); ?>/images/rss.gif" alt="rss" /></a></h2>
		<?php } ?>
		
	 <?php
		$postCount = 0;
		query_posts( 'showposts=900&post_per_page=-1&cat=' . get_query_var('cat') );
		while (have_posts()) { the_post(); 
		if( $postcount == 0 ) { ?>
			
			<div id="lead" class="clearfloat">
			<?php 
			//This grabs the custom_field image if there is one, plus the latest excerpt
			$values = get_post_custom_values("Image");
	if (isset($values[0])) {						
	?>
	<div class="left">
			<a href="<?php the_permalink() ?>" rel="bookmark"><img src="<?php echo bloginfo('template_url'); ?>/scripts/timthumb.php?src=/<?php
			$values = get_post_custom_values("Image"); echo $values[0]; ?>&amp;w=150&amp;h=175&amp;zc=1" alt="<?php the_title(); ?>" /></a></div>
			<?php } ?>
			
			<div class="right">
			<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
			<div class="date"><?php the_date(); ?></div>
			<div class="bigger"><?php the_excerpt(); ?></div>
			<a href="<?php the_permalink() ?>" rel="bookmark" id="fullstory"><?php the_title(); ?></a>
			</div>
			</div>
		
		<?php } elseif( $postcount > 0 && $postcount <= 3 ) { ?>
			
			<div class="cat-excerpt clearfloat bluebox">
			<?php 
			//This gets the next three latest excerpts and images
			$values = get_post_custom_values("Image");
	if (isset($values[0])) {						
	?>
	<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
	<img src="<?php echo bloginfo('template_url'); ?>/scripts/timthumb.php?src=/<?php
			$values = get_post_custom_values("Image"); echo $values[0]; ?>&amp;w=70&amp;h=70&amp;zc=1" alt="<?php the_title(); ?>" />
			</a>
			
			
			<?php } ?>
			
			
			<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
			<?php the_excerpt(); ?>
			</div>
		
		
		<?php } else { 
			ob_start();
			echo '<li><a href="'; 
			the_permalink();
			echo '">';
			the_title();
			echo '</a></li>';
			$links[] = ob_get_contents();
			ob_end_clean();			
		}
		$postcount ++;
		}
	
	} else { ?>

	<?php } ?>
<?php 
//This gets the rest of the archived headlines
if(count($links)): ?>

<br />
<h4>More in this category:</h4>
<ul class="bullets"><?php echo join("\n", $links); ?></ul>
<?php endif; ?>
</div><!--END CONTENT-->
</div><!--END FLOATS-->


<?php get_footer(); ?>
