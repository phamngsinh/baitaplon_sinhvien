function edDoCode( v, formname, textname ) {

	startv	= v;
	endv	= v;
 
	// link
	if (startv == "a") {
	
 		url = prompt( 'Enter the link url' ,'http://' );
		if ( url != "" ) { startv = "a href=\"" + url + "\""; };
		
	}
 
 	// for IE
	if ( document.selection ) {
	
	    var str = document.selection.createRange().text;
	    document.forms[ formname ].elements[ textname ].focus();
	    var sel = document.selection.createRange();
	    sel.text = "<" + startv + ">" + str + "</" + endv + ">";
	    return;
	    
	// for mozilla
	} else if ( ( typeof document.forms[ formname ].elements[ textname ].selectionStart ) != 'undefined' ) {
	
	    var txtarea = document.forms[ formname ].elements[ textname ];
	    var selLength = txtarea.textLength;
	    var selStart = txtarea.selectionStart;
	    var selEnd = txtarea.selectionEnd;
	    var oldScrollTop = txtarea.scrollTop;
	    //if (selEnd == 1 || selEnd == 2)
	    //selEnd = selLength;
	    var s1 = (txtarea.value).substring(0,selStart);
	    var s2 = (txtarea.value).substring(selStart, selEnd)
	    var s3 = (txtarea.value).substring(selEnd, selLength);
	    txtarea.value = s1 + '<' + startv + '>' + s2 + '</' + endv + '>' + s3;
	    txtarea.selectionStart = s1.length;
	    txtarea.selectionEnd = s1.length + 5 + s2.length + startv.length + endv.length;
	    txtarea.scrollTop = oldScrollTop;
	    txtarea.focus();
	    return;
	    
	} else {
	
		insert('<' + v + '></' + v + '> ' , formname, textname );
		
	}
}

function edInsert( what , formname, textname ) {
	
	if (document.forms[formname].elements[textname].createTextRange) {
	
		document.forms[formname].elements[textname].focus();
		document.selection.createRange().duplicate().text = what;
		
	} else if ((typeof document.forms[formname].elements[textname].selectionStart) != 'undefined') {
	
	    var tarea = document.forms[formname].elements[textname];
	    var selEnd = tarea.selectionEnd;
	    var txtLen = tarea.value.length;
	    var txtbefore = tarea.value.substring(0,selEnd);
	    var txtafter =  tarea.value.substring(selEnd, txtLen);
	    var oldScrollTop = tarea.scrollTop;
	    tarea.value = txtbefore + what + txtafter;
	    tarea.selectionStart = txtbefore.length + what.length;
	    tarea.selectionEnd = txtbefore.length + what.length;
	    tarea.scrollTop = oldScrollTop;
	    tarea.focus();
	    
	} else {
	
		document.forms[formname].elements[textname].value += what;
		document.forms[formname].elements[textname].focus();
		
	}
	
}
 
 
function edSelector(selObj){

	v = selObj.options[selObj.selectedIndex].value;
	if (v != "") {
		bbcode(v);
		selObj.options[0].selected = true;
	}
	
}

function edWrite( formname, textname ) {

	document.write( "<div id=\"edToolbar\">" );
		
	document.write( "<a href=\"#\" id=\"button-bold\" onclick=\"edDoCode('strong', '" + formname + "', '" + textname + "'); return false;\">Bold</a>" );
	document.write( "<a href=\"#\" id=\"button-italic\" onclick=\"edDoCode('em', '" + formname + "', '" + textname + "'); return false;\">Italic</a>" );
		document.write( "<a href=\"#\" id=\"button-link\" onclick=\"edDoCode('a', '" + formname + "', '" + textname + "'); return false;\">Link</a>" );
	document.write( "<a href=\"#\" id=\"button-block\" onclick=\"edDoCode('blockquote', '" + formname + "', '" + textname + "'); return false;\">Blockquote</a>" );

		
	document.write( "</div>" );

}
