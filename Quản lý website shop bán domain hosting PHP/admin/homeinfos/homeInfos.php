<?php
/*-----------------------------------
* TYPICAL INFORMATION AT HOME
-------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public $title = "Dịch vụ tiêu biểu";
	public $text = "";	
	private $process = "";
	private $frmValue = array('update' => '');
	private $page = "";
	
  	function __construct()
    {  
		 global $str, $sess, $db;
	
	 	// Show form content	  
		$this->text = $this->show_form($this->frmValue);
	}
	
	
	/*----------------------------------------
	 | SHOW FORM
	+ ----------------------------------------*/
	function show_form($frmValue)
	{
		global $frm, $db, $str, $sess, $time, $token;
		
		// Get main stuff for paging
	   	$query = $db->simple_select('*', 'homeinfos', '', '', '1');
		$result = $db->query($query);		
		
		// JS for lightbox		
		$text = '<link rel="stylesheet" href="'. DIR_LIGHTBOX .'css/lightbox.css" type="text/css" media="screen" />
				  <script src="'. DIR_LIGHTBOX .'js/prototype.js" type="text/javascript"></script>
				  <script src="'. DIR_LIGHTBOX .'js/scriptaculous.js?load=effects" type="text/javascript"></script>
				  <script src="'. DIR_LIGHTBOX .'js/lightbox.js" type="text/javascript"></script>';
		
		
		// Prepare the form
		$text .= $frm->draw_form("", "", 2, "POST", "frmHomeInfos");
		$text .=  $frm->draw_hidden("process", "delNews");
				
		$text .=  "<table cellspacing='0' cellpadding='6' class='tbl_main' align='center'>";
		
		$text .=   "<tr class='trc'>					
						<td>Nội dung</td>
						<td>Hình minh họa</td>
						<td>Cập nhật</td>
					</tr>";
					
		$row = $db->fetch_assoc($result);
		$text .= "<tr class=".($count%2? "ho" : "hr").">
				  <td>". $row['content'] ."</td>";
				  
		
		if ($row['img'])
		{
			$img = "<a href='".HI_IMG.$row['img']."' rel='lightbox'><img src='".HI_IMG.$row['img']."' class='thumbnail' /></a>";
		}
		else
		{
			$img = "<img src='".ADMIN_IMG."noimg.jpg' class='thumbnail' />";
		}
		
		$text .= "<td>".$img."</td>";
		
				  			
		$text .= "<td>
				   <a href='?mod=homeInfosEdit' class='style10'>Hiệu chỉnh </a>&nbsp;
					</td>
					</tr>";
	
		 
		$text .=  "</table>
					</form>";
		
		$text .= '<br/><div style="text-align:center;color:#0000FF;">Thông tin này được hiển thị tại trang chủ như dịch vụ đặc trưng hoặc dịch vụ mới của công ty</div><br/>';
			
		return $text;
	}
	
	/*---------------------------------------------
	 | GET INPUT DATA AND ACTIONS
	+----------------------------------------------*/
	function get_input()
	{
		global $str;
		$this->page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";
		//$this->frmValue['update'] = isset($_POST['Update']) ? $str->input($_POST['Update']) : "";
		
    }		

}

?>