<?php
/*-----------------------------------------------
| CONFIG GENERAL SETTINGS FOR WEBSITE
-------------------------------------------------*/

// Check for Security
if (!defined('HCR'))
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

$cnt = new content;

class content
{
	public $title = 'Thiết lập cho website';
	public $text = '';	
	public $process = '';
	public $frmValue = array(
								'AdminEmail' => '',
								'TimeZone'   => '',
								'timeout'    => '',
								'antiflood'  => '',
						    );
	public $warning = '';
	public $arr = '';
	
	function __construct()
	{
		global $str , $sess;		
		$this->get_input();
		$check_input = $this->check_input( $this->frmValue );
		if ($this->process == 'setting')
		{
			if ($check_input)
			{
				$this->insert_field( $this->frmValue );
				$this->warning = '<span class="span_ok">Yêu cầu cập nhật đã được thực thi !!</span>';
			}
			else
			{
				$this->warning = '<b><u>Lỗi nhập liệu</u>:</b><span class="span_err"><ul>'. $this->err .'</ul></span>';
			}
	   } 
		
	   $this->text = ($this->warning) ? "<div class='warning'>". $this->warning ."</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
    }	
	
	/*-----------------------
	| SHOW FORM
	+ -----------------------*/
	function show_form($frmValue)
	{
		global $frm, $main, $lang;
		
		$text = $frm->draw_form("", "", 2, "POST");
		$text .= "<table cellspacing='0' cellspadding='0' class='tbl_add' >
				   	  <tr><td colspan='2' height='15'></td></tr>";
			$text .= $frm->draw_hidden("process", "setting");
			
			 
				$text .= "<tr>";
				$text .= "<td align='right'>". $lang['AdminEmail'] ."&nbsp;:&nbsp;</td>";
				$text .= "<td>". $frm->draw_textfield('AdminEmail', $frmValue['AdminEmail'] ? $frmValue['AdminEmail'] : $main->setting['AdminEmail'],"","35") ."</td>";
				$text .= "</tr>
						  <tr><td colspan='2' height='8'></td></tr>";
		
				$text .= "<tr>";
					$text .= "<td align='right'>". $lang['TimeZone'] ."&nbsp;:&nbsp;</td>";
					$text .= "<td>". $frm->draw_textfield('TimeZone', $frmValue['TimeZone'] ? $frmValue['TimeZone'] : $main->setting['TimeZone'],"","35") ."</td>";
				$text .= "</tr>
						  <tr><td colspan='2' height='8'></td></tr>";
				
				
				$option_timeout = '';
				for ( $i=10 ; $i<=60 ; $i+=5 )
				{   
				    $value = $i*60;
					if($frmValue['timeout'])
					{
						$check = $frmValue['timeout']==$value ? true : false;
					}
					else
					{
						$check = $main->setting['timeout']==$value ? true : false;
					}
				 	$option_timeout .= $frm->draw_option( $value , $i , $check );
				}	
				$text .= "<tr>";
					$text .= "<td align='right'>". $lang['timeout'] ."&nbsp;:&nbsp;</td>";
					$text .= "<td>". $frm->draw_select('timeout', $option_timeout) ."&nbsp;". $lang['minutes'] ."</td>";
				$text .= "</tr>
						  <tr><td colspan='2' height='8'></td></tr>";
				
				
				$option_antiflood = '';
				for ( $j=10 ; $j<=40 ; $j+=5 )
				{   
				    $value1 = $j*60;
					if($frmValue['antiflood'])
					{
						$check1 = $frmValue['antiflood']==$value1 ? true : false;
					}
					else
					{
						$check1 = $main->setting['antiflood']==$value1 ? true : false;
					}
				 	$option_antiflood .= $frm->draw_option( $value1 , $j , $check1 );
					
				}	
				$text .= "<tr>";
					$text .= "<td align='right'>". $lang['antiflood'] ."&nbsp;:&nbsp;</td>";
					$text .= "<td>". $frm->draw_select('antiflood', $option_antiflood) ."&nbsp;". $lang['minutes'] ."</td>";
				$text .= "</tr>
						  <tr><td colspan='2' height='8'></td></tr>";
						
				$text .= "<tr>";
					$text .= "<td colspan='2' align=center>";
					$text .= $frm->draw_submit("Thiết lập", "button");
					$text .= "</td>";
				$text .= "</tr>";
			
				$text .= "</table>";		
				$text .= "</form>";				
		
				return $text;
	}
	
	
	/*------------------------------------------------------
	| GET INPUT DATA
	+-------------------------------------------------------*/
	function get_input()
	{
		global $str;
		
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";		
		$this->frmValue['AdminEmail'] = isset($_POST['AdminEmail']) ? $str->input($_POST['AdminEmail']) : "";
		$this->frmValue['TimeZone'] = isset($_POST['TimeZone']) ? $str->input($_POST['TimeZone']) : "";
		$this->frmValue['timeout'] = isset($_POST['timeout']) ? $str->input($_POST['timeout']) : "";
		$this->frmValue['antiflood'] = isset($_POST['antiflood']) ? $str->input($_POST['antiflood']) : "";
	}
	
	
	/*--------------------------------------------------
	| CHECK FOR INPUT DATA
	+---------------------------------------------------*/
	function check_input($frmValue)
	{
		global $frm;				
		$no_error = true;
		
		if ( !$frm->check_email($frmValue['AdminEmail']) )
		{
			$no_error = false;	
			$this->err .= "<li>AdminEmail không hợp lệ</li>";
		}
		
		if ( !$frm->check_password($frmValue['TimeZone'], 3, 11) )
		{
			$no_error = false;
			$this->err .= "<li>Timezone không hợp lệ</li>";
		} else if ( $frmValue['TimeZone'] != strval(intval($frmValue['TimeZone'])) )
		{
			
			$no_error = false;
			$this->err .= "<li>Timezone không hợp lệ</li>";
		}
		
		
		return $no_error;
	}
	
	/*-------------------------------------------------
	| INSERT DATA TO DB
	+--------------------------------------------------*/
	function insert_field($frmValue)
	{
		global $db;		
		foreach ( $frmValue as $key => $value )
		{
			$arr = array('value'  => $value);			
			$db->do_update( "config" , $arr , "name='". $key ."'" );
		}
		
		return true;
	 }

 } // end class


?>