<?php
 /*-------------------------------
  * DETAIL ORDER
----------------------------------*/

// Check for Security
if (!defined('HCR'))
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

$cnt = new content;

class content
{
	public  $title = "Chi tiết thông tin đăng kí dịch vụ";
	public  $text = "";	
	private $process = "";
	private $oid = "";
	private $frmValue = array();
		
	function __construct()
	{
		global $str , $sess , $token;
		$this->oid = isset($_GET['id']) ? $_GET['id'] : '';		
		$this->text = $this->show_form();
	}	
	
	/*---------------------------------------
	| SHOW FORM
	+ --------------------------------------*/
	function show_form()
	{
		global $frm, $dfrm, $db, $sess, $time, $token;
		// Update information as read
		$arr_update = array('status'=>'1');
		$readed = $db->do_update('orders', $arr_update, 'id = "'. $this->oid .'"');
		
		// Select information of this contact information		
		$order = $db->getData('SELECT * FROM orders WHERE id="'.$this->oid.'"');
		$text  = $frm->draw_form("", "", 2, "POST");
		$text .= "<table border='0' cellspacing='0' cellspadding='4' class='tbl_edit'>";
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Chủ thể đăng kí</u>: </td>";
		$text .= "<td>". ($order['subject'] == 1 ? 'Công ty' : 'Cá nhân') ."</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
		
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Họ tên</u>: </td>";
		$text .= "<td>". $order['fullname'] ."</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
				  
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Công ty</u>: </td>";
		$text .= "<td>". $order['company'] ."</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
				  
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Địa chỉ</u>: </td>";
		$text .= "<td>". $order['address'] ."</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
		
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Điện thoại</u>: </td>";
		$text .= "<td>". $order['phone'] ."</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
				  
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Fax</u>: </td>";
		$text .= "<td>". $order['fax'] ."</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
				  
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Email</u>: </td>";
		$text .= "<td>". $order['email'] ."</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
				  
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Tên miền</u>: </td>";
		$text .= "<td><a href='".$order['domain']."' target='_blank'>". $order['domain'] ."</a></td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
		
		// Service
		$service = $db->getData('SELECT * FROM service WHERE id="'.$order['service'].'"');
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Loại dịch vụ</u>: </td>";
		$text .= "<td><a href='?mod=serviceEdit&id=".$service['id']."' class='external' target='_blank'>". $service['title'] ."</a></td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
				  
		// Service plan
		$sp = $db->getData('SELECT * FROM service_plan WHERE id="'.$order['service_plan'].'"');
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Gói dịch vụ</u>: </td>";
		$text .= "<td><a href='?mod=spEdit&id=".$sp['id']."' class='external' target='_blank'>". $sp['title'] ."</a></td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";

					
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Ngày đăng kí</u>: </td>";
		$text .= "<td>". date('H:i:s d-m-Y',$order['order_date']) ."</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
		
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Thời hạn đăng kí</u>: </td>";
		$text .= "<td>". $order['duration'] ."</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
		// Payment		  
		switch ($order['payment'])
		{
			case 1 : $pay = 'Thanh toán trực tiếp'; break;
			case 2 : $pay = 'Chuyển khoản ngân hàng'; break;
			case 3 : $pay = 'Thanh toán bưu điện'; break;
			case 4 : $pay = 'Khác'; break;
			default : $pay = 'Thanh toán trực tiếp';
		}
		$text .= "<tr>";
		$text .= "<td valign='top'><u>Hình thức thanh toán</u>: </td>";
		$text .= "<td>". $pay ."</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
			
		$text .= "<tr>";
		$text .= "<td valign='top'><u>Yêu cầu thêm</u> : </td><td>&nbsp;</td></tr>";
		$text .= "<tr><td colspan='2'>". nl2br($order['infos']) ."</td>";
		$text .= "</tr><tr><td colspan=2 height=15>&nbsp;</td></tr>";
		
		// Define back link
		$backLink = '?mod=orderList';
		if (isset($_GET['r']) && strlen($_GET['r']) > 0)
		{
			$backLink = urldecode($_GET['r']);
		}
		
		$text .= "<td colspan='2' align=center>";
		$text .= "&nbsp;&nbsp;&nbsp; 
					&nbsp;&nbsp;<input type='button' value=' Back ' onclick='javascript: window.location=\"".$backLink."\";')'></td>";
		$text .= "</tr>";			
		$text .= "</table>";		
		$text .= "</form>";
	
		return $text;
	}
	

}
?>