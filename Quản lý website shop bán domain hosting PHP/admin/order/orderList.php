<?php
/*-----------------------------------
* LIST OF ORDERS
-------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public $title = "Danh sách đăng kí dịch vụ";
	public $text = "";	
	private $process = "";
	private $frmValue = array('update' => '');
	private $page = "";
	
  	function __construct()
    {  
		 global $str, $sess, $db;
	     $this->get_input();
		 
		// Delete news
		if ($this->process == 'delOrder')
	    {    
	        $arry = preg_split('/[,]/', $this->frmValue['update'], -1, PREG_SPLIT_DELIM_CAPTURE);
			$count = count($arry);
			for( $i = 0 ; $i < $count ; $i++ )
			{	
				$query = $db -> do_update('orders', array('active' => 0),'id = "'. $arry[$i] .'"');	
			}
		    
	 	}
	 	// Show form content	  
		$this->text = $this->show_form($this->frmValue);
	}
	
	
	/*----------------------------------------
	 | SHOW FORM
	+ ----------------------------------------*/
	function show_form($frmValue)
	{
		global $frm, $db, $str, $sess, $time, $token, $hpaging;
		
		// Get main stuff for paging
	    $start = ( ( $this->page - 1 ) * 10 ); 
		$query1 = $db->simple_select('*', 'orders', 'active=1', 'status ASC, id DESC');
		$query = $db->simple_select('*', 'orders', 'active=1', 'status ASC, id DESC', $start.',10');
			 	
		$result1 = $db->query($query1);
		$num_rows = mysql_num_rows($result1);
	    $paging_show = $hpaging->paging_section($num_rows, $this->page, 5, 10, '?mod=orderList');
		$result = $db->query($query);		
		$count = 0;		
		// Reduce hack
		if ($this->page > 1 && mysql_num_rows($result) == 0)
		{
			$str->goto_url("?mod=orderList");
		}
		
		// JS for lightbox		
		$text = '<link rel="stylesheet" href="'. DIR_LIGHTBOX .'css/lightbox.css" type="text/css" media="screen" />
				  <script src="'. DIR_LIGHTBOX .'js/prototype.js" type="text/javascript"></script>
				  <script src="'. DIR_LIGHTBOX .'js/scriptaculous.js?load=effects" type="text/javascript"></script>
				  <script src="'. DIR_LIGHTBOX .'js/lightbox.js" type="text/javascript"></script>';
		
		
		// Prepare the form
		$text .= $frm->draw_form("", "", 2, "POST", "frmOrders");
		$text .=  $frm->draw_hidden("process", "delOrder");
				
		$text .= "<div class='div_add'>
					   <img src='".ADMIN_IMG."del.png' border='0' align='absmiddle'/>
					  <a href='javascript:OnDelete();' class='topadd'>Xóa mục chọn</a>
					  &nbsp;|&nbsp;
					  <img src='".ADMIN_IMG."refresh.png' border='0'/>
					  <a href='?mod=orderList' class='topadd'>Refresh</a>
				  </div><br/>";		
				
		$text .=  "<table cellspacing='0' cellpadding='6' class='tbl_main' align='center'>";
		
		$text .=   "<tr class='trc'>					
						<td>Chủ thể</td>
						<td>Họ tên</td>
						<td>Công ty</td>
						<td>Email</td>
						<td>Loại dịch vụ</td>
						<td>Gói dịch vụ</td>	
						<td>Ngày đăng kí</td>					
						<td>Thời hạn đăng kí</td>
						<td>Hình thức thanh toán</td>
						<td>Tình trạng</td>
						<td>Chọn xóa&nbsp;<input type='checkbox' name='banid' value='ON' onclick='CheckAll();'></td>
					</tr>";
					
		while ($row = $db->fetch_assoc($result))
		{  
		    $count += 1; 
			$isnew = '';
			$service_plan = $db->getData('SELECT * FROM service_plan WHERE id="'.$row['service_plan'].'"');
			$service = $db->getData('SELECT * FROM service WHERE id="'.$service_plan['service_id'].'"');
			$text .= "<tr class=".($count%2? "ho" : "hr").">
						<td>". ($row['subject'] == 1 ? 'Công ty' : 'Cá nhân') ."</td>
						<td>".$row['fullname'] ."</td>
						<td>".$row['company'] ."</td>
						<td>".$row['email'] ."</td>
						<td><a href='?mod=seviceEdit&id=".$service['id']."' class='external'>".$service['title'] ."</a></td>
						<td><a href='?mod=spEdit&id=".$service_plan['id']."' class='external'>".$service_plan['title'] ."</a></td>
						<td>".date('H:i:s d-m-Y',$row['order_date'])."</td>
						<td>".$row['duration']."</td>";	
			
			// How to pay
			switch ($row['payment'])
			{
				case 1 : $pay = 'Thanh toán trực tiếp'; break;
				case 2 : $pay = 'Chuyển khoản ngân hàng'; break;
				case 3 : $pay = 'Thanh toán bưu điện'; break;
				case 4 : $pay = 'Khác'; break;
				default : $pay = 'Thanh toán trực tiếp';
			
			}
			$text .= '<td>'.$pay.'</td>';
			
			// Striking new
			if ($row['status'] == 0)				
			{
				$text .= "<td align='center'><img src='".ADMIN_IMG."new.gif' border='0'/></td>";
			}
			else
			{
				$text .= "<td>&nbsp;</td>";
			}
	 		
			// Generate back link
			$rLink = '?mod=orderList';
			if ($this->page > 1) $rLink .= '&page='.$this->page;
			$text .= "<td>
					<input type='checkbox' name='OnIs:". $row['id'] ."' value='". $row['id'] ."' onclick='docheckone()'> 
					&nbsp;
					<a href='?mod=orderView&id=".$row['id'].'&r='.urlencode($rLink)."' class='style10'>
					Chi tiết </a>&nbsp;
					</td>
					</tr>";
		   }	
		 
		$text .=  "</table>
					<div class='div_page'>
					<input type='button' name='delete' value='Xóa mục chọn'  onclick='OnDelete();'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					<input type='hidden' name='Update' value=''>";
		
		$text .=   "</form>";			  
	    $text .= "<div align='center' class='div_page'>". $paging_show ."</div>";				
		
	// JS for handle
	$text .= "<script language='JavaScript'>
			 function CheckAll()
			  {	
				  for (var i = 0; i < document.frmOrders.elements.length; i++)	
				  {
					   var e = document.frmOrders.elements[i];
					   if (e.name.indexOf('OnIs:')==0)  
					   {
							e.checked=document.frmOrders.banid.checked;
					   }
				  }
			  }
		
			  function docheckone()
			  {
				   var isChecked=true;
				   for (var i = 0; i < document.frmOrders.elements.length; i++)	
				   {
						var e = document.frmOrders.elements[i];
						if (e.name.indexOf('OnIs:')==0)  
						{   if(e.checked==false)
							isChecked=false;
						}
				   }
								
				   document.frmOrders.banid.checked=isChecked;
			 }		
		
			 function OnDelete()
			 {
			     var tmpStr;
				 tmpStr=new String('');
				 
					for (var i = 0; i < document.frmOrders.elements.length; i++)	
					{
						var e = document.frmOrders.elements[i];
						if ((e.name.indexOf('OnIs:')==0) && e.checked) 
						{
							tmpStr += e.name.substring(e.name.indexOf(\":\")+1) + \",\";	
						}
					}
		
					if (tmpStr.length > 0) 
					{
						if(confirm('Bạn thực sự muốn xóa mục đã chọn?')==true)
				 		{
							tmpStr=tmpStr.substring(0,tmpStr.length-1);
							document.frmOrders.action =\"\";
							document.frmOrders.Update.value=tmpStr;
							document.frmOrders.submit() ;
						}
					}
					else
					{
						alert('Bạn hãy chọn ít nhất một mục để xóa!');
					}
		    }
			</script>";		
					
		return $text;
	}
	
	/*---------------------------------------------
	 | GET INPUT DATA AND ACTIONS
	+----------------------------------------------*/
	function get_input()
	{
		global $str;
		$this->page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";
		$this->frmValue['update'] = isset($_POST['Update']) ? $str->input($_POST['Update']) : "";
		
    }		

}

?>