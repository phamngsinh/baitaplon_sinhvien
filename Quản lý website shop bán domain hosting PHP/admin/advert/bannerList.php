<?php
/*-----------------------------------
| LIST OF BANNER SHOWS ON TOP
-------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public $title = "Danh sách banner";
	public $text = "";	
	private $process = "";
	private $frmValue = array('update' => '');
	private $page = "";
	
  	function __construct()
    {  
		 global $str, $sess, $db;
	     $this->get_input();
		// Delete banner from list
		if ($this->process == 'delBanner')
	    {    
	       unlink(B_IMG.$_GET['bname']);
		   // Update xml file
		   require_once 'fw.php';
	 	}
	   
		$this->text = $this->show_form($this->frmValue);
	}
	
	
	/*----------------------------------------
	 | SHOW FORM
	+ ----------------------------------------*/
	function show_form($frmValue)
	{
		global $frm, $db, $str, $sess, $time, $token, $hpaging, $paging_info;	    
		
		$text = '<link rel="stylesheet" href="'. DIR_LIGHTBOX .'css/lightbox.css" type="text/css" media="screen" />
				  <script src="'. DIR_LIGHTBOX .'js/prototype.js" type="text/javascript"></script>
				  <script src="'. DIR_LIGHTBOX .'js/scriptaculous.js?load=effects" type="text/javascript"></script>
				  <script src="'. DIR_LIGHTBOX .'js/lightbox.js" type="text/javascript"></script>';		
		
		// Prepare the form
		$text .= $frm->draw_form("", "", 2, "POST", "frm_Banner");
			
	
		$text .= "<div class='div_add'>
					  <img src='". ADMIN_IMG ."add.png' border='0'/>
					  <a href='?mod=bannerAdd' class='topadd'>Upload banner</a>					  
					  &nbsp;|&nbsp;
					  <img src='". ADMIN_IMG ."refresh.png' border='0' align='absmiddle'/>
					  <a href='?mod=bannerList' class='topadd'>Refresh</a> 
   				  </div><br/>";		
		
				
		$text .= "<table cellspacing='0' cellpadding='6' class='tbl_main' align='center'>";
		
		$text .= "<tr class='trc'>					
						<td>Banner</td>						
						<td>Chọn xóa&nbsp;<input type='checkbox' name='banid' value='ON' onclick='CheckAll();'></td>
				  </tr>";
		
		   // Get list of banner unde libbanner/banner/
			$needToRead = B_IMG;
			$img_array = array();
			// List of file type to filter
			$type = array('.gif', '.jpg', '.jpeg');
							
			// Open directory
			if ($handle = opendir($needToRead)) 
			{
				// Read all file in this directory
				while (false !== ($file = readdir($handle)))
				{
					// Exclude system file
					if ($file != "." && $file != "..")
					{
						// Get file extention
						$extention = strtolower(strrchr($file,'.'));
						if (in_array($extention, $type))
						{
							$img_array[] = $file;
						}
					}
				}
									 
				closedir($handle);
			}
			$count = 0;
			foreach ($img_array as $k=>$v)
			{
				$count += 1;
				$text .= "<tr class=".($count%2? "ho" : "hr").">";
			
	        	$text .= "<td><a href='". B_IMG . $v ."' rel='lightbox[]'><img src='". B_IMG . $v ."' class='banner' title='Nhấp để xem kích thước thật'/></a></td>";			
			
				$text .= "<td align='center'><a href='?mod=bannerList&action=delBanner&bname=".$v."'>Xóa banner</a></td>
						  </tr>";	
			}
			$text .= "</table>";
					
		return $text;
	}
	
	/*---------------------------------------------
	 | GET INPUT DATA AND ACTIONS
	+----------------------------------------------*/
	function get_input()
	{
		global $str;
		$this->process = isset($_GET['action']) ? $str->input($_GET['action']) : "";
	
	}		

}

?>