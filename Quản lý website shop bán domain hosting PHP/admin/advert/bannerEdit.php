<?php
/*--------------------------------
 | EDIT BANNER
--------------------------------*/

// Check for security
if ( !defined('HCR') )
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public $title = "Hiệu chỉnh banner";
	public $text = "";	
	private $process = "";
	private $id = 0;
	private $frmValue = array(								  
								  'title' => '',
								  'rank' 	 => ''
							  );
	private $file = array(
							 'name' => "",
							 'type' => "",
							 'size' => "",
							 'tmp' => ""
					     );
					
	private $warning = "";
	private $err = "";
		
	function __construct()
	{
		global $str, $sess, $token;
		$this->id = isset($_GET['id']) ? $_GET['id'] : '';
		$this->get_input();
		$check_input = $this->check_input($this->frmValue);

		if ($this->process == "editBanner")
		{ 
			if ( $check_input )
			{				
				$this->update_field($this->frmValue);
				$str->goto_url('?mod=bannerList');				
			}
			else
			{
				$this->warning = "<b><u>Lỗi nhập liệu</u>:&nbsp;</b><span class='span_err'><ul>". $this->err ."</ul></span>";
			 }
		} 
		
		$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
	}	
	
	/*-----------------------------------------------------
	 | SHOW FORM
	+ ---------------------------------------------------*/
	function show_form($frmValue)
	{
		global $frm, $db, $sess, $time, $token;
		
		$query = $db->simple_select('*', 'banner', 'id="'. $this->id .'"');
		$result = $db->query($query);
		$row = $db->fetch_assoc($result);
		
		// JS for lightbox		
		$text = '<link rel="stylesheet" href="'. DIR_LIGHTBOX .'css/lightbox.css" type="text/css" media="screen" />
				  <script src="'. DIR_LIGHTBOX .'js/prototype.js" type="text/javascript"></script>
				  <script src="'. DIR_LIGHTBOX .'js/scriptaculous.js?load=effects" type="text/javascript"></script>
				  <script src="'. DIR_LIGHTBOX .'js/lightbox.js" type="text/javascript"></script>';
		
		$text .= $frm->draw_form("", "", 2, "POST", "frm_editBanner");
		$text .= $frm->draw_hidden("process", "editBanner");
		$text .= "<table cellspacing='0' cellspadding='4' class='tbl_edit'>";

		$text .= "<tr>";
		$text .= "<td>Tiêu đề : </td>";
		$text .= "<td>". $frm->draw_textfield("title", $frmValue["title"] ? $frmValue["title"] : $row['title'],"","60","255") ."</td>";
		$text .=   "</tr>
					<tr><td colspan='2' height='6'></td></tr>";	
	
		$text .= "<tr>";
		$text .= "<td>Banner : </td>";
		$logo = $row['name'] ? "<a href='". B_IMG . $row['name'] ."' rel='lightbox'><img src='". B_IMG . $row['name'] ."' class='thumbnail' title='Nhấp để xem kích thước thật'></a>" : "<font color='red'>Chưa upload banner</font>";
		
		$text .= "<td>". $logo ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";
		
		$text .= "<tr><td colspan='2'><br/><span class='spupload'>-- UPLOAD BANNER --</span><br/></td></tr>";		
		$text .= "<tr>";
		$text .= "<td>Đổi banner : </td>";
		$text .= "<td>". $frm->draw_file("file") ."&nbsp;(*.jpg, *.gif, *.png &amp; <=1MB)</td>
		          </tr>
				  <tr><td colspan='2' align='center'><font color=blue>-- Nếu không muốn thay đổi banner hãy để trống --</font></td></tr>
		    	  <tr><td colspan='2' height='10'>&nbsp;</td></tr>";					
					
		
		$text .= "<tr>";
		$text .= "<td>Ưu tiên : </td>";
		$text .= "<td>". $frm->draw_textfield("rank", $frmValue["rank"] ? $frmValue["rank"] : $row['rank'],"","10","10") ."</td>";
		$text .=   "</tr>
					<tr><td colspan='2' height='6'></td></tr>";	
						
	
		$text .=  "<tr>";
		$text .=  "<td colspan='2' align=center>";
		$text .=  $frm->draw_submit(" Cập nhật ", "");
		$text .=  "&nbsp;&nbsp;<input type='reset' value=' Hủy '></td>";
		$text .=  "</tr>";
			
		$text .=  "</table>";		
		$text .=  "</form>";
					
		return $text;
	}
	
	/*--------------------------------------------
	| GET INPUT
	+--------------------------------------------*/
	function get_input()
	{
		global $str;
		
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";	
	    $this->frmValue['title'] = isset($_POST['title']) ? $str->input($_POST['title']) : '';
		$this->frmValue['rank'] = isset($_POST['rank']) ? $str->input($_POST['rank']) : "";

		if (isset($_FILES['file']['name']))
		{
			$this->file['name'] = $_FILES['file']['name'];
			$this->file['type'] = $_FILES['file']['type'];
			$this->file['size'] = $_FILES['file']['size'];
			$this->file['tmp']  = $_FILES['file']['tmp_name'];
		}
	  
	}
	
	/*------------------------------------------------
	 | CHECK FOR INPUT DATA
	+--------------------------------------------------*/
	function check_input($frmValue)
	{
		global $frm, $time, $str;		
		$no_error = true;
				
		if ( !$frm->check_input($frmValue['title'], 3) )
		{
			$no_error = false; 
			$this->err .= '<li>Hãy nhập tiêu đề</li>';
		}
		
		
		if ( $this->file['name'] )
		{   
		   if (!is_uploaded_file($this->file['tmp']))
		   {
		   		$no_error = false;
				$this->err .= "<li>Lỗi upload file lên server</li>";
		   }		   
		   
		   if ( ( $this->file['type'] != "image/jpeg" ) && ( $this->file['type'] != "image/gif" ) && ( $this->file['type'] != "image/x-png" ) && ( $this->file['type'] != "image/pjpeg" ) && ( $this->file['type'] != "image/png" ) )
		    {
				$no_error = false; 
				$this->err .= '<li>Loại file không hợp lệ(jpg,gif,png)</li>';
			}
			
			if ($this->file['size'] > 1048576)
			{
				$no_error = false;
				$this->err .= "<li>Kích thước file quá lớn(<=1MB)</li>";
			}
			
			if ( file_exists(B_IMG . $this->file['name'] ) )
			{
				$no_error = false;
				$this->err .= '<li>File đã tồn tại hoặc bị trùng tên</li>';
			 }
	     }
		 
		 return $no_error;
	}
	
	
	
	/*----------------------------------------------------
	 | UPDATE DATA TO DB
	+------------------------------------------------------*/
	function update_field($frmValue)
	{
		global $db, $time, $sess;	
		
		
		$arr = array(
						'title' => $frmValue['title'],
						'rank' => intval($frmValue['rank'])
					);
					
		if ($this->file['name'])
		{			
			@move_uploaded_file( $this->file['tmp'], B_IMG.$this->file['name'] );
			$arr['name'] = $this->file['name'];			
			// Update xml file for Banner Gallery
			require_once 'fw.php';
		}
							
		$db->do_update("banner", $arr, "id = '". $this->id ."'");		
		return true;
	 }

	
}


?>