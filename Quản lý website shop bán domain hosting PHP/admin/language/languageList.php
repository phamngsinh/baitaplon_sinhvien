<?php
/*-------------------------------------------------------------
| LIST LANGUAGE THAT IS USED FOR FRONTPAGE
---------------------------------------------------------------*/

// Check for security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

$cnt = new content;

class content
{
	public $title = "Thiết lập ngôn ngữ cho website";
	public $text = "";	
	private $process = "";
	private $warning = "";
	
  function __construct()
  { 
		global $frm, $db, $str;
	
		$this->process = isset($_POST['action']) ? $str->input($_POST['action']) : "";
	   if( $this->process == 'choose_lang' )
	   { 
		    $arr_update1 = array (
									'isdefault' =>1,
									'active'    => 1
								 );
			$query = $db->do_update('language', $arr_update1, 'code = "'. $_POST['lang_default'] .'"');
	  	 	$arr_update2 = array('isdefault'=>0);
			$query =  $db->do_update('language', $arr_update2, 'code != "'. $_POST['lang_default'] .'"');
	   }
	   
	   // Show list of available language
	   $query = $db->simple_select('*', 'language');
	   $result = $db->query($query) ;
	   $this->text .= "<br><table cellspacing='0' cellpadding='6' class='tbl_main' align='center'>
						<tr class='trc'>
							<td>Tên ngôn ngữ</td>
							<td>Hình</td>
							<td>Tình trạng</td>
							<td>Mặc định</td>
							<td>Hiệu chỉnh</td>
						</tr>";
		$l = '';
		$i = 0;
		while ( $row = $db->fetch_assoc($result) )
		{
	      	$i++;
		 	if ( $row['active'] == 1 ){ $l .= "<option value=". $row['code'] .">". $row['name'] ."</option>";}
			$this->text .= "<tr class=". ( $i%2 ? "ho" : "hr" ) .">
							<td align='center'>". $row['name'] ."</td>
							<td align='center'><img src='". DIR_IMG . $row['icon'] ."' border=0 width='30' height='15'></td>							
							<td align='center'>";
		
			$a = ( $row['active'] == 1 ) ? 'actived' : 'disabled';
			$this->text .= $a ."</td><td align=center>";
			$d = ( $row['isdefault'] == 1 ) ? "có" : "không";
			$this->text .= $d."</td>
			<td>&nbsp;<a href='?mod=languageEdit&id=". $row['code'] ."' class='style10'>Cập nhật</a>&nbsp;</td>
			</tr>";	 
	} 
	  $this->text .= "  </table>
					  	<br><center>
					  	<form action='' method='post'>
					  	Chọn ngôn ngữ mặc định là <select name='lang_default'>". $l ."</select>
					  	&nbsp;<input type='submit' value=' Ok '><input type='hidden' name='action' value='choose_lang'>
					 	 </form></center> ";	
	
	   }
	   
}

?>