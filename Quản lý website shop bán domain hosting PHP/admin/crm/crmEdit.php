<?php
/*----------------------------------------
 * EDIT LINK BACK TO CRM
------------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public $title = 'Hiệu chỉnh liên kết đến CRM';
	public $text = '';	
	private $process = '';
	private $crmID = '';
	private $frmValue = array(
								 'name' 	 => '',
								 'intro' 	 => '',
								 'content'   => '',
 								 'rank'      => '',								 
						     );
							  
	private $file = array(
							 'name' => '',
							 'type' => '',
							 'size' => '',
							 'tmp' => ''
						 );
					
	private $warning = '';
	private $err = '';
		
		
	function __construct()
	{
		global $str, $sess, $token;
		
		$this->crmID = isset($_GET['id']) ? $_GET['id'] : 0;
		$this->get_input();
		$check_input = $this->check_input($this->frmValue);

		if ($this->process == "editcrmLink")
		{ 
			if ($check_input)
			{				
				$this->update_field($this->frmValue);	
				$str->goto_url('?mod=crmList');				
			}
			else
			{
				$this->warning = "<b><u>Lỗi nhập liệu</u>:&nbsp;</b><span class='span_err'><ul>". $this->err ."</ul></span>";
			}
		} 
		
		$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
	}	
	
	
	/*---------------------------
	 | SHOW FORM 
	+ ---------------------------
	*/
	function show_form($frmValue)
	{
		global $frm, $db, $sess, $time;
		
		$query = $db->simple_select('*', 'crm', 'id = "'. $this->crmID .'"');
		$result = $db->query($query);
		$row = $db->fetch_assoc($result);
		$text = '';


		$text .= $frm->draw_form("", "", 2, "POST", "frm_editcrmLink");
		$text .= $frm->draw_hidden("process", "editcrmLink");
		
		$text .= '<table cellpadding="4" cellspacing="0" class="tbl_edit">';			
		
		switch ($row['id'])
		{
			case 1 : $title = 'Liên hệ kinh doanh'; break;
			case 2 : $title = 'Hỗ trợ kỹ thuật'; break;
			case 3 : $title = 'Than phiền dịch vụ'; break;
			case 4 : $title = 'Góp ý chất lượng'; break;
			default : $title = 'Liên hệ kinh doanh';
		}
		
		$text .= "<tr>";
		$text .= "<td>Tiêu đề liên kết : </td>";
		$text .= "<td>".$title."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";	
		
		$text .= "<tr>";
		$text .= "<td>Link : </td>";
		$text .= "<td>". $frm->draw_textfield("blink", $frmValue["blink"] ? $frmValue["blink"] : $row['blink'], "", "60", "255") ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";	
		
			
		$text .= "<tr>";
		$text .= "<td colspan='2' align='center'>";
		$text .= $frm->draw_submit(" Cập nhật ", "");
		$text .=  "&nbsp;&nbsp;<input type='reset' value=' Hủy '></td>";
		$text .=  "</tr>";
			
		$text .=  "</table>";		
		$text .=  "</form>";
		
		return $text;
	}
	
	
	/*-------------------------
	 | GET INPUT DATA
	+--------------------------*/
	function get_input()
	{
		global $str;
		
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";		
	   	$this->frmValue['blink'] = isset($_POST['blink']) ? $str->input($_POST['blink']) : "";
		
	}
	
	
	/*----------------------------
	 | CHECK FOR INPUT DATA
	+-----------------------------*/
	function check_input($frmValue)
	{
		global $frm, $str, $db;		
		$no_error = true;
				
		if (!$frm->check_input($frmValue['blink'], 1))
		{
			$no_error = false; 
			$this->err .= '<li>Hãy nhập liên kết</li>';
		}
		return $no_error;
	}
	
	
	
	// UPDATE DATA TO DB
	function update_field($frmValue)
	{
		global $db, $time, $sess;			
		$arr = array(
						'blink'  => $frmValue['blink'],
					);
		$db->do_update("crm", $arr, "id = '". $this->crmID ."'" );		
		return true;
	}
	
 
}

?>