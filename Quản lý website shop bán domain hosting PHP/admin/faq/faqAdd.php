<?php
/*----------------------------------------
| ADD NEW FAQ
------------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public $title = "Thêm hỏi đáp mới";
	public $text = "";	
	private $process = "";
	private $inserted_id = 0;
	private $frmValue = array(
								'question' 	 => '',
								'answer' 	 => ''
							 );
							 
	private $file = array(
							  'name' => '',
							  'type' => '',
							  'size' => '',
							  'tmp'  => ''
						  );
					  
	
	private $warning = '';
	private $err = '';
		
		
	function __construct()
	{
		global $str, $sess, $db;				
		 
		$this->get_input();
		$check_input = $this->check_input( $this->frmValue );	

		if ($this->process == "addFaq")
		{ 
			if ($check_input)
			{ 
				$this->insert_field( $this->frmValue );
				$this->warning = "<span class='span_ok'>Thêm hỏi đáp mới thành công !!</span>";
				$this->warning .= "<ul><ol><img src='". ADMIN_IMG ."red.gif' align='absmiddle' /> Nhấp <a href='?mod=faqEdit&id=". $this->inserted_id ."' class='topadd'>vào đây</a> để hiệu chỉnh.</ol>";
				$this->warning .= "<ol><img src='". ADMIN_IMG ."red.gif' align='absmiddle'/> Nhấp <a href='?mod=faqList' class='topadd'>vào đây</a> để xem danh sách.</ol></ul><br/>";
				
				$this->frmValue['question'] = "";
				$this->frmValue['answer'] = "";
				$this->err = "";
				
			}
			else
			{
				$this->warning = "<b><u>Lỗi nhập liệu</u>:</b>&nbsp;<span class='span_err'><ul>". $this->err ."</ul></span>";
			}
		} 
		
		$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
	}	
	
	/*-------------------------------------------
	  SHOW FORM FOR INPUT DATA
	 -------------------------------------------*/
	function show_form($frmValue)
	{
		global $frm, $db, $sess, $time;
		
		$text = $frm->draw_form( "", "", 2, "POST", "frm_addFaq" );
		$text .= $frm->draw_hidden("process", "addFaq");		

		$text .= "<table cellspacing='0' cellpadding='4' class='tbl_add'>";
			
		$text .= "<tr>";
		$text .= "<td>Câu hỏi : </td>"; 
		$text .= "<td><textarea name='question' rows='3' cols='50'>".$frmValue['question']."</textarea></td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";
				
		$text .= "<tr>";
		$text .= "<td>Trả lời : </td><td>";
			//---------Chen bo editor FCK---------------//
			$oFCKeditor = new FCKeditor('answer') ;
			$oFCKeditor->BasePath = DIR_LIB_EDITOR;
			$oFCKeditor->Value = $frmValue['answer'];
			$oFCKeditor->Width  = '650' ;
			$oFCKeditor->Height = '500' ;					
		$text .= $oFCKeditor->Create() ."</td></tr>";
		$text .= "<tr><td colspan='2' height='6'></td></tr>";
		
		
		$text .= "<tr>";
		$text .= "<td colspan='2' align=center>";
		$text .= $frm->draw_submit(" Thêm ", "");
		$text .= "&nbsp;&nbsp;<input type='reset' value=' Hủy '></td>";
		$text .= "</tr>";
					
		$text .= "</table>";		
		$text .= "</form>";
				
		return $text;
	}
	
	/*----------------------------------------------
	 | GET INPUT DATA
	+-----------------------------------------------*/
	function get_input()
	{
		global $str;
		
	  	$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";		
	  	$this->frmValue['question'] = isset($_POST['question']) ? $str->input($_POST['question']) : "";
		$this->frmValue['answer'] = isset($_POST['answer']) ? $str->input_html($_POST['answer']) : "";
	}
	
	
	/*----------------------------------
	| CHECK FOR INPUT DATA	
	+-----------------------------------*/
	function check_input($frmValue)
	{
		global $frm, $time, $db, $str;
		
		$no_error = true;
		if ( !$frm->check_input($frmValue['question'], 1) )
		{
			 $no_error = false;
			 $this->err .= "<li>Hãy nhập câu hỏi</li>";
		}
		
		
		if ( !$frm->check_input($frmValue['answer'], 1) )
		{
			$no_error = false;
			$this->err .= "<li>Hãy nhập trả lời</li>";
		}
		
		return $no_error;
	}
	
	/*---------------------------------------------
	| ADD INFORMATIONS TO DB
	+----------------------------------------------*/
	function insert_field($frmValue)
	{
		global $db, $time, $sess;
				
		$str_replace = array("á","à","ã","â","é","è","ê","í","ì","ý","ú","ù","ó","ò","õ","ô","Á","À","Ã","Â","É","È","Ê","Í","Ì","Ý","Ú","Ù","Ó","Ò","Õ","Ô");
		$str = array("&aacute;","&agrave;","&atilde;","&acirc;","&eacute;","&egrave;","&ecirc;","&iacute;","&igrave;","&yacute;","&uacute;","&ugrave;","&oacute;","&ograve;","&otilde;","&ocirc;","&Aacute;","&Agrave;","&Atilde;","&Acirc;","&Eacute;","&Egrave;","&Ecirc;","&Iacute;","&Igrave;","&Yacute;","&Uacute;","&Ugrave;","&Oacute;","&Ograve;","&Otilde;","&Ocirc;");
		$frmValue['answer'] = str_replace($str, $str_replace, $frmValue['answer']);
				
		$arr = array(
						'question' 		=> $frmValue['question'],
						'answer' 		=> $frmValue['answer'],
					);
		
		$db->do_insert("faq", $arr);		
		$this->inserted_id = mysql_insert_id();
		return true;
	}
}

?>
