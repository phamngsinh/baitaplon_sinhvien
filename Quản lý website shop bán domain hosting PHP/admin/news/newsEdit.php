<?php
/*----------------------------------------
 * EDIT NEWS INFORMATION
------------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public $title = 'Hiệu chỉnh tin tức';
	public $text = '';	
	private $process = '';
	private $nID = '';
	private $frmValue = array(
								 'title' 	 => '',
								 'intro' 	 => '',
								 'content'   => '',
 								 'striking'  => '',								 
						     );
							  
	private $file = array(
							 'name' => '',
							 'type' => '',
							 'size' => '',
							 'tmp' => ''
						 );
					
	private $warning = '';
	private $err = '';
		
		
	function __construct()
	{
		global $str, $sess, $token;
		
		$this->nID = isset($_GET['id']) ? $_GET['id'] : 0;
		$this->get_input();
		$check_input = $this->check_input($this->frmValue);

		if ($this->process == "editNews")
		{ 
			if ($check_input)
			{				
				$this->update_field($this->frmValue);	
				if (isset($_GET['r']) && strlen($_GET['r']) > 0)			
				{
					$str->goto_url(urldecode($_GET['r']));
				}
				$str->goto_url('?mod=newsList');				
			}
			else
			{
				$this->warning = "<b><u>Lỗi nhập liệu</u>:&nbsp;</b><span class='span_err'><ul>". $this->err ."</ul></span>";
			}
		} 
		
		$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
	}	
	
	
	/*---------------------------
	 | SHOW FORM 
	+ ---------------------------
	*/
	function show_form($frmValue)
	{
		global $frm, $db, $sess, $time;
		
		$query = $db->simple_select('*', 'news', 'id = "'. $this->nID .'"');
		$result = $db->query($query);
		$row = $db->fetch_assoc($result);
		$text = '';
		
		// JS for lightbox
		
		$text = '<link rel="stylesheet" href="'. DIR_LIGHTBOX .'css/lightbox.css" type="text/css" media="screen" />
				  <script src="'. DIR_LIGHTBOX .'js/prototype.js" type="text/javascript"></script>
				  <script src="'. DIR_LIGHTBOX .'js/scriptaculous.js?load=effects" type="text/javascript"></script>
				  <script src="'. DIR_LIGHTBOX .'js/lightbox.js" type="text/javascript"></script>';
		/*
		// Some stuffs for calendar
		$text .= "<script type='text/javascript' src='". DIR_LIB ."jscalendar/calendar.js'></script>
				<script type='text/javascript' src='". DIR_LIB ."jscalendar/lang/calendar-en.js'></script>
				<script type='text/javascript' src='". DIR_LIB ."jscalendar/calendar-setup.js'></script>
				<link rel='stylesheet' type='text/css' href='". DIR_LIB ."jscalendar/calendar.css'>";
		*/
		$text .= $frm->draw_form("", "", 2, "POST", "frm_editNews");
		$text .= $frm->draw_hidden("process", "editNews");
		
		$text .= '<table cellpadding="4" cellspacing="0" class="tbl_edit">';			
			
		/*
		$text .= "<tr>";
		$text .= "<td>Ngày đăng tin : </td>";
		$text .= "<td>
				  <input type='text' name='date' id='start_date' value='". ( $frmValue["date"] ? $frmValue["date"] : $time->unixtime_2_ddmmyyyy($row['date'])) ."'  onblur='parseDate(this, \"%d/%m/%Y\");'>
				  <img src='". DIR_LIB ."jscalendar/jscalendar.gif' alt='Nhấp chọn ngày tháng'  id='jscal_trigger' align='absmiddle'>
				  <script type='text/javascript'>
   						Calendar.setup ({
											inputField : 'start_date', ifFormat : '%d/%m/%Y', showsTime : false, button : 'jscal_trigger', singleClick : true, step : 1
		});
					</script>
		    		</td>";		
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";*/					
			
		$text .= "<tr>";
		$text .= "<td>Tiêu đề : </td>";
		$text .= "<td>". $frm->draw_textfield("title", $frmValue["title"] ? $frmValue["title"] : $row['title'], "", "60", "255") ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";	
				  
		$choose = $frmValue['striking'] ? $frmValue['striking'] : $row['striking'];
		$text .= "<tr>";
		$text .= "<td>Tin nổi bật : </td>";
		$text .= "<td>". $frm->draw_checkbox("striking", "1", "", ($choose == 1 ? 'checked' : '')) ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";

		
		$text .= "<tr>";
		$text .= "<td>Hình hiện tại : </td>";
		$image = $row['img'] ? "<a href='". N_IMG . $row['img'] ."' rel='lightbox'><img src='". N_IMG . $row['img'] ."' class='thumbnail' title='Nhấp để xem kích thước thật' /></a>" : "<font color='red'>Chưa có hình</font>";
		
		$text .= "<td>". $image ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";
			
		$text .= "<tr>";
		$text .= "<td>Đổi hình : </td>";
		$text .= "<td>". $frm->draw_file("file") ."&nbsp;(*.jpg, *.gif, *.png & <=1MB)</td>
		          </tr>
				  <tr><td colspan='2' align='center'><font color='blue'>Nếu không muốn thay đổi,hãy để trống</font></td></tr>
		    	   <tr><td colspan='2' height='6'></td></tr>";

				  
	
		$text .= "<tr>";
		$text .= "<td>Giới thiệu : </td><td>";		
	        /*
			// Embed FCKEditor
			$oFCKeditor = new FCKeditor('intro');
			$oFCKeditor->BasePath = DIR_LIB_EDITOR;
			$oFCKeditor->Value = $frmValue['intro'] ? $frmValue['intro'] : $row['intro'];
			$oFCKeditor->Width  = '600' ;
			$oFCKeditor->Height = '300' ;				
		$text .= $oFCKeditor->Create() ."</td></tr>";*/
		$text .= "<textarea name='intro' cols='70' rows='8'>".($frmValue['intro'] ? $frmValue['intro'] : $row['intro'])."</textarea>";
		$text .= "<tr><td colspan='2' height='6'></td></tr>";
		
		
		$text .= "<tr>";
		$text .= "<td>Nội dung : </td><td>";		
	        // Embed FCKEditor
			$oFCKeditor = new FCKeditor('content');
			$oFCKeditor->BasePath = DIR_LIB_EDITOR;
			$oFCKeditor->Value = $frmValue['content'] ? $frmValue['content'] : $row['content'];
			$oFCKeditor->Width  = '600' ;
			$oFCKeditor->Height = '500' ;				
		$text .= $oFCKeditor->Create() ."</td></tr>";
		$text .= "<tr><td colspan='2' height='6'></td></tr>";
		
		
			
		$text .= "<tr>";
		$text .= "<td colspan='2' align='center'>";
		$text .= $frm->draw_submit(" Cập nhật ", "");
		$text .=  "&nbsp;&nbsp;<input type='reset' value=' Hủy '></td>";
		$text .=  "</tr>";
			
		$text .=  "</table>";		
		$text .=  "</form>";
		
		return $text;
	}
	
	
	/*-------------------------
	 | GET INPUT DATA
	+--------------------------*/
	function get_input()
	{
		global $str;
		
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";		
	   	$this->frmValue['title'] = isset($_POST['title']) ? $str->input($_POST['title']) : "";
		$this->frmValue['intro'] = isset($_POST['intro']) ? $str->input_html($_POST['intro']) : "";
		$this->frmValue['content'] = isset($_POST['content']) ? $str->input_html($_POST['content']) : "";
		$this->frmValue['striking'] = isset($_POST['striking']) ? $str->input($_POST['striking']) : "";		
	
		if (isset($_FILES['file']['name']))
		{
			$this->file['name'] = $_FILES['file']['name'];
			$this->file['type'] = $_FILES['file']['type'];
			$this->file['size'] = $_FILES['file']['size'];
			$this->file['tmp']  = $_FILES['file']['tmp_name'];
		}
	    
		
	}
	
	
	/*----------------------------
	 | CHECK FOR INPUT DATA
	+-----------------------------*/
	function check_input($frmValue)
	{
		global $frm, $str, $db;		
		$no_error = true;
				
		if (!$frm->check_input($frmValue['title'], 1))
		{
			$no_error = false; 
			$this->err .= '<li>Hãy nhập tiêu đề</li>';
		}
		
		
		if (!$frm->check_input($frmValue['intro'], 10))
		{
			$no_error = false; 
			$this->err .= '<li>Hãy nhập giới thiệu</li>';
		}
		

		if (!$frm->check_input($frmValue['content'], 10))
		{
			$no_error = false; 
			$this->err .= '<li>Hãy nhập nội dung</li>';
		}
		
		
		if ($this->file['name'])
		{
			if (!is_uploaded_file($this->file['tmp']))
			{
				$no_error = false;
				$this->err .= '<li>Lỗi trong quá trình upload ảnh lên server</li>';
			}
		
		    if ( ( $this->file['type'] != "image/jpeg" ) && ( $this->file['type'] != "image/gif" ) && ( $this->file['type'] != "image/x-png" ) && ( $this->file['type'] != "image/pjpeg" ) && ( $this->file['type'] != "image/png" ) )
		    {
				$no_error = false; 
				$this->err .= '<li>Loại file ảnh không hợp lệ</li>';
		    }
						
			if ($this->file['size'] > 1048576)
			{
				$no_error = false;
				$this->err .= '<li>Kích thước file quá lớn(<=1MB)</li>';
			}

		}	
		
		return $no_error;
	}
	
	
	
	// UPDATE DATA TO DB
	function update_field($frmValue)
	{
		global $db, $time, $sess;		
		
		$str_replace = array("á","à","ã","â","é","è","ê","í","ì","ý","ú","ù","ó","ò","õ","ô","Á","À","Ã","Â","É","È","Ê","Í","Ì","Ý","Ú","Ù","Ó","Ò","Õ","Ô");
		$str = array("&aacute;","&agrave;","&atilde;","&acirc;","&eacute;","&egrave;","&ecirc;","&iacute;","&igrave;","&yacute;","&uacute;","&ugrave;","&oacute;","&ograve;","&otilde;","&ocirc;","&Aacute;","&Agrave;","&Atilde;","&Acirc;","&Eacute;","&Egrave;","&Ecirc;","&Iacute;","&Igrave;","&Yacute;","&Uacute;","&Ugrave;","&Oacute;","&Ograve;","&Otilde;","&Ocirc;");
		
		//$frmValue['intro'] = str_replace($str, $str_replace, $frmValue['intro']);
		$frmValue['content'] = str_replace($str, $str_replace, $frmValue['content']);
	
		$arr = array(
						'title' 	 => $frmValue['title'],
						'intro' 	 => $frmValue['intro'],
						'content'    => $frmValue['content'],
						'striking'   => intval($frmValue['striking'])
					);
		
		if ($this->file['name'])
		{
			// Generate image name
			$img_name = rand(9,999999);
			$img_name .= '_'.$this->file['name'];
			$arr['img'] = $img_name;	
			@move_uploaded_file( $this->file['tmp'], N_IMG . $img_name );		
		}
		
		
		$db->do_update("news", $arr, "id = '". $this->nID ."'" );		
		return true;
	}
	
 
}

?>