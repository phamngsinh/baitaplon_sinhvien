<?php
/*-----------------------------------
* LIST OF NEWS
-------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public $title = "Danh sách tin tức";
	public $text = "";	
	private $process = "";
	private $frmValue = array('update' => '');
	private $page = "";
	
  	function __construct()
    {  
		 global $str, $sess, $db;
	     $this->get_input();
		 
		// Delete news
		if ($this->process == 'delNews')
	    {    
	        $arry = preg_split('/[,]/', $this->frmValue['update'], -1, PREG_SPLIT_DELIM_CAPTURE);
			$count = count($arry);
			for( $i = 0 ; $i < $count ; $i++ )
			{	
				$query = $db -> do_delete('news', 'id = "'. $arry[$i] .'"');	
			}
		    
	 	}
	 	// Show form content	  
		$this->text = $this->show_form($this->frmValue);
	}
	
	
	/*----------------------------------------
	 | SHOW FORM
	+ ----------------------------------------*/
	function show_form($frmValue)
	{
		global $frm, $db, $str, $sess, $time, $token, $hpaging;
		
		// Get main stuff for paging
	    $start = ( ( $this->page - 1 ) * 10 ); 
		$query1 = $db->simple_select('*', 'news', '', 'id DESC');
		$query = $db->simple_select('*', 'news', '', 'id DESC', $start.',10');
			 	
		$result1 = $db->query($query1);
		$num_rows = mysql_num_rows($result1);
	    $paging_show = $hpaging->paging_section($num_rows, $this->page, 5, 10, '?mod=newsList');
		$result = $db->query($query);		
		$count = 0;		
		// Reduce hack
		if ($this->page > 1 && mysql_num_rows($result) == 0)
		{
			$str->goto_url("?mod=newsList");
		}
		
		// JS for lightbox		
		$text = '<link rel="stylesheet" href="'. DIR_LIGHTBOX .'css/lightbox.css" type="text/css" media="screen" />
				  <script src="'. DIR_LIGHTBOX .'js/prototype.js" type="text/javascript"></script>
				  <script src="'. DIR_LIGHTBOX .'js/scriptaculous.js?load=effects" type="text/javascript"></script>
				  <script src="'. DIR_LIGHTBOX .'js/lightbox.js" type="text/javascript"></script>';
		
		
		// Prepare the form
		$text .= $frm->draw_form("", "", 2, "POST", "frmNews");
		$text .=  $frm->draw_hidden("process", "delNews");
				
		$text .= "<div class='div_add'>
					  <img src='".ADMIN_IMG."add.png' border='0'/>
					  <a href='?mod=newsAdd' class='topadd'>Thêm mới</a>
					  &nbsp;|&nbsp;
					  <img src='".ADMIN_IMG."del.png' border='0' align='absmiddle'/>
					  <a href='javascript:OnDelete();' class='topadd'>Xóa mục chọn</a>
					  &nbsp;|&nbsp;
					  <img src='".ADMIN_IMG."refresh.png' border='0'/>
					  <a href='?mod=newsList' class='topadd'>Refresh</a>
				  </div><br/>";		
				
		$text .=  "<table cellspacing='0' cellpadding='6' class='tbl_main' align='center'>";
		
		$text .=   "<tr class='trc'>					
						<td>Tiêu đề</td>
						<td>Ngày đăng</td>
						<td>Tin nổi bật</td>
						<td>Giới thiệu</td>
						<td>Hình</td>						
						<td>Chọn xóa&nbsp;<input type='checkbox' name='banid' value='ON' onclick='CheckAll();'></td>
					</tr>";
					
		while ($row = $db->fetch_assoc($result))
		{  
		    $count += 1; 
			$isnew = '';
			$text .= "<tr class=".($count%2? "ho" : "hr").">
						<td>". $row['title'] ."</td>
						<td>". date('d-m-Y', $row['postdate']) ."</td>";	
			
			// Striking new
			if ($row['striking'] == 1)				
			{
				$text .= "<td align='center'><img src='".ADMIN_IMG."typical.jpg' border='0'/></td>";
			}
			else
			{
				$text .= "<td>&nbsp;</td>";
			}
	 
			$text .= "<td>". $row['intro'] ."</td><td>";
			
			$text .= $row['img'] ? "<a href='". N_IMG . $row['img'] ."' rel='lightbox'><img src='". N_IMG . $row['img'] ."' class='thumbnail' title='Nhấp để xem kích thước thật' /></a>" : "<img src='". ADMIN_IMG ."noimg.jpg' class='thumbnail' />";			
			$text .= "</td>";
			
			// Generate back link
			$rLink = '?mod=newsList';
			if ($this->page > 1) $rLink .= '&page='.$this->page;
			$text .= "<td>
					<input type='checkbox' name='OnIs:". $row['id'] ."' value='". $row['id'] ."' onclick='docheckone()'> 
					&nbsp;
					<a href='?mod=newsEdit&id=".$row['id'].'&r='.urlencode($rLink)."' class='style10'>
					Hiệu chỉnh </a>&nbsp;
					</td>
					</tr>";
		}	
		 
		$text .=  "</table>
					<div class='div_page'>
					<input type='button' name='delete' value='Xóa mục chọn'  onclick='OnDelete();'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					<input type='hidden' name='Update' value=''>";
		
		$text .=   "</form>";			  
	    $text .= "<div align='center' class='div_page'>". $paging_show ."</div>";				
		
		// JS for handle
		$text .= "<script language='JavaScript'>
				 function CheckAll()
				  {	
					  for (var i = 0; i < document.frmNews.elements.length; i++)	
					  {
						   var e = document.frmNews.elements[i];
						   if (e.name.indexOf('OnIs:')==0)  
						   {
								e.checked=document.frmNews.banid.checked;
						   }
					  }
				  }
			
				  function docheckone()
				  {
					   var isChecked=true;
					   for (var i = 0; i < document.frmNews.elements.length; i++)	
					   {
							var e = document.frmNews.elements[i];
							if (e.name.indexOf('OnIs:')==0)  
							{   if(e.checked==false)
								isChecked=false;
							}
					   }
									
					   document.frmNews.banid.checked=isChecked;
				 }		
			
				 function OnDelete()
				 {
					 var tmpStr;
					 tmpStr=new String('');
					 
						for (var i = 0; i < document.frmNews.elements.length; i++)	
						{
							var e = document.frmNews.elements[i];
							if ((e.name.indexOf('OnIs:')==0) && e.checked) 
							{
								tmpStr += e.name.substring(e.name.indexOf(\":\")+1) + \",\";	
							}
						}
			
						if (tmpStr.length > 0) 
						{
							if(confirm('Bạn thực sự muốn xóa mục đã chọn?')==true)
							{
								tmpStr=tmpStr.substring(0,tmpStr.length-1);
								document.frmNews.action =\"\";
								document.frmNews.Update.value=tmpStr;
								document.frmNews.submit() ;
							}
						}
						else
						{
							alert('Bạn hãy chọn ít nhất một mục để xóa!');
						}
				}
				</script>";		
					
		return $text;
	}
	
	/*---------------------------------------------
	 | GET INPUT DATA AND ACTIONS
	+----------------------------------------------*/
	function get_input()
	{
		global $str;
		$this->page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";
		$this->frmValue['update'] = isset($_POST['Update']) ? $str->input($_POST['Update']) : "";
		
    }		

}

?>