<?php
/*----------------------------------------
 * EDIT REFEREN INFORMATION
------------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public $title = 'Hiệu chỉnh thông tin tham khảo';
	public $text = '';	
	private $process = '';
	private $infosID = '';
	private $frmValue = array(
								 'title' 	=> '',
								 'content'  => '',
 								 'rank'     => '',								 
						     );
							  
	private $file = array(
							 'name' => '',
							 'type' => '',
							 'size' => '',
							 'tmp' => ''
						 );
					
	private $warning = '';
	private $err = '';
		
		
	function __construct()
	{
		global $str, $sess, $token;
		
		$this->infosID = isset($_GET['id']) ? $_GET['id'] : 0;
		$this->get_input();
		$check_input = $this->check_input($this->frmValue);

		if ($this->process == "editInfos")
		{ 
			if ($check_input)
			{				
				$this->update_field($this->frmValue);
				if (isset($_GET['r']) & strlen($_GET['r']) > 0)				
				{
					$str->goto_url(urldecode($_GET['r']));	
				}
				$str->goto_url('?mod=infosList');				
			}
			else
			{
				$this->warning = "<b><u>Lỗi nhập liệu</u>:&nbsp;</b><span class='span_err'><ul>". $this->err ."</ul></span>";
			}
		} 
		
		$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
	}	
	
	
	/*---------------------------
	 | SHOW FORM 
	+ ---------------------------
	*/
	function show_form($frmValue)
	{
		global $frm, $db, $sess, $time;
		
		$query = $db->simple_select('*', 'infos', 'id = "'. $this->infosID .'"');
		$result = $db->query($query);
		$row = $db->fetch_assoc($result);
		$text = '';
		
		
		$text .= $frm->draw_form("", "", 2, "POST", "frm_editInfos");
		$text .= $frm->draw_hidden("process", "editInfos");
		
		$text .= '<table cellpadding="4" cellspacing="0" class="tbl_edit">';			
			
		$text .= "<tr>";
		$text .= "<td>Tiêu đề : </td>";
		$text .= "<td>". $frm->draw_textfield("title", $frmValue["title"] ? $frmValue["title"] : $row['title'], "", "60", "255") ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";	
		
		$text .= "<tr>";
		$text .= "<td>Thứ tự : </td>";
		$text .= "<td>". $frm->draw_textfield("rank", $frmValue["rank"] ? $frmValue["rank"] : $row['rank'], "", "10", "10") ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";	
		
		$text .= "<tr>";
		$text .= "<td>Nội dung : </td><td>";		
	        // Embed FCKEditor
			$oFCKeditor = new FCKeditor('content');
			$oFCKeditor->BasePath = DIR_LIB_EDITOR;
			$oFCKeditor->Value = $frmValue['content'] ? $frmValue['content'] : $row['content'];
			$oFCKeditor->Width  = '650' ;
			$oFCKeditor->Height = '600' ;				
		$text .= $oFCKeditor->Create() ."</td></tr>";
		$text .= "<tr><td colspan='2' height='6'></td></tr>";
		
		
			
		$text .= "<tr>";
		$text .= "<td colspan='2' align='center'>";
		$text .= $frm->draw_submit(" Cập nhật ", "");
		$text .=  "&nbsp;&nbsp;<input type='reset' value=' Hủy '></td>";
		$text .=  "</tr>";
			
		$text .=  "</table>";		
		$text .=  "</form>";
		
		return $text;
	}
	
	
	/*-------------------------
	 | GET INPUT DATA
	+--------------------------*/
	function get_input()
	{
		global $str;
		
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";		
	   	$this->frmValue['title'] = isset($_POST['title']) ? $str->input($_POST['title']) : "";
		$this->frmValue['rank'] = isset($_POST['rank']) ? $str->input($_POST['rank']) : "";
		$this->frmValue['content'] = isset($_POST['content']) ? $str->input_html($_POST['content']) : "";
	}
	
	
	/*----------------------------
	 | CHECK FOR INPUT DATA
	+-----------------------------*/
	function check_input($frmValue)
	{
		global $frm, $str, $db;		
		$no_error = true;
				
		if (!$frm->check_input($frmValue['title'], 1))
		{
			$no_error = false; 
			$this->err .= '<li>Hãy nhập tiêu đề</li>';
		}
		
		
		if (!$frm->check_input($frmValue['content'], 1))
		{
			$no_error = false; 
			$this->err .= '<li>Hãy nhập nội dung</li>';
		}
	
		return $no_error;
	}
	
	
	
	// UPDATE DATA TO DB
	function update_field($frmValue)
	{
		global $db, $time, $sess;		
		
		$str_replace = array("á","à","ã","â","é","è","ê","í","ì","ý","ú","ù","ó","ò","õ","ô","Á","À","Ã","Â","É","È","Ê","Í","Ì","Ý","Ú","Ù","Ó","Ò","Õ","Ô");
		$str = array("&aacute;","&agrave;","&atilde;","&acirc;","&eacute;","&egrave;","&ecirc;","&iacute;","&igrave;","&yacute;","&uacute;","&ugrave;","&oacute;","&ograve;","&otilde;","&ocirc;","&Aacute;","&Agrave;","&Atilde;","&Acirc;","&Eacute;","&Egrave;","&Ecirc;","&Iacute;","&Igrave;","&Yacute;","&Uacute;","&Ugrave;","&Oacute;","&Ograve;","&Otilde;","&Ocirc;");
		
		$frmValue['content'] = str_replace($str, $str_replace, $frmValue['content']);
	
		$arr = array(
						'title'    => $frmValue['title'],
						'content'  => $frmValue['content'],
						'rank'     => $frmValue['rank']
					);
		
		$db->do_update("infos", $arr, "id = '". $this->infosID ."'" );		
		return true;
	}
	
 
}

?>