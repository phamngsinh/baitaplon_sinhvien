<?php
/*--------------------------------------
* LIST CONTACT FROM CUSTOMERS
 ---------------------------------------*/
 
// Check for security
if (!defined('HCR'))
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

$cnt = new content;

class content
{
	public $title = "Danh sách khách hàng liên hệ";
	public $text = "";
	private $process = "";
	private $frmValue = array('update' => '');
	private $page = "";
	
  function __construct()
  { 
	   global $str, $sess, $db, $token;	  
	   $this->get_input();
		// Delete contact user has chosen
	    if ($this->process == 'delContact')
	    {   
		      $arry = preg_split('/[,]/', $this->frmValue['update'], -1, PREG_SPLIT_DELIM_CAPTURE);
			  $count = count($arry);
			  for( $i=0; $i < $count; $i++ )
			  {
			  		$result = $db->do_delete('contact', 'id="'. $arry[$i] .'"');
	      	  }
	     }	
		  
		$this->text = $this->show_form($this->frmValue);
	}
	
	
	/*-------------------------------------
	 | SHOW LIST FORM
	 --------------------------------------*/
	function show_form($frmValue)
	{
		global $frm, $db, $sess, $time, $token, $hpaging;
		
		$this->page = isset($_GET['page']) ? $_GET['page'] : 1;
		
	    $start = (( $this->page-1 ) * 10 ); 
	    $query1 = $db->simple_select("*", "contact", "", "isread ASC, id DESC");
		$result1 = $db->query($query1);		
		$num_rows = mysql_num_rows($result1);
		$paging_show = $hpaging->paging_section($num_rows, $this->page, 5, 10, "?mod=contactList");
	
		$query = $db->simple_select('*', 'contact', '', 'isread ASC, id DESC', $start.',10');
		$result = $db->query($query);
		$count = 0;
		
		// Reduce hack
		if ($this->page >1 && mysql_num_rows($result) == 0)
		{
			$str->goto_url('?mod=contactList');	
		}		
		
		$text = $frm->draw_form(FILE_ADMIN."contact", "", 2, "POST", "frmcontact");
		$text .= $frm->draw_hidden("process", "delContact");
		$text .= "<div class='div_add'>
					  <img src='". ADMIN_IMG ."del.png' border='0' align='absmiddle' />
					  <a href='javascript:OnDelete();' class='topadd'>Xóa mục chọn</a>
						&nbsp;|&nbsp;
					  <img src='". ADMIN_IMG ."refresh.png' border='0' align='absmiddle'/>
					  <a href='?mod=contactList' class='topadd'>Refresh</a> 
				  </div><br/>";
				  
		$text .= "<table cellspacing='0' cellpadding='4' align='center' class='tbl_main'>";		
		$text .= "<tr class='trc'>
				  	  <td>Tên người gởi</td>
					  <td>Công ty</td>
					  <td>Email</td>					  
					  <td>Tiêu đề</td>
					  <td>Ngày gởi</td>
					  <td>Tình trạng</td>
					  <td>Chọn xóa&nbsp;<input type='checkbox' name='banid' value='ON' onclick='CheckAll();'></td>
				  </tr>";
				  
		while ($row = $db->fetch_assoc($result))
		{   
		    $count += 1; 
		    $text .= "<tr class=". ( $count%2 ? "ho" : "hr" ) .">
						  <td>". $row['fullname'] ."</td>
						  <td>". $row['company'] ."</td>
						  <td><a href='mailto:".$row['email']."'>".$row['email']."</a></td>
						  <td>".$row['title']."</td>";

			$text .= "<td>". date('H:i:s d-m-Y',$row['senddate']) ."</td><td align=center>";
			// Define information has been read or not
			$a = ($row['isread'] == 0) ? "<img src='". ADMIN_IMG ."new.gif' border='0'>" : "";
			// Define back link
			$rlink = '?mod=contactList';
			if ($this->page > 1) $rlink .= '&page='.$this->page;			
			$text .= $a ."&nbsp;</td>
					 <td><input type='checkbox' name='OnIs:". $row['id'] ."' value='". $row['id'] ."' onclick='docheckone()'>&nbsp;
					 <a href='?mod=contactView&id=".$row['id'].'&r='.urlencode($rlink)."' class='style10'>	
					Xem chi tiết </a>&nbsp; </td></tr>";
		   
		}
			
		$text .= "</table>
				  <div class='div_page'>
				   	  <input type='button' name='delete' value='Xóa mục chọn'  onclick='OnDelete()';>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  </div>
				  <input type='hidden' name='Update' value=''>";
		
		$text .= "</form>";		
	 	// Paging show		  
		$text .= "<div class='div_page'>". $paging_show ."<div>";		
		
	// JS
	$text .= "<script language='JavaScript'>
			  function CheckAll()
			  {	
				   for (var i = 0; i < document.frmcontact.elements.length; i++)	
				   {
					    var e = document.frmcontact.elements[i];
						if (e.name.indexOf('OnIs:')==0)  
						{
						    e.checked=document.frmcontact.banid.checked;
						}
					}
			   }
					
			   function docheckone()
			   {
					var isChecked=true;
					for (var i = 0; i < document.frmcontact.elements.length; i++)	
					{
						var e = document.frmcontact.elements[i];
						if (e.name.indexOf('OnIs:')==0)  
						{   if(e.checked==false)
							isChecked=false;
						}
					}
									
					document.frmcontact.banid.checked=isChecked;
				}	
			
				function OnDelete()
				{
					var tmpStr;
					tmpStr=new String('');
					for (var i = 0; i < document.frmcontact.elements.length; i++)	
					{
						var e = document.frmcontact.elements[i];
						if ((e.name.indexOf('OnIs:')==0) && e.checked) 
						{
							tmpStr += e.name.substring(e.name.indexOf(\":\")+1) + \",\";	
						}
					}
			
					if (tmpStr.length > 0) 
					{
						if(confirm('Bạn thực sự muốn xóa thông tin đã chọn?')==true)
						{
							tmpStr=tmpStr.substring(0,tmpStr.length-1);
							document.frmcontact.action =\"\";
							document.frmcontact.Update.value=tmpStr;
							document.frmcontact.submit() ;
						}
					}
					else
					{
						alert('Hãy chọn ít nhất một thông tin bạn muốn xoá!');
					}
					
				}
				</script>";		
			
		return $text;
	}
	
	/*-----------------------------------
	 | GET INPUT DATA
	+------------------------------------*/
	function get_input()
	{
		global $str;		
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";
		$this->frmValue['update'] = isset($_POST['Update'])? $str->input($_POST['Update']) : "";
	}
		

}
  
  
?>