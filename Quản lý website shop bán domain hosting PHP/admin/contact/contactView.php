<?php
 /*-------------------------------
  * DETAIL CONTACT FROM CUSTOMER
  --------------------------------*/

// Check for Security
if (!defined('HCR'))
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

$cnt = new content;

class content
{
	public  $title = "Chi tiết khách hàng liên hệ";
	public  $text = "";	
	private $process = "";
	private $cid = "";
	private $frmValue = array();
		
	function __construct()
	{
		global $str , $sess , $token;
		$this->cid = isset($_GET['id']) ? $_GET['id'] : '';		
		$this->text = $this->show_form();
	}	
	
	/*---------------------------------------
	| SHOW FORM
	+ --------------------------------------*/
	function show_form()
	{
		global $frm, $dfrm, $db, $sess, $time, $token;
		// Update information as read
		$arr_update = array('isread'=>'1');
		$readed = $db->do_update('contact', $arr_update, 'id = "'. $this->cid .'"');
		
		// Select information of this contact information		
		$query = $db->simple_select("*","contact","id = '". $this->cid ."'");
		$result = $db->query($query);
		$contact = $db->fetch_assoc($result);
		$text  = $frm->draw_form("", "", 2, "POST");
		$text .= "<table border='0' cellspacing='0' cellspadding='4' class='tbl_edit'>";
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Tên người gởi </u>: </td>";
		$text .= "<td>". $contact['fullname'] ."&nbsp;</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
		
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Địa chỉ </u>: </td>";
		$text .= "<td>". $contact['address'] ."&nbsp;</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
				  
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Công ty </u>: </td>";
		$text .= "<td>". $contact['company'] ."&nbsp;</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
		
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Ngày gởi </u>: </td>";
		$text .= "<td>". date('H:i:s d-m-Y',$contact['senddate']) ."&nbsp;</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";


		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Email </u>: </td>";
		$text .= "<td><a href='mailto:".$contact['email']."' class='style10'>". $contact['email'] ."</a>&nbsp;</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";

					
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Số điện thoại </u>: </td>";
		$text .= "<td>". $contact['phone'] ."&nbsp;</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
		
		$text .= "<tr>";
		$text .= "<td width='140' valign='top'><u>Fax </u>: </td>";
		$text .= "<td>". $contact['fax'] ."&nbsp;</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
				
		$text .= "<tr>";
		$text .= "<td valign='top'><u>Tiêu đề</u> : </td>";
		$text .= "<td>". $contact['title'] ."&nbsp;</td>";
		$text .= "</tr>
				  <tr><td colspan=2 height=6></td></tr>";
			
		$text .= "<tr>";
		$text .= "<td valign='top'><u>Nội dung</u> : </td>";
		$text .= "<td>". nl2br($contact['content']) ."&nbsp;</td>";
		$text .= "</tr><tr><td colspan=2 height=15>&nbsp;</td></tr>";
		
		// Define back link
		$backLink = '?mod=contactList';
		if (isset($_GET['r']) && strlen($_GET['r']) > 0)
		{
			$backLink = urldecode($_GET['r']);
		}
		
		$text .= "<td colspan='2' align=center>";
		$text .= "&nbsp;&nbsp;&nbsp; 
					&nbsp;&nbsp;<input type='button' value='Xem danh sách liên hệ' onclick='javascript: window.location=\"".$backLink."\";')'></td>";
		$text .= "</tr>";			
		$text .= "</table>";		
		$text .= "</form>";
	
		return $text;
	}
	

}
?>