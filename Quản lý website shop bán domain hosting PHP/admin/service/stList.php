<?php
/*----------------------------------------
| LIST OF SERVICES TYPE
------------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{
	
	public $title = "Danh mục dịch vụ";
	public $text = "";	
	private $process = "";
	private $frmValue = array( 
								 'update'  => '',								 
							 );
	private $warning = "";
	private $err = "";
	private $page = 1;

	
    function __construct()
    {
	    global $str, $sess, $db;
	    $this->get_input();
		
		if ($this->process != '')
	 	{    
			$upArr = array();
			if ($this->process == 'enable')
			{
				$upArr['active'] = 1;
			}
			elseif ($this->process == 'disable')
			{
				$upArr['active'] = 0;
			}
		    $arry = preg_split('/[,]/', $this->frmValue['update'], -1, PREG_SPLIT_DELIM_CAPTURE);
			$count = count($arry);
			for ($i=0 ; $i < $count ; $i++)
			{ 
				// Don't allow to delete Server, Hosting and Domain service
				if (!in_array($arry[$i], array(1,2,3)))  
				{
			   		$updare_result = $db->do_update('service_type', $upArr, 'id="'. $arry[$i] .'"');			
				}
	        }
			
	 	}
	  
		$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
		
	}
	
	/*-----------------------
	| SHOW FORM
	+ -----------------------*/
	function show_form($frmValue)
	{
		global $frm, $db, $sess, $token, $hpaging;		
				
	    $start = (($this->page - 1)*10); 
	    $query1 = $db->simple_select("*", "service_type", "", "active DESC,name ASC");
		$query = $db->simple_select("*", "service_type", "", "active DESC,name ASC", $start .",10");		
		$result1 = $db->query($query1);
		$num_rows = mysql_num_rows($result1);
		$paging_show = $hpaging->paging_section_h($num_rows, $this->page, 5, 10);
		
		$result = $db->query( $query );	  
		$count = 0;		
		if ($this->page > 1 && mysql_num_rows($result) == 0)
		{
			$str->goto_url('?mod=stList');
		}
		
		$text = $frm->draw_form("", "", 2, "POST", "frmstList");
		$text .= $frm->draw_hidden("process", "enable");
		$text .= $frm->draw_hidden("curPg", "1");
		
		$text .= "<div class='div_add'>
					  <img src='".ADMIN_IMG."add.png' border='0'/>
					  <a href='?mod=stAdd' class='topadd'>Thêm mới</a>
					  &nbsp;|&nbsp;				  
					  <img src='".ADMIN_IMG."new.png' border='0'/>
					  <a href='javascript:OnEnable();' class='topadd'>Kích hoạt</a>
					  &nbsp;|&nbsp;
					  <img src='".ADMIN_IMG."del.png' border='0' align='absmiddle'/>
					  <a href='javascript:OnDisable();' class='topadd'>Ngừng kích hoạt</a>
					  &nbsp;|&nbsp;
					  <img src='".ADMIN_IMG."refresh.png' border='0'/>
					  <a href='?mod=stList' class='topadd'>Refresh</a>
				  </div><br/>";
		
				  
		$text .= "<table cellspacing='0' cellpadding='6' class='tbl_main' align='center'>";
		
		$text .= "	<tr class='trc'>
						<td>Tên danh mục dịch vụ</td>
						<td> Kích hoạt</td>
						<td>Chọn xóa&nbsp;<input type='checkbox' name='banid' value='ON' onclick='CheckAll();'> </td>
				   </tr>";
		
		while ($row = $db->fetch_array($result))
		{   
		    $count += 1; 		  
		    $text .= "<tr class=".( $count%2 ? "ho" : "hr" ).">
					  <td>".$row['name']."</td>";			
			$text .= "<td>". ($row['active'] == 1 ? 'active' : 'disabled') ."</td>
						  <td>";
			// Don't allow to delete Server, Hosting and Domain service
			if (!in_array($row['id'], array(1,2,3)))
			{
				$text .= "<input type='checkbox' name='OnIs:". $row['id'] ."' value='". $row['id'] ."' onclick='docheckone()'> 
						&nbsp;";
			}
			
			// Generate back link
			$rLink = '?mod=stList';
			if ($this->page > 1) $rLink .= '&page='.$this->page;
			$text .= "
						<a href='?mod=stEdit&id=".$row['id'].'&r='.urlencode($rLink)."' class='style10'>Hiệu chỉnh </a>&nbsp;
						  </td>
					  </tr>";
		}	
		
		$text .= "</table>
					<div class='div_page'>
					<input type='button' name='disable' value='Kích hoạt'  onclick='OnEnable()';>&nbsp;&nbsp;
					<input type='button' name='enable' value='Ngừng kích hoạt'  onclick='OnDisable()';>&nbsp;&nbsp;
					</div>
					<input type='hidden' name='Update' value=''>";
		
		$text .= "</form>";		  
	    $text .= "<div align='center'>". $paging_show ."</div>";			
	
	// JS for delete action
	$text .= "<script language='JavaScript'>
			  function CheckAll()
			  {	
				  for (var i = 0; i < document.frmstList.elements.length; i++)	
				  {
					  var e = document.frmstList.elements[i];
					  if (e.name.indexOf('OnIs:')==0)  
					  {
						  e.checked=document.frmstList.banid.checked;
					  }
				  }
			  }
		
			  function docheckone()
			  {
				  var isChecked=true;
				  for (var i = 0; i < document.frmstList.elements.length; i++)	
				  {
					  var e = document.frmstList.elements[i];
					  if (e.name.indexOf('OnIs:')==0)  
					  {   
					      if(e.checked==false)
						  isChecked=false;
					  }  
				  }
								
				   document.frmstList.banid.checked=isChecked;
			   }		
		
			   function OnEnable()
			   {
				   var tmpStr;
				   tmpStr=new String('');
				   
				       for (var i = 0; i < document.frmstList.elements.length; i++)	
					   {
							var e = document.frmstList.elements[i];
							if ((e.name.indexOf('OnIs:')==0) && e.checked) 
							{
								tmpStr += e.name.substring(e.name.indexOf(\":\")+1) + \",\";	
							}
					    }
		
						if (tmpStr.length > 0) 
						{
							if(confirm('Bạn thực sự muốn kích hoạt mục đã chọn?')==true)
				   			{
								tmpStr=tmpStr.substring(0,tmpStr.length-1);
								document.frmstList.action =\"\";
								document.frmstList.Update.value=tmpStr;
								document.frmstList.process.value='enable';
								document.frmstList.submit() ;
							}
						}
						else
						{
						     alert('Bạn hãy chọn ít nhất một mục để kích hoạt!');
						}
					
				}
				
				function OnDisable()
			    {
				   var tmpStr;
				   tmpStr=new String('');
				   
				       for (var i = 0; i < document.frmstList.elements.length; i++)	
					   {
							var e = document.frmstList.elements[i];
							if ((e.name.indexOf('OnIs:')==0) && e.checked) 
							{
								tmpStr += e.name.substring(e.name.indexOf(\":\")+1) + \",\";	
							}
					    }
		
						if (tmpStr.length > 0) 
						{
							if(confirm('Bạn thực sự muốn ngừng kích hoạt mục đã chọn?')==true)
				   		    {
								tmpStr=tmpStr.substring(0,tmpStr.length-1);
								document.frmstList.action =\"\";
								document.frmstList.Update.value=tmpStr;
								document.frmstList.process.value='disable';
								document.frmstList.submit() ;
							}
						}
						else
						{
						     alert('Bạn hãy chọn ít nhất một mục để ngừng kích hoạt!');
						}
					
				}
				</script>";		
			
		return $text;
	}
	
	
	/*-------------------------------------------
	 | GET ACTION 
	+---------------------------------------------*/
	function get_input()
	{
		global $str;
		$this->err = "";
		$this->page = isset($_POST['curPg']) ? intval($_POST['curPg']) : 1;
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";
		$this->frmValue['update'] = isset($_POST['Update'])? $str->input($_POST['Update']) : "";		
		
	 }
		
	
}

?>
