<?php
/*----------------------------------------
| LIST OF SERVICE PLAN
------------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{
	
	public $title = "Danh sách gói dịch vụ";
	public $text = "";	
	private $process = "";
	private $frmValue = array( 
								 'update'  => '',
								 'type' => ''	
							 );
	private $warning = "";
	private $err = "";
	private $page = 1;

	
    function __construct()
    {
	    global $str, $sess, $db;
	    $this->get_input();
		
		if ($this->process == 'delSP')
	 	{    
		    $arry = preg_split('/[,]/', $this->frmValue['update'], -1, PREG_SPLIT_DELIM_CAPTURE);
			$count = count($arry);
			for ($i=0 ; $i < $count ; $i++)
			{   
			   	$db->do_delete('service_plan', 'id="'. $arry[$i] .'"');			
	        }
			
	 	}
	  
		$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning ."</div>". $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
	}
	
	/*-----------------------
	| SHOW FORM
	+ -----------------------*/
	function show_form($frmValue)
	{
		global $frm, $db, $sess, $token, $hpaging, $dsp;		
				
	    $start = (($this->page - 1)*10); 
	    $query1 = $db->simple_select("*", "service_plan", "", "id DESC");
		$query = $db->simple_select("*", "service_plan", "", "id DESC", $start .",10");
		
		// Chose product type to filter categories
		if ($frmValue['type'] > 0)
		{
			$query1 = $db->simple_select("*", "service_plan", "service_id = '". $frmValue['type'] ."'", "id DESC" );
			$query = $db->simple_select("*", "service_plan", "service_id = '". $frmValue['type'] ."'", "id DESC", $start .",10");
		}
		
		$result1 = $db->query($query1);
		$num_rows = mysql_num_rows($result1);
		$paging_show = $hpaging->paging_section_h($num_rows, $this->page, 5, 10);

		
		$result = $db->query( $query );	  
		$count = 0;		
		if ($this->page > 1 && mysql_num_rows($result) == 0)
		{
			$str->goto_url('?mod=spList');
		}
		
		$text = $frm->draw_form("", "", 2, "POST", "frmSP");
		$text .= $frm->draw_hidden("process", "delSP");
		$text .= $frm->draw_hidden("curPg", "1");
		
		$text .= "<div class='div_add'>
					  <img src='".ADMIN_IMG."add.png' border='0'/>
					  <a href='?mod=spAdd' class='topadd'>Thêm mới</a>
					  &nbsp;|&nbsp;				  
					  <img src='".ADMIN_IMG."new.png' border='0'/>
					  <a href='javascript:OnDelete();' class='topadd'>Xóa mục chọn</a>
					  &nbsp;|&nbsp;
					  <img src='".ADMIN_IMG."refresh.png' border='0'/>
					  <a href='?mod=spList' class='topadd'>Refresh</a>
				  </div>";
		
		// Get list of product section to filter
		$text .= "<div class='div_cat'>Loại dịch vụ : ";
			$text .= $dsp->showServicesList('type', $frmValue['type'], 'Change()', 1);
		$text .= "</div><br/>";		  
				  
		$text .= "<table cellspacing='0' cellpadding='6' class='tbl_main' align='center'>";
		
		$text .= "<tr class='trc'>
						<td>Tên gói dịch vụ</td>
						<td>Loại dịch vụ</td>
						<td>Hiện ở trang chủ</td>
						<td>Thông tin giá</td>
						<td>Frontpage Url</td>
						<td>Cho phép đăng kí</td>
						<td>Link đặt hàng</td>
						<td>Chọn xóa&nbsp;<input type='checkbox' name='banid' value='ON' onclick='CheckAll();'> </td>
				   </tr>";

		while ($row = $db->fetch_array($result))
		{   
		    $count += 1; 		  
		    $text .= "<tr class=".( $count%2 ? "ho" : "hr" ).">
					  <td>". $row['title'] ."</td>";
			// Show product type
			$type= $db->getData('SELECT id,title FROM service WHERE id="'.$row['service_id'].'"');
			$text .= '<td><a href="?mod=serviceEdit&id='.$type['id'].'" class="external">'.$type['title'].'</a></td>';		
			$text .= '<td align="center">'.($row['showhome'] == 1 ? '<img src="'.ADMIN_IMG.'typical.jpg" border="0" />' : '').'</td>';			
			$text .= '<td>'.$row['price'].'</td>';
			$text .= '<td><a href="index.php?mod=sp&id='.$row['id'].'" target="_blank" class="external">index.php?mod=sp&id='.$row['id'].'</a></td>';	
			$text .= '<td align="center">'.($row['allow_order'] == 1 ? '<img src="'.ADMIN_IMG.'typical.jpg" border="0" />' : '').'</td>';				
			if ($row['allow_order'] == 1)
			{
				$text .= '<td><a href="index.php?mod=order&spid='.$row['id'].'" target="_blank" class="external">index.php?mod=order&spid='.$row['id'].'</a></td>';			
			}
			else
			{
				$text .= '<td>&nbsp;</td>';	
			}
			
			// Generate back link
			$rLink = '?mod=spList';
			if ($this->page > 1) $rLink .= '&page='.$this->page;
			if ($frmValue['type'] > 0) $rLink .= '&type='.$frmValue['type'];
			$text .= "<td>
						  <input type='checkbox' name='OnIs:". $row['id'] ."' value='". $row['id'] ."' onclick='docheckone()'> 
						&nbsp;
						<a href='?mod=spEdit&id=".$row['id'].'&r='.urlencode($rLink)."' class='style10'>Hiệu chỉnh </a>&nbsp;
					  </td>					  
					  </tr>";
		}	
		
		$text .= "</table>
					<div class='div_page'>
					<input type='button' name='disable' value='Xóa mục chọn'  onclick='OnDelete()';>&nbsp;&nbsp;
					</div>
					<input type='hidden' name='Update' value=''>";
		
		$text .= "</form>";		  
	    $text .= "<div align='center'>". $paging_show ."</div>";			
	
	// JS for delete action
	$text .= "<script language='JavaScript'>
	          function Change()
			  {
				  document.frmSP.process.value = '';
				  document.frmSP.submit();
			  }
			  
			  function GotoPage(p)
			  { 
				  document.frmSP.curPg.value=p;
				  document.frmSP.process.value='';
				  document.frmSP.submit();
			  }
				
			  function CheckAll()
			  {	
				  for (var i = 0; i < document.frmSP.elements.length; i++)	
				  {
					  var e = document.frmSP.elements[i];
					  if (e.name.indexOf('OnIs:')==0)  
					  {
						  e.checked=document.frmSP.banid.checked;
					  }
				  }
			  }
		
			  function docheckone()
			  {
				  var isChecked=true;
				  for (var i = 0; i < document.frmSP.elements.length; i++)	
				  {
					  var e = document.frmSP.elements[i];
					  if (e.name.indexOf('OnIs:')==0)  
					  {   
					      if(e.checked==false)
						  isChecked=false;
					  }  
				  }
								
				   document.frmSP.banid.checked=isChecked;
			   }		
		
			   function OnDelete()
			   {
				   var tmpStr;
				   tmpStr=new String('');
				   
				       for (var i = 0; i < document.frmSP.elements.length; i++)	
					   {
							var e = document.frmSP.elements[i];
							if ((e.name.indexOf('OnIs:')==0) && e.checked) 
							{
								tmpStr += e.name.substring(e.name.indexOf(\":\")+1) + \",\";	
							}
					    }
		
						if (tmpStr.length > 0) 
						{
							if(confirm('Bạn thực sự muốn xóa mục đã chọn?')==true)
				   			{
								tmpStr=tmpStr.substring(0,tmpStr.length-1);
								document.frmSP.action =\"\";
								document.frmSP.Update.value=tmpStr;
								document.frmSP.process.value='delSP';
								document.frmSP.submit() ;
							}
						}
						else
						{
						     alert('Bạn hãy chọn ít nhất một mục để xóa!');
						}
					
				}				
				</script>";		
			
		return $text;
	}
	
	
	/*-------------------------------------------
	 | GET ACTION 
	+---------------------------------------------*/
	function get_input()
	{
		global $str;
		$this->err = "";
		$this->page = isset($_POST['curPg']) ? intval($_POST['curPg']) : (isset($_GET['page']) ? intval($_GET['page']) : 1);
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";
		$this->frmValue['update'] = isset($_POST['Update'])? $str->input($_POST['Update']) : "";
		$this->frmValue['type'] = isset($_POST['type'])? intval($_POST['type']) : (isset($_GET['type']) ? $str->input($_GET['type']) : "");
		
	}
		
	
}

?>