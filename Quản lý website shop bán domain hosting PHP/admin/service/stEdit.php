<?php
/*----------------------------------------
| EDIT SERVICE TYPE
------------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public $title = "Hiệu chỉnh danh mục dịch vụ";
	public $text = "";	
	private $process = "";
	private $catID = 0;
	private $frmValue = array(
								  'name'    => '',
								  'active'  => ''
							  );


	private $warning = "";
	private $err = "";
	
		
	function __construct()
	{
		global $str, $sess, $token ;		
		$this->catID = isset($_GET['id']) ? $_GET['id'] : '';		
		$this->get_input();
		$check_input = $this->check_input($this->frmValue);
		
		if ($this->process == "editST")
		{
			if ($check_input)
			{
			    $this->update_field($this->frmValue);
				if (isset($_GET['r']) && strlen($_GET['r']) > 0)
				{
					$str->goto_url(urldecode($_GET['r']));	
				}
				$str->goto_url('?mod=stList');
				
			}
			else
			{
				$this->warning = "<b><u>Lỗi nhập liệu</u>:&nbsp;</b><span class='span_err'><ul>". $this->err ."</ul></span>";
			}
		} 
		
		$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
	}	
	
	/*--------------------------------
	 | SHOW FORM
	+ --------------------------------*/
	function show_form($frmValue)
	{
		global $frm, $db, $sess;
		
		$query = $db->simple_select('*', 'service_type', 'id="'. $this->catID .'"');
		$result = $db->query($query);
		$row = $db->fetch_assoc($result);
		
		$text = $frm->draw_form("", "", 2, "POST", "frm_typeEdit");
		$text .= $frm->draw_hidden("process", "editST");
		$text .= "<table cellspacing='0' cellspadding='4' class='tbl_edit'>
				  <tr><td colspan='2' height='10'></td></tr>";
		
		$text .= "<tr>";
		$text .= "<td>Tên danh mục : </td>";
		$text .= "<td>". $frm->draw_textfield("name", $frmValue["name"] ? $frmValue["name"] : $row['name'], "", "50", "255") ."</td></tr>
				  <tr><td colspan='2' height='6'></td></tr>";
		
		$text .= "<tr>";
		$text .= "<td>Kích hoạt: </td>";
		$text .= "<td>". $frm->draw_checkbox("active", "1","", ( ( $frmValue["active"]==1 )||( $row['active'] ) ) ? "checked" : "") ."</td></tr>
				<tr><td colspan='2' height='6'></td></tr>";
			
		$text .= "<tr>";
		$text .= "<td colspan='2' align=center>";
		$text .= $frm->draw_submit(" Cập nhật ", "");
		$text .= "&nbsp;&nbsp;<input type='reset' value=' Hủy '></td>";
		$text .= "</tr>";			
		$text .= "</table>";		
		$text .= "</form>";		
		return $text;
	}
	
	
	/*--------------------------------------------------
	 | GET INPUT DATA
	+---------------------------------------------------*/
	function get_input()
	{
		global $str;
		
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";	
		$this->frmValue['name'] = isset($_POST['name']) ? $str->input($_POST['name']) : "";
	    $this->frmValue['active'] = isset($_POST['active']) ? 1 : 0;
			
	}
	
	
	/*-----------------------------------------------
	 | CHECK INPUT DATA
	+-------------------------------------------------*/
	function check_input($frmValue)
	{
		global $frm;		
		$no_error = true;		
			
		if (!$frm->check_input($frmValue['name'], 1))
		{
			$no_error = false;
			$this->err .= "<li>Hãy nhập tên danh mục</li>";
		}
		
		return $no_error;
	}
	
	
	
	/*-----------------------------------------------
	 | UPDATE INFORMATION
	+------------------------------------------------*/
	function update_field($frmValue)
	{
		global $db, $time;		
		
		$arr = array(   
						'name'    => $frmValue['name'],
						'active'  => $frmValue['active']
				    );
		
		 $db->do_update("service_type", $arr, 'id="'.$this->catID.'"');
		 return true;
	
	}
	
	
}

?>
