<?php
/*----------------------------------------
| LIST OF SERVICES
------------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{
	
	public $title = "Danh sách loại dịch vụ";
	public $text = "";	
	private $process = "";
	private $frmValue = array( 
								 'update'  => '',
								 'type' => ''	
							 );
	private $warning = "";
	private $err = "";
	private $page = 1;

	
    function __construct()
    {
	    global $str, $sess, $db;
	    $this->get_input();
		
		if ($this->process == 'delService')
	 	{    
		    $arry = preg_split('/[,]/', $this->frmValue['update'], -1, PREG_SPLIT_DELIM_CAPTURE);
			$count = count($arry);
			for ($i=0 ; $i < $count ; $i++)
			{   
			   	$db->do_delete('service', 'id="'. $arry[$i] .'"');			
	        }
			
	 	}
	  
		$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning ."</div>". $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
	}
	
	/*-----------------------
	| SHOW FORM
	+ -----------------------*/
	function show_form($frmValue)
	{
		global $frm, $db, $sess, $token, $hpaging;		
				
	    $start = (($this->page - 1)*10); 
	    $query1 = $db->simple_select("*", "service", "", "rank ASC, id DESC");
		$query = $db->simple_select("*", "service", "", "rank ASC, id DESC", $start .",10");
		
		// Chose product type to filter categories
		if ($frmValue['type'])
		{
			$query1 = $db->simple_select("*", "service", "type = '". $frmValue['type'] ."'", "rank ASC, id DESC" );
			$query = $db->simple_select("*", "service", "type = '". $frmValue['type'] ."'", "rank ASC, id DESC", $start .",10");
		}
		
		$result1 = $db->query($query1);
		$num_rows = mysql_num_rows($result1);
		$paging_show = $hpaging->paging_section_h($num_rows, $this->page, 5, 10);

		
		$result = $db->query( $query );	  
		$count = 0;		
		if ($this->page > 1 && mysql_num_rows($result) == 0)
		{
			$str->goto_url('?mod=serviceList');
		}
		
		$text = $frm->draw_form("", "", 2, "POST", "frmService");
		$text .= $frm->draw_hidden("process", "delService");
		$text .= $frm->draw_hidden("curPg", "1");
		
		$text .= "<div class='div_add'>
					  <img src='".ADMIN_IMG."add.png' border='0'/>
					  <a href='?mod=serviceAdd' class='topadd'>Thêm mới</a>
					  &nbsp;|&nbsp;				  
					  <img src='".ADMIN_IMG."new.png' border='0'/>
					  <a href='javascript:OnDelete();' class='topadd'>Xóa mục chọn</a>
					  &nbsp;|&nbsp;
					  <img src='".ADMIN_IMG."refresh.png' border='0'/>
					  <a href='?mod=serviceList' class='topadd'>Refresh</a>
				  </div>";
		
		// Get list of product section to filter
		$sql_section = $db->simple_select('id, name', 'service_type', '', 'name ASC');
		$text .= "<div class='div_cat'>Danh mục : ";
			$text .= $frm->draw_select_dynamic($sql_section, 'type', $frmValue['type'], 'Change()', 1);
		$text .= "</div><br/>";		  
				  
		$text .= "<table cellspacing='0' cellpadding='6' class='tbl_main' align='center'>";
		
		$text .= "	<tr class='trc'>
						<td>Tên loại dịch vụ</td>
						<td>Danh mục</td>
						<td>Cho phép đăng kí</td>
						<td>Thứ tự</td>
						<td>Chọn xóa&nbsp;<input type='checkbox' name='banid' value='ON' onclick='CheckAll();'> </td>
				   </tr>";

		while ($row = $db->fetch_array($result))
		{   
		    $count += 1; 		  
		    $text .= "<tr class=".( $count%2 ? "ho" : "hr" ).">
					  <td>". $row['title'] ."</td>";
			// Show product type
			$type= $db->getData($db->simple_select('id,name','service_type', 'id="'. $row['type'] .'"'));
			$text .= '<td><a href="?mod=stEdit&id='.$type['id'].'">'.$type['name'].'</a></td>';		
			$text .= '<td align="center">'.($row['allow_order'] == 1 ? '<img src="'.ADMIN_IMG.'typical.jpg" border="0" />' : '').'</td>';
			$text .= '<td align="center">'.$row['rank'].'</td>';			
			
			// Generate back link
			$rLink = '?mod=serviceList';
			if ($this->page > 1) $rLink .= '&page='.$this->page;
			if ($frmValue['type'] > 0) $rLink .= '&type='.$frmValue['type'];
			$text .= "<td>
						  <input type='checkbox' name='OnIs:". $row['id'] ."' value='". $row['id'] ."' onclick='docheckone()'> 
						&nbsp;
						<a href='?mod=serviceEdit&id=".$row['id'].'&r='.urlencode($rLink)."' class='style10'>Hiệu chỉnh </a>&nbsp;
						  </td>
					  </tr>";
		}	
		
		$text .= "</table>
					<div class='div_page'>
					<input type='button' name='disable' value='Xóa mục chọn'  onclick='OnDelete()';>&nbsp;&nbsp;
					</div>
					<input type='hidden' name='Update' value=''>";
		
		$text .= "</form>";		  
	    $text .= "<div align='center'>". $paging_show ."</div>";			
	
	// JS for delete action
	$text .= "<script language='JavaScript'>
	          function Change()
			  {
				  document.frmService.process.value = '';
				  document.frmService.submit();
			  }
			  
			  function GotoPage(p)
			  { 
				  document.frmService.curPg.value=p;
				  document.frmService.process.value='';
				  document.frmService.submit();
			  }
				
			  function CheckAll()
			  {	
				  for (var i = 0; i < document.frmService.elements.length; i++)	
				  {
					  var e = document.frmService.elements[i];
					  if (e.name.indexOf('OnIs:')==0)  
					  {
						  e.checked=document.frmService.banid.checked;
					  }
				  }
			  }
		
			  function docheckone()
			  {
				  var isChecked=true;
				  for (var i = 0; i < document.frmService.elements.length; i++)	
				  {
					  var e = document.frmService.elements[i];
					  if (e.name.indexOf('OnIs:')==0)  
					  {   
					      if(e.checked==false)
						  isChecked=false;
					  }  
				  }
								
				   document.frmService.banid.checked=isChecked;
			   }		
		
			   function OnDelete()
			   {
				   var tmpStr;
				   tmpStr=new String('');
				   
				       for (var i = 0; i < document.frmService.elements.length; i++)	
					   {
							var e = document.frmService.elements[i];
							if ((e.name.indexOf('OnIs:')==0) && e.checked) 
							{
								tmpStr += e.name.substring(e.name.indexOf(\":\")+1) + \",\";	
							}
					    }
		
						if (tmpStr.length > 0) 
						{
							if(confirm('Bạn thực sự muốn xóa mục đã chọn?')==true)
				   			{
								tmpStr=tmpStr.substring(0,tmpStr.length-1);
								document.frmService.action =\"\";
								document.frmService.Update.value=tmpStr;
								document.frmService.process.value='delService';
								document.frmService.submit() ;
							}
						}
						else
						{
						     alert('Bạn hãy chọn ít nhất một mục để xóa!');
						}
					
				}				
				</script>";		
			
		return $text;
	}
	
	
	/*-------------------------------------------
	 | GET ACTION 
	+---------------------------------------------*/
	function get_input()
	{
		global $str;
		$this->err = "";
		$this->page = isset($_POST['curPg']) ? intval($_POST['curPg']) : (isset($_GET['page']) ? intval($_GET['page']) : 1);
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";
		$this->frmValue['update'] = isset($_POST['Update'])? $str->input($_POST['Update']) : "";
		$this->frmValue['type'] = isset($_POST['type'])? intval($_POST['type']) : (isset($_GET['type']) ? $str->input($_GET['type']) : "");
		
	  }
		
	
}

?>