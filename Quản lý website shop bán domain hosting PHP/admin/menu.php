<?php
/*-----------------------------------------------
 | LIST OF LINK FOR ADMIN PANEL
 -----------------------------------------------*/
 
// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}
$info = $sess->username ? $sess->username : "Guest";
/* 
	| 'key'=>'' : Caption of group menu (without link)
	| 'key'=>'value': title of menu with link
 */
$menu=array(

'THIẾT LẬP SITE' => '',
'Thiết lập chính' => 'setting',
'Thiết lập phân trang' => 'pagingSetting',

'NGÔN NGỮ' => '',
'Thiết lập ngôn ngữ' => 'languageList',

'FILE TĨNH' => '',
'Thông tin liên hệ' => 'information',
'Loại file tĩnh' => 'htmltypeList',
'Thêm loại file tĩnh' => 'htmltypeAdd',
'Danh sách file tĩnh' => 'htmlList',
'Thêm file tĩnh' => 'htmlAdd',

'KHÁCH HÀNG' => '',
'Danh sách liên hệ' => 'contactList',
'Danh sách guestbook' => 'guestbookList',
'Danh sách khách hàng' => 'customerList',
'Thêm khách hàng' => 'customerAdd',

'SẢN PHẨM' => '',
'Phân mục' => 'productsecList',
'Thêm phân mục' => 'productsecAdd',
'Danh mục loại SP' => 'producttypeList',
'Thêm loại SP' => 'producttypeAdd',
'Danh sách sản phẩm' => 'productList',
'Thêm sản phẩm' => 'productAdd',
'Đơn hàng sản phẩm' => 'productorderlist',


'QUẢNG CÁO' => '',
'Các vị trí' => 'adposList',
'Thêm vị trí' => 'adposAdd',
'Danh sách quảng cáo' => 'advertList',
'Thêm quảng cáo' => 'advertAdd',

'FAQ' => '',
'Danh sách faq' => 'faqList',
'Thêm faq' => 'faqAdd',

'TIN CNTT' => '',
'Loại tin CN' => 'itnewstypeList',
'Thêm loại tin CN' => 'itnewstypeAdd',
'Danh sách tin CN' => 'itnewsList',
'Thêm tin CN' => 'itnewsAdd',

'HƯỚNG DẪN-TUT' => '',
'Danh mục hướng dẫn' => 'webtuttypeList',
'Thêm mục hướng dẫn' => 'webtuttypeAdd',
'Danh sách hướng dẫn' => 'webtutList',
'Thêm bài hướng dẫn' => 'webtutAdd',
'Nhận xét bài hướng dẫn' => 'tutcommentlist',

'LIÊN KẾT WEB' => '',
'Danh sách liên kết' => 'weblinkList',
'Thêm liên kết' => 'weblinkAdd',

'PHẦN MỀM' => '',
'Loại phần mềm' => 'softtypeList',
'Thêm loại phần mềm' => 'softtypeAdd',
'Danh sách phần mềm' => 'softList',
'Thêm phần mềm mới' => 'softAdd',

);



?>
