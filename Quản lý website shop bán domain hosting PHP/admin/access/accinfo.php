<?php
/*------------------------------------------------
| UPDATE ACCESS INFORMATION AND INSTALL SETTING
--------------------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access</h1>';
	exit();
}

$cnt = new content();

class content{
	
	var $process = '';
	var $title = 'Thông tin truy cập và cài đặt website';
	var $text = '';
	var $warning = '';
	var $frmValue = array(
							   'install_date' 		=> '',
							   'today_online' 		=> '',
							   'most_online' 		=> '',
							   'most_online_date' 	=> '',
							   'hitcounter' 		=> '',
							   'online_now' 		=> '',
						 );

	function content()
	{
	 	global $frm, $db;
		
		 $this->get_input();
		   if ( $this->process == "editInfos" )	
		   {  
			 	$this->update_field($this->frmValue);
			    $this->warning = "<span class='span_ok'>Cập nhật thông tin truy cập thành công !!</span>";
		   }
			
		   $this->text = ($this->warning) ? "<div id='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);	
	}	
		
	
	/*---------------------------------
	| SHOW FORM 
	---------------------------------*/
	function show_form($frmValue)
	{
		global $db, $frm, $str, $sess, $time;
		
		$text = '';
		// Get access information from DB		
		$query_infos = $db->simple_select("*", "statistics");
		$result_infos = $db->query($query_infos);
		
		while ($infos = $db->fetch_assoc($result_infos))
		{ 
			$statistic[$infos['keyword']] = $infos['value'];
		}		
		
		// Some stuffs for calendar
		$text .= "<script type='text/javascript' src='". URL . DIR_LIB ."jscalendar/calendar.js'></script>
				  <script type='text/javascript' src='". URL . DIR_LIB ."jscalendar/lang/calendar-en.js'></script>
				  <script type='text/javascript' src='". URL . DIR_LIB ."jscalendar/calendar-setup.js'></script>
				  <link rel='stylesheet' type='text/css' href='". URL . DIR_LIB ."jscalendar/calendar.css'>";
				
		// Show infos to screen
		$text .= $frm->draw_form("", "", 2, "POST", "frm_access");
		$text .= "<table cellspacing='0' cellpadding='4' class='tbl_edit'>
				  <tr><td colspan='2' height='10'></td></tr>";
		$text .= $frm->draw_hidden("process", "editInfos");
			
		$text .= "<tr>";
		$text .= "<td>Ngày cài đặt : </td>";
		$text .= "<td>
		          <input type='text' name='install_date' id='install_date' value='". ( $frmValue["install_date"] ? $frmValue["install_date"] : $time->unixtime_2_ddmmyyyy($statistic['install_date'])) ."'  onblur='parseDate(this, \"%d/%m/%Y\");'>
				  <img src='". URL . DIR_LIB ."jscalendar/jscalendar.gif' alt='Nhấp chọn ngày tháng'  id='jscal_trigger' align='absmiddle'>
				  <script type='text/javascript'>
   						Calendar.setup ({
											inputField : 'install_date', ifFormat : '%d/%m/%Y', showsTime : false, button : 'jscal_trigger', singleClick : true, step : 1
		});
					</script>
				  </td></tr>
				  <tr><td colspan='2' height='6'></td></tr>";		
				
		$text .= "<tr>";
		$text .= "<td>Trực tuyến hôm nay : </td>";
		$text .= "<td>". $frm->draw_textfield("today_online", $frmValue["today_online"] ? $frmValue["today_online"] : $statistic['today_online'],"","20","15") ."</td></tr>
				 <tr><td colspan='2' height='6'></td></tr>";		
		
		$text .= "<tr>";
		$text .= "<td>Trực tuyến nhiều nhất/ngày : </td>";
		$text .= "<td>". $frm->draw_textfield("most_online", $frmValue["most_online"] ? $frmValue["most_online"] : $statistic['most_online'],"","20","15") ."</td></tr>
				 <tr><td colspan='2' height='6'></td></tr>";		
		
		$text .= "<tr>";
		$text .= "<td>Ngày trực tuyến nhiều nhất : </td>";
		$text .= "<td>
		          <input type='text' name='most_online_date' id='most_online_date' value='". ( $frmValue["most_online_date"] ? $frmValue["most_online_date"] : $time->unixtime_2_ddmmyyyy($statistic['most_online_date'])) ."'  onblur='parseDate(this, \"%d/%m/%Y\");'>
				  <img src='". URL . DIR_LIB ."jscalendar/jscalendar.gif' alt='Nhấp chọn ngày tháng'  id='jscal_trigger' align='absmiddle'>
				  <script type='text/javascript'>
   						Calendar.setup ({
											inputField : 'most_online_date', ifFormat : '%d/%m/%Y', showsTime : false, button : 'jscal_trigger', singleClick : true, step : 1
		});
					</script>
				  </td></tr>
				  <tr><td colspan='2' height='6'></td></tr>";		
		
		$text .= "<tr>";
		$text .= "<td>Lượt truy cập : </td>";
		$text .= "<td>". $frm->draw_textfield("hitcounter", $frmValue["hitcounter"] ? $frmValue["hitcounter"] : $statistic['hitcounter'],"","20","15") ."</td></tr>
				 <tr><td colspan='2' height='6'></td></tr>";
		
		$text .= "<tr>";
		$text .= "<td>Trực tuyến : </td>";
		$text .= "<td>". $frm->draw_textfield("online_now", $frmValue["online_now"] ? $frmValue["online_now"] : $statistic['online_now'],"","20","15") ."</td></tr>
				 <tr><td colspan='2' height='6'></td></tr>";		
			
		
		$text .= "<tr>";
		$text .= "<td colspan='2' align=center>";
		$text .= $frm->draw_submit(" Cập nhật ", "");
		$text .= "&nbsp;&nbsp;<input type='reset' value=' Hủy '></td>";
		$text .= "</tr>";			
		$text .= "</table>";		
		$text .= "</form>";	
		
		return $text;

	}
	
	/*--------------------------------------------
	| GET INPUT DATA
	---------------------------------------------*/
	function get_input()
	{ 
		global $str;
	    $this->process = isset($_POST['process']) ? $_POST['process'] : "";
	  	$this->frmValue['install_date'] = isset($_POST['install_date']) ? $str->input($_POST['install_date']) : "";
		$this->frmValue['today_online'] = isset($_POST['today_online']) ? $str->input($_POST['today_online']) : "";
		$this->frmValue['most_online'] = isset($_POST['most_online']) ? $str->input($_POST['most_online']) : "";
		$this->frmValue['most_online_date'] = isset($_POST['most_online_date']) ? $str->input($_POST['most_online_date']) : "";
		$this->frmValue['hitcounter'] = isset($_POST['hitcounter']) ? $str->input($_POST['hitcounter']) : "";
		$this->frmValue['online_now'] = isset($_POST['online_now']) ? $str->input($_POST['online_now']) : "";
		
	}
	
	/*-------------------------------------------
	| UPDATE DATA
	 ------------------------------------------*/
	 function update_field($frmValue)
	 {
	   	global $db, $time;
	    foreach ( $frmValue as $key => $value )
		{   
			$inDateFormat = array('install_date','most_online_date');
			if (in_array($key, $inDateFormat))
			{
				$arr = array('value'  => $time->ddmmyyyy_2_unixtime($value));			
			}
			else
			{
				$arr = array('value'  => $value);
			}
			$db->do_update( "statistics" , $arr , "keyword='". $key ."'" );
		}
		return true;
	   
	  }
	
	
	
}



?>