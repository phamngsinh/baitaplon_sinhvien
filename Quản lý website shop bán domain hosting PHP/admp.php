<?php
/*---------------------------------------------------
*  ADMIN CONTROL PANEL - MAIN PAGE
+-----------------------------------------------------*/

/*
* Start output-buffering handle - by this way, the page won't have header fault
*/
ob_start();
			
// Define constant for security	
define("HCR",true); 
	
// All config for site
require("configure.php"); 
	
/*-----------------------------------------
| ALL NEEDED CLASS
+-----------------------------------------*/
require_once DIR_CLASS."string.php";
$str = new str;
require_once DIR_CLASS."token.php";
$token = new token;
require_once DIR_CLASS."time.php";
$time = new timer;
require_once DIR_CLASS."form.php";
$frm = new form;
require_once DIR_CLASS."display.php";
$dsp = new display;
require_once DIR_CLASS."crypt.php";
$crypt = new crypter;
require_once DIR_CLASS."db_mysql.php";
$db = new database;
require_once DIR_CLASS."session.php";
$sess = new session;
require_once DIR_CLASS."paging.php";
$hpaging=new hpaging;
require_once DIR_LIB."fckeditor/fckeditor.php";
require_once DIR_ADMIN.'menu.php';

// Include class to handle multi category
require_once DIR_CLASS."category.php";
$catV = new catVisualize();

/*------------------------------------
| LANGUAGE PORTION
+-------------------------------------*/
//require(DIR_LANG."language.php");
//$lg = new language('vn');
//require("language/".$sess->language.".php");
// Only Vietnamese language for Admin Panel
require("language/vn.php");

require(DIR_SOURCE."main.php");
$main = new main;
$sess->now += $main->setting['TimeZone'];
	
	
	
	
/*----------------------------------------
| DEFINE CONTENT FOR BODY
+-----------------------------------------*/
$center = $token->admin();
$showLoginForm = FALSE;
if ($sess->groups_id != 1)
{
	$center = "login.php";
	$showLoginForm = TRUE;
	
}

require(DIR_ADMIN.$center);



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Administrator Control Panel</title>
	<link href="<?=URL.DIR_ADMIN?>style/hdiv.css" rel="stylesheet" type="text/css" />
</head>
<body class="body">

<?php

if (!$showLoginForm)
{

?>
	<div class="main">
	
	<table cellpadding="0" cellspacing="0" border="0" style="width:100%">
	<tr><td>
	
		<!--Head-->	
		<div class="footer2"></div>	
		<div class="header_main">
		<span class="style7"><img src="<?=ADMIN_IMG?>png2.png" align="middle"/>&nbsp;MEGAHOST.VN - ADMIN CONTROL PANEL</span>
		</div>
		<div class="header1"></div>
		<div style="text-align:center; padding:3px; background-color:#FFFFCC">
			<img src="<?=ADMIN_IMG?>h6.gif"/>&nbsp;<a href="admp.php" class="style3">Trang chính quản trị</a>&nbsp;|&nbsp;
			<img src="<?=ADMIN_IMG?>h6.gif"/>&nbsp;<a href="index.php" class="style3" target="_blank">Xem trang chủ</a>&nbsp;|&nbsp;
			<img src="<?=ADMIN_IMG?>h6.gif" />&nbsp;<a href="?mod=changepass" class="style3">Đổi mật khẩu</a>&nbsp;|&nbsp;
			<img src="<?=ADMIN_IMG?>h6.gif" />&nbsp;<a href="?mod=logout" class="style3">Logout</a>
		</div>
		<!--end head-->
		<div class="div_blank"></div>
	
	</td></tr>
	<tr><td>
	
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<!--Phan Left-->
		<td width="170" align="left" valign="top" style="border:#999999 solid 1px;">
		<div class="left">
		<!--Thong tin dang nhap-->
		<div class="left_title1"></div>
		<div class="left_title2">
		<span class="style2"><img src="<?=ADMIN_IMG?>s4.gif" vspace="2" />&nbsp;Thông tin đăng nhập </span>
		</div>
		<div class="left_title3"></div>
		<div class="left_menu1"></div>
		<div class="left_menu">&nbsp;&nbsp;Hi!, <?=$sess->username ? $sess->username : "Guest"?></div>
		<div class="left_menu1"></div>		
		
		<div class="left_title1"></div>	
		
		<?php
		// LIST OF ADMIN MENU COME FROM THIS FILE
		require_once DIR_ADMIN.'menulist.php';	
		?>
		
		</div>
		</td>
		<!--End left-->
		
		<!--Main-->
		<td valign="top">
		<!--Bar slogan-->
		<div class="bar1"></div>
		<div class='bar'>
		<div class='bar_left'>
		<span class="style3"><strong><?=$cnt->title?></strong></span>
		</div>
		<div class='bar_right'>
		<span class="style3"><?=$time->unixtime_2_ddmmyyyy($sess->now)?></span>
		</div>
		</div>
		<div class="bar2"></div>
		<!--End bar slogan-->
		
		<!--Main content-->
		<div class="div_content">
		<?=$cnt->text?>
		</div>
		<!--End main content-->
		
		</td>
		<!--End main-->
		</tr>
		</table>
		
	</td></tr>
	<tr><td>
	
		<div class="div_blank"></div>
		<!--Footer-->
		<div class="header1"></div>
		<div class="footer">
		 Copyrights &copy; 2010 by MEGAHOST. All right reserved.<br/>		 
		</div>
		<div class="footer2"></div>
		<!--End footer-->
	
	</td></tr>
	</table>
	
	</div>

<?php
}
else
{ // User did not login yet
?>
<br/>
<div class="dloginForm">
<p class="plogintitle">MEGAHOST CONTROL PANEL</p><br/>
<?=$cnt->text?>
</div>

<? 
}
?>	
	
</body>
</html>
<?php

$db->close(); // Close connect to db
ob_end_flush(); // Clean output buffering
?>