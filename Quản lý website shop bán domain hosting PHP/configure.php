<?php
/*===========================================
| DEFINE ALL CONSTANT THAT NEED FOR SITE
+============================================*/
	
// Check for security
if ( !defined('HCR') )
{
	print '<h1>Incorrect Access</h1>';
	exit();
}

///////////////////////////////
// START PAGE
//////////////////////////////
define('URL', 'http://megahost.vn/');

// DEFINE MAIN DIRECTORY
define('DIR_CLASS', 'kernel/');    // Directory hold all main class
define('DIR_SOURCE', 'modules/');  // Directory for modules
define('DIR_LANG', 'language/');   // Language
define('DIR_ADMIN', 'admin/');     // Directory for admin site

// DEFINE SOME MAIN PAGES
define('FILE_INDEX', 'index.php');
define('FILE_AJAX', 'a.php');
define('FILE_ADMIN', 'admp.php');
define('FILE_PRINT', 'print.php/');

// SOME STUFFS RELATE TO IMAGES AND CSS
define('ADMIN_IMG', 'admin/images/');
define('DIR_IMG', 'images/');
define('DIR_UIMG', 'images/');
define('DIR_LIB', 'lib/');
define('DIR_STYLE', 'style/');
define('DIR_JS', 'js/');
define('DIR_TOOLTIP', 'tooltip/');
define('DIR_FUNCTION', 'function/');

// DEFINE URL FOR THUMBNAIL CLASS
define('THUMBNAIL_CLASS', DIR_LIB.'thumbnail/');

// DEFINE FOLDER FOR JS LIBRARIES
define('DIR_THICKBOX', 'js/thickbox/');
define('DIR_LIGHTBOX', 'js/lightbox/');
define('DIR_JQUERY', 'js/jquery/');

// DEFINE URL FOR FCK EDITOR
define('DIR_LIB_EDITOR', DIR_LIB.'fckeditor/');

// DEFINE URL FOR CAPTCHA
define('DIR_CAPTCHA', DIR_LIB.'captcha/');

// DB CONNECTION INFORMATIONS
define('MYSQL_DATABASE', 'ten-data');
define('MYSQL_HOST', 'localhost');
define('MYSQL_USERNAME', 'ten-user');
define('MYSQL_PASSWORD', 'ten-pass');
define('MYSQL_PREFIX', '');
define('MYSQL_PORT', '');
define('MYSQL_PERSISTENT', '0');


// SALT STRING TO ENCODE PASS
define('SALT', 'Q1Lr5QNyPCDH');


////////////////////////////////////
// DEFINE SPECIFIED IMAGE PATH
////////////////////////////////////
// Define url for news
define('N_IMG', DIR_IMG.'news/');
define('NT_IMG', N_IMG.'thumb/');

// Image for typical information at home
define('HI_IMG', DIR_IMG.'hinfo/');

// For service introduction
define('SI_IMG', DIR_IMG.'si/');

// Define images url for banner
define('B_IMG', 'libbanner/banner/');


?>
