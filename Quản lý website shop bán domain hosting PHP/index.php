<?php
ob_start(); // Start output buffering
				
define("HCR",true); // Check for security
	
require_once "configure.php"; // COnfiguration file
	
	
/*=========================================
| GET ALL KERNEL CLASS
+==========================================*/
require_once DIR_CLASS."string.php";
$str = new str;
require_once DIR_CLASS."time.php";
$time = new timer;
require_once DIR_CLASS."form.php";
$frm = new form;
require_once DIR_CLASS."display.php";
$dsp = new display;
require_once DIR_CLASS."crypt.php";
$crypt = new crypter;
require_once DIR_CLASS."db_mysql.php";
$db = new database;
require_once DIR_CLASS."session.php";
$sess = new session;
require_once DIR_CLASS."token.php";
$token = new token;
require_once DIR_CLASS."paging.php";
$pagings = new hpaging;
require_once DIR_CLASS."dynamic_form.php";
$dfrm = new dynamic_form;
//require_once DIR_CLASS."paging_information.php";
//$paging_info = new paging_info;
require_once DIR_CLASS."mail.php";
$email = new email;


// Execute token to get content of site
$body = $token->act();

/*================================
| Get language
+=================================*/
//require_once DIR_LANG."language.php";
//$lg = new language('vn');
require(DIR_LANG."vn.php");


/*=================================================
| Get file that hold all needed box for site
+==================================================*/
require_once DIR_SOURCE."panel.php";
$panel = new panel;

/*======================================
| Define content for current token
+=======================================*/
require_once DIR_SOURCE.$token->body;	

/*===================
| DISPLAY CONTENT
+====================*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$cnt->title?>MegaHost.Vn - Hosting share, lưu trữ website, VPS, Dedicated Server, Tên miền .Vn, Tên miền Quốc Tế .Com, .Net, Host sinh viên</title>
<meta name="description" content="<?=$cnt->title?>Đăng Ký Tên miền - Domain – Hosting - Email Hosting – VPS - Virtual Private Server – Thuê Máy Chủ Riêng - Dedicated Server – Chỗ Đặt Máy Chủ - Colocation - Hàng Đầu Việt Nam"> 
<meta name="keywords" content="<?=$cnt->title?>Server riêng, Máy chủ Email, damg ky ten mien quoc te, Đăng ký tên miền Quốc tế, dns manage, dns control, lay lai ten mien, quan ly dns, domain forwarding, an thong tin ten mien, can bang tai may chu, cân bằng tải máy chủ, load balancing, mien phi quan ly dich vu dns, chuyen ten mien, transfer domain, tu van lua chon ten mien, quan ly ten mien">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script language="JavaScript">
<!--
function mmLoadMenus() {
  if (window.mm_menu_0922234615_0) return;
  window.mm_menu_0922234615_0 = new Menu("root",200,23,"Arial, Helvetica, sans-serif",12,"#333333","#333333","#B2E466","#84D50D","left","middle",3,0,300,-5,7,true,true,true,0,false,false);
  <?php
  // Get list of service for domain service type
  $sd_rs = $db->query('SELECT * FROM service WHERE type=3 ORDER BY rank ASC, id DESC');
  while ($sd = mysql_fetch_assoc($sd_rs))  
  { ?>
  	mm_menu_0922234615_0.addMenuItem("<?=str_replace(' ','&nbsp;',$sd['title']);?>","window.open('index.php?mod=st&id=3&cid=<?=$sd['id']?>', '_self');");
  <?php
  }
  ?>
  //mm_menu_0922234615_0.addMenuItem("Ten&nbsp;miền&nbsp;1","window.open('index.php?mod=st&id=3', '_self');");
  //mm_menu_0922234615_0.addMenuItem("Tên&nbsp;miền&nbsp;2","window.open('index.php?mod=st&id=4', '_self');");
   mm_menu_0922234615_0.fontWeight="bold";
   mm_menu_0922234615_0.hideOnMouseOut=true;
   mm_menu_0922234615_0.bgColor='#B2E466';
   mm_menu_0922234615_0.menuBorder=0;
   //mm_menu_0922234615_0.menuLiteBgColor=0;
   mm_menu_0922234615_0.menuBorderBgColor='#BFD2A3';

  window.mm_menu_0922234959_0 = new Menu("root",200,23,"Arial, Helvetica, sans-serif",12,"#333333","#333333","#B2E466","#84D50D","left","middle",3,0,300,-5,7,true,true,true,0,false,false);
  <?php
  // Get list of service for hosting service type
  $sh_rs = $db->query('SELECT * FROM service WHERE type=2 ORDER BY rank ASC, id DESC');
  while ($sh = mysql_fetch_assoc($sh_rs))  
  { ?>
  	mm_menu_0922234959_0.addMenuItem("<?=str_replace(' ','&nbsp;',$sh['title']);?>","window.open('index.php?mod=st&id=2&cid=<?=$sh['id']?>', '_self');");
  <?php
  }
  ?>
  //mm_menu_0922234959_0.addMenuItem("Menu&nbsp;hosting&nbsp;1","window.open('index.php?mod=st', '_self');");
  //mm_menu_0922234959_0.addMenuItem("Menu&nbsp;hosting&nbsp;2","window.open('index.php?mod=st', '_self');");
   mm_menu_0922234959_0.fontWeight="bold";
   mm_menu_0922234959_0.hideOnMouseOut=true;
   mm_menu_0922234959_0.bgColor='#B2E466';
   mm_menu_0922234959_0.menuBorder=0;
  // mm_menu_0922234959_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0922234959_0.menuBorderBgColor='#BFD2A3';

  window.mm_menu_0923001849_0 = new Menu("root",200,23,"Arial, Helvetica, sans-serif",12,"#333333","#333333","#B2E466","#84D50D","left","middle",3,0,300,-5,7,true,true,true,0,false,false);
   <?php
  // Get list of service for server service type
  $ss_rs = $db->query('SELECT * FROM service WHERE type=1 ORDER BY rank ASC, id DESC');
  while ($ss = mysql_fetch_assoc($ss_rs))  
  { ?>
  	mm_menu_0923001849_0.addMenuItem("<?=str_replace(' ','&nbsp;',$ss['title']);?>","window.open('index.php?mod=st&id=1&cid=<?=$ss['id']?>', '_self');");
  <?php
  }
  ?>
  //mm_menu_0923001849_0.addMenuItem("Máy&nbsp;chủ&nbsp;1","window.open('index.php?mod=st', '_self');");
  //mm_menu_0923001849_0.addMenuItem("Máy&nbsp;chủ&nbsp;2","window.open('index.php?mod=st', '_self');");
   mm_menu_0923001849_0.fontWeight="bold";
   mm_menu_0923001849_0.hideOnMouseOut=true;
   mm_menu_0923001849_0.bgColor='#B2E466';
   mm_menu_0923001849_0.menuBorder=0;
   //mm_menu_0923001849_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0923001849_0.menuBorderBgColor='#BFD2A3';

  window.mm_menu_0923002241_0 = new Menu("root",200,23,"Arial, Helvetica, sans-serif",12,"#333333","#333333","#B2E466","#84D50D","left","middle",3,0,300,-5,7,true,true,true,0,false,false);
  <?php
  // Get list of items of guide to register service
  $sr_rs = $db->query('SELECT * FROM register ORDER BY rank ASC, id DESC');
  while ($sr = mysql_fetch_assoc($sr_rs))  
  { ?>
  	mm_menu_0923002241_0.addMenuItem("<?=str_replace(' ','&nbsp;',$sr['title']);?>","window.open('index.php?mod=regi&id=<?=$sr['id']?>', '_self');");
  <?php
  }
  ?>
  //mm_menu_0923002241_0.addMenuItem("Đăng&nbsp;kí&nbsp;1","window.open('index.php?mod=reg', '_self');");
  //mm_menu_0923002241_0.addMenuItem("Đăng&nbsp;kí&nbsp;2","window.open('index.php?mod=reg', '_self');");
   mm_menu_0923002241_0.fontWeight="bold";
   mm_menu_0923002241_0.hideOnMouseOut=true;
   mm_menu_0923002241_0.bgColor='#B2E466';
   mm_menu_0923002241_0.menuBorder=0;
   //mm_menu_0923002241_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0923002241_0.menuBorderBgColor='#BFD2A3';

  window.mm_menu_0923002511_0 = new Menu("root",200,23,"Arial, Helvetica, sans-serif",12,"#333333","#333333","#B2E466","#84D50D","left","middle",3,0,300,-5,7,true,true,true,0,false,false);
  <?php
  // Get list of items of support information
  $su_rs = $db->query('SELECT * FROM support ORDER BY rank ASC, id DESC');
  while ($su = mysql_fetch_assoc($su_rs))  
  { ?>
  	mm_menu_0923002511_0.addMenuItem("<?=str_replace(' ','&nbsp;',$su['title']);?>","window.open('index.php?mod=support&id=<?=$su['id']?>', '_self');");
  <?php
  }
  ?>
  //mm_menu_0923002511_0.addMenuItem("Hỗ&nbsp;trợ&nbsp;1","window.open('index.php?mod=st', '_self');");
  //mm_menu_0923002511_0.addMenuItem("Hỗ&nbsp;trợ&nbsp;2","window.open('index.php?mod=st', '_self');");
   mm_menu_0923002511_0.fontWeight="bold";
   mm_menu_0923002511_0.hideOnMouseOut=true;
   mm_menu_0923002511_0.bgColor='#B2E466';
   mm_menu_0923002511_0.menuBorder=0;
   //mm_menu_0923002511_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0923002511_0.menuBorderBgColor='#BFD2A3';

  window.mm_menu_0923003037_0 = new Menu("root",200,23,"Arial, Helvetica, sans-serif",12,"#333333","#333333","#B2E466","#84D50D","left","middle",3,0,300,-5,7,true,true,true,0,false,false);
  <?php
  // Get list of items of support information
  $si_rs = $db->query('SELECT * FROM infos ORDER BY rank ASC, id DESC');
  while ($si = mysql_fetch_assoc($si_rs))  
  { ?>
  	mm_menu_0923003037_0.addMenuItem("<?=str_replace(' ','&nbsp;',$si['title']);?>","window.open('index.php?mod=rinfo&id=<?=$si['id']?>', '_self');");
  <?php
  }
  ?>
  //mm_menu_0923003037_0.addMenuItem("Thông&nbsp;tin&nbsp;1","window.open('index.php?mod=infos', '_self');");
  //mm_menu_0923003037_0.addMenuItem("Thông&nbsp;tin&nbsp;2","window.open('index.php?mod=infos', '_self');");
   mm_menu_0923003037_0.fontWeight="bold";
   mm_menu_0923003037_0.hideOnMouseOut=true;
   mm_menu_0923003037_0.bgColor='#B2E466';
   mm_menu_0923003037_0.menuBorder=0;
   //mm_menu_0923003037_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0923003037_0.menuBorderBgColor='#BFD2A3';

mm_menu_0923003037_0.writeMenus();
} // mmLoadMenus()
//-->
</script>
<script language="JavaScript" src="mm_menu.js"></script>
</head>

<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script language="JavaScript1.2">mmLoadMenus();</script>
<div class="nen">
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="106" align="left" valign="top"><table width="1000" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="625" height="106" align="left" valign="top"><img src="images/megahost_02.gif" width="625" height="106" alt="" /></td>
        <td width="375" height="106" align="left" valign="top"><table width="375" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="31" align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td height="48" align="left" valign="top"><table width="375" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="147" align="left" valign="top"><a href="http://crm.megahost.vn/index.php"><img src="images/megahost_05.gif" alt="" width="147" height="48" border="0" /></a></td>
                <td width="228" align="left" valign="top"><img src="images/megahost_06.gif" width="228" height="48" alt="" /></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="36" align="left" valign="top" background="images/megahost_10.gif"><table width="1000" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="36" align="left" valign="top" class="top-menu"><a href="index.php">TRANG CHỦ</a></td>
        <td width="1" height="36" align="left" valign="top"><img src="images/megahost_09.gif" width="1" height="36" alt="" /></td>
        <td height="36" align="left" valign="top" class="top-menu"><a href="?mod=st&id=3" name="link1" id="link1" onmouseover="MM_showMenu(window.mm_menu_0922234615_0,-38,26,null,'link1')" onmouseout="MM_startTimeout();">TÊN MIỀN</a></td>
        <td width="1" height="36" align="left" valign="top"><img src="images/megahost_09.gif" width="1" height="36" alt="" /></td>
        <td height="36" align="left" valign="top" class="top-menu"><a href="?mod=st&id=2" name="link2" id="link2" onmouseover="MM_showMenu(window.mm_menu_0922234959_0,-36,26,null,'link2')" onmouseout="MM_startTimeout();">HOSTING</a></td>
        <td width="1" height="36" align="left" valign="top"><img src="images/megahost_09.gif" width="1" height="36" alt="" /></td>
        <td height="36" align="left" valign="top" class="top-menu"><a href="?mod=st&id=1" name="link3" id="link3" onmouseover="MM_showMenu(window.mm_menu_0923001849_0,-37,26,null,'link3')" onmouseout="MM_startTimeout();">MÁY CHỦ</a></td>
        <td width="1" height="36" align="left" valign="top"><img src="images/megahost_09.gif" width="1" height="36" alt="" /></td>
        <td height="36" align="left" valign="top" class="top-menu"><a href="?mod=regi" name="link4" id="link4" onmouseover="MM_showMenu(window.mm_menu_0923002241_0,-36,26,null,'link4')" onmouseout="MM_startTimeout();">ĐĂNG KÝ</a></td>
        <td width="1" height="36" align="left" valign="top"><img src="images/megahost_09.gif" width="1" height="36" alt="" /></td>
        <td height="36" align="left" valign="top" class="top-menu"><a href="?mod=support" name="link5" id="link5" onmouseover="MM_showMenu(window.mm_menu_0923002511_0,-32,26,null,'link5')" onmouseout="MM_startTimeout();">HỖ TRỢ</a></td>
        <td width="1" height="36" align="left" valign="top"><img src="images/megahost_09.gif" width="1" height="36" alt="" /></td>
        <td height="36" align="left" valign="top" class="top-menu"><a href="?mod=rinfo" name="link6" id="link6" onmouseover="MM_showMenu(window.mm_menu_0923003037_0,-43,26,null,'link6')" onmouseout="MM_startTimeout();">THÔNG TIN</a></td>
        <td width="1" height="36" align="left" valign="top"><img src="images/megahost_09.gif" width="1" height="36" alt="" /></td>
        <td height="36" align="left" valign="top" class="top-menu"><a href="?mod=contact">LIÊN HỆ</a></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="310" align="left" valign="top" style="z-index:-1;">
    <iframe src="libbanner/index.html" width="1000" height="287" frameborder="0" scrolling="no"></iframe>
    </td>
  </tr>
  <!--Start Main content-->
  <?=$cnt->text?>
  <!--End Main content-->
  <tr>
    <td height="7" align="left" valign="top"></td>
  </tr>
    <tr>
    <td height="72" align="left" valign="top" class="icon"><img src="images/icon-control.jpg" width="850" height="29" /></td>
  </tr>
  <tr>
    <td height="109" align="left" valign="top"><table width="1000" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="109" align="left" valign="top" class="copyright"><br />          <strong>Công Ty TNHH Phần Mềm Hoàn Mỹ </strong><br />
          Địa Chỉ: 98/6  Thích Quảng Đức, P.5, Q.Phú Nhuận, HCM
          <br />
          Copyright © 2008 -2010 megahost.vn  <a href="http://imsvietnam.com/quang-ba-website" title="Qu&#7843;ng B&aacute; Website"" >Qu&#7843;ng B&aacute; Website</a></td>
        <td height="109" align="left" valign="top" class="copyright"><strong><br />
          Phòng Kinh Doanh </strong><br />
          Email: <a href="mailto:duc@megahost.vn">duc@megahost.vn</a><br />
          Mobile: 0903339009</td>
        <td height="109" align="left" valign="top" class="copyright"><br />
          <strong>Phòng Kỹ Thuật </strong><br />
          Email:<a href="mailto:contact@megahost.vn"> contact@megahost.vn</a><br />
          Mobile: 0985661672</td>
          <td height="109" align="left" valign="top" class="copyright1">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</body>
</html>


<?php

$db->close(); // Close connect to db
ob_end_flush(); // clean output buffering

?>
