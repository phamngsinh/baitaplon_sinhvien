<?php
/*------------------------------------------------
	| DO SOMETHING FOR NEWS DISPLAY
+-------------------------------------------------*/	
// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

class main
{
	public $setting = array();
	public $error = array(
							'username'=>false,
							'user_same'=>false,
							);
	public $today = "";
	public $yesterday = "";
	
	function __construct()
	{
		global $db, $sess, $time;		
		// Lay tham so config chuong trinh
		$query = $db->simple_select("*", "config");
		$result = $db->query($query);
		
		while ( $row = $db->fetch_array($result) )
		{
			$this->setting[$row['name']] = $row['value'];
		}
		
		// Cai dat gio hien tai theo Time Zone
		$sess->now += $this->setting['TimeZone'];
	}
	
	function check_username($username)
	{
		global $frm, $db;
		
		if ( !$frm->check_username($username, 3) )
		{
			$this->error['username'] = true;
			return false;
		}
		
		$query = $db->simple_select("id", "user_members", "username='".$username."'");
		$result = $db->query($query);
		$row = $db->fetch_array($result);
			
		if ($row['id'])
		{
			$this->error['username'] = true;
			$this->error['user_same'] = true;
			return false;
		}
		
		return true;
	}
}
?>