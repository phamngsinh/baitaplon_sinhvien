<?php 
/*-------------------------------
 * NEWS LISTING
--------------------------------*/

// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}


$cnt = new content;

class content
{
	public $text = '';
	public $title = 'Tin tức - ';
	private $page;
	
	function __construct()
	{
   		global $db, $pagings;
		$text = '';
		$text .= '
		           <!--MIDDLE-->
					<tr>
					<td align="left" valign="top" height="7"></td>
				  </tr>
					  <tr>
						<td align="left" valign="top"><table width="1000" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="277" align="left" valign="top"><table width="277" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td align="left" valign="top">
								<!--TYPICAL NEWS-->
								<table width="277" border="0" cellspacing="0" cellpadding="0">
								  <tr>
									<td height="38" align="left" valign="top" background="images/detail_12.gif" class="danhmuc">TIN TỨC <a href="?mod=news&choose=typical">NỔI BẬT</a></td>
								  </tr>';
						
						
						// Get list of typical news
						$tn_result = $db->query('SELECT * FROM news WHERE striking = 1 ORDER BY id DESC LIMIT 5');		  
						while ($tn = mysql_fetch_assoc($tn_result))
						{
							$text .= '
								  <tr>
									<td height="39" align="left" valign="top" background="images/detail_13.gif" class="danhmuc1"><a href="?mod=newsView&id='.$tn['id'].'">'.$tn['title'].'</a></td>
								  </tr>';
						}
								  
						$text .= '
								  <tr>
									<td align="left" valign="top"><img src="images/detail_10.gif" width="277" height="11" alt="" /></td>
								  </tr>
								</table>
								<!--TYPICAL NEWS-->
								</td>
							  </tr>
							  <tr>
								<td align="left" valign="top">&nbsp;</td>
							  </tr>
							</table></td>
							<td width="723" align="left" valign="top">
							<!--NEWS-->
							<table width="723" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td height="43" align="left" valign="top" background="images/detail_04.gif" class="danhmuc">TIN TỨC MỚI</td>
							  </tr>
							  <tr>
								<td align="left" valign="top" background="images/detail_15.gif" class="main">
								<table width="693" border="0" cellspacing="0" cellpadding="0">
							  ';
					
					////////////////////////////////////
					// Get list of normal news
					////////////////////////////////////
					$this->page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
					$start = ($this->page - 1)*7;
					$sql1 = 'SELECT * FROM news ORDER BY id DESC';
					$sql = 'SELECT * FROM news ORDER BY id DESC LIMIT '.$start.',7';
					$curl = '?mod=news';
					if (isset($_GET['choose']) && $_GET['choose'] == 'typical')		  
					{
						$sql1 = 'SELECT * FROM news WHERE striking = 1 ORDER BY id DESC';
						$sql = 'SELECT * FROM news WHERE striking = 1 ORDER BY id DESC LIMIT '.$start.',7';
						$curl = '?mod=news&choose=typical';
					}
					$result1 = $db->query($sql1);
					$result = $db->query($sql);
					$total_rows = mysql_num_rows($result1);
					$pagingInfos = $pagings->paging_front($total_rows, $this->page, 5, 7, $curl);
					$i=1;
					while ($news = mysql_fetch_assoc($result))
					{
						$text .= '							  
								  <tr>
									<td align="left" valign="top" '.($i++%2 ? 'bgcolor="#ECFFEC"':'').'><a href="?mod=newsView&id='.$news['id'].'"><img src="'.N_IMG.$news['img'].'" width="188" height="137" hspace="10" vspace="5" border="0" align="left" />'.$news['title'].'</a> - '.date('d/m/Y', $news['postdate']).'<br />
									  '.nl2br($news['intro']).'
									  </td>
								  </tr>';
					}
						
								  
						$text .= '
								  <tr>
									<td align="left" valign="top" class="page">
									  '.$pagingInfos.'
									 </td>
								  </tr>';
							
						$text .= '
								</table>                
								
							    </td>
							  </tr>
							  <tr>
								<td align="left" valign="top"><img src="images/detail_16.gif" width="723" height="12" alt="" /></td>
							  </tr>
							</table>
							<!--NEWS-->
							</td>
						  </tr>
						</table></td>
					</tr>
					<!--MIDDLE-->';
		
		$this->text = $text;
	}
	
	
}