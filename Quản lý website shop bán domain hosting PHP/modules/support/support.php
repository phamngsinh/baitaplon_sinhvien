<?php
/*-------------------------------
 * SUPPORT INFORMATION
--------------------------------*/

// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}


$cnt = new content; 

class content
{
	public $text = '';
	public $title = '';
	private $id = '';
	
	function __construct()
	{
   		global $db, $pagings;
		// Define id of item will be show
		if (isset($_GET['id']) && $_GET['id'] > 0)
		{
			$this->id = $_GET['id'];
		}
		else
		{
			$default = $db->getData('SELECT * FROM support ORDER BY rank ASC, id DESC LIMIT 1');
			$this->id = $default['id'];
		}
		
		$text = '
		           <!--MIDDLE-->
					<tr>
					<td align="left" valign="top" height="7"></td>
				  </tr>
					  <tr>
						<td align="left" valign="top"><table width="1000" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="277" align="left" valign="top">
							<!--LEFT-->
							<table width="277" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td align="left" valign="top">
								<!--SUPPORT ITEMS-->
								<table width="277" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td height="43" align="left" valign="top" background="images/detail_03.gif" class="danhmuc">HỖ TRỢ</td>
								  </tr>';
						
						// Get list of infos items
						$infos_result = $db->query('SELECT * FROM support ORDER BY rank ASC, id DESC');
						while ($infos = mysql_fetch_assoc($infos_result))		
						{
							$text .= '								  
									  <tr>
										<td height="32" align="left" valign="top" background="images/detail_09.gif" class="menu"><a href="?mod=support&id='.$infos['id'].'">'.$infos['title'].'</a></td>
									  </tr>';
						}
													
						$text .= '
								  <tr>
									<td align="left" valign="top"><img src="images/detail_10.gif" width="277" height="11" alt="" /></td>
								  </tr>
								</table>
								<!--SUPPORT ITEMS-->
								</td>
							  </tr>
							  <tr>
								<td align="left" valign="top">&nbsp;</td>
							  </tr>
							  <tr>
								<td align="left" valign="top">
								<!--TYPICAL NEWS-->
								<table width="277" border="0" cellspacing="0" cellpadding="0">
								  <tr>
									<td height="38" align="left" valign="top" background="images/detail_12.gif" class="danhmuc">TIN TỨC <a href="?mod=news&choose=typical">NỔI BẬT</a></td>
								  </tr>';
						
						// List of typical news
						$tn_result = $db->query('SELECT * FROM news WHERE striking = 1 ORDER BY id DESC LIMIT 5');		  
						while ($tn = mysql_fetch_assoc($tn_result))
						{
							$text .= '
								  <tr>
									<td height="39" align="left" valign="top" background="images/detail_13.gif" class="danhmuc1"><a href="?mod=newsView&id='.$tn['id'].'">'.$tn['title'].'</a></td>
								  </tr>';
						}
								  
						$text .= '
								  <tr>
									<td align="left" valign="top"><img src="images/detail_10.gif" width="277" height="11" alt="" /></td>
								  </tr>
								</table>
								<!--TYPICAL NEWS-->
								</td>
							  </tr>
							  <tr>
								<td align="left" valign="top">&nbsp;</td>
							  </tr>
							</table>
							<!--LEFT-->
							</td>
							<td width="723" align="left" valign="top">';
				
				// Get infos of current item	
				$citem = $db->getData('SELECT * FROM support WHERE id="'.$this->id.'"');
				$this->title = $citem['title'].' - ';		
				$text .= '
							<!--CONTENT-->
							<table width="723" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td height="43" align="left" valign="top" background="images/detail_04.gif" class="danhmuc">'.mb_strtoupper($citem['title'],'UTF-8').'</td>
							  </tr>
							  <tr>
								<td align="left" valign="top" background="images/detail_15.gif" class="main">
								<div class="dholder">'.$citem['content'].'</div>
								</td>
							  </tr>
							  <tr>
								<td align="left" valign="top"><img src="images/detail_16.gif" width="723" height="12" alt="" /></td>
							  </tr>
							</table>
							<!--CONTENT-->
							</td>
						  </tr>
						</table></td>
					</tr>
					<!--MIDDLE-->';
		$this->text = $text;
	}
	
	
}