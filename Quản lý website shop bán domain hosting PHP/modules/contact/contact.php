<?php
/*-------------------------------
 * CONTACT PAGE
--------------------------------*/

// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}


$cnt = new content; 

class content{

	public $text = '';
	public $title = 'Khách hàng liên hệ - ';
	private $warning = '';
	
	function __construct()
	{
   		global $db, $sess, $crypt, $panel;
		$text = '';
		// If user submit the information
		if (isset($_POST['action']) && $_POST['action'] == 'sendcontact')
		{
			$arr = array(
			 				'fullname' => $_POST['txtname'],
							'address'  => $_POST['txtaddress'],
							'company'  => $_POST['txtcompany'],
							'phone'    => $_POST['txtphone'],
							'fax'      => $_POST['txtfax'],
							'email'    => $_POST['txtemail'],
							'title'    => $_POST['txttitle'],
							'content'  => $_POST['txtcontent'],
							'senddate' => time(),
							'isread'   => 0
   						);
			
				//print_r($arr);
			$db->do_insert('contact',$arr);
			header("location:index.php?mod=contact&cs=sent");
			exit;
		}		
		
		$crm = $panel->getCRMInfos();
		$text .= '
					<!--MIDDLE-->
					<tr>
					<td align="left" valign="top" height="7"></td>
				  </tr>
					  <tr>
						<td align="left" valign="top"><table width="1000" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="277" align="left" valign="top"><table width="278" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td width="278" align="left" valign="top">
								<!--SUPPORT-->
								<table width="277" border="0" cellspacing="0" cellpadding="0">
								 <td height="38" align="left" valign="top" background="images/detail_03.gif" class="danhmuc">THÔNG TIN</td>
								  </tr>';
						
						// Get list of infos items
						$infos_result = $db->query('SELECT * FROM infos ORDER BY rank ASC, id DESC');
						while ($infos = mysql_fetch_assoc($infos_result))		
						{
							$text .= '								  
									  <tr>
										<td height="32" align="left" valign="top" background="images/detail_09.gif" class="menu"><a href="?mod=rinfo&id='.$infos['id'].'">'.$infos['title'].'</a></td>
									  </tr>';
						}
													
						$text .= '
								  <tr>
									<td height="27" align="left" valign="top"><img src="images/detail_10.gif" width="277" height="11" alt="" /></td>
									
								  
								 
								  <tr>
								  
									<td height="38" align="left" valign="top" background="images/detail_12.gif" class="danhmuc">THÔNG TIN HỖ TRỢ</td>
								  </tr>
								  <tr>
									<td height="39" align="left" valign="top" background="images/detail_130.gif" class="main"><table width="247" border="0" cellspacing="0" cellpadding="0">
									  <tr>
										<td width="45" align="left" valign="top"><a href="'.$crm[0]['blink'].'"><img src="images/megahost_25.gif" alt="" width="43" height="45" border="0" /></a></td>
										<td width="81" align="left" valign="top"><a href="'.$crm[0]['blink'].'">Liên hệ<br />
										  Kinh doanh</a></td>
										<td width="45" align="left" valign="top"><a href="'.$crm[1]['blink'].'"><img src="images/megahost_27.gif" alt="" width="39" height="45" border="0" /></a></td>
										<td width="74" align="left" valign="top"><a href="'.$crm[1]['blink'].'">Hỗ trợ <br />
										  Kỹ Thuật</a></td>
									  </tr>
										<tr>
										<td width="45" height="15" align="left" valign="top">&nbsp;</td>
										<td width="81" height="15" align="left" valign="top">&nbsp;</td>
										<td width="45" height="15" align="left" valign="top">&nbsp;</td>
										<td width="74" height="15" align="left" valign="top">&nbsp;</td>
										</tr>
										<tr>
										<td width="45" align="left" valign="top"><a href="'.$crm[2]['blink'].'"><img src="images/megahost_29.gif" alt="" width="41" height="45" border="0" /></a></td>
										<td width="81" align="left" valign="top"><a href="'.$crm[2]['blink'].'">Than phiền<br />
										  Dịch vụ</a></td>
										<td width="45" align="left" valign="top"><a href="'.$crm[3]['blink'].'"><img src="images/megahost_31.gif" alt="" width="47" height="45" border="0" /></a></td>
										<td width="74" align="left" valign="top"><a href="'.$crm[3]['blink'].'">Góp ý<br />
										Chất lượng</a></td>
										</tr>
									</table></td>
								  </tr>
								  <tr>
									<td align="left" valign="top"><img src="images/detail_10.gif" width="277" height="11" alt="" /></td>
								  </tr>
								</table>
								<!--SUPPORT-->
								</td>
							  </tr>
							  <tr>
								<td align="left" valign="top">&nbsp;</td>
							  </tr>
							</table></td>
							<td width="723" align="left" valign="top"><table width="723" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td align="left" valign="top">
								<!--INFOS-->
								<!--INFOS--></td>
							  </tr>
							  <tr>
								
							  </tr>
							  <tr>
								<td align="left" valign="top">
								<!--CONTACT-->
								<form action="" method="post" onsubmit="return checkValidForm();" name="frmContact">
								<table width="723" border="0" cellspacing="0" cellpadding="0">
								  <tr>
									<td height="43" align="left" valign="top" background="images/detail_04.gif" class="danhmuc">LIÊN HỆ QUA EMAIL</td>
								  </tr>
								  <tr>
									<td align="left" valign="top" background="images/detail_15.gif" class="main">
									<table width="693" border="0" cellpadding="0" cellspacing="0" class="login">';
					// Sent contact information
					if ($_GET['cs'] == 'sent')				
					{
						$text .= '
								    <tr>
										<td colspan="2" height="25" align="left" valign="middle" class="noidung" style="color:#0000FF;text-align:center;">Thông tin liên hệ đã được gởi. Chúng tôi sẽ xem xét trong thời gian sớm nhất!
										</td>
									  </tr>';
					}
									  
						$text .= '
									  <tr>
										<td width="147" height="25" align="left" valign="middle" class="noidung">Tên quý vị</td>
										<td width="556" height="25" align="left" valign="middle" class="noidung"><input name="txtname" type="text" class="input-email" id="textfield" />
										  *</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Công ty (nếu có)</td>
										<td height="25" align="left" valign="middle" class="noidung"><input name="txtcompany" type="text" class="input-email" id="textfield2" /></td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Địa chỉ</td>
										<td height="25" align="left" valign="middle" class="noidung"><input name="txtaddress" type="text" class="input-email" id="textfield3" /></td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Điện thoại</td>
										<td height="25" align="left" valign="middle" class="noidung"><input name="txtphone" type="text" class="input-email" id="textfield4" />
										  *</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Fax</td>
										<td height="25" align="left" valign="middle" class="noidung"><input name="txtfax" type="text" class="input-email" id="textfield5" /></td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Email</td>
										<td height="25" align="left" valign="middle" class="noidung"><input name="txtemail" type="text" class="input-email" id="textfield6" />
										  *</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Tiêu đề</td>
										<td height="25" align="left" valign="middle" class="noidung"><input name="txttitle" type="text" class="input-email" id="textfield7" />
										  *</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Thông tin</td>
										<td height="25" align="left" valign="middle" class="noidung"><textarea name="txtcontent" id="textarea" cols="65" rows="8"></textarea>*</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Mã bảo vệ</td>
										<td height="25" align="left" valign="middle" class="noidung"><input name="txtcode" type="text" class="input-code" id="textfield8" /> 
										  <span class="style1">';
							
							// Generate security code
							$secu_code = $crypt->security_code(6); // Create security code
							//$db->do_update("session", array( 'security_code'  => $secu_code), "id = '".$sess->sess_id."'"); // Update the security code to db in sesstion table
							$text .= $crypt->print_screen($secu_code); // Convert security code to string format						
										  
							$text .= '			  
										  </span></td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">&nbsp;</td>
										<td height="25" align="left" valign="middle" class="noidung"><input type="submit" name="button" id="button" value="Gởi mail" />
										  <input type="reset" name="button2" id="button2" value="Hủy bỏ" />
										  <input type="hidden" name="action" value="sendcontact"/>
										 </td>
									  </tr>
									</table></td>
								  </tr>
								  <tr>
									<td align="left" valign="top"><img src="images/detail_16.gif" width="723" height="12" alt="" /></td>
								  </tr>
								</table>
								</form>
								<!--CONTACT-->';
								
				$text .= '
								</td>
							  </tr>
							</table></td>
						  </tr>
						</table></td>
					</tr>
					<!--MIDDLE-->';
					
				// JS for check form
				$text .= '
				          <script language="javascript">
							function checkValidForm()
							{
								if (document.frmContact.txtname.value == "")
								{
									alert("Vui lòng nhập họ tên");
									document.frmContact.txtname.focus();
									return false;
								}
								if (document.frmContact.txtphone.value == "")
								{
									alert("Vui lòng nhập số điện thoại");
									document.frmContact.txtphone.focus();
									return false;
								}
								if (!validateEmail(document.frmContact.txtemail.value))
								{
									alert("Địa chỉ email không hợp lệ");
									document.frmContact.txtemail.focus();
									return false;
								}
								if (document.frmContact.txttitle.value == "")
								{
									alert("Vui lòng nhập tiêu đề");
									document.frmContact.txttitle.focus();
									return false;
								}
								if (document.frmContact.txtcontent.value == "")
								{
									alert("Vui lòng nhập nội dung");
									document.frmContact.txtcontent.focus();
									return false;
								}	
								if (document.frmContact.txtcode.value != "'.$secu_code.'")
								{
									alert("Mã bảo vệ không trùng khớp");
									document.frmContact.txtcode.focus();
									return false;
								}
								
								return true;
							}
							
							 // Check email valid email address
							function validateEmail(elementValue)
							{  
								 var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;  
								 return emailPattern.test(elementValue);  
							}
							</script>
						 ';
		$this->text = $text;
	}
	
	
}