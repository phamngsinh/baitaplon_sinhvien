<?php
/*-------------------------------
 * PAGE FOR ORDER SERVICE
--------------------------------*/

// Check for Security 
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}


$cnt = new content;

class content{

	public $text = '';
	public $title = 'Đặt hàng dịch vụ - ';
	private $frmValue = array(
									'subject' => '',
									'fullname' => '',
									'company' => '',
									'address' => '',
									'phone' => '',
									'fax' => '',
									'email' => '',
									'domain' => '',
									'sv' => '',
									'sp' => '',
									'duration' => '',
									'payment' => '',
									'infos' => ''
							  );
	
	
	
	function __construct()
	{
   		global $db, $panel, $dsp, $crypt;
		$text = '';
		$this->get_input();
		$crm = $panel->getCRMInfos();
		
		// User submitted order
		if (isset($_POST['action']) && $_POST['action'] == 'sendorder')
		{
			$arr = array(
			                 'subject' => $this->frmValue['subject'],
							 'fullname' => $this->frmValue['fullname'],
							 'company' => $this->frmValue['company'],
							 'address' => $this->frmValue['address'],
							 'phone' => $this->frmValue['phone'],
							 'fax' => $this->frmValue['fax'],
							 'email' => $this->frmValue['email'],
							 'domain' => $this->frmValue['domain'],
							 'service' => $this->frmValue['sv'],
							 'service_plan' => $this->frmValue['sp'],
							 'duration' => $this->frmValue['duration'],
							 'payment' => $this->frmValue['payment'],
							 'infos' => $this->frmValue['infos'],
							 'order_date' => time()
							 
			             );
			$db->do_insert('orders', $arr);
			header("location:index.php?mod=order&status=sentorder");
			exit();
		}
		
		
		$text .= '	          
				   <!--MIDDLE-->
					<tr>
					<td align="left" valign="top" height="7"></td>
				  </tr>
					  <tr>
						<td align="left" valign="top"><table width="1000" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="277" align="left" valign="top"><table width="277" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td align="left" valign="top">
								<!--SUPPORT-->
								<table width="277" border="0" cellspacing="0" cellpadding="0">
								<td height="43" align="left" valign="top" background="images/detail_03.gif" class="danhmuc">THÔNG TIN</td>
								  </tr>';
						
						// Get list of infos items
						$infos_result = $db->query('SELECT * FROM infos ORDER BY rank ASC, id DESC');
						while ($infos = mysql_fetch_assoc($infos_result))		
						{
							$text .= '								  
									  <tr>
										<td height="32" align="left" valign="top" background="images/detail_09.gif" class="menu"><a href="?mod=rinfo&id='.$infos['id'].'">'.$infos['title'].'</a></td>
									  </tr>';
						}
													
						$text .= '
								  <tr>
									<td height="27" align="left" valign="top"><img src="images/detail_10.gif" width="277" height="11" alt="" /></td>
									
								  <tr>
								  <tr>
									<td height="38" align="left" valign="top" background="images/detail_12.gif" class="danhmuc">THÔNG TIN HỖ TRỢ</td>
								  </tr>
								  <tr>
									<td height="39" align="left" valign="top" background="images/detail_130.gif" class="main"><table width="247" border="0" cellspacing="0" cellpadding="0">
									  <tr>
										<td width="45" align="left" valign="top"><a href="'.$crm[0]['blink'].'"><img src="images/megahost_25.gif" alt="" width="43" height="45" border="0" /></a></td>
										<td width="81" align="left" valign="top"><a href="'.$crm[0]['blink'].'">Liên hệ<br />
										  Kinh doanh</a></td>
										<td width="45" align="left" valign="top"><a href="'.$crm[1]['blink'].'"><img src="images/megahost_27.gif" alt="" width="39" height="45" border="0" /></a></td>
										<td width="74" align="left" valign="top"><a href="'.$crm[1]['blink'].'">Hỗ trợ <br />
										  Kỹ Thuật</a></td>
										</tr>
										<tr>
										<td width="45" height="15" align="left" valign="top">&nbsp;</td>
										<td width="81" height="15" align="left" valign="top">&nbsp;</td>
										<td width="45" height="15" align="left" valign="top">&nbsp;</td>
										<td width="74" height="15" align="left" valign="top">&nbsp;</td>
										</tr>
										<tr>
										<td width="45" align="left" valign="top"><a href="'.$crm[2]['blink'].'"><img src="images/megahost_29.gif" alt="" width="41" height="45" border="0" /></a></td>
										<td width="81" align="left" valign="top"><a href="'.$crm[2]['blink'].'">Than phiền<br />
										  Dịch vụ</a></td>
										<td width="45" align="left" valign="top"><a href="'.$crm[3]['blink'].'"><img src="images/megahost_31.gif" alt="" width="47" height="45" border="0" /></a></td>
										<td width="74" align="left" valign="top"><a href="'.$crm[3]['blink'].'">Góp ý<br />
										Chất lượng</a></td>
										</tr>
									</table></td>
								  </tr>
								  <tr>
									<td align="left" valign="top"><img src="images/detail_10.gif" width="277" height="11" alt="" /></td>
								  </tr>
								</table>
								<!--SUPPORT-->
								</td>
							  </tr>
							  <tr>
								<td align="left" valign="top">&nbsp;</td>
							  </tr>
							</table></td>
							<td width="723" align="left" valign="top"><table width="723" border="0" cellspacing="0" cellpadding="0">
							  
							  
							  <tr>
								<td align="left" valign="top">
								<table width="723" border="0" cellspacing="0" cellpadding="0">
								  <tr>
									<td height="43" align="left" valign="top" background="images/detail_04.gif" class="danhmuc">FORM ĐĂNG KÝ DỊCH VỤ</td>
								  </tr>
								  <tr>
									<td align="left" valign="top" background="images/detail_15.gif" class="main">';
									
					$text .= '
									<!--ORDER-->
									<form action="" method="post" name="frmOrder" onsubmit="return checkOrderForm();">
									<table width="693" border="0" cellpadding="0" cellspacing="0" class="login">';
					
					// Show order result information
					if (isset($_GET['status']) && $_GET['status'] == 'sentorder')
					{
						$text .= '
					<tr><td colspan="2" style="text-align:center; font-size:1.1em; color:#0000FF;">Gởi thông tin đặt hành thành công. Chúng tôi sẽ phản hồi trong thời gian sớm nhất có thể.<br/>Cảm ơn đã sử dụng dịch vụ của chúng tôi<br/></td></tr>';
					}
					
						$text .= '
									  <tr>
										<td width="153" height="25" align="left" valign="middle" class="noidung">Chủ thể đăng ký</td>
									  <td width="540" height="25" align="left" valign="middle" class="noidung">
						        <input name="subject" type="radio" id="radio" value="1" '.($this->frmValue['subject'] == 1 ? 'checked="checked"' : '').' /> 
										  Công ty 
											<input type="radio" name="subject" id="radio2" value="2" '.($this->frmValue['subject'] == 2 ? 'checked="checked"' : '').'/> 
											Cá nhân</td>
									  </tr>
									  <tr>
										<td width="153" height="25" align="left" valign="middle" class="noidung">Tên quý vị*</td>
									  <td width="540" height="25" align="left" valign="middle" class="noidung">
										<input name="txtfullname" type="text" class="input-email" id="textfield" value="'.$this->frmValue['fullname'].'"/>
										</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Công ty </td>
										<td height="25" align="left" valign="middle" class="noidung">
											<input name="txtcompany" type="text" class="input-email" id="textfield2" value="'.$this->frmValue['company'].'"/>
										</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Địa chỉ</td>
										<td height="25" align="left" valign="middle" class="noidung">
											<input name="txtaddress" type="text" class="input-email" id="textfield3" value="'.$this->frmValue['address'].'"/>
										</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Điện thoại*</td>
									  <td height="25" align="left" valign="middle" class="noidung">
											<input name="txtphone" type="text" class="input-email" id="textfield4" value="'.$this->frmValue['phone'].'"/>
										</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Fax</td>
										<td height="25" align="left" valign="middle" class="noidung">
											<input name="txtfax" type="text" class="input-email" id="textfield5" value="'.$this->frmValue['fax'].'"/>
										</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Email*</td>
									  <td height="25" align="left" valign="middle" class="noidung">
											<input name="txtemail" type="text" class="input-email" id="textfield6" value="'.$this->frmValue['email'].'"/>
										</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Tên miền của bạn</td>
										<td height="25" align="left" valign="middle" class="noidung">
											<input name="txtdomain" type="text" class="input-email" id="textfield7" value="'.$this->frmValue['domain'].'"/>
										</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Loại dịch vụ*</td>
										<td height="25" align="left" valign="middle" class="noidung">';
							
							
							// User access order page from buy link of service plan page
							if ($_GET['spid'] > 0 && !isset($_POST['sp']))
							{
								$svt = $db->getData('SELECT s.id,s.title FROM service AS s, service_plan AS sp WHERE sp.allow_order = 1 AND sp.service_id = s.id AND sp.id="'.$_GET['spid'].'"');
								$this->frmValue['sv'] = $svt['id'];
							}
							
							$text .= $dsp->showServices('sv', $this->frmValue['sv'], 'svChange();');
																								
							$text .= '
										</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Gói dịch vụ</td>
										<td height="25" align="left" valign="middle" class="noidung">';
							if ($this->frmValue['sv'] > 0)
							{
							    $text .= $dsp->showServicesPlan('sp', $this->frmValue['sv'], $this->frmValue['sp'], '', FALSE);
							}
							else
							{
								$text .= '<select name="sp" class="input-list1" id="select">
											  <option value="">Vui lòng chọn</option>
										  </select>';
							}
							$text .= '
										</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Thời hạn đăng ký*</td>
										<td height="25" align="left" valign="middle" class="noidung">';
										
								$text .= '
										<select name="duration" class="input-list1" id="select">
										  <option value="">Vui lòng chọn</option>
										  <option value="3 Tháng" '.($this->frmValue['duration'] == "3 Tháng" ? 'selected' : '').'>3 Tháng</option>
										  <option value="6 Tháng" '.($this->frmValue['duration'] == "6 Tháng" ? 'selected' : '').'>6 Tháng </option>
										  <option value="12 Tháng" '.($this->frmValue['duration'] == "12 Tháng" ? 'selected' : '').'>12 Tháng</option>
										  <option value="24 Tháng" '.($this->frmValue['duration'] == "24 Tháng" ? 'selected' : '').'>24 Tháng</option>
										  <option value="36 Tháng" '.($this->frmValue['duration'] == "36 Tháng" ? 'selected' : '').'>36 Tháng</option>
										</select>';
										
								$text .= '
										</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Hình thức thanh toán*</td>
										<td height="25" align="left" valign="middle" class="noidung">';
										
																		
								$text .= '
										<select name="payment" class="input-list1" id="select">
										  <option value="">Vui lòng chọn</option>
										  <option value="1" '.($this->frmValue['payment'] == 1 ? 'selected' : '').'>Thanh toán trực tiếp</option>
										  <option value="2" '.($this->frmValue['payment'] == 2 ? 'selected' : '').'>Chuyển khoản ngân hàng</option>
										  <option value="3" '.($this->frmValue['payment'] == 3 ? 'selected' : '').'>Thanh toán bưu điện</option>
										  <option value="4" '.($this->frmValue['payment'] == 4 ? 'selected' : '').'>Khác</option>
										</select>';
										
								$text .= '
										</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Yêu cầu thêm</td>
										<td height="25" align="left" valign="middle" class="noidung">
										<textarea name="txtinfos" id="textarea" cols="50" rows="5">'.$this->frmValue['infos'].'</textarea>
										</td>
									  </tr>
									  <tr>
										<td height="25" align="left" valign="middle" class="noidung">Mã bảo vệ</td>
										<td height="25" align="left" valign="middle" class="noidung">
										<input name="txtcode" type="text" class="input-code" id="textfield8" /> ';
								
								// Generate security code
								$secu_code = $crypt->security_code(6); 		
								$text .= '<span class="style1">'.$crypt->print_screen($secu_code).'</span></td>';
								$text .= '
									  </tr>
									   <tr>
										<td height="25" align="left" valign="middle" class="noidung">&nbsp;</td>
										<td height="25" align="left" valign="middle" class="noidung">
										  <input type="submit" name="button" id="button" value="Đăng ký dịch vụ" />
										  <input type="reset" name="button2" id="button2" value="Hủy bỏ" />
										  <input type="hidden" name="action" value="sendorder" />
										</td>
									  </tr>
									</table>
									</form>
									<!--ORDER-->';
									
						$text .= '
									</td>
								  </tr>
								  
								  <tr>
									<td align="left" valign="top"><img src="images/detail_16.gif" width="723" height="12" alt="" /></td>
								  </tr>
								  <tr>
								<td height="7" align="left" valign="top"></td>
							  </tr>
								  <tr>
								<td align="left" valign="top">';
								
					$text .= '
								<!--BANK-->
								<!--BANK-->
								';
								
						$text .= '								</td>
							  </tr>
								</table></td>
							  </tr>
							</table></td>
						  </tr>
						</table></td>
					</tr>
					<!--MIDDLE-->';
					
					// JS
					$text .= '
					           <script language="JavaScript">
								  function svChange()
								  {
									  document.frmOrder.action.value = "";
									  document.frmOrder.submit();
									  document.frmOrder.sv.focus();
								  }
								  function checkOrderForm()
								  {
								  		if(document.frmOrder.txtfullname.value == "")
										{
											alert("Vui lòng nhập họ tên");
											document.frmOrder.txtfullname.focus();
											return false;
										}
										
										if(document.frmOrder.txtphone.value == "")
										{
											alert("Vui lòng nhập điện thoại");
											document.frmOrder.txtphone.focus();
											return false;
										}
										
										if(!validateEmail(document.frmOrder.txtemail.value))
										{
											alert("Vui lòng địa chỉ email hợp lệ");
											document.frmOrder.txtemail.focus();
											return false;
										}
										
										var myTest = document.frmOrder.sv.options[document.frmOrder.sv.selectedIndex].value;
										if (myTest < 1)
										{
											alert("Vui lòng chọn loại dịch vụ");
											document.frmOrder.sv.focus();
											return false;
										}
										
										
										var myTest3 = document.frmOrder.duration.options[document.frmOrder.duration.selectedIndex].value;
										if (myTest3 == "")
										{
											alert("Vui lòng chọn thời gian đăng kí");
											document.frmOrder.duration.focus();
											return false;
										}
										
										var myTest4 = document.frmOrder.payment.options[document.frmOrder.payment.selectedIndex].value;
										if (myTest4 < 1)
										{
											alert("Vui lòng chọn hình thức thanh toán");
											document.frmOrder.payment.focus();
											return false;
										}
										
										if (document.frmOrder.txtcode.value != "'.$secu_code.'")
										{
											alert("Mã bảo vệ không trùng khớp");
											document.frmOrder.txtcode.value="";
											document.frmOrder.txtcode.focus();
											return false;
										}
										
										return true;
										
										
								  
								  }
								  
								  function validateEmail(elementValue)
								  {  
									  var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;  
									  return emailPattern.test(elementValue);  
								  }
							   </script>
					         ';
					
		$this->text = $text;
	}
	
	
	// Get input data
	function get_input()
	{
		global $str;
		$this->frmValue['subject'] = isset($_POST['subject']) ? $str->input($_POST['subject']) : "1";
		$this->frmValue['fullname'] = isset($_POST['txtfullname']) ? $str->input($_POST['txtfullname']) : "";
		$this->frmValue['company'] = isset($_POST['txtcompany']) ? $str->input($_POST['txtcompany']) : "";
		$this->frmValue['address'] = isset($_POST['txtaddress']) ? $str->input($_POST['txtaddress']) : "";
		$this->frmValue['phone'] = isset($_POST['txtphone']) ? $str->input($_POST['txtphone']) : "";
		$this->frmValue['fax'] = isset($_POST['txtfax']) ? $str->input($_POST['txtfax']) : "";
		$this->frmValue['email'] = isset($_POST['txtemail']) ? $str->input($_POST['txtemail']) : "";
		$this->frmValue['domain'] = isset($_POST['txtdomain']) ? $str->input($_POST['txtdomain']) : "";
		$this->frmValue['sv'] = isset($_POST['sv']) ? $str->input($_POST['sv']) : "";
		$this->frmValue['sp'] = isset($_POST['sp']) ? $str->input($_POST['sp']) : ($_GET['spid'] > 0 ? $_GET['spid'] : "");
		$this->frmValue['duration'] = isset($_POST['duration']) ? $str->input($_POST['duration']) : "";
		$this->frmValue['payment'] = isset($_POST['payment']) ? $str->input($_POST['payment']) : "";
		$this->frmValue['infos'] = isset($_POST['txtinfos']) ? $str->input_html($_POST['txtinfos']) : "";
		
	
	}

	
	
}