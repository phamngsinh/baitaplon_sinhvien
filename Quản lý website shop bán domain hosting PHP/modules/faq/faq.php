<?php
/*-------------------------------
 * FAQ LISTING
--------------------------------*/

// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}


$cnt = new content;

class content
{
	public $text = '';
	public $title = 'Hỏi đáp - ';
	private $page = ''; 
	
	function __construct()
	{
   		global $db, $pagings, $panel;
		$text = '';
		$crm = $panel->getCRMInfos();
		$text .= '
				    <!--MIDDLE-->
					<tr>
					<td align="left" valign="top" height="7"></td>
				  </tr>
					  <tr>
						<td align="left" valign="top"><table width="1000" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="277" align="left" valign="top"><table width="277" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td align="left" valign="top">
								<!--SUPPORT-->
								<table width="277" border="0" cellspacing="0" cellpadding="0">
								  <tr>
									<td height="38" align="left" valign="top" background="images/detail_12.gif" class="danhmuc">THÔNG TIN HỖ TRỢ</td>
								  </tr>
								  <tr>
									<td height="39" align="left" valign="top" background="images/detail_130.gif" class="main"><table width="247" border="0" cellspacing="0" cellpadding="0">
									  <tr>
										<td width="45" align="left" valign="top"><a href="'.$crm[0]['blink'].'"><img src="images/megahost_25.gif" alt="" width="43" height="45" border="0" /></a></td>
										<td width="81" align="left" valign="top"><a href="'.$crm[0]['blink'].'">Liên hệ<br />
										  Kinh doanh</a></td>
										<td width="45" align="left" valign="top"><a href="'.$crm[1]['blink'].'"><img src="images/megahost_27.gif" alt="" width="39" height="45" border="0" /></a></td>
										<td width="74" align="left" valign="top"><a href="'.$crm[1]['blink'].'">Hỗ trợ <br />
										  Kỹ Thuật</a></td>
										</tr>
										<tr>
										<td width="45" height="15" align="left" valign="top">&nbsp;</td>
										<td width="81" height="15" align="left" valign="top">&nbsp;</td>
										<td width="45" height="15" align="left" valign="top">&nbsp;</td>
										<td width="74" height="15" align="left" valign="top">&nbsp;</td>
										</tr>
										<tr>
										<td width="45" align="left" valign="top"><a href="'.$crm[2]['blink'].'"><img src="images/megahost_29.gif" alt="" width="41" height="45" border="0" /></a></td>
										<td width="81" align="left" valign="top"><a href="'.$crm[2]['blink'].'">Than phiền<br />
										  Dịch vụ</a></td>
										<td width="45" align="left" valign="top"><a href="'.$crm[3]['blink'].'"><img src="images/megahost_31.gif" alt="" width="47" height="45" border="0" /></a></td>
										<td width="74" align="left" valign="top"><a href="'.$crm[3]['blink'].'">Góp ý<br />
										Chất lượng</a></td>
										</tr>
									</table></td>
								  </tr>
								  <tr>
									<td align="left" valign="top"><img src="images/detail_10.gif" width="277" height="11" alt="" /></td>
								  </tr>
								</table>
								<!--SUPPORT-->
								</td>
							  </tr>
							  <tr>
								<td align="left" valign="top">&nbsp;</td>
							  </tr>
							</table></td>
							<td width="723" align="left" valign="top">
							<!--FAQ-->
							<table width="723" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td height="43" align="left" valign="top" background="images/detail_04.gif" class="danhmuc">CÂU HỎI THƯỜNG GẶP</td>
							  </tr>
							  <tr>
								<td align="left" valign="top" background="images/detail_15.gif" class="main">
								<table width="693" border="0" cellspacing="0" cellpadding="0">';
						
						
						// List of faq
						$this->page = (isset($_GET['page']) && $_GET['page'] > 0)? $_GET['page'] : 1;
						$start = ($this->page - 1)*15;
						$query1 = 'SELECT * FROM faq ORDER BY id DESC';
						$query = 'SELECT * FROM faq ORDER BY id DESC LIMIT '.$start.',15';
						$result1 = $db->query($query1);
						$result = $db->query($query);
						$total_rows = mysql_num_rows($result1);
						$pagingInfos = $pagings->paging_front($total_rows,$this->page,5,15,'?mod=faq');
						while($f = mysql_fetch_assoc($result))		
						{
							$text .= '
								  <tr>
									<td height="35" align="left" valign="middle" bgcolor="#FFFFDF"><a href="?mod=faqView&id='.$f['id'].'"><img src="images/help.gif" width="20" height="20" hspace="10" border="0" align="left" />'.$f['question'].'</a></td>
								  </tr>';
						}
					// Paging information
					$text .= '
								  <tr>
									<td align="left" valign="top" class="page">
									  '.$pagingInfos.'
									  </td>
								  </tr>';
								  
					$text .= '				
								</table>                
								
							    </td>
							  </tr>
							  <tr>
								<td align="left" valign="top"><img src="images/detail_16.gif" width="723" height="12" alt="" /></td>
							  </tr>
							</table>
							<!--FAQ-->
							</td>
						  </tr>
						</table></td>
					</tr>
					<!--MIDDLE-->';
		
		$this->text = $text;
	}
	
	
}