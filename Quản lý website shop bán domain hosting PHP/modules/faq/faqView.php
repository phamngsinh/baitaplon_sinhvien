<?php
/*-------------------------------
 * VIEW FAQ DETAILS
--------------------------------*/

// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}


$cnt = new content;

class content
{
	public $text = '';
	public $title = '';
	private $fID = '';
	
	function __construct()
	{
   		global $db, $panel;
		$this->fID = isset($_GET['id']) ? $_GET['id'] : '';
		if ($this->fID <= 0)
		{
			header("location:index.php?mod=faq");
			exit;
		}
		
		$crm = $panel->getCRMInfos();
		$text = '
				  <!--MIDDLE-->
					<tr>
					<td align="left" valign="top" height="7"></td>
				  </tr>
					  <tr>
						<td align="left" valign="top"><table width="1000" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="277" align="left" valign="top"><table width="277" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td align="left" valign="top">
								<!--SUPPORT-->
								<table width="277" border="0" cellspacing="0" cellpadding="0">
								<td height="38" align="left" valign="top" background="images/detail_03.gif" class="danhmuc">THÔNG TIN</td>
								  </tr>';
						
						// Get list of infos items
						$infos_result = $db->query('SELECT * FROM infos ORDER BY rank ASC, id DESC');
						while ($infos = mysql_fetch_assoc($infos_result))		
						{
							$text .= '								  
									  <tr>
										<td height="32" align="left" valign="top" background="images/detail_09.gif" class="menu"><a href="?mod=rinfo&id='.$infos['id'].'">'.$infos['title'].'</a></td>
									  </tr>';
						}
													
						$text .= '
								  <tr>
									<td height="27" align="left" valign="top"><img src="images/detail_10.gif" width="277" height="11" alt="" /></td>
									
								  <tr>
								  <tr>
									<td height="38" align="left" valign="top" background="images/detail_12.gif" class="danhmuc">THÔNG TIN HỖ TRỢ</td>
								  </tr>
								  <tr>
									<td height="39" align="left" valign="top" background="images/detail_130.gif" class="main"><table width="247" border="0" cellspacing="0" cellpadding="0">
								    <tr>
										  <td width="45" align="left" valign="top"><a href="'.$crm[0]['blink'].'"><img src="images/megahost_25.gif" alt="" width="43" height="45" border="0" /></a></td>
										  <td width="81" align="left" valign="top"><a href="'.$crm[0]['blink'].'">Liên hệ<br />
											Kinh doanh</a></td>
										  <td width="45" align="left" valign="top"><a href="'.$crm[1]['blink'].'"><img src="images/megahost_27.gif" alt="" width="39" height="45" border="0" /></a></td>
										  <td width="74" align="left" valign="top"><a href="'.$crm[1]['blink'].'">Hỗ trợ <br />
											Kỹ Thuật</a></td>
										</tr>
										<tr>
										  <td width="45" height="15" align="left" valign="top">&nbsp;</td>
										  <td width="81" height="15" align="left" valign="top">&nbsp;</td>
										  <td width="45" height="15" align="left" valign="top">&nbsp;</td>
										  <td width="74" height="15" align="left" valign="top">&nbsp;</td>
										</tr>
										<tr>
										  <td width="45" align="left" valign="top"><a href="'.$crm[2]['blink'].'"><img src="images/megahost_29.gif" alt="" width="41" height="45" border="0" /></a></td>
										  <td width="81" align="left" valign="top"><a href="'.$crm[2]['blink'].'">Than phiền<br />
											Dịch vụ</a></td>
										  <td width="45" align="left" valign="top"><a href="'.$crm[3]['blink'].'"><img src="images/megahost_31.gif" alt="" width="47" height="45" border="0" /></a></td>
										  <td width="74" align="left" valign="top"><a href="'.$crm[3]['blink'].'">Góp ý<br />
											Chất lượng</a></td>
										</tr>
									</table></td>
								  </tr>
								  <tr>
									<td align="left" valign="top"><img src="images/detail_10.gif" width="277" height="11" alt="" /></td>
								  </tr>
								</table>
								<!--SUPPORT-->
								</td>
							  </tr>
							  <tr>
								<td align="left" valign="top">&nbsp;</td>
							  </tr>
							</table></td>
							<td width="723" align="left" valign="top"><table width="723" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td align="left" valign="top">';
					
					// Get FAQ information
					$f = $db->getData('SELECT * FROM faq WHERE id="'.$this->fID.'"');	
					$this->title = $f['question'].' - ';
					$text .= '
								<!--FAQ-->
								<table width="723" border="0" cellspacing="0" cellpadding="0">
								  <tr>
									<td height="43" align="left" valign="top" background="images/detail_04.gif" class="danhmuc">Tiêu đề câu hỏi</td>
								  </tr>
								  <tr>
									<td align="left" valign="top" background="images/detail_15.gif" class="main"><table width="693" border="0" cellspacing="0" cellpadding="0">
										<tr>
										  <td align="left" valign="top"><a href="?mod=faqView&id='.$f['id'].'">'.$f['question'].'</a><br />
											'.$f['answer'].'
											</td>
										</tr>
									</table></td>
								  </tr>
								  <tr>
									<td align="left" valign="top"><img src="images/detail_16.gif" width="723" height="12" alt="" /></td>
								  </tr>
								</table>
								<!--FAQ-->';
								
					$text .= '
								</td>
							  </tr>
							  <tr>
								<td align="left" valign="top">&nbsp;</td>
							  </tr>
							  <tr>
								<td align="left" valign="top">
								<!--OTHER-->
								<table width="723" border="0" cellspacing="0" cellpadding="0">
								  <tr>
									<td height="43" align="left" valign="top" background="images/detail_04.gif" class="danhmuc">CÁC CÂU HỎI KHÁC</td>
								  </tr>
								  <tr>
									<td align="left" valign="top" background="images/detail_15.gif" class="main"><table width="693" border="0" cellspacing="0" cellpadding="0">
								    <tr>
										  <td align="left" valign="top">
										  <ul>';
										  
								// Get other FAQ
								$others_result = $db->query('SELECT * FROM faq WHERE id <> '.$this->fID.' ORDER BY id DESC LIMIT 7');
								while ($others = mysql_fetch_assoc($others_result))		
								$text .= '
											<li><a href="?mod=faqView&id='.$others['id'].'">'.$others['question'].'</a></li>';
								
							$text .= '				
										  </ul>
										  </td>
										</tr>
									</table></td>
								  </tr>
								  <tr>
									<td align="left" valign="top"><img src="images/detail_16.gif" width="723" height="12" alt="" /></td>
								  </tr>
								</table>
								<!--OTHER-->
								</td>
							  </tr>
							</table></td>
						  </tr>
						</table>						
						</td>
					</tr>
					<!--MIDDLE-->';

		$this->text = $text;
	}
	
	
}