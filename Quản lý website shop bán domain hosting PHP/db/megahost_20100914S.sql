-- phpMyAdmin SQL Dump
-- version 2.11.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 14, 2010 at 09:49 AM
-- Server version: 5.0.45
-- PHP Version: 5.2.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `megahost`
--

-- --------------------------------------------------------

--
-- Table structure for table `adm_members`
--

CREATE TABLE `adm_members` (
  `id` tinyint(3) NOT NULL auto_increment,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `login_key` varchar(32) NOT NULL,
  `groups` int(11) NOT NULL default '2',
  `name` varchar(50) NOT NULL,
  `identity` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `joined` int(11) NOT NULL,
  `last_activity` int(11) NOT NULL,
  `validate_code` varchar(6) NOT NULL,
  `active` tinyint(3) NOT NULL default '0',
  `last_visit` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `adm_members`
--

INSERT INTO `adm_members` (`id`, `username`, `password`, `login_key`, `groups`, `name`, `identity`, `email`, `address`, `telephone`, `joined`, `last_activity`, `validate_code`, `active`, `last_visit`) VALUES
(1, 'admin', 'qsnoweyMuNFQIyJzPRO2R2pKBspnwC', '9f6eg1jh7aejydlovnzoqzo24qijf5nm', 1, 'Do Ngoc Nhat Hoang', 0, 'admin@localhost.com', 'TPHCM', '906256258', 0, 1188152588, '', 1, 1284013358);

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `name` varchar(20) NOT NULL default '',
  `value` varchar(128) default NULL,
  PRIMARY KEY  (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`name`, `value`) VALUES
('AdminEmail', 'admin@localhost.com'),
('TimeZone', '25200'),
('timeout', '3600'),
('antiflood', '1200');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL auto_increment,
  `fullname` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `company` varchar(50) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `fax` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `senddate` int(11) NOT NULL,
  `isread` tinyint(3) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `fullname`, `address`, `company`, `phone`, `fax`, `email`, `title`, `content`, `senddate`, `isread`) VALUES
(2, 'Do Ngoc Nhat Hoang', '14A Chu Dong Tu, P7, TB', 'hoangcn02', '0938546342', '08 888738463', 'hoangcn02@yahoo.com', 'Demo title lien he hoangcn02', 'Noi dung lien he hoangcn02. demo noi dung lien he hoangcn02', 2147483647, 1),
(3, 'Le Thuy Minh Ngoc', '123A Nguyen Trong Tuye, P8, TB', 'hoangcn02', '0938543323', '08 88874363', 'minhngoc@yahoo.com', 'Demo title cua minh ngoc', 'Demo noi dung cua minh ngoc goi', 2147483647, 1);

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL auto_increment,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `question`, `answer`) VALUES
(1, 'Lam the nao de config domain va hosting chia se?', '<p>Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.</p>'),
(2, 'Lam the nao de config hosting va domain chia se?', 'Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.');

-- --------------------------------------------------------

--
-- Table structure for table `homeinfos`
--

CREATE TABLE `homeinfos` (
  `content` text NOT NULL,
  `img` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `homeinfos`
--

INSERT INTO `homeinfos` (`content`, `img`) VALUES
('<p>Noi dung dich vu tieu bieu tai trang</p>', '692141_26392-b.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `infos`
--

CREATE TABLE `infos` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `rank` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `infos`
--

INSERT INTO `infos` (`id`, `title`, `content`, `rank`) VALUES
(1, 'ThÃ´ng tin tham kháº£o sá»‘ 1', '<p>Ná»™i dung thÃ´ng tin tham kháº£o sá»‘ 1</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `name` varchar(255) NOT NULL,
  `icon` varchar(85) NOT NULL default '',
  `code` varchar(32) NOT NULL,
  `rank` tinyint(3) NOT NULL default '1',
  `active` tinyint(1) NOT NULL default '1',
  `isdefault` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`name`, `icon`, `code`, `rank`, `active`, `isdefault`) VALUES
('Viá»‡t Nam', 'vn.gif', 'vn', 3, 1, 1),
('English', 'en.gif', 'en', 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `intro` text NOT NULL,
  `content` text NOT NULL,
  `postdate` int(11) NOT NULL,
  `striking` tinyint(3) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `img`, `intro`, `content`, `postdate`, `striking`) VALUES
(1, 'TiÃªu Ä‘á» tin tá»©c má»›i', '747835_25789-b.jpg', '<p>Giá»›i thiá»‡u tin tá»©c má»›i. Giá»›i thiá»‡u tin tá»©c má»›i</p>', '<p>Noi dung dich vu tieu bieu tai trang chu. Atha ma ta nat ta ghi</p>\r\n<p>Noi dung dich vu tieu bieu tai trang chu. Atha ma ta nat ta ghi</p>', 1283514611, 1),
(2, 'TiÃªu Ä‘á» tin tá»©c má»›i', '624942_22512-b.jpg', '<p>Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i.</p>', '<p>Noi dung dich vu tieu bieu tai trang chu. Atha ma ta nat ta ghi</p>\r\n<p>Noi dung dich vu tieu bieu tai trang chu. Atha ma ta nat ta ghi</p>', 1283752939, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL auto_increment,
  `subject` tinyint(3) NOT NULL default '1',
  `fullname` varchar(50) NOT NULL,
  `company` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `fax` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `domain` varchar(30) NOT NULL,
  `service_type` int(11) NOT NULL,
  `service` int(11) NOT NULL,
  `service_plan` int(11) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `payment` int(11) NOT NULL default '1',
  `infos` text NOT NULL,
  `order_date` int(11) NOT NULL,
  `status` tinyint(3) NOT NULL default '0',
  `active` tinyint(3) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `subject`, `fullname`, `company`, `address`, `phone`, `fax`, `email`, `domain`, `service_type`, `service`, `service_plan`, `duration`, `payment`, `infos`, `order_date`, `status`, `active`) VALUES
(1, 1, 'Do Hoang', 'hoangcn02', 'sd fhdsf dsjdf', '544545645', '54644456', 'hoangcn02@yahoo.com', '', 1, 1, 1, '3 thang', 1, 'f gdfg dfgfd gfdg fd gdg', 1234588745, 1, 0),
(2, 1, 'Minh Ngoc', 'hoangcn02', 'dsf hdsf hdsfsdfdsjf', '54545644', '64644566', 'minhngoc@yahoo.com', 'minhngoc.com', 1, 1, 2, '5 thang', 2, 'dsf dfsf sd fds fsd fsdfdsf', 123459887, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `rank` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`id`, `title`, `content`, `rank`) VALUES
(1, 'HÆ°á»›ng dáº«n Ä‘Äƒng kÃ­ tÃªn miá»n', '<p><strong>1. TÃŠN MIá»€N</strong></p>\r\n<p><strong><em><span style="text-decoration: underline;">BÆ°á»›c 1</span></em></strong>: Báº¡n tiáº¿n hÃ nh <a href="http://nhanhoa.com/?site=whois">Kiá»ƒm tra tÃªn miá»n</a> vÃ  <a href="http://nhanhoa.com/?site=service&amp;view=group&amp;id=1">Báº£ng giÃ¡ tÃªn miá»n</a> Náº¿u káº¿t quáº£ tráº£ vá» lÃ : <strong>TÃªn miá»n www.***.com</strong> chÆ°a cÃ³ chá»§ thá»ƒ Ä‘Äƒng kÃ½, báº¡n cÃ³ thá»ƒ Ä‘Äƒng kÃ½ tÃªn miá»n nÃ y. Báº¡n tiáº¿p tá»¥c qua bÆ°á»›c 2.</p>\r\n<p><strong><em><span style="text-decoration: underline;">BÆ°á»›c 2</span></em></strong>: Báº¡n cÃ³ thá»ƒ thanh toÃ¡n trá»±c tiáº¿p táº¡i <a href="http://nhanhoa.com/?site=page&amp;id=24">vÄƒn phÃ²ng cÃ´ng ty</a> hoáº·c <a href="http://nhanhoa.com/?site=page&amp;id=24">chuyá»ƒn tiá»n</a> qua ngÃ¢n hÃ ng hoáº·c qua bÆ°u Ä‘iá»‡n. TrÆ°á»ng há»£p chuyá»ƒn tiá»n qua ngÃ¢n hÃ ng hoáº·c qua bÆ°u Ä‘iá»‡n, báº¡n email tá»›i Sales@nhanhoa.com (khu vá»±c HÃ  Ná»™i) hoáº·c hcmsales@nhanhoa.com (khu vá»±c HCM) Ä‘Ã­nh kÃ¨m thÃ´ng tin Ä‘Äƒng kÃ½ dá»‹ch vá»¥ vÃ  thÃ´ng tin xÃ¡c nháº­n thanh toÃ¡n (kÃ¨m báº£n scan phiáº¿u chuyá»ƒn tiá»n náº¿u báº¡n cáº§n dá»‹ch vá»¥ sá»›m) nhÆ° sau:</p>\r\n<p><strong>TiÃªu Ä‘á» (Subject):</strong> ÄÄƒng kÃ½ tÃªn miá»n &hellip; &hellip; &hellip;</p>\r\n<p><strong>Ná»™i dung (Body):</strong></p>\r\n<p>-	TÃªn chá»§ thá»ƒ Ä‘Äƒng kÃ½ tÃªn miá»n (TÃªn cÃ´ng ty hoáº·c CÃ¡ nhÃ¢n): ..... <br />\r\n-	TÃªn ngÆ°á»i phá»¥ trÃ¡ch: ..... <br />\r\n-	Äá»‹a chá»‰: ..... <br />\r\n-	Äiá»‡n thoáº¡i: ..... <br />\r\n-	Email quáº£n lÃ½ dá»‹ch vá»¥ (NhÃ¢n HÃ²a sáº½ liÃªn láº¡c vá»›i khÃ¡ch hÃ ng qua email nÃ y trong suá»‘t quÃ¡ trÃ¬nh sá»­ dá»¥ng dá»‹ch vá»¥): ..... <br />\r\n-	Sá»‘ CMT, ngÃ y cáº¥p, nÆ¡i cáº¥p (Äá»‘i vá»›i khÃ¡ch hÃ ng lÃ  cÃ¡ nhÃ¢n): ..... <br />\r\n-	TÃªn miá»n Ä‘Äƒng kÃ½: ..... <br />\r\n-	Thá»i háº¡n: ...../nÄƒm (Ã­t nháº¥t 01 nÄƒm, nhiá»u nháº¥t 10 nÄƒm)<br />\r\n- YÃªu cáº§u thÃªm: Trá» tÃªn miá»n vá» Ä‘á»‹a chá»‰ DNS vÃ  IP cá»¥ thá»ƒ (náº¿u cÃ³)</p>\r\n<p>-	<strong>LÆ°u Ã½ Ä‘á»‘i vá»›i tÃªn miá»n QUá»C GIA:</strong></p>\r\n<p>-	<strong>CÃ¡ nhÃ¢n</strong> cáº§n cÃ³ báº£n khai Ä‘Äƒng kÃ½ + Báº£n photo CMT.</p>\r\n<p>ÄÄ‚NG KÃ TÃŠN MIá»€N Cáº¤P 2 .VN : <a title="Download" target="_blank" href="http://nhanhoa.com/uploads/attach/1273636909_canhan_cap2.doc">Dowload báº£n khai</a><br />\r\nÄÄ‚NG KÃ TÃŠN MIá»€N Cáº¤P 3 .VN : <a title="Download" target="_blank" href="http://nhanhoa.com/uploads/attach/1273636914_canhan_cap3.doc">Dowload báº£n khai</a></p>\r\n<p>-	<strong>Doanh nghiá»‡p</strong> cáº§n cÃ³ báº£n khai Ä‘Äƒng kÃ½ + KÃ½ Ä‘Ã³ng dáº¥u cÃ´ng ty (3 báº£n). <a title="Download" target="_blank" href="http://nhanhoa.com/uploads/attach/1273636901_bankhai_congty.doc">Download báº£n khai</a></p>\r\n<p>- Báº£n khai cÃ³ 2 tá», cáº§n cÃ³ dáº¥u giÃ¡p lai, báº£n khai khÃ´ng Ä‘Æ°á»£c gáº¡ch bá», táº©y xÃ³a.</p>\r\n<p><strong><em><span style="text-decoration: underline;">BÆ°á»›c 3</span></em></strong>: Ngay sau khi nháº­n Ä‘Æ°á»£c thanh toÃ¡n &amp; email chÃºng tÃ´i sáº½ tiáº¿n hÃ nh khá»Ÿi táº¡o dá»‹ch vá»¥ vÃ  gá»­i thÃ´ng tin xÃ¡c nháº­n Ä‘áº¿n email quáº£n lÃ½ dá»‹ch vá»¥ cá»§a báº¡n sá»›m nháº¥t trong vÃ²ng 3h Ä‘áº¿n 24h.</p>', 1),
(3, 'ÄÄƒng kÃ­ hosting - server', '<p><strong><em><span style="text-decoration: underline;">BÆ°á»›c 1</span></em></strong>: Báº¡n tham kháº£o <a href="http://nhanhoa.com/?site=service&amp;view=group&amp;id=4">Báº£ng giÃ¡ hosting</a> vÃ  <a href="http://nhanhoa.com/?site=service&amp;view=group&amp;id=8">Báº£ng giÃ¡ server</a> táº¡i Ä‘Ã¢y</p>\r\n<p><strong><em><span style="text-decoration: underline;">BÆ°á»›c 2</span></em></strong>: Báº¡n cÃ³ thá»ƒ thanh toÃ¡n trá»±c tiáº¿p táº¡i <a href="http://nhanhoa.com/?site=page&amp;id=24">vÄƒn phÃ²ng cÃ´ng ty</a> hoáº·c <a href="http://nhanhoa.com/?site=page&amp;id=24">chuyá»ƒn tiá»n</a> qua ngÃ¢n hÃ ng hoáº·c qua bÆ°u Ä‘iá»‡n.</p>\r\n<p>TrÆ°á»ng há»£p chuyá»ƒn tiá»n qua ngÃ¢n hÃ ng hoáº·c qua bÆ°u Ä‘iá»‡n, báº¡n email tá»›i Sales@nhanhoa.com (khu vá»±c HÃ  Ná»™i) hoáº·c hcmsales@nhanhoa.com (khu vá»±c HCM) Ä‘Ã­nh kÃ¨m báº£n scan phiáº¿u chuyá»ƒn tiá»n vÃ  cÃ¡c thÃ´ng tin Ä‘Äƒng kÃ½ dá»‹ch vá»¥ nhÆ° sau:</p>\r\n<p><strong>TiÃªu Ä‘á» (Subject):</strong> ÄÄƒng kÃ½ dá»‹ch vá»¥ &hellip; &hellip; &hellip;</p>\r\n<p><strong>Ná»™i dung (Body):</strong></p>\r\n<p>-	TÃªn chá»§ thá»ƒ Ä‘Äƒng kÃ½ (TÃªn cÃ´ng ty hoáº·c CÃ¡ nhÃ¢n): ..... <br />\r\n-	TÃªn ngÆ°á»i phá»¥ trÃ¡ch: ..... <br />\r\n-	Äá»‹a chá»‰: ..... <br />\r\n-	Äiá»‡n thoáº¡i: ..... <br />\r\n-	Email quáº£n lÃ½ dá»‹ch vá»¥ (NhÃ¢n HÃ²a sáº½ liÃªn láº¡c vá»›i khÃ¡ch hÃ ng qua email nÃ y trong suá»‘t quÃ¡ trÃ¬nh sá»­ dá»¥ng dá»‹ch vá»¥): ..... <br />\r\n-	GÃ³i dá»‹ch vá»¥ cáº§n Ä‘Äƒng kÃ½/ Há»‡ Ä‘iá»u hÃ nh/ thá»i háº¡n sá»­ dá»¥ng (vÃ­ dá»¥: Windows Hosting - CÃ¡ NhÃ¢n/12 thÃ¡ng): ..... <br />\r\n-	YÃªu cáº§u thÃªm: Add domain: ..&hellip;vÃ o host (náº¿u cÃ³)</p>\r\n<p><strong><em><span style="text-decoration: underline;">BÆ°á»›c 3</span></em></strong>: Ngay sau khi nháº­n Ä‘Æ°á»£c thanh toÃ¡n &amp; email chÃºng tÃ´i sáº½ tiáº¿n hÃ nh khá»Ÿi táº¡o dá»‹ch vá»¥ vÃ  gá»­i thÃ´ng tin xÃ¡c nháº­n Ä‘áº¿n email quáº£n lÃ½ dá»‹ch vá»¥ cá»§a báº¡n sá»›m nháº¥t trong vÃ²ng 3h Ä‘áº¿n 24h.</p>', 2),
(4, 'Duy trÃ¬ dá»‹ch vá»¥', '<p>- Äá»‘i vá»›i cÃ¡c dá»‹ch vá»¥ Ä‘Ã£ Ä‘Äƒng kÃ½ trÆ°á»›c Ä‘Ã³, tá»›i háº¡n thanh toÃ¡n tiáº¿p theo cá»§a há»£p Ä‘á»“ng, NhÃ¢n HÃ²a sáº½ gá»­i yÃªu cáº§u thanh toÃ¡n phÃ­ duy trÃ¬, quÃ½ khÃ¡ch vui lÃ²ng thanh toÃ¡n theo biá»ƒu phÃ­ quy Ä‘á»‹nh Ä‘á»ƒ dá»‹ch vá»¥ cÃ³ thá»ƒ Ä‘Æ°á»£c tiáº¿p tá»¥c duy trÃ¬ vÃ  khÃ´ng bá»‹ giÃ¡n Ä‘oáº¡n.</p>\r\n<p>- TrÆ°á»ng há»£p quÃ¡ háº¡n thanh toÃ¡n vÃ  chÆ°a thanh toÃ¡n phÃ­ duy trÃ¬, dá»‹ch vá»¥ sáº½ bá»‹ táº¡m ngÆ°ng, nÃªn quÃ½ khÃ¡ch lÆ°u Ã½ chá»§ Ä‘á»™ng thanh toÃ¡n trÆ°á»›c thá»i gian háº¿t háº¡n há»£p Ä‘á»“ng.</p>\r\n<p>- Khi thay Ä‘á»•i thÃ´ng tin liÃªn há»‡ (Ä‘á»‹a chá»‰, Ä‘iá»‡n thoáº¡i, email...), QuÃ½ khÃ¡ch vui lÃ²ng thÃ´ng bÃ¡o láº¡i Ä‘á»ƒ chÃºng tÃ´i cáº­p nháº­t vÃ  liÃªn há»‡ khi cáº§n thiáº¿t. CÃ¡c trÆ°á»ng há»£p chuyá»ƒn khoáº£n duy trÃ¬ dá»‹ch vá»¥, QuÃ½ khÃ¡ch cáº§n ghi rÃµ thÃ´ng tin dá»‹ch vá»¥ cáº§n duy trÃ¬, trÃ¡nh tÃ¬nh tráº¡ng khÃ´ng xÃ¡c nháº­n Ä‘Æ°á»£c dá»‹ch vá»¥ cáº§n duy trÃ¬ dáº«n tá»›i háº¿t háº¡n dá»‹ch vá»¥.</p>', 3);

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL auto_increment,
  `type` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `rank` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `type`, `title`, `content`, `rank`) VALUES
(1, 2, 'Linux hosting', '<p>ds df ds fds fsdf dsfds fdsf dsf dsfds fsddsfsd fds fsdfdssd fds</p>', 1),
(2, 1, 'MÃ¡y chá»§ linux', '<p>Ná»™i dung mÃ¡y chá»§ linux</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service_intro`
--

CREATE TABLE `service_intro` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `intro` text NOT NULL,
  `content` text NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `rank` tinyint(3) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `service_intro`
--

INSERT INTO `service_intro` (`id`, `name`, `intro`, `content`, `thumb`, `img`, `rank`) VALUES
(2, 'TiÃªu Ä‘á» thÃ´ng tin giá»›i thiá»‡u dá»‹ch vá»¥', '<p>Giá»›i thiá»‡u vá» dá»‹ch vá»¥</p>', '<p>Ná»™i dung dá»‹ch vá»¥</p>', '276343_125x125-7.gif', '', 10),
(3, 'TiÃªu Ä‘á» dá»‹ch vá»¥ má»›i', '<p>Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.</p>', '<p>Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.</p>', '356969_ima3es.jpg', '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `service_plan`
--

CREATE TABLE `service_plan` (
  `id` int(11) NOT NULL auto_increment,
  `service_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `showhome` tinyint(3) NOT NULL default '0',
  `price` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `service_plan`
--

INSERT INTO `service_plan` (`id`, `service_id`, `title`, `content`, `showhome`, `price`) VALUES
(1, 1, 'GÃ³i dá»‹ch vá»¥ linux hosting sá»‘ 1', '<p>ds dsf dsf sd fsd fsd fsd fsdf sdf sdf sdf sdf</p>', 1, '320 VND'),
(2, 2, 'GÃ³i dá»‹ch vá»¥ Ä‘áº·t mÃ¡y chá»§ linux 1', '<p>Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng. Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng. Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng. Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng. Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng. Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng. Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng. Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng.</p>\r\n<p>Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng. Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng. Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng. Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng. Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng. Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng. Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng. Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng. Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng. Ná»™i dung gÃ³i dá»‹ch vá»¥ mÃ¡y chá»§ linux theo thÃ¡ng.</p>', 1, '500 VND');

-- --------------------------------------------------------

--
-- Table structure for table `service_type`
--

CREATE TABLE `service_type` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `rank` tinyint(3) NOT NULL default '1',
  `active` tinyint(3) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `service_type`
--

INSERT INTO `service_type` (`id`, `name`, `rank`, `active`) VALUES
(1, 'MÃ¡y chá»§', 1, 1),
(2, 'Hosting', 1, 1),
(3, 'TÃªn miá»n', 1, 1),
(4, 'NÆ¡i Ä‘áº·t mÃ¡y chá»§', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` varchar(32) NOT NULL,
  `username` varchar(32) default NULL,
  `member_id` int(11) default '0',
  `groups_id` int(11) NOT NULL default '0',
  `security_code` varchar(32) NOT NULL,
  `ip_address` varchar(15) default NULL,
  `running_time` int(10) default NULL,
  `register_time` int(11) NOT NULL,
  `antiflood` int(10) NOT NULL default '0',
  `language` varchar(32) default NULL,
  `skin` varchar(32) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `username`, `member_id`, `groups_id`, `security_code`, `ip_address`, `running_time`, `register_time`, `antiflood`, `language`, `skin`) VALUES
('403bb0aa6f18e61a359cc8ea16aaef5a', 'admin', 1, 1, '', '127.0.0.1', 1283947378, 0, 0, NULL, NULL),
('75c0cd459ac3522e4f105948bb83c4f2', 'admin', 1, 1, '', '127.0.0.1', 1283930199, 0, 0, NULL, NULL),
('8ca586042dc292d692abeab847600289', 'admin', 1, 1, '', '127.0.0.1', 1283969451, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `statistics`
--

CREATE TABLE `statistics` (
  `keyword` varchar(32) NOT NULL,
  `value` int(10) default NULL,
  PRIMARY KEY  (`keyword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `statistics`
--

INSERT INTO `statistics` (`keyword`, `value`) VALUES
('install_date', 1222876800),
('today_online', 1),
('last_update', 1283962956),
('most_online', 303),
('most_online_date', 1256922000),
('hitcounter', 46),
('online_now', 1);

-- --------------------------------------------------------

--
-- Table structure for table `support`
--

CREATE TABLE `support` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `rank` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `support`
--

INSERT INTO `support` (`id`, `title`, `content`, `rank`) VALUES
(1, 'Quy Ä‘á»‹nh sá»­ dá»¥ng dá»‹ch vá»¥', '<p><span class="highlight">1.</span> NgÆ°á»i sá»­ dá»¥ng pháº£i tá»± chá»‹u trÃ¡ch nhiá»‡m vÃ  Ä‘áº£m báº£o vá»›i chÃºng tÃ´i trong viá»‡c sá»­ dá»¥ng Web Site cá»§a mÃ¬nh trÃªn server vÃ o nhá»¯ng má»¥c Ä‘Ã­ch há»£p phÃ¡p. Äáº·c biá»‡t trong nhá»¯ng trÆ°á»ng há»£p sau:</p>\r\n<p class="indent-l1"><span class="highlight">1.1</span> KhÃ´ng dÃ¹ng server vÃ o báº¥t kÃ¬ má»¥c Ä‘Ã­ch/hÃ¬nh thá»©c nÃ o vi pháº¡m phÃ¡p luáº­t Viá»‡t Nam, Ä‘áº·c biá»‡t vá» váº¥n Ä‘á» báº£n quyá»n pháº§n má»m, ca khÃºc... Äá»“ng thá»i cÃ³ trÃ¡ch nhiá»‡m kiá»ƒm soÃ¡t vÃ  ngÄƒn cáº¥m ngÆ°á»i khÃ¡c lÃ m Ä‘iá»u Ä‘Ã³ trÃªn Website cá»§a mÃ¬nh.</p>\r\n<p class="indent-l1"><span class="highlight">1.2</span> NgÆ°á»i sá»­ dá»¥ng khÃ´ng Ä‘Æ°á»£c gá»­i, táº¡o liÃªn káº¿t hoáº·c trung chuyá»ƒn cho:</p>\r\n<p class="indent-l2"><span class="highlight">1.2.1</span> Báº¥t kÃ¬ loáº¡i dá»¯ liá»‡u nÃ o mang tÃ­nh báº¥t há»£p phÃ¡p, Ä‘e dá»a, lá»«a dá»‘i, thÃ¹ háº±n, xuyÃªn táº¡c, nÃ³i xáº¥u, tá»¥c tÄ©u, khiÃªu dÃ¢m, xÃºc pháº¡m...hay cÃ¡c hÃ¬nh thá»©c bá»‹ ngÄƒn cáº¥m khÃ¡c dÆ°á»›i báº¥t kÃ¬ cÃ¡ch thá»©c nÃ o.</p>\r\n<p class="indent-l2"><span class="highlight">1.2.2</span> Báº¥t kÃ¬ loáº¡i dá»¯ liá»‡u nÃ o mÃ  cáº¥u thÃ nh hoáº·c khuyáº¿n khÃ­ch cÃ¡c hÃ¬nh thá»©c pháº¡m tá»™i; hoáº·c cÃ¡c dá»¯ liá»‡u mang tÃ­nh vi pháº¡m luáº­t sÃ¡ng cháº¿, nhÃ£n hiá»‡u, quyá»n thiáº¿t káº¿, báº£n quyá»n hay báº¥t kÃ¬ quyá»n sá»¡ há»¯u trÃ­ tuá»‡ hoáº·c cÃ¡c quyá»n háº¡n cá»§a báº¥t kÃ¬ cÃ¡ nhÃ¢n nÃ o.</p>\r\n<p class="indent-l1"><span class="highlight">1.3</span> NgÆ°á»i sá»­ dá»¥ng khÃ´ng sá»­ dá»¥ng hosting Ä‘á»ƒ gá»­i SPAM, thÆ° QUáº¢NG CÃO, BULK MAIL, MASS MAIL, BOMB MAIL, &hellip;(báº¥t ká»ƒ tá»« Ä‘Ã¢u) cÃ³ liÃªn quan Ä‘áº¿n cÃ¡c web site lÆ°u trá»¯ trong há»‡ thá»‘ng mÃ¡y chá»§ (gá»­i tá»« 15 Email cÃ¹ng má»™t thá»i Ä‘iá»ƒm vá»›i cÃ¹ng má»™t ná»™i dung) hoáº·c mang má»¥c Ä‘Ã­ch phÃ¡ hoáº¡i tá»« server hay máº¡ng cá»§a chÃºng tÃ´i, khÃ´ng khuyáº¿n khÃ­ch má»™t site nÃ o Ä‘Ã³ Ä‘áº·t trÃªn server sá»­ dá»¥ng bulk email. NgÆ°á»i sá»­ dá»¥ng pháº£i Ä‘áº£m báº£o táº¥t cáº£ cÃ¡c mail Ä‘Æ°á»£c gá»­i Ä‘i phÃ¹ há»£p vá»›i cÃ¡c luáº­t cÃ³ thá»ƒ Ã¡p dá»¥ng (bao gá»“m luáº­t báº£o vá»‡ thÃ´ng tin) vÃ  dÆ°á»›i cÃ¡c hÃ¬nh thá»©c an toÃ n. Sá»‘ lÆ°á»£ng Email gá»­i ra ngoÃ i ná»™i bá»™ cÃ´ng ty khÃ´ng quÃ¡ 300 Email/ngÃ y</p>\r\n<p class="indent-l1"><span class="highlight">1.4</span> NgÆ°á»i sá»­ dá»¥ng khÃ´ng sá»­ dá»¥ng cÃ¡c chÆ°Æ¡ng trÃ¬nh cÃ³ kháº£ nÄƒng lÃ m táº¯c ngháº½n hoáº·c Ä‘Ã¬nh trá»‡ há»‡ thá»‘ng, nhÆ° gÃ¢y cáº¡n kiá»‡t tÃ i nguyÃªn há»‡ thá»‘ng, lÃ m quÃ¡ táº£i bá»™ vi xá»­ lÃ½ vÃ  bá»™ nhá»›..</p>\r\n<p><span class="highlight">2.</span> KhÃ´ng xÃ³a file LOGFILES trong thÆ° má»¥c /logs (Dung lÆ°á»£ng file logs khÃ´ng tÃ­nh vÃ o dung lÆ°á»£ng báº¡n mua)</p>\r\n<p><span class="highlight">3.</span> ChÃºng tÃ´i cÃ³ quyá»n loáº¡i bá» account mÃ  chÃºng tÃ´i cho lÃ  khÃ´ng phÃ¹ há»£p á»Ÿ site cá»§a báº¡n mÃ  khÃ´ng thÃ´ng bÃ¡o trÆ°á»›c: ChÃºng tÃ´i khÃ´ng cháº¥p nháº­n cÃ¡c pháº§n má»m mÃ  báº¡n khÃ´ng chá»©ng minh Ä‘Æ°á»£c báº£n quyá»n thuá»™c vá» cá»§a mÃ¬nh, cÃ¡c pháº§n má»m hÆ°á»›ng dáº«n báº» khÃ³a, shell, backdoor, cÃ¡c thÃ´ng tin xáº¥u hay cÃ¡c loáº¡i nháº¡c (MP3, Video...) khÃ´ng chá»©ng minh Ä‘Æ°á»£c báº£n quyá»n cá»§a ngÆ°á»i Ä‘Æ°a lÃªn</p>\r\n<p><span class="highlight">4.</span> NgÆ°á»i sá»­ dá»¥ng giá»¯ má»™t cÃ¡ch an toÃ n cÃ¡c thÃ´ng tin nháº­n biáº¿t, máº­t kháº©u hay nhá»¯ng thÃ´ng tin máº­t khÃ¡c liÃªn quan Ä‘áº¿n tÃ i khoáº£n cá»§a báº¡n vÃ  láº­p tá»©c thÃ´ng bÃ¡o cho chÃºng tÃ´i khi báº¡n phÃ¡t hiá»‡n cÃ¡c hÃ¬nh thá»©c truy cáº­p trÃ¡i phÃ©p báº±ng tÃ i khoáº£n cá»§a báº¡n hoáº·c cÃ¡c sÆ¡ há»Ÿ vá» báº£o máº­t, bao gá»“m viá»‡c máº¥t mÃ¡t, Ä‘Ã¡nh cáº¯p hoáº·c Ä‘á»ƒ lá»™ cÃ¡c thÃ´ng tin vá» máº­t kháº©u vÃ  cÃ¡c thÃ´ng tin báº£o máº­t khÃ¡c</p>\r\n<p><span class="highlight">5.</span> NgÆ°á»i sá»­ dá»¥ng tuÃ¢n thá»§ cÃ¡c thá»§ tá»¥c mÃ  chÃºng tÃ´i Ä‘Æ°a ra vÃ  sáº½ khÃ´ng Ä‘Æ°á»£c dÃ¹ng server vÃ o nhá»¯ng má»¥c Ä‘Ã­ch cÃ³ thá»ƒ gÃ¢y áº£nh hÆ°á»Ÿng Ä‘áº¿n cÃ¡c khÃ¡ch hÃ ng khÃ¡c cá»§a chÃºng tÃ´i.</p>\r\n<p><span class="highlight">6.</span> ChÃºng tÃ´i sao lÆ°u dá»¯ liá»‡u hÃ ng tuáº§n, tuy nhiÃªn viá»‡c sao lÆ°u nÃ y chá»‰ phá»¥c vá»¥ cho má»¥c Ä‘Ã­ch quáº£n lÃ½ cá»§a chÃºng tÃ´i. Náº¿u khÃ¡ch hÃ ng yÃªu cáº§u cung cáº¥p, chÃºng tÃ´i sáº½ gá»­i nhá»¯ng báº£n sao lÆ°u nhÆ°ng chÃºng tÃ´i khÃ´ng chá»‹u trÃ¡ch nhiá»‡m vá» nhá»¯ng sá»± cá»‘ xáº£y ra cho cÃ¡c dá»¯ liá»‡u nÃ y. VÃ¬ váº­y, chÃºng tÃ´i khuyáº¿n cÃ¡o khÃ¡ch hÃ ng nÃªn sao lÆ°u dá»¯ liá»‡u thÆ°á»ng xuyÃªn vá» PC cá»§a mÃ¬nh Ä‘á»ƒ trÃ¡nh nhá»¯ng sá»± cá»‘ Ä‘Ã¡ng tiáº¿c xáº£y ra.</p>\r\n<p><span class="highlight">7.</span> Trong khi dÃ¹ng cÃ¡c biá»‡n phÃ¡p cÃ³ thá»ƒ vÃ  ná»— lá»±c nháº±m báº£o Ä‘áº£m sá»± toÃ n váº¹n vÃ  an toÃ n cho server, chÃºng tÃ´i khÃ´ng chá»‹u trÃ¡ch nhiá»‡m bá»“i thÆ°á»ng dá»¯ liá»‡u cá»§a bÃªn A lÆ°u trá»¯ táº¡i bÃªn B trong trÆ°á»ng há»£p há»‡ thá»‘ng server bá»‹ phÃ¡ hoáº¡i tá»« nhá»¯ng ngÆ°á»i dÃ¹ng báº¥t há»£p phÃ¡p, cÃ¡c hacker hoáº·c do cÃ¡c sá»± cá»‘ báº¥t kháº£ khÃ¡ng: thiÃªn tai, hoáº£ hoáº¡n, há»‡ thá»‘ng mÃ¡y chá»§ há»ng váº­t lÃ½ pháº§n cá»©ng.</p>\r\n<p><span class="highlight">8.</span> ChÃºng tÃ´i sáº½ táº¡m ngÆ°ng dá»‹ch vá»¥ mÃ  khÃ´ng cáº§n thÃ´ng bÃ¡o trÆ°á»›c vÃ¬ lÃ½ do vi pháº¡m tá»« phÃ­a ngÆ°á»i sá»­ dá»¥ng á»Ÿ cÃ¡c má»¥c Ä‘Ã£ nÃªu trÃªn. TrÆ°á»ng há»£p khÃ¡ch hÃ ng cam káº¿t vá»›i chÃºng tÃ´i khÃ´ng tÃ¡i vi pháº¡m quy Ä‘á»‹nh sá»­ dá»¥ng sáº½ pháº£i gá»­i CÃ´ng vÄƒn cam káº¿t vÃ  ná»™p khoáº£n phÃ­ khá»Ÿi táº¡o láº¡i dá»‹ch vá»¥ lÃ  180.000 vnÄ‘.</p>\r\n<p>Trong trÆ°á»ng há»£p xáº¥u nháº¥t, chÃºng tÃ´i sáº½ buá»™c pháº£i cáº¯t dá»‹ch vá»¥ vÄ©nh viá»…n mÃ  khÃ´ng hoÃ n tráº£ láº¡i phÃ­.</p>', 1),
(4, 'Quy Ä‘á»‹nh vá» tÃªn miá»n quá»‘c táº¿', '<p><span style="color: rgb(247, 89, 7);"><strong>Má»™t sá»‘ quy Ä‘á»‹nh hiá»‡n hÃ nh cá»§a phÃ¡p luáº­t liÃªn quan tá»›i viá»‡c Ä‘Äƒng kÃ½, sá»­ dá»¥ng tÃªn miá»n quá»‘c táº¿ táº¡i Viá»‡t Nam</strong></span></p>\r\n<ol>\r\n <li><strong>I. Luáº­t cÃ´ng nghá»‡ ThÃ´ng tin cá»§a Quá»‘c há»™i nÆ°á»›c Cá»™ng hÃ²a xÃ£ há»™i chá»§ nghÄ©a Viá»‡t Nam sá»‘ 67/2006/QH11 ngÃ y 29 thÃ¡ng 6 nÄƒm 2006</strong></li>\r\n</ol>\r\n<p>&nbsp;<strong>Äiá»u 23</strong>. Thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­</p>\r\n<p>&nbsp;1. Tá»• chá»©c, cÃ¡ nhÃ¢n cÃ³ quyá»n thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ theo quy Ä‘á»‹nh cá»§a phÃ¡p luáº­t vÃ  chá»‹u trÃ¡ch nhiá»‡m quáº£n lÃ½ ná»™i dung vÃ  hoáº¡t Ä‘á»™ng trang thÃ´ng tin Ä‘iá»‡n tá»­ cá»§a mÃ¬nh.</p>\r\n<p>2. Tá»• chá»©c, cÃ¡ nhÃ¢n sá»­ dá»¥ng tÃªn miá»n quá»‘c gia Viá»‡t Nam &ldquo;.vn&rdquo; khi thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ khÃ´ng cáº§n thÃ´ng bÃ¡o vá»›i Bá»™ BÆ°u chÃ­nh, Viá»…n thÃ´ng. Tá»• chá»©c, cÃ¡ nhÃ¢n khi thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ khÃ´ng sá»­ dá»¥ng tÃªn miá»n quá»‘c gia Viá»‡t Nam &ldquo;.vn&rdquo; pháº£i thÃ´ng bÃ¡o trÃªn mÃ´i trÆ°á»ng máº¡ng táº¡i Ä‘á»‹a chá»‰: <a href="http://thongbaotenmien.vn/">http://thongbaotenmien.vn</a>&nbsp; vá»›i Bá»™ BÆ°u chÃ­nh, Viá»…n thÃ´ng nhá»¯ng thÃ´ng tin sau Ä‘Ã¢y:</p>\r\n<p>a) TÃªn tá»• chá»©c ghi trong quyáº¿t Ä‘á»‹nh thÃ nh láº­p, giáº¥y phÃ©p hoáº¡t Ä‘á»™ng, giáº¥y chá»©ng nháº­n Ä‘Äƒng kÃ½ kinh doanh hoáº·c giáº¥y phÃ©p má»Ÿ vÄƒn phÃ²ng Ä‘áº¡i diá»‡n; tÃªn cÃ¡ nhÃ¢n;</p>\r\n<p>b) Sá»‘, ngÃ y cáº¥p, nÆ¡i cáº¥p chá»©ng minh thÆ° nhÃ¢n dÃ¢n hoáº·c sá»‘, ngÃ y cáº¥p, nÆ¡i cáº¥p há»™ chiáº¿u cá»§a cÃ¡ nhÃ¢n;</p>\r\n<p>c) Äá»‹a chá»‰ trá»¥ sá»Ÿ chÃ­nh cá»§a tá»• chá»©c hoáº·c nÆ¡i thÆ°á»ng trÃº cá»§a cÃ¡ nhÃ¢n;</p>\r\n<p>d) Sá»‘ Ä‘iá»‡n thoáº¡i, sá»‘ fax, Ä‘á»‹a chá»‰ thÆ° Ä‘iá»‡n tá»­;</p>\r\n<p>Ä‘) CÃ¡c tÃªn miá»n Ä‘Ã£ Ä‘Äƒng kÃ½.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n<p>3. Tá»• chá»©c, cÃ¡ nhÃ¢n pháº£i chá»‹u trÃ¡ch nhiá»‡m trÆ°á»›c phÃ¡p luáº­t vá» tÃ­nh chÃ­nh xÃ¡c cá»§a cÃ¡c thÃ´ng tin quy Ä‘á»‹nh táº¡i khoáº£n 2 Äiá»u nÃ y, khi thay Ä‘á»•i thÃ´ng tin&nbsp; thÃ¬ pháº£i thÃ´ng bÃ¡o vá» sá»± thay Ä‘á»•i Ä‘Ã³.</p>\r\n<ol>\r\n <li><strong>II. NGHá»Š Äá»ŠNH Sá»‘: 28/2009/NÄ-CP </strong></li>\r\n</ol>\r\n<p><strong>Quy Ä‘á»‹nh xá»­ pháº¡t vi pháº¡m hÃ nh chÃ­nh trong quáº£n lÃ½, cung cáº¥p, sá»­ dá»¥ng dá»‹ch vá»¥ Internet vÃ  thÃ´ng tin Ä‘iá»‡n tá»­ trÃªn Internet</strong></p>\r\n<p><strong>Äiá»u 11. </strong>Vi pháº¡m cÃ¡c quy Ä‘á»‹nh vá» sá»­ dá»¥ng tÃªn miá»n Internet</p>\r\n<p>1. Pháº¡t tiá»n tá»« 2.000.000 Ä‘á»“ng Ä‘áº¿n 5.000.000 Ä‘á»“ng Ä‘á»‘i vá»›i hÃ nh vi sá»­ dá»¥ng tÃªn miá»n cáº¥p cao khÃ¡c tÃªn miá»n &quot;.vn&quot; mÃ  khÃ´ng thÃ´ng bÃ¡o hoáº·c thÃ´ng bÃ¡o thÃ´ng tin khÃ´ng chÃ­nh xÃ¡c hoáº·c thay Ä‘á»•i thÃ´ng tin mÃ  khÃ´ng thÃ´ng bÃ¡o&nbsp; Bá»™ ThÃ´ng tin vÃ  Truyá»n thÃ´ng theo quy Ä‘á»‹nh; khai bÃ¡o thÃ´ng tin khÃ´ng chÃ­nh xÃ¡c hoáº·c khÃ´ng cáº­p nháº­t khi cÃ³ thay Ä‘á»•i thÃ´ng tin vá» tÃªn, Ä‘á»‹a chá»‰ liÃªn há»‡ Ä‘á»‘i vá»›i tá»• chá»©c hoáº·c tÃªn, Ä‘á»‹a chá»‰ liÃªn há»‡, sá»‘ giáº¥y chá»©ng minh nhÃ¢n dÃ¢n hoáº·c sá»‘ há»™ chiáº¿u Ä‘á»‘i vá»›i cÃ¡ nhÃ¢n Ä‘Äƒng kÃ½, sá»­ dá»¥ng tÃªn miá»n &quot;.vn&quot;.</p>', 2);
