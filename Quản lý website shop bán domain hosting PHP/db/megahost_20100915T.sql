-- phpMyAdmin SQL Dump
-- version 2.11.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 16, 2010 at 02:14 AM
-- Server version: 5.0.45
-- PHP Version: 5.2.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `megahost`
--

-- --------------------------------------------------------

--
-- Table structure for table `adm_members`
--

CREATE TABLE `adm_members` (
  `id` tinyint(3) NOT NULL auto_increment,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `login_key` varchar(32) NOT NULL,
  `groups` int(11) NOT NULL default '2',
  `name` varchar(50) NOT NULL,
  `identity` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `joined` int(11) NOT NULL,
  `last_activity` int(11) NOT NULL,
  `validate_code` varchar(6) NOT NULL,
  `active` tinyint(3) NOT NULL default '0',
  `last_visit` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `adm_members`
--

INSERT INTO `adm_members` (`id`, `username`, `password`, `login_key`, `groups`, `name`, `identity`, `email`, `address`, `telephone`, `joined`, `last_activity`, `validate_code`, `active`, `last_visit`) VALUES
(1, 'admin', 'qsnoweyMuNFQIyJzPRO2R2pKBspnwC', '9f6eg1jh7aejydlovnzoqzo24qijf5nm', 1, 'Do Ngoc Nhat Hoang', 0, 'admin@localhost.com', 'TPHCM', '906256258', 0, 1188152588, '', 1, 1284615472);

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `name` varchar(20) NOT NULL default '',
  `value` varchar(128) default NULL,
  PRIMARY KEY  (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`name`, `value`) VALUES
('AdminEmail', 'admin@localhost.com'),
('TimeZone', '25200'),
('timeout', '3600'),
('antiflood', '1200');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL auto_increment,
  `fullname` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `company` varchar(50) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `fax` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `senddate` int(11) NOT NULL,
  `isread` tinyint(3) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `fullname`, `address`, `company`, `phone`, `fax`, `email`, `title`, `content`, `senddate`, `isread`) VALUES
(3, 'dsf sdf dsfsdfds', '', '', '0938549024', '', 'hoangcvndsfsd@yaoo.com', 'df dsf sd fds', 'ef sfsd', 1284549858, 0),
(4, 'Nguyen Van Minh', 'df sdfsf', 'Cong Ty A', '465546456', '7856546546546', 'dfhj@yahoo.com', 'dif sdf', 'h dufdf', 1284550151, 0),
(5, 'Bui LÃª LÃ½', 'Le Quang Äá»‹nh', 'Zui Za', '045656546', '045643545345435', 'leminh@gmail.com', 'dsdfdsfsdf', 'dfdsfd', 1284550208, 0),
(6, 'NgÃ´ Vá»¹ NguyÃªn', '123 LÃª VÄƒn Thá», P2, GV', 'NhÃ  Viá»‡t', '0935478145', '088885478', 'vynguyen@yahoo.com', 'TiÃªu Ä‘á» liÃªn há»‡', 'Ná»™i dung liÃªn há»‡\r\n\r\n', 1284565154, 1);

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL auto_increment,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `question`, `answer`) VALUES
(4, 'LÃ m tháº¿ nÃ o Ä‘á»ƒ báº£o vá»‡ tÃªn miá»n?', '<p>1.Báº£o vá»‡ máº­t kháº©u tÃªn miá»n cá»§a báº¡n má»™t cÃ¡ch cáº©n tháº­n, sá»­ dá»¥ng nhá»¯ng máº­t kháº©u máº¡nh tá»‘i thiá»ƒu lÃ  8 kÃ½ tá»± trá»Ÿ lÃªn káº¿t há»£p chá»¯ sá»‘ vÃ  chá»¯ cÃ¡i<br />\r\n<br />\r\n2. Khi báº¡n Ä‘Äƒng kÃ½ tÃªn miá»n, nÃªn cung cáº¥p Ä‘á»‹a chá»‰ email mÃ  báº¡n thÆ°á»ng xuyÃªn dÃ¹ng Ä‘á»ƒ check cÃ¡c thÃ´ng tin liÃªn quan Ä‘áº¿n tÃªn miá»n cá»§a báº¡n do nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n gá»­i.<br />\r\n<br />\r\n3. Náº¿u báº¡n tá»«ng thay Ä‘á»•i Ä‘á»‹a chá»‰ email cá»§a mÃ¬nh, hÃ£y truy nháº­p vÃ o tÃ i khoáº£n domain cá»§a mÃ¬nh vÃ  cáº­p nháº­t thÃ´ng tin Ä‘á»‹a chá»‰ email giÃºp báº¡n luÃ´n nháº­n thÃ´ng tin tá»« nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n<br />\r\n<br />\r\n4. Äá»«ng Ä‘Æ°a UserID vÃ  Password cá»§a tÃ i khoáº£n email Ä‘Ã³ cho ngÆ°á»i khÃ¡c<br />\r\n<br />\r\n5. Náº¿u báº¡n tháº¥y báº¥t cá»© yÃªu cáº§u nÃ o nghi ngá» liÃªn quan Ä‘áº¿n domain cá»§a báº¡n, hÃ£y liÃªn láº¡c vá»›i nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n cá»§a báº¡n ngay láº­p tá»©c.<br />\r\n<br />\r\n6. Khi báº¡n nháº­n Ä‘Æ°á»£c má»™t email thÃ´ng bÃ¡o gia háº¡n tÃªn miá»n, hÃ£y gia háº¡n tÃªn miá»n cá»§a mÃ¬nh ngay láº­p tá»©c. ThÃ´ng thÆ°á»ng tÃªn miá»n cá»§a báº¡n Ä‘Æ°á»£c báº£o lÆ°u 30 ngÃ y ká» tá»« ngÃ y háº¿t háº¡n. Ráº¥t nhiá»u ngÆ°á»i cháº§n chá»« khÃ´ng gia háº¡n dáº«n Ä‘áº¿n máº¥t tÃªn miá»n. NÃªn gia háº¡n tÃªn miá»n nhiá»u nÄƒm.<br />\r\n<br />\r\n6. TÃªn miá»n cá»§a báº¡n luÃ´n Ä‘Æ°á»£c Ä‘áº·t á»Ÿ cháº¿ Ä‘á»™ KHÃ“A.Vá»›i cháº¿ Ä‘á»™ nÃ y cÃ¡c yÃªu cáº§u vá» transfer domain sáº½ bá»‹ tá»« chá»‘i. Náº¿u báº¡n khÃ´ng cháº¯c lÃ m tháº¿ nÃ o Ä‘á»ƒ lÃ m Ä‘iá»u nÃ y thÃ¬ hÃ£y liÃªn láº¡c vá»›i nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n.<br />\r\n<br />\r\n7. KhÃ´ng giao UserID/Password truy cáº­p tÃ i khoáº£n tÃªn miá»n cho ngÆ°á»i khÃ¡c.</p>'),
(5, 'LÃ m tháº¿ nÃ o Ä‘á»ƒ báº£o vá»‡ tÃªn miá»n?', '<p>1.Báº£o vá»‡ máº­t kháº©u tÃªn miá»n cá»§a báº¡n má»™t cÃ¡ch cáº©n tháº­n, sá»­ dá»¥ng nhá»¯ng máº­t kháº©u máº¡nh tá»‘i thiá»ƒu lÃ  8 kÃ½ tá»± trá»Ÿ lÃªn káº¿t há»£p chá»¯ sá»‘ vÃ  chá»¯ cÃ¡i<br />\r\n<br />\r\n2. Khi báº¡n Ä‘Äƒng kÃ½ tÃªn miá»n, nÃªn cung cáº¥p Ä‘á»‹a chá»‰ email mÃ  báº¡n thÆ°á»ng xuyÃªn dÃ¹ng Ä‘á»ƒ check cÃ¡c thÃ´ng tin liÃªn quan Ä‘áº¿n tÃªn miá»n cá»§a báº¡n do nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n gá»­i.<br />\r\n<br />\r\n3. Náº¿u báº¡n tá»«ng thay Ä‘á»•i Ä‘á»‹a chá»‰ email cá»§a mÃ¬nh, hÃ£y truy nháº­p vÃ o tÃ i khoáº£n domain cá»§a mÃ¬nh vÃ  cáº­p nháº­t thÃ´ng tin Ä‘á»‹a chá»‰ email giÃºp báº¡n luÃ´n nháº­n thÃ´ng tin tá»« nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n<br />\r\n<br />\r\n4. Äá»«ng Ä‘Æ°a UserID vÃ  Password cá»§a tÃ i khoáº£n email Ä‘Ã³ cho ngÆ°á»i khÃ¡c<br />\r\n<br />\r\n5. Náº¿u báº¡n tháº¥y báº¥t cá»© yÃªu cáº§u nÃ o nghi ngá» liÃªn quan Ä‘áº¿n domain cá»§a báº¡n, hÃ£y liÃªn láº¡c vá»›i nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n cá»§a báº¡n ngay láº­p tá»©c.<br />\r\n<br />\r\n6. Khi báº¡n nháº­n Ä‘Æ°á»£c má»™t email thÃ´ng bÃ¡o gia háº¡n tÃªn miá»n, hÃ£y gia háº¡n tÃªn miá»n cá»§a mÃ¬nh ngay láº­p tá»©c. ThÃ´ng thÆ°á»ng tÃªn miá»n cá»§a báº¡n Ä‘Æ°á»£c báº£o lÆ°u 30 ngÃ y ká» tá»« ngÃ y háº¿t háº¡n. Ráº¥t nhiá»u ngÆ°á»i cháº§n chá»« khÃ´ng gia háº¡n dáº«n Ä‘áº¿n máº¥t tÃªn miá»n. NÃªn gia háº¡n tÃªn miá»n nhiá»u nÄƒm.<br />\r\n<br />\r\n6. TÃªn miá»n cá»§a báº¡n luÃ´n Ä‘Æ°á»£c Ä‘áº·t á»Ÿ cháº¿ Ä‘á»™ KHÃ“A.Vá»›i cháº¿ Ä‘á»™ nÃ y cÃ¡c yÃªu cáº§u vá» transfer domain sáº½ bá»‹ tá»« chá»‘i. Náº¿u báº¡n khÃ´ng cháº¯c lÃ m tháº¿ nÃ o Ä‘á»ƒ lÃ m Ä‘iá»u nÃ y thÃ¬ hÃ£y liÃªn láº¡c vá»›i nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n.<br />\r\n<br />\r\n7. KhÃ´ng giao UserID/Password truy cáº­p tÃ i khoáº£n tÃªn miá»n cho ngÆ°á»i khÃ¡c.</p>'),
(6, 'LÃ m tháº¿ nÃ o Ä‘á»ƒ báº£o vá»‡ tÃªn miá»n?', '<p>1.Báº£o vá»‡ máº­t kháº©u tÃªn miá»n cá»§a báº¡n má»™t cÃ¡ch cáº©n tháº­n, sá»­ dá»¥ng nhá»¯ng máº­t kháº©u máº¡nh tá»‘i thiá»ƒu lÃ  8 kÃ½ tá»± trá»Ÿ lÃªn káº¿t há»£p chá»¯ sá»‘ vÃ  chá»¯ cÃ¡i<br />\r\n<br />\r\n2. Khi báº¡n Ä‘Äƒng kÃ½ tÃªn miá»n, nÃªn cung cáº¥p Ä‘á»‹a chá»‰ email mÃ  báº¡n thÆ°á»ng xuyÃªn dÃ¹ng Ä‘á»ƒ check cÃ¡c thÃ´ng tin liÃªn quan Ä‘áº¿n tÃªn miá»n cá»§a báº¡n do nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n gá»­i.<br />\r\n<br />\r\n3. Náº¿u báº¡n tá»«ng thay Ä‘á»•i Ä‘á»‹a chá»‰ email cá»§a mÃ¬nh, hÃ£y truy nháº­p vÃ o tÃ i khoáº£n domain cá»§a mÃ¬nh vÃ  cáº­p nháº­t thÃ´ng tin Ä‘á»‹a chá»‰ email giÃºp báº¡n luÃ´n nháº­n thÃ´ng tin tá»« nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n<br />\r\n<br />\r\n4. Äá»«ng Ä‘Æ°a UserID vÃ  Password cá»§a tÃ i khoáº£n email Ä‘Ã³ cho ngÆ°á»i khÃ¡c<br />\r\n<br />\r\n5. Náº¿u báº¡n tháº¥y báº¥t cá»© yÃªu cáº§u nÃ o nghi ngá» liÃªn quan Ä‘áº¿n domain cá»§a báº¡n, hÃ£y liÃªn láº¡c vá»›i nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n cá»§a báº¡n ngay láº­p tá»©c.<br />\r\n<br />\r\n6. Khi báº¡n nháº­n Ä‘Æ°á»£c má»™t email thÃ´ng bÃ¡o gia háº¡n tÃªn miá»n, hÃ£y gia háº¡n tÃªn miá»n cá»§a mÃ¬nh ngay láº­p tá»©c. ThÃ´ng thÆ°á»ng tÃªn miá»n cá»§a báº¡n Ä‘Æ°á»£c báº£o lÆ°u 30 ngÃ y ká» tá»« ngÃ y háº¿t háº¡n. Ráº¥t nhiá»u ngÆ°á»i cháº§n chá»« khÃ´ng gia háº¡n dáº«n Ä‘áº¿n máº¥t tÃªn miá»n. NÃªn gia háº¡n tÃªn miá»n nhiá»u nÄƒm.<br />\r\n<br />\r\n6. TÃªn miá»n cá»§a báº¡n luÃ´n Ä‘Æ°á»£c Ä‘áº·t á»Ÿ cháº¿ Ä‘á»™ KHÃ“A.Vá»›i cháº¿ Ä‘á»™ nÃ y cÃ¡c yÃªu cáº§u vá» transfer domain sáº½ bá»‹ tá»« chá»‘i. Náº¿u báº¡n khÃ´ng cháº¯c lÃ m tháº¿ nÃ o Ä‘á»ƒ lÃ m Ä‘iá»u nÃ y thÃ¬ hÃ£y liÃªn láº¡c vá»›i nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n.<br />\r\n<br />\r\n7. KhÃ´ng giao UserID/Password truy cáº­p tÃ i khoáº£n tÃªn miá»n cho ngÆ°á»i khÃ¡c.</p>'),
(7, 'LÃ m tháº¿ nÃ o Ä‘á»ƒ báº£o vá»‡ tÃªn miá»n?', '<p>1.Báº£o vá»‡ máº­t kháº©u tÃªn miá»n cá»§a báº¡n má»™t cÃ¡ch cáº©n tháº­n, sá»­ dá»¥ng nhá»¯ng máº­t kháº©u máº¡nh tá»‘i thiá»ƒu lÃ  8 kÃ½ tá»± trá»Ÿ lÃªn káº¿t há»£p chá»¯ sá»‘ vÃ  chá»¯ cÃ¡i<br />\r\n<br />\r\n2. Khi báº¡n Ä‘Äƒng kÃ½ tÃªn miá»n, nÃªn cung cáº¥p Ä‘á»‹a chá»‰ email mÃ  báº¡n thÆ°á»ng xuyÃªn dÃ¹ng Ä‘á»ƒ check cÃ¡c thÃ´ng tin liÃªn quan Ä‘áº¿n tÃªn miá»n cá»§a báº¡n do nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n gá»­i.<br />\r\n<br />\r\n3. Náº¿u báº¡n tá»«ng thay Ä‘á»•i Ä‘á»‹a chá»‰ email cá»§a mÃ¬nh, hÃ£y truy nháº­p vÃ o tÃ i khoáº£n domain cá»§a mÃ¬nh vÃ  cáº­p nháº­t thÃ´ng tin Ä‘á»‹a chá»‰ email giÃºp báº¡n luÃ´n nháº­n thÃ´ng tin tá»« nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n<br />\r\n<br />\r\n4. Äá»«ng Ä‘Æ°a UserID vÃ  Password cá»§a tÃ i khoáº£n email Ä‘Ã³ cho ngÆ°á»i khÃ¡c<br />\r\n<br />\r\n5. Náº¿u báº¡n tháº¥y báº¥t cá»© yÃªu cáº§u nÃ o nghi ngá» liÃªn quan Ä‘áº¿n domain cá»§a báº¡n, hÃ£y liÃªn láº¡c vá»›i nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n cá»§a báº¡n ngay láº­p tá»©c.<br />\r\n<br />\r\n6. Khi báº¡n nháº­n Ä‘Æ°á»£c má»™t email thÃ´ng bÃ¡o gia háº¡n tÃªn miá»n, hÃ£y gia háº¡n tÃªn miá»n cá»§a mÃ¬nh ngay láº­p tá»©c. ThÃ´ng thÆ°á»ng tÃªn miá»n cá»§a báº¡n Ä‘Æ°á»£c báº£o lÆ°u 30 ngÃ y ká» tá»« ngÃ y háº¿t háº¡n. Ráº¥t nhiá»u ngÆ°á»i cháº§n chá»« khÃ´ng gia háº¡n dáº«n Ä‘áº¿n máº¥t tÃªn miá»n. NÃªn gia háº¡n tÃªn miá»n nhiá»u nÄƒm.<br />\r\n<br />\r\n6. TÃªn miá»n cá»§a báº¡n luÃ´n Ä‘Æ°á»£c Ä‘áº·t á»Ÿ cháº¿ Ä‘á»™ KHÃ“A.Vá»›i cháº¿ Ä‘á»™ nÃ y cÃ¡c yÃªu cáº§u vá» transfer domain sáº½ bá»‹ tá»« chá»‘i. Náº¿u báº¡n khÃ´ng cháº¯c lÃ m tháº¿ nÃ o Ä‘á»ƒ lÃ m Ä‘iá»u nÃ y thÃ¬ hÃ£y liÃªn láº¡c vá»›i nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n.<br />\r\n<br />\r\n7. KhÃ´ng giao UserID/Password truy cáº­p tÃ i khoáº£n tÃªn miá»n cho ngÆ°á»i khÃ¡c.</p>'),
(8, 'LÃ m tháº¿ nÃ o Ä‘á»ƒ báº£o vá»‡ tÃªn miá»n?', '<p>1.Báº£o vá»‡ máº­t kháº©u tÃªn miá»n cá»§a báº¡n má»™t cÃ¡ch cáº©n tháº­n, sá»­ dá»¥ng nhá»¯ng máº­t kháº©u máº¡nh tá»‘i thiá»ƒu lÃ  8 kÃ½ tá»± trá»Ÿ lÃªn káº¿t há»£p chá»¯ sá»‘ vÃ  chá»¯ cÃ¡i<br />\r\n<br />\r\n2. Khi báº¡n Ä‘Äƒng kÃ½ tÃªn miá»n, nÃªn cung cáº¥p Ä‘á»‹a chá»‰ email mÃ  báº¡n thÆ°á»ng xuyÃªn dÃ¹ng Ä‘á»ƒ check cÃ¡c thÃ´ng tin liÃªn quan Ä‘áº¿n tÃªn miá»n cá»§a báº¡n do nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n gá»­i.<br />\r\n<br />\r\n3. Náº¿u báº¡n tá»«ng thay Ä‘á»•i Ä‘á»‹a chá»‰ email cá»§a mÃ¬nh, hÃ£y truy nháº­p vÃ o tÃ i khoáº£n domain cá»§a mÃ¬nh vÃ  cáº­p nháº­t thÃ´ng tin Ä‘á»‹a chá»‰ email giÃºp báº¡n luÃ´n nháº­n thÃ´ng tin tá»« nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n<br />\r\n<br />\r\n4. Äá»«ng Ä‘Æ°a UserID vÃ  Password cá»§a tÃ i khoáº£n email Ä‘Ã³ cho ngÆ°á»i khÃ¡c<br />\r\n<br />\r\n5. Náº¿u báº¡n tháº¥y báº¥t cá»© yÃªu cáº§u nÃ o nghi ngá» liÃªn quan Ä‘áº¿n domain cá»§a báº¡n, hÃ£y liÃªn láº¡c vá»›i nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n cá»§a báº¡n ngay láº­p tá»©c.<br />\r\n<br />\r\n6. Khi báº¡n nháº­n Ä‘Æ°á»£c má»™t email thÃ´ng bÃ¡o gia háº¡n tÃªn miá»n, hÃ£y gia háº¡n tÃªn miá»n cá»§a mÃ¬nh ngay láº­p tá»©c. ThÃ´ng thÆ°á»ng tÃªn miá»n cá»§a báº¡n Ä‘Æ°á»£c báº£o lÆ°u 30 ngÃ y ká» tá»« ngÃ y háº¿t háº¡n. Ráº¥t nhiá»u ngÆ°á»i cháº§n chá»« khÃ´ng gia háº¡n dáº«n Ä‘áº¿n máº¥t tÃªn miá»n. NÃªn gia háº¡n tÃªn miá»n nhiá»u nÄƒm.<br />\r\n<br />\r\n6. TÃªn miá»n cá»§a báº¡n luÃ´n Ä‘Æ°á»£c Ä‘áº·t á»Ÿ cháº¿ Ä‘á»™ KHÃ“A.Vá»›i cháº¿ Ä‘á»™ nÃ y cÃ¡c yÃªu cáº§u vá» transfer domain sáº½ bá»‹ tá»« chá»‘i. Náº¿u báº¡n khÃ´ng cháº¯c lÃ m tháº¿ nÃ o Ä‘á»ƒ lÃ m Ä‘iá»u nÃ y thÃ¬ hÃ£y liÃªn láº¡c vá»›i nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n.<br />\r\n<br />\r\n7. KhÃ´ng giao UserID/Password truy cáº­p tÃ i khoáº£n tÃªn miá»n cho ngÆ°á»i khÃ¡c.</p>'),
(9, 'LÃ m tháº¿ nÃ o Ä‘á»ƒ báº£o vá»‡ tÃªn miá»n?', '<p>1.Báº£o vá»‡ máº­t kháº©u tÃªn miá»n cá»§a báº¡n má»™t cÃ¡ch cáº©n tháº­n, sá»­ dá»¥ng nhá»¯ng máº­t kháº©u máº¡nh tá»‘i thiá»ƒu lÃ  8 kÃ½ tá»± trá»Ÿ lÃªn káº¿t há»£p chá»¯ sá»‘ vÃ  chá»¯ cÃ¡i<br />\r\n<br />\r\n2. Khi báº¡n Ä‘Äƒng kÃ½ tÃªn miá»n, nÃªn cung cáº¥p Ä‘á»‹a chá»‰ email mÃ  báº¡n thÆ°á»ng xuyÃªn dÃ¹ng Ä‘á»ƒ check cÃ¡c thÃ´ng tin liÃªn quan Ä‘áº¿n tÃªn miá»n cá»§a báº¡n do nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n gá»­i.<br />\r\n<br />\r\n3. Náº¿u báº¡n tá»«ng thay Ä‘á»•i Ä‘á»‹a chá»‰ email cá»§a mÃ¬nh, hÃ£y truy nháº­p vÃ o tÃ i khoáº£n domain cá»§a mÃ¬nh vÃ  cáº­p nháº­t thÃ´ng tin Ä‘á»‹a chá»‰ email giÃºp báº¡n luÃ´n nháº­n thÃ´ng tin tá»« nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n<br />\r\n<br />\r\n4. Äá»«ng Ä‘Æ°a UserID vÃ  Password cá»§a tÃ i khoáº£n email Ä‘Ã³ cho ngÆ°á»i khÃ¡c<br />\r\n<br />\r\n5. Náº¿u báº¡n tháº¥y báº¥t cá»© yÃªu cáº§u nÃ o nghi ngá» liÃªn quan Ä‘áº¿n domain cá»§a báº¡n, hÃ£y liÃªn láº¡c vá»›i nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n cá»§a báº¡n ngay láº­p tá»©c.<br />\r\n<br />\r\n6. Khi báº¡n nháº­n Ä‘Æ°á»£c má»™t email thÃ´ng bÃ¡o gia háº¡n tÃªn miá»n, hÃ£y gia háº¡n tÃªn miá»n cá»§a mÃ¬nh ngay láº­p tá»©c. ThÃ´ng thÆ°á»ng tÃªn miá»n cá»§a báº¡n Ä‘Æ°á»£c báº£o lÆ°u 30 ngÃ y ká» tá»« ngÃ y háº¿t háº¡n. Ráº¥t nhiá»u ngÆ°á»i cháº§n chá»« khÃ´ng gia háº¡n dáº«n Ä‘áº¿n máº¥t tÃªn miá»n. NÃªn gia háº¡n tÃªn miá»n nhiá»u nÄƒm.<br />\r\n<br />\r\n6. TÃªn miá»n cá»§a báº¡n luÃ´n Ä‘Æ°á»£c Ä‘áº·t á»Ÿ cháº¿ Ä‘á»™ KHÃ“A.Vá»›i cháº¿ Ä‘á»™ nÃ y cÃ¡c yÃªu cáº§u vá» transfer domain sáº½ bá»‹ tá»« chá»‘i. Náº¿u báº¡n khÃ´ng cháº¯c lÃ m tháº¿ nÃ o Ä‘á»ƒ lÃ m Ä‘iá»u nÃ y thÃ¬ hÃ£y liÃªn láº¡c vá»›i nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n.<br />\r\n<br />\r\n7. KhÃ´ng giao UserID/Password truy cáº­p tÃ i khoáº£n tÃªn miá»n cho ngÆ°á»i khÃ¡c.</p>'),
(10, 'Báº£o máº­t thÃ´ng tin tÃªn miá»n lÃ  gÃ¬ ?', '<p>Khi báº¡n Ä‘Äƒng kÃ½ má»™t tÃªn miá»n vá»›i nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n, cÃ¡c thÃ´ng tin tÃªn miá»n cá»§a báº¡n (cháº³ng háº¡n nhÆ° tÃªn, TÃªn cÃ´ng ty, Äá»‹a chá»‰, Sá»‘ Ä‘iá»‡n thoáº¡i, Äá»‹a chá»‰ Email).&nbsp; ThÃ´ng tin nÃ y trá»Ÿ nÃªn sáºµn cÃ³ cho báº¥t cá»© ai thá»±c hiá»‡n má»™t tra cá»©u tÃªn miá»n cá»§a báº¡n.<br />\r\n<br />\r\nBáº±ng cÃ¡ch sá»­ dá»¥ng cá»§a dá»‹ch vá»¥ báº£o vá»‡ thÃ´ng tin tÃªn miá»n, báº¡n cÃ³ thá»ƒ ngay láº­p tá»©c cháº¥m dá»©t viá»‡c láº¡m dá»¥ng.&nbsp; Khi báº¡n kÃ­ch hoáº¡t dá»‹ch vá»¥ nÃ y cho cÃ¡c tÃªn miá»n cá»§a báº¡n, nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n thay tháº¿ cÃ¡c chi tiáº¿t liÃªn há»‡ cá»§a báº¡n trong Whois báº±ng chi tiáº¿t liÃªn láº¡c khÃ¡c.</p>'),
(11, 'Báº£o máº­t thÃ´ng tin tÃªn miá»n lÃ  gÃ¬?', '<p>Khi báº¡n Ä‘Äƒng kÃ½ má»™t tÃªn miá»n vá»›i nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n, cÃ¡c thÃ´ng tin tÃªn miá»n cá»§a báº¡n (cháº³ng háº¡n nhÆ° tÃªn, TÃªn cÃ´ng ty, Äá»‹a chá»‰, Sá»‘ Ä‘iá»‡n thoáº¡i, Äá»‹a chá»‰ Email).&nbsp; ThÃ´ng tin nÃ y trá»Ÿ nÃªn sáºµn cÃ³ cho báº¥t cá»© ai thá»±c hiá»‡n má»™t tra cá»©u tÃªn miá»n cá»§a báº¡n.<br />\r\n<br />\r\nBáº±ng cÃ¡ch sá»­ dá»¥ng cá»§a dá»‹ch vá»¥ báº£o vá»‡ thÃ´ng tin tÃªn miá»n, báº¡n cÃ³ thá»ƒ ngay láº­p tá»©c cháº¥m dá»©t viá»‡c láº¡m dá»¥ng.&nbsp; Khi báº¡n kÃ­ch hoáº¡t dá»‹ch vá»¥ nÃ y cho cÃ¡c tÃªn miá»n cá»§a báº¡n, nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n thay tháº¿ cÃ¡c chi tiáº¿t liÃªn há»‡ cá»§a báº¡n trong Whois báº±ng chi tiáº¿t liÃªn láº¡c khÃ¡c.</p>'),
(12, 'Báº£o máº­t thÃ´ng tin tÃªn miá»n lÃ  gÃ¬?', '<p>Khi báº¡n Ä‘Äƒng kÃ½ má»™t tÃªn miá»n vá»›i nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n, cÃ¡c thÃ´ng tin tÃªn miá»n cá»§a báº¡n (cháº³ng háº¡n nhÆ° tÃªn, TÃªn cÃ´ng ty, Äá»‹a chá»‰, Sá»‘ Ä‘iá»‡n thoáº¡i, Äá»‹a chá»‰ Email).&nbsp; ThÃ´ng tin nÃ y trá»Ÿ nÃªn sáºµn cÃ³ cho báº¥t cá»© ai thá»±c hiá»‡n má»™t tra cá»©u tÃªn miá»n cá»§a báº¡n.<br />\r\n<br />\r\nBáº±ng cÃ¡ch sá»­ dá»¥ng cá»§a dá»‹ch vá»¥ báº£o vá»‡ thÃ´ng tin tÃªn miá»n, báº¡n cÃ³ thá»ƒ ngay láº­p tá»©c cháº¥m dá»©t viá»‡c láº¡m dá»¥ng.&nbsp; Khi báº¡n kÃ­ch hoáº¡t dá»‹ch vá»¥ nÃ y cho cÃ¡c tÃªn miá»n cá»§a báº¡n, nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n thay tháº¿ cÃ¡c chi tiáº¿t liÃªn há»‡ cá»§a báº¡n trong Whois báº±ng chi tiáº¿t liÃªn láº¡c khÃ¡c.</p>'),
(13, 'Báº£o máº­t thÃ´ng tin tÃªn miá»n lÃ  gÃ¬?', '<p>Khi báº¡n Ä‘Äƒng kÃ½ má»™t tÃªn miá»n vá»›i nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n, cÃ¡c thÃ´ng tin tÃªn miá»n cá»§a báº¡n (cháº³ng háº¡n nhÆ° tÃªn, TÃªn cÃ´ng ty, Äá»‹a chá»‰, Sá»‘ Ä‘iá»‡n thoáº¡i, Äá»‹a chá»‰ Email).&nbsp; ThÃ´ng tin nÃ y trá»Ÿ nÃªn sáºµn cÃ³ cho báº¥t cá»© ai thá»±c hiá»‡n má»™t tra cá»©u tÃªn miá»n cá»§a báº¡n.<br />\r\n<br />\r\nBáº±ng cÃ¡ch sá»­ dá»¥ng cá»§a dá»‹ch vá»¥ báº£o vá»‡ thÃ´ng tin tÃªn miá»n, báº¡n cÃ³ thá»ƒ ngay láº­p tá»©c cháº¥m dá»©t viá»‡c láº¡m dá»¥ng.&nbsp; Khi báº¡n kÃ­ch hoáº¡t dá»‹ch vá»¥ nÃ y cho cÃ¡c tÃªn miá»n cá»§a báº¡n, nhÃ  Ä‘Äƒng kÃ½ tÃªn miá»n thay tháº¿ cÃ¡c chi tiáº¿t liÃªn há»‡ cá»§a báº¡n trong Whois báº±ng chi tiáº¿t liÃªn láº¡c khÃ¡c.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `homeinfos`
--

CREATE TABLE `homeinfos` (
  `content` text NOT NULL,
  `img` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `homeinfos`
--

INSERT INTO `homeinfos` (`content`, `img`) VALUES
('<p>Mega Share Hosting lÃ  gÃ³i dá»‹ch vá»¥ lÆ°u trá»¯ web site chuyÃªn nghiá»‡p cÃ³ mÃ¡y chá»§ Ä‘áº·t táº¡i Viá»‡t Nam vÃ  Má»¹ vá»›i Ä‘Æ°á»ng truyá»n tá»‘c Ä‘á»™ cao. Shared hosting luÃ´n lÃ  giáº£i phÃ¡p phÃ¹ há»£p cho cÃ¡c cÃ¡ nhÃ¢n hoáº·c doanh nghiá»‡p muá»‘n cÃ³ má»™t website giá»›i thiá»‡u, giao dá»‹ch thÆ°Æ¡ng<br />\r\ntrÃªn Internet má»™t cÃ¡ch hiá»‡u quáº£ vÃ  tiáº¿t kiá»‡m chi phÃ­. MÃ¡y chá»§ share hosting cá»§a Mega Host xá»­ dá»¥ng bá»™ vi xá»­ lÃ½ máº¡nh máº½ cá»§a Intel vá»›i cÃ´ng nghá»‡ 4 nhÃ¢n (Intel Quad Core Xeon) vÃ  ram hÆ¡n 8GB chuyÃªn dá»¥ng cho mÃ¡y chá»§ (ECC RAM &ndash; FB-DIMM) nháº±m tÄƒng tá»‘c Ä‘á»™ vÃ  sá»± á»•n Ä‘á»‹nh hoáº¡t Ä‘á»™ng bá»n bá»‰ trong thá»i gian dÃ i.</p>\r\n<p><strong>CÃ¡c tÃ­nh nÄƒng cá»§a Share Hosting:</strong><br />\r\n- Há»— trá»£ nhiá»u ngÃ´n ngá»¯ láº­p trÃ¬nh vá»›i cÃ¡c phiÃªn báº£n khÃ¡c nhau.<br />\r\n- Miá»…n phÃ­ táº¡o, quáº£n lÃ½ há»™p thÆ° Ä‘iá»‡n tá»­ (Email) theo tÃªn miá»n riÃªng.<br />\r\n- Miá»…n phÃ­ táº¡o tÃªn miá»n con (Subdomain)<br />\r\n- Sá»­ dá»¥ng nhiá»u tÃªn miá»n cho 1 website<br />\r\n- Quáº£n lÃ½ nhiá»u website trÃªn cÃ¹ng 1 tÃ i khoáº£n hosting<br />\r\n- Quáº£n lÃ½ Ä‘Äƒng nháº­p, giÃ¡m sÃ¡t thÃ´ng sá»‘ bÄƒng thÃ´ng vÃ  dung lÆ°á»£ng<br />\r\n- Sao lÆ°u dá»± phÃ²ng vÃ  khÃ´i phá»¥c dá»¯ liá»‡u</p>\r\n<p>CÃ¡c mÃ£ nguá»“n má»Ÿ nhÆ° : Joomla , WordPress, Phpbb, Vbuletin, Drupal, Nuke Viá»‡t &hellip;. cháº¡y tá»‘t trÃªn há»‡ thá»‘ng Mega Host cá»§a chÃºng tÃ´i.</p>\r\n<p><strong>CÃ¡c dá»‹ch vá»¥ cá»™ng thÃªm :</strong><br />\r\nBandwidth: -Sá»­ dá»¥ng 5GB bandwidth tiáº¿p theo: 35.000 VNÄ/thÃ¡ng</p>', '931152_host1.png');

-- --------------------------------------------------------

--
-- Table structure for table `infos`
--

CREATE TABLE `infos` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `rank` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `infos`
--

INSERT INTO `infos` (`id`, `title`, `content`, `rank`) VALUES
(1, 'ThÃ´ng tin tham kháº£o sá»‘ 1', '<p>Ná»™i dung thÃ´ng tin tham kháº£o sá»‘ 1</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `name` varchar(255) NOT NULL,
  `icon` varchar(85) NOT NULL default '',
  `code` varchar(32) NOT NULL,
  `rank` tinyint(3) NOT NULL default '1',
  `active` tinyint(1) NOT NULL default '1',
  `isdefault` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`name`, `icon`, `code`, `rank`, `active`, `isdefault`) VALUES
('Viá»‡t Nam', 'vn.gif', 'vn', 3, 1, 1),
('English', 'en.gif', 'en', 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `intro` text NOT NULL,
  `content` text NOT NULL,
  `postdate` int(11) NOT NULL,
  `striking` tinyint(3) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `img`, `intro`, `content`, `postdate`, `striking`) VALUES
(4, 'TÃªn miá»n .vn cÃ³ xu hÆ°á»›ng tÄƒng máº¡nh', '549259_imsages.jpg', 'Theo bÃ¡o cÃ¡o tá»•ng káº¿t má»›i nháº¥t cá»§a Trung tÃ¢m Internet Viá»‡t Nam (VNNIC), nÄƒm 2009, sá»‘ lÆ°á»£ng tÃªn miá»n .vn cáº¥p má»›i Ä‘áº¡t 43.023 (nÄƒm 2008 lÃ  32.000), nÃ¢ng tá»•ng sá»‘ tÃªn miá»n .vn Ä‘ang hoáº¡t Ä‘á»™ng Ä‘áº¡t ngÆ°á»¡ng 100.000 (trong tá»•ng sá»‘ 120.000 tÃªn miá»n Ä‘Ã£ Ä‘Äƒng kÃ½), Ä‘áº¡t hÆ¡n 110% káº¿ hoáº¡ch Ä‘á» ra.', '<h3><a href="http://nhanhoa.com/?site=news&amp;view=topic&amp;id=2">TÃªn miá»n .vn cÃ³ xu hÆ°á»›ng tÄƒng máº¡nh</a></h3>\r\n<p><small>Thá»i gian gá»­i: 25/05/2010, 11:38 pm</small></p>\r\n<div>\r\n<p><strong><span style="font-family: arial; font-size: 13px;">TÃªn miá»n quá»‘c gia .vn Ä‘ang ngÃ y cÃ ng phá»• biáº¿n, vá»›i lÆ°á»£ng tÃªn miá»n cáº¥p má»›i trong nÄƒm 2009 tÄƒng tá»›i 34,4% so vá»›i nÄƒm trÆ°á»›c.</span></strong></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">Theo bÃ¡o cÃ¡o tá»•ng káº¿t má»›i nháº¥t cá»§a Trung tÃ¢m Internet Viá»‡t Nam (VNNIC), nÄƒm 2009, sá»‘ lÆ°á»£ng tÃªn miá»n .vn cáº¥p má»›i Ä‘áº¡t 43.023 (nÄƒm 2008 lÃ  32.000), nÃ¢ng tá»•ng sá»‘ tÃªn miá»n .vn Ä‘ang hoáº¡t Ä‘á»™ng Ä‘áº¡t ngÆ°á»¡ng 100.000 (trong tá»•ng sá»‘ 120.000 tÃªn miá»n Ä‘Ã£ Ä‘Äƒng kÃ½), Ä‘áº¡t hÆ¡n 110% káº¿ hoáº¡ch Ä‘á» ra. </span></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">NgoÃ i ra, sá»‘ lÆ°á»£ng cÃ¡c nhÃ  cung cáº¥p tÃªn miá»n cÅ©ng tÄƒng máº¡nh. Sá»‘ Ä‘Æ¡n vá»‹ phÃ¡t triá»ƒn má»›i lÃ  25 thÃ nh viÃªn, nÃ¢ng tá»•ng sá»‘ thÃ nh viÃªn cá»§a VNNIC lÃªn 64. </span></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">Ã”ng Nguyá»…n LÃª ThÃºy, GiÃ¡m Ä‘á»‘c VNNIC cho biáº¿t, theo káº¿ hoáº¡ch, dá»± tÃ­nh Ä‘áº¿n cuá»‘i nÄƒm 2010 thÃ¬ sá»‘ lÆ°á»£ng tÃªn miá»n .vn cá»§a Viá»‡t Nam má»›i Ä‘áº¡t 100.000 tÃªn miá»n, nhÆ°ng do tá»‘c Ä‘á»™ phÃ¡t triá»ƒn nhanh nÃªn má»¥c tiÃªu nÃ y Ä‘Ã£ vá» trÆ°á»›c káº¿ hoáº¡ch má»™t nÄƒm. </span></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">Tuy nhiÃªn, trong máº£ng tÃªn miá»n tiáº¿ng Viá»‡t, theo thá»‘ng kÃª cá»§a VNNIC, tÃªn miá»n tiáº¿ng Viá»‡t váº«n chÆ°a thu Ä‘Æ°á»£c sá»± quan tÃ¢m cá»§a ngÆ°á»i tiÃªu dÃ¹ng, vÃ¬ nÄƒm 2009, sá»‘ lÆ°á»£ng tÃªn miá»n dáº¡ng nÃ y Ä‘Æ°á»£c cáº¥p phÃ¡t chá»‰ Ä‘áº¡t 519, giáº£m Ä‘Ã¡ng ká»ƒ so vá»›i nÄƒm 2008 (nÄƒm 2008 lÃ  858 tÃªn miá»n). </span></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">Tá»•ng sá»‘ tÃªn miá»n tiáº¿ng Viá»‡t Ä‘ang hoáº¡t Ä‘á»™ng tÃ­nh Ä‘áº¿n cuá»‘i thÃ¡ng 12/2009 lÃ  3.340 tÃªn miá»n. Theo Thá»© trÆ°á»Ÿng Bá»™ ThÃ´ng tin vÃ  Truyá»n thÃ´ng LÃª Nam Tháº¯ng, hiá»‡n Viá»‡t Nam Ä‘ang lÃ  nÆ°á»›c sá»­ dá»¥ng tÃªn miá»n quá»‘c gia lá»›n thá»© hai cá»§a khu vá»±c ÄÃ´ng Nam Ã, chá»‰ Ä‘á»©ng sau Singapore. </span></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">Tuy nhiÃªn, náº¿u so vá»›i dÃ¢n sá»‘ khoáº£ng 86 triá»‡u ngÆ°á»i cá»§a Viá»‡t Nam thÃ¬ sá»‘ tÃªn miá»n .vn váº«n ráº¥t khiÃªm tá»‘n, trong khi Singapore dÃ¢n sá»‘ chá»‰ cÃ³ khoáº£ng 5 triá»‡u ngÆ°á»i nhÆ°ng sá»‘ tÃªn miá»n quá»‘c gia cá»§a Singapore Ä‘Ã£ Ä‘áº¡t má»©c khoáº£ng 110.000. </span></p>\r\n</div>', 1284484613, 1),
(5, 'TÃªn miá»n .vn cÃ³ xu hÆ°á»›ng tÄƒng máº¡nh', '776826_dds.jpg', '<p><span style="font-family: arial; font-size: 13px;">Theo bÃ¡o cÃ¡o tá»•ng káº¿t má»›i nháº¥t cá»§a Trung tÃ¢m Internet Viá»‡t Nam (VNNIC), nÄƒm 2009, sá»‘ lÆ°á»£ng tÃªn miá»n .vn cáº¥p má»›i Ä‘áº¡t 43.023 (nÄƒm 2008 lÃ  32.000), nÃ¢ng tá»•ng sá»‘ tÃªn miá»n .vn Ä‘ang hoáº¡t Ä‘á»™ng Ä‘áº¡t ngÆ°á»¡ng 100.000 (trong tá»•ng sá»‘ 120.000 tÃªn miá»n Ä‘Ã£ Ä‘Äƒng kÃ½), Ä‘áº¡t hÆ¡n 110% káº¿ hoáº¡ch Ä‘á» ra. </span></p>', '<p><strong><span style="font-family: arial; font-size: 13px;">TÃªn miá»n quá»‘c gia .vn Ä‘ang ngÃ y cÃ ng phá»• biáº¿n, vá»›i lÆ°á»£ng tÃªn miá»n cáº¥p má»›i trong nÄƒm 2009 tÄƒng tá»›i 34,4% so vá»›i nÄƒm trÆ°á»›c.</span></strong></p>\r\n<div>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">Theo bÃ¡o cÃ¡o tá»•ng káº¿t má»›i nháº¥t cá»§a Trung tÃ¢m Internet Viá»‡t Nam (VNNIC), nÄƒm 2009, sá»‘ lÆ°á»£ng tÃªn miá»n .vn cáº¥p má»›i Ä‘áº¡t 43.023 (nÄƒm 2008 lÃ  32.000), nÃ¢ng tá»•ng sá»‘ tÃªn miá»n .vn Ä‘ang hoáº¡t Ä‘á»™ng Ä‘áº¡t ngÆ°á»¡ng 100.000 (trong tá»•ng sá»‘ 120.000 tÃªn miá»n Ä‘Ã£ Ä‘Äƒng kÃ½), Ä‘áº¡t hÆ¡n 110% káº¿ hoáº¡ch Ä‘á» ra. </span></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">NgoÃ i ra, sá»‘ lÆ°á»£ng cÃ¡c nhÃ  cung cáº¥p tÃªn miá»n cÅ©ng tÄƒng máº¡nh. Sá»‘ Ä‘Æ¡n vá»‹ phÃ¡t triá»ƒn má»›i lÃ  25 thÃ nh viÃªn, nÃ¢ng tá»•ng sá»‘ thÃ nh viÃªn cá»§a VNNIC lÃªn 64. </span></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">Ã”ng Nguyá»…n LÃª ThÃºy, GiÃ¡m Ä‘á»‘c VNNIC cho biáº¿t, theo káº¿ hoáº¡ch, dá»± tÃ­nh Ä‘áº¿n cuá»‘i nÄƒm 2010 thÃ¬ sá»‘ lÆ°á»£ng tÃªn miá»n .vn cá»§a Viá»‡t Nam má»›i Ä‘áº¡t 100.000 tÃªn miá»n, nhÆ°ng do tá»‘c Ä‘á»™ phÃ¡t triá»ƒn nhanh nÃªn má»¥c tiÃªu nÃ y Ä‘Ã£ vá» trÆ°á»›c káº¿ hoáº¡ch má»™t nÄƒm. </span></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">Tuy nhiÃªn, trong máº£ng tÃªn miá»n tiáº¿ng Viá»‡t, theo thá»‘ng kÃª cá»§a VNNIC, tÃªn miá»n tiáº¿ng Viá»‡t váº«n chÆ°a thu Ä‘Æ°á»£c sá»± quan tÃ¢m cá»§a ngÆ°á»i tiÃªu dÃ¹ng, vÃ¬ nÄƒm 2009, sá»‘ lÆ°á»£ng tÃªn miá»n dáº¡ng nÃ y Ä‘Æ°á»£c cáº¥p phÃ¡t chá»‰ Ä‘áº¡t 519, giáº£m Ä‘Ã¡ng ká»ƒ so vá»›i nÄƒm 2008 (nÄƒm 2008 lÃ  858 tÃªn miá»n). </span></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">Tá»•ng sá»‘ tÃªn miá»n tiáº¿ng Viá»‡t Ä‘ang hoáº¡t Ä‘á»™ng tÃ­nh Ä‘áº¿n cuá»‘i thÃ¡ng 12/2009 lÃ  3.340 tÃªn miá»n. Theo Thá»© trÆ°á»Ÿng Bá»™ ThÃ´ng tin vÃ  Truyá»n thÃ´ng LÃª Nam Tháº¯ng, hiá»‡n Viá»‡t Nam Ä‘ang lÃ  nÆ°á»›c sá»­ dá»¥ng tÃªn miá»n quá»‘c gia lá»›n thá»© hai cá»§a khu vá»±c ÄÃ´ng Nam Ã, chá»‰ Ä‘á»©ng sau Singapore. </span></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">Tuy nhiÃªn, náº¿u so vá»›i dÃ¢n sá»‘ khoáº£ng 86 triá»‡u ngÆ°á»i cá»§a Viá»‡t Nam thÃ¬ sá»‘ tÃªn miá»n .vn váº«n ráº¥t khiÃªm tá»‘n, trong khi Singapore dÃ¢n sá»‘ chá»‰ cÃ³ khoáº£ng 5 triá»‡u ngÆ°á»i nhÆ°ng sá»‘ tÃªn miá»n quá»‘c gia cá»§a Singapore Ä‘Ã£ Ä‘áº¡t má»©c khoáº£ng 110.000. </span></p>\r\n</div>', 1284484650, 1),
(6, 'TÃªn miá»n .vn cÃ³ xu hÆ°á»›ng tÄƒng máº¡nh', '691286_imagssees.jpg', 'Theo bÃ¡o cÃ¡o tá»•ng káº¿t má»›i nháº¥t cá»§a Trung tÃ¢m Internet Viá»‡t Nam (VNNIC), nÄƒm 2009, sá»‘ lÆ°á»£ng tÃªn miá»n .vn cáº¥p má»›i Ä‘áº¡t 43.023 (nÄƒm 2008 lÃ  32.000), nÃ¢ng tá»•ng sá»‘ tÃªn miá»n .vn Ä‘ang hoáº¡t Ä‘á»™ng Ä‘áº¡t ngÆ°á»¡ng 100.000 (trong tá»•ng sá»‘ 120.000 tÃªn miá»n Ä‘Ã£ Ä‘Äƒng kÃ½), Ä‘áº¡t hÆ¡n 110% káº¿ hoáº¡ch Ä‘á» ra.', '<div>\r\n<p><strong><span style="font-family: arial; font-size: 13px;">TÃªn miá»n quá»‘c gia .vn Ä‘ang ngÃ y cÃ ng phá»• biáº¿n, vá»›i lÆ°á»£ng tÃªn miá»n cáº¥p má»›i trong nÄƒm 2009 tÄƒng tá»›i 34,4% so vá»›i nÄƒm trÆ°á»›c.</span></strong></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">Theo bÃ¡o cÃ¡o tá»•ng káº¿t má»›i nháº¥t cá»§a Trung tÃ¢m Internet Viá»‡t Nam (VNNIC), nÄƒm 2009, sá»‘ lÆ°á»£ng tÃªn miá»n .vn cáº¥p má»›i Ä‘áº¡t 43.023 (nÄƒm 2008 lÃ  32.000), nÃ¢ng tá»•ng sá»‘ tÃªn miá»n .vn Ä‘ang hoáº¡t Ä‘á»™ng Ä‘áº¡t ngÆ°á»¡ng 100.000 (trong tá»•ng sá»‘ 120.000 tÃªn miá»n Ä‘Ã£ Ä‘Äƒng kÃ½), Ä‘áº¡t hÆ¡n 110% káº¿ hoáº¡ch Ä‘á» ra. </span></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">NgoÃ i ra, sá»‘ lÆ°á»£ng cÃ¡c nhÃ  cung cáº¥p tÃªn miá»n cÅ©ng tÄƒng máº¡nh. Sá»‘ Ä‘Æ¡n vá»‹ phÃ¡t triá»ƒn má»›i lÃ  25 thÃ nh viÃªn, nÃ¢ng tá»•ng sá»‘ thÃ nh viÃªn cá»§a VNNIC lÃªn 64. </span></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">Ã”ng Nguyá»…n LÃª ThÃºy, GiÃ¡m Ä‘á»‘c VNNIC cho biáº¿t, theo káº¿ hoáº¡ch, dá»± tÃ­nh Ä‘áº¿n cuá»‘i nÄƒm 2010 thÃ¬ sá»‘ lÆ°á»£ng tÃªn miá»n .vn cá»§a Viá»‡t Nam má»›i Ä‘áº¡t 100.000 tÃªn miá»n, nhÆ°ng do tá»‘c Ä‘á»™ phÃ¡t triá»ƒn nhanh nÃªn má»¥c tiÃªu nÃ y Ä‘Ã£ vá» trÆ°á»›c káº¿ hoáº¡ch má»™t nÄƒm. </span></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">Tuy nhiÃªn, trong máº£ng tÃªn miá»n tiáº¿ng Viá»‡t, theo thá»‘ng kÃª cá»§a VNNIC, tÃªn miá»n tiáº¿ng Viá»‡t váº«n chÆ°a thu Ä‘Æ°á»£c sá»± quan tÃ¢m cá»§a ngÆ°á»i tiÃªu dÃ¹ng, vÃ¬ nÄƒm 2009, sá»‘ lÆ°á»£ng tÃªn miá»n dáº¡ng nÃ y Ä‘Æ°á»£c cáº¥p phÃ¡t chá»‰ Ä‘áº¡t 519, giáº£m Ä‘Ã¡ng ká»ƒ so vá»›i nÄƒm 2008 (nÄƒm 2008 lÃ  858 tÃªn miá»n). </span></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">Tá»•ng sá»‘ tÃªn miá»n tiáº¿ng Viá»‡t Ä‘ang hoáº¡t Ä‘á»™ng tÃ­nh Ä‘áº¿n cuá»‘i thÃ¡ng 12/2009 lÃ  3.340 tÃªn miá»n. Theo Thá»© trÆ°á»Ÿng Bá»™ ThÃ´ng tin vÃ  Truyá»n thÃ´ng LÃª Nam Tháº¯ng, hiá»‡n Viá»‡t Nam Ä‘ang lÃ  nÆ°á»›c sá»­ dá»¥ng tÃªn miá»n quá»‘c gia lá»›n thá»© hai cá»§a khu vá»±c ÄÃ´ng Nam Ã, chá»‰ Ä‘á»©ng sau Singapore. </span></p>\r\n<p align="justify"><span style="font-family: arial; font-size: 13px;">Tuy nhiÃªn, náº¿u so vá»›i dÃ¢n sá»‘ khoáº£ng 86 triá»‡u ngÆ°á»i cá»§a Viá»‡t Nam thÃ¬ sá»‘ tÃªn miá»n .vn váº«n ráº¥t khiÃªm tá»‘n, trong khi Singapore dÃ¢n sá»‘ chá»‰ cÃ³ khoáº£ng 5 triá»‡u ngÆ°á»i nhÆ°ng sá»‘ tÃªn miá»n quá»‘c gia cá»§a Singapore Ä‘Ã£ Ä‘áº¡t má»©c khoáº£ng 110.000. </span></p>\r\n</div>', 1284484677, 1),
(7, 'CÃ¡c quy Ä‘á»‹nh sá»­ dá»¥ng tÃªn miá»n Quá»‘c táº¿', '289221_dimages.jpg', 'Pháº¡t tiá»n tá»« 2.000.000 Ä‘á»“ng Ä‘áº¿n 5.000.000 Ä‘á»“ng Ä‘á»‘i vá»›i hÃ nh vi sá»­ dá»¥ng tÃªn miá»n cáº¥p cao khÃ¡c tÃªn miá»n ".vn" mÃ  khÃ´ng thÃ´ng bÃ¡o hoáº·c thÃ´ng bÃ¡o thÃ´ng tin khÃ´ng chÃ­nh xÃ¡c hoáº·c thay Ä‘á»•i thÃ´ng tin mÃ  khÃ´ng thÃ´ng bÃ¡oÂ  Bá»™ ThÃ´ng tin vÃ  Truyá»n thÃ´ng theo quy Ä‘á»‹nh; khai bÃ¡o thÃ´ng tin khÃ´ng chÃ­nh xÃ¡c hoáº·c khÃ´ng cáº­p nháº­t khi cÃ³ thay Ä‘á»•i thÃ´ng tin vá» tÃªn, Ä‘á»‹a chá»‰ liÃªn há»‡ Ä‘á»‘i vá»›i tá»• chá»©c hoáº·c tÃªn, Ä‘á»‹a chá»‰ liÃªn há»‡, sá»‘ giáº¥y chá»©ng minh nhÃ¢n dÃ¢n hoáº·c sá»‘ há»™ chiáº¿u Ä‘á»‘i vá»›i cÃ¡ nhÃ¢n Ä‘Äƒng kÃ½, sá»­ dá»¥ng tÃªn miá»n ".vn"', '<div>\r\n<p align="center"><strong>Má»™t sá»‘ quy Ä‘á»‹nh hiá»‡n hÃ nh cá»§a phÃ¡p luáº­t liÃªn quan tá»›i viá»‡c Ä‘Äƒng kÃ½, sá»­ dá»¥ng tÃªn miá»n quá»‘c táº¿ táº¡i Viá»‡t Nam</strong></p>\r\n<ol>\r\n <li>\r\n <p><span style="color: rgb(0, 0, 255);"><strong>I. Luáº­t cÃ´ng nghá»‡ ThÃ´ng tin cá»§a Quá»‘c há»™i nÆ°á»›c Cá»™ng hÃ²a xÃ£ há»™i chá»§ nghÄ©a Viá»‡t Nam sá»‘ 67/2006/QH11 ngÃ y 29 thÃ¡ng 6 nÄƒm 2006</strong></span></p>\r\n </li>\r\n</ol>\r\n<p>&nbsp;<strong>Äiá»u 23. Thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ </strong></p>\r\n<p>&nbsp;1. Tá»• chá»©c, cÃ¡ nhÃ¢n cÃ³ quyá»n thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ theo quy Ä‘á»‹nh cá»§a phÃ¡p luáº­t vÃ  chá»‹u trÃ¡ch nhiá»‡m quáº£n lÃ½ ná»™i dung vÃ  hoáº¡t Ä‘á»™ng trang thÃ´ng tin Ä‘iá»‡n tá»­ cá»§a mÃ¬nh.</p>\r\n<p>2. Tá»• chá»©c, cÃ¡ nhÃ¢n sá»­ dá»¥ng tÃªn miá»n quá»‘c gia Viá»‡t Nam &ldquo;.vn&rdquo; khi thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ khÃ´ng cáº§n thÃ´ng bÃ¡o vá»›i Bá»™ BÆ°u chÃ­nh, Viá»…n thÃ´ng. Tá»• chá»©c, cÃ¡ nhÃ¢n khi thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ khÃ´ng sá»­ dá»¥ng tÃªn miá»n quá»‘c gia Viá»‡t Nam &ldquo;.vn&rdquo; pháº£i thÃ´ng bÃ¡o trÃªn mÃ´i trÆ°á»ng máº¡ng táº¡i Ä‘á»‹a chá»‰: <a href="http://thongbaotenmien.vn/">http://thongbaotenmien.vn</a>&nbsp; vá»›i Bá»™ BÆ°u chÃ­nh, Viá»…n thÃ´ng nhá»¯ng thÃ´ng tin sau Ä‘Ã¢y:</p>\r\n<p>a) TÃªn tá»• chá»©c ghi trong quyáº¿t Ä‘á»‹nh thÃ nh láº­p, giáº¥y phÃ©p hoáº¡t Ä‘á»™ng, giáº¥y chá»©ng nháº­n Ä‘Äƒng kÃ½ kinh doanh hoáº·c giáº¥y phÃ©p má»Ÿ vÄƒn phÃ²ng Ä‘áº¡i diá»‡n; tÃªn cÃ¡ nhÃ¢n;</p>\r\n<p>b) Sá»‘, ngÃ y cáº¥p, nÆ¡i cáº¥p chá»©ng minh thÆ° nhÃ¢n dÃ¢n hoáº·c sá»‘, ngÃ y cáº¥p, nÆ¡i cáº¥p há»™ chiáº¿u cá»§a cÃ¡ nhÃ¢n;</p>\r\n<p>c) Äá»‹a chá»‰ trá»¥ sá»Ÿ chÃ­nh cá»§a tá»• chá»©c hoáº·c nÆ¡i thÆ°á»ng trÃº cá»§a cÃ¡ nhÃ¢n;</p>\r\n<p>d) Sá»‘ Ä‘iá»‡n thoáº¡i, sá»‘ fax, Ä‘á»‹a chá»‰ thÆ° Ä‘iá»‡n tá»­;</p>\r\n<p>Ä‘) CÃ¡c tÃªn miá»n Ä‘Ã£ Ä‘Äƒng kÃ½.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n<p>3. Tá»• chá»©c, cÃ¡ nhÃ¢n pháº£i chá»‹u trÃ¡ch nhiá»‡m trÆ°á»›c phÃ¡p luáº­t vá» tÃ­nh chÃ­nh xÃ¡c cá»§a cÃ¡c thÃ´ng tin quy Ä‘á»‹nh táº¡i khoáº£n 2 Äiá»u nÃ y, khi thay Ä‘á»•i thÃ´ng tin&nbsp; thÃ¬ pháº£i thÃ´ng bÃ¡o vá» sá»± thay Ä‘á»•i Ä‘Ã³.</p>\r\n<ol>\r\n <li>\r\n <p><span style="color: rgb(0, 0, 255);"><strong>II. NGHá»Š Äá»ŠNH Sá»‘: 28/2009/NÄ-CP -</strong><strong> Quy Ä‘á»‹nh xá»­ pháº¡t vi pháº¡m hÃ nh chÃ­nh trong quáº£n lÃ½, cung cáº¥p,</strong><strong> sá»­ dá»¥ng dá»‹ch vá»¥ Internet vÃ  thÃ´ng tin Ä‘iá»‡n tá»­ trÃªn Internet</strong></span></p>\r\n </li>\r\n</ol>\r\n<p><strong>Äiá»u 11. Vi pháº¡m cÃ¡c quy Ä‘á»‹nh vá» sá»­ dá»¥ng tÃªn miá»n Internet</strong></p>\r\n<p>1. Pháº¡t tiá»n tá»« 2.000.000 Ä‘á»“ng Ä‘áº¿n 5.000.000 Ä‘á»“ng Ä‘á»‘i vá»›i hÃ nh vi sá»­ dá»¥ng tÃªn miá»n cáº¥p cao khÃ¡c tÃªn miá»n &quot;.vn&quot; mÃ  khÃ´ng thÃ´ng bÃ¡o hoáº·c thÃ´ng bÃ¡o thÃ´ng tin khÃ´ng chÃ­nh xÃ¡c hoáº·c thay Ä‘á»•i thÃ´ng tin mÃ  khÃ´ng thÃ´ng bÃ¡o&nbsp; Bá»™ ThÃ´ng tin vÃ  Truyá»n thÃ´ng theo quy Ä‘á»‹nh; khai bÃ¡o thÃ´ng tin khÃ´ng chÃ­nh xÃ¡c hoáº·c khÃ´ng cáº­p nháº­t khi cÃ³ thay Ä‘á»•i thÃ´ng tin vá» tÃªn, Ä‘á»‹a chá»‰ liÃªn há»‡ Ä‘á»‘i vá»›i tá»• chá»©c hoáº·c tÃªn, Ä‘á»‹a chá»‰ liÃªn há»‡, sá»‘ giáº¥y chá»©ng minh nhÃ¢n dÃ¢n hoáº·c sá»‘ há»™ chiáº¿u Ä‘á»‘i vá»›i cÃ¡ nhÃ¢n Ä‘Äƒng kÃ½, sá»­ dá»¥ng tÃªn miá»n &quot;.vn&quot;.</p>\r\n</div>', 1284484747, 1),
(8, 'CÃ¡c quy Ä‘á»‹nh sá»­ dá»¥ng tÃªn miá»n Quá»‘c táº¿', '670596_imasges.jpg', 'Pháº¡t tiá»n tá»« 2.000.000 Ä‘á»“ng Ä‘áº¿n 5.000.000 Ä‘á»“ng Ä‘á»‘i vá»›i hÃ nh vi sá»­ dá»¥ng tÃªn miá»n cáº¥p cao khÃ¡c tÃªn miá»n ".vn" mÃ  khÃ´ng thÃ´ng bÃ¡o hoáº·c thÃ´ng bÃ¡o thÃ´ng tin khÃ´ng chÃ­nh xÃ¡c hoáº·c thay Ä‘á»•i thÃ´ng tin mÃ  khÃ´ng thÃ´ng bÃ¡oÂ  Bá»™ ThÃ´ng tin vÃ  Truyá»n thÃ´ng theo quy Ä‘á»‹nh; khai bÃ¡o thÃ´ng tin khÃ´ng chÃ­nh xÃ¡c hoáº·c khÃ´ng cáº­p nháº­t khi cÃ³ thay Ä‘á»•i thÃ´ng tin vá» tÃªn, Ä‘á»‹a chá»‰ liÃªn há»‡ Ä‘á»‘i vá»›i tá»• chá»©c hoáº·c tÃªn, Ä‘á»‹a chá»‰ liÃªn há»‡, sá»‘ giáº¥y chá»©ng minh nhÃ¢n dÃ¢n hoáº·c sá»‘ há»™ chiáº¿u Ä‘á»‘i vá»›i cÃ¡ nhÃ¢n Ä‘Äƒng kÃ½, sá»­ dá»¥ng tÃªn miá»n ".vn".', '<div>\r\n<p align="center"><strong>Má»™t sá»‘ quy Ä‘á»‹nh hiá»‡n hÃ nh cá»§a phÃ¡p luáº­t liÃªn quan tá»›i viá»‡c Ä‘Äƒng kÃ½, sá»­ dá»¥ng tÃªn miá»n quá»‘c táº¿ táº¡i Viá»‡t Nam</strong></p>\r\n<ol>\r\n <li>\r\n <p><span style="color: rgb(0, 0, 255);"><strong>I. Luáº­t cÃ´ng nghá»‡ ThÃ´ng tin cá»§a Quá»‘c há»™i nÆ°á»›c Cá»™ng hÃ²a xÃ£ há»™i chá»§ nghÄ©a Viá»‡t Nam sá»‘ 67/2006/QH11 ngÃ y 29 thÃ¡ng 6 nÄƒm 2006</strong></span></p>\r\n </li>\r\n</ol>\r\n<p>&nbsp;<strong>Äiá»u 23. Thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ </strong></p>\r\n<p>&nbsp;1. Tá»• chá»©c, cÃ¡ nhÃ¢n cÃ³ quyá»n thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ theo quy Ä‘á»‹nh cá»§a phÃ¡p luáº­t vÃ  chá»‹u trÃ¡ch nhiá»‡m quáº£n lÃ½ ná»™i dung vÃ  hoáº¡t Ä‘á»™ng trang thÃ´ng tin Ä‘iá»‡n tá»­ cá»§a mÃ¬nh.</p>\r\n<p>2. Tá»• chá»©c, cÃ¡ nhÃ¢n sá»­ dá»¥ng tÃªn miá»n quá»‘c gia Viá»‡t Nam &ldquo;.vn&rdquo; khi thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ khÃ´ng cáº§n thÃ´ng bÃ¡o vá»›i Bá»™ BÆ°u chÃ­nh, Viá»…n thÃ´ng. Tá»• chá»©c, cÃ¡ nhÃ¢n khi thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ khÃ´ng sá»­ dá»¥ng tÃªn miá»n quá»‘c gia Viá»‡t Nam &ldquo;.vn&rdquo; pháº£i thÃ´ng bÃ¡o trÃªn mÃ´i trÆ°á»ng máº¡ng táº¡i Ä‘á»‹a chá»‰: <a href="http://thongbaotenmien.vn/">http://thongbaotenmien.vn</a>&nbsp; vá»›i Bá»™ BÆ°u chÃ­nh, Viá»…n thÃ´ng nhá»¯ng thÃ´ng tin sau Ä‘Ã¢y:</p>\r\n<p>a) TÃªn tá»• chá»©c ghi trong quyáº¿t Ä‘á»‹nh thÃ nh láº­p, giáº¥y phÃ©p hoáº¡t Ä‘á»™ng, giáº¥y chá»©ng nháº­n Ä‘Äƒng kÃ½ kinh doanh hoáº·c giáº¥y phÃ©p má»Ÿ vÄƒn phÃ²ng Ä‘áº¡i diá»‡n; tÃªn cÃ¡ nhÃ¢n;</p>\r\n<p>b) Sá»‘, ngÃ y cáº¥p, nÆ¡i cáº¥p chá»©ng minh thÆ° nhÃ¢n dÃ¢n hoáº·c sá»‘, ngÃ y cáº¥p, nÆ¡i cáº¥p há»™ chiáº¿u cá»§a cÃ¡ nhÃ¢n;</p>\r\n<p>c) Äá»‹a chá»‰ trá»¥ sá»Ÿ chÃ­nh cá»§a tá»• chá»©c hoáº·c nÆ¡i thÆ°á»ng trÃº cá»§a cÃ¡ nhÃ¢n;</p>\r\n<p>d) Sá»‘ Ä‘iá»‡n thoáº¡i, sá»‘ fax, Ä‘á»‹a chá»‰ thÆ° Ä‘iá»‡n tá»­;</p>\r\n<p>Ä‘) CÃ¡c tÃªn miá»n Ä‘Ã£ Ä‘Äƒng kÃ½.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n<p>3. Tá»• chá»©c, cÃ¡ nhÃ¢n pháº£i chá»‹u trÃ¡ch nhiá»‡m trÆ°á»›c phÃ¡p luáº­t vá» tÃ­nh chÃ­nh xÃ¡c cá»§a cÃ¡c thÃ´ng tin quy Ä‘á»‹nh táº¡i khoáº£n 2 Äiá»u nÃ y, khi thay Ä‘á»•i thÃ´ng tin&nbsp; thÃ¬ pháº£i thÃ´ng bÃ¡o vá» sá»± thay Ä‘á»•i Ä‘Ã³.</p>\r\n<ol>\r\n <li>\r\n <p><span style="color: rgb(0, 0, 255);"><strong>II. NGHá»Š Äá»ŠNH Sá»‘: 28/2009/NÄ-CP -</strong><strong> Quy Ä‘á»‹nh xá»­ pháº¡t vi pháº¡m hÃ nh chÃ­nh trong quáº£n lÃ½, cung cáº¥p,</strong><strong> sá»­ dá»¥ng dá»‹ch vá»¥ Internet vÃ  thÃ´ng tin Ä‘iá»‡n tá»­ trÃªn Internet</strong></span></p>\r\n </li>\r\n</ol>\r\n<p><strong>Äiá»u 11. Vi pháº¡m cÃ¡c quy Ä‘á»‹nh vá» sá»­ dá»¥ng tÃªn miá»n Internet</strong></p>\r\n<p>1. Pháº¡t tiá»n tá»« 2.000.000 Ä‘á»“ng Ä‘áº¿n 5.000.000 Ä‘á»“ng Ä‘á»‘i vá»›i hÃ nh vi sá»­ dá»¥ng tÃªn miá»n cáº¥p cao khÃ¡c tÃªn miá»n &quot;.vn&quot; mÃ  khÃ´ng thÃ´ng bÃ¡o hoáº·c thÃ´ng bÃ¡o thÃ´ng tin khÃ´ng chÃ­nh xÃ¡c hoáº·c thay Ä‘á»•i thÃ´ng tin mÃ  khÃ´ng thÃ´ng bÃ¡o&nbsp; Bá»™ ThÃ´ng tin vÃ  Truyá»n thÃ´ng theo quy Ä‘á»‹nh; khai bÃ¡o thÃ´ng tin khÃ´ng chÃ­nh xÃ¡c hoáº·c khÃ´ng cáº­p nháº­t khi cÃ³ thay Ä‘á»•i thÃ´ng tin vá» tÃªn, Ä‘á»‹a chá»‰ liÃªn há»‡ Ä‘á»‘i vá»›i tá»• chá»©c hoáº·c tÃªn, Ä‘á»‹a chá»‰ liÃªn há»‡, sá»‘ giáº¥y chá»©ng minh nhÃ¢n dÃ¢n hoáº·c sá»‘ há»™ chiáº¿u Ä‘á»‘i vá»›i cÃ¡ nhÃ¢n Ä‘Äƒng kÃ½, sá»­ dá»¥ng tÃªn miá»n &quot;.vn&quot;.</p>\r\n</div>', 1284484989, 1),
(9, 'CÃ¡c quy Ä‘á»‹nh sá»­ dá»¥ng tÃªn miá»n Quá»‘c táº¿ táº¡i Viá»‡t Nam', '877656_iwndex.jpg', 'Tá»• chá»©c, cÃ¡ nhÃ¢n cÃ³ quyá»n thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ theo quy Ä‘á»‹nh cá»§a phÃ¡p luáº­t vÃ  chá»‹u trÃ¡ch nhiá»‡m quáº£n lÃ½ ná»™i dung vÃ  hoáº¡t Ä‘á»™ng trang thÃ´ng tin Ä‘iá»‡n tá»­ cá»§a mÃ¬nh.Tá»• chá»©c, cÃ¡ nhÃ¢n sá»­ dá»¥ng tÃªn miá»n quá»‘c gia Viá»‡t Nam â€œ.vnâ€ khi thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ khÃ´ng cáº§n thÃ´ng bÃ¡o vá»›i Bá»™ BÆ°u chÃ­nh, Viá»…n thÃ´ng.', '<div>\r\n<p align="center"><strong>Má»™t sá»‘ quy Ä‘á»‹nh hiá»‡n hÃ nh cá»§a phÃ¡p luáº­t liÃªn quan tá»›i viá»‡c Ä‘Äƒng kÃ½, sá»­ dá»¥ng tÃªn miá»n quá»‘c táº¿ táº¡i Viá»‡t Nam</strong></p>\r\n<ol>\r\n <li>\r\n <p><span style="color: rgb(0, 0, 255);"><strong>I. Luáº­t cÃ´ng nghá»‡ ThÃ´ng tin cá»§a Quá»‘c há»™i nÆ°á»›c Cá»™ng hÃ²a xÃ£ há»™i chá»§ nghÄ©a Viá»‡t Nam sá»‘ 67/2006/QH11 ngÃ y 29 thÃ¡ng 6 nÄƒm 2006</strong></span></p>\r\n </li>\r\n</ol>\r\n<p>&nbsp;<strong>Äiá»u 23. Thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ </strong></p>\r\n<p>&nbsp;1. Tá»• chá»©c, cÃ¡ nhÃ¢n cÃ³ quyá»n thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ theo quy Ä‘á»‹nh cá»§a phÃ¡p luáº­t vÃ  chá»‹u trÃ¡ch nhiá»‡m quáº£n lÃ½ ná»™i dung vÃ  hoáº¡t Ä‘á»™ng trang thÃ´ng tin Ä‘iá»‡n tá»­ cá»§a mÃ¬nh.</p>\r\n<p>2. Tá»• chá»©c, cÃ¡ nhÃ¢n sá»­ dá»¥ng tÃªn miá»n quá»‘c gia Viá»‡t Nam &ldquo;.vn&rdquo; khi thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ khÃ´ng cáº§n thÃ´ng bÃ¡o vá»›i Bá»™ BÆ°u chÃ­nh, Viá»…n thÃ´ng. Tá»• chá»©c, cÃ¡ nhÃ¢n khi thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ khÃ´ng sá»­ dá»¥ng tÃªn miá»n quá»‘c gia Viá»‡t Nam &ldquo;.vn&rdquo; pháº£i thÃ´ng bÃ¡o trÃªn mÃ´i trÆ°á»ng máº¡ng táº¡i Ä‘á»‹a chá»‰: <a href="http://thongbaotenmien.vn/">http://thongbaotenmien.vn</a>&nbsp; vá»›i Bá»™ BÆ°u chÃ­nh, Viá»…n thÃ´ng nhá»¯ng thÃ´ng tin sau Ä‘Ã¢y:</p>\r\n<p>a) TÃªn tá»• chá»©c ghi trong quyáº¿t Ä‘á»‹nh thÃ nh láº­p, giáº¥y phÃ©p hoáº¡t Ä‘á»™ng, giáº¥y chá»©ng nháº­n Ä‘Äƒng kÃ½ kinh doanh hoáº·c giáº¥y phÃ©p má»Ÿ vÄƒn phÃ²ng Ä‘áº¡i diá»‡n; tÃªn cÃ¡ nhÃ¢n;</p>\r\n<p>b) Sá»‘, ngÃ y cáº¥p, nÆ¡i cáº¥p chá»©ng minh thÆ° nhÃ¢n dÃ¢n hoáº·c sá»‘, ngÃ y cáº¥p, nÆ¡i cáº¥p há»™ chiáº¿u cá»§a cÃ¡ nhÃ¢n;</p>\r\n<p>c) Äá»‹a chá»‰ trá»¥ sá»Ÿ chÃ­nh cá»§a tá»• chá»©c hoáº·c nÆ¡i thÆ°á»ng trÃº cá»§a cÃ¡ nhÃ¢n;</p>\r\n<p>d) Sá»‘ Ä‘iá»‡n thoáº¡i, sá»‘ fax, Ä‘á»‹a chá»‰ thÆ° Ä‘iá»‡n tá»­;</p>\r\n<p>Ä‘) CÃ¡c tÃªn miá»n Ä‘Ã£ Ä‘Äƒng kÃ½.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n<p>3. Tá»• chá»©c, cÃ¡ nhÃ¢n pháº£i chá»‹u trÃ¡ch nhiá»‡m trÆ°á»›c phÃ¡p luáº­t vá» tÃ­nh chÃ­nh xÃ¡c cá»§a cÃ¡c thÃ´ng tin quy Ä‘á»‹nh táº¡i khoáº£n 2 Äiá»u nÃ y, khi thay Ä‘á»•i thÃ´ng tin&nbsp; thÃ¬ pháº£i thÃ´ng bÃ¡o vá» sá»± thay Ä‘á»•i Ä‘Ã³.</p>\r\n<ol>\r\n <li>\r\n <p><span style="color: rgb(0, 0, 255);"><strong>II. NGHá»Š Äá»ŠNH Sá»‘: 28/2009/NÄ-CP -</strong><strong> Quy Ä‘á»‹nh xá»­ pháº¡t vi pháº¡m hÃ nh chÃ­nh trong quáº£n lÃ½, cung cáº¥p,</strong><strong> sá»­ dá»¥ng dá»‹ch vá»¥ Internet vÃ  thÃ´ng tin Ä‘iá»‡n tá»­ trÃªn Internet</strong></span></p>\r\n </li>\r\n</ol>\r\n<p><strong>Äiá»u 11. Vi pháº¡m cÃ¡c quy Ä‘á»‹nh vá» sá»­ dá»¥ng tÃªn miá»n Internet</strong></p>\r\n<p>1. Pháº¡t tiá»n tá»« 2.000.000 Ä‘á»“ng Ä‘áº¿n 5.000.000 Ä‘á»“ng Ä‘á»‘i vá»›i hÃ nh vi sá»­ dá»¥ng tÃªn miá»n cáº¥p cao khÃ¡c tÃªn miá»n &quot;.vn&quot; mÃ  khÃ´ng thÃ´ng bÃ¡o hoáº·c thÃ´ng bÃ¡o thÃ´ng tin khÃ´ng chÃ­nh xÃ¡c hoáº·c thay Ä‘á»•i thÃ´ng tin mÃ  khÃ´ng thÃ´ng bÃ¡o&nbsp; Bá»™ ThÃ´ng tin vÃ  Truyá»n thÃ´ng theo quy Ä‘á»‹nh; khai bÃ¡o thÃ´ng tin khÃ´ng chÃ­nh xÃ¡c hoáº·c khÃ´ng cáº­p nháº­t khi cÃ³ thay Ä‘á»•i thÃ´ng tin vá» tÃªn, Ä‘á»‹a chá»‰ liÃªn há»‡ Ä‘á»‘i vá»›i tá»• chá»©c hoáº·c tÃªn, Ä‘á»‹a chá»‰ liÃªn há»‡, sá»‘ giáº¥y chá»©ng minh nhÃ¢n dÃ¢n hoáº·c sá»‘ há»™ chiáº¿u Ä‘á»‘i vá»›i cÃ¡ nhÃ¢n Ä‘Äƒng kÃ½, sá»­ dá»¥ng tÃªn miá»n &quot;.vn&quot;.</p>\r\n</div>', 1284485021, 0),
(10, 'Tá»•ng quan vá» RAID-Giáº£i phÃ¡p phÃ²ng há»™ cho Server', '56771_aa.jpg', 'ÄÃ¢y lÃ  dáº¡ng RAID Ä‘ang Ä‘Æ°á»£c ngÆ°á»i dÃ¹ng Æ°a thÃ­ch do kháº£ nÄƒng nÃ¢ng cao hiá»‡u suáº¥t trao Ä‘á»•i dá»¯ liá»‡u cá»§a Ä‘Ä©a cá»©ng. ÄÃ²i há»i tá»‘i thiá»ƒu hai Ä‘Ä©a cá»©ng, RAID 0 cho phÃ©p mÃ¡y tÃ­nh ghi dá»¯ liá»‡u lÃªn chÃºng theo má»™t phÆ°Æ¡ng thá»©c Ä‘áº·c biá»‡t Ä‘Æ°á»£c gá»i lÃ  Striping. VÃ­ dá»¥ báº¡n cÃ³ 8 Ä‘oáº¡n dá»¯ liá»‡u Ä‘Æ°á»£c Ä‘Ã¡nh sá»‘ tá»« 1 Ä‘áº¿n 8, cÃ¡c Ä‘oáº¡n Ä‘Ã¡nh sá»‘ láº» (1,3,5,7) sáº½ Ä‘Æ°á»£c ghi lÃªn Ä‘Ä©a cá»©ng Ä‘áº§u tiÃªn vÃ  cÃ¡c Ä‘oáº¡n Ä‘Ã¡nh sá»‘ cháºµn (2,4,6,8) sáº½ Ä‘Æ°á»£c ghi lÃªn Ä‘Ä©a thá»© hai. Äá»ƒ Ä‘Æ¡n giáº£n hÆ¡n, báº¡n cÃ³ thá»ƒ hÃ¬nh dung mÃ¬nh cÃ³ 100MB dá»¯ liá»‡u vÃ  thay vÃ¬ dá»“n 100MB vÃ o má»™t Ä‘Ä©a cá»©ng duy nháº¥t', '<p>Trong nhá»¯ng nÄƒm gáº§n Ä‘Ã¢y viá»‡c Ä‘áº§u tÆ° raid cho mÃ¡y tÃ­nh Ä‘á»ƒ bÃ n hoáº·c cho nhá»¯ng há»‡ thá»‘ng mÃ¡y tÃ­nh lá»›n lÃ  Ä‘iá»u cáº§n thiáº¿t. Trong vÃ i nÄƒm trÆ°á»›c nghe nÃ³i raid cÃ³ thá»ƒ gá»i lÃ  má»™t thá»© gÃ¬ Ä‘Ã³ xa xá»‰ nhÆ°ng so vá»›i thá»‹ trÆ°á»ng thiáº¿t bá»‹ mÃ¡y tÃ­nh ngÃ y nay vÃ  vá»›i viá»‡c bá»‹ máº¥t dá»¯ liá»‡u, hÆ° á»• cá»©ng thÃ¬ Ä‘iá»u má»i ngÆ°á»i nghÄ© Ä‘áº¿n Ä‘áº§u tiÃªn lÃ  sá»­ dá»¥ng raid cho há»‡ thá»‘ng cá»§a mÃ¬nh. Äa sá»‘ cÃ¡c mainboard ngÃ y nay Ä‘á»u cÃ³ cÃ´ng nghá»‡ RAID hoáº·c cÃ³ há»— trá»£ slot cáº¯m card RAID Controller nhÆ°ng khÃ´ng pháº£i ai cÅ©ng biáº¿t cÃ¡ch sá»­ dá»¥ng hiá»‡u quáº£. BÃ i viáº¿t tÃ´i chá»‰ giá»›i thiá»‡u thÃ´ng tin cÆ¡ báº£n vá» RAID. Vá» cÃ¡ch cáº¥u hÃ¬nh , create array... raid cá»©ng cÅ©ng nhÆ° raid má»m thÃ¬ cÃ¡c báº¡n search trong diá»…n Ä‘Ã n hoáº·c google hoáº·c Ä‘á»c thÃªm document cá»§a mÃ¡y.<br />\r\n<br />\r\nA. Váº­y RAID LÃ€ GÃŒ ?<br />\r\n<br />\r\nRAID lÃ  chá»¯ viáº¿t táº¯t cá»§a Redundant Array of Independent Disks. Ban Ä‘áº§u, RAID Ä‘Æ°á»£c sá»­ dá»¥ng nhÆ° má»™t giáº£i phÃ¡p phÃ²ng há»™ vÃ¬ nÃ³ cho phÃ©p ghi dá»¯ liá»‡u lÃªn nhiá»u Ä‘Ä©a cá»©ng cÃ¹ng lÃºc. Vá» sau, RAID Ä‘Ã£ cÃ³ nhiá»u biáº¿n thá»ƒ cho phÃ©p khÃ´ng chá»‰ Ä‘áº£m báº£o an toÃ n dá»¯ liá»‡u mÃ  cÃ²n giÃºp gia tÄƒng Ä‘Ã¡ng ká»ƒ tá»‘c Ä‘á»™ truy xuáº¥t dá»¯ liá»‡u tá»« Ä‘Ä©a cá»©ng. DÆ°á»›i Ä‘Ã¢y lÃ  nÄƒm loáº¡i RAID Ä‘Æ°á»£c dÃ¹ng phá»• biáº¿n: <br />\r\n<br />\r\n1. RAID 0 <br />\r\n<br />\r\nÄÃ¢y lÃ  dáº¡ng RAID Ä‘ang Ä‘Æ°á»£c ngÆ°á»i dÃ¹ng Æ°a thÃ­ch do kháº£ nÄƒng nÃ¢ng cao hiá»‡u suáº¥t trao Ä‘á»•i dá»¯ liá»‡u cá»§a Ä‘Ä©a cá»©ng. ÄÃ²i há»i tá»‘i thiá»ƒu hai Ä‘Ä©a cá»©ng, RAID 0 cho phÃ©p mÃ¡y tÃ­nh ghi dá»¯ liá»‡u lÃªn chÃºng theo má»™t phÆ°Æ¡ng thá»©c Ä‘áº·c biá»‡t Ä‘Æ°á»£c gá»i lÃ  Striping. VÃ­ dá»¥ báº¡n cÃ³ 8 Ä‘oáº¡n dá»¯ liá»‡u Ä‘Æ°á»£c Ä‘Ã¡nh sá»‘ tá»« 1 Ä‘áº¿n 8, cÃ¡c Ä‘oáº¡n Ä‘Ã¡nh sá»‘ láº» (1,3,5,7) sáº½ Ä‘Æ°á»£c ghi lÃªn Ä‘Ä©a cá»©ng Ä‘áº§u tiÃªn vÃ  cÃ¡c Ä‘oáº¡n Ä‘Ã¡nh sá»‘ cháºµn (2,4,6,8) sáº½ Ä‘Æ°á»£c ghi lÃªn Ä‘Ä©a thá»© hai. Äá»ƒ Ä‘Æ¡n giáº£n hÆ¡n, báº¡n cÃ³ thá»ƒ hÃ¬nh dung mÃ¬nh cÃ³ 100MB dá»¯ liá»‡u vÃ  thay vÃ¬ dá»“n 100MB vÃ o má»™t Ä‘Ä©a cá»©ng duy nháº¥t, RAID 0 sáº½ giÃºp dá»“n 50MB vÃ o má»—i Ä‘Ä©a cá»©ng riÃªng giÃºp giáº£m má»™t ná»­a thá»i gian lÃ m viá»‡c theo lÃ½ thuyáº¿t. Tá»« Ä‘Ã³ báº¡n cÃ³ thá»ƒ dá»… dÃ ng suy ra náº¿u cÃ³ 4, 8 hay nhiá»u Ä‘Ä©a cá»©ng hÆ¡n ná»¯a thÃ¬ tá»‘c Ä‘á»™ sáº½ cÃ ng cao hÆ¡n. Tuy nghe cÃ³ váº» háº¥p dáº«n nhÆ°ng trÃªn thá»±c táº¿, RAID 0 váº«n áº©n chá»©a nguy cÆ¡ máº¥t dá»¯ liá»‡u. NguyÃªn nhÃ¢n chÃ­nh láº¡i náº±m á»Ÿ cÃ¡ch ghi thÃ´ng tin xÃ© láº» vÃ¬ nhÆ° váº­y dá»¯ liá»‡u khÃ´ng náº±m hoÃ n toÃ n á»Ÿ má»™t Ä‘Ä©a cá»©ng nÃ o vÃ  má»—i khi cáº§n truy xuáº¥t thÃ´ng tin (vÃ­ dá»¥ má»™t file nÃ o Ä‘Ã³), mÃ¡y tÃ­nh sáº½ pháº£i tá»•ng há»£p tá»« cÃ¡c Ä‘Ä©a cá»©ng. Náº¿u má»™t Ä‘Ä©a cá»©ng gáº·p trá»¥c tráº·c thÃ¬ thÃ´ng tin (file) Ä‘Ã³ coi nhÆ° khÃ´ng thá»ƒ Ä‘á»c Ä‘Æ°á»£c vÃ  máº¥t luÃ´n. Tháº­t may máº¯n lÃ  vá»›i cÃ´ng nghá»‡ hiá»‡n Ä‘áº¡i, sáº£n pháº©m pháº§n cá»©ng khÃ¡ bá»n nÃªn nhá»¯ng trÆ°á»ng há»£p máº¥t dá»¯ liá»‡u nhÆ° váº­y xáº£y ra khÃ´ng nhiá»u. CÃ³ thá»ƒ tháº¥y RAID 0 thá»±c sá»± thÃ­ch há»£p cho nhá»¯ng ngÆ°á»i dÃ¹ng cáº§n truy cáº­p nhanh khá»‘i lÆ°á»£ng dá»¯ liá»‡u lá»›n, vÃ­ dá»¥ cÃ¡c game thá»§ hoáº·c nhá»¯ng ngÆ°á»i chuyÃªn lÃ m Ä‘á»“ hoáº¡, video sá»‘.</p>', 1284485075, 0),
(11, 'PhÆ°Æ¡ng phÃ¡p báº£o vá»‡ server', '948059_imsages.jpg', 'ÄÃ¢y lÃ  dáº¡ng RAID Ä‘ang Ä‘Æ°á»£c ngÆ°á»i dÃ¹ng Æ°a thÃ­ch do kháº£ nÄƒng nÃ¢ng cao hiá»‡u suáº¥t trao Ä‘á»•i dá»¯ liá»‡u cá»§a Ä‘Ä©a cá»©ng. ÄÃ²i há»i tá»‘i thiá»ƒu hai Ä‘Ä©a cá»©ng, RAID 0 cho phÃ©p mÃ¡y tÃ­nh ghi dá»¯ liá»‡u lÃªn chÃºng theo má»™t phÆ°Æ¡ng thá»©c Ä‘áº·c biá»‡t Ä‘Æ°á»£c gá»i lÃ  Striping. VÃ­ dá»¥ báº¡n cÃ³ 8 Ä‘oáº¡n dá»¯ liá»‡u Ä‘Æ°á»£c Ä‘Ã¡nh sá»‘ tá»« 1 Ä‘áº¿n 8, cÃ¡c Ä‘oáº¡n Ä‘Ã¡nh sá»‘ láº» (1,3,5,7) sáº½ Ä‘Æ°á»£c ghi lÃªn Ä‘Ä©a cá»©ng Ä‘áº§u tiÃªn vÃ  cÃ¡c Ä‘oáº¡n Ä‘Ã¡nh sá»‘ cháºµn (2,4,6,8) sáº½ Ä‘Æ°á»£c ghi lÃªn Ä‘Ä©a thá»© hai. Äá»ƒ Ä‘Æ¡n giáº£n hÆ¡n, báº¡n cÃ³ thá»ƒ hÃ¬nh dung mÃ¬nh cÃ³ 100MB dá»¯ liá»‡u vÃ  thay vÃ¬ dá»“n 100MB vÃ o má»™t Ä‘Ä©a cá»©ng duy nháº¥t', '<p>ÄÃ¢y lÃ  dáº¡ng RAID Ä‘ang Ä‘Æ°á»£c ngÆ°á»i dÃ¹ng Æ°a thÃ­ch do kháº£ nÄƒng nÃ¢ng cao hiá»‡u suáº¥t trao Ä‘á»•i dá»¯ liá»‡u cá»§a Ä‘Ä©a cá»©ng. ÄÃ²i há»i tá»‘i thiá»ƒu hai Ä‘Ä©a cá»©ng, RAID 0 cho phÃ©p mÃ¡y tÃ­nh ghi dá»¯ liá»‡u lÃªn chÃºng theo má»™t phÆ°Æ¡ng thá»©c Ä‘áº·c biá»‡t Ä‘Æ°á»£c gá»i lÃ  Striping. VÃ­ dá»¥ báº¡n cÃ³ 8 Ä‘oáº¡n dá»¯ liá»‡u Ä‘Æ°á»£c Ä‘Ã¡nh sá»‘ tá»« 1 Ä‘áº¿n 8, cÃ¡c Ä‘oáº¡n Ä‘Ã¡nh sá»‘ láº» (1,3,5,7) sáº½ Ä‘Æ°á»£c ghi lÃªn Ä‘Ä©a cá»©ng Ä‘áº§u tiÃªn vÃ  cÃ¡c Ä‘oáº¡n Ä‘Ã¡nh sá»‘ cháºµn (2,4,6,8) sáº½ Ä‘Æ°á»£c ghi lÃªn Ä‘Ä©a thá»© hai. Äá»ƒ Ä‘Æ¡n giáº£n hÆ¡n, báº¡n cÃ³ thá»ƒ hÃ¬nh dung mÃ¬nh cÃ³ 100MB dá»¯ liá»‡u vÃ  thay vÃ¬ dá»“n 100MB vÃ o má»™t Ä‘Ä©a cá»©ng duy nháº¥t</p>\r\n<p>ÄÃ¢y lÃ  dáº¡ng RAID Ä‘ang Ä‘Æ°á»£c ngÆ°á»i dÃ¹ng Æ°a thÃ­ch do kháº£ nÄƒng nÃ¢ng cao hiá»‡u suáº¥t trao Ä‘á»•i dá»¯ liá»‡u cá»§a Ä‘Ä©a cá»©ng. ÄÃ²i há»i tá»‘i thiá»ƒu hai Ä‘Ä©a cá»©ng, RAID 0 cho phÃ©p mÃ¡y tÃ­nh ghi dá»¯ liá»‡u lÃªn chÃºng theo má»™t phÆ°Æ¡ng thá»©c Ä‘áº·c biá»‡t Ä‘Æ°á»£c gá»i lÃ  Striping. VÃ­ dá»¥ báº¡n cÃ³ 8 Ä‘oáº¡n dá»¯ liá»‡u Ä‘Æ°á»£c Ä‘Ã¡nh sá»‘ tá»« 1 Ä‘áº¿n 8, cÃ¡c Ä‘oáº¡n Ä‘Ã¡nh sá»‘ láº» (1,3,5,7) sáº½ Ä‘Æ°á»£c ghi lÃªn Ä‘Ä©a cá»©ng Ä‘áº§u tiÃªn vÃ  cÃ¡c Ä‘oáº¡n Ä‘Ã¡nh sá»‘ cháºµn (2,4,6,8) sáº½ Ä‘Æ°á»£c ghi lÃªn Ä‘Ä©a thá»© hai. Äá»ƒ Ä‘Æ¡n giáº£n hÆ¡n, báº¡n cÃ³ thá»ƒ hÃ¬nh dung mÃ¬nh cÃ³ 100MB dá»¯ liá»‡u vÃ  thay vÃ¬ dá»“n 100MB vÃ o má»™t Ä‘Ä©a cá»©ng duy nháº¥t</p>\r\n<p>ÄÃ¢y lÃ  dáº¡ng RAID Ä‘ang Ä‘Æ°á»£c ngÆ°á»i dÃ¹ng Æ°a thÃ­ch do kháº£ nÄƒng nÃ¢ng cao hiá»‡u suáº¥t trao Ä‘á»•i dá»¯ liá»‡u cá»§a Ä‘Ä©a cá»©ng. ÄÃ²i há»i tá»‘i thiá»ƒu hai Ä‘Ä©a cá»©ng, RAID 0 cho phÃ©p mÃ¡y tÃ­nh ghi dá»¯ liá»‡u lÃªn chÃºng theo má»™t phÆ°Æ¡ng thá»©c Ä‘áº·c biá»‡t Ä‘Æ°á»£c gá»i lÃ  Striping. VÃ­ dá»¥ báº¡n cÃ³ 8 Ä‘oáº¡n dá»¯ liá»‡u Ä‘Æ°á»£c Ä‘Ã¡nh sá»‘ tá»« 1 Ä‘áº¿n 8, cÃ¡c Ä‘oáº¡n Ä‘Ã¡nh sá»‘ láº» (1,3,5,7) sáº½ Ä‘Æ°á»£c ghi lÃªn Ä‘Ä©a cá»©ng Ä‘áº§u tiÃªn vÃ  cÃ¡c Ä‘oáº¡n Ä‘Ã¡nh sá»‘ cháºµn (2,4,6,8) sáº½ Ä‘Æ°á»£c ghi lÃªn Ä‘Ä©a thá»© hai. Äá»ƒ Ä‘Æ¡n giáº£n hÆ¡n, báº¡n cÃ³ thá»ƒ hÃ¬nh dung mÃ¬nh cÃ³ 100MB dá»¯ liá»‡u vÃ  thay vÃ¬ dá»“n 100MB vÃ o má»™t Ä‘Ä©a cá»©ng duy nháº¥t</p>\r\n<p>ÄÃ¢y lÃ  dáº¡ng RAID Ä‘ang Ä‘Æ°á»£c ngÆ°á»i dÃ¹ng Æ°a thÃ­ch do kháº£ nÄƒng nÃ¢ng cao hiá»‡u suáº¥t trao Ä‘á»•i dá»¯ liá»‡u cá»§a Ä‘Ä©a cá»©ng. ÄÃ²i há»i tá»‘i thiá»ƒu hai Ä‘Ä©a cá»©ng, RAID 0 cho phÃ©p mÃ¡y tÃ­nh ghi dá»¯ liá»‡u lÃªn chÃºng theo má»™t phÆ°Æ¡ng thá»©c Ä‘áº·c biá»‡t Ä‘Æ°á»£c gá»i lÃ  Striping. VÃ­ dá»¥ báº¡n cÃ³ 8 Ä‘oáº¡n dá»¯ liá»‡u Ä‘Æ°á»£c Ä‘Ã¡nh sá»‘ tá»« 1 Ä‘áº¿n 8, cÃ¡c Ä‘oáº¡n Ä‘Ã¡nh sá»‘ láº» (1,3,5,7) sáº½ Ä‘Æ°á»£c ghi lÃªn Ä‘Ä©a cá»©ng Ä‘áº§u tiÃªn vÃ  cÃ¡c Ä‘oáº¡n Ä‘Ã¡nh sá»‘ cháºµn (2,4,6,8) sáº½ Ä‘Æ°á»£c ghi lÃªn Ä‘Ä©a thá»© hai. Äá»ƒ Ä‘Æ¡n giáº£n hÆ¡n, báº¡n cÃ³ thá»ƒ hÃ¬nh dung mÃ¬nh cÃ³ 100MB dá»¯ liá»‡u vÃ  thay vÃ¬ dá»“n 100MB vÃ o má»™t Ä‘Ä©a cá»©ng duy nháº¥t</p>', 1284485101, 0),
(12, 'PhÆ°Æ¡ng phÃ¡p phÃ²ng há»™ mÃ¡y tÃ­nh', '978027_dhe.jpg', 'JBOD (Just a Bunch Of Disks) thá»±c táº¿ khÃ´ng pháº£i lÃ  má»™t dáº¡ng RAID chÃ­nh thá»‘ng, nhÆ°ng láº¡i cÃ³ má»™t sá»‘ Ä‘áº·c Ä‘iá»ƒm liÃªn quan tá»›i RAID vÃ  Ä‘Æ°á»£c Ä‘a sá»‘ cÃ¡c thiáº¿t bá»‹ Ä‘iá»u khiá»ƒn RAID há»— trá»£. JBOD cho phÃ©p báº¡n gáº¯n bao nhiÃªu á»• Ä‘Ä©a tÃ¹y thÃ­ch vÃ o bá»™ Ä‘iá»u khiá»ƒn RAID cá»§a mÃ¬nh (dÄ© nhiÃªn lÃ  trong giá»›i háº¡n cá»•ng cho phÃ©p). Sau Ä‘Ã³ chÃºng sáº½ Ä‘Æ°á»£c â€œtá»•ng há»£pâ€ láº¡i thÃ nh má»™t Ä‘Ä©a cá»©ng lá»›n hÆ¡n cho há»‡ thá»‘ng sá»­ dá»¥ng. VÃ­ dá»¥ báº¡n cáº¯m vÃ o Ä‘Ã³ cÃ¡c á»• 10GB, 20GB, 30GB thÃ¬ thÃ´ng qua bá»™ Ä‘iá»u khiá»ƒn RAID cÃ³ há»— trá»£ JBOD, mÃ¡y tÃ­nh sáº½ nháº­n ra má»™t á»• Ä‘Ä©a 60GB. Tuy nhiÃªn, lÆ°u Ã½ lÃ  JBOD khÃ´ng há» Ä‘em láº¡i báº¥t cá»© má»™t giÃ¡ trá»‹ phá»¥ trá»™i nÃ o khÃ¡c: khÃ´ng cáº£i thiá»‡n vá» hiá»‡u nÄƒng, khÃ´ng mang láº¡i giáº£i phÃ¡p an toÃ n dá»¯ liá»‡u, chá»‰ lÃ  káº¿t ná»‘i vÃ  tá»•ng há»£p dung lÆ°á»£ng mÃ  thÃ´i.', '<p>5. JBOD <br />\r\n<br />\r\nJBOD (Just a Bunch Of Disks) thá»±c táº¿ khÃ´ng pháº£i lÃ  má»™t dáº¡ng RAID chÃ­nh thá»‘ng, nhÆ°ng láº¡i cÃ³ má»™t sá»‘ Ä‘áº·c Ä‘iá»ƒm liÃªn quan tá»›i RAID vÃ  Ä‘Æ°á»£c Ä‘a sá»‘ cÃ¡c thiáº¿t bá»‹ Ä‘iá»u khiá»ƒn RAID há»— trá»£. JBOD cho phÃ©p báº¡n gáº¯n bao nhiÃªu á»• Ä‘Ä©a tÃ¹y thÃ­ch vÃ o bá»™ Ä‘iá»u khiá»ƒn RAID cá»§a mÃ¬nh (dÄ© nhiÃªn lÃ  trong giá»›i háº¡n cá»•ng cho phÃ©p). Sau Ä‘Ã³ chÃºng sáº½ Ä‘Æ°á»£c &ldquo;tá»•ng há»£p&rdquo; láº¡i thÃ nh má»™t Ä‘Ä©a cá»©ng lá»›n hÆ¡n cho há»‡ thá»‘ng sá»­ dá»¥ng. VÃ­ dá»¥ báº¡n cáº¯m vÃ o Ä‘Ã³ cÃ¡c á»• 10GB, 20GB, 30GB thÃ¬ thÃ´ng qua bá»™ Ä‘iá»u khiá»ƒn RAID cÃ³ há»— trá»£ JBOD, mÃ¡y tÃ­nh sáº½ nháº­n ra má»™t á»• Ä‘Ä©a 60GB. Tuy nhiÃªn, lÆ°u Ã½ lÃ  JBOD khÃ´ng há» Ä‘em láº¡i báº¥t cá»© má»™t giÃ¡ trá»‹ phá»¥ trá»™i nÃ o khÃ¡c: khÃ´ng cáº£i thiá»‡n vá» hiá»‡u nÄƒng, khÃ´ng mang láº¡i giáº£i phÃ¡p an toÃ n dá»¯ liá»‡u, chá»‰ lÃ  káº¿t ná»‘i vÃ  tá»•ng há»£p dung lÆ°á»£ng mÃ  thÃ´i. <br />\r\n<br />\r\n6. Má»™t sá»‘ loáº¡i RAID khÃ¡c<br />\r\n<br />\r\nNgoÃ i cÃ¡c loáº¡i Ä‘Æ°á»£c Ä‘á» cáº­p á»Ÿ trÃªn, báº¡n cÃ²n cÃ³ thá»ƒ báº¯t gáº·p nhiá»u loáº¡i RAID khÃ¡c nhÆ°ng chÃºng khÃ´ng Ä‘Æ°á»£c sá»­ dá»¥ng rá»™ng rÃ£i mÃ  chá»‰ giá»›i háº¡n trong cÃ¡c há»‡ thá»‘ng mÃ¡y tÃ­nh phá»¥c vá»¥ má»¥c Ä‘Ã­ch riÃªng, cÃ³ thá»ƒ ká»ƒ nhÆ°: Level 2 (Error-Correcting Coding), Level 3 (Bit-Interleaved Parity), Level 4 (Dedicated Parity Drive), Level 6 (Independent Data Disks with Double Parity), Level 10 (Stripe of Mirrors, ngÆ°á»£c láº¡i vá»›i RAID 0+1), Level 7 (thÆ°Æ¡ng hiá»‡u cá»§a táº­p Ä‘oÃ n Storage Computer, cho phÃ©p thÃªm bá»™ Ä‘á»‡m cho RAID 3 vÃ  4), RAID S (phÃ¡t minh cá»§a táº­p Ä‘oÃ n EMC vÃ  Ä‘Æ°á»£c sá»­ dá»¥ng trong cÃ¡c há»‡ thá»‘ng lÆ°u trá»¯ Symmetrix cá»§a há»). BÃªn cáº¡nh Ä‘Ã³ cÃ²n má»™t sá»‘ biáº¿n thá»ƒ khÃ¡c, vÃ­ dá»¥ nhÆ° Intel Matrix Storage cho phÃ©p cháº¡y kiá»ƒu RAID 0+1 vá»›i chá»‰ 2 á»• cá»©ng hoáº·c RAID 1.5 cá»§a DFI trÃªn cÃ¡c há»‡ BMC 865, 875. ChÃºng tuy cÃ³ nhiá»u Ä‘iá»ƒm khÃ¡c biá»‡t nhÆ°ng Ä‘a pháº§n Ä‘á»u lÃ  báº£n cáº£i tiáº¿n cá»§a cÃ¡c phÆ°Æ¡ng thá»©c RAID truyá»n thá»‘ng. <br />\r\n<br />\r\nB. Báº N Cáº¦N GÃŒ Äá»‚ CHáº Y RAID? <br />\r\n<br />\r\nÄá»ƒ cháº¡y Ä‘Æ°á»£c RAID, báº¡n cáº§n tá»‘i thiá»ƒu má»™t card Ä‘iá»u khiá»ƒn (cÃ³ thá»ƒ lÃ  onboard hoáº·c card rá»i) vÃ  hai á»• Ä‘Ä©a cá»©ng giá»‘ng nhau. ÄÄ©a cá»©ng cÃ³ thá»ƒ á»Ÿ báº¥t cá»© chuáº©n nÃ o, tá»« ATA, Serial ATA hay SCSI, SAS tá»‘t nháº¥t chÃºng nÃªn hoÃ n toÃ n giá»‘ng nhau vÃ¬ má»™t nguyÃªn táº¯c Ä‘Æ¡n giáº£n lÃ  khi hoáº¡t Ä‘á»™ng á»Ÿ cháº¿ Ä‘á»™ Ä‘á»“ng bá»™ nhÆ° RAID, hiá»‡u nÄƒng chung cá»§a cáº£ há»‡ thá»‘ng sáº½ bá»‹ kÃ©o xuá»‘ng theo á»• tháº¥p nháº¥t náº¿u cÃ³. VÃ­ dá»¥ khi báº¡n báº¯t á»• 160GB cháº¡y RAID vá»›i á»• 40GB (báº¥t ká»ƒ 0 hay 1) thÃ¬ coi nhÆ° báº¡n Ä‘Ã£ lÃ£ng phÃ­ 120GB vÃ´ Ã­ch vÃ¬ há»‡ thá»‘ng Ä‘iá»u khiá»ƒn chá»‰ coi chÃºng lÃ  má»™t cáº·p hai á»• cá»©ng 40GB mÃ  thÃ´i (ngoáº¡i trá»« trÆ°á»ng há»£p JBOD nhÆ° Ä‘Ã£ Ä‘á» cáº­p). Yáº¿u tá»‘ quyáº¿t Ä‘á»‹nh tá»›i sá»‘ lÆ°á»£ng á»• Ä‘Ä©a chÃ­nh lÃ  kiá»ƒu RAID mÃ  báº¡n Ä‘á»‹nh cháº¡y. Chuáº©n giao tiáº¿p khÃ´ng quan trá»ng láº¯m, Ä‘áº·c biá»‡t lÃ  giá»¯a SATA vÃ  ATA. Má»™t sá»‘ BMC Ä‘á»i má»›i cho phÃ©p cháº¡y RAID theo kiá»ƒu trá»™n láº«n cáº£ hai giao tiáº¿p nÃ y vá»›i nhau. Äiá»ƒn hÃ¬nh nhÆ° MSI K8N Neo2 Platinum hay dÃ²ng DFI Lanparty NForce4. Bá»™ Ä‘iá»u khiá»ƒn RAID (RAID Controller) lÃ  nÆ¡i táº­p trung cÃ¡c cÃ¡p dá»¯ liá»‡u ná»‘i cÃ¡c Ä‘Ä©a cá»©ng trong há»‡ thá»‘ng RAID vÃ  nÃ³ xá»­ lÃ½ toÃ n bá»™ dá»¯ liá»‡u Ä‘i qua Ä‘Ã³. Bá»™ Ä‘iá»u khiá»ƒn nÃ y cÃ³ nhiá»u dáº¡ng khÃ¡c nhau, tá»« card tÃ¡ch rá»i cho dáº¿n chip tÃ­ch há»£p trÃªn BMC. Äá»‘i vá»›i cÃ¡c há»‡ thá»‘ng PC, tuy chÆ°a phá»• biáº¿n nhÆ°ng viá»‡c chá»n mua BMC cÃ³ RAID tÃ­ch há»£p lÃ  Ä‘iá»u nÃªn lÃ m vÃ¬ nÃ³i chung Ä‘Ã¢y lÃ  má»™t trong nhá»¯ng giáº£i phÃ¡p cáº£i thiá»‡n hiá»‡u nÄƒng há»‡ thá»‘ng rÃµ rá»‡t vÃ  ráº» tiá»n nháº¥t, chÆ°a tÃ­nh tá»›i giÃ¡ trá»‹ an toÃ n dá»¯ liá»‡u cá»§a chÃºng. Má»™t thÃ nh pháº§n khÃ¡c cá»§a há»‡ thá»‘ng RAID khÃ´ng báº¯t buá»™c pháº£i cÃ³ nhÆ°ng Ä‘Ã´i khi lÃ  há»¯u dá»¥ng, Ä‘Ã³ lÃ  cÃ¡c khay hoÃ¡n Ä‘á»•i nÃ³ng á»• Ä‘Ä©a. NÃ³ cho phÃ©p báº¡n thay cÃ¡c Ä‘Ä©a cá»©ng gáº·p trá»¥c tráº·c trong khi há»‡ thá»‘ng Ä‘ang hoáº¡t Ä‘á»™ng mÃ  khÃ´ng pháº£i táº¯t mÃ¡y (chá»‰ Ä‘Æ¡n giáº£n lÃ  má»Ÿ khÃ³a, rÃºt á»• ra vÃ  cáº¯m á»• má»›i vÃ o). Thiáº¿t bá»‹ nÃ y thÆ°á»ng sá»­ dá»¥ng vá»›i á»• cá»©ng SCSI hoáº·c SAS vÃ  khÃ¡ quan trá»ng Ä‘á»‘i vá»›i cÃ¡c há»‡ thá»‘ng mÃ¡y chá»§ vá»‘n yÃªu cáº§u hoáº¡t Ä‘á»™ng liÃªn tá»¥c. Vá» pháº§n má»m thÃ¬ khÃ¡ Ä‘Æ¡n giáº£n vÃ¬ háº§u háº¿t cÃ¡c há»‡ Ä‘iá»u hÃ nh hiá»‡n Ä‘áº¡i Ä‘á»u há»— trá»£ RAID ráº¥t tá»‘t, Ä‘áº·c biá»‡t lÃ  Microsoft Windows. Náº¿u báº¡n sá»­ dá»¥ng Windows XP, 2k3... thÃ¬ bá»• sung RAID khÃ¡ dá»… dÃ ng. Quan trá»ng nháº¥t lÃ  trÃ¬nh Ä‘iá»u khiá»ƒn nhÆ°ng tháº­t tuyá»‡t khi chÃºng Ä‘Ã£ Ä‘Æ°á»£c kÃ¨m sáºµn vá»›i thiáº¿t bá»‹.</p>', 1284485166, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL auto_increment,
  `subject` tinyint(3) NOT NULL default '1',
  `fullname` varchar(50) NOT NULL,
  `company` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `fax` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `domain` varchar(30) NOT NULL,
  `service_type` int(11) NOT NULL,
  `service` int(11) NOT NULL,
  `service_plan` int(11) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `payment` int(11) NOT NULL default '1',
  `infos` text NOT NULL,
  `order_date` int(11) NOT NULL,
  `status` tinyint(3) NOT NULL default '0',
  `active` tinyint(3) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `subject`, `fullname`, `company`, `address`, `phone`, `fax`, `email`, `domain`, `service_type`, `service`, `service_plan`, `duration`, `payment`, `infos`, `order_date`, `status`, `active`) VALUES
(1, 1, 'Do Hoang', 'hoangcn02', 'sd fhdsf dsjdf', '544545645', '54644456', 'hoangcn02@yahoo.com', '', 1, 1, 1, '3 thang', 1, 'f gdfg dfgfd gfdg fd gdg', 1234588745, 1, 0),
(2, 1, 'Minh Ngoc', 'hoangcn02', 'dsf hdsf hdsfsdfdsjf', '54545644', '64644566', 'minhngoc@yahoo.com', 'minhngoc.com', 1, 1, 2, '5 thang', 2, 'dsf dfsf sd fds fsd fsdfdsf', 123459887, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `rank` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`id`, `title`, `content`, `rank`) VALUES
(1, 'HÆ°á»›ng dáº«n Ä‘Äƒng kÃ­ tÃªn miá»n', '<p><strong>1. TÃŠN MIá»€N</strong></p>\r\n<p><strong><em><span style="text-decoration: underline;">BÆ°á»›c 1</span></em></strong>: Báº¡n tiáº¿n hÃ nh <a href="http://nhanhoa.com/?site=whois">Kiá»ƒm tra tÃªn miá»n</a> vÃ  <a href="http://nhanhoa.com/?site=service&amp;view=group&amp;id=1">Báº£ng giÃ¡ tÃªn miá»n</a> Náº¿u káº¿t quáº£ tráº£ vá» lÃ : <strong>TÃªn miá»n www.***.com</strong> chÆ°a cÃ³ chá»§ thá»ƒ Ä‘Äƒng kÃ½, báº¡n cÃ³ thá»ƒ Ä‘Äƒng kÃ½ tÃªn miá»n nÃ y. Báº¡n tiáº¿p tá»¥c qua bÆ°á»›c 2.</p>\r\n<p><strong><em><span style="text-decoration: underline;">BÆ°á»›c 2</span></em></strong>: Báº¡n cÃ³ thá»ƒ thanh toÃ¡n trá»±c tiáº¿p táº¡i <a href="http://nhanhoa.com/?site=page&amp;id=24">vÄƒn phÃ²ng cÃ´ng ty</a> hoáº·c <a href="http://nhanhoa.com/?site=page&amp;id=24">chuyá»ƒn tiá»n</a> qua ngÃ¢n hÃ ng hoáº·c qua bÆ°u Ä‘iá»‡n. TrÆ°á»ng há»£p chuyá»ƒn tiá»n qua ngÃ¢n hÃ ng hoáº·c qua bÆ°u Ä‘iá»‡n, báº¡n email tá»›i Sales@nhanhoa.com (khu vá»±c HÃ  Ná»™i) hoáº·c hcmsales@nhanhoa.com (khu vá»±c HCM) Ä‘Ã­nh kÃ¨m thÃ´ng tin Ä‘Äƒng kÃ½ dá»‹ch vá»¥ vÃ  thÃ´ng tin xÃ¡c nháº­n thanh toÃ¡n (kÃ¨m báº£n scan phiáº¿u chuyá»ƒn tiá»n náº¿u báº¡n cáº§n dá»‹ch vá»¥ sá»›m) nhÆ° sau:</p>\r\n<p><strong>TiÃªu Ä‘á» (Subject):</strong> ÄÄƒng kÃ½ tÃªn miá»n &hellip; &hellip; &hellip;</p>\r\n<p><strong>Ná»™i dung (Body):</strong></p>\r\n<p>-	TÃªn chá»§ thá»ƒ Ä‘Äƒng kÃ½ tÃªn miá»n (TÃªn cÃ´ng ty hoáº·c CÃ¡ nhÃ¢n): ..... <br />\r\n-	TÃªn ngÆ°á»i phá»¥ trÃ¡ch: ..... <br />\r\n-	Äá»‹a chá»‰: ..... <br />\r\n-	Äiá»‡n thoáº¡i: ..... <br />\r\n-	Email quáº£n lÃ½ dá»‹ch vá»¥ (NhÃ¢n HÃ²a sáº½ liÃªn láº¡c vá»›i khÃ¡ch hÃ ng qua email nÃ y trong suá»‘t quÃ¡ trÃ¬nh sá»­ dá»¥ng dá»‹ch vá»¥): ..... <br />\r\n-	Sá»‘ CMT, ngÃ y cáº¥p, nÆ¡i cáº¥p (Äá»‘i vá»›i khÃ¡ch hÃ ng lÃ  cÃ¡ nhÃ¢n): ..... <br />\r\n-	TÃªn miá»n Ä‘Äƒng kÃ½: ..... <br />\r\n-	Thá»i háº¡n: ...../nÄƒm (Ã­t nháº¥t 01 nÄƒm, nhiá»u nháº¥t 10 nÄƒm)<br />\r\n- YÃªu cáº§u thÃªm: Trá» tÃªn miá»n vá» Ä‘á»‹a chá»‰ DNS vÃ  IP cá»¥ thá»ƒ (náº¿u cÃ³)</p>\r\n<p>-	<strong>LÆ°u Ã½ Ä‘á»‘i vá»›i tÃªn miá»n QUá»C GIA:</strong></p>\r\n<p>-	<strong>CÃ¡ nhÃ¢n</strong> cáº§n cÃ³ báº£n khai Ä‘Äƒng kÃ½ + Báº£n photo CMT.</p>\r\n<p>ÄÄ‚NG KÃ TÃŠN MIá»€N Cáº¤P 2 .VN : <a title="Download" target="_blank" href="http://nhanhoa.com/uploads/attach/1273636909_canhan_cap2.doc">Dowload báº£n khai</a><br />\r\nÄÄ‚NG KÃ TÃŠN MIá»€N Cáº¤P 3 .VN : <a title="Download" target="_blank" href="http://nhanhoa.com/uploads/attach/1273636914_canhan_cap3.doc">Dowload báº£n khai</a></p>\r\n<p>-	<strong>Doanh nghiá»‡p</strong> cáº§n cÃ³ báº£n khai Ä‘Äƒng kÃ½ + KÃ½ Ä‘Ã³ng dáº¥u cÃ´ng ty (3 báº£n). <a title="Download" target="_blank" href="http://nhanhoa.com/uploads/attach/1273636901_bankhai_congty.doc">Download báº£n khai</a></p>\r\n<p>- Báº£n khai cÃ³ 2 tá», cáº§n cÃ³ dáº¥u giÃ¡p lai, báº£n khai khÃ´ng Ä‘Æ°á»£c gáº¡ch bá», táº©y xÃ³a.</p>\r\n<p><strong><em><span style="text-decoration: underline;">BÆ°á»›c 3</span></em></strong>: Ngay sau khi nháº­n Ä‘Æ°á»£c thanh toÃ¡n &amp; email chÃºng tÃ´i sáº½ tiáº¿n hÃ nh khá»Ÿi táº¡o dá»‹ch vá»¥ vÃ  gá»­i thÃ´ng tin xÃ¡c nháº­n Ä‘áº¿n email quáº£n lÃ½ dá»‹ch vá»¥ cá»§a báº¡n sá»›m nháº¥t trong vÃ²ng 3h Ä‘áº¿n 24h.</p>', 1),
(3, 'ÄÄƒng kÃ­ hosting - server', '<p><strong><em><span style="text-decoration: underline;">BÆ°á»›c 1</span></em></strong>: Báº¡n tham kháº£o <a href="http://nhanhoa.com/?site=service&amp;view=group&amp;id=4">Báº£ng giÃ¡ hosting</a> vÃ  <a href="http://nhanhoa.com/?site=service&amp;view=group&amp;id=8">Báº£ng giÃ¡ server</a> táº¡i Ä‘Ã¢y</p>\r\n<p><strong><em><span style="text-decoration: underline;">BÆ°á»›c 2</span></em></strong>: Báº¡n cÃ³ thá»ƒ thanh toÃ¡n trá»±c tiáº¿p táº¡i <a href="http://nhanhoa.com/?site=page&amp;id=24">vÄƒn phÃ²ng cÃ´ng ty</a> hoáº·c <a href="http://nhanhoa.com/?site=page&amp;id=24">chuyá»ƒn tiá»n</a> qua ngÃ¢n hÃ ng hoáº·c qua bÆ°u Ä‘iá»‡n.</p>\r\n<p>TrÆ°á»ng há»£p chuyá»ƒn tiá»n qua ngÃ¢n hÃ ng hoáº·c qua bÆ°u Ä‘iá»‡n, báº¡n email tá»›i Sales@nhanhoa.com (khu vá»±c HÃ  Ná»™i) hoáº·c hcmsales@nhanhoa.com (khu vá»±c HCM) Ä‘Ã­nh kÃ¨m báº£n scan phiáº¿u chuyá»ƒn tiá»n vÃ  cÃ¡c thÃ´ng tin Ä‘Äƒng kÃ½ dá»‹ch vá»¥ nhÆ° sau:</p>\r\n<p><strong>TiÃªu Ä‘á» (Subject):</strong> ÄÄƒng kÃ½ dá»‹ch vá»¥ &hellip; &hellip; &hellip;</p>\r\n<p><strong>Ná»™i dung (Body):</strong></p>\r\n<p>-	TÃªn chá»§ thá»ƒ Ä‘Äƒng kÃ½ (TÃªn cÃ´ng ty hoáº·c CÃ¡ nhÃ¢n): ..... <br />\r\n-	TÃªn ngÆ°á»i phá»¥ trÃ¡ch: ..... <br />\r\n-	Äá»‹a chá»‰: ..... <br />\r\n-	Äiá»‡n thoáº¡i: ..... <br />\r\n-	Email quáº£n lÃ½ dá»‹ch vá»¥ (NhÃ¢n HÃ²a sáº½ liÃªn láº¡c vá»›i khÃ¡ch hÃ ng qua email nÃ y trong suá»‘t quÃ¡ trÃ¬nh sá»­ dá»¥ng dá»‹ch vá»¥): ..... <br />\r\n-	GÃ³i dá»‹ch vá»¥ cáº§n Ä‘Äƒng kÃ½/ Há»‡ Ä‘iá»u hÃ nh/ thá»i háº¡n sá»­ dá»¥ng (vÃ­ dá»¥: Windows Hosting - CÃ¡ NhÃ¢n/12 thÃ¡ng): ..... <br />\r\n-	YÃªu cáº§u thÃªm: Add domain: ..&hellip;vÃ o host (náº¿u cÃ³)</p>\r\n<p><strong><em><span style="text-decoration: underline;">BÆ°á»›c 3</span></em></strong>: Ngay sau khi nháº­n Ä‘Æ°á»£c thanh toÃ¡n &amp; email chÃºng tÃ´i sáº½ tiáº¿n hÃ nh khá»Ÿi táº¡o dá»‹ch vá»¥ vÃ  gá»­i thÃ´ng tin xÃ¡c nháº­n Ä‘áº¿n email quáº£n lÃ½ dá»‹ch vá»¥ cá»§a báº¡n sá»›m nháº¥t trong vÃ²ng 3h Ä‘áº¿n 24h.</p>', 2),
(4, 'Duy trÃ¬ dá»‹ch vá»¥', '<p>- Äá»‘i vá»›i cÃ¡c dá»‹ch vá»¥ Ä‘Ã£ Ä‘Äƒng kÃ½ trÆ°á»›c Ä‘Ã³, tá»›i háº¡n thanh toÃ¡n tiáº¿p theo cá»§a há»£p Ä‘á»“ng, NhÃ¢n HÃ²a sáº½ gá»­i yÃªu cáº§u thanh toÃ¡n phÃ­ duy trÃ¬, quÃ½ khÃ¡ch vui lÃ²ng thanh toÃ¡n theo biá»ƒu phÃ­ quy Ä‘á»‹nh Ä‘á»ƒ dá»‹ch vá»¥ cÃ³ thá»ƒ Ä‘Æ°á»£c tiáº¿p tá»¥c duy trÃ¬ vÃ  khÃ´ng bá»‹ giÃ¡n Ä‘oáº¡n.</p>\r\n<p>- TrÆ°á»ng há»£p quÃ¡ háº¡n thanh toÃ¡n vÃ  chÆ°a thanh toÃ¡n phÃ­ duy trÃ¬, dá»‹ch vá»¥ sáº½ bá»‹ táº¡m ngÆ°ng, nÃªn quÃ½ khÃ¡ch lÆ°u Ã½ chá»§ Ä‘á»™ng thanh toÃ¡n trÆ°á»›c thá»i gian háº¿t háº¡n há»£p Ä‘á»“ng.</p>\r\n<p>- Khi thay Ä‘á»•i thÃ´ng tin liÃªn há»‡ (Ä‘á»‹a chá»‰, Ä‘iá»‡n thoáº¡i, email...), QuÃ½ khÃ¡ch vui lÃ²ng thÃ´ng bÃ¡o láº¡i Ä‘á»ƒ chÃºng tÃ´i cáº­p nháº­t vÃ  liÃªn há»‡ khi cáº§n thiáº¿t. CÃ¡c trÆ°á»ng há»£p chuyá»ƒn khoáº£n duy trÃ¬ dá»‹ch vá»¥, QuÃ½ khÃ¡ch cáº§n ghi rÃµ thÃ´ng tin dá»‹ch vá»¥ cáº§n duy trÃ¬, trÃ¡nh tÃ¬nh tráº¡ng khÃ´ng xÃ¡c nháº­n Ä‘Æ°á»£c dá»‹ch vá»¥ cáº§n duy trÃ¬ dáº«n tá»›i háº¿t háº¡n dá»‹ch vá»¥.</p>', 3);

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL auto_increment,
  `type` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `rank` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `type`, `title`, `content`, `rank`) VALUES
(1, 2, 'Linux hosting', '<p>ds df ds fds fsdf dsfds fdsf dsf dsfds fsddsfsd fds fsdfdssd fds</p>', 1),
(2, 1, 'MÃ¡y chá»§ linux', '<p>Ná»™i dung mÃ¡y chá»§ linux</p>', 1),
(3, 2, 'Window Hosting', '<p>Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting.</p>\r\n<p>Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting. Ná»™i dung pháº§n window hosting.</p>', 1),
(4, 2, 'Linux hosting', '<p>Ná»™i dung linux hosting. Ná»™i dung linux hosting. Ná»™i dung linux hosting. Ná»™i dung linux hosting. Ná»™i dung linux hosting. Ná»™i dung linux hosting. Ná»™i dung linux hosting. Ná»™i dung linux hosting. Ná»™i dung linux hosting.</p>', 2),
(5, 2, 'Email Hosting', '<p>Ná»™i dung pháº§n email hosting. Ná»™i dung pháº§n email hosting. Ná»™i dung pháº§n email hosting. Ná»™i dung pháº§n email hosting. Ná»™i dung pháº§n email hosting. Ná»™i dung pháº§n email hosting. Ná»™i dung pháº§n email hosting. Ná»™i dung pháº§n email hosting. Ná»™i dung pháº§n email hosting. Ná»™i dung pháº§n email hosting.</p>\r\n<p>Ná»™i dung pháº§n email hosting. Ná»™i dung pháº§n email hosting. Ná»™i dung pháº§n email hosting. Ná»™i dung pháº§n email hosting. Ná»™i dung pháº§n email hosting. Ná»™i dung pháº§n email hosting.</p>', 3),
(6, 2, 'Mega Host', '<p>Ná»™i dung megahosting. Ná»™i dung megahostingNá»™i dung megahostingNá»™i dung megahostingNá»™i dung megahostingNá»™i dung megahostingNá»™i dung megahostingNá»™i dung megahostingNá»™i dung megahosting</p>\r\n<p>Ná»™i dung megahostingNá»™i dung megahostingNá»™i dung megahostingNá»™i dung megahostingNá»™i dung megahosting</p>', 4),
(7, 1, 'MÃ¡y chá»§ VPS', '<p>Ná»™i dung mÃ¡y chá»§ VPS</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service_intro`
--

CREATE TABLE `service_intro` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `intro` text NOT NULL,
  `content` text NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `rank` tinyint(3) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `service_intro`
--

INSERT INTO `service_intro` (`id`, `name`, `intro`, `content`, `thumb`, `img`, `rank`) VALUES
(2, 'TiÃªu Ä‘á» thÃ´ng tin giá»›i thiá»‡u dá»‹ch vá»¥', '<p>Giá»›i thiá»‡u vá» dá»‹ch vá»¥</p>', '<p>Ná»™i dung dá»‹ch vá»¥</p>', '276343_125x125-7.gif', '', 10),
(3, 'TiÃªu Ä‘á» dá»‹ch vá»¥ má»›i', '<p>Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.</p>', '<p>Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.Giá»›i thiá»‡u dá»‹ch vá»¥ má»›i.</p>', '356969_ima3es.jpg', '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `service_plan`
--

CREATE TABLE `service_plan` (
  `id` int(11) NOT NULL auto_increment,
  `service_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `showhome` tinyint(3) NOT NULL default '0',
  `price` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `service_plan`
--

INSERT INTO `service_plan` (`id`, `service_id`, `title`, `content`, `showhome`, `price`) VALUES
(3, 6, 'PLAN A', '<table>\r\n<tr>\r\n <td height="33" align="left" valign="top" background="images/megahost_49.gif" class="danhmuc1">Dung lÆ°á»£ng: 500Mb</td>\r\n </tr>\r\n <tr>\r\n <td height="33" align="left" valign="top" background="images/megahost_49.gif" class="danhmuc1">BÄƒng thÃ´ng: 50000mb / thÃ¡ng</td>\r\n </tr>\r\n <tr>\r\n <td height="33" align="left" valign="top" background="images/megahost_49.gif" class="danhmuc1">Database: 5</td>\r\n </tr>\r\n <tr>\r\n <td height="33" align="left" valign="top" background="images/megahost_49.gif" class="danhmuc1">Email: 50</td>\r\n </tr>\r\n <tr>\r\n <td height="33" align="left" valign="top" background="images/megahost_49.gif" class="danhmuc1">TÃªn miá»n: 5</td>\r\n </tr>\r\n</table>', 1, '30.000VND/ThÃ¡ng');

-- --------------------------------------------------------

--
-- Table structure for table `service_type`
--

CREATE TABLE `service_type` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `rank` tinyint(3) NOT NULL default '1',
  `active` tinyint(3) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `service_type`
--

INSERT INTO `service_type` (`id`, `name`, `rank`, `active`) VALUES
(1, 'MÃ¡y chá»§', 1, 1),
(2, 'Hosting', 1, 1),
(3, 'TÃªn miá»n', 1, 1),
(4, 'NÆ¡i Ä‘áº·t mÃ¡y chá»§', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` varchar(32) NOT NULL,
  `username` varchar(32) default NULL,
  `member_id` int(11) default '0',
  `groups_id` int(11) NOT NULL default '0',
  `security_code` varchar(32) NOT NULL,
  `ip_address` varchar(15) default NULL,
  `running_time` int(10) default NULL,
  `register_time` int(11) NOT NULL,
  `antiflood` int(10) NOT NULL default '0',
  `language` varchar(32) default NULL,
  `skin` varchar(32) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `username`, `member_id`, `groups_id`, `security_code`, `ip_address`, `running_time`, `register_time`, `antiflood`, `language`, `skin`) VALUES
('7c3abebf2e45f59d2713cae6345a2bc0', NULL, 0, 0, '', '127.0.0.1', 1284514155, 0, 0, NULL, NULL),
('ad4ef5e3c2ef5a67f0fc6146cebc2090', NULL, 0, 0, '', '127.0.0.1', 1284488605, 0, 0, NULL, NULL),
('de602020556ca4baa365333b328764da', 'admin', 1, 1, '', '127.0.0.1', 1284490674, 0, 0, NULL, NULL),
('50a0567c8dbf294021ceae31c00103a2', NULL, 0, 0, '', '127.0.0.1', 1284514261, 0, 0, NULL, NULL),
('4a6c27d6685568eff6a84c0c1cf4c830', NULL, 0, 0, '1DRAQL', '127.0.0.1', 1284550340, 0, 0, NULL, NULL),
('3c70ba544ac49be8a0179272a598ef67', 'admin', 1, 1, 'PX8MM1', '127.0.0.1', 1284550335, 0, 0, NULL, NULL),
('f8d248a6d3c989b537c8605b4efc1938', 'admin', 1, 1, '', '127.0.0.1', 1284574297, 0, 0, NULL, NULL),
('08a864dba0507d7abc202089a2e678ab', NULL, 0, 0, '', '127.0.0.1', 1284573114, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `statistics`
--

CREATE TABLE `statistics` (
  `keyword` varchar(32) NOT NULL,
  `value` int(10) default NULL,
  PRIMARY KEY  (`keyword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `statistics`
--

INSERT INTO `statistics` (`keyword`, `value`) VALUES
('install_date', 1222876800),
('today_online', 1),
('last_update', 1284569774),
('most_online', 303),
('most_online_date', 1256922000),
('hitcounter', 57),
('online_now', 2);

-- --------------------------------------------------------

--
-- Table structure for table `support`
--

CREATE TABLE `support` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `rank` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `support`
--

INSERT INTO `support` (`id`, `title`, `content`, `rank`) VALUES
(1, 'Quy Ä‘á»‹nh sá»­ dá»¥ng dá»‹ch vá»¥', '<p><span class="highlight">1.</span> NgÆ°á»i sá»­ dá»¥ng pháº£i tá»± chá»‹u trÃ¡ch nhiá»‡m vÃ  Ä‘áº£m báº£o vá»›i chÃºng tÃ´i trong viá»‡c sá»­ dá»¥ng Web Site cá»§a mÃ¬nh trÃªn server vÃ o nhá»¯ng má»¥c Ä‘Ã­ch há»£p phÃ¡p. Äáº·c biá»‡t trong nhá»¯ng trÆ°á»ng há»£p sau:</p>\r\n<p class="indent-l1"><span class="highlight">1.1</span> KhÃ´ng dÃ¹ng server vÃ o báº¥t kÃ¬ má»¥c Ä‘Ã­ch/hÃ¬nh thá»©c nÃ o vi pháº¡m phÃ¡p luáº­t Viá»‡t Nam, Ä‘áº·c biá»‡t vá» váº¥n Ä‘á» báº£n quyá»n pháº§n má»m, ca khÃºc... Äá»“ng thá»i cÃ³ trÃ¡ch nhiá»‡m kiá»ƒm soÃ¡t vÃ  ngÄƒn cáº¥m ngÆ°á»i khÃ¡c lÃ m Ä‘iá»u Ä‘Ã³ trÃªn Website cá»§a mÃ¬nh.</p>\r\n<p class="indent-l1"><span class="highlight">1.2</span> NgÆ°á»i sá»­ dá»¥ng khÃ´ng Ä‘Æ°á»£c gá»­i, táº¡o liÃªn káº¿t hoáº·c trung chuyá»ƒn cho:</p>\r\n<p class="indent-l2"><span class="highlight">1.2.1</span> Báº¥t kÃ¬ loáº¡i dá»¯ liá»‡u nÃ o mang tÃ­nh báº¥t há»£p phÃ¡p, Ä‘e dá»a, lá»«a dá»‘i, thÃ¹ háº±n, xuyÃªn táº¡c, nÃ³i xáº¥u, tá»¥c tÄ©u, khiÃªu dÃ¢m, xÃºc pháº¡m...hay cÃ¡c hÃ¬nh thá»©c bá»‹ ngÄƒn cáº¥m khÃ¡c dÆ°á»›i báº¥t kÃ¬ cÃ¡ch thá»©c nÃ o.</p>\r\n<p class="indent-l2"><span class="highlight">1.2.2</span> Báº¥t kÃ¬ loáº¡i dá»¯ liá»‡u nÃ o mÃ  cáº¥u thÃ nh hoáº·c khuyáº¿n khÃ­ch cÃ¡c hÃ¬nh thá»©c pháº¡m tá»™i; hoáº·c cÃ¡c dá»¯ liá»‡u mang tÃ­nh vi pháº¡m luáº­t sÃ¡ng cháº¿, nhÃ£n hiá»‡u, quyá»n thiáº¿t káº¿, báº£n quyá»n hay báº¥t kÃ¬ quyá»n sá»¡ há»¯u trÃ­ tuá»‡ hoáº·c cÃ¡c quyá»n háº¡n cá»§a báº¥t kÃ¬ cÃ¡ nhÃ¢n nÃ o.</p>\r\n<p class="indent-l1"><span class="highlight">1.3</span> NgÆ°á»i sá»­ dá»¥ng khÃ´ng sá»­ dá»¥ng hosting Ä‘á»ƒ gá»­i SPAM, thÆ° QUáº¢NG CÃO, BULK MAIL, MASS MAIL, BOMB MAIL, &hellip;(báº¥t ká»ƒ tá»« Ä‘Ã¢u) cÃ³ liÃªn quan Ä‘áº¿n cÃ¡c web site lÆ°u trá»¯ trong há»‡ thá»‘ng mÃ¡y chá»§ (gá»­i tá»« 15 Email cÃ¹ng má»™t thá»i Ä‘iá»ƒm vá»›i cÃ¹ng má»™t ná»™i dung) hoáº·c mang má»¥c Ä‘Ã­ch phÃ¡ hoáº¡i tá»« server hay máº¡ng cá»§a chÃºng tÃ´i, khÃ´ng khuyáº¿n khÃ­ch má»™t site nÃ o Ä‘Ã³ Ä‘áº·t trÃªn server sá»­ dá»¥ng bulk email. NgÆ°á»i sá»­ dá»¥ng pháº£i Ä‘áº£m báº£o táº¥t cáº£ cÃ¡c mail Ä‘Æ°á»£c gá»­i Ä‘i phÃ¹ há»£p vá»›i cÃ¡c luáº­t cÃ³ thá»ƒ Ã¡p dá»¥ng (bao gá»“m luáº­t báº£o vá»‡ thÃ´ng tin) vÃ  dÆ°á»›i cÃ¡c hÃ¬nh thá»©c an toÃ n. Sá»‘ lÆ°á»£ng Email gá»­i ra ngoÃ i ná»™i bá»™ cÃ´ng ty khÃ´ng quÃ¡ 300 Email/ngÃ y</p>\r\n<p class="indent-l1"><span class="highlight">1.4</span> NgÆ°á»i sá»­ dá»¥ng khÃ´ng sá»­ dá»¥ng cÃ¡c chÆ°Æ¡ng trÃ¬nh cÃ³ kháº£ nÄƒng lÃ m táº¯c ngháº½n hoáº·c Ä‘Ã¬nh trá»‡ há»‡ thá»‘ng, nhÆ° gÃ¢y cáº¡n kiá»‡t tÃ i nguyÃªn há»‡ thá»‘ng, lÃ m quÃ¡ táº£i bá»™ vi xá»­ lÃ½ vÃ  bá»™ nhá»›..</p>\r\n<p><span class="highlight">2.</span> KhÃ´ng xÃ³a file LOGFILES trong thÆ° má»¥c /logs (Dung lÆ°á»£ng file logs khÃ´ng tÃ­nh vÃ o dung lÆ°á»£ng báº¡n mua)</p>\r\n<p><span class="highlight">3.</span> ChÃºng tÃ´i cÃ³ quyá»n loáº¡i bá» account mÃ  chÃºng tÃ´i cho lÃ  khÃ´ng phÃ¹ há»£p á»Ÿ site cá»§a báº¡n mÃ  khÃ´ng thÃ´ng bÃ¡o trÆ°á»›c: ChÃºng tÃ´i khÃ´ng cháº¥p nháº­n cÃ¡c pháº§n má»m mÃ  báº¡n khÃ´ng chá»©ng minh Ä‘Æ°á»£c báº£n quyá»n thuá»™c vá» cá»§a mÃ¬nh, cÃ¡c pháº§n má»m hÆ°á»›ng dáº«n báº» khÃ³a, shell, backdoor, cÃ¡c thÃ´ng tin xáº¥u hay cÃ¡c loáº¡i nháº¡c (MP3, Video...) khÃ´ng chá»©ng minh Ä‘Æ°á»£c báº£n quyá»n cá»§a ngÆ°á»i Ä‘Æ°a lÃªn</p>\r\n<p><span class="highlight">4.</span> NgÆ°á»i sá»­ dá»¥ng giá»¯ má»™t cÃ¡ch an toÃ n cÃ¡c thÃ´ng tin nháº­n biáº¿t, máº­t kháº©u hay nhá»¯ng thÃ´ng tin máº­t khÃ¡c liÃªn quan Ä‘áº¿n tÃ i khoáº£n cá»§a báº¡n vÃ  láº­p tá»©c thÃ´ng bÃ¡o cho chÃºng tÃ´i khi báº¡n phÃ¡t hiá»‡n cÃ¡c hÃ¬nh thá»©c truy cáº­p trÃ¡i phÃ©p báº±ng tÃ i khoáº£n cá»§a báº¡n hoáº·c cÃ¡c sÆ¡ há»Ÿ vá» báº£o máº­t, bao gá»“m viá»‡c máº¥t mÃ¡t, Ä‘Ã¡nh cáº¯p hoáº·c Ä‘á»ƒ lá»™ cÃ¡c thÃ´ng tin vá» máº­t kháº©u vÃ  cÃ¡c thÃ´ng tin báº£o máº­t khÃ¡c</p>\r\n<p><span class="highlight">5.</span> NgÆ°á»i sá»­ dá»¥ng tuÃ¢n thá»§ cÃ¡c thá»§ tá»¥c mÃ  chÃºng tÃ´i Ä‘Æ°a ra vÃ  sáº½ khÃ´ng Ä‘Æ°á»£c dÃ¹ng server vÃ o nhá»¯ng má»¥c Ä‘Ã­ch cÃ³ thá»ƒ gÃ¢y áº£nh hÆ°á»Ÿng Ä‘áº¿n cÃ¡c khÃ¡ch hÃ ng khÃ¡c cá»§a chÃºng tÃ´i.</p>\r\n<p><span class="highlight">6.</span> ChÃºng tÃ´i sao lÆ°u dá»¯ liá»‡u hÃ ng tuáº§n, tuy nhiÃªn viá»‡c sao lÆ°u nÃ y chá»‰ phá»¥c vá»¥ cho má»¥c Ä‘Ã­ch quáº£n lÃ½ cá»§a chÃºng tÃ´i. Náº¿u khÃ¡ch hÃ ng yÃªu cáº§u cung cáº¥p, chÃºng tÃ´i sáº½ gá»­i nhá»¯ng báº£n sao lÆ°u nhÆ°ng chÃºng tÃ´i khÃ´ng chá»‹u trÃ¡ch nhiá»‡m vá» nhá»¯ng sá»± cá»‘ xáº£y ra cho cÃ¡c dá»¯ liá»‡u nÃ y. VÃ¬ váº­y, chÃºng tÃ´i khuyáº¿n cÃ¡o khÃ¡ch hÃ ng nÃªn sao lÆ°u dá»¯ liá»‡u thÆ°á»ng xuyÃªn vá» PC cá»§a mÃ¬nh Ä‘á»ƒ trÃ¡nh nhá»¯ng sá»± cá»‘ Ä‘Ã¡ng tiáº¿c xáº£y ra.</p>\r\n<p><span class="highlight">7.</span> Trong khi dÃ¹ng cÃ¡c biá»‡n phÃ¡p cÃ³ thá»ƒ vÃ  ná»— lá»±c nháº±m báº£o Ä‘áº£m sá»± toÃ n váº¹n vÃ  an toÃ n cho server, chÃºng tÃ´i khÃ´ng chá»‹u trÃ¡ch nhiá»‡m bá»“i thÆ°á»ng dá»¯ liá»‡u cá»§a bÃªn A lÆ°u trá»¯ táº¡i bÃªn B trong trÆ°á»ng há»£p há»‡ thá»‘ng server bá»‹ phÃ¡ hoáº¡i tá»« nhá»¯ng ngÆ°á»i dÃ¹ng báº¥t há»£p phÃ¡p, cÃ¡c hacker hoáº·c do cÃ¡c sá»± cá»‘ báº¥t kháº£ khÃ¡ng: thiÃªn tai, hoáº£ hoáº¡n, há»‡ thá»‘ng mÃ¡y chá»§ há»ng váº­t lÃ½ pháº§n cá»©ng.</p>\r\n<p><span class="highlight">8.</span> ChÃºng tÃ´i sáº½ táº¡m ngÆ°ng dá»‹ch vá»¥ mÃ  khÃ´ng cáº§n thÃ´ng bÃ¡o trÆ°á»›c vÃ¬ lÃ½ do vi pháº¡m tá»« phÃ­a ngÆ°á»i sá»­ dá»¥ng á»Ÿ cÃ¡c má»¥c Ä‘Ã£ nÃªu trÃªn. TrÆ°á»ng há»£p khÃ¡ch hÃ ng cam káº¿t vá»›i chÃºng tÃ´i khÃ´ng tÃ¡i vi pháº¡m quy Ä‘á»‹nh sá»­ dá»¥ng sáº½ pháº£i gá»­i CÃ´ng vÄƒn cam káº¿t vÃ  ná»™p khoáº£n phÃ­ khá»Ÿi táº¡o láº¡i dá»‹ch vá»¥ lÃ  180.000 vnÄ‘.</p>\r\n<p>Trong trÆ°á»ng há»£p xáº¥u nháº¥t, chÃºng tÃ´i sáº½ buá»™c pháº£i cáº¯t dá»‹ch vá»¥ vÄ©nh viá»…n mÃ  khÃ´ng hoÃ n tráº£ láº¡i phÃ­.</p>', 1),
(4, 'Quy Ä‘á»‹nh vá» tÃªn miá»n quá»‘c táº¿', '<p><span style="color: rgb(247, 89, 7);"><strong>Má»™t sá»‘ quy Ä‘á»‹nh hiá»‡n hÃ nh cá»§a phÃ¡p luáº­t liÃªn quan tá»›i viá»‡c Ä‘Äƒng kÃ½, sá»­ dá»¥ng tÃªn miá»n quá»‘c táº¿ táº¡i Viá»‡t Nam</strong></span></p>\r\n<ol>\r\n <li><strong>I. Luáº­t cÃ´ng nghá»‡ ThÃ´ng tin cá»§a Quá»‘c há»™i nÆ°á»›c Cá»™ng hÃ²a xÃ£ há»™i chá»§ nghÄ©a Viá»‡t Nam sá»‘ 67/2006/QH11 ngÃ y 29 thÃ¡ng 6 nÄƒm 2006</strong></li>\r\n</ol>\r\n<p>&nbsp;<strong>Äiá»u 23</strong>. Thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­</p>\r\n<p>&nbsp;1. Tá»• chá»©c, cÃ¡ nhÃ¢n cÃ³ quyá»n thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ theo quy Ä‘á»‹nh cá»§a phÃ¡p luáº­t vÃ  chá»‹u trÃ¡ch nhiá»‡m quáº£n lÃ½ ná»™i dung vÃ  hoáº¡t Ä‘á»™ng trang thÃ´ng tin Ä‘iá»‡n tá»­ cá»§a mÃ¬nh.</p>\r\n<p>2. Tá»• chá»©c, cÃ¡ nhÃ¢n sá»­ dá»¥ng tÃªn miá»n quá»‘c gia Viá»‡t Nam &ldquo;.vn&rdquo; khi thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ khÃ´ng cáº§n thÃ´ng bÃ¡o vá»›i Bá»™ BÆ°u chÃ­nh, Viá»…n thÃ´ng. Tá»• chá»©c, cÃ¡ nhÃ¢n khi thiáº¿t láº­p trang thÃ´ng tin Ä‘iá»‡n tá»­ khÃ´ng sá»­ dá»¥ng tÃªn miá»n quá»‘c gia Viá»‡t Nam &ldquo;.vn&rdquo; pháº£i thÃ´ng bÃ¡o trÃªn mÃ´i trÆ°á»ng máº¡ng táº¡i Ä‘á»‹a chá»‰: <a href="http://thongbaotenmien.vn/">http://thongbaotenmien.vn</a>&nbsp; vá»›i Bá»™ BÆ°u chÃ­nh, Viá»…n thÃ´ng nhá»¯ng thÃ´ng tin sau Ä‘Ã¢y:</p>\r\n<p>a) TÃªn tá»• chá»©c ghi trong quyáº¿t Ä‘á»‹nh thÃ nh láº­p, giáº¥y phÃ©p hoáº¡t Ä‘á»™ng, giáº¥y chá»©ng nháº­n Ä‘Äƒng kÃ½ kinh doanh hoáº·c giáº¥y phÃ©p má»Ÿ vÄƒn phÃ²ng Ä‘áº¡i diá»‡n; tÃªn cÃ¡ nhÃ¢n;</p>\r\n<p>b) Sá»‘, ngÃ y cáº¥p, nÆ¡i cáº¥p chá»©ng minh thÆ° nhÃ¢n dÃ¢n hoáº·c sá»‘, ngÃ y cáº¥p, nÆ¡i cáº¥p há»™ chiáº¿u cá»§a cÃ¡ nhÃ¢n;</p>\r\n<p>c) Äá»‹a chá»‰ trá»¥ sá»Ÿ chÃ­nh cá»§a tá»• chá»©c hoáº·c nÆ¡i thÆ°á»ng trÃº cá»§a cÃ¡ nhÃ¢n;</p>\r\n<p>d) Sá»‘ Ä‘iá»‡n thoáº¡i, sá»‘ fax, Ä‘á»‹a chá»‰ thÆ° Ä‘iá»‡n tá»­;</p>\r\n<p>Ä‘) CÃ¡c tÃªn miá»n Ä‘Ã£ Ä‘Äƒng kÃ½.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n<p>3. Tá»• chá»©c, cÃ¡ nhÃ¢n pháº£i chá»‹u trÃ¡ch nhiá»‡m trÆ°á»›c phÃ¡p luáº­t vá» tÃ­nh chÃ­nh xÃ¡c cá»§a cÃ¡c thÃ´ng tin quy Ä‘á»‹nh táº¡i khoáº£n 2 Äiá»u nÃ y, khi thay Ä‘á»•i thÃ´ng tin&nbsp; thÃ¬ pháº£i thÃ´ng bÃ¡o vá» sá»± thay Ä‘á»•i Ä‘Ã³.</p>\r\n<ol>\r\n <li><strong>II. NGHá»Š Äá»ŠNH Sá»‘: 28/2009/NÄ-CP </strong></li>\r\n</ol>\r\n<p><strong>Quy Ä‘á»‹nh xá»­ pháº¡t vi pháº¡m hÃ nh chÃ­nh trong quáº£n lÃ½, cung cáº¥p, sá»­ dá»¥ng dá»‹ch vá»¥ Internet vÃ  thÃ´ng tin Ä‘iá»‡n tá»­ trÃªn Internet</strong></p>\r\n<p><strong>Äiá»u 11. </strong>Vi pháº¡m cÃ¡c quy Ä‘á»‹nh vá» sá»­ dá»¥ng tÃªn miá»n Internet</p>\r\n<p>1. Pháº¡t tiá»n tá»« 2.000.000 Ä‘á»“ng Ä‘áº¿n 5.000.000 Ä‘á»“ng Ä‘á»‘i vá»›i hÃ nh vi sá»­ dá»¥ng tÃªn miá»n cáº¥p cao khÃ¡c tÃªn miá»n &quot;.vn&quot; mÃ  khÃ´ng thÃ´ng bÃ¡o hoáº·c thÃ´ng bÃ¡o thÃ´ng tin khÃ´ng chÃ­nh xÃ¡c hoáº·c thay Ä‘á»•i thÃ´ng tin mÃ  khÃ´ng thÃ´ng bÃ¡o&nbsp; Bá»™ ThÃ´ng tin vÃ  Truyá»n thÃ´ng theo quy Ä‘á»‹nh; khai bÃ¡o thÃ´ng tin khÃ´ng chÃ­nh xÃ¡c hoáº·c khÃ´ng cáº­p nháº­t khi cÃ³ thay Ä‘á»•i thÃ´ng tin vá» tÃªn, Ä‘á»‹a chá»‰ liÃªn há»‡ Ä‘á»‘i vá»›i tá»• chá»©c hoáº·c tÃªn, Ä‘á»‹a chá»‰ liÃªn há»‡, sá»‘ giáº¥y chá»©ng minh nhÃ¢n dÃ¢n hoáº·c sá»‘ há»™ chiáº¿u Ä‘á»‘i vá»›i cÃ¡ nhÃ¢n Ä‘Äƒng kÃ½, sá»­ dá»¥ng tÃªn miá»n &quot;.vn&quot;.</p>', 2);
