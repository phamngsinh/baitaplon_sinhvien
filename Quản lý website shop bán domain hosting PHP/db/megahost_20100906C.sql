-- phpMyAdmin SQL Dump
-- version 2.11.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 06, 2010 at 07:16 PM
-- Server version: 5.0.45
-- PHP Version: 5.2.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `megahost`
--

-- --------------------------------------------------------

--
-- Table structure for table `adm_members`
--

CREATE TABLE `adm_members` (
  `id` tinyint(3) NOT NULL auto_increment,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `login_key` varchar(32) NOT NULL,
  `groups` int(11) NOT NULL default '2',
  `name` varchar(50) NOT NULL,
  `identity` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `joined` int(11) NOT NULL,
  `last_activity` int(11) NOT NULL,
  `validate_code` varchar(6) NOT NULL,
  `active` tinyint(3) NOT NULL default '0',
  `last_visit` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `adm_members`
--

INSERT INTO `adm_members` (`id`, `username`, `password`, `login_key`, `groups`, `name`, `identity`, `email`, `address`, `telephone`, `joined`, `last_activity`, `validate_code`, `active`, `last_visit`) VALUES
(1, 'admin', 'qsnoweyMuNFQIyJzPRO2R2pKBspnwC', '9f6eg1jh7aejydlovnzoqzo24qijf5nm', 1, 'Do Ngoc Nhat Hoang', 0, 'admin@localhost.com', 'TPHCM', '906256258', 0, 1188152588, '', 1, 1283817461);

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `name` varchar(20) NOT NULL default '',
  `value` varchar(128) default NULL,
  PRIMARY KEY  (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`name`, `value`) VALUES
('AdminEmail', 'admin@localhost.com'),
('TimeZone', '25200'),
('timeout', '3600'),
('antiflood', '1200');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL auto_increment,
  `fullname` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `company` varchar(50) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `fax` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `senddate` int(11) NOT NULL,
  `isread` tinyint(3) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `fullname`, `address`, `company`, `phone`, `fax`, `email`, `title`, `content`, `senddate`, `isread`) VALUES
(2, 'Do Ngoc Nhat Hoang', '14A Chu Dong Tu, P7, TB', 'hoangcn02', '0938546342', '08 888738463', 'hoangcn02@yahoo.com', 'Demo title lien he hoangcn02', 'Noi dung lien he hoangcn02. demo noi dung lien he hoangcn02', 2147483647, 1),
(3, 'Le Thuy Minh Ngoc', '123A Nguyen Trong Tuye, P8, TB', 'hoangcn02', '0938543323', '08 88874363', 'minhngoc@yahoo.com', 'Demo title cua minh ngoc', 'Demo noi dung cua minh ngoc goi', 2147483647, 1);

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL auto_increment,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `question`, `answer`) VALUES
(1, 'Lam the nao de config domain va hosting chia se?', 'Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.'),
(2, 'Lam the nao de config hosting va domain chia se?', 'Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.Tra loi lam the nao de config domain va hosting chia se.');

-- --------------------------------------------------------

--
-- Table structure for table `homeinfos`
--

CREATE TABLE `homeinfos` (
  `content` text NOT NULL,
  `img` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `homeinfos`
--

INSERT INTO `homeinfos` (`content`, `img`) VALUES
('<p>Noi dung dich vu tieu bieu tai trang</p>', '692141_26392-b.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `name` varchar(255) NOT NULL,
  `icon` varchar(85) NOT NULL default '',
  `code` varchar(32) NOT NULL,
  `rank` tinyint(3) NOT NULL default '1',
  `active` tinyint(1) NOT NULL default '1',
  `isdefault` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`name`, `icon`, `code`, `rank`, `active`, `isdefault`) VALUES
('Viá»‡t Nam', 'vn.gif', 'vn', 3, 1, 1),
('English', 'en.gif', 'en', 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `intro` text NOT NULL,
  `content` text NOT NULL,
  `postdate` int(11) NOT NULL,
  `striking` tinyint(3) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `img`, `intro`, `content`, `postdate`, `striking`) VALUES
(1, 'TiÃªu Ä‘á» tin tá»©c má»›i', '747835_25789-b.jpg', '<p>Giá»›i thiá»‡u tin tá»©c má»›i. Giá»›i thiá»‡u tin tá»©c má»›i</p>', '<p>Noi dung dich vu tieu bieu tai trang chu. Atha ma ta nat ta ghi</p>\r\n<p>Noi dung dich vu tieu bieu tai trang chu. Atha ma ta nat ta ghi</p>', 1283514611, 1),
(2, 'TiÃªu Ä‘á» tin tá»©c má»›i', '624942_22512-b.jpg', '<p>Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i. Giá»›i thiá»‡u vá» tin tá»©c má»›i.</p>', '<p>Noi dung dich vu tieu bieu tai trang chu. Atha ma ta nat ta ghi</p>\r\n<p>Noi dung dich vu tieu bieu tai trang chu. Atha ma ta nat ta ghi</p>', 1283752939, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL auto_increment,
  `subject` tinyint(3) NOT NULL default '1',
  `fullname` varchar(50) NOT NULL,
  `company` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `fax` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `domain` varchar(30) NOT NULL,
  `service_type` int(11) NOT NULL,
  `service` int(11) NOT NULL,
  `service_plan` int(11) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `payment` int(11) NOT NULL default '1',
  `infos` text NOT NULL,
  `order_date` int(11) NOT NULL,
  `status` tinyint(3) NOT NULL default '0',
  `active` tinyint(3) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `orders`
--


-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL auto_increment,
  `type` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `rank` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `service`
--


-- --------------------------------------------------------

--
-- Table structure for table `service_intro`
--

CREATE TABLE `service_intro` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `intro` text NOT NULL,
  `content` text NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `rank` tinyint(3) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `service_intro`
--

INSERT INTO `service_intro` (`id`, `name`, `intro`, `content`, `thumb`, `img`, `rank`) VALUES
(2, 'TiÃªu Ä‘á» thÃ´ng tin giá»›i thiá»‡u dá»‹ch vá»¥', '<p>Giá»›i thiá»‡u vá» dá»‹ch vá»¥</p>', '<p>Ná»™i dung dá»‹ch vá»¥</p>', '276343_125x125-7.gif', '', 10);

-- --------------------------------------------------------

--
-- Table structure for table `service_plan`
--

CREATE TABLE `service_plan` (
  `id` int(11) NOT NULL auto_increment,
  `service_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `showhome` tinyint(3) NOT NULL default '0',
  `price` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `service_plan`
--


-- --------------------------------------------------------

--
-- Table structure for table `service_type`
--

CREATE TABLE `service_type` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `rank` tinyint(3) NOT NULL default '1',
  `active` tinyint(3) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `service_type`
--


-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` varchar(32) NOT NULL,
  `username` varchar(32) default NULL,
  `member_id` int(11) default '0',
  `groups_id` int(11) NOT NULL default '0',
  `security_code` varchar(32) NOT NULL,
  `ip_address` varchar(15) default NULL,
  `running_time` int(10) default NULL,
  `register_time` int(11) NOT NULL,
  `antiflood` int(10) NOT NULL default '0',
  `language` varchar(32) default NULL,
  `skin` varchar(32) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `username`, `member_id`, `groups_id`, `security_code`, `ip_address`, `running_time`, `register_time`, `antiflood`, `language`, `skin`) VALUES
('fc66abe2973285c7a0b652672c42d3d0', 'admin', 1, 1, '', '127.0.0.1', 1283743568, 0, 0, NULL, NULL),
('24654aed491ae167dd6e1cb9b9fdef97', 'admin', 1, 1, '', '127.0.0.1', 1283755613, 0, 0, NULL, NULL),
('c253173d916a413dd89e81cf191d3e78', 'admin', 1, 1, '', '127.0.0.1', 1283751714, 0, 0, NULL, NULL),
('161eee599f23856cbc611a0ed910da64', 'admin', 1, 1, '', '127.0.0.1', 1283771459, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `statistics`
--

CREATE TABLE `statistics` (
  `keyword` varchar(32) NOT NULL,
  `value` int(10) default NULL,
  PRIMARY KEY  (`keyword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `statistics`
--

INSERT INTO `statistics` (`keyword`, `value`) VALUES
('install_date', 1222876800),
('today_online', 4),
('last_update', 1283767055),
('most_online', 303),
('most_online_date', 1256922000),
('hitcounter', 39),
('online_now', 1);
