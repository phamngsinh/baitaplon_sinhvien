<?php
// CUSTOM PAGE
// The file allow user to download profile or document
ob_start();
define("HCR",true); // Check for security
if (empty($_GET['id']) || $_GET['id'] <= 0) exit('Invalid Access');

require_once 'configure.php';
mysql_connect("localhost",MYSQL_USERNAME,MYSQL_PASSWORD) or die('Can not connect db');
mysql_select_db(MYSQL_DATABASE) or die("Can not select DB");

$itemID = trim($_GET['id']);

if ($_GET['type'] == 'profile')
{
	$sql = 'SELECT * FROM profile WHERE id = '.$itemID;
}
else
{
	$sql = 'SELECT * FROM doc_down WHERE id = '.$itemID;
}

$result = mysql_query($sql);
$item = mysql_fetch_assoc($result);

if ($item['download_file'] == '') exit('No such file');


if ($_GET['type'] == 'profile')
{	
	$filename = 'images/profile/'.$item['download_file'];	
}
else
{
		$filename = 'images/document/'.$item['download_file'];	
}
	
	 // Set headers
	 /*
     header("Cache-Control: public");
     header("Content-Description: File Transfer");
     header("Content-Disposition: attachment; filename=$file");
    /* header("Content-Type: application/zip");*/
     //header("Content-Transfer-Encoding: binary");    
     // Read the file from disk
     //readfile($file);
	 
	$file_extension = strtolower(substr(strrchr($filename,"."),1)); 
	 switch ($file_extension)
	{
		  case "pdf": $ctype="application/pdf"; break;
		  case "exe": $ctype="application/octet-stream"; break;
		  case "zip": $ctype="application/zip"; break;
		  case "doc": $ctype="application/msword"; break;
		  case "xls": $ctype="application/vnd.ms-excel"; break;
		  case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
		  case "gif": $ctype="image/gif"; break;
		  case "png": $ctype="image/png"; break;
		  case "jpeg":
		  case "jpg": $ctype="image/jpg"; break;
		  default: $ctype="application/force-download";
	}
	header("Pragma: public"); // required
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private",false); // required for certain browsers 
	header("Content-Type: $ctype");
	header("Content-Disposition: attachment; filename=\"".basename($filename)."\";" );
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: ".filesize($filename));
	readfile("$filename");

ob_end_flush();

?>