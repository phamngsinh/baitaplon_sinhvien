<?php
/*----------------------------------------------------------
| CLASS FOR PAGING HANDLE
-----------------------------------------------------------*/
// Kiem tra bao mat
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

class hpaging
{
	
		/*------------------------------------------------------
		* PAGING FOLLOW SECTION - PAGE NUMBER WOULD BE IN URL
		* @Params :			
			+ $totalrows : total of rows
			+ $curpg : current page
			+ $maxpage : max page to be showed in a section
			+ $maxitem	: max item per page
			+ $url : url of page
		-----------------------------------------------------------*/
		function paging_section($total_rows,$curpg,$maxpage,$maxitem,$url)
		{
			global $token,$lang;
			// Some needed params
			$paging = "";
			$maxr = $maxitem;
			$maxp = $maxpage;
			// Total page
			$total_pages = ceil($total_rows/$maxr);
			// Get current page
			$cur_row = ($curpg-1)*$maxr + 1;
			// Prepair return paging string
			$paging  = $lang['current_page']."&nbsp;";
			$paging .= "<font color=red>".$curpg;
			$paging .= "</font>&nbsp;&nbsp;";
			$paging .= $lang['total_page']."&nbsp;";
			$paging .= "<font color=red>";
			$paging .= $total_pages."</font><br>";
			// Paging if there are more than one page
			if($total_rows>$maxr)
			{
				// Define start and end page
				// Ex 1->5,or 6->10,or 20->22
				$start   = 1;
				$end     = 1;
				$paging1 = "";
			
				for( $i=1; $i<=$total_pages; $i++)
				{
					if( ( $i > ( (int)(($curpg-1)/$maxp) ) * $maxp )&&(  $i <= ( (int)(($curpg-1)/$maxp+1) ) * $maxp ) )
					{
						if($start==1) $start = $i;
						if($i==$curpg)
						 $paging1 .="<font color='#0000ff'>[".$i."]</font>&nbsp;";
						else
						{
							$paging1 .= "<a  href=\"".$url."&page=".$i."\" class='style10'>[".$i."]</a>&nbsp;";
						} 
						$end = $i;
					}
				}
				
				if($curpg>$maxp && $curpg>1)
				{   $paging .= '<a href="'.$url.'&page=1" class="style10">'.$lang['first'].'</a>&nbsp;';
				}
				if($curpg>$maxp)
				{   
					$paging .= '<a href="'.$url.'&page='.($start-1).'" class="style10">'.$lang['previous'].'</a>&nbsp;';
				}
				$paging .= $paging1;
								
				if((((int)(($curpg-1)/$maxp)+1)*$maxp)<$total_pages)
				{   $paging .= '<a href="'.$url.'&page='.($end+1).'" class="style10">'.$lang['next'].'</a>&nbsp;';
				}
				if(($curpg<$total_pages) && ((((int)(($curpg-1)/$maxp)+1)*$maxp)<$total_pages))
				{
				$paging .= '<a href="'.$url.'&page='.$total_pages.'" class="style10">'.$lang['end'].'</a>&nbsp;';
				}
								
			}
			return $paging;
			
		}
		
		
		/*------------------------------------------------------------------------
		* PAGING IN CASE THERE'RE SELECT LIST FOR FILTER. IN THIS CASE WE CAN'T PAGING BY 
		* USING URL. WE MUST USE POST WITH JAVASCRIPT
		* @Params :
				+ $total_rows : total of row
				+ $curpg : current page
				+ $maxpage : max page per segment
				+ $maxitem : max item per page
		---------------------------------------------------------------------------------*/			
		function paging_section_h($total_rows,$curpg,$maxpage=5,$maxitem=10)
		{
			global $token,$lang;
			$paging = "";
			$maxr = $maxitem;
			$maxp = $maxpage;
			
			$total_pages = ceil($total_rows/$maxr);
			$cur_row = ($curpg-1)*$maxr + 1;
			
			$paging  = $lang['current_page']."&nbsp;";
			$paging .= "<font color=red>".$curpg;
			$paging .= "</font>&nbsp;&nbsp;";
			$paging .= $lang['total_page']."&nbsp;";
			$paging .= "<font color=red>";
			$paging .= $total_pages."</font><br>";
			
			if($total_rows>$maxr)
			{
			
				$start   = 1;
				$end     = 1;
				$paging1 = "";
			
				for( $i=1; $i<=$total_pages; $i++)
				{
					if( ( $i > ( (int)(($curpg-1)/$maxp) ) * $maxp )&&(  $i <= ( (int)(($curpg-1)/$maxp+1) ) * $maxp ) )
					{
						if($start==1) $start = $i;
						if($i==$curpg)
						 $paging1 .="<font color='#0000ff'>[".$i."]</font>&nbsp;";
						else
						{
							$paging1 .= "<a  href='javascript:GotoPage(".$i.");' class='style10'>[".$i."]</a>&nbsp;";
						} 
						$end = $i;
					}
				}
				
				if($curpg>$maxp && $curpg>1)
				{   $paging .= '<a href="javascript:GotoPage(1);" class="style10">'.$lang['first'].'</a>&nbsp;';
				}
				if($curpg>$maxp)
				{   
					$paging .= '<a href="javascript:GotoPage('.($start-1).');" class="style10">'.$lang['previous'].'</a>&nbsp;';
				}
				$paging .= $paging1;
								
				if((((int)(($curpg-1)/$maxp)+1)*$maxp)<$total_pages)
				{  
				  $paging .= '<a href="javascript:GotoPage('.($end+1).');" class="style10">'.$lang['next'].'</a>&nbsp;';
				}
				if(($curpg<$total_pages) && ((((int)(($curpg-1)/$maxp)+1)*$maxp)<$total_pages))
				{
				$paging .= '<a href="javascript:GotoPage('.$total_pages.');" class="style10">'.$lang['end'].'</a>&nbsp;';
				}
								
			}
			return $paging;
			
		}	
		
		
		
		
		
		
		////////////////////////////////////////////////////////////////
		// !! SPECIFIC PAGINATION FUNCTION THAT IS USED ON FRONTPAGE
		///////////////////////////////////////////////////////////////
		
		
		////////////////////////////////////////////////////////////////
		// !! SPECIFIC PAGINATION FUNCTION THAT IS USED ON FRONTPAGE
		///////////////////////////////////////////////////////////////

		
		/*------------------------------------------------------------------------
		* PAGING IN CASE THERE'RE SELECT LIST FOR FILTER. IN THIS CASE WE CAN'T PAGING BY 
		* USING URL. WE MUST USE POST WITH JAVASCRIPT
		* @Params :
					+ $total_rows : total of row
					+ $curpg : current page
					+ $maxpage : max page per segment
					+ $maxitem : max item per page
					+ $class : css for tag a
		---------------------------------------------------------------------------------*/			
		function paging_front($total_rows, $curpg, $maxpage=5, $maxitem=10, $url)
		{
			global $token, $lang;
			$paging = "";
			$maxr = $maxitem;
			$maxp = $maxpage;
			
			$total_pages = ceil($total_rows/$maxr);
			$cur_row = ($curpg-1)*$maxr + 1;
			
			$paging = '<div class="dpaging">';
			
			if ($total_rows>$maxr)
			{
			
				$start   = 1;
				$end     = 1;
				$paging1 = "";
			
				for ($i=1; $i<=$total_pages; $i++)
				{
					if ( ( $i > ( (int)(($curpg-1)/$maxp) ) * $maxp )&&(  $i <= ( (int)(($curpg-1)/$maxp+1) ) * $maxp ) )
					{						
						if ($start==1){ $start = $i; }
						if ($i==$curpg)
						{
						 	$paging1 .= '<a href="#" class="paging_btn_a">'.$i.'</a>';
						}
						else
						{
							$paging1 .= '<a  href="'.$url.'&page='.$i.'" class="paging_btn_a">'.$i.'</a>';
							
						} 
						$end = $i;
					}
				}
				
				if (($curpg > $maxp) && ($curpg > 1))
				{   
					$paging .= '<a href="'.$url.'&page=1" class="paging_btn_a">&lt;&lt;</a>';
				}
				
				if ($curpg>$maxp)
				{   
					$paging .= '<a href="'.$url.'&page='.($start-1).'" class="paging_btn_a">&lt;</a>';
				}
				$paging .= ''.$paging1;
								
				if ((((int)(($curpg-1)/$maxp)+1)*$maxp)<$total_pages)
				{  
				  $paging .= '<a href="'.$url.'&page='.($end+1).'" class="paging_btn_a">&gt;</a>';
				}
				
				if (($curpg<$total_pages) && ((((int)(($curpg-1)/$maxp)+1)*$maxp)<$total_pages))
				{
					$paging .= '<a href="'.$url.'&page='.$total_pages.'" class="paging_btn_a">&gt;&gt;</a>';
				}
								
			}
			
			$paging .= "</div>";
			return $paging;
			
		}
		
		
		// Paging associates with javascript
		function paging_frontp($total_rows, $curpg, $maxpage=5, $maxitem=10)
		{
			global $token, $lang;
			$paging = "";
			$maxr = $maxitem;
			$maxp = $maxpage;
			
			$total_pages = ceil($total_rows/$maxr);
			$cur_row = ($curpg-1)*$maxr + 1;
			
			$paging = '<div class="dpaging">';
			
			if ($total_rows>$maxr)
			{
			
				$start   = 1;
				$end     = 1;
				$paging1 = "";
			
				for ($i=1; $i<=$total_pages; $i++)
				{
					if ( ( $i > ( (int)(($curpg-1)/$maxp) ) * $maxp )&&(  $i <= ( (int)(($curpg-1)/$maxp+1) ) * $maxp ) )
					{
						if ($start==1) $start = $i;
						if ($i==$curpg)
						{
						 	$paging1 .= '<a href="#" class="paging_btn_a">'.$i.'</a>';
						}
						else
						{
							$paging1 .= '<a  href="javascript:GotoPage('.$i.');" class="paging_btn_a">'.$i.'</a>';
						} 
						$end = $i;
					}
				}
				
				if (($curpg > $maxp) && ($curpg > 1))
				{   
					$paging .= '<a href="javascript:GotoPage(1);" class="paging_btn_a">&lt;&lt;</a>';
				}
				
				if ($curpg>$maxp)
				{   
					$paging .= '<a href="javascript:GotoPage('.($start-1).');" class="paging_btn_a">&lt;</a>';
				}
				$paging .= $paging1;
								
				if ((((int)(($curpg-1)/$maxp)+1)*$maxp)<$total_pages)
				{  
				  $paging .= '<a href="javascript:GotoPage('.($end+1).');" class="paging_btn_a">&gt;</a>';
				}
				
				if (($curpg<$total_pages) && ((((int)(($curpg-1)/$maxp)+1)*$maxp)<$total_pages))
				{
					$paging .= '<a href="javascript:GotoPage('.$total_pages.');" class="paging_btn_a">&gt;&gt;</a>';
				}
								
			}
			return $paging;
			
		}			
		
		
}


?>