<?php
/*-------------------------------------
| CLASS FOR SESSION HANDLE
---------------------------------------*/
	
// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}


class session
{
	private $timeout = 1500; // 25 minutes - time for session exprires
	public $now   = 0;		 // time current
	private $ip_address = 0; // IP
	public $sess_id = "";    // Id of session
	public $username = "";   // Username
	public $member_id = 0;   // Id of member
	public $groups_id = 0;   // Id of group
	
	private $statistics_hitcounter = 0;     // Click counter
	private $statistics_installdate = 0;    // install date
	private $statistics_todayonline = 0;    // today online
	private $statistics_lastupdate = 0;     // Last update
	private $statistics_mostonline = 0;     // Most online
	private $statistics_mostonlinedate = 0; // Most online day
	
	private $antiflood = 0;
	public $language = "";      // Language
	private $skin = "";         // Skin
	public $security_code = ""; // Security code
	
	/*-----------------------------
	| CONSTRUCT 
	-------------------------------*/
	function session()
	{
		global $db, $time;
		
		
		session_start();
		header("Cache-control: "); // Fix bug in IE6
		
		$this->ip_address = $_SERVER['REMOTE_ADDR'];		
		$this->now = time();		
		$this->sess_id = session_id();		
		$config = array();
		$query = $db->query("SELECT * FROM  config");
		while ( $row = @mysql_fetch_array($query)){ $config[$row['name']] = $row['value'] ;}
		$this->timeout = $config['timeout'];
		$this->antiflood = $config['antiflood']; 
		
				
		//  Delete all session have IP concur with new session
		$db->do_delete("session", "ip_address<>'".$this->ip_address."' AND id='".$this->sess_id."'");
		
		// Delete all session of yesterday to back
		$yesterday = time() - 86400;
		$db->do_delete("session", "running_time < ".$yesterday);
				
		
		$query = $db->simple_select("*", "statistics");
		$result = $db->query($query);
		$tmp = array();
		
		while ($row = $db->fetch_array($result)) $tmp[$row['keyword']] = $row['value'];
		
		$this->statistics_installdate = $tmp['install_date'];
		$this->statistics_todayonline = $tmp['today_online'];
		$this->statistics_lastupdate = $tmp['last_update'];
		$this->statistics_mostonline = $tmp['most_online'];
		$this->statistics_mostonlinedate = $tmp['most_online_date'];
		$this->statistics_hitcounter = intval($tmp['hitcounter']);
		
		// Check if this session had existed
		$query = $db->simple_select("*", "session", "ip_address='".$this->ip_address."' AND id='".$this->sess_id."'");
		$result = $db->query($query);
		$row = $db->fetch_array($result);
		
		// Session exists
		if ($row['id'])
		{
			// Check for timeout
			if ($this->now - $row['running_time'] > $this->timeout)
			{
				// Destroy timeouted session
				$this->destroy();
				
				// Create new session
				$this->session();
			}
			
			
			$arr = array( 'running_time' => $this->now );
			$db->do_update("session", $arr, "id='".$row['id']."'");
			
			// Update infos
			$this->username = $row['username'];
			$this->member_id = $row['member_id'];
			$this->groups_id = $row['groups_id'];
			$this->antiflood = $row['antiflood'];
			$this->language = $row['language'];
			$this->skin = $row['skin'];
			$this->security_code = $row['security_code'];
		}
		
		// Insert new row to session table
		else
		{	
			
			$arr = array( 
							'id'  => $this->sess_id,
					  	    'ip_address' => $this->ip_address,
						    'running_time' => $this->now,
						);
			$db->do_insert("session", $arr);
			
			// Update infos install day of website
			if (!$this->statistics_installdate)
			{
				$arr = array( 'value'  => $this->now );
				$where = "install_date";
				$db->do_update("statistics", $arr, "keyword='install_date'");
			}
			
			// Update today online
			if ( $time->unixtime_2_ddmmyyyy($this->statistics_lastupdate) != $time->unixtime_2_ddmmyyyy($this->now) )
			{
				$arr = array( 'value' => 1 );
			} else
			{
				$arr = array( 'value' => $this->statistics_todayonline + 1 );
			}
			$db->do_update("statistics", $arr, "keyword='today_online'");
			
			// Update last update
			$arr = array( 'value'  => $this->now );
			$db->do_update("statistics", $arr, "keyword='last_update'");
			
			// Update most online
			if ( $this->statistics_todayonline > $this->statistics_mostonline )
			{
				$arr = array( 'value'  => $this->statistics_todayonline );
				$db->do_update("statistics", $arr, "keyword='most_online'");
				
				$arr = array( 'value'  => $this->now );
				$db->do_update("statistics", $arr, "keyword='most_online_date'");
			}
			
			// Update hitcounter
			$arr = array( 'value'  => $this->statistics_hitcounter + 1 );
			$db->do_update("statistics", $arr, "keyword='hitcounter'");
			
			// Update online users - in 15'
			$sql_online = $db-> simple_select( "COUNT(*) AS user_online" , "session" , "running_time >= ". ($this->now - 900) );
			$query_online = $db->query($sql_online);
			$row_online = @mysql_fetch_array($query_online);
			$arr = array( 'value' => $row_online['user_online'] );
			$db->do_update("statistics", $arr, "keyword='online_now'");
		}
	}
	

	/*------------------------------
	| DESTROY SESSION
	--------------------------------*/
	function destroy()
	{
		global $db;
		
		// Delete Session ID
		session_destroy();
		
		// Delete session in DB
		$db->do_delete("session", "id='".$this->sess_id."'");
		
		
		$this->ip_address = 0;
		$this->now = 0;
		$this->sess_id = "";
		$this->username = "";
		$this->member_id = 0;
		$this->groups_id = 0;
		$this->statistics_hitcounter = 0;
		$this->statistics_installdate = 0;
		$this->statistics_todayonline = 0;
		$this->statistics_lastupdate = 0;
		$this->statistics_mostonline = 0;
		$this->statistics_mostonlinedate = 0;
	}
	
	
	/*-------------------------------------------------
	| UPDATE LANGUAGE IN CASE IT HAD BEEN CHANGED
	--------------------------------------------------*/
	function isLanguage($old, $new)
	{
		global $db;
		
		if ($old!=$new)
		{
			
			$arr = array( 'language'  => $new );
			$db->do_update("session", $arr, "id='".$this->sess_id."'");
			return false;
		}
		
		return true;
	}
}
?>