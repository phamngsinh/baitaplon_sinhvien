<?php
/*---------------------------------------
|	CLASS FOR STRING HANDLE
------------------------------------------*/

// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

class str
{
	
	/*------------------------------
	| CHECK FOR INPUT TEXT
	+-------------------------------*/
	function input($string)
	{
    	if (is_string($string))
		{
			return trim($this->sanitize_string(stripslashes($string)));
		} elseif (is_array($string))
		{
			reset($string);
			//while (list($key, $value) = each($string))
			//{
        		//$string[$key] = $this->input($value);
      		//}
			foreach ($string as $key => $value)
			{
				$string[$key] = $this->input($value);
			}
			return $string;
		} else
		{
			return $string;
		}
	}
	
	
	function sanitize_string($string)
	{
		$string = ereg_replace(' +', ' ', trim($string));

		$find = array("'", "<", ">", "\\", "[", "]", "--");
		
		return str_replace($find, "_", $string);
	}
	
	/*------------------------------------------------------------------------
	| CHECK FOR INPUT TEXT IN CASE USER ENTER HTML CODE
	+------------------------------------------------------------------------*/
	function input_html($string)
	{
    	if (is_string($string))
		{
			return trim($this->sanitize_html(stripslashes($string)));
		} elseif (is_array($string))
		{
			reset($string);
			while (list($key, $value) = each($string))
			{
        		$string[$key] = $this->input_html($value);
      		}
			return $string;
		} else
		{
			return $string;
		}
	}
	
	
	function sanitize_html($string)
	{
		$string = ereg_replace(' +', ' ', trim($string));

		$find = array("'", "\\", "--");
		
		return str_replace($find, "_", $string);
	}


	/*------------------------------------------------
	| REDIRECT USER TO ANOTHER PAGE IN SITE
	+------------------------------------------------*/
	function redirect($filename="")
	{	
		if ( (strstr($filename, "\n") != false) || (strstr($filename, "\r") != false) )
		{ 
			$this->redirect("index.php");
		}
		
		$host  = $_SERVER['HTTP_HOST'];
		$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
		
		header("Location: http://$host$uri/$filename");
		exit;
	}
	
	
	/*----------------------------------------------
	| REDIRECT TO ANY PAGE
	+-----------------------------------------------*/
	function goto_url($url="www.lory.vn")
	{	
		header("Location: ".$url);
		exit;
	}
	
	
	/*-----------------------------------------
	| GET SUBSTRING IN A STRING
	+-----------------------------------------*/
	function subword($text, $limit=50)
	{
	  $word = explode(" ", $text);
			
	  $text_out = "";
	  for ($i=0; $i < $limit; $i++) $text_out .= $word[$i]." ";	
	  return $text_out;
	}

	
	/*-----------------------------------------------------
	* GET ALL SUBSTRINGS OF A STRING
	* @Param: 
			  + $key_search : input keyword
	* @Return : array of all substring
	----------------------------------------------------------*/
	function zen_key_search($key_search="")
	{
		   $str  = $key_search;
		   
		   // Implode to an array
		   $arry = split(" ",$str);
		   
		   //$arry_temp1 use to reverse string
		   $arry_temp1 = $arry;	
				
		   // Number of words
		   $numphase = count($arry);
		   
		   
			for( $i = 2 ; $i <= $numphase ; $i++  )
			{   
				$arry_temp = array();
				
				for($j = 0 ; $j < $numphase-($i-1) ; $j++)
				{
							
					for($k = $j + ($i-1) ; $k < $numphase ; $k++)
					{
								
						$tempstr = '';
						for($m = $j ; $m < $j + ($i-1) ; $m++)
						{
							$tempstr .= $arry[$m].' ';  
						}
						
						$arry_temp[] =  $tempstr.$arry[$k];
					}	
							
				}
				 
				
				for($l=(count($arry_temp));$l>0;$l--)
				{
					$arry[] = $arry_temp[$l-1];
				}	
					
			 }
					
				
				for($l=(count($arry_temp1));$l>0;$l--)
				{
					$arry[count($arry_temp1)-$l] = $arry_temp1[$l-1];
				}		
				return $arry;
	}//End function zen_key_search		
	
	
	/*---------------------------------------------------
	* CHECK FOR VALID OF URL
	* @Param : input url
	* @Return : true if url is valid, else return false				
	-----------------------------------------------------*/
	function valid_url($str)
	{
		return ( ! preg_match('/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i', $str)) ? FALSE : TRUE;
	}
	
	
}


?>