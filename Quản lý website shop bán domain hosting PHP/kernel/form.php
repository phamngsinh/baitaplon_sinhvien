<?php
/*-------------------------------------------
| CLASS FOR ELEMENTS OF FORM
+--------------------------------------------*/
// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

class form
{
	private $error = false;
	
	/*--------------------------------------------
		| DRAW A FORM 
	+---------------------------------------------*/
	function draw_form($action, $class="", $enctype=2, $method="POST", $name="aForm", $script="")
	{
		$text = "<form class='".$class."' action='".$action."' method='".$method."' ".$script;
		$text .= ($name) ? " name='".$name."'" : "";
		switch ($enctype)
		{
			case 1:
				$text .= " enctype='application/x-www-form-urlencoded'";
				break;
			case 2:
				$text .= " enctype='multipart/form-data'";
				break;
			case 3:
				$text .= " enctype='text/plain'";
				break;
		}
		$text .= ">";
		
		return $text;
	}
	
	
	/*-----------------------------
	| HIDDEN INPUT
	+-----------------------------*/
	function draw_hidden($name, $value, $id='')
	{
		return "<input type='hidden' name='".$name."' value='".$value."' id='".$id."' >";
	}
	
	
	/*-----------------------
	| DRAW SELECT LIST
	+------------------------*/
	function draw_select($name, $option, $class='', $js_event='')
	{
		$text =  "<select name='".$name."' class='".$class."' ". ($js_event ? $js_event : "") ." >\n";
	  	$text .=  $option;
	  	$text .=  "</select>\n";
		
		return $text;
	}
	
	
	/*----------------------
	| DRAW OPTION
	+-----------------------*/
	function draw_option($value, $text, $checked=false, $class="")
	{
		$new_text = "<option value='".$value."' class='".$class."' ";
		
		if ($checked == true) $new_text .= "selected";
		
		$new_text .= ">".$text."</option>\n";
		
		return $new_text;
	}
	
	
	/*-------------------------
	| DRAW INPUT TEXT
	+--------------------------*/
	function draw_textfield($name, $value="", $class="text", $size="", $maxlength="", $accesskey="", $script="")
	{
		return "<input type='text' name='".$name."' value='".$value."' class='".$class."' size='".$size."' maxlength='".$maxlength."' accesskey='".$accesskey."' ".$script.">";
	}
	
	
	/*---------------------------------------
	| DRAW INPUT TEXTAREA
	+----------------------------------------*/
	function draw_textarea($name, $value="", $cols="40", $rows="7", $class="text", $wrap="virtual", $id='')
	{
		return "<textarea name='".$name."'  ". ( $id ? "id='". $id ."'" : "" ) ." class='".$class."' rows='".$rows."' cols='".$cols."' wrap='".$wrap."'>".$value."</textarea>"; 
	}
	
	
	
	/*----------------------------------------
	| DRAW PASSWORD FIELD
	+-----------------------------------------*/
	function draw_password($name, $class="password", $size="", $maxlength="")
	{
		return "<input type='password' name='".$name."' class='".$class."' size='".$size."' maxlength='".$maxlength."'>";
	}
	
	
	/*-----------------------
	| DRAW RADIO BUTTON
	+------------------------*/
	function draw_radio($name, $value, $text, $checked="", $class="")
	{
		return "<input type='radio' name='".$name."' value='".$value."' class='".$class."' ".$checked.">".$text;
	}
	
	
	/*----------------------
	| DRAW CHECK BOX
	+-----------------------*/
	function draw_checkbox($name, $value, $text="", $checked="", $class="")
	{
		return "<input type='checkbox' name='".$name."' value='".$value."' class='".$class."' ".$checked.">".$text;
	}
	
	
	/*--------------------------------
	| DRAW FILE UPLOAD FIELD
	+---------------------------------*/
	function draw_file($name, $class="")
	{
		return "<input type='file' name='".$name."' class='".$class."'>";
	}
	

	
	/*------------------------------
	| DRAW SUBMIT BUTTON
	+-------------------------------*/
	function draw_submit($value, $class='', $name='')
	{
		return "<input type='submit' value='".$value."' class='".$class."' name='".$name."'>";
	}
	
	/*---------------------
	| DRAW BUTTON
	+----------------------*/
	function draw_button($value, $class='', $name='' , $onclick='')
	{
		return "<input type='button' value='".$value."' class='".$class."' name='".$name."' onclick='". $onclick ."'>";
	}
	
	/*----------------------------
	| DRAW BUTTON RESET
	+-----------------------------*/
	function draw_reset($value, $class='', $name='')
	{
		return "<input type='reset' value='".$value."' class='".$class."' name='".$name."'>";
	}
	
	
	/*------------------------------
	| DRAW FLASH OBJECT
	+-------------------------------*/
	function draw_flash($src ,$width='300' ,$height='300')
	{ 
	    $flash_ob = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' 		             codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0' width='". $width ."' height='". $height ."'>
					  <param name='movie' value='". $src ."'>
					  <param name='quality' value='high'>
					  <param name='wmode' value='transparent'>
					  <embed src='". $src ."' quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwave-flash' wmode='transparent' width='". $width ."' height='". $height ."'></embed>
					</object>";
		return $flash_ob;
		
	}
	
	
	/*-------------------------------------------
	| DRAW WINDOW MEDIA OBJECT
	+--------------------------------------------*/
	function draw_winmedia($src ,$width='300', $height='50' , $control='false')
	 {
		$media_object = "<OBJECT ID='WinMedia' width='". $width ."' height='". $height ."' classid='CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95'
codebase='http://activex.microsoft.com/activex/controls/
mplayer/en/nsmp2inf.cab#Version=5,1,52,701' standby=
'Loading Microsoft� Windows� Media Player components...'
type='application/x-oleobject'>
			<PARAM NAME='FileName' VALUE='". $src ."'>
			<PARAM NAME='AutoStart' Value='True'>
			<PARAM NAME='ShowControls' VALUE='". $control ."'>
			<PARAM NAME='ShowStatusBar' VALUE='True'>
			<EMBED type='application/x-mplayer2' pluginspage='http://www.microsoft.com/Windows/MediaPlayer/' SRC='". $src ."' name='WinMedia' autostart=1 width='". $width ."' height='". $height ."' ShowStatusBar=true
	ShowControls=false>
			</EMBED>
		</OBJECT>";	 
	 		return $media_object;
	   }	
	   
	   
	/*-----------------------------
	| DRAW DYNAMIC SELECT LIST 
	+-----------------------------*/
	function draw_select_dynamic($SQLStr,$CboBoxName,$SelectValue='',$OnchangeFunction='',$all='0',$cut='',$word='20')
	{
		global $lang, $str ;
		$returnStr = '';
		$returnStr = '<select  name="'. $CboBoxName .'" size="1" onchange="'. $OnchangeFunction .'">';
		
		$query = mysql_query($SQLStr);
		if ( $all == 1 )
		{
        	$returnStr = $returnStr .$this->draw_option('0','All',true);
		}
		while ( $row = mysql_fetch_row($query) )
		{
		    if ( $cut=='true' )
			{
				 $row1 = $str->subword($row['1'],$word) ."..."; 
			}
			else
			{
				 $row1 = $row[1] ; 
			}
			if ( $SelectValue == $row[0] )
			{
				$returnStr = $returnStr .$this->draw_option($row[0],$row[1],true);
			}
			else
			{
				$returnStr = $returnStr .$this->draw_option($row[0],$row[1]);
			}
		}
		$returnStr = $returnStr .'</select>';
		return $returnStr;
	}	
	
	
	/*----------------------
	|	DRAW IMAGE
	+-----------------------*/
	function draw_img($src , $alt='', $width='150' , $height='150' , $js='', $class='' , $border='0' , $hspace='0' , $vspace='0' )
	{
	  return "<img src='". $src ."' alt='". $alt ."' width='". $width ."' height='". $height ."' class='". $class ."' border='". $border ."' hspace='". $hspace ."' vspace='". $vspace ."' ". $js .">";
	   }


	/*---------------------------------
	| CHECK FOR LENTH OF INPUT TEXT
	+----------------------------------*/
	function check_input($text, $field_size)
	{
		if ( !$text || strlen($text) < $field_size)  return false;
		
		return true;
	}
	
	
	/*----------------------------------------------------------------------
	| CHECK THE VALID OF USERNAME
	+-----------------------------------------------------------------------*/
	function check_username($text, $field_sizemin=6, $field_sizemax=32)
	{
		if ( !$text || strlen($text) < $field_sizemin || strlen($text) > $field_sizemax ) return false;
		
		// Username khong chua nhung ky tu nay
		$invalidChars = array("/",";",",",":","*","&","^","%","$","#","!","`","~","(",")","-","+","=","|","\\","'","\"","<",">","?","[","]","{","}","@",".");
		$text_arr = str_split($text);
		  
		$text_intersect = array_intersect($invalidChars, $text_arr);
		  
		if ( count($text_intersect) ) return false;
		
		return true;
	}
	
	
	
	/*--------------------------------------------------
	| CHECK FOR THE VALID OF EMAIL
	+---------------------------------------------------*/
	function check_email($email)
	{
		if ( !$email || strlen($email) < 8) return false;
		
		// Email khong duoc chua nhung ky tu trong mang nay
		$invalidChars = array("/",";",",",":","*","&","^","%","$","#","!","`","~","(",")","-","+","=","|","\\","'","\"","<",">","?","[","]","{","}");
		$text_arr = str_split($email);
		  
		$text_intersect = array_intersect($invalidChars, $text_arr);
		  
		if (count($text_intersect)) return false;
		  
		$email_array = explode("@", $email);
					
		$atCount = count($email_array);
		if ( ($atCount < 2) || ($atCount > 2) ) return false;
		
		if (strlen($email_array[0]) < 2) return false;
					
		$ext_email_array = explode(".", $email_array[1]);
					
		foreach ($ext_email_array as $v)
		{
			if (strlen($v) < 2) return false;
		}
		
		return true;
	}
	
	
	/*----------------------------------------------
	| CHECK IF THE RADIO HAD BEEN CHECKED
	+-----------------------------------------------*/
	function check_radio($text)
	{
		if ( !$text ) return false;
		
		return true;
	}
	
	
	/*---------------------------------------------------------------
	| CHECK A MENU HAD BEEN CHECK
	+----------------------------------------------------------------*/
	function check_select($text)
	{
		if ( !$text ) return false;
		return true;
	}
	
	
	/*-----------------------------------------------------------
	| CHECK FOR THE VALID PASSWORD
	+-------------------------------------------------------------*/
	function check_password($password, $field_sizemin=6, $field_sizemax=32)
	{
		if ( !$password || strlen($password) < $field_sizemin || strlen($pasword) > $field_sizemax ) return false;
		
		//----------Password khong duoc chua cac ky tu nay---------------//
		$invalidChars = array("/",";",",",":","*","&","^","%","$","#","!","`","~","(",")","-","+","=","|","\\","'","\"","<",">","?","[","]","{","}",".","@","_");
		  
		$text_arr = str_split($password);
		  
		$text_intersect = array_intersect($invalidChars, $text_arr);
		  
		if (count($text_intersect)) return false;
			
		return true;
	}
	
	
	/*--------------------------------------------------
	| CHECK IF PASSWORD AND CONFIRM MATCH OR NOT  
	----------------------------------------------------*/
	function check_password_confirm($password, $confirm, $field_sizemin, $field_sizemax)
	{
		if ( !$this->check_password($password, $field_sizemin, $field_sizemax) ) return false;
		
		if ($password != $confirm) return false;
		
		return true;
	}
	
	
	/*---------------------------------------------------
	* Get back url - this should be used in case 
	* we need to header back to previous url
	-----------------------------------------------------*/
	function backURL()
	{
		$curl_arr = explode('/',$_SERVER['PHP_SELF']);
		$new_url = URL.implode('/',array_slice($curl_arr,2));
		return $new_url;

	}
		
	

}

?>
