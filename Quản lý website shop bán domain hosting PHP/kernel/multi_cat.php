<?php
/*---------------------------------------------
| SHOW LIST OF MULTI CAT BY TREE
-------------------------------------------------*/
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}


class multi_cat
{  
	 public $content = '';
     public $content1 = '';
	 public $content2 = '';
	 public $content3 = '';
	 public $content4 = '';
   
    function __multi_cat($content = '',$content1 = '',$content2 = '',$content3 = '',$content4 = '')
    {  
	    $this->content = $content;
	    $this->content1 = $content1;
	    $this->content2 = $content2;
	    $this->content3 = $content3;
	    $this->content4 = $content4;
	 
	 }
	   
	   
	 /*-------------------------------------------------------------------
	 | SHOW LIST OF MULTI CAT FOLLOW FREE - NO DISTINGUISH LANGUAGE
	 | @Params :
	 			$cur_parent: id of parent of current cat
				$parent_id: id of parent in case call recursive
				$insert_text: symbol to differ from cat and its parent

     -------------------------------------------------------------------------*/	
	
	function show_categories($cur_parent, $parent_id = "0", $insert_text = "") 
	{  
		global $db; 
		
		if (! $categories = mysql_query("SELECT * FROM mqo_news_category WHERE parent=". $parent_id ." ORDER BY parent"))
		{ 
			die ("cannot query"); 
		} 
	    
		while ($category = mysql_fetch_array($categories, MYSQL_ASSOC)) 
 		{   
		        $selected = $category['id'] == $cur_parent ? 'selected' : '';
     			$this->content .= "<option value='". $category["id"] ."' ". $selected ." >". $insert_text.$category["name"]. "</option>"; 
     			$this->show_categories($cur_parent,$category["id"],$insert_text."---"); 
  		} 
		return true; 

 	 } // end function
		 
		 
		 
	/*-----------------------------------------------------------------------
	* LIST OF MULTI CAT IN CASE EDITING- MEANS THAT THE EDITED CAT AND ITS CHILD DO NOT SHOWED	 
	*	$cur_parent: id of parent of current cat
	*	$where_clause: id of editting cat
	*	$parent_id: id of parent in case recursive calling
	*	$insert_text: symbol to differ each cat
	----------------------------------------------------------------------------*/
	function show_categories1($cur_parent = '', $where_clause = '0', $parent_id = "0", $insert_text = "") 
	{  
		global $db; 
		if (! $categories = mysql_query("SELECT * FROM mqo_news_category WHERE parent=". $parent_id ." AND active=1 AND id <> ". $where_clause ." ORDER BY parent"))
		{ 
			die ("cannot query"); 
		} 
	 
		while ($category = mysql_fetch_array($categories, MYSQL_ASSOC)) 
 		{ 
		      $selected = $category['id'] == $cur_parent?'selected':'';
     		  $this->content1 .= "<option value='".$category["id"]."' ".$selected." >".$insert_text.$category["name"]."</option>"; 
      		  $this->show_categories1($cur_parent,$where_clause,$category["id"],$insert_text."---"); 
  		} 
		
		return true; 

    } // end function


	/*--------------------------------------------------------------------
	* SHOW LIST OF MULTI CAT IN CASE EDITTING FOLLOW A SPECIFIED LANGUAGE
	* @Params :
	*			$cur_parent: 
	*			$cur_language: 
	*			$where_clause: 
	*			$parent_id: 
	*			$insert_text: 
	-------------------------------------------------------------------------*/
	function show_categories_edit($cur_parent = '', $cur_language = 'vn', $where_clause = '0',$parent_id = "0", $insert_text = "") 
	{  
		global $db; 
		if (! $categories = mysql_query("SELECT * FROM mqo_news_category WHERE parent=". $parent_id ." AND active=1 AND language='". $cur_language ."' AND id <> ". $where_clause ." ORDER BY parent"))
		{ 
			die ("cannot query"); 
		} 
	 
		while ($category = mysql_fetch_array($categories, MYSQL_ASSOC)) 
 		{ 
		      $selected = $category['id'] == $cur_parent ? 'selected' : '';
     	 	  $this->content4 .= "<option value='". $category["id"] ."' ". $selected ." >".$insert_text.$category["name"]."</option>"; 
      		  $this->show_categories_edit($cur_parent, $cur_language, $where_clause, $category["id"], $insert_text."---"); 
  		} 
		
		return true; 

  } // end function




	/*----------------------------------------------------------
			$cur_parent: 
			$parent_id: 
			$insert_text: 
	----------------------------------------------------*/	
	
	function show_categories_a($cur_parent, $parent_id = "0", $insert_text = "") 
	{  
		global $db; 
		if (! $categories = mysql_query("SELECT * FROM mqo_news_category WHERE parent=". $parent_id ." AND active=1 ORDER BY parent"))
		{ 
			die ("cannot query"); 
		} 
	 
		while ($category = mysql_fetch_array($categories, MYSQL_ASSOC)) 
 		{   
		      $selected = $category['id'] == $cur_parent ? 'selected' : '';
     	  	  $this->content2 .= "<option value='". $category["id"] ."' ". $selected ." >". $insert_text.$category["name"] ."</option>"; 
     		  $this->show_categories_a($cur_parent, $category["id"], $insert_text."---"); 
  		} 
		
		return true; 

 	 } // end function
	 
	 

	/*----------------------------------------------------------
		$cur_parent: 
		$cur_language:
		$parent_id:
		$insert_text:
    ------------------------------------------------------------*/		
	function show_categories_b($cur_parent, $cur_language = 'vn', $parent_id = "0", $insert_text = "") 
	{  
		global $db; 
		if (! $categories = mysql_query("SELECT * FROM mqo_news_category WHERE parent=". $parent_id ." AND active=1 AND language='". $cur_language ."' ORDER BY parent"))
		{ 
			die ("cannot query"); 
		} 
	 
		while ($category = mysql_fetch_array($categories, MYSQL_ASSOC)) 
 		{   
		     $selected = $category['id'] == $cur_parent ? 'selected' : '';
     	  	 $this->content3 .= "<option value='". $category["id"] ."' ". $selected ." >". $insert_text.$category["name"]. "</option>"; 
     		 $this->show_categories_b($cur_parent, $cur_language, $category["id"], $insert_text."---"); 
  		} 
		
		return true; 

 	 } // end function	 
	 
	 
	 
	 
	 
	/*------------------------------------------------------
	| DEACTIVE CHILD IF PARENT HAD BEEN DEACTIVED
	------------------------------------------------------*/ 
	function deactived_sub($parent_id)
	{   
	 	global $db;
	    if (! $parents = mysql_query("SELECT * FROM mqo_news_category WHERE parent='". $parent_id ."' ORDER BY id"))
		{ 
			die ("cannot query"); 
		} 
	
    	while ( $parent = mysql_fetch_array($parents, MYSQL_ASSOC))
	    {
		    $db->query("UPDATE mqo_news_category SET active=0 WHERE parent='". $parent_id ."'");
	         $this->deactived_sub($parent['id']);
	    }
     }
	  

   /*-------------------------------------------------------------
   | SET LANGUAGE OF CHILD FOLLOW PARENT
   --------------------------------------------------------------*/
	function set_child_language($parent_id, $language)
	{
	    global $db;
	    if (! $parents = mysql_query("SELECT * FROM mqo_news_category WHERE parent='". $parent_id ."' ORDER BY id"))
		{ 
			die ("cannot query"); 
		} 
	
   	     while ( $parent = mysql_fetch_array($parents, MYSQL_ASSOC))
	     {   $db->query("UPDATE mqo_news_category SET language='". $language ."' WHERE parent='". $parent_id ."'");
	         $this->deactived_sub($parent['id']);
	     }
     }


}  // end class


?>

