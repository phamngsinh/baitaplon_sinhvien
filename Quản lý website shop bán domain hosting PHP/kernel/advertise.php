<?php
/*------------------------------------------------------
	| CLASS USE FOR ADVERTISEMENT 
 -------------------------------------------------------*/
// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

class adv
{
	private $img = "";
	
	/*--------------------------------------------------------------------
	| FUNCTION RETURN ARRAY OF BANNES IN A ADVERT PORTION
	+--------------------------------------------------------------------*/
	/*
	  $cate  : the position
	  return : $text array of banners
	*/
	function show($cate)
	{	
		global $db, $sess, $lang;		
		$text = array();
		
		//-----------Get info about position------------------//
		$query    = $db->simple_select("*", "adv_category", "active=1 AND id=".$cate);
		$result   = $db->query($query);
		$row_cate = $db->fetch_array($result);
		
		//-----------Get banners in a specified position ---------------//
		$query  = $db->simple_select("*", "adv_img", "active=1 AND startdate<=".$sess->now." AND enddate>=".$sess->now." AND category=".$cate, "rank ASC, id DESC");
		$result = $db->query($query);
		
		//-------------Add banners to $img[]---------------------//
		while ( $row_img = $db->fetch_array($result) )
		{
		 	$img[] = $row_img;
		}
		
		//-------------If there are banners in this portion--------------------//
		if ( is_array($img) && count($img)>0 )
		{
			//-----------Show banners with static =1-----------------//
			//------------With banner has static differ from 1 add to  new[]----------//
			$i=0;
			foreach ( $img as $v )
			{
				if ( $v['static']==1 )
				{
					$text[$i]['id'] = $v['id'];
					$text[$i]['path'] = $v['path'];
					$text[$i]['border'] = $v['border'] ? $v['border'] : $row_cate['border'];
					$text[$i]['width'] = $v['width'] ? $v['width'] : $row_cate['width'];
					$text[$i]['height'] = $v['height'] ? $v['height'] : $row_cate['height'];
					$text[$i]['class'] = $v['class'] ? $v['class'] : $row_cate['class'];
					$text[$i]['name'] = $v['name'];
					
					// Increase display time for banners
					$this->viewclick($v['id']);
					
					$i++;
				}
				else
				{ 
					$new[] = $v;
				}
			}
			
			$img = $new;
			
			/* Get position with static<>1 randomly------------------*/
			$key = array_rand($img, (($row_cate['max']-$i)<=count($img)) ? ($row_cate['max']-$i) : count($img));
			
			if ( is_array($key) )
			{ 
				foreach ( $key as $k )
				{
					$text[$i]['id'] = $img[$k]['id'];
					$text[$i]['path'] = $img[$k]['path'];
					$text[$i]['border'] = $img[$k]['border']?$img[$k]['border']:$row_cate['border'];
					$text[$i]['width'] = $img[$k]['width']?$img[$k]['width']:$row_cate['width'];
					$text[$i]['height'] = $img[$k]['height']?$img[$k]['height']:$row_cate['height'];
					$text[$i]['class'] = $img[$k]['class']?$img[$k]['class']:$row_cate['class'];
					$text[$i]['name'] = $img[$k]['name'];
					
					
					$this->viewclick($img[$k]['id']);
					
					$i++;
				}
			}
			else
			{ 
			
				$text[$i]['id'] = $img[$key]['id'];
				$text[$i]['path'] = $img[$key]['path'];
				$text[$i]['border'] = $img[$key]['border']?$img[$key]['border']:$row_cate['border'];
				$text[$i]['width'] = $img[$key]['width']?$img[$key]['width']:$row_cate['width'];
				$text[$i]['height'] = $img[$key]['height']?$img[$key]['height']:$row_cate['height'];
				$text[$i]['class'] = $img[$key]['class']?$img[$key]['class']:$row_cate['class'];
				$text[$i]['name'] = $img[$key]['name'];
				
				
				$this->viewclick($img[$key]['id']);
				
				$i++;
			}
		}
		
		//---------------------Show a default banner if there's an available banner-----------------//
		
		if ( $i<$row_cate['max'] )
		{
			$text[$i]['link'] = $row_cate['link'];
			$text[$i]['path'] = $row_cate['default'];
			$text[$i]['border'] = $row_cate['border'];
			$text[$i]['width'] = $row_cate['width'];
			$text[$i]['height'] = $row_cate['height'];
			$text[$i]['class'] = $row_cate['class'];
			$text[$i]['name'] = $lang['adv_default'];
		}
		
		return $text;
	}
	
	/*------------------------------------------------------------
	| TRACK THE DISPLAY OR CLICK OF BANNERS
	+-------------------------------------------------------------*/
	function viewclick($id, $type="view")
	{
		global $db, $str;
		
		
		$query = $db->simple_select("view, click, link", "adv_img", "id=".$id);
		$result = $db->query($query);
		$row = $db->fetch_array($result);
		
		
		if ( $type=="view" )
		{
			if ( $row['view'] )
			{
		
				$arr = array(
								'view'  => $row['view']+1,
							);
				$db->do_update("adv_img", $arr, "id=".$id);
			}
		}
		elseif ( $type=="click" )
		{
		
			if ( $row['click'] )
			{
		
				$arr = array(
								'click'  => $row['click']+1,
							);
				$db->do_update("adv_img", $arr, "id=".$id);
				
				$str->goto_url("http://".$row['link']);
			}//if
		} // elseif
		
	} // end function
	
	
 } // end class
  
?>