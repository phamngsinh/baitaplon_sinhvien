<?php
/*--------------------------------------------------
| CLASS FOR HANDLE SOME FEATURE OF NEWS
+-----------------------------------------------------*/	
// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

class news
{
   
	public $setting  = array();
	public $tree     = array();
	public $category = array();
	
	function __construct()
	{
		$this->get_setting();		
		$this->cate_tree();
	}
	
	/*------------------------------------------------
	| GET CONFIGURATIONS FROM DB
	+-------------------------------------------------*/
	function get_setting()
	{
		global $db;		
		$query      = $db->simple_select("*", "news_setting");
		$result     = $db->query($query);
		while ($row = $db->fetch_array($result)) 
			$this->setting[$row['name']] = $row['value'];
	}
	
	
	/*--------------------------------------------------
	| DEFINE 2 LEVEL TREE
	+---------------------------------------------------*/
	function cate_tree()
	{
		global $db, $sess;

		$query = $db->simple_select("*", "news_category", "", "rank ASC");
		$result = $db->query($query);
		while ($row = $db->fetch_array($result))
			$this->category[$row['id']] = $row;
		
	    $this->cate_tree_sort($this->category);
	}
	
	
	/*--------------------------------------------
	| SORT TREE
	+---------------------------------------------*/
	function cate_tree_sort($category, $parent=0)
	{
		foreach($category as $k=>$v)
		{
			if ($v['parent'] == $parent)
			{
				$this->tree[] = $v;
				$this->cate_tree_sort($category, $v['id']);
			}
		}
		
	}
}
?>