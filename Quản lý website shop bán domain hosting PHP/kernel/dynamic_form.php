<?php
/*-----------------------------------
| CREATE DYNAMIC PART FOR SITE
-----------------------------------*/
class dynamic_form
{
     
	/*--------------------------------------------- 
	   // SELECT LIST OF VOTE FOLLOW A LANGUAGE
	   Params : 
		+ $select_name: name of select
		+ $lang : name of language . Default language is Vietnam
		+ $check : selected option
	   Return : select list of votes
	------------------------------------------------*/
	function display_vote($select_name, $lang='vn', $check='')
	{  
	   global $db, $str;
	   $result = '';
	   $result .= "<select name='".$select_name."'>";
	   $query = $db->query("SELECT * FROM mqo_vote WHERE active=1 and language='".$lang."'");
	   while($row = mysql_fetch_array($query))
	   {  $selected = ($check == $row['id']) ? "selected" : "";
		   $content = $str->subword($row['vote_content'],15);
		   $result .= "<option value='". $row['id'] ."' ". $selected .">". $content. "...</option>";	
		
	   }
	   $result .= "</select>"; 
	   return $result;
	}  
		
	/*-----------------------------------------------------------
		SHOW NAME OF LANGUAGE DEPEND ON CODE NAME OF LANGUAGE
		@Param :
			$lang_code : code of language need to get name(Ex: vn or vn|en)
		@Return : name of language
				  Ex : input la vn thi output la Vietnam
					   input la vn|en thi output la Vietnam|English
	 ----------------------------------------------------------*/
 	function get_language_name($lang_code)
 	{  global $db ;
	    $lang_arr = array();
		$lang_name = '';
	    //----Customize string code of languages---------//
	    $lang_code = str_replace("|","','",$lang_code);
		$lang_code = "'".$lang_code."'";
		//------Query db and get infos-------------//
	   $sql = $db->simple_select( "*" , "language" , "code IN (". $lang_code .")" ) ;
	   $query = $db->query( $sql );
	   
	    while ( $row = mysql_fetch_array( $query ) )
		   	{  
			     $lang_arr[] = $row['name'];
			   }
		// Convert to string format to display
		$lang_name = implode('|',$lang_arr);		 
			 
		return $lang_name;
			 
	  }
	  
	  
	/*--------------------------------------------------------------------
		SHOW CHECK BOX ABOUT LANGUAGE
		In this case we allow user to choose more than one languages
	---------------------------------------------------------------------*/
	function show_language_checkbox($name, $selected='')
	{ 
	   global $frm , $db;
	   $string_return = '<div>';
	   $sql = $db->simple_select( "code , name , isdefault" , "language" , "active=1" );
	   $query = $db->query($sql);
	   while ( $row = @mysql_fetch_array( $query ) )
	   {  
			if(!$selected)
			{
				$ischeck = $row['isdefault'] == 1 ? "checked" : ""; 
			}
			else
			{ 
			   if(strpos($selected,$row['code'])===false)
			   { $ischeck = ""; }
			   else { $ischeck = "checked"; }
			}
			   $string_return .= $frm->draw_checkbox( $name."[]" , $row['code'] , $row['name'] , $ischeck )."&nbsp;&nbsp;";
	   }
	   $string_return .= "</div>";
	   return $string_return;
	}

}


?>
