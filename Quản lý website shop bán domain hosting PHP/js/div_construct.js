isNS = document.layers?true:false;
isIE = navigator.appName.indexOf("Microsoft") != -1
isNS6=document.getElementById&&!isIE?true:false;

function NewDiv2(window, id, body, left, top, width, height, zIndex, absolute) {
    this.window = window;
    this.id     = id;
    this.body   = body;
    var d = window.document;
    d.writeln('<STYLE TYPE="text/css">#' + id + ' {');
	if (absolute) d.write('position:absolute;');
	else          d.write('position:relative;');
    if (left)   d.write('left:'  + left  + ';');
    if (top)    d.write('top:'   + top   + ';');
    if (width)  d.write('width:' + width + ';');
	if (height) d.write('height:' + height + ';');
	if (zIndex) d.write('z-index:' + zIndex + ';');
    d.writeln('}</STYLE>');
}
if (isNS) {
    NewDiv2.prototype.output             = function()      { var d = this.window.document;d.writeln('<DIV ID="' + this.id + '">');d.writeln(this.body);d.writeln("</DIV>");this.layer = d[this.id];}
    NewDiv2.prototype.moveTo             = function(x,y)   { this.layer.moveTo(x,y); }
    NewDiv2.prototype.moveBy             = function(x,y)   { this.layer.moveBy(x,y); }
    NewDiv2.prototype.show               = function()      { this.layer.visibility = "show"; }
    NewDiv2.prototype.hide               = function()      { this.layer.visibility = "hide"; }
    NewDiv2.prototype.setZ               = function(z)     { this.layer.zIndex = z; }
    NewDiv2.prototype.setBgColor         = function(color) { this.layer.bgColor = color; }
    NewDiv2.prototype.setBgImage         = function(image) { this.layer.background.src = image;}
    NewDiv2.prototype.getX               = function() { return this.layer.left; }
    NewDiv2.prototype.getY               = function() { return this.layer.top; } //was right .. ??
    NewDiv2.prototype.getWidth           = function() { return this.layer.width; }
    NewDiv2.prototype.getHeight          = function() { return this.layer.height; }
    NewDiv2.prototype.getZ               = function() { return this.layer.zIndex; }
    NewDiv2.prototype.isVisible          = function() { return this.layer.visibility == "show"; }
    NewDiv2.prototype.setBody            = function() { for(var i = 0; i < arguments.length; i++) this.layer.document.writeln(arguments[i]);this.layer.document.close();}
    NewDiv2.prototype.addEventHandler    = function(eventname, handler) {this.layer.captureEvents(NewDiv2._eventmasks[eventname]); var newdivel = this;this.layer[eventname] = function(event) { return handler(newdivel, event.type, event.x, event.y, event.which, event.which,((event.modifiers & Event.SHIFT_MASK) != 0),((event.modifiers & Event.CTRL_MASK)  != 0),((event.modifiers & Event.ALT_MASK)   != 0));}}
    NewDiv2.prototype.removeEventHandler = function(eventname)          {this.layer.releaseEvents(NewDiv2._eventmasks[eventname]);delete this.layer[eventname];}
    NewDiv2.prototype.centerX            = function() {this.layer.moveTo(Math.round((window.pageXOffset+document.width-100)/2),this.layer.top)}
    NewDiv2._eventmasks                  = {onabort:Event.ABORT,onblur:Event.BLUR,onchange:Event.CHANGE,onclick:Event.CLICK,ondblclick:Event.DBLCLICK, ondragdrop:Event.DRAGDROP,onerror:Event.ERROR, onfocus:Event.FOCUS,onkeydown:Event.KEYDOWN,onkeypress:Event.KEYPRESS,onkeyup:Event.KEYUP,onload:Event.LOAD,onmousedown:Event.MOUSEDOWN,onmousemove:Event.MOUSEMOVE, onmouseout:Event.MOUSEOUT,onmouseover:Event.MOUSEOVER, onmouseup:Event.MOUSEUP,onmove:Event.MOVE,onreset:Event.RESET,onresize:Event.RESIZE,onselect:Event.SELECT,onsubmit:Event.SUBMIT,onunload:Event.UNLOAD};
}

if (isIE) {
    NewDiv2.prototype.output             = function()                   { var d = this.window.document;d.writeln('<DIV ID="' + this.id + '">');d.writeln(this.body);d.writeln("</DIV>");this.element = d.all[this.id];this.style = this.element.style;}
    NewDiv2.prototype.moveTo             = function(x,y)                { this.style.pixelLeft = x;this.style.pixelTop = y;}
    NewDiv2.prototype.moveBy             = function(x,y)                { this.style.pixelLeft += x;this.style.pixelTop += y;}
    NewDiv2.prototype.show               = function()                   { this.style.visibility = "visible"; }
    NewDiv2.prototype.hide               = function()                   { this.style.visibility = "hidden"; }
    NewDiv2.prototype.setZ               = function(z)                  { this.style.zIndex = z; }
    NewDiv2.prototype.setBgColor         = function(color)              { this.style.backgroundColor = color; }
    NewDiv2.prototype.setBgImage         = function(image)              { this.style.backgroundImage = image;}
    NewDiv2.prototype.getX               = function()                   { return this.style.pixelLeft; }
    NewDiv2.prototype.getY               = function()                   { return this.style.pixelRight; }
    NewDiv2.prototype.getWidth           = function()                   { return this.style.width; }
    NewDiv2.prototype.getHeight          = function()                   { return this.style.height; }
    NewDiv2.prototype.getZ               = function()                   { return this.style.zIndex; }
    NewDiv2.prototype.isVisible          = function()                   { return this.style.visibility == "visible"; }
    NewDiv2.prototype.setBody            = function()                   { var body = "";for(var i = 0; i < arguments.length; i++) {body += arguments[i] + "\n";}this.element.innerHTML = body;}
    NewDiv2.prototype.addEventHandler    = function(eventname, handler) { var NewDiv = this;this.element[eventname] = function() { var e = NewDiv2.window.event;e.cancelBubble = true;return handler(NewDiv2, e.type, e.x, e.y, e.button, e.keyCode, e.shiftKey, e.ctrlKey, e.altKey); }}
    NewDiv2.prototype.removeEventHandler = function(eventname)          { delete this.element[eventname];}
	NewDiv2.prototype.centerX            = function()                   { this.style.pixelLeft=Math.round((document.body.clientWidth+document.body.scrollLeft-300)/2)}
}

/*
 * The following was written by:
 *   Dario Guzik
 *      Spam unfriendly email address: dguzik AT bigfoot DOT com
 */
if (isNS6) {
    NewDiv2.prototype.output             = function()                   { var d = this.window.document;d.writeln('<DIV ID="' + this.id + '">');d.writeln(this.body);d.writeln("</DIV>");this.element = d.getElementById(this.id);this.style = this.element.style;}
    NewDiv2.prototype.moveTo             = function(x,y)                { this.style.pixelLeft = x;this.style.pixelTop = y;}
    NewDiv2.prototype.moveBy             = function(x,y)                { this.style.pixelLeft += x;this.style.pixelTop += y;}
    NewDiv2.prototype.show               = function()                   { this.style.visibility = "visible"; }
    NewDiv2.prototype.hide               = function()                   { this.style.visibility = "hidden"; }
    NewDiv2.prototype.setZ               = function(z)                  { this.style.zIndex = z; }
    NewDiv2.prototype.setBgColor         = function(color)              { this.style.backgroundColor = color; }
    NewDiv2.prototype.setBgImage         = function(image)              { this.style.backgroundImage = image;}
    NewDiv2.prototype.getX               = function()                   { return this.style.pixelLeft; }
    NewDiv2.prototype.getY               = function()                   { return this.style.pixelRight; }
    NewDiv2.prototype.getWidth           = function()                   { return this.style.width; }
    NewDiv2.prototype.getHeight          = function()                   { return this.style.height; }
    NewDiv2.prototype.getZ               = function()                   { return this.style.zIndex; }
    NewDiv2.prototype.isVisible          = function()                   { return this.style.visibility == "visible"; }
    NewDiv2.prototype.setBody            = function()                   { var body = "";for(var i = 0; i < arguments.length; i++) {body += arguments[i] + "\n";}this.element.innerHTML = body;}
    NewDiv2.prototype.addEventHandler    = function(eventname, handler) { var NewDiv = this;this.element[eventname] = function() { var e = NewDiv2.window.event;e.cancelBubble = true;return handler(NewDiv, e.type, e.x, e.y, e.button, e.keyCode, e.shiftKey, e.ctrlKey, e.altKey); }}
    NewDiv2.prototype.removeEventHandler = function(eventname)          { delete this.element[eventname];}
  NewDiv2.prototype.centerX            = function()                   { this.style.pixelLeft=Math.round((document.body.clientWidth+document.body.scrollLeft-300)/2)}
}
