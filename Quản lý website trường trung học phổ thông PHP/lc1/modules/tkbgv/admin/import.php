<?php
/**
 * @Author GaNguCay (gangucay@gmail.com)
 * @copyright Freeware
 * @createdate 11/08/2010
 */

if (! defined ( 'NV_IS_FILE_ADMIN' ))
	die ( 'Stop!!!' );
$page_title = $lang_module ['import'];
if ($nv_Request->isset_request ( 'import1', 'post' )) {
	$data = array();  
	if ( $_FILES['ufile1']['tmp_name'])  
	{  
	    $dom = DOMDocument::load( $_FILES['ufile1']['tmp_name'] );  
	    $rows = $dom->getElementsByTagName( 'Row' );  
		$tde=array();
		$line=0;
		$them=0;
		$sua=0;
		foreach ($rows as $row){ 
		$cells = $row->getElementsByTagName( 'Cell' );  
		$datarow = array();  
			foreach ($cells as $cell){  
	     		if ($line==0){
	        		$tde[]=$cell->nodeValue;
	     		}else{
	     			$datarow []= $cell->nodeValue;
	     		} 
		 	}  
		$data []= $datarow;  
		$line=$line+1;      
		}
//
	foreach( $data as $row ) {  
		$tkbgv=array();
		$i=0;
		if (isset($row[0])){
		foreach( $row as $item ) {
		//chen vo CSDL
			$tkbgv[$i]=$item;
			$i=$i+1;	
		} 
		
	   if( $tkbgv[0]!="" and $tkbgv[1]!="") {
	   	$sql = "SELECT * FROM `" . NV_PREFIXLANG . "_" . $module_data . "` WHERE `id`='" . $tkbgv[0]."'";
		$result = $db->sql_query($sql);
		$numrows = $db->sql_numrows($result);
		if(!$numrows) {
	   	$sql1 = "INSERT INTO `" . NV_PREFIXLANG . "_" . $module_data . "` ( `id` , `gvid` , `tiet` , `thu2` , `thu3` , `thu4` , `thu5` , `thu6` , `thu7`) VALUES ('".$tkbgv[0]."', '".$tkbgv[1]."', '".$tkbgv[2]."', '".$tkbgv[3]."', '".$tkbgv[4]."', '".$tkbgv[5]."', '".$tkbgv[6]."', '".$tkbgv[7]."', '".$tkbgv[8]."')";
			$result = $db->sql_query($sql1);
			$them=$them+1;
		}else{
		$sql2 = "UPDATE `" . NV_PREFIXLANG . "_" . $module_data . "` SET `id`='".$tkbgv[0]."', `gvid`='".$tkbgv[1]."', `tiet`='".$tkbgv[2]."', `thu2`='".$tkbgv[3]."' , `thu3`='".$tkbgv[4]."', `thu4`='".$tkbgv[5]."', `thu5`='".$tkbgv[6]."' , `thu6`='".$tkbgv[7]."', `thu7`='".$tkbgv[8]."' WHERE `id`='".$tkbgv[0]."'";
		$db->sql_query($sql2);
		$sua=$sua+1;
		}
		}
		}  
	}
	$line=$line-1;
	//Hien thi thong bao sau khi import
	$contents .= "<div class=\"quote\" style=\"width:780px;\">\n";
    $contents .= "<blockquote class=\"error\"><span>" . $lang_module['import_success'] . "</span></blockquote>\n";
    $contents .= "</div><br>";
    $contents .= "<div class=\"clear\"></div>\n";
    $contents .= "<div id=\"list_mods\"
	<form id=\"form\" name=\"form\" method=\"post\">
	<table class=\"tab1\">
	<tr>
		<td class=\"fr\">" . $lang_module['line'] . "" . $line. "<br></td>
	</tr>
	<tr>
		<td class=\"fr\">" . $lang_module['them'] . "" . $them. "<br></td>
	</tr>
	<tr>
		<td class=\"fr\">" . $lang_module['sua'] . "" . $sua. "<br></td>
	</tr>
	</table>
	</form></div>";
}
}else if ($nv_Request->isset_request ( 'import2', 'post' )) {
	$data = array();  
	if ( $_FILES['ufile2']['tmp_name'])  
	{  
	    $dom = DOMDocument::load( $_FILES['ufile2']['tmp_name'] );  
	    $rows = $dom->getElementsByTagName( 'Row' );  
		$tde=array();
		$line=0;
		$them=0;
		$sua=0;
		foreach ($rows as $row){ 
		$cells = $row->getElementsByTagName( 'Cell' );  
		$datarow = array();  
			foreach ($cells as $cell){  
	     		if ($line==0){
	        		$tde[]=$cell->nodeValue;
	     		}else{
	     			$datarow []= $cell->nodeValue;
	     		} 
		 	}  
		$data []= $datarow;  
		$line=$line+1;      
		}
//
	foreach( $data as $row ) {  
		$tkbgv=array();
		$i=0;
		if (isset($row[0])){
		foreach( $row as $item ) {
		//chen vo CSDL
			$tkbgv[$i]=$item;
			$i=$i+1;	
		} 
	   if( $tkbgv[0]!="" and $tkbgv[1]!="") {
	   	$sql = "SELECT * FROM `" . NV_PREFIXLANG . "_" . $module_data . "_cat` WHERE `gvid`='" . $tkbgv[0]."'";
		$result = $db->sql_query($sql);
		$numrows = $db->sql_numrows($result);
		if(!$numrows) {
	   	$sql1 = "INSERT INTO `" . NV_PREFIXLANG . "_" . $module_data . "_cat` ( `gvid` ,`toid`, `tengv`) VALUES ('".$tkbgv[0]."', '".$tkbgv[1]."', '".$tkbgv[2]."')";
			$result = $db->sql_query($sql1);
			$them=$them+1;
		}else{
		$sql2 = "UPDATE `" . NV_PREFIXLANG . "_" . $module_data . "_cat` SET `gvid`='".$tkbgv[0]."', `toid`='".$tkbgv[1]."', `tengv`='".$tkbgv[2]."' WHERE `gvid`='".$tkbgv[0]."'";
		$db->sql_query($sql2);
		$sua=$sua+1;
		}
		}
		}  
	}
	$line=$line-1;
	//Hien thi thong bao sau khi import
	$contents .= "<div class=\"quote\" style=\"width:780px;\">\n";
    $contents .= "<blockquote class=\"error\"><span>" . $lang_module['import_success_cat'] . "</span></blockquote>\n";
    $contents .= "</div><br>";
    $contents .= "<div class=\"clear\"></div>\n";
    $contents .= "<div id=\"list_mods\"
	<form id=\"form\" name=\"form\" method=\"post\">
	<table class=\"tab1\">
	<tr>
		<td class=\"fr\">" . $lang_module['line'] . "" . $line. "<br></td>
	</tr>
	<tr>
		<td class=\"fr\">" . $lang_module['them'] . "" . $them. "<br></td>
	</tr>
	<tr>
		<td class=\"fr\">" . $lang_module['sua'] . "" . $sua. "<br></td>
	</tr>
	</table>
	</form></div>";
}
}else if ($nv_Request->isset_request ( 'import3', 'post' )) {
	$data = array();  
	if ( $_FILES['ufile3']['tmp_name'])  
	{  
	    $dom = DOMDocument::load( $_FILES['ufile3']['tmp_name'] );  
	    $rows = $dom->getElementsByTagName( 'Row' );  
		$tde=array();
		$line=0;
		$them=0;
		$sua=0;
		foreach ($rows as $row){ 
		$cells = $row->getElementsByTagName( 'Cell' );  
		$datarow = array();  
			foreach ($cells as $cell){  
	     		if ($line==0){
	        		$tde[]=$cell->nodeValue;
	     		}else{
	     			$datarow []= $cell->nodeValue;
	     		} 
		 	}  
		$data []= $datarow;  
		$line=$line+1;      
		}
//
	foreach( $data as $row ) {  
		$tkbgv=array();
		$i=0;
		if (isset($row[0])){
		foreach( $row as $item ) {
		//chen vo CSDL
			$tkbgv[$i]=$item;
			$i=$i+1;	
		} 
	   if( $tkbgv[0]!="") {
	   	$sql = "SELECT * FROM `" . NV_PREFIXLANG . "_" . $module_data . "_to` WHERE `toid`='" . $tkbgv[0]."'";
		$result = $db->sql_query($sql);
		$numrows = $db->sql_numrows($result);
		if(!$numrows) {
	   	$sql1 = "INSERT INTO `" . NV_PREFIXLANG . "_" . $module_data . "_to` ( `toid` , `tento`) VALUES ('".$tkbgv[0]."', '".$tkbgv[1]."')";
			$result = $db->sql_query($sql1);
			$them=$them+1;
		}else{
		$sql2 = "UPDATE `" . NV_PREFIXLANG . "_" . $module_data . "_to` SET `toid`='".$tkbgv[0]."', `tento`='".$tkbgv[1]."' WHERE `toid`='".$tkbgv[0]."'";
		$db->sql_query($sql2);
		$sua=$sua+1;
		}
		}
		}  
	}
	$line=$line-1;
	//Hien thi thong bao sau khi import
	$contents .= "<div class=\"quote\" style=\"width:780px;\">\n";
    $contents .= "<blockquote class=\"error\"><span>" . $lang_module['import_success_cat'] . "</span></blockquote>\n";
    $contents .= "</div><br>";
    $contents .= "<div class=\"clear\"></div>\n";
    $contents .= "<div id=\"list_mods\"
	<form id=\"form\" name=\"form\" method=\"post\">
	<table class=\"tab1\">
	<tr>
		<td class=\"fr\">" . $lang_module['line'] . "" . $line. "<br></td>
	</tr>
	<tr>
		<td class=\"fr\">" . $lang_module['them'] . "" . $them. "<br></td>
	</tr>
	<tr>
		<td class=\"fr\">" . $lang_module['sua'] . "" . $sua. "<br></td>
	</tr>
	</table>
	</form></div>";
}
}else
{
$contents .= "<div><form enctype=\"multipart/form-data\" id=\"form1\" name=\"form1\" method=\"post\">
<table class=\"tab1\">
	<tr>
		<td class=\"fr\" width=\"200\">". $lang_module ['import'] . "</td>
		<td class=\"fr1\"><input type=\"file\" name=\"ufile1\" size = \"35\" id=\"ufile1\"/>
		<input type=\"submit\" name=\"import1\" id=\"import1\" value=\"Import\" /></td>
	</tr>
	</table></center>
	</form></div>
	
	<div><form enctype=\"multipart/form-data\" id=\"form2\" name=\"form2\" method=\"post\"><center>
	<table class=\"tab1\">
	<tr>
		<td class=\"fr\"  width=\"200\">". $lang_module ['import_cat'] . "</td>
		<td class=\"fr1\"><input type=\"file\" name=\"ufile2\" size = \"35\" id=\"ufile2\"/>
		<input type=\"submit\" name=\"import2\" id=\"import2\" value=\"Import\" /></td>
	</tr>
	</table></center>
	</form></div>
	
	<div><form enctype=\"multipart/form-data\" id=\"form3\" name=\"form3\" method=\"post\"><center>
	<table class=\"tab1\">
	<tr>
		<td class=\"fr\"  width=\"200\">". $lang_module ['import_to'] . "</td>
		<td class=\"fr1\"><input type=\"file\" name=\"ufile3\" size = \"35\" id=\"ufile3\"/>
		<input type=\"submit\" name=\"import3\" id=\"import3\" value=\"Import\" /></td>
	</tr>
</table>
</form></div><br>";
}
include (NV_ROOTDIR . "/includes/header.php");
echo nv_admin_theme ( $contents );
include (NV_ROOTDIR . "/includes/footer.php");
?>
