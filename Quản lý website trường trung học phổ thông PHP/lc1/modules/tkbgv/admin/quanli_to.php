<?php

/**
 * @Author GaNguCay (gangucay@gmail.com)
 * @copyright Freeware
 * @createdate 11/08/2010
 */
if ( ! defined( 'NV_IS_FILE_ADMIN' ) ) die( 'Stop!!!' );
$page_title = $lang_module['quanli_to'];

		$toid = $nv_Request->get_int ( 'toid', 'get' );
		$delid = $nv_Request->get_int ( 'delid', 'get' );
        // Thuc hien xoa, them moi hay cap nhat to
        $error="";
	    $value_add=filter_text_input('addto','post','',1);
	    $value_edit=filter_text_input('editto','post','',1);
	    if(!empty($value_add)){
			$sql1 ="SELECT `tento` FROM `" . NV_PREFIXLANG . "_" . $module_data . "_to` WHERE  `tento`='".$value_add."'";
			$result1 = $db->sql_query( $sql1);
			$row1 = $db->sql_fetchrow( $result1);
			$tento = stripslashes($row1[0]);
				if($value_add==$tento){
					//Ten to da ton tai
					$error= "<center><b><font color=blue size=\"2\">" . $lang_module['dacoto'] . "</font></b></center>";
				}else{
					//Them to moi
					$db->sql_query("INSERT INTO `" . NV_PREFIXLANG . "_" . $module_data . "_to` (`toid`, `tento`) VALUES (NULL,'".$value_add."')");
					$error= "<center><b><font color=blue size=\"2\">" . $lang_module['isto'] . "</font></b></center>";
				}
		}else if(!empty($value_edit)){
			$sql2 ="SELECT `tento` FROM `" . NV_PREFIXLANG . "_" . $module_data . "_to` WHERE  `tento`='".$value_edit."'";
			$result2 = $db->sql_query( $sql2);
			$row2 = $db->sql_fetchrow( $result2);
			$tento = stripslashes($row2[0]);
				if($value_edit==$tento){
					//Ten to da ton tai
					$error= "<center><b><font color=blue size=\"2\">" . $lang_module['dacoto'] . "</font></b></center>";
				}else{
					//Cap nhat lai ten to
					$db->sql_query("UPDATE`" . NV_PREFIXLANG . "_" . $module_data . "_to` SET `tento`='".$value_edit."' WHERE `toid`='".$toid."'");
					$error= "<center><b><font color=blue size=\"2\">" . $lang_module['usto'] . "</font></b></center>";
				}
		}else if($delid!=0){
			$sql3 ="SELECT `toid` FROM `" . NV_PREFIXLANG . "_" . $module_data . "_cat` WHERE  `toid`=".$delid."";
			$result3 = $db->sql_query( $sql3);
			$num3 = $db->sql_numrows( $result3 );
			if($num3>=1){
				//Co lien ket phu thuoc
				$error= "<center><b><font color=blue size=\"2\">" . $lang_module['lkptto'] . "</font></b></center>";
			}else{
				//Xoa to
				$db->sql_query("DELETE FROM`" . NV_PREFIXLANG . "_" . $module_data . "_to` WHERE `toid`='".$delid."'");
				$error= "<center><b><font color=blue size=\"2\">" . $lang_module['delsc'] . "</font></b></center>";
			}
		}
$sql = "SELECT DISTINCT * FROM `" . NV_PREFIXLANG . "_" . $module_data . "_to` ORDER BY `toid` ASC";
$result = $db->sql_query( $sql );
$num = $db->sql_numrows( $result );
if ( $num > 0 )
{	
    $contents .= "<table summary=\"\" class=\"tab1\">\n";
    $contents .= "<td>";
    $contents .= "<center><b><font color=blue size=\"3\">" . $lang_module['quanli_hd_to'] . "</font></b></center>";
    $contents .= "</td>\n";
    $contents .= "</table>";
    if ($toid!=0){
    	//Lay ten to can sua
    	$sqledit = "SELECT * FROM `" . NV_PREFIXLANG . "_" . $module_data . "_to` WHERE  `toid`=".$toid."";
		$resultedit = $db->sql_query( $sqledit);
		$row = $db->sql_fetchrow( $resultedit );
		$tento = stripslashes($row[1]);

    	//$contents .= "<div>";
	    $contents .= "<form name=\"edit_to\" method=\"post\">";
	    $contents .= "<table summary=\"\" class=\"tab1\">\n";
	    $contents .= "<td>";
	    $contents .= "<center><b><font color=blue size=\"2\">" . $lang_module['edit_to'] . "</font></b></center>";
	    $contents .= "<center><font size=\"2\">" . $lang_module['tento'] . "</font>";
	    $contents .= "&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"text\" name=\"editto\" size = \"30\" value=\"".$tento."\"/>";
	    $contents .= "<input type=\"hidden\" name=\"id_to\" value=\"".$toid."\"/>";
	    $contents .= "&nbsp;&nbsp;<input type=\"submit\" name=\"submitedit\" value = \"" . $lang_module['edit_to'] . "\"/></center>";
	    $contents .= "</td>\n";
	    $contents .= "</table>";
		$contents .= "</form>";
	    //$contents .= "</div>";
    }
    else{
	    $contents .= "<form name=\"add_to\" action=\"\" method=\"post\">";
	    $contents .= "<table summary=\"\" class=\"tab1\">\n";
	    $contents .= "<td>";
	    $contents .= "<center><b><font color=blue size=\"2\">" . $lang_module['add_to'] . "</font></b></center>";
	    $contents .= "<center><font size=\"2\">" . $lang_module['tento'] . "</font>";
	    $contents .= "&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"text\" name=\"addto\" size = \"30\" />";
	    $contents .= "&nbsp;&nbsp;<input type=\"submit\" name=\"submitadd\" value = \"" . $lang_module['add_to'] . "\" /></center>";
	    $contents .= "</td>\n";
	    $contents .= "</table>";
		$contents .= "</form>";
    }
    //Hien thi thong bao loi
    if(!empty($error)){
		$contents .= "<div class=\"quote\" style=\"width:780px;\">\n";
		$contents .= "<blockquote class='error'><span id='message'>" .$error. "</span></blockquote>\n";
		$contents .= "</div>\n";
    }
    // Hien thi danh sach cac to
	$contents .= "<table class=\"tab1\">\n";
    $contents .= "<thead>\n";
    $contents .= "<tr>\n";
    $contents .= "<td align=\"center\">" . $lang_module['stt'] . "</td>\n";
    $contents .= "<td align=\"center\">" . $lang_module['toid'] . "</td>\n";
    $contents .= "<td align=\"center\">" . $lang_module['tento'] . "</td>\n";
    $contents .= "<td align=\"center\">" . $lang_module['quanli'] . "</td>\n";
    $contents .= "</tr>\n";
    $contents .= "</thead>\n";
    $a = 0;
    $b=1;
    while ( $row = $db->sql_fetchrow( $result ) )
    {
        $class = ( $a % 2 ) ? " class=\"second\"" : "";
        $contents .= "<tbody" . $class . ">\n";
        $contents .= "<tr>\n";
        $contents .= "<td align=\"center\">" . $b. "</td>\n";
        $contents .= "<td align=\"center\">" . $row['toid'] . "</td>\n";
        $contents .= "<td>" . $row['tento'] . "</td>\n";
        $contents .= "<td align=\"center\"><span class=\"edit_icon\"><a href=\"index.php?" . NV_NAME_VARIABLE . "=" . $module_name . "&amp;" . NV_OP_VARIABLE . "=quanli_to&amp;toid=" . $row['toid'] . "\">" . $lang_global['edit'] . "</a></span>\n";
        $contents .= "&nbsp;-&nbsp;<span class=\"delete_icon\"><a href=\"index.php?" . NV_NAME_VARIABLE . "=" . $module_name . "&amp;" . NV_OP_VARIABLE . "=quanli_to&amp;delid=" . $row['toid'] . "\">" . $lang_global['delete'] . "</a></span></td>\n";
        $contents .= "</tr>\n";
        $contents .= "</tbody>\n";
        $a ++;
        $b ++;
    }
    $contents .= "</table>\n";
    include ( NV_ROOTDIR . "/includes/header.php" );
    echo nv_admin_theme( $contents );
    include ( NV_ROOTDIR . "/includes/footer.php" );
}
else
{
	$contents .= "<div id='edit'></div>\n";
	$contents .= "<div class=\"quote\" style=\"width:780px;\">\n";
	$contents .= "<blockquote class='error'><span id='message'>" . $lang_module ['quanli_err'] . "</span></blockquote>\n";
	$contents .= "</div>\n";
	include (NV_ROOTDIR . "/includes/header.php");
	echo nv_admin_theme ( $contents);
	include (NV_ROOTDIR . "/includes/footer.php");
}
?>