<?php

/**
 * @Author GaNguCay (gangucay@gmail.com)
 * @copyright Freeware
 * @createdate 11/08/2010
 */
if ( ! defined( 'NV_IS_FILE_ADMIN' ) ) die( 'Stop!!!' );
$page_title = $lang_module['quanli_cat'];
		$gvid = $nv_Request->get_int ( 'gvid', 'get' );
		$delid = $nv_Request->get_int ( 'delid', 'get' );
		
	    $value_add=filter_text_input('addcat','post','',1);
	    $value_edit=filter_text_input('editcat','post','',1);
   	    $value_to=filter_text_input('keywords','post','',1);
	    if(!empty($value_add)){
			$sql1 ="SELECT * FROM `" . NV_PREFIXLANG . "_" . $module_data . "_cat` WHERE  `tengv`='".$value_add."'";
			$result1 = $db->sql_query( $sql1);
			$row1 = $db->sql_fetchrow( $result1);
			$tento= stripslashes($row1[1]);
			$tengv = stripslashes($row1[2]);
				if($value_add==$tengv  && $value_to==$tento){
					//Ten gv da ton tai
					$error= "<center><b><font color=blue size=\"2\">" . $lang_module['dacogv'] . "</font></b></center>";
				} else if (empty($value_to) or $value_to==0){
					// Chua chon to
					$error= "<center><b><font color=blue size=\"2\">" . $lang_module['ccto'] . "</font></b></center>";
				}else{
					//Them giao vien moi
					$db->sql_query("INSERT INTO `" . NV_PREFIXLANG . "_" . $module_data . "_cat` (`gvid`,`toid`, `tengv`) VALUES (NULL,'".$value_to."','".$value_add."')");
					$error= "<center><b><font color=blue size=\"2\">" . $lang_module['isgv'] . "</font></b></center>";
				}
		}else if(!empty($value_edit)){
			$sql2 ="SELECT * FROM `" . NV_PREFIXLANG . "_" . $module_data . "_cat` WHERE  `tengv`='".$value_edit."'";
			$result2 = $db->sql_query( $sql2);
			$row2 = $db->sql_fetchrow( $result2);
			$tento= stripslashes($row2[1]);
			$tengv = stripslashes($row2[2]);
				if($value_edit==$tengv  && $value_to==$tento){
					//Ten giao vien da ton tai
					$error= "<center><b><font color=blue size=\"2\">" . $lang_module['dacogvedit'] . "</font></b></center>";
				} else if (empty($value_to) or $value_to==0){
					// Chua chon to
					$error= "<center><b><font color=blue size=\"2\">" . $lang_module['ccto'] . "</font></b></center>";
				}else{
					//Cap nhat lai ten giáo viên
					$db->sql_query("UPDATE`" . NV_PREFIXLANG . "_" . $module_data . "_cat` SET `toid`='".$value_to."',`tengv`='".$value_edit."' WHERE `gvid`='".$gvid."'");
					$error= "<center><b><font color=blue size=\"2\">" . $lang_module['usgvedit'] . "</font></b></center>";
				}
		}else if($delid!=0){
			$sql3 ="SELECT `gvid` FROM `" . NV_PREFIXLANG . "_" . $module_data . "` WHERE  `gvid`=".$delid."";
			$result3 = $db->sql_query( $sql3);
			$num3 = $db->sql_numrows( $result3 );
			if($num3>=1){
				//Co lien ket phu thuoc
				$error= "<center><b><font color=blue size=\"2\">" . $lang_module['lkptgv'] . "</font></b></center>";
			}else{
				//Xoa giao vien
				$db->sql_query("DELETE FROM`" . NV_PREFIXLANG . "_" . $module_data . "_cat` WHERE `gvid`='".$delid."'");
				$error= "<center><b><font color=blue size=\"2\">" . $lang_module['delscgv'] . "</font></b></center>";
			}
		}

//$sql = "SELECT DISTINCT * FROM `" . NV_PREFIXLANG . "_" . $module_data . "_cat` ORDER BY `gvid` ASC";
$sql = "SELECT DISTINCT " . NV_PREFIXLANG . "_" . $module_data . "_cat.gvid," . NV_PREFIXLANG . "_" . $module_data . "_cat.tengv," . NV_PREFIXLANG . "_" . $module_data . "_to.tento FROM " . NV_PREFIXLANG . "_" . $module_data . "_cat," . NV_PREFIXLANG . "_" . $module_data . "_to WHERE " . NV_PREFIXLANG . "_" . $module_data . "_cat.toid=" . NV_PREFIXLANG . "_" . $module_data . "_to.toid ORDER BY " . NV_PREFIXLANG . "_" . $module_data . "_cat.gvid";

$result = $db->sql_query( $sql );
$num = $db->sql_numrows( $result );
if ( $num > 0 )
{
	$contents .= "<div>";
    $contents .= "<form name=\"deltkb\" action=\"\" method=\"post\">";
    $contents .= "<table summary=\"\" class=\"tab1\">\n";
    $contents .= "<td>";
    $contents .= "<center><b><font color=blue size=\"3\">" . $lang_module['quanli_hd_cat'] . "</font></b></center>";
    $contents .= "</td>\n";
    $contents .= "</table>";
	$contents .= "</form>";
    $contents .= "</div>";

    if ($gvid!=0){
    	//Lay ten to can sua
		$sqlcat = "SELECT * FROM `" . NV_PREFIXLANG . "_" . $module_data . "_cat` WHERE  `gvid`='".$gvid."'";
		$resultcat = $db->sql_query( $sqlcat);
		$rowcat = $db->sql_fetchrow( $resultcat);
	    $contents .= "<form name=\"edit_cat\" action=\"\" method=\"post\">";
	    $contents .= "<table summary=\"\" class=\"tab1\">\n";
	    $contents .= "<td>";
	    $contents .= "<center><b><font color=blue size=\"2\">" . $lang_module['edit_cat'] . "</font></b></center>";
	    $contents .= "<center><font size=\"2\">" . $lang_module['gv'] . "</font>";
	    $contents .= "&nbsp;&nbsp;&nbsp;<input type=\"text\" name=\"editcat\" size = \"30\" value=\"".stripslashes($rowcat[2])."\"/>";
   	    $contents .= "&nbsp;&nbsp;&nbsp;<font size=\"2\">" . $lang_module['thuocto'] . "</font>";
		$contents .= "&nbsp;&nbsp;&nbsp;<select name=\"keywords\">";
		$contents .= "<option value=\"0\" size = \"30\">&nbsp;Chọn tổ</option>";
		$sqlto = "SELECT * FROM `" . NV_PREFIXLANG . "_" . $module_data . "_to`";
		$resultto = $db->sql_query( $sqlto);
		//$dsto = array();
		while ($dsto = $db->sql_fetchrow($resultto))
		{
			$sel = (stripslashes($rowcat[1])==$dsto[0])?' selected':'';	
			$contents .= "<option value=\"$dsto[0]\" ".$sel.">&nbsp;$dsto[1]</option>";
		}
		$contents .= "</select>";
	    $contents .= "&nbsp;&nbsp;&nbsp;<input type=\"submit\" name=\"submitedit\" value = \"" . $lang_module['thuchien'] . "\" /></center>";
	    $contents .= "</td>\n";
	    $contents .= "</table>";
		$contents .= "</form>";
    }
    else{
	    $contents .= "<form name=\"add_cat\" action=\"\" method=\"post\">";
	    $contents .= "<table summary=\"\" class=\"tab1\">\n";
	    $contents .= "<td>";
	    $contents .= "<center><b><font color=blue size=\"2\">" . $lang_module['add_cat'] . "</font></b></center>";
	    $contents .= "<center><font size=\"2\">" . $lang_module['gv'] . "</font>";
	    $contents .= "&nbsp;&nbsp;&nbsp;<input type=\"text\" name=\"addcat\" size = \"30\" />";
   	    $contents .= "&nbsp;&nbsp;&nbsp;<font size=\"2\">" . $lang_module['thuocto'] . "</font>";
		// Load danh sach cac to
		$contents .= "&nbsp;&nbsp;&nbsp;<select name=\"keywords\" >";
		$contents .= "<option value=\"0\" size = \"30\">&nbsp;Chọn tổ</option>";
		$sqlto = "SELECT * FROM `" . NV_PREFIXLANG . "_" . $module_data . "_to`";
		$resultto = $db->sql_query( $sqlto);
		//$dsto = array();
		while ($dsto = $db->sql_fetchrow($resultto))
		{
			$contents .= "<option value=\"$dsto[0]\">&nbsp;$dsto[1]</option>";
		}
		$contents .= "</select>";
	    $contents .= "&nbsp;&nbsp;&nbsp;<input type=\"submit\" name=\"submitadd\" value = \"" . $lang_module['thuchien'] . "\" /></center>";
	    $contents .= "</td>\n";
	    $contents .= "</table>";
		$contents .= "</form>";
    }
    //Hien thi thong bao loi
    if(!empty($error)){
		$contents .= "<div class=\"quote\" style=\"width:780px;\">\n";
		$contents .= "<blockquote class='error'><span id='message'>" .$error. "</span></blockquote>\n";
		$contents .= "</div>\n";
    }

    $contents .= "<table class=\"tab1\">\n";
    $contents .= "<thead>\n";
    $contents .= "<tr>\n";
    $contents .= "<td align=\"center\">" . $lang_module['stt'] . "</td>\n";
    $contents .= "<td align=\"center\">" . $lang_module['gvid'] . "</td>\n";
    $contents .= "<td align=\"center\">" . $lang_module['gv'] . "</td>\n";
    $contents .= "<td align=\"center\">" . $lang_module['thuocto'] . "</td>\n";
    $contents .= "<td align=\"center\">" . $lang_module['quanli'] . "</td>\n";
    $contents .= "</tr>\n";
    $contents .= "</thead>\n";
    $a = 0;
    $b=1;
    while ( $row = $db->sql_fetchrow( $result ) )
    {
        $class = ( $a % 2 ) ? " class=\"second\"" : "";
        $contents .= "<tbody" . $class . ">\n";
        $contents .= "<tr>\n";
        $contents .= "<td align=\"center\">" . $b. "</td>\n";
        $contents .= "<td align=\"center\">" . $row['gvid'] . "</td>\n";
        $contents .= "<td>" . $row['tengv'] . "</td>\n";
        $contents .= "<td align=\"center\">" . $row['tento'] . "</td>\n";
        $contents .= "<td align=\"center\"><span class=\"edit_icon\"><a href=\"index.php?" . NV_NAME_VARIABLE . "=" . $module_name . "&" . NV_OP_VARIABLE . "=quanli_cat&amp;gvid=" . $row['gvid'] . "\">" . $lang_global['edit'] . "</a></span>\n";
        $contents .= "&nbsp;-&nbsp;<span class=\"delete_icon\"><a href=\"index.php?" . NV_NAME_VARIABLE . "=" . $module_name . "&" . NV_OP_VARIABLE . "=quanli_cat&amp;delid=" . $row['gvid'] . "\">" . $lang_global['delete'] . "</a></span></td>\n";
        $contents .= "</tr>\n";
        $contents .= "</tbody>\n";
        $a ++;
        $b ++;
    }
    $contents .= "</table>\n";
    include ( NV_ROOTDIR . "/includes/header.php" );
    echo nv_admin_theme( $contents );
    include ( NV_ROOTDIR . "/includes/footer.php" );
}
else
{
	$contents .= "<div id='edit'></div>\n";
	$contents .= "<div class=\"quote\" style=\"width:780px;\">\n";
	$contents .= "<blockquote class='error'><span id='message'>" . $lang_module ['quanli_err'] . "</span></blockquote>\n";
	$contents .= "</div>\n";
	include (NV_ROOTDIR . "/includes/header.php");
	echo nv_admin_theme ( $contents);
	include (NV_ROOTDIR . "/includes/footer.php");
}
?>