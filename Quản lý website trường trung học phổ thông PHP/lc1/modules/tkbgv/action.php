<?php

/**
 * @Author GaNguCay (gangucay@gmail.com)
 * @copyright Freeware
 * @createdate 11/08/2010
 */

if ( ! defined( 'NV_IS_FILE_MODULES' ) ) die( 'Stop!!!' );

$sql_drop_module = array();

$sql_drop_module[] = "DROP TABLE IF EXISTS `" . $db_config['prefix'] . "_" . $lang . "_" . $module_data . "`";
$sql_drop_module[] = "DROP TABLE IF EXISTS `" . $db_config['prefix'] . "_" . $lang . "_" . $module_data . "_cat`";
$sql_drop_module[] = "DROP TABLE IF EXISTS `" . $db_config['prefix'] . "_" . $lang . "_" . $module_data . "_to`";

$sql_create_module = $sql_drop_module;

$sql_create_module[] = "CREATE TABLE `" . $db_config['prefix'] . "_" . $lang . "_" . $module_data . "` (
  `id` INT(4) NOT NULL auto_increment,
  `gvid` INT(3) NOT NULL,
  `tiet` INT(2) NOT NULL,
  `thu2` varchar(15) NULL,
  `thu3` varchar(15) NULL,
  `thu4` varchar(15) NULL,
  `thu5` varchar(15) NULL,
  `thu6` varchar(15) NULL,
  `thu7` varchar(15) NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8";

$sql_create_module[] = "CREATE TABLE `" . $db_config['prefix'] . "_" . $lang . "_" . $module_data . "_cat` (
  `gvid` INT(3) NOT NULL auto_increment,
  `toid` INT(2) NOT NULL,
  `tengv` varchar(40) NOT NULL,
  PRIMARY KEY  (`gvid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8";

$sql_create_module[] = "CREATE TABLE `" . $db_config['prefix'] . "_" . $lang . "_" . $module_data . "_to` (
  `toid` INT(2) NOT NULL auto_increment,
  `tento` varchar(30) NOT NULL,
  PRIMARY KEY  (`toid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8";
?>