<?php
/**
 * @Author GaNguCay (gangucay@gmail.com)
 * @copyright Freeware
 * @createdate 11/08/2010
 */

if (! defined ( 'NV_SYSTEM' ))
	die ( 'Stop!!!' );
define ( 'NV_IS_MOD_TKBGV', true );
function creat_config($str) {
	if (! file_exists ( NV_ROOTDIR . '/' . NV_DATADIR . '/tkbgv/' . $str . ".txt" )) {
		$f = fopen ( NV_ROOTDIR . '/' . NV_DATADIR . '/tkbgv/' . $str . ".txt", "w" );
		if ($str == 'config') {
			fwrite ( $f, 'H&#7879; th&#7889;ng tra c&#7913;u TKB gi�o vi�n c&#7911;a tr&#432;&#7901;ng THPT Vinh L&#7897;c|20/08/2010|7h00|13h00' );
		}
		fclose ( $f );
	}
}
function is_admin($nickname) {
	global $module_data, $db;
	$sql = "SELECT a.userid FROM " . NV_USERS_GLOBALTABLE . " a INNER JOIN " . NV_AUTHORS_GLOBALTABLE . " b ON a.userid=b.admin_id WHERE a.username=" . $db->dbescape ( $nickname ) . "";
	list ( $userid ) = $db->sql_fetchrow ( $db->sql_query ( $sql ) );
	return ($userid) ? 1 : 0;
}
?>