<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
$theimgurl = $config['imageurl'];

$USERID = intval($_REQUEST['id']);
if($USERID > 0)
{	
	STemplate::assign('id',$USERID);
	$query = "SELECT * FROM members WHERE USERID='".mysql_real_escape_string($USERID)."' AND status='1'";
	$executequery = $conn->execute($query);
	$profilearray = $executequery->getarray();
	STemplate::assign('p',$profilearray[0]);
	$username = $profilearray[0]['username'];
	
	$public = $profilearray[0]['public'];
	
	if($public == "0")
	{
		$ME = intval($_SESSION['USERID']);
		if($ME > 0)
		{
			if($USERID == $ME)
			{
				$display = "1";
			}
			else
			{
				$is_fr = check_friend($ME, $USERID);
				if($is_fr == "1")
				{
					$display = "1";
				}
				else
				{
					$display = "2";
				}
			}
		}
		else
		{
			$display = "3";
		}
	}
	else
	{
		$ME = intval($_SESSION['USERID']);
		if($ME > 0)
		{
			$query="SELECT count(*) as total FROM block WHERE USERID='".mysql_real_escape_string($USERID)."' AND BID='".mysql_real_escape_string($ME)."'";
			$executequery=$conn->execute($query);
			$block_count = $executequery->fields[total];
			if($block_count > 0)
			{
				$display = "4";
			}
			else
			{
				$display = "1";
			}
		}
		else
		{
			$display = "1";
		}
	}
	
	
	if($display == "1")
	{
	
		$page = intval($_REQUEST['page']);
	
		if($page=="")
		{
			$page = "1";
		}
		$currentpage = $page;
		
		if ($page >=2)
		{
			$pagingstart = ($page-1)*$config['max_posts_userfollowing'];
		}
		else
		{
			$pagingstart = "0";
		}
		
		$sort = $_REQUEST['sort'];
		if($sort == "date_added")
		{
			$addto = "B.ID desc";
		}
		else
		{
			$addto = "A.username asc";
		}
	
		$query1 = "SELECT A.USERID FROM members A, follow B WHERE B.USERID='".mysql_real_escape_string($USERID)."' AND A.USERID=B.FID";	
		
		$query2 = "SELECT A.username, A.USERID, A.saying, A.city, A.country, A.public FROM members A, follow B WHERE B.USERID='".mysql_real_escape_string($USERID)."' AND A.USERID=B.FID order by $addto limit $pagingstart, $config[max_posts_userfollowing]";
				
		$executequery1 = $conn->Execute($query1);
		
		$totalposts = count($executequery1->getrows());
		if ($totalposts > 0)
		{
			if($totalposts<=$config['maximum_results'])
			{
				$total = $totalposts;
			}
			else
			{
				$total = $config[maximum_results];
			}
			
			$toppage = ceil($total/$config['max_posts_userfollowing']);
			if($toppage==0)
			{
				$xpage=$toppage+1;
			}
			else
			{
				$xpage = $toppage;
			}
			
			$executequery2 = $conn->Execute($query2);
			$posts = $executequery2->getrows();
			$beginning=$pagingstart+1;
			$ending=$pagingstart+$executequery2->recordcount();
			$pagelinks="";
			$k=1;
			$theprevpage=$currentpage-1;
			$thenextpage=$currentpage+1;
			
			if ($currentpage > 0)
			{
				STemplate::assign('page',$currentpage);
				if($currentpage > 1) 
				{
					$pagelinks.="<a href='$thebaseurl/viewfollowing.php?id=$USERID&page=$theprevpage' class=\"standardButton leftArrow\"><span><img src=\"$theimgurl/arrow_left.gif\" width=\"4\" height=\"8\" /></span></a>";
				}
				
				$counter=0;
				
				$lowercount = $currentpage-5;
				if ($lowercount <= 0) $lowercount = 1;
				
				while ($lowercount < $currentpage)
				{
					$pagelinks.="<a href='$thebaseurl/viewfollowing.php?id=$USERID&page=$lowercount' class=\"page\">$lowercount</a>";
					$lowercount++;
					$counter++;
				}
				
				$pagelinks.="<div class=\"currentPage\">$currentpage</div>";
				
				$uppercounter = $currentpage+1;
				
				while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
				{
					$pagelinks.="<a href='$thebaseurl/viewfollowing.php?id=$USERID&page=$uppercounter' class=\"page\">$uppercounter</a>";
					$uppercounter++;
				}
				
				if($currentpage < $toppage) 
				{
					$pagelinks.="<a href='$thebaseurl/viewfollowing.php?id=$USERID&page=$thenextpage' class=\"standardButton rightArrow\"><span><img src=\"$theimgurl/arrow_right.gif\" width=\"4\" height=\"8\" /></span></a>";
				}
			}
		}
		STemplate::assign('posts',$posts);
		
		get_bg($USERID);
		$templateselect = "viewfollowing.tpl";
	}
	else
	{
		if($display == "2")
		{
			if($_REQUEST['saddprifr'] == "1")
			{
				$FID = intval($_SESSION['USERID']);
				if($FID > 0)
				{
					$fquery="INSERT INTO messages_inbox SET MSGTO='".mysql_real_escape_string($USERID)."', MSGFROM='".mysql_real_escape_string($FID)."', type='fr', time='".time()."'";
					$conn->execute($fquery);
					$msg = $lang['216'];
				}
			}
		}
		STemplate::assign('display',$display);
		$templateselect = "profile_private.tpl";
	}
}
else
{
	header("Location:$config[baseurl]");exit;
}

if($username != "")
{
	$pagetitle = $username." - ";
}
$pagetitle .= $lang[120];
STemplate::assign('pagetitle',$pagetitle);

//TEMPLATES BEGIN
STemplate::assign('beginning',$beginning);
STemplate::assign('ending',$ending);
STemplate::assign('pagelinks',$pagelinks);
STemplate::assign('total',$total+0);
STemplate::assign('error',$error);
STemplate::display('header.tpl');
STemplate::display($templateselect);
STemplate::display('footer.tpl');
//TEMPLATES END
?>