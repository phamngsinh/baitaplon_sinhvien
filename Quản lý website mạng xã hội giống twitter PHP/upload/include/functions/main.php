<?php

function escape($data)
{
    if (ini_get('magic_quotes_gpc'))
	{
    	$data = stripslashes($data);
    }
    return mysql_real_escape_string($data);
}

function insert_get_advertisement($var)
{
        global $conn;
        $query="SELECT code FROM advertisements WHERE AID='".mysql_real_escape_string($var[AID])."' AND active='1' limit 1";
        $executequery=$conn->execute($query);
        $getad = $executequery->fields[code];
		echo strip_mq_gpc($getad);
}

function insert_get_seo_profile($a)
{
        $uname = $a['username'];
		$uname = preg_replace('/[^a-z0-9-_* ]/i', '', $uname);
		echo $uname;
}

function verify_login_admin()
{
        global $config,$conn;
        if($_SESSION['ADMINID'] != "" && is_numeric($_SESSION['ADMINID']) && $_SESSION['ADMINUSERNAME'] != "" && $_SESSION['ADMINPASSWORD'] != "")
        {
			$query="SELECT * FROM administrators WHERE username='".mysql_real_escape_string($_SESSION['ADMINUSERNAME'])."' AND password='".mysql_real_escape_string($_SESSION['ADMINPASSWORD'])."' AND ADMINID='".mysql_real_escape_string($_SESSION['ADMINID'])."'";
        	$executequery=$conn->execute($query);
			
			if(mysql_affected_rows()==1)
			{
			
			}
			else
			{
				header("location:$config[adminurl]/index.php");
            	exit;
			}
			
        }
		else
		{
			header("location:$config[adminurl]/index.php");
            exit;
		}
}

function verify_email_username($usernametocheck)
{
    global $config,$conn;
	$query = "select count(*) as total from members where username='".mysql_real_escape_string($usernametocheck)."' limit 1"; 
	$executequery = $conn->execute($query);
	$totalu = $executequery->fields[total];
	if ($totalu >= 1)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function onlyvalidchars($usernametocheck)
{
	$bad = true;
	$array = explode("\r\n",trim(chunk_split($usernametocheck,1)));
	foreach($array as $char)
	{
		if(!eregi("^[a-zA-Z0-9]$", $char))
		{
			$bad = false;
		}
	}

    return $bad;
}

function verify_valid_email($emailtocheck)
{	
	if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $emailtocheck))
	{
		return false;
	}
	else
	{
		return true;
	}

}

function verify_email_unique($emailtocheck)
{
    global $config,$conn;
	$query = "select count(*) as total from members where email='".mysql_real_escape_string($emailtocheck)."' limit 1"; 
	$executequery = $conn->execute($query);
	$totalemails = $executequery->fields[total];
	if ($totalemails >= 1)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="")
{
	global $SERVER_NAME;
	$subject = nl2br($subject);
	$sendmailbody = nl2br($sendmailbody);
	$sendto = $sendto;
	if($bcc!="")
	{
		$headers = "Bcc: ".$bcc."\n";
	}
	$headers = "MIME-Version: 1.0\n";
	$headers .= "Content-type: text/html; charset=utf-8\n";
	$headers .= "X-Priority: 3\n";
	$headers .= "X-MSMail-Priority: Normal\n";
	$headers .= "X-Mailer: PHP/"."MIME-Version: 1.0\n";
	$headers .= "From: " . $from . "\n";
	$headers .= "Content-Type: text/html\n";
	mail("$sendto","$subject","$sendmailbody","$headers");
}

function generateCode($length) 
{
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
    $code = "";
    $clen = strlen($chars) - 1;
    while (strlen($code) < $length) {
        $code .= $chars[mt_rand(0,$clen)];
    }
    return $code;
}

function insert_get_stripped_phrase($a)
{
	$stripper = $a[details]; 
	$stripper = str_replace("\\n", "<br>", $stripper);
	$stripper = str_replace("\\r", "", $stripper);
	$stripper = str_replace("\\", "", $stripper);
	return $stripper;
}

function insert_get_stripped_phrase2($a)
{
	$stripper = $a[details]; 
	$stripper = str_replace("\\n", "\n", $stripper);
	$stripper = str_replace("\\r", "\r", $stripper);
	return $stripper;
}

function listdays($selected)
{
	$days = "";
    for($i=1;$i<=31;$i++)
    {
    	if($i == $selected)
		{
        	$days .= "<option value=\"$i\" selected>$i</option>";
        }
        else
		{
        	$days .= "<option value=\"$i\">$i</option>";
        }
    }
    return $days;
}

function listmonths($selected)
{
	global $lang;
    $months = "";
    $allmonths = array("","$lang[165]","$lang[166]","$lang[167]","$lang[168]","$lang[169]","$lang[170]","$lang[171]","$lang[172]","$lang[173]","$lang[174]","$lang[175]","$lang[176]");
    for($i=1;$i<=12;$i++)
    {
        if($i == $selected)
		{
        	$months .= "<option value=\"$i\" selected>$allmonths[$i]</option>";
        }
        else
		{
        	$months .= "<option value=\"$i\">$allmonths[$i]</option>";
        }
    }
    return $months;
}

function listyears($selected)
{
        $years = "";
        $thisyear = date("Y");
        for($i=$thisyear-100+13;$i<=$thisyear-13;$i++)
        {
                if($i == $selected)
                        $years .= "<option value=\"$i\" selected>$i</option>";
                else
                        $years .= "<option value=\"$i\">$i</option>";
        }
        return $years;
}

function listcountries($selected)
{
    $country="";
    $countries = array("Afghanistan","Albania","Algeria","American Samoa","Andorra","Angola","Anguilla","Antartica","Antigua and Barbuda","Argentina","Armenia","Aruba","Ascension Island","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Botswana","Bouvet Island","Brazil","Brunei Darussalam","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde Islands","Cayman Islands","Chad","Chile","China","Christmas Island","Colombia","Comoros","Cook Islands","Costa Rica","Cote d Ivoire","Croatia/Hrvatska","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","East Timor","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Guiana","French Polynesia","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guernsey","Guinea","Guinea-Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Ireland","Isle of Man","Israel","Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte Island", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Island", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion Island", "Romania", "Russian Federation", "Rwanda", "Saint Helena", "Saint Lucia", "San Marino", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovak Republic", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia", "Spain", "Sri Lanka", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tokelau", "Tonga Islands", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Western Sahara", "Western Samoa", "Yemen", "Yugoslavia", "Zambia","Zimbabwe");
    for($i=0;$i<count($countries);$i++)
    {
    	if($selected == $countries[$i])
        {
			$country .="<option value=\"$countries[$i]\" selected>$countries[$i]</option>";
		}
        else
		{
			$country .="<option value=\"$countries[$i]\">$countries[$i]</option>";
		}
	}
    return $country;
}

function insert_get_propic($var)
{
        global $conn;
        $query="SELECT profilepicture FROM members WHERE USERID='".mysql_real_escape_string($var[USERID])."' limit 1";
        $executequery=$conn->execute($query);
		$results = $executequery->fields[profilepicture];
		if ($results == "")
			return "default.jpg";
		else
			return $var[USERID].$results;
}

function get_propic($var)
{
        global $conn;
        $query="SELECT profilepicture FROM members WHERE USERID='".mysql_real_escape_string($var)."' limit 1";
        $executequery=$conn->execute($query);
		$results = $executequery->fields[profilepicture];
		if ($results == "")
			return "default.jpg";
		else
			return $var.$results;
}

function insert_get_propic1($var)
{
        global $conn;
        $query="SELECT profilepicture FROM members WHERE USERID='".mysql_real_escape_string($var[USERID])."' limit 1";
        $executequery=$conn->execute($query);
		$results = $executequery->fields[profilepicture];
		if ($results == "")
			return "default-s.jpg";
		else
			return $var[USERID]."-s".$results;
}

function insert_get_propic2($var)
{
        global $conn;
        $query="SELECT profilepicture FROM members WHERE USERID='".mysql_real_escape_string($var[USERID])."' limit 1";
        $executequery=$conn->execute($query);
		$results = $executequery->fields[profilepicture];
		if ($results == "")
			return "default-m.jpg";
		else
			return $var[USERID]."-m".$results;
}

function generatethumbs($theconvertimg,$thevideoimgnew,$thewidth,$theheight)
{
	global $config;
    $convertimg = $theconvertimg;
    $videoimgnew = $thevideoimgnew;

    $theimagesizedata = GetImageSize($convertimg);
    $videoimgwidth = $theimagesizedata[0];
    $videoimgheight = $theimagesizedata[1];
    $videoimgformat = $theimagesizedata[2];

    $whratio = 1;
    if ($videoimgwidth > $thewidth)
    {
        $whratio = $thewidth/$videoimgwidth;
    }
	
    if($whratio != 1)
    {
        $dest_height = $whratio * $videoimgheight;
        $dest_width = $thewidth;
    }
    else
    {
        $dest_height=$videoimgheight;
        $dest_width=$videoimgwidth;
    }
	
    if($dest_height > $theheight)
    {
        $whratio = $theheight/$dest_height;
        $dest_width = $whratio * $dest_width;
        $dest_height = $theheight;
    }
	
    if($videoimgformat == 2)
    {
        $videoimgsource = @imagecreatefromjpeg($convertimg);
        $videoimgdest = @imageCreateTrueColor($dest_width, $dest_height);
        ImageCopyResampled($videoimgdest, $videoimgsource, 0, 0, 0, 0, $dest_width, $dest_height, $videoimgwidth, $videoimgheight);
        imagejpeg($videoimgdest, $videoimgnew, 100);
        imagedestroy($videoimgsource);
        imagedestroy($videoimgdest);
    }
    elseif ($videoimgformat == 3)
    {
        $videoimgsource = imagecreatefrompng($convertimg);
        $videoimgdest = imageCreateTrueColor($dest_width, $dest_height);
        ImageCopyResampled($videoimgdest, $videoimgsource, 0, 0, 0, 0, $dest_width, $dest_height, $videoimgwidth, $videoimgheight);
        imagepng($videoimgdest, $videoimgnew, 9);
        imagedestroy($videoimgsource);
        imagedestroy($videoimgdest);
    }
    else
    {
        $videoimgsource = imagecreatefromgif($convertimg);
        $videoimgdest = imageCreateTrueColor($dest_width, $dest_height);
        ImageCopyResampled($videoimgdest, $videoimgsource, 0, 0, 0, 0, $dest_width, $dest_height, $videoimgwidth, $videoimgheight);
        imagejpeg($videoimgdest, $videoimgnew, 100);
        imagedestroy($videoimgsource);
        imagedestroy($videoimgdest);
    }
}

function do_resize_image($file, $width = 0, $height = 0, $proportional = false, $output = 'file')
{
	if($height <= 0 && $width <= 0)
	{
	  return false;
	}
	
	$info = getimagesize($file);
	$image = '';

	$final_width = 0;
	$final_height = 0;
	list($width_old, $height_old) = $info;

	if($proportional) 
	{
	  if ($width == 0) $factor = $height/$height_old;
	  elseif ($height == 0) $factor = $width/$width_old;
	  else $factor = min ( $width / $width_old, $height / $height_old);   
	
	  $final_width = round ($width_old * $factor);
	  $final_height = round ($height_old * $factor);
	
	}
	else 
	{
	  $final_width = ( $width <= 0 ) ? $width_old : $width;
	  $final_height = ( $height <= 0 ) ? $height_old : $height;
	}
	
	switch($info[2]) 
	{
	  case IMAGETYPE_GIF:
		$image = imagecreatefromgif($file);
	  break;
	  case IMAGETYPE_JPEG:
		$image = imagecreatefromjpeg($file);
	  break;
	  case IMAGETYPE_PNG:
		$image = imagecreatefrompng($file);
	  break;
	  default:
		return false;
	}

	$image_resized = imagecreatetruecolor( $final_width, $final_height );

	if(($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG))
	{
	  $trnprt_indx = imagecolortransparent($image);
	
	  if($trnprt_indx >= 0)
	  {
		$trnprt_color    = imagecolorsforindex($image, $trnprt_indx);
		$trnprt_indx    = imagecolorallocate($image_resized, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
		imagefill($image_resized, 0, 0, $trnprt_indx);
		imagecolortransparent($image_resized, $trnprt_indx);	
	  } 
	  elseif($info[2] == IMAGETYPE_PNG) 
	  {
		imagealphablending($image_resized, false);
		$color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
		imagefill($image_resized, 0, 0, $color);
		imagesavealpha($image_resized, true);
	  }
	}
	imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);

	switch( strtolower($output))
	{
	  case 'browser':
		$mime = image_type_to_mime_type($info[2]);
		header("Content-type: $mime");
		$output = NULL;
	  break;
	  case 'file':
		$output = $file;
	  break;
	  case 'return':
		return $image_resized;
	  break;
	  default:
	  break;
	}
	
	if(file_exists($output))
	{
		@unlink($output);
	}

	switch($info[2])
	{
	  case IMAGETYPE_GIF:
		imagegif($image_resized, $output);
	  break;
	  case IMAGETYPE_JPEG:
		imagejpeg($image_resized, $output);
	  break;
	  case IMAGETYPE_PNG:
		imagepng($image_resized, $output);
	  break;
	  default:
		return false;
	}
	return true;
}

function does_post_exist($a)
{
	global $conn, $config;
    $query="SELECT USERID FROM posts WHERE PID='".mysql_real_escape_string($a)."'";
    $executequery=$conn->execute($query);
    if ($executequery->recordcount()>0)
        return true;
    else
		return false;
}

function update_last_viewed($a)
{
        global $conn;
		$query = "UPDATE posts SET last_viewed='".time()."' WHERE PID='".mysql_real_escape_string($a)."'";
        $executequery=$conn->execute($query);
}

function update_your_viewed ($a)
{
        global $conn;
		$query = "UPDATE members SET yourviewed  = yourviewed  + 1 WHERE USERID='".mysql_real_escape_string($a)."'";
        $executequery=$conn->execute($query);
}

function update_you_viewed($a)
{
        global $conn;
		$query = "UPDATE members SET youviewed = youviewed + 1 WHERE USERID='".mysql_real_escape_string($a)."'";
        $executequery=$conn->execute($query);
}

function session_verification()
{
    if ($_SESSION[USERID] != "")
	{
		if (is_numeric($_SESSION[USERID]))
		{
        	return true;
		}
    }
	else
		return false;
}

function insert_get_username_from_userid($var)
{
        global $conn;
        $query="SELECT username FROM members WHERE USERID='".mysql_real_escape_string($var[USERID])."'";
        $executequery=$conn->execute($query);
        $getusername = $executequery->fields[username];
		return "$getusername";
}

function get_username_from_userid($var)
{
        global $conn;
        $query="SELECT username FROM members WHERE USERID='".mysql_real_escape_string($var)."'";
        $executequery=$conn->execute($query);
        $getusername = $executequery->fields[username];
		return "$getusername";
}

function does_profile_exist($a)
{
	global $conn;
    global $config;
    $query="SELECT username FROM members WHERE USERID='".mysql_real_escape_string($a)."'";
    $executequery=$conn->execute($query);
    if ($executequery->recordcount()>0)
        return true;
    else
		return false;
}

function update_viewcount_profile($a)
{
        global $conn;
		$query = "UPDATE members SET profileviews = profileviews + 1 WHERE USERID='".mysql_real_escape_string($a)."'";
        $executequery=$conn->execute($query);
}

function insert_get_posts_count($var)
{
        global $conn;
        $query="SELECT count(*) as total FROM posts WHERE USERID='".mysql_real_escape_string($var[USERID])."'";
        $executequery=$conn->execute($query);
        $results = $executequery->fields[total];
		echo "$results";
}

function insert_get_static($var)
{
        global $conn;
        $query="SELECT $var[sel] FROM static WHERE ID='".mysql_real_escape_string($var[ID])."'";
        $executequery=$conn->execute($query);
        $returnme = $executequery->fields[$var[sel]];
		$returnme = strip_mq_gpc($returnme);
		echo "$returnme";
}

function insert_get_time_to_days_ago($a)
{
	global $lang;
	$currenttime = time();
	$timediff = $currenttime - $a[time];
	$oneday = 60 * 60 * 24;
	$dayspassed = floor($timediff/$oneday);
	if ($dayspassed == "0")
	{
		$mins = floor($timediff/60);
		if($mins == "0")
		{
			$secs = floor($timediff);
			if($secs == "1")
			{
				return $lang['86'];
			}
			else
			{
				return $secs." ".$lang['87'];
			}
		}
		elseif($mins == "1")
		{
			return $lang['88'];
		}
		elseif($mins < "60")
		{
			return $mins." ".$lang['89'];
		}
		elseif($mins == "60")
		{
			return $lang['90'];
		}
		else
		{
			$hours = floor($mins/60);
			return "$hours ".$lang['91'];
		}
	}
	else
	{
		return date("F j, Y",$a[time]);
	}
}

function insert_get_post($a)
{
	global $conn, $config;
    $phrase = $a[post];
	$words = explode(" ", $phrase);
	foreach($words as $word)
	{
		if(substr($word, 0, 1) == "@")
		{
			$word = str_replace(":", "", $word);
			$sword = str_replace("@", "", $word);
			$query = "SELECT USERID FROM members WHERE username='".mysql_real_escape_string($sword)."' limit 1";
			$executequery = $conn->execute($query);
			$GUSERID = intval($executequery->fields[USERID]);
			if($GUSERID > 0)
			{
				$phrase = str_replace($word, "<a href=\"".$config[baseurl]."/".$sword."\">".$word."</a>", $phrase);
			}
		}
		elseif(substr($word, 0, 1) == "#")
		{
			$sword = str_replace("#", "", $word);
			$phrase = str_replace($word, "<a href=\"".$config[baseurl]."/searchupdates.php?query=".urlencode($sword)."\">".$word."</a>", $phrase);
		}
	}
	$phrase = clickable_link($phrase);
	return $phrase;
}


function insert_com_owner($a)
{
    global $config,$conn;
	$query = "SELECT USERID FROM posts where ID='".mysql_real_escape_string($a[id])."' limit 1"; 
	$executequery = $conn->execute($query);
	return $executequery->fields[USERID];
}

function insert_com_reply_info($a)
{
    global $config,$conn;
	$query = "SELECT A.msg, A.time_added, A.USERID, A.UID, B.username FROM posts A, members B where A.ID='".mysql_real_escape_string($a[ID])."' AND A.USERID=B.USERID limit 1"; 
	$executequery = $conn->execute($query);
	return $executequery->getarray();
}

function insert_com_count($a)
{
    global $config,$conn;
	$query = "SELECT count(*) as total FROM posts where type='com-update' AND UID='".mysql_real_escape_string($a[ID])."'"; 
	$executequery = $conn->execute($query);
	return $executequery->fields[total];
}

function insert_is_new_post($a)
{
    $aday = time() - (24 * 60 * 60);
	if($a['time'] > $aday)
	{
		return "1";
	}
	else
	{
		return "0";
	}
}

function insert_is_new_cpost($a)
{
	global $config,$conn;
	$query = "SELECT time_added FROM posts where type='com-update' AND UID='".mysql_real_escape_string($a[id])."'"; 
	$executequery = $conn->execute($query);
	$time_added = $executequery->fields[time_added];
    $aday = time() - (24 * 60 * 60);
	if($time_added > $aday)
	{
		return "1";
	}
	else
	{
		return "0";
	}
}

function delete_com_update($delcom, $USERID)
{
    global $config,$conn;
	$query="DELETE FROM posts WHERE ID='".mysql_real_escape_string($delcom)."' AND USERID='".mysql_real_escape_string($USERID)."' limit 1";
	$result=$conn->execute($query);
	$query="DELETE FROM posts WHERE ID='".mysql_real_escape_string($delcom)."' AND UIDO='".mysql_real_escape_string($USERID)."' limit 1";
	$result=$conn->execute($query);
	$query="UPDATE posts SET reply='0' WHERE reply='".mysql_real_escape_string($delcom)."'";
	$result=$conn->execute($query);
}

function insert_get_profileviews($var)
{
        global $conn;
        $query="SELECT profileviews FROM members WHERE USERID='".mysql_real_escape_string($var[USERID])."' limit 1";
        $executequery=$conn->execute($query);
		$results = $executequery->fields[profileviews];
		return $results+0;
}

function insert_get_updatecount($var)
{
        global $conn;
        $query="SELECT count(*) as total FROM posts WHERE USERID='".mysql_real_escape_string($var[USERID])."' and type='update'";
        $executequery=$conn->execute($query);
		$results = $executequery->fields[total];
		return $results+0;
}

function insert_get_followerscount($var)
{
        global $conn;
		$query = "SELECT count(*) as total FROM follow WHERE FID='".mysql_real_escape_string($var[USERID])."'";	
        $executequery=$conn->execute($query);
		$results = $executequery->fields[total];
		return $results+0;
}

function insert_get_following($var)
{
        global $conn;
		$query = "SELECT A.username, A.USERID FROM members A, follow B WHERE B.USERID='".mysql_real_escape_string($var[USERID])."' AND A.USERID=B.FID order by rand() limit 18";	
		$executequery = $conn->execute($query);
		$following = $executequery->getarray();
		return $following;
}

function insert_get_following_total($var)
{
        global $conn;
		$query = "SELECT count(*) as total FROM members A, follow B WHERE B.USERID='".mysql_real_escape_string($var[USERID])."' AND A.USERID=B.FID";	
		$executequery = $conn->execute($query);
		$results = $executequery->fields[total];
		return $results+0;
}

function seo_clean_titles_rss($title)
{
	$title = str_replace ( array ('&Agrave;', '&agrave;', '&Aacute;', '&aacute;', '&yacute;', '&Yacute;', '&Egrave;', '&egrave;', '&Eacute;', '&eacute;', '&Igrave;', '&igrave;', '&Iacute;', '&iacute;', '&Ntilde;', '&ntilde;', '&Ograve;', '&ograve;', '&Oacute;', '&oacute;', '&Ugrave;', '&ugrave;', '&Uacute;', '&uacute;', '&Uuml;', '&uuml;', '&Atilde;', '&sup3;', '&aelig;', '&thorn;', '&aacute;', '&eth;', '&uml;', '&ouml;', '&Ouml;'), array ( 'A', 'a', 'A', 'a', 'y', 'Y', 'E', 'e', 'E', 'e', 'I', 'i', 'I', 'i', 'N', 'n', 'O', 'o', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'o', '', 'ae', 'th', 'a', 'd', 'o', 'o', 'O'), $title);
    $title = str_replace("&amp;", "&", $title);
    $title = preg_replace("/&([a-z]+);/", "", $title); 
    $title = str_replace(
            array(":", "?", ".", "!", "$", "^", "*", ",", ";", "'", '"', "%", "~", "@", "#", "[", "]", "<", ">", "\\", "/", "=", "+"), 
                        "", $title);
    $title = str_replace(array("&"), array("und"), $title);
    $title = htmlentities(strtolower($title));
    $title = str_replace ( array ('&auml;', '&ouml;', '&uuml;', '&szlig;'), array ( 'ae', 'oe', 'ue', 'ss'), $title);
    $title = preg_replace("/&([a-z]+);/", "", $title); 
	return "$title";
}

function insert_get_lastupdate($var)
{
        global $conn;
        $query="SELECT ID, msg, time_added FROM posts WHERE USERID='".mysql_real_escape_string($var[USERID])."' and type='update' AND msg!='' order by ID desc limit 1";
        $executequery=$conn->execute($query);
		$udt = $executequery->getarray();
		return $udt;
}

function insert_is_following($var)
{
        global $conn;
        $query="SELECT count(*) as total FROM follow WHERE USERID='".mysql_real_escape_string($var[FID])."' and FID='".mysql_real_escape_string($var[USERID])."'";
        $executequery=$conn->execute($query);
		$results = $executequery->fields[total];
		return $results+0;
}

function insert_is_friend($var)
{
        global $conn;
        $query="SELECT count(*) as total FROM follow WHERE USERID='".mysql_real_escape_string($var[USERID])."' and FID='".mysql_real_escape_string($var[FID])."' AND friend='1'";
        $executequery=$conn->execute($query);
		$results = $executequery->fields[total];
		return $results+0;
}

function insert_req_status($var)
{
        global $conn;
        $query="SELECT count(*) as total FROM messages_inbox WHERE MSGTO='".mysql_real_escape_string($var[USERID])."' and MSGFROM='".mysql_real_escape_string($var[FID])."' AND type='fr'";
        $executequery=$conn->execute($query);
		$results = $executequery->fields[total];
		return $results+0;
}

function insert_get_age($var) 
{
	list($Year, $Month, $Day) = explode("-",$var[DOB]);
	$YearDifference = date("Y") - $Year;
	$MonthDifference = date("m") - $Month;
	$DayDifference = date("d") - $day;
	if ($DayDifference < 0 || $MonthDifference < 0)
	{
		$YearDifference--;
	}
	return $YearDifference;
} 

function get_bg($USERID)
{
	if($USERID > 0)
	{
		global $conn, $config;
		$query = "SELECT bg, showbg, tile FROM members WHERE USERID='".mysql_real_escape_string($USERID)."'"; 
		$executequery = $conn->execute($query);
		$b = $executequery->getarray();
		
		$bg = $b[0][bg];
		$showbg = $b[0][showbg];
		$tile = $b[0][tile];
		if($showbg == "1" && $bg != "")
		{
			if($tile == "1")
			{
				STemplate::assign('custombg',"background-image: url('$config[mbgurl]/$bg'); background-repeat: repeat; background-attachment: scroll; background-position: top center;");
			}
			else
			{
				STemplate::assign('custombg',"background-image: url('$config[mbgurl]/$bg'); background-repeat: no-repeat; background-attachment: fixed;");
			}
		}
	}
}

function insert_trimme($var)
{
	$limit = $var[limit];
	$word = $var[w];
	if($var[w2] != "")
	{
		$word .= " ".$var[w2];
	}
	$return = substr($word, 0, $limit);
	$count = strlen($word);
	if($count > $limit )
	{
		$return .= "...";
	}
	echo $return;
}

function newmail($USERID)
{
        global $conn;
        $query="SELECT count(*) as total FROM messages_inbox WHERE MSGTO='".mysql_real_escape_string($USERID)."' and unread='1'";
        $executequery=$conn->execute($query);
		$results = $executequery->fields[total];
		return $results+0;
}

function update_read($MID)
{
	global $conn;
	$query="UPDATE messages_inbox SET unread='0' WHERE MSGTO='".mysql_real_escape_string(intval($_SESSION[USERID]))."' AND MID='".mysql_real_escape_string(intval($MID))."' AND unread='1'";
	$conn->execute($query);
}

function insert_new_mail($var)
{
	global $conn;
	$query="SELECT count(*) as total FROM messages_inbox WHERE MSGTO='".mysql_real_escape_string($var[USERID])."' and unread='1'";
	$executequery=$conn->execute($query);
	$results = $executequery->fields[total];
	return $results+0;
}

function check_friend($me, $userid)
{
	global $conn;
	$query="SELECT count(*) as total FROM follow WHERE USERID='".mysql_real_escape_string($userid)."' AND FID='".mysql_real_escape_string($me)."' AND friend='1'";
	$executequery=$conn->execute($query);
	$results = $executequery->fields[total];
	if($results > 0)
	{
		return "1";
	}
	else
	{
		return "0";
	}
}

function insert_block_cnt($var)
{
        global $conn;
        $query="SELECT count(*) as total FROM block WHERE USERID='".mysql_real_escape_string($var[USERID])."' AND BID='".mysql_real_escape_string($var[BID])."'";
        $executequery=$conn->execute($query);
		$results = $executequery->fields[total];
		return $results+0;
}

function notify_user($phrase, $id)
{
	global $conn, $config, $lang;
    $phrase = mysql_real_escape_string($phrase);
	$words = explode(" ", $phrase);
	foreach($words as $word)
	{
		if(substr($word, 0, 1) == "@")
		{
			$word = str_replace(":", "", $word);
			$sword = str_replace("@", "", $word);
			$query = "SELECT USERID, email, alert_com FROM members WHERE username='".mysql_real_escape_string($sword)."' limit 1";
			$executequery = $conn->execute($query);
			$GUSERID = intval($executequery->fields[USERID]);
			if($GUSERID > 0)
			{
				//e-mail
				$eusername = $sword;
				$sendto = $executequery->fields['email'];
				$alert_com = $executequery->fields['alert_com'];
				if($alert_com == "1")
				{
					$sendername = $config['site_name'];
					$from = $config['site_email'];
					$subject = $lang['293'];
					$link = $config['baseurl']."/viewupdate.php?id=".$id;
					$sendmailbody = stripslashes($eusername).",<br><br>".$lang['294']."<br><a href=\"".$link."\">".$link."</a><br><br>".$lang['69'].",<br>".$sendername;
					mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
				}
				//end email
			}
		}
	}
	return $phrase;
}

function insert_clickable_link($var)
{
	$text = $var['text'];
	$text = preg_replace('#(script|about|applet|activex|chrome):#is', "\\1:", $text);
	$ret = ' ' . $text;
	$ret = preg_replace("#(^|[\n ])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
	$ret = preg_replace("#(^|[\n ])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
	$ret = preg_replace("#(^|[\n ])([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i", "\\1<a href=\"mailto:\\2@\\3\">\\2@\\3</a>", $ret);
	$ret = substr($ret, 1);
	return $ret;
}

function clickable_link($var)
{
	$text = $var;
	$text = preg_replace('#(script|about|applet|activex|chrome):#is', "\\1:", $text);
	$ret = ' ' . $text;
	$ret = preg_replace("#(^|[\n ])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
	$ret = preg_replace("#(^|[\n ])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
	$ret = preg_replace("#(^|[\n ])([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i", "\\1<a href=\"mailto:\\2@\\3\">\\2@\\3</a>", $ret);
	$ret = substr($ret, 1);
	return $ret;
}

function cleanit($text)
{
	return htmlentities(strip_tags(stripslashes($text)), ENT_COMPAT, "UTF-8");
}

function banned_words_chk($phrase)
{
	global $conn, $config;
	$query = "SELECT word from bans_words";
	$executequery = $conn->Execute($query);
	$bwords = $executequery->getarray();
	$found = 0;
	$words = explode(" ", $phrase);
	foreach($words as $word)
	{
		foreach($bwords as $bword)
		{
			if($word == $bword[0])
			{
				$found++;
			}
			else
			{
				$pos2 = strpos($word, $bword[0]);
				if($pos2 !== false)
				{
					$found++;
				}
			}
		}
	}
	if($found > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

?>