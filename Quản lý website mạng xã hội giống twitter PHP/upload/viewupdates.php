<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
$theimgurl = $config['imageurl'];

$USERID = intval($_REQUEST['id']);
if($USERID > 0)
{	
	STemplate::assign('USERID',$USERID);
	$query = "SELECT * FROM members WHERE USERID='".mysql_real_escape_string($USERID)."' AND status='1'";
	$executequery = $conn->execute($query);
	$profilearray = $executequery->getarray();
	STemplate::assign('p',$profilearray[0]);
	$username = $profilearray[0]['username'];
	
	$public = $profilearray[0]['public'];
	
	if($public == "0")
	{
		$ME = intval($_SESSION['USERID']);
		if($ME > 0)
		{
			if($USERID == $ME)
			{
				$display = "1";
			}
			else
			{
				$is_fr = check_friend($ME, $USERID);
				if($is_fr == "1")
				{
					$display = "1";
				}
				else
				{
					$display = "2";
				}
			}
		}
		else
		{
			$display = "3";
		}
	}
	else
	{
		$ME = intval($_SESSION['USERID']);
		if($ME > 0)
		{
			$query="SELECT count(*) as total FROM block WHERE USERID='".mysql_real_escape_string($USERID)."' AND BID='".mysql_real_escape_string($ME)."'";
			$executequery=$conn->execute($query);
			$block_count = $executequery->fields[total];
			if($block_count > 0)
			{
				$display = "4";
			}
			else
			{
				$display = "1";
			}
		}
		else
		{
			$display = "1";
		}
	}
	
	
	if($display == "1")
	{
	
		$page = intval($_REQUEST['page']);
	
		if($page=="")
		{
			$page = "1";
		}
		$currentpage = $page;
		
		if ($page >=2)
		{
			$pagingstart = ($page-1)*$config['max_posts_userupdates'];
		}
		else
		{
			$pagingstart = "0";
		}
		
		$query1 = "SELECT DISTINCT A.ID FROM posts A, members B WHERE (A.USERID='".mysql_real_escape_string($USERID)."' AND A.USERID=B.USERID AND A.type='update')";	
		
		$query2 = "SELECT DISTINCT A.*, B.username FROM posts A, members B WHERE (A.USERID='".mysql_real_escape_string($USERID)."' AND A.USERID=B.USERID AND A.type='update') order by A.ID desc limit $pagingstart, $config[max_posts_userupdates]";
				
		$executequery1 = $conn->Execute($query1);
		
		$totalposts = count($executequery1->getrows());
		if ($totalposts > 0)
		{
			if($totalposts<=$config['maximum_results'])
			{
				$total = $totalposts;
			}
			else
			{
				$total = $config[maximum_results];
			}
			
			$toppage = ceil($total/$config['max_posts_userupdates']);
			if($toppage==0)
			{
				$xpage=$toppage+1;
			}
			else
			{
				$xpage = $toppage;
			}
			
			$executequery2 = $conn->Execute($query2);
			$posts = $executequery2->getrows();
			$beginning=$pagingstart+1;
			$ending=$pagingstart+$executequery2->recordcount();
			$pagelinks="";
			$k=1;
			$theprevpage=$currentpage-1;
			$thenextpage=$currentpage+1;
			
			if ($currentpage > 0)
			{
				if($currentpage > 1) 
				{
					$pagelinks.="<a href='$thebaseurl/viewupdates.php?id=$USERID&page=$theprevpage' class=\"standardButton leftArrow\"><span><img src=\"$theimgurl/arrow_left.gif\" width=\"4\" height=\"8\" /></span></a>";
				}
				
				$counter=0;
				
				$lowercount = $currentpage-5;
				if ($lowercount <= 0) $lowercount = 1;
				
				while ($lowercount < $currentpage)
				{
					$pagelinks.="<a href='$thebaseurl/viewupdates.php?id=$USERID&page=$lowercount' class=\"page\">$lowercount</a>";
					$lowercount++;
					$counter++;
				}
				
				$pagelinks.="<div class=\"currentPage\">$currentpage</div>";
				
				$uppercounter = $currentpage+1;
				
				while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
				{
					$pagelinks.="<a href='$thebaseurl/viewupdates.php?id=$USERID&page=$uppercounter' class=\"page\">$uppercounter</a>";
					$uppercounter++;
				}
				
				if($currentpage < $toppage) 
				{
					$pagelinks.="<a href='$thebaseurl/viewupdates.php?id=$USERID&page=$thenextpage' class=\"standardButton rightArrow\"><span><img src=\"$theimgurl/arrow_right.gif\" width=\"4\" height=\"8\" /></span></a>";
				}
			}
		}
		else
		{
			$total = 0;	
		}
		STemplate::assign('posts',$posts);
		
		if($_REQUEST['ssendmsg'] == "1")
		{
			$mysub = cleanit($_REQUEST['mysub']);
			$mymsg = cleanit($_REQUEST['mymsg']);
			if($mysub == "")
			{
				$mserr = $lang['240'];
			}
			elseif($mymsg == "")
			{
				$mserr = $lang['241'];
			}
			
			if($mserr == "")
			{
				$FID = intval($_SESSION['USERID']);
				if($FID > 0)
				{
					$fquery="INSERT INTO messages_inbox SET MSGTO='".mysql_real_escape_string($USERID)."', MSGFROM='".mysql_real_escape_string($FID)."', subject='".mysql_real_escape_string($mysub)."', message='".mysql_real_escape_string($mymsg)."', time='".time()."'";
					$conn->execute($fquery);
					
					//e-mail
					$equery="SELECT username, email, alert_msg FROM members WHERE USERID='".mysql_real_escape_string($USERID)."' AND status='1'";
					$executequerye=$conn->execute($equery);
					$eusername = $executequerye->fields['username'];
					$sendto = $executequerye->fields['email'];
					$alert_msg = $executequerye->fields['alert_msg'];
					if($alert_msg == "1")
					{
						$sendername = $config['site_name'];
						$from = $config['site_email'];
						$subject = $lang['282'];
						$link = $thebaseurl."/inbox.php";
						$sendmailbody = stripslashes($eusername).",<br><br>".stripslashes($_SESSION['USERNAME'])." ".$lang['283']."<br><br>".$lang['284']."<br><a href=\"$link\" target=\"_blank\">$link</a><br><br>".$lang['69'].",<br>".$sendername;
						mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
					}
					//end email
				}
				$mserr = $lang['229'];
			}
			else
			{
				STemplate::assign('mysub',$mysub);
				STemplate::assign('mymsg',$mymsg);
			}
			STemplate::assign('mserr',$mserr);
		}
		
		get_bg($USERID);
		$templateselect = "viewupdates.tpl";
	}
	else
	{
		if($display == "2")
		{
			if($_REQUEST['saddprifr'] == "1")
			{
				$FID = intval($_SESSION['USERID']);
				if($FID > 0)
				{
					$fquery="INSERT INTO messages_inbox SET MSGTO='".mysql_real_escape_string($USERID)."', MSGFROM='".mysql_real_escape_string($FID)."', type='fr', time='".time()."'";
					$conn->execute($fquery);
					$msg = $lang['216'];
				}
			}
		}
		STemplate::assign('display',$display);
		$templateselect = "profile_private.tpl";
	}
}
else
{
	header("Location:$config[baseurl]");exit;
}

if($username != "")
{
	$pagetitle = $username." - ";
}
$pagetitle .= $lang[124];
STemplate::assign('pagetitle',$pagetitle);

//TEMPLATES BEGIN
STemplate::assign('beginning',$beginning);
STemplate::assign('ending',$ending);
STemplate::assign('pagelinks',$pagelinks);
STemplate::assign('total',$total);
STemplate::assign('error',$error);
STemplate::display('header.tpl');
STemplate::display($templateselect);
STemplate::display('footer.tpl');
//TEMPLATES END
?>