<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
$theimgurl = $config['imageurl'];

$USERID = intval(cleanit($_SESSION['USERID']));
if ($USERID > 0)
{	
	if($_REQUEST['subedit'] == "1")
	{
		$EID = intval($_REQUEST['EID']);
		if($EID > 0)
		{
			$editComment = cleanit($_REQUEST['editComment'.$EID]);
			if($editComment != "")
			{
				$query = "UPDATE posts SET msg='".mysql_real_escape_string($editComment)."', edited='".time()."' where type='com-update' AND ID='".mysql_real_escape_string($EID)."' AND USERID='".mysql_real_escape_string($USERID)."'"; 
				$executequery = $conn->execute($query);
				$error = $lang['108'];
			}
		}
	}
	elseif($_REQUEST['subeditupdate'] == "1")
	{
		$EID = intval($_REQUEST['EID']);
		if($EID > 0)
		{
			$editComment = cleanit($_REQUEST['editComment'.$EID]);
			if($editComment != "")
			{
				$query = "UPDATE posts SET msg='".mysql_real_escape_string($editComment)."', edited='".time()."' where type='update' AND ID='".mysql_real_escape_string($EID)."' AND USERID='".mysql_real_escape_string($USERID)."'"; 
				$executequery = $conn->execute($query);
				$error = $lang['110'];
			}
		}
	}
	elseif($_REQUEST['subdelcom'] == "1")
	{
		$CID = intval($_REQUEST['CID']);
		if($CID > 0)
		{
			delete_com_update($CID, $USERID);
			$error = $lang['113'];
		}
	}
	elseif($_REQUEST['subdelupdate'] == "1")
	{
		$DID = intval($_REQUEST['DID']);
		if($DID > 0)
		{
			$query = "SELECT pic FROM posts where type='update' AND ID='".mysql_real_escape_string($DID)."' AND USERID='".mysql_real_escape_string($USERID)."'"; 
			$executequery = $conn->execute($query);
			$dpic = $executequery->fields['pic'];
			if($dpic != "")
			{
				$delimg=$config['picdir']."/".$dpic;
				if(file_exists($delimg))
				{
					unlink($delimg);
				}
				$delimg=$config['tpicdir']."/".$DID.".jpg";
				if(file_exists($delimg))
				{
					unlink($delimg);
				}
			}
			
			$query="DELETE FROM posts WHERE ID='".mysql_real_escape_string($DID)."' AND USERID='".mysql_real_escape_string($USERID)."' limit 1";
			$result=$conn->execute($query);
			$query="DELETE FROM posts WHERE UID='".mysql_real_escape_string($DID)."' AND UIDO='".mysql_real_escape_string($USERID)."'";
			$result=$conn->execute($query);
			$error = $lang['114'];
		}
	}
	elseif($_REQUEST['sfilterupdates'] == "1")
	{
		$fil1 = intval($_REQUEST['fil1']);
		$fil2 = intval($_REQUEST['fil2']);
		$fil3 = intval($_REQUEST['fil3']);
		$fil4 = intval($_REQUEST['fil4']);
		$fil5 = intval($_REQUEST['fil5']);
		$query = "UPDATE members SET fil1='".mysql_real_escape_string($fil1)."', fil2='".mysql_real_escape_string($fil2)."', fil3='".mysql_real_escape_string($fil3)."', fil4='".mysql_real_escape_string($fil4)."', fil5='".mysql_real_escape_string($fil5)."' WHERE USERID='".mysql_real_escape_string($USERID)."'"; 
		$executequery = $conn->execute($query);
		$error = $lang['187'];
	}

	if($_REQUEST['subupdate'] == "1")
	{
		$description = cleanit($_REQUEST['description']);
		$def = $lang['83']."...";
		if($description == $def)
		{
			$description = "";
		}
		elseif(banned_words_chk($description))
		{
			$error = $lang['350'];
		}
		$uploadedimage = $_FILES['file']['tmp_name'];
		if($error == "")
		{
		if($description != "" || $uploadedimage != "")
		{		
			$query="INSERT INTO posts SET type='update', USERID='".mysql_real_escape_string($USERID)."', msg='".mysql_real_escape_string($description)."', time_added='".time()."', pip='".$_SERVER['REMOTE_ADDR']."'";
			$result=$conn->execute($query);
			$id = mysql_insert_id();
			
			if($uploadedimage != "")
			{
				$theimageinfo = getimagesize($uploadedimage);
				$thepp = $id;
			
				if($theimageinfo[2] == 1)
				{
					$thepp .= ".gif";
				}
				elseif($theimageinfo[2] == 2)
				{
					$thepp .= ".jpg";
				}
				elseif($theimageinfo[2] == 3)
				{
					$thepp .= ".png";
				}
				else
				{
					$error = "1";
				}
				
				if($error == "")
				{
					$myvideoimgnew=$config['picdir']."/".$thepp;
					if(file_exists($myvideoimgnew))
					{
						unlink($myvideoimgnew);
					}
					$myconvertimg = $_FILES['file']['tmp_name'];
					
					move_uploaded_file($myconvertimg, $myvideoimgnew);
					
					$origwidth = $theimageinfo[0];
					$origheight = $theimageinfo[1];
					$max_width = "568";
					if ($origwidth > $max_width)
					{
						$new_width = $max_width;
						$ratio = $origheight / $origwidth;
						$new_height = ceil($new_width *$ratio);
					}
					else
					{
						$new_width = $origwidth;
						$new_height = $origheight;
					}
									
					generatethumbs($myvideoimgnew,$myvideoimgnew,$new_width,$new_height);
					
					$max_width_thumbs = "100";
					$max_height_thumbs = "75";
					$tothumbdir = $config['tpicdir']."/".$thepp;
					if(file_exists($tothumbdir))
					{
						unlink($tothumbdir);
					}
					generatethumbs($myvideoimgnew,$tothumbdir,$max_width_thumbs,$max_height_thumbs);
					
					$max_width_thumbs2 = "36";
					$max_height_thumbs2 = "27";
					$tothumbdir2 = $config['tpicdir']."/small_".$thepp;
					if(file_exists($tothumbdir2))
					{
						unlink($tothumbdir2);
					}
					generatethumbs($myvideoimgnew,$tothumbdir2,$max_width_thumbs2,$max_height_thumbs2);
					
					if(file_exists($config['picdir']."/".$thepp))
					{
						$query = "UPDATE posts SET pic='$thepp' WHERE ID='$id'";
						$conn->execute($query);
					}
					else
					{
						$error = "1";
					}
				}
				
				if($error == "1" && $description == "")
				{
					$query = "DELETE FROM posts WHERE ID='$id' AND USERID='".mysql_real_escape_string($USERID)."' limit 1 desc";
					$conn->execute($query);
				}
			}
			
			notify_user($description, $id);
			
		}
		}
	}
	
	$page = intval($_REQUEST['page']);

	if($page=="")
	{
		$page = "1";
	}
	$currentpage = $page;
	
	if ($page >=2)
	{
		$pagingstart = ($page-1)*$config['max_posts_userhome'];
	}
	else
	{
		$pagingstart = "0";
	}
	
	$fquery = "SELECT fil1, fil2, fil3, fil4, fil5 FROM members WHERE USERID='".mysql_real_escape_string($USERID)."'"; 
	$sexecutequery = $conn->execute($fquery);
    $f = $sexecutequery->getarray();
	STemplate::assign('f',$f[0]);
	
	$q1 = "SELECT DISTINCT A.ID FROM posts A, members B WHERE (A.USERID='".mysql_real_escape_string($USERID)."' AND A.USERID=B.USERID AND A.type='update') order by A.ID desc";
	$e1 = $conn->Execute($q1);
	$t1 = count($e1->getrows());
	
	if($t1 > 0)
	{
		if($f['0']['fil1'] == "1")
		{
			$one = "(A.USERID='".mysql_real_escape_string($USERID)."' AND A.USERID=B.USERID AND A.type='update')";
		}
	}
	
	$q2 = "SELECT DISTINCT A.ID FROM posts A, members B WHERE (A.UIDO='".mysql_real_escape_string($USERID)."' AND A.USERID=B.USERID AND A.type='com-update') order by A.ID desc";
	$e2 = $conn->Execute($q2);
	$t2 = count($e2->getrows());
	
	if($t2 > 0)
	{
		if($f['0']['fil2'] == "1")
		{
			$two = "(A.UIDO='".mysql_real_escape_string($USERID)."' AND A.USERID=B.USERID AND A.type='com-update')";
		}
	}
		
	$q3 = "SELECT DISTINCT A.ID FROM posts A, members B WHERE (A.USERID='".mysql_real_escape_string($USERID)."' AND A.UIDO!='".mysql_real_escape_string($USERID)."' AND A.USERID=B.USERID AND A.type='com-update') order by A.ID desc";
	$e3 = $conn->Execute($q3);
	$t3 = count($e3->getrows());
	
	if($t3 > 0)
	{
		if($f['0']['fil3'] == "1")
		{
			$three = "(A.USERID='".mysql_real_escape_string($USERID)."' AND A.UIDO!='".mysql_real_escape_string($USERID)."' AND A.USERID=B.USERID AND A.type='com-update')";
		}
	}
	
	$q4 = "SELECT DISTINCT A.ID FROM posts A, members B, follow C WHERE (C.USERID='".mysql_real_escape_string($USERID)."' AND A.USERID!='".mysql_real_escape_string($USERID)."' AND C.FID=B.USERID AND A.USERID=C.FID AND A.type='update') order by A.ID desc";
	$e4 = $conn->Execute($q4);
	$t4 = count($e4->getrows());
	
	if($t4 > 0)
	{
		if($f['0']['fil4'] == "1")
		{
			$four = "(C.USERID='".mysql_real_escape_string($USERID)."' AND A.USERID!='".mysql_real_escape_string($USERID)."' AND C.FID=B.USERID AND A.USERID=C.FID AND A.type='update')";
		}
		$showc = ", follow C";
	}
	
	$q5 = "SELECT DISTINCT A.ID FROM posts A, members B, follow C WHERE (C.USERID='".mysql_real_escape_string($USERID)."' AND A.USERID!='".mysql_real_escape_string($USERID)."' AND C.FID=B.USERID AND A.USERID=C.FID AND A.type='com-update') order by A.ID desc";
	$e5 = $conn->Execute($q5);
	$t5 = count($e5->getrows());
	
	if($t5 > 0)
	{
		if($f['0']['fil5'] == "1")
		{
			$five = "(C.USERID='".mysql_real_escape_string($USERID)."' AND A.USERID!='".mysql_real_escape_string($USERID)."' AND C.FID=B.USERID AND A.USERID=C.FID AND A.type='com-update')";
		}
		$showc = ", follow C";
	}
	
	if($one != "")
	{
		$full = $one;
		if($two != "" OR $three != "" OR $four != "" OR $five != "")
		{
			$full .= " OR ";
		}
	}
	if($two != "")
	{
		$full .= $two;
		if($three != "" OR $four != "" OR $five != "")
		{
			$full .= " OR ";
		}
	}
	if($three != "")
	{
		$full .= $three;
		if($four != "" OR $five != "")
		{
			$full .= " OR ";
		}
	}
	if($four != "")
	{
		$full .= $four;
		if($five != "")
		{
			$full .= " OR ";
		}
	}
	if($five != "")
	{
		$full .= $five;
	}
	
	if($one != "" OR $two != "" OR $three != "" OR $four != "" OR $five != "")
	{	
	
		$query1 = "SELECT DISTINCT A.ID FROM posts A, members B $showc WHERE $full";	
		
		$query2 = "SELECT DISTINCT A.*, B.username FROM posts A, members B $showc WHERE $full order by A.ID desc";
		
		$executequery1 = $conn->Execute($query1);
		
		$totalposts = count($executequery1->getrows());
		if ($totalposts > 0)
		{
			if($totalposts<=$config['maximum_results'])
			{
				$total = $totalposts;
			}
			else
			{
				$total = $config[maximum_results];
			}
			
			$toppage = ceil($total/$config['max_posts_userhome']);
			if($toppage==0)
			{
				$xpage=$toppage+1;
			}
			else
			{
				$xpage = $toppage;
			}
			
			$executequery2 = $conn->Execute($query2);
			$posts = $executequery2->getrows();
			$beginning=$pagingstart+1;
			$ending=$pagingstart+$executequery2->recordcount();
			$pagelinks="";
			$k=1;
			$theprevpage=$currentpage-1;
			$thenextpage=$currentpage+1;
			
			if ($currentpage > 0)
			{
				if($currentpage > 1) 
				{
					$pagelinks.="<a href='$thebaseurl/home.php?page=$theprevpage' class=\"standardButton leftArrow\"><span><img src=\"$theimgurl/arrow_left.gif\" width=\"4\" height=\"8\" /></span></a>";
				}
				
				$counter=0;
				
				$lowercount = $currentpage-5;
				if ($lowercount <= 0) $lowercount = 1;
				
				while ($lowercount < $currentpage)
				{
					$pagelinks.="<a href='$thebaseurl/home.php?page=$lowercount' class=\"page\">$lowercount</a>";
					$lowercount++;
					$counter++;
				}
				
				$pagelinks.="<div class=\"currentPage\">$currentpage</div>";
				
				$uppercounter = $currentpage+1;
				
				while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
				{
					$pagelinks.="<a href='$thebaseurl/home.php?page=$uppercounter' class=\"page\">$uppercounter</a>";
					$uppercounter++;
				}
				
				if($currentpage < $toppage) 
				{
					$pagelinks.="<a href='$thebaseurl/home.php?page=$thenextpage' class=\"standardButton rightArrow\"><span><img src=\"$theimgurl/arrow_right.gif\" width=\"4\" height=\"8\" /></span></a>";
				}
			}
		}
		STemplate::assign('posts',$posts);
		
	}
	
	$query = "SELECT A.username, A.USERID FROM members A, follow B WHERE B.USERID='".mysql_real_escape_string($USERID)."' AND A.USERID=B.FID order by rand() limit 18";	
	$executequery = $conn->execute($query);
	$following = $executequery->getarray();
	STemplate::assign('following',$following);
	
	$query = "SELECT A.username, A.USERID FROM members A, follow B WHERE B.FID='".mysql_real_escape_string($USERID)."' AND A.USERID=B.USERID order by rand() limit 18";	
	$executequery = $conn->execute($query);
	$followers = $executequery->getarray();
	STemplate::assign('followers',$followers);
	
	get_bg($USERID);
	$templateselect = "home.tpl";
}
else
{
	$redirect = base64_encode($config['baseurl']."/home.php");
	header("Location:$config[baseurl]/login.php?redirect=$redirect");exit;
}

$pagetitle = $lang[79];
STemplate::assign('pagetitle',$pagetitle);

//TEMPLATES BEGIN
STemplate::assign('pagingstart',$pagingstart);
STemplate::assign('beginning',$beginning);
STemplate::assign('ending',$ending);
STemplate::assign('pagelinks',$pagelinks);
STemplate::assign('total',$total);
STemplate::assign('error',$error);
STemplate::display('header.tpl');
STemplate::display($templateselect);
STemplate::display('footer.tpl');
//TEMPLATES END
?>