<?php
header('Content-type: text/css');
include("../include/config.php");
$imageurl = $config['imageurl'];
?>
a.yellowButton { background-image: url(<?php echo $imageurl; ?>/ie6/btn_yellow_left.gif); }
a.yellowButton span { background-image: url(<?php echo $imageurl; ?>/ie6/btn_yellow_right.gif); }
#rightPanel .gridPaging { width: 311px; }
#rightPanel .gridPaging .grid { width: 293px; }
#rightPanel .gridPaging #updatesArea { height: 34px; }
a.tooltip:hover div.top { background-image: url(<?php echo $imageurl; ?>/ie6/tooltip_top.gif); }
a.tooltip:hover div.middle { background-image: url(<?php echo $imageurl; ?>/ie6/tooltip_bg.gif); }
a.tooltip:hover div.bottom { background-image: url(<?php echo $imageurl; ?>/ie6/tooltip_btm.gif); }
a.tooltip.medium:hover div.top {  background-image: url(<?php echo $imageurl; ?>/ie6/tooltip_top_med.gif); }
a.tooltip.medium:hover div.middle { background-image: url(<?php echo $imageurl; ?>/ie6/tooltip_bg_med.gif); }
a.tooltip.medium:hover div.bottom { background-image: url(<?php echo $imageurl; ?>/ie6/tooltip_btm_med.gif); }
#footer { background-image: url(<?php echo $imageurl; ?>/ie6/footer.gif); }
#footer #links { display: inline; }
#home { background: none; background-color: #ffffff; }
#home #sidebar .box .error { line-height: 16px; }
#home #sidebar .box select { display: inline; }
#home #sidebar #joinForm .birthdayFields { display: inline; }
.largeBrick { display: inline; }
.findFriends .nameFieldArea { width: 211px; }
#alertBox .top .profilePic { display: inline; }
#alertBox .top .content { display: inline; }
#nav { background-image: url(<?php echo $imageurl; ?>/ie6/nav.gif); }
#nav .navLogo { background-image: url(<?php echo $imageurl; ?>/ie6/logo.gif); display: inline; }
#nav .buttons a.btn { background-image: url(<?php echo $imageurl; ?>/ie6/btn_left.gif); }
#nav .buttons a.btn span { background-image: url(<?php echo $imageurl; ?>/ie6/btn_right.gif); }
#nav .buttons a.btn.icon span div.settingsIcon { background-image: url(<?php echo $imageurl; ?>/ie6/icon_settings.gif); }
#nav #navHome #homeBtnIsNew, #nav #navHome #homeBtnIsNew2 { background-image: url(<?php echo $imageurl; ?>/ie6/icon_whatsnew_bubble_small2.gif); }
#nav #navHome .dropdown .links a.new span { background-image: url(<?php echo $imageurl; ?>/ie6/icon_whatsnew_bubble_small2.gif); }
#nav #navHome .dropdown .top { background-image: url(<?php echo $imageurl; ?>/ie6/dropdown_home_top.gif); }
#nav #navHome .dropdown .btm { background-image: url(<?php echo $imageurl; ?>/ie6/dropdown_home_btm.gif); }
#nav #navTopics .dropdown .top { background-image: url(<?php echo $imageurl; ?>/ie6/dropdown_explore_top.gif); }
#nav #navTopics .dropdown .btm { background-image: url(<?php echo $imageurl; ?>/ie6/dropdown_explore_btm.gif); }
#nav #navSettings .dropdown .top { background-image: url(<?php echo $imageurl; ?>/ie6/dropdown_settings_top.gif); }
#nav #navSettings .dropdown .btm { background-image: url(<?php echo $imageurl; ?>/ie6/dropdown_settings_btm.gif); }
#nav #navSettings .dropdown a.icon { background-image: url(<?php echo $imageurl; ?>/ie6/icon_settings2.gif); display: inline; }
#nav #navSearch a.searchBtn { background-image: url(<?php echo $imageurl; ?>/ie6/btn_search.gif); }
#nav #navSearch .searchBox { background-image: url(<?php echo $imageurl; ?>/ie6/search_input2.gif); }