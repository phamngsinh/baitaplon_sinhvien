<?php
header('Content-type: text/css');
include("../include/config.php");
$imageurl = $config['imageurl'];
?>
body {
	margin:0px;
	padding:0px;
	font-size:11px;
	font-family:Arial,Helvetica,sans-serif,serif;
	background: #b7ebf7 url(<?php echo $imageurl; ?>/bg.gif) top left repeat-x;
}
a {
	color:#0e6db7;
	text-decoration:none;
}
a:hover {
	text-decoration:underline;
}
a:active {
	ooutline:none;
}
img {
	border:0px;
}
input {
	font-size:13px;
	font-family:"Trebuchet MS";
}
h1,h2,h3,h4,h5,h6 {
	font-weight:normal;
}
.standardButton2 {
	background:url(<?php echo $imageurl; ?>/btn_left.gif) top left no-repeat;
	padding-left:10px;
    padding-right:0px;
}
input.standardButton {
	background:url(<?php echo $imageurl; ?>/btn_right.gif) top right no-repeat;
    padding-left:0px;
	padding-right:10px;
    border: 0px;
    display:block;
	float:left;
	height:24px;
	line-height:24px;
	text-decoration:none;
	font-family:Arial,Helvetica,sans-serif;
	font-size:11px;
	color:#0e6db7;
	text-align:center;
	white-space:nowrap;
	text-transform:lowercase;
	cursor:pointer;
}
a.standardButton,a.mediumButton,a.standardButton span,a.mediumButton span {
	display:block;
	float:left;
	height:24px;
	line-height:24px;
	text-decoration:none;
	font-family:Arial,Helvetica,sans-serif;
	font-size:11px;
	color:#0e6db7;
	text-align:center;
	white-space:nowrap;
	text-transform:lowercase;
	cursor:pointer;
}
a.standardButton,a.mediumButton {
	background:url(<?php echo $imageurl; ?>/btn_left.gif) top left no-repeat;
	padding-left:10px;
}
a.standardButton span,a.mediumButton span {
	background:url(<?php echo $imageurl; ?>/btn_right.gif) top right no-repeat;
	padding-right:10px;
}
a.standardButton:hover,a.mediumButton:hover {
	background-position:0% -24px;
	text-decoration:none;
}
a.standardButton:hover span,a.mediumButton:hover span {
	background-position:100% -24px;
	text-decoration:none;
}
a.mediumButton {
	width:60px;
}
a.mediumButton span {
	width:50px;
}
a.yellowButton,a.yellowButton span {
	display:block;
	float:left;
	height:32px;
	line-height:32px;
	text-decoration:none;
	font-family:Arial,Helvetica,sans-serif;
	font-size:18px;
	color:#000;
	text-align:center;
	white-space:nowrap;
	cursor:pointer;
}
a.yellowButton {
	background:url(<?php echo $imageurl; ?>/btn_yellow_left.png) top left no-repeat;
	padding-left:25px;
}
a.yellowButton span {
	background:url(<?php echo $imageurl; ?>/btn_yellow_right.png) top right no-repeat;
	padding-right:25px;
}
a.yellowButton:hover {
	background-position:0% -32px;
	text-decoration:none;
}
a.yellowButton:hover span {
	background-position:100% -32px;
	text-decoration:none;
}
a.smallYellowButton,a.smallYellowButton span {
	display:block;
	float:left;
	height:24px;
	line-height:24px;
	font-family:Arial,Helvetica,sans-serif;
	font-size:12px;
	font-weight:bold;
	color:#000;
	text-align:center;
	text-decoration:none;
	white-space:nowrap;
	cursor:pointer;
}
a.smallYellowButton {
	background:url(<?php echo $imageurl; ?>/btn_yellow_small_left.gif) top left no-repeat;
	padding-left:10px;
}
a.smallYellowButton span {
	background:url(<?php echo $imageurl; ?>/btn_yellow_small_right.gif) top right no-repeat;
	padding-right:10px;
}
a.smallYellowButton:hover {
	background-position:0% -24px;
	text-decoration:none;
}
a.smallYellowButton:hover span {
	background-position:100% -24px;
	text-decoration:none;
}
a.navButton,a.navButton span {
	display:block;
	float:left;
	height:28px;
	line-height:25px;
	text-decoration:none;
	font-family:Arial,Helvetica,sans-serif;
	font-size:16px;
	color:#fff;
	text-align:center;
	white-space:nowrap;
	cursor:pointer;
}
a.navButton {
	background:url(<?php echo $imageurl; ?>/btn_nav_left.gif) top left no-repeat;
	padding-left:8px;
}
a.navButton span {
	background:url(<?php echo $imageurl; ?>/btn_nav_right.gif) top right no-repeat;
	padding-right:8px;
}
a.navButton:hover {
	background-position:0% -28px;
	text-decoration:none;
}
a.navButton:hover span {
	background-position:100% -28px;
	text-decoration:none;
}
.hidden {
	position:absolute;
	top:-5000px;
	left:0px;
	width:1px;
	height:1px;
}
.clear {
	clear:both;
	line-height:0px;
	height:0px;
}
.floatLeft {
	float:left;
}
.floatClearLeft {
	float:left;
	clear:left;
}
.floatRight {
	float:right;
}
.error,.warning {
	font-family:Arial,Helvetica,sans-serif;
	font-size:12px;
	color:#ff8a00;
	font-style:italic;
}
.error a {
	color:#ff8a00;
	text-decoration:underline;
}
.errorDiv {
	margin:4px 0px;
}
.warning {
	font-style:normal;
}
.errormsg {
	font-family:Arial,Helvetica,sans-serif;
	font-size:14px;
	color:#ff8a00;
    padding-left: 15px;
    padding-right: 15px;
    padding-top: 5px;
    padding-bottom: 5px;
    background: #fee2c1;
    margin-left: 20px;
    border: 1px dotted #fad2a4;
}
.edittime {
	color:#faad52;
	font-style:italic;
}
#mainCenter {
	width:984px;
	margin:0px auto;
	position:relative;
}
#mainContainer {
	float:left;
}
#shadowContainer {
	float:left;
	width:974px;
	padding:0px 5px 0px;
	background:url(<?php echo $imageurl; ?>/backing_shadow2.png) repeat-y;
}
#mainContent {
	width:974px;
	background-color:#ffffff;
	float:left;
	position:relative;
	padding-bottom:20px;
}
#mainContent.homepage {
	background-color:transparent;
	padding-bottom:0px;
}
#footer {
	float:left;
	width:984px;
	padding:20px 0px 25px;
	height:31px;
	background:url(<?php echo $imageurl; ?>/footer.png) top left no-repeat;
}
#footer #links {
	float:left;
	margin-left:103px;
	font-size:12px;
	color:#888888;
}
#footer #links.longer {
	margin-left:78px;
}
#footer #links a {
	color:#888888;
}
#footer #links a:hover {
	color:#0e6db7;
}
#footer #links div {
	float:left;
	margin-right:25px;
}
#footer #copyright {
	float:left;
	margin-left:5px;
	color:#ffffff;
	white-space:nowrap;
}
#loading {
	position:absolute;
	top:0px;
	right:0px;
	z-index:1000;
}
#noscript {
	background:#e0e0e0;
	color:#00c0ff;
	font-weight:bold;
	font-size:14px;
	border-top:1px dotted #00c0ff;
	border-bottom:1px dotted #00c0ff;
	text-align:center;
}
#verticalAd {
	position:absolute;
	top:44px;
	right:26px;
}
.userVoice {
	font-family:Trebuchet MS;
}
.iconMargin {
	margin:0px 3px;
}
.eleven {
	font-size:11px;
}
#customBanner {
	float:left;
	display:inline;
	width: 974px;
}
#nav {
	float:left;
	position:relative;
	z-index:3000;
	height:39px;
	width:974px;
	background:url(<?php echo $imageurl; ?>/nav.png) no-repeat;
	margin-top:15px;
	padding:5px 5px 0px;
	font-size:13px;
	color:#aee3f0;
}
#nav a {
	color:#aee3f0;
}
#nav .navLogo {
	float:left;
	display:block;
	margin:11px 0px 0px 12px;
	width:122px;
	height:20px;
	background:url(<?php echo $imageurl; ?>/logo.png) no-repeat;
}
#nav .loggedIn {
	float:left;
	line-height:13px;
	margin:17px 0px 0px 10px;
}
#nav .loggedout {
	float:left;
	line-height:13px;
	margin:9px 13px 0px 0px;
}
#nav .loggedIn span,#nav .loggedout span {
	margin:0px 8px;
}
#nav .mainArea {
	float:right;
	margin:6px 11px 0px 0px;
}
#nav .mainAreaRegister {
	float:right;
	margin:12px 20px 0px 0px;
	font-size:16px;
}
#nav .buttons {
	float:left;
}
#nav .buttons a.btn,#nav .buttons a.btn span {
	display:block;
	float:left;
	height:28px;
	line-height:28px;
	text-deocoration:none;
	font-family:Arial,Helvetica,sans-serif;
	font-size:16px;
	text-align:center;
	white-space:nowrap;
	cursor:pointer;
	color:#fff;
}
#nav .buttons a.btn {
	background:url(<?php echo $imageurl; ?>/btn_left.png) top left no-repeat;
	padding-left:8px;
}
#nav .buttons a.btn span {
	background:url(<?php echo $imageurl; ?>/btn_right.png) top right no-repeat;
	padding-right:8px;
}
#nav .buttons a.btn:hover,#nav .buttons a.btn.selected {
	background-position:0% -28px;
	text-decoration:none;
	color:#fff;
}
#nav .buttons a.btn:hover span,#nav .buttons a.btn.selected span {
	background-position:100% -28px;
	text-decoration:none;
	color:#fff;
}
#nav .buttons a.btn.icon span div {
	background-position:top left;
	background-repeat:no-repeat;
	width:18px;
	height:17px;
	margin:5px -2px 0px;
}
#nav .buttons a.btn.icon span div.settingsIcon {
	background-image:url(<?php echo $imageurl; ?>/icon_settings.png);
}
#nav .dropdownArea {
	float:left;
	position:relative;
	margin-right:4px;
	height:28px;
}
#nav .dropdown {
	position:absolute;
	top:0px;
	left:0px;
	color:#fff;
}
#nav .dropdown .top {
	float:left;
	background-position:top left;
	background-repeat:no-repeat;
	width:100%;
}
#nav .dropdown .btm {
	float:left;
	background-position:top left;
	background-repeat:no-repeat;
	height:5px;
	line-height:5px;
	width:100%;
}
#nav .dropdown a.mainLink {
	float:left;
	display:block;
	font-size:16px;
	height:28px;
	line-height:28px;
	padding:0px 8px 5px;
	color:#fff;
}
#nav .dropdown a.mainLink:hover {
	text-decoration:none;
}
#nav .dropdown .emptySpace {
	float:left;
	height:33px;
}
#nav .dropdown .links {
	float:left;
	clear:left;
	margin-top:6px;
}
#nav .dropdown .links a {
	display:block;
	float:left;
	height:23px;
	line-height:23px;
	padding-left:10px;
	color:#fff;
}
#nav .dropdown .links a:hover {
	text-decoration:none;
	background-color:#005cd3;
}
#nav #navHome {
	width:55px;
}
#nav #navHome #homeBtn {
	position:relative;
}
#nav #navHome #homeBtnIsNew,#nav #navHome #homeBtnIsNew2 {
	background:url(<?php echo $imageurl; ?>/icon_whatsnew_bubble_small2.png) top left no-repeat;
	width:11px;
	height:12px;
	position:absolute;
	top:-4px;
	right:-2px;
}
#nav #navHome a.btn span {
	width:39px;
}
#nav #navHome .dropdown {
	width:114px;
}
#nav #navHome .dropdown a.mainLink {
	position:relative;
	width:39px;
}
#nav #navHome .dropdown .emptySpace {
	width:59px;
}
#nav #navHome .dropdown .links a {
	width:104px;
}
#nav #navHome .dropdown .links a.new span {
	background:url(<?php echo $imageurl; ?>/icon_whatsnew_bubble_small2.png) no-repeat top right;
	padding-right:15px;
}
#nav #navHome .dropdown .top {
	background-image:url(<?php echo $imageurl; ?>/dropdown_home_top.png);
}
#nav #navHome .dropdown .btm {
	background-image:url(<?php echo $imageurl; ?>/dropdown_home_btm.png);
}
#nav #navTopics {
	width:68px;
}
#nav #navTopics a.btn span {
	width:53px;
}
#nav #navTopics .dropdown {
	width:140px;
}
#nav #navTopics .dropdown a.mainLink {
	width:53px;
}
#nav #navTopics .dropdown .emptySpace {
	width:71px;
}
#nav #navTopics .dropdown .links a {
	width:130px;
}
#nav #navTopics .dropdown .top {
	background-image:url(<?php echo $imageurl; ?>/dropdown_explore_top.png);
}
#nav #navTopics .dropdown .btm {
	background-image:url(<?php echo $imageurl; ?>/dropdown_explore_btm.png);
}
#nav #navSettings {
	width:30px;
}
#nav #navSettings .dropdown {
	width:65px;
}
#nav #navSettings .dropdown .emptySpace {
	width:35px;
}
#nav #navSettings .dropdown .links a {
	padding-left:8px;
	width:57px;
}
#nav #navSettings .dropdown .top {
	background-image:url(<?php echo $imageurl; ?>/dropdown_settings_top.png);
}
#nav #navSettings .dropdown .btm {
	background-image:url(<?php echo $imageurl; ?>/dropdown_settings_btm.png);
}
#nav #navSettings .dropdown a.icon {
	float:left;
	display:block;
	width:18px;
	height:17px;
	background:url(<?php echo $imageurl; ?>/icon_settings.png) top left no-repeat;
	margin:5px 6px 6px;
}
#nav #navSearch {
	float:left;
	margin-left:1px;
}
#nav #navSearch a.searchBtn {
	display:block;
	float:left;
	background:url(<?php echo $imageurl; ?>/btn_search.png) top left no-repeat;
	width:29px;
	height:28px;
}
#nav #navSearch a.searchBtn:hover {
	background-position:0% -28px;
}
#nav #navSearch .searchBox {
	float:left;
	background:url(<?php echo $imageurl; ?>/search_input2.png) top left no-repeat;
	width:135px;
	height:28px;
}
#nav #navSearch .searchBox input {
	width:128px;
	height:20px;
	margin:4px 0px 0px 6px;
	padding:0px;
	border:0px none;
	font-size:16px;
	font-family:"Trebuchet MS";
	color: #7b7b7b;
}
#modalDiv {
	position:absolute;
	top:0px;
	left:0px;
	display:block;
	margin:0px auto;
	padding:0px;
	width:100%;
	z-index:4000;
	background-color:#000;
	filter:alpha(opacity=30);
	opacity:0.3;
}
.popup p {
	font-size:12px;
}
.popup {
	position:absolute;
	width:100%;
	margin:0px;
	padding:0px;
	z-index:5000;
}
.innerPopup,.innerPopupSmall,.innerPopupLarge,.innerPopupExtraLarge {
	position:relative;
	padding:12px 10px 12px 10px;
}
.innerPopup {
	width:400px;
	position:relative;
}
.innerPopupLarge {
	width:470px;
}
.innerPopupExtraLarge {
	width:630px;
}
.smallPopupWrapper,.popupWrapper,.largePopupWrapper,.extraLargePopupWrapper {
	position:absolute;
	left:50%;
	margin-left:-200px;
	width:360px;
}
.popupWrapper {
	width:400px;
	margin-left:-200px;
}
.largePopupWrapper {
	width:490px;
	margin-left:-245px;
	background:url(<?php echo $imageurl; ?>/pop_shadow_lg-trans.png) bottom left no-repeat;
}
.extraLargePopupWrapper {
	width:630px;
	margin-left:-315px;
	background:url(<?php echo $imageurl; ?>/pop_shadow_lg-trans.png) bottom left no-repeat;
}
.innerPopupSmall {
	width:360px;
}

.innerPopup h2,.innerPopupSmall h2,.innerPopupLarge h2 {
	font-size:20px;
	color:#f33408;
	margin:0px;
	margin-bottom:20px;
	padding:0px;
	font-weight:normal;
}
.innerPopup p,.innerPopupSmall p,.innerPopupLarge p {
	font-size:12px;
	color:#000;
	line-height:16px;
	margin:5px 0px;
}
.innerPopup form,.innerPopupSmall form,.innerPopupLarge form {
	margin:0px;
}
.innerPopup .label,.innerPopupSmall .label,.innerPopupLarge .label {
	font-size:11px;
	color:#8f8f8f;
	text-transform:lowercase;
}
.innerPopup .heading,.innerPopupSmall .heading,.innerPopupLarge .heading {
	color:#f33408;
	font-size:14px;
	text-transform:lowercase;
}
.innerPopup .alignRight,.innerPopupSmall .alignRight,.innerPopupLarge .alignRight {
	padding-right:7px;
	text-align:right;
}
.innerPopup .highlight,.innerPopupSmall .hightlight,.innerPopupLarge .highlight {
	color:#f33408;
}
.textInput,.textSelect {
	font:16px trebuchet ms;
	color:#000;
}
.textInput {
	width:280px;
}
.innerPopupSmall input[type=text],.innerPopupSmall input[type=password],.innerPopupSmall textarea,.innerPopupSmall input.textInput {
	width:240px;
	font:13px trebuchet ms;
	color:#000;
}
.innerPopup .embedTextArea,.innerPopupLarge .embedTextArea {
	width:390px;
	padding:3px;
	font-size:12px;
	font-family:Arial,Helvetica,sans-serif;
	border:1px solid #cdcdcd;
}
.innerPopupLarge .embedTextArea {
	width:460px;
}
.popupButtons {
	text-align:right;
	float:right;
}
.popupWrapper input[type=radio] {
	margin:0px;
}
.popupWrapper .checkBoxOption {
	margin-left:5px;
	padding:0px;
}
.form {
	width:400px;
	color:#8f8f8f;
}
.full label {
	font-size:11px;
	text-transform:lowercase;
	color:#8f8f8f;
}
.full {
	padding:5px 0px;
	clear:left;
	display:block;
}
.full textarea,.full input {
	width:390px;
}
.close {
	position:absolute;
	top:8px;
	right:8px;
}
#editTitle label {
	font-size:11px;
	color:#8f8f8f;
	text-transform:lowercase;
}
#editTitle .lineitem {
	margin-top:5px;
}
.smallPopupWrapper ul#buttons {
	margin:0px;
	padding:0px;
}
.smallPopupWrapper ul#buttons li {
	float:left;
	display:inline;
	margin-right:5px;
}
#inviteFriends input,#inviteFriends textarea {
	font-family:"Trebuchet MS";
	font-size:12px;
}
#inviteFriends .label {
	padding-bottom:2px;
}
#inviteFriends .fromFields {
	float:left;
}
#inviteFriends .nameField {
	width:188px;
}
#inviteFriends #toField {
	float:left;
	width:390px;
	padding:20px 0px 5px 0px;
}
#inviteFriends #toField input {
	width:390px;
	margin-bottom:3px;
}
#inviteFriends #importButton {
	float:right;
	padding-top:3px;
}
#inviteFriends #msgLabel {
	padding-top:10px;
	clear:both;
}
#inviteFriends #msgDefault {
	padding-top:5px;
	line-height:14px;
}
#inviteFriends .defaultText {
	color:#8f8f8f;
	font-style:italic;
	font-family:Arial,Helvetica,sans-serif;
}
#inviteFriends .inviteText {
	color:#000;
	font-style:normal;
	font-family:"Trebuchet MS";
}
#inviteFriends textarea {
	width:390px;
	height:45px;
	margin:5px 0px 25px 0px;
	padding:3px;
}
#inviteFriends #requiredText {
	position:absolute;
	bottom:10px;
	left: 10px;
}
.about {
	display:inline;
	width:750px;
	background:#fff url(<?php echo $imageurl; ?>/panels_topgradient.jpg) top left repeat-x;
}
.about #tophead {
	font-family:Trebuchet MS;
	font-size:30px;
	font-weight:bold;
	color:#008acb;
	margin:0px;
	padding:0px;
}
.about #leftcolumn {
	display:inline;
	float:left;
	width:570px;
	padding:51px 0;
	margin:0 0 0 30px;
}
.about #leftcolumn img {
	margin-left:0px;
	margin-top:0px;
}
.about #copy {
	display:block;
	font-size:13px;
	line-height:20px;
	color:#000;
	margin-top:15px;
}
.about #rightcolumn {
	display:inline;
	float:left;
	width:331px;
	margin:30px 0 0 38px;
}
.about #computernotes {
	height:285px;
	background:url(<?php echo $imageurl; ?>/about_computer.jpg) top left no-repeat;
}
.about #phonenotes {
	height:363px;
	background:url(<?php echo $imageurl; ?>/about_phone.jpg) top left no-repeat;
	margin:31px 0 31px 0;
}
.about #rightcolumn #title {
	font-family:Trebuchet MS;
	font-size:18px;
	color:#008acb;
	font-weight:bolder;
	margin:0 0 0 100px;
	padding:45px 0px 40px 0px;
	;
	display:block;
}
.about #rightcolumn #point {
	padding:0px;
	margin:0px 0px 6px 22px;
	font-family:Arial;
	font-size:11px;
	color:#5d5d5d;
	display:inline;
	width:275px;
	float:left;
}
.about #rightcolumn #bullet {
	font-weight:bolder;
	color:#d3006a;
	display:inline;
	float:left;
}
.about #rightcolumn #text {
	margin-left:18px;
	display:block;
	line-height:15px;
}
.about #rightcolumn #intro {
	display:block;
	font-family:Arial;
	color:#5d5d5d;
	margin-left:20px;
	font-size:11px;
	margin-bottom:10px;
}
#about {
	width:835px;
	margin:10px 0px 100px 70px;
	display:inline;
	float:left;
	position:relative;
}
#about h2 {
	text-align:left;
	color:#f33408;
	font-size:20px;
	margin:0px 50px 10px 52px;
}
#about p {
	font-size:13px;
	color:#888;
	line-height:18px;
	text-align:left;
	margin:0px 50px 10px 52px;
}
#yearbook {
	width:835px;
	padding:0px;
	position:relative;
	margin:0px;
	float:left;
	display:inline;
}
#yearbook-box {
	width:835px;
	margin:0px 0px 0px 50px;
	list-style:none;
	text-align:center;
}
#yearbook-box li {
	float:left;
	display:inline;
}
#yearbook-box img {
	margin:8px 1px 6px 1px;
	border:3px solid #fff;
}
#yearbook-box a {
	border:none;
}
#yearbook-box a img:hover {
	border: 3px solid #A2C6D8;
}
.help {
	width:575px;
	margin:0px 0px 50px 23px;
	padding-top:23px;
	display:inline;
	float:left;
}
.help #tophead {
	font-size:20px;
	color:#f33408;
	margin:0px;
	padding:0px;
}
.help #question {
	font-size:18px;
	color:#f33408;
	font-weight:normal;
}
.help #answer {
	font-size:12px;
	color:#000000;
	font-weight:normal;
	line-height:18px;
	display:block;
	margin-top: 5px;
}
div#gsfn_search_widget img {
	border:none;
}
div#gsfn_search_widget {
	font-size:12px;
	width:280px;
	background-color:#f8f8f8;
	padding:10px;
}
div#gsfn_search_widget h4 {
	color:#f33408;
	font-size:16px;
	margin:0px 0px 15px 0px;
}
div#gsfn_search_widget a.widget_title {
	display:block;
	margin-bottom:10px;
}
div#gsfn_search_widget .powered_by {
	margin-top:8px;
	padding-top:8px;
	border-top:1px solid #ddd;
	clear:both;
}
div#gsfn_search_widget .powered_by a {
	font-size:90%;
}
div#gsfn_search_widget form {
	margin-bottom:8px;
}
div#gsfn_search_widget form label {
	margin-bottom:5px;
	display:block;
}
div#gsfn_search_widget form #gsfn_search_query {
	width:60%;
}
div#gsfn_search_widget div.gsfn_content ul {
	margin:10px 0px 0px;
	float:left;
}
div#gsfn_search_widget div.gsfn_content li {
	text-align:left;
	margin-bottom:15px;
	width:100%;
}
div#gsfn_search_widget div.gsfn_content li a {
	display:inline;
	float:left;
	margin-right:5px;
}
div#gsfn_search_widget div.gsfn_content .gsfn_or {
	line-height:25px;
	float:left;
	display:inline;
	margin-right:5px;
}
div#gsfn_search_widget div.gsfn_content span.time {
	font-size:90%;
	padding-left:3px;
}
div#gsfn_search_widget div.gsfn_content p.gsfn_summary {
	margin-top: 2px
}
#terms {
	font-family:Arial;
	font-size:11px;
	width:874px;
	padding-left:50px;
	margin-top: 10px;
}
#addressList {
	margin:0px;
	list-style-type:none;
	padding:0px;
	width:394px;
}
#addressList.withScrollBar {
	width:374px;
}
#addressList li {
	font-family:"Trebuchet MS";
	font-size:12px;
	margin:5px 0px;
	padding-right:10px;
	background:#f2f2f2;
}
#addressList li a {
	float:right;
	margin-left:3px;
	padding-top:6px;
}
#addressList li div {
	padding:5px;
	height:14px;
}
#addressListContainer.scrollVertical {
	overflow-y:auto;
	overflow-x:hidden;
}
#addressList li.errorHighlight,#address.errorHighlight {
	background-color:#fdf48a
}
#suggestContainer {
	position:relative;
}
#suggestContainer div {
	position:absolute;
	border:1px solid #767676;
	background:#f1f1f1;
	padding:0px;
}
#suggestContainer div span {
	padding:5px;
	font-size:12px;
	color:#000;
}
#suggestContainer input.disabled {
	font-style:italic;
	color: #8f8f8f;
}
.friendInvite {
	margin:10px 20px;
	margin-bottom:0px;
	font:bold 12px Trebuchet MS;
	color:#000;
}
.friendInvite a {
	color:#ff6600;
}
.input2 .desc {
	display:block;
	width:350px;
	margin:0px 0px 15px 150px;
	line-height:16px;
	color:#000;
}
.input .desc {
	display:block;
	width:260px;
	margin-left:150px;
	color:#737373;
	font-size:11px;
	line-height: 16px;
}
#settings .carrier {
	margin:10px 0px;
	color:#000;
	font-size:14px;
}
#settings .grayText {
	color:#737373;
}
#settings .smallerGrayText {
	color:#7b7b7b;
	font-size:12px;
}
#settings .carrierLeft {
	width:120px;
	display:inline;
	float:left;
}
#settings .carrierRight {
	display:inline;
	float:left;
}
#settings .phoneNumber {
	margin:0px 10px;
	color:#000;
	font-size:24px;
	font-weight:bold;
}
#settings .red {
	color:#f00;
}
#settings #phoneicon {
	position:inline;
	margin:23px 0px 23px 60px;
	text-align:center;
	font-size:14px;
	line-height:normal;
}
#settings #phoneicon img {
	width:16px;
	height:28px;
	vertical-align:top;
}
#settings #emailicon {
	position:inline;
	margin:23px 0px 23px 0px;
	text-align:center;
	font-size:14px;
	line-height:normal;
}
#settings .smallLink {
	font-size:11px;
}
#editPhoneDiv {
	margin-left:50px;
	width:456px;
}
#editPhoneForm {
	height:63px;
	background:url(<?php echo $imageurl; ?>/confirm_editphone_bg.gif) top left no-repeat;
}
#editPhoneForm .grayLabel {
	font-size:11px;
	color:#a49d8c;
}
#settings .yellowError {
	font-size:11px;
	color:#ff9000;
	font-style: italic;
}
#settings {
	width:928px;
	padding:23px 0px;
	margin:0px 23px;
	font-size:12px;
	line-height:15px;
}
#settings .label {
	color:#737373;
	display:block;
	margin-bottom:2px;
}
#settings #myAlerts .label {
	display:inline;
}
#settings .entry {
	font-family:Trebuchet MS;
}
.pagePopup {
	position:relative;
	width:570px;
	margin:0px 0px 10px 0px;
	padding-right:10px;
	padding-left:22px;
	background:url(<?php echo $imageurl; ?>/panel_middle.gif) top left repeat-y;
}
#rightCol {
	float:right;
	display:inline;
}
.passwordChange {
	background:#efefef;
	padding:10px 20px;
	margin:10px -18px;
	width:550px;
}
.pagePopup h3 {
	font-size:18px;
	padding:16px 0px 0px 22px;
	color:#f33408;
	width:576px;
	margin:0px 0px 0px -22px;
	background:url(<?php echo $imageurl; ?>/panel_top.gif) top left no-repeat;
	height:51px;
}
.pagePopup .bottom {
	width:598px;
	margin-left:-22px;
	background:url(<?php echo $imageurl; ?>/panel_bottom.gif) bottom left no-repeat;
	height:46px;
	position:relative;
}
.pagePopup .bottom2 {
	width:598px;
	margin-left:-22px;
	background:url(<?php echo $imageurl; ?>/panel_bottom_no_rule.gif) bottom left no-repeat;
	height:46px;
	position:relative;
}
.pagePopup p {
	margin:9px 0px;
	padding-right:4px;
}
.pagePopup .buttonsPanel {
	position:absolute;
	top:7px;
	right:20px;
}
.col {
	width:305px;
	margin-top:0px;
}
.wideCol {
	width:600px;
	margin:0px;
}
.wideCol ul {
	margin:0px;
	padding:0px;
	margin-right:30px;
	list-style-type:none;
}
.wideCol li {
	position:relative;
	margin:0px 0px 10px 30px;
}
.wideCol .radio {
	position:absolute;
	top:-2px;
	left:-30px;
}
.wideCol ul li ul {
	margin:5px;
	padding:0px;
}
.settingHead {
	border-bottom:1px solid #eeeded;
	padding-bottom:10px;
	width:555px;
}
.settingHead ul {
	height:20px;
}
.settingHead ul li {
	float:left;
	display:inline;
	margin-right:30px;
}
.settingHead .logo {
	float:right;
	text-align:right;
}
.pagePopupRight {
	position:relative;
	width:253px;
	padding-left:19px;
	padding-right:18px;
	margin:0px 0px 10px 0px;
	background-color:#F8F8F8;
	line-height:15px;
}
.pagePopupRight h3 {
	color:#f33408;
	font-size:18px;
	padding:15px 20px 0px 0px;
	margin-top:0px;
	background-color:#F8F8F8;
}
.pagePopupRight h4 {
	color:#737373;
	display:inline;
	margin-right:5px;
	font-size:12px;
}
.pagePopupRight p {
	margin:9px 0px;
}
.pagePopupRight .bottom {
	height:40px;
	background-color:#F8F8F8;
	position:relative;
}
.pagePopupRight .buttonsPanel {
	position:absolute;
	top:7px;
	right:2px;
}
.pagePopupRight .divider {
	background:url(<?php echo $imageurl; ?>/divider_faq.gif) no-repeat;
	width:261px;
	height:2px;
}
.pagePopupRight .divider2 {
	margin:12px 0px;
	height:1px;
	line-height:1px;
	background-color:#ffffff;
	width:100%;
	border-bottom:1px solid #edeee8;
}
.pagePopupRight .faq .ans {
	color:#737373;
	display:block;
	padding-bottom:15px;
	margin:0px;
}
.pagePopupRight .faq h3 {
	font-size:14px;
}
.pagePopupRight .faq p {
	margin:0px 0px 5px;
	line-height:15px;
}
.pagePopupRight .faq ul.ans {
	margin:0px 0px 10px 14px;
	padding:0px;
	width:250px;
}
.pagePopupRight ul.ans li {
	margin-bottom:5px;
}
.helpNEdit {
	position:absolute;
	right:20px;
	top:15px;
	width:50px;
	line-height:22px;
	font-size:12px;
}
.helpNEdit a {
	color:#ff2d00;
	text-decoration:none;
}
.helpNEdit a:hover {
	text-decoration:underline;
}
.helpNEdit .helpLink {
	float:right;
}
.alertPanel {
	background:#f8f8f8;
	margin:10px 0px;
	margin-top:0px;
	margin-right:10px;
	padding-right:10px;
	padding-bottom:0px;
	padding-left:0;
}
.gender {
	font:normal 12px;
	color:#444;
	line-height:16px;
}
.saveButton {
	margin-right: 5px;
}
#settingsPage {
	float:left;
	display:inline;
	width:928px;
	padding:23px 0px;
	margin:0px 23px;
	font-size:12px;
	line-height:15px;
}
#settingsPage .columnLeft {
	float:left;
	display:inline;
	width:598px;
	margin-right:23px;
	margin-bottom:10px;
}
#settingsPage .columnRight {
	float:left;
	display:inline;
	width:253px;
}
#settingsPage .panel {
	float:left;
	width:598px;
	margin-bottom:10px;
}
#settingsPage .panel .buttonArea {
	float:right;
}
#settingsPage .panelTop {
	float:left;
	background:url(<?php echo $imageurl; ?>/panel_top.gif) top left no-repeat;
	width:554px;
	height:36px;
	padding:15px 22px 0px;
}
#settingsPage .panelTop h1 {
	margin:3px 0px 0px;
}
#settingsPage .panelBottom {
	float:left;
	background:url(<?php echo $imageurl; ?>/panel_bottom.gif) bottom left no-repeat;
	width:554px;
	height:39px;
	padding:7px 22px 0px;
}
#settingsPage .panelMain {
	float:left;
	position:relative;
	width:554px;
	padding:20px 22px;
	background:url(<?php echo $imageurl; ?>/panel_middle.gif) top left repeat-y;
}
#settingsPage .panelRight {
	float:left;
	width:253px;
	padding:20px 18px 10px;
	margin-bottom:10px;
	background-color:#F8F8F8;
}
#settingsPage h1,#settingsPage h2,#settingsPage h3 {
	font-size:20px;
	line-height:20px;
	margin:0px;
	color:#f33408;
}
#settingsPage h2 {
	font-size:16px;
	line-height:16px;
}
#settingsPage h3 {
	font-size:15px;
	line-height:15px;
	clear:both;
	float:left;
}
#settingsPage h4 {
	font-size:12px;
	line-height:15px;
	clear:both;
	float:left;
	color:#737373;
	margin:18px 0px 10px;
}
#settingsPage .divider {
	clear:both;
	float:left;
	margin:22px 0px;
	height:1px;
	line-height:1px;
	background-color:#dddddd;
	width:100%;
	border-bottom:1px solid #edeee8;
}
#settingsPage .panelRight .divider {
	margin:15px 0px 20px;
	background-color:#ffffff;
}
#settingsPage .label {
	color:#737373;
}
#settingsPage label.textField {
	display:block;
	color:#737373;
	margin-bottom:2px;
}
#settingsPage input.radio {
	float:left;
}
#settingsPage input.radioVert {
	margin-left:30px;
}
#settingsPage label.radio {
	float:left;
	margin:2px 0px 0px 4px;
}
#settingsPage label.radio span {
	color:#737373;
}
#settingsPage .faq .qa {
	float:left;
	margin:0px 10px 15px 0px;
	line-height:16px;
}
#settingsPage .faq .qst {
	color:#000000;
}
#settingsPage .faq .ans {
	color:#737373;
}
#settingsPage .faq ul.ans {
	margin:5px 0px 0px 14px;
	padding:0px;
	width: 250px;
}
#settingsPage #privacy .privacyOption {
	clear:both;
	float:left;
	margin-bottom:6px;
}
#settingsPage #privacy .privacyOption div {
	clear:left;
	margin:2px 0px 0px 25px;
	color:#737373;
}
#settingsPage #blocked input {
	float:left;
	width:190px;
	display:inline;
	margin-right:10px;
}
#settingsPage #blocked .blocking {
	float:left;
	margin-bottom:15px;
	width:180px;
}
#settingsPage #blocked .heading {
	margin-bottom:10px;
}
#settingsPage #blocked .blockedUser {
	float:left;
	clear:both;
	margin-bottom:5px;
	width: 100%;
}
#settingsPage #alert .alertOption {
	clear:both;
	float:left;
	margin-bottom:10px;
	width:100%;
}
#settingsPage #alert .alertOption input {
	float:left;
}
#settingsPage #alert h4 {
	margin-bottom:5px;
	margin-top:10px;
}
#settingsPage #alert .alertOption select {
	width:200px;
}
#settingsPage #alert .alertOption .timeSelect {
	width:75px;
}
#settingsPage #alert .alertOption label {
	float:left;
	margin:3px 0px 0px 10px;
}
#settingsPage #alert .alertOption label span {
	color:#737373;
}
#settingsPage #alert .alertOption span {
	color:#737373;
}
#settingsPage #alert .alertOption div {
	clear:left;
	margin-left:31px;
	color:#737373;
}
#settingsPage #alert .customOptions {
	clear:both;
	float:left;
	margin-left:20px;
	width:500px;
	overflow:auto;
	display:inline;
}
#settingsPage #alert .customOptions div {
	float:left;
	width:50%;
	position:inline;
}
#settingsPage #alert .customOptions span {
	color:#737373;
	display:block;
	margin-bottom: 5px;
}
#processPage {
	float:left;
	padding:0px;
	margin:0px 30px 0px 40px;
	font-size:12px;
	line-height:16px;
	color:#888;
}
#processPage h2 {
	color:#f33408;
	font-size:18px;
	line-height:18px;
	margin:0px 0px 15px 0px;
}
#processPage h3 {
	color:#f33408;
	font-size:18px;
	margin:0px 0px 15px 0px;
}
#processPage h4 {
	color:#f33408;
	font-size:16px;
	margin:0px 0px 15px 0px;
}
#processPage h5 {
	color:#f33408;
	font-size:14px;
	margin:0px 0px 10px 0px;
}
#processPage .tipBubble {
	background:url(<?php echo $imageurl; ?>/tipbubble_bk.gif) no-repeat;
	position:relative;
	float:left;
	display:inline;
	margin:4px 0px 7px;
}
#processPage .tipBubble p {
	background:url(<?php echo $imageurl; ?>/tipbubble_btm.gif) no-repeat 0% 100%;
	width:337px;
	padding:8px 12px 8px 16px;
	margin:0px;
	font:italic bold 12px arial;
	line-height:16px;
	color:#777777;
}
#processPage .tipBubble p.oneLine {
	line-height:30px;
}
#processPage #columnLeft {
	float:left;
	width:564px;
	margin-right:37px;
}
#processPage #columnLeft .latestUpdate {
	float:left;
	margin-bottom:17px;
}
#processPage #columnLeft .buttonArea {
	float:right;
	margin-bottom:25px;
}
#processPage #columnRight {
	float:left;
	width:253px;
}
#processPage .panelRight {
	float:left;
	width:253px;
	padding:20px 18px 10px;
	margin-bottom:10px;
	background-color:#f8f8f8;
}
#processPage .panelRightGreen {
	float:left;
	width:288px;
	margin-bottom:10px;
	background-color:#eff9f1;
	border:1px solid #d2deba;
}
#processPage .panelRightGreen .innerGreen {
	background-color:#e6f2dc;
	margin:1px;
	padding:22px 27px 10px 24px;
	;
	color:#676767;
}
#processPage .panelRightGreen p {
	margin-bottom:13px;
	line-height:17px;
}
#processPage .panelRightGreen .icon {
	top:2px;
	margin:0px 0px -2px;
}
#processPage .cluster {
	margin-bottom:15px;
	float:left;
}
#processPage p {
	margin:0px 0px 15px 0px;
}
#processPage .black {
	color:#000;
}
#processPage .gray {
	color:#a9a9a9;
}
#processPage .req {
	color:#f33408;
	font-size:18px;
	position:absolute;
	top:3px;
	left:auto;
	padding-left:3px;
}
#processPage .requiredLabel {
	float:right;
	font-size:11px;
	color:#a9a9a9;
}
#processPage .requiredLabel .star {
	color:#f33408;
	font-size:18px;
	position:relative;
	top:3px;
	margin-right:3px;
}
#processPage h1,#processPage h2,#processPage h3,#processPage h5 {

	color:#f33408;
	clear:both;
}
#processPage h5 {
	margin-bottom:5px;
}
#processPage .h2info {
	display:inline;
	color:#888;
	font-size:12px;
	margin-left:5px;
}
#processPage .gray12 {
	margin:0px 0px 2px 0px;
	clear:both;
	line-height:16px;
}
#processPage .divider {
	clear:both;
	float:left;
	margin:0px 0px 15px 0px;
	height:1px;
	line-height:1px;
	background-color:#edeee8;
	width:100%;
	font-size:0px;
}
#processPage .panelRight .divider {
	margin:10px 0px 10px;
	background-color:#ffffff;
}
#processPage .label {
	width:265px;
	color:#a9a9a9;
	display:inline;
	float:left;
	margin:0px 4px 15px 0px;
	font-size:11px;
	position:relative;
}
#processPage .label input {
	width:257px;
	font:14px trebuchet ms;
}
#processPage .label.wide {
	width:563px;
}
#processPage .qst {
	color:#000000;
	margin:0px 10px 5px 0px;
	line-height:13px;
}
#processPage .ans {
	color:#737373;
	margin:0px 0px 10px 0px;
}
#processPage ul.ans {
	margin:0px 0px 15px 14px;
	padding:0px;
	line-height:13px;
}
#processPage ul.ans li {
	margin-bottom:7px;
}
#processPage .mainError {
	margin:0px 0px 10px 0px;
	font-size:14px;
}
#processPage .errorField {
	width:100%;
	float:left;
	margin-bottom:5px;
}
#processPage input.radio {
	float:left;
	margin-left:0px;
	display:inline;
}
#processPage label.radioLabel {
	float:left;
	margin:2px 0px 0px 4px;
	color:#000;
	font-size:12px;
	display:inline;
}
#processPage label.radioLabel.gray {
	color:#a9a9a9;
}
#processPage .radioGender {
	float:left;
	display:inline
}
#processPage .radioGender input.radio,#processPage .radioGender .radioLabel {
	margin-left:0px;
}
#processPage #regForm {
	margin-bottom:20px;
	position:relative;
	float:left;
}
#processPage #regForm .heading {
	float:left;
	margin-bottom:10px;
}
#processPage #regForm .heading h3 {
	float:left;
	margin-bottom:0px;
	line-height:20px;
}
#processPage #regForm .heading div {
	float:left;
	font-size:18px;
	line-height:20px;
	color:#888888;
	margin-left:10px;
}
#processPage #regForm .label {
	width:211px;
	margin:0px 0px 5px 0px;
}
#processPage #regForm .label input {
	width:205px;
}
#processPage #regForm .tipBubble {
	position:absolute;
	top:0px;
	left:220px;
	text-align:left;
}
#processPage #regForm select {
	border:1px solid #a0a5A7;
	font-family:Trebuchet MS;
	font-size:14px;
}
#processPage #regForm .usernameAvailable,#processPage #regForm .usernameUnavailable {
	float:left;
	margin-left:10px;
	color:#7cc81d;
	font-size:11px;
	font-style:italic;
}
#processPage #regForm .usernameUnavailable {
	color:#ff8a00;
}
#processPage #regForm .error {
	margin-top:5px;
}
#processPage #regForm .mainError {
	float:left;
	margin:0px 0px 15px 0px;
}
#processPage .legalText {
	float:left;
	margin-left:10px;
	width:350px;
	color:#a9a9a9;
}
#processPage .label.select,#processPage .label.select select {
	width:210px;
}
#processPage .moreInfo {
	margin-left:10px;
	font-size:11px;
}
#processPage .faqExpandPanel {
	position:relative;
	float:left;
	width:97%;
	margin:0px 0px 10px;
	padding:12px;
	background-color:#f3f5f6;
	border:1px solid #e5eaeb;
	font-size:12px;
	color:#6f6f6d;
}
#processPage .faqExpandPanel .close {
	position:absolute;
	top:2px;
	right:8px;
}
#processPage .settingStatus {
	font-size:14px;
	color:#808080;
	font-style:italic;
	margin-bottom:15px;
}
#processPage .infoPanel {
	background-color:#F8F8F8;
	font-size:11px;
	padding: 10px;
}
#processPage .radioBlock input.radio,#processPage ul.radioBlock li input.radio {
	width:15px;
	margin:0px 5px 0px 0px;
}
#processPage ul.radioBlock {
	margin:0px 0px 15px 0px;
	padding:0px;
}
#processPage ul.radioBlock li {
	list-style-type:none;
	margin:0px 0px 5px 0px;
	padding:0px;
	width: 100%;
}
#processPage #columnLeft .latestUpdate {
	float:left;
	margin-bottom:17px;
}
#processPage #columnLeft .latestUpdate img {
	border:1px solid #cbd2d7;
	padding:1px;
}
#processPage #connectedList {
	float:left;
	width:100%;
}
#processPage #connectedList .icon {
	margin:2px 6px 15px 0px;
	float:left;
}
#processPage #connectedList .black {
	font-size:14px;
}
#processPage .panelRightGreen .connectFriends {
	background:url(<?php echo $imageurl; ?>/connectfriends_rightpanel.gif) no-repeat;
	color:#676767;
	font-size:14px;
}
#processPage .panelRightGreen .connectFriends .promoText {
	font-size:13px;
	padding:50px 0px 0px;
	line-height:17px;
	width:232px;
}
#processPage .panelRightGreen .connectFriends .note {
	line-height:14px;
	font-size:11px;
	color:#929292;
	margin-bottom:26px;
}
#processPage .panelRightGreen .connectFriends .latestUpdates {
	background-color:#eff9f1;
	border:1px solid #d2deba;
	display:inline;
	margin:-10px 13px 10px 0px;
	float:left;
	height:47px;
}
#processPage .panelRightGreen .connectFriends .latestUpdates img {
	padding:1px;
	float:left;
}
ul#addContactsPopup {
	list-style-type:none;
	margin:0px 0px 10px 0px;
	padding:0px;
	width:100%;
}
ul#addContactsPopup li {
	background-color:#f1f1f1;
	margin-bottom:5px;
	line-height:24px;
	padding:0px 5px;
	position:relative;
	height:24px;
}
ul#addContactsPopup li a {
	position:absolute;
	top:5px;
	right:5px;
	background:url(<?php echo $imageurl; ?>/icon_delete_x.gif) no-repeat;
	width:13px;
	height: 13px;
}
.profilePicPreview {
	width:100px;
	height:75px;
	margin:0px 12px 0px 0px;
	padding:1px;
	text-align:center;
	display:inline;
	float:left;
	border:solid 1px #cbd2d7;
	position:relative;
}
.profileName {
	font:24px trebuchet ms;
	color:#000;
	display:inline;
	margin-right: 10px;
}
#processPage #privacy .privacyOption {
	clear:both;
	float:left;
	margin-bottom:6px;
}
#processPage #privacy .privacyOption div {
	clear:left;
	margin:2px 0px 0px 25px;
	color:#737373;
}
#processPage #blocked input {
	float:left;
	width:190px;
	display:inline;
	margin:0px 10px 0px 0px;
}
#processPage #blocked .blocking {
	float:left;
	margin-bottom:15px;
	width:180px;
}
#processPage #blocked .heading {
	margin-bottom:10px;
	color:#000;
}
#processPage #blocked .blockedUser {
	float:left;
	clear:both;
	margin-bottom:5px;
	width: 100%;
}
#featuredPerson {
	width:300px;
	float:left;
	display:inline;
	position:relative;
	margin:0px 0px 0px 8px;
}
#featuredPerson .info {
	margin:0px 0px 10px 0px;
	float:left;
	width:290px;
	display:block;
}
#featuredPerson .icon {
	display:inline;
	float:left;
	margin:0px 12px 0px 0px;
	padding:1px;
	border:solid 1px #cbd2d7;
}
#featuredPerson .contentPreview {
	position:relative;
	width:100px;
	height:75px;
	display:inline;
	float:left;
	background:#000;
	border:solid 1px #cbd2d7;
	text-align:center;
	padding:1px;
	margin:0px 12px 0px 0px;
}
#featuredPerson .featuredTitle {
	color:#282e31;
	font-size:12px;
	line-height:17px;
	display:block;
	font-family:"Trebuchet MS";
	margin:0px 0px 5px 0px;
}
#featuredPerson .featuredTitle a {
	color:#282e31;
}
#featuredPerson .text {
	color:#91b9d8;
	font-size:11px;
	line-height:13px;
	display:block;
	margin:0px;
}
#customMktgBlock {
	width:300px;
	float:left;
	display:inline;
	position:relative;
	margin-left:8px;
}
#rightPanel #customMktgBlock h2 {
	margin-left: 0px;
}
.tinyuser {
	text-align:center;
	background:#fff;
	font-size:0px;
	position:relative;
}
.tinyOutline a,.tinyOutlineOwner {
	width:36px;
	height:27px;
	padding:1px;
	border:solid 1px #cbd2d7;
	display:block;
}
.tinyOutline a:hover {
	width:36px;
	height:27px;
	padding:1px;
	border:solid 1px #0e6db7;
	display:block;
}
.outline {
	padding:1px;
	border:solid 1px #cbd2d7;
}
.matureIcon {
	position:absolute;
	bottom:0px;
	right:0px;
}
.videoIcon {
	position:absolute;
	bottom:1px;
	left:1px;
}
a.playIcon {
	position:absolute;
	bottom:1px;
	left:1px;
	display:block;
	width:19px;
	height:19px;
	background:url(<?php echo $imageurl; ?>/icon_play.gif) no-repeat;
}
a.playIcon:hover {
	background-position:0% -19px;
}
a.playIcon.disabled {
	background-image:url(<?php echo $imageurl; ?>/icon_play_gray.png);
}
a.playIcon.disabled:hover {
	background-position:0 0
}
.tinyVideoIcon li {
	background:url(<?php echo $imageurl; ?>/icon_tinyvideo.gif) no-repeat;
	width:8px;
	height:8px;
	bottom:1px;
	left:1px;
	position:absolute;
	overflow:hidden;
	border:none;
}
.tinyVideoIcon a {
	background:url(<?php echo $imageurl; ?>/icon_tinyvideo.gif) no-repeat;
	width:8px;
	height:8px;
	bottom:-1px;
	left:3px;
	position:absolute;
	overflow:hidden;
	border:none;
}
.tinyVideoIcon a:hover {
	width:8px;
	height:8px;
	border:none;
}
#publicPreviewClose {
	float:left;
	width:972px;
	padding:5px 0px;
	text-align:center;
	border:1px solid #dedecd;
	background-color: #ffffde;
}
#ppUploadStatusMain #ppUploadStatus .progress {
	font-size:13px;
	text-align:center;
	position:relative;
	width:64px;
	left:140px;
	padding-top:10px;
	text-transform:lowercase;
}
#profilePickerPanel {
	position:relative;
	border:solid 1px #d7dbde;
	margin-bottom:5px;
	display:block;
	width:360px;
	float:left;
}
#profilePickerPanel .grid {
	width:300px;
	position:relative;
	display:inline;
	float:left;
	margin:0px 0px -9px 8px;
	padding:0px;
}
#profilePickerPanel .grid .gridItemContainer {
	position:relative;
	display:inline;
	float:left;
	margin:0px 9px 9px 0px;
	padding:0px;
	width:41px;
	height:31px;
}
#profilePickerPanel .grid .selected a {
	border:2px solid #0e6db7;
}
profilePicker #profilePickerPanel .grid .gridItemContainer {
	margin:0px 9px 0px 0px;
	height:41px;
}
#picker_paging {
	width:100%;
	position:relative;
	height:15px;
	margin-top:-5px;
	margin-bottom:20px;
	float:left;
}
#picker_paging ul {
	position:absolute;
	top:0px;
	right:0px;
	margin:10px 0px;
}
#picker_paging li {
	display:inline;
	float:left;
	color:#8F8F8F;
	line-height:0.61;
}
#picker_paging li a {
	text-decoration:none;
}
a.picker_newer {
	background:url(<?php echo $imageurl; ?>/icon_picker_newer.gif) no-repeat;
	width:45px;
	text-indent:12px;
	height:7px;
	margin:0px 2px;
	display:block;
}
a.picker_newer:hover {
	background-position:0% -7px;
}
li.picker_newer {
	background:url(<?php echo $imageurl; ?>/icon_picker_newer.gif) 0px -14px no-repeat;
	width:45px;
	text-indent:12px;
	height:7px;
	margin:0px 2px;
	display:block;
}
a.picker_newest {
	background:url(<?php echo $imageurl; ?>/icon_picker_newest.gif) no-repeat;
	width:9px;
	height:7px;
	margin:0px 2px;
	display:block;
}
a.picker_newest:hover {
	background-position:0% -7px;
}
li.picker_newest {
	background:url(<?php echo $imageurl; ?>/icon_picker_newest.gif) 0px -14px no-repeat;
	width:9px;
	height:7px;
	margin:0px 2px;
	display:block;
}
a.picker_older {
	background:url(<?php echo $imageurl; ?>/icon_picker_older.gif) 32px 0px no-repeat;
	text-indent:3px;
	width:40px;
	height:7px;
	margin:0px 2px;
	display:block;
}
a.picker_older:hover {
	background-position:32px -7px;
}
li.picker_older {
	background:url(<?php echo $imageurl; ?>/icon_picker_older.gif) 32px -14px no-repeat;
	text-indent:3px;
	width:40px;
	height:7px;
	margin:0px 2px;
	display:block;
}
a.picker_oldest {
	background:url(<?php echo $imageurl; ?>/icon_picker_oldest.gif) no-repeat;
	width:9px;
	height:7px;
	margin:0px 2px;
	display:block;
}
a.picker_oldest:hover {
	background-position:0% -7px;
}
li.picker_oldest {
	background:url(<?php echo $imageurl; ?>/icon_picker_oldest.gif) 0px -14px;
	no-repeat;
	width:9px;
	height:7px;
	margin:0px 2px;
	display: block;
}
#flashContainerDiv {
	margin-left:27px;
	text-align:center;
	width:512px;
	height:426px;
	background-color:#000;
	border:1px solid #cbd2d7;
}
#flashContainerDiv div {
	color:#666;
	font-size:20px;
	padding: 100px 50px 50px 50px;
}
#bricksPrivate,#bricksError {
	width:480px;
	float:left;
	padding:0px;
	font-size:18px;
	display:inline;
	margin:60px 40px 0px 100px;
}
#bricksPrivate .more {
	display:inline;
	font-size:11px;
	margin-left:10px;
}
#bricksError {
	margin-bottom:250px;
}
#bricks {
	width:620px;
	float:left;
	margin:15px 0px 0px 0px;
	padding:0px;
	display:inline;
	background:#fff;
	position:relative;
	z-index: 2000;
}
#bricks2 {
	width:533px;
	float:left;
	margin:15px 0px 0px 0px;
	padding:0px;
	display:inline;
	position:relative;
	z-index: 2000;
}
#bricks.generalContent {
	width:598px;
	margin-right:22px;
	margin-left:22px;
}
#bricks h2 {
	color:#f33408;
	font-size:20px;
	margin:0px 0px 10px 22px;
	display:inline;
}
#bricks h2 a.link {
	color:#f33408;
}
#bricks h3 {
	color:#f33408;
	font-size:14px;
	margin:0px 10px 10px 22px;
	display:inline;
}
#bricks h2.viewall {
	text-transform:lowercase;
}
#bricks h2.viewall span {
	text-transform:none;
}
#bricks h2.viewall .more {
	font-size:12px;
	color:#8b989A;
}
#bricks ul {
	width:607px;
	list-style-type:none;
	margin:10px 0px 10px 22px;
	padding:0px;
}
#bricks ul li {
	margin:0px 0px 7px 0px;
	padding:0px;
	float:left;
	display:inline;
}
#bricks ul li.first {
	width:36px;
	margin:8px 8px 0px 0px;
	padding:0px;
	display:inline;
}
#bricks2 ul {
	width:533px;
	list-style-type:none;
	margin:10px 0px 10px 22px;
	padding:0px;
}
#bricks2 ul li {
	margin:0px 0px 7px 0px;
	padding:0px;
	float:left;
	display:inline;
}
#bricks2 ul li.first {
	width:36px;
	margin:8px 8px 0px 0px;
	padding:0px;
	display:inline;
}
#bricks .divider {
	background:url(<?php echo $imageurl; ?>/divider_large.gif) no-repeat;
	width:622px;
	height:3px;
	float:left;
	font-size:0px;
	margin-top:10px;
	margin-bottom:15px;
	clear:both;
}
#bricks .more {
	display:inline;
	font-size:11px;
	margin-left:10px;
}
#bricks .more2 {
	display:inline;
	font-size:14px;
	text-align:right;
	float:right;
	margin:5px 12px 0px 0px;
}
#bricks .mainHeading {
	position:relative;
	width:100%;
}
#bricks .shadedArea,#bricks .shadedAreaContent {
	height:24px;
	float:left;
	display:inline;
}
#bricks .shadedArea {
	background:url(<?php echo $imageurl; ?>/rounded_corners_gray_left.gif) top left no-repeat;
	padding-left:8px;
}
#bricks .shadedAreaContent {
	background:url(<?php echo $imageurl; ?>/rounded_corners_gray_right.gif) top right no-repeat;
	padding-right:8px;
	padding-top:6px;
	height:18px;
	width:578px;
}
#bricks .pagingArrows {
	float:right;
	margin:10px 5px 0px 0px;
	font-size:12px;
	line-height:12px;
}
#bricks .pagingArrows.top {
	margin-top:11px;
}
#bricks .pagingArrows .newest {
	float:left;
	margin:-1px 4px 0px 0px;
}
#bricks .pagingArrows a.newer {
	float:left;
	background:url(<?php echo $imageurl; ?>/arrow_paging_left.gif) no-repeat left;
	width:33px;
	padding-left:14px;
}
#bricks .pagingArrows a.newer:hover {
	background:url(<?php echo $imageurl; ?>/arrow_paging_left_over.gif) no-repeat left;
}
#bricks .pagingArrows a.older {
	float:left;
	background:url(<?php echo $imageurl; ?>/arrow_paging_right.gif) no-repeat right;
	width:41px;
}
#bricks .pagingArrows a.older:hover {
	background:url(<?php echo $imageurl; ?>/arrow_paging_right_over.gif) no-repeat right;
}
#bricks .pagingArrows .pipe {
	float:left;
	margin:0px 10px;
	width:0px;
	border-left:1px solid #d3d3d3;
}
#smsg {
	float:left;
	display:inline;
	position:relative;
	width:275px;
	margin:8px 5px 0px 8px;
	padding:8px;
	background-color:#f3f5f6;
	border:1px solid #e5eaeb;
	font-size:12px;
	color:#6f6f6d;
}
#smsg input {
	float:left;
	margin:0px 5px 0px 0px;
	padding:0px;
	width:178px;
	height:15px;
    font-size:12px;
}
#bricks #feedSettings,#bricks #followSettings,#bricks #followSettingsFriendsOnly {
	float:left;
	display:inline;
	position:relative;
	width:575px;
	margin:8px 5px 0px 22px;
	padding:8px;
	background-color:#f3f5f6;
	border:1px solid #e5eaeb;
	font-size:12px;
	color:#6f6f6d;
}
#bricks #feedSettings input,#bricks #followSettings input {
	float:left;
	margin:0px 5px 0px 0px;
	padding:0px;
	width:15px;
	height:15px;
}
#bricks #feedSettings label {
	display:block;
	float:left;
	width:135px;
}
#bricks #followSettings,#bricks #followSettingsFriendsOnly {
	margin-bottom:15px;
}
#bricks #followSettings .heading,#bricks #followSettingsFriendsOnly .heading {
	float:left;
	font-weight:bold;
	font-size:13px;
	margin-bottom:10px;
	width:100%;
}
#bricks #followSettings .heading .icon,#bricks #followSettingsFriendsOnly .heading .icon {
	float:left;
	margin-right:5px;
}
#bricks #followSettings .heading .confirm,#bricks #followSettingsFriendsOnly .heading .deny {
	float:left;
	height:19px;
	line-height:19px;
	color:#5baf00;
}
#bricks #followSettingsFriendsOnly .heading .deny {
	color:#4f4f4f;
}
#bricks #followSettings div.setting {
	clear:left;
	float:left;
	display:inline;
	width:575px;
	margin-bottom:8px;
}
#bricks #followSettings label {
	float:left;
	height:16px;
	line-height:16px;
}
#bricks #followSettings .info {
	float:left;
	height:16px;
	line-height:16px;
	margin-left:5px;
}
#bricks #followSettings .link {
	line-height:24px;
}
#bricks #feedSettings .close,#bricks #followSettings .close,#bricks #followSettingsFriendsOnly .close {
	position:absolute;
	top:6px;
	right:8px;
	line-height:7px;
}
#bricks #followUserBtn {
	position:absolute;
	top:0px;
	right:5px;
	z-index: 2001;
}
#bricks a.settingsBtnAlerts,#bricks a.settingsBtnAlerts span,#bricks a.settingsBtn,#bricks a.settingsBtn span {
	float:left;
	display:block;
	height:24px;
	line-height:24px;
	font-family:Arial,Helvetica,sans-serif;
	font-size:12px;
	color:#215e07;
	text-align:center;
	white-space:nowrap;
	text-decoration:none;
	cursor:pointer;
}
#bricks a.settingsBtnAlerts {
	background:url(<?php echo $imageurl; ?>/btn_settings_alerts_left.gif) top left no-repeat;
	padding-left:34px;
}
#bricks a.settingsBtn {
	background:url(<?php echo $imageurl; ?>/btn_settings_left.gif) top left no-repeat;
	padding-left:10px;
}
#bricks a.settingsBtnAlerts.closed span,#bricks a.settingsBtn.closed span {
	background:url(<?php echo $imageurl; ?>/btn_settings_closed_right.gif) top right no-repeat;
	padding:0px 22px 0px 3px;
}
#bricks a.settingsBtnAlerts.open span,#bricks a.settingsBtn.open span {
	background:url(<?php echo $imageurl; ?>/btn_settings_opened_right.gif) top right no-repeat;
	padding:0px 22px 0px 3px;
}
#bricks a.settingsBtnAlerts:hover,#bricks a.settingsBtn:hover {
	background-position:0% -24px;
	text-decoration:none;
}
#bricks a.settingsBtnAlerts:hover span,#bricks a.settingsBtn:hover span {
	background-position:100% -24px;
	text-decoration:none;
}
#bricks .faqExpandPanel {
	position:relative;
	float:left;
	width:570px;
	margin:-5px 0px 15px 22px;
	padding:12px;
	background-color:#f3f5f6;
	border:1px solid #e5eaeb;
	font-size:12px;
	line-height:16px;
	color:#6f6f6d;
}
#bricks .faqExpandPanel ul {
	list-style-type:disc;
	list-style-position:outside;
	width:450px;
	margin:0px 0px 0px 13px;
}
#bricks .faqExpandPanel ul li {
	display:list-item;
	margin:0px 0px 5px 0px;
}
#bricks .faqExpandPanel .heading {
	font-size:13px;
	font-weight:bold;
	margin-bottom:8px;
}
#bricks .faqExpandPanel .close {
	position:absolute;
	top:2px;
	right: 8px;
}
#bricks h2.searchUpdatesHeading {
	float:left;
	width:600px;
}
#bricks h3.searchUpdatesTopic {
	color:#888888;
	font-size:12px;
}
#bricks ul.searchUpdatesTopic {
	margin-top:5px;
}
#bricks.search h2 {
	float:left;
	width:600px;
}
#bricks.search .searchDivider {
	clear:both;
	float:left;
	margin-left:22px;
	margin-top:5px;
	margin-bottom:5px;
	height:1px;
	line-height:1px;
	background-color:#ddd;
	width:596px;
	font-size:0px;
}
#bricks.search .searchDivider.topicDivider {
	margin-top:6px;
	margin-bottom:15px;
}
#bricks.search .subheading {
	float:left;
	display:inline;
	width:596px;
	margin:0px 0px 10px 22px;
	font-size:15px;
	line-height:15px;
	color:#6f6f6d;
}
#bricks.search .subheading div {
	float:left;
}
#bricks.search .subheading a.viewLink {
	float:right;
	display:block;
	font-size:12px;
}
#bricks.search .subheading.separator {
	margin-top:15px;
}
#rightPanel.search {
	margin:25px 0px 75px;
}
#rightPanel.search .seeMore {
	margin:10px 0px 0px 8px;
}
#rightPanel.search .seeMore a {
	font-size:13px
}
#rightPanel.search .options {
	clear:left;
	float:left;
	margin-left:8px;
	width:250px;
}
#rightPanel.search .options input {
	float:left;
	margin:0px;
	padding:0px;
}
#rightPanel.search .options label {
	float:left;
	color:#8f8f8f;
	font-size:12px;
	margin-left:8px;
}
#rightPanel.search .options.multiple div {
	float:left;
	margin-bottom:10px;
	width:100%;
}
#rightPanel.search .options.multiple .submit {
	float:left;
	margin-top:5px;
}
#rssArea {
	margin:25px 0px 0px 22px;
	width:597px;
	border-top:1px solid #e0e1dc;
	padding-top:10px;
}
#rssArea .rssIcon {
	margin:0px 8px -3px 0px;
}
.brick {
	background:url(<?php echo $imageurl; ?>/brick_update_bg.gif) repeat-y;
	width:551px;
	margin:0px;
	float:left;
	padding:0px;
	position:relative;
	z-index: 0
}
.brick2 {
	background:url(<?php echo $imageurl; ?>/brick2_update_bg.gif) repeat-y;
	width:533px;
	margin:0px;
	float:left;
	padding:0px;
	position:relative;
	z-index: 0
}
.brick_top {
	background:url(<?php echo $imageurl; ?>/brick_update_top.gif) no-repeat;
	width:551px;
	height:36px;
}
.brick2_top {
	background:url(<?php echo $imageurl; ?>/brick2_update_top.gif) no-repeat;
	width:533px;
	height:36px;
}
.brick_btm {
	background:url(<?php echo $imageurl; ?>/brick_update_btm.gif) no-repeat;
	width:551px;
	height:21px;
	float:left;
	font-size:0px;
}
.brick2_btm {
	background:url(<?php echo $imageurl; ?>/brick2_update_btm.gif) no-repeat;
	width:533px;
	height:21px;
	float:left;
	font-size:0px;
}
.brick_content {
	margin:-31px 0px -16px 17px;
	width:525px;
	float:left;
	display:inline;
	position:relative;
}
.brick2_content {
	margin:-31px 0px -16px 17px;
	width:499px;
	float:left;
	display:inline;
	position:relative;
}
.brick_content .videoContainer {
	float:left;
	width:250px;
	height:208px;
	background-color:#000;
	margin-right:12px;
}
.brick_btm .videoExpandActions {
	font-size:11px;
	line-height:12px;
	position:absolute;
	left:279px;
	bottom:7px;
}
.brick_btm .videoExpandActions a.collapse {
	float:left;
	background:url(<?php echo $imageurl; ?>/icon_collapse.gif) no-repeat;
}
.brick_btm .videoExpandActions a.collapse span {
	margin-left:16px;
}
.brick_btm .videoExpandActions a.collapse:hover {
	background-position:0% -12px;
	text-decoration:none;
}
.brick_btm .videoExpandActions a.collapse:hover span {
	text-decoration:underline;
}
.brick_preview {
	width:100px;
	height:75px;
	margin:0px 12px 0px 0px;
	text-align:center;
	display:inline;
	float:left;
	border:solid 1px #cbd2d7;
	background:#000;
	position:relative;
}
.brick_preview2 {
	width:140px;
	height:115px;
	margin:10px 12px 0px 0px;
	text-align:center;
	display:inline;
	float:left;
	border:solid 1px #c9f2fc;
	background:#e1f7fc;
	position:relative;
}
.brick_online {
	position:absolute;
	bottom:0px;
	left:0px;
}
.brick_topic_icon {
	position:absolute;
	top:-3px;
	right:-5px;
}
.brick_mature_icon {
	position:absolute;
	bottom:0px;
	right:0px;
}
.brick_title {
	line-height:18px;
	display:block;
	margin:4px 0px 5px 0px;
    overflow:hidden;
}
.brick_title {
	color:#282e31;
	font-size:14px;
	line-height:18px;
	font-family:"Trebuchet MS";
}
.brick_title a {
	color:#6e7778;
}
.brick_title a.moreLink {
	font-size:11px;
}
.brick_title a:hover {
	color:#0E6DB7;
}
.brick_byline {
	color:#91b9d8;
	font-size:11px;
	display:inline;
	margin:2px 0px 5px 0px;
}
.brick_byline .link {
	color:#91b9d8;
}
.brick_byline .location {
	display:inline;
}
.brick_byline .location.separator {
	border-left:1px solid #91b9d8;
	margin-left:4px;
	padding-left:6px;
}
.brick_actions {
	color:#91b9d8;
	font-size:11px;
	position:absolute;
	bottom:7px;
	right:10px;
	text-align:right;
	line-height:12px;
}
.brick_actions .action_icon {
	float:left;
}
.brick_actions .comment_num {
	margin-right:4px;
	float:left;
}
.brick_actions a.editIcon,.brick_actions a.commentIcon,.brick_actions a.commentIconWhatsNew {
	float:left;
	display:block;
	width:12px;
	height:12px;
	cursor:pointer;
	background-position:top left;
	background-repeat:no-repeat;
}
.brick_actions a.editIcon {
	margin-top:-1px;
}
.brick_actions a.editIcon.gray {
	background-image:url(<?php echo $imageurl; ?>/icon_edit_gray2.gif);
}
.brick_actions a.editIcon.blue {
	background-image:url(<?php echo $imageurl; ?>/icon_edit_blue2.gif);
}
.brick_actions a.commentIcon {
	background-image:url(<?php echo $imageurl; ?>/icon_comment4.gif);
	width:11px;
}
.brick_actions a.commentIconWhatsNew {
	background-image:url(<?php echo $imageurl; ?>/icon_comment_whatsnew4.gif);
	width:11px;
}
.brick_actions a:hover.editIcon,.brick_actions a:hover.commentIcon,.brick_actions a:hover.commentIconWhatsNew {
	background-position:0% -12px;
	text-decoration:none;
}
.brick_icon {
	margin:0px 0px -3px 0px;
}
.brick_icon_divider {
	float:left;
	margin:-2px 6px 0px;
}
.brick.commentBrick .brick_actions {
	right:0px;
	bottom:0px;
}
.brick.commentBrick {
	background:url(<?php echo $imageurl; ?>/brick_comment_bg.gif) repeat-y;
	width:551px;
	margin:0px;
	float:left;
	padding:0px;
	position:relative;
}
.brick.commentBrick .brick_top {
	background:url(<?php echo $imageurl; ?>/brick_comment_top.gif) no-repeat;
	width:551px;
	height:36px;
}
.brick.commentBrick .brick_btm {
	background:url(<?php echo $imageurl; ?>/brick_comment_btm.gif) no-repeat;
	width:551px;
	height:21px;
	float:left;
	font-size:0px;
}
.brick.commentBrick .brick_title {
	color:#282e31;
	font-size:15px;
}
.brick.commentBrick .brick_title .content a {
	color:#282e31;
}
.brick.commentBrick .brick_title a {
	color:#6e7778;
	font-size:15px;
}
.brick.commentBrick .brick_title a:hover {
	color:#0E6DB7;
}
.brick.commentBrick .brick_content {
	overflow:visible;
}
.brick.commentBrick .brick_content .deleted {
	color:#8B989A;
	font-style:italic;
}
.brick.commentBrick .brick_byline {
	float:left;
	color:#8b989a;
	position:relative;
}
.brick.commentBrick .brick_in_response a {
	color:#8B989A;
}
.brick.commentBrick .brick_in_response a:hover {
	color:#0E6DB7;
}
.brick.sponsorBrick .brick_title a,.brick.promoBrick .brick_title a {
	font-family:Arial,Helvetica,sans-serif;
	font-size:14px;
	color:#282E31;
}
.brick.sponsorBrick .brick_actions a,.brick.promoBrick .brick_actions a {
	font-size:12px;
	font-weight:bold;
}
.brick.sponsorBrick {
	background:url(<?php echo $imageurl; ?>/brick_sponsor_bg.gif) repeat-y;
	width:551px;
	margin:0px;
	float:left;
	padding:0px;
	position:relative;
}
.brick.sponsorBrick .brick_top {
	background:url(<?php echo $imageurl; ?>/brick_sponsor_top.gif) no-repeat;
	width:551px;
	height:36px;
}
.brick.sponsorBrick .brick_btm {
	background:url(<?php echo $imageurl; ?>/brick_sponsor_btm.gif) no-repeat;
	width:524px;
	height:17px;
	padding:3px 10px 3px 17px;
	float:left;
	font-size:0px;
}
.brick.sponsorBrick .brick_byline {
	color:#e4cda1;
}
.brick.sponsorBrick .brick_content,.brick.promoBrick .brick_content {
	margin-bottom:3px;
}
.brick.sponsorBrick .brick_actions,.brick.promoBrick .brick_actions {
	bottom:27px;
}
.brick.sponsorBrick .promoHeading,.brick.promoBrick .promoHeading,.promoHeading a {
	font-size:14px;
	margin-bottom:-1px;
	color:#282e31;
	line-height:16px;
}
.brick.sponsorBrick .promoDesc,.promoDesc a {
	margin-left:5px;
	font-size:11px;
	line-height:18px;
	color:#c8b085;
}
.brick.promoBrick .brick_btm {
	background:url(<?php echo $imageurl; ?>/brick_promo_btm.gif) no-repeat;
	width:524px;
	height:17px;
	padding:3px 10px 3px 17px;
	float:left;
	font-size:0px;
}
.brick.promoBrick .promoDesc {
	margin-left:5px;
	font-size:11px;
	line-height:18px;
	color:#7da6b9;
}
.brick.promoBrick .promoDesc a {
	font-size:11px;
	line-height:18px;
	color:#7da6b9;
}
.largeBrick {
	background:url(<?php echo $imageurl; ?>/recentpost_bg2.gif) repeat-y;
	width:607px;
	margin:0px;
	float:left;
	padding:0px;
	position:relative;
	z-index: 1;
}
.largeBrick .brick_top {
	background:url(<?php echo $imageurl; ?>/recentpost_top2.gif) no-repeat;
	width:607px;
	height:54px;
}
.largeBrick .brick_btm {
	background:url(<?php echo $imageurl; ?>/recentpost_btm2.gif) no-repeat;
	width:607px;
	height:44px;
}
.largeBrick .brick_content {
	margin:-43px 0px -33px 12px;
	width:571px;
}
.largeBrick .brick_preview,.largeBrickLeft .brick_preview {
	width:176px;
	height:132px;
}
.largeBrick .brick_title,.largeBrick .brick_title a,.largeBrickLeft .brick_title,.largeBrickLeft .brick_title a {
	font-size:20px;
	line-height:23px;
}
.largeBrick .brick_title a.moreLink,.largeBrickLeft .brick_title a.moreLink {
	font-size:12px;
}
.largeBrick .brick_actions {
	margin:23px 15px 0px 0px
}
.largeBrick .spacing {
	margin:0px 4px 0px 0px;
}
.largeBrick.viewer {
	margin:5px 0px 15px 22px;
}
.largeBrick.viewer .brick_title a {
	color:#6e7778;
}
.largeBrick.viewer .brick_title a:hover {
	color:#0e6db7;
}
.largeBrick.viewer .brick_title {
	float:left;
	margin-top:8px;
	width:100%;
}
.largeBrick.viewer .brick_title div.content {
	width:530px;
}
.largeBrick.viewer .brick_byline {
	float:left;
	margin:0px 0px 10px 0px;
	position:relative;
	z-index: 1;
}
.largeBrick.viewer .updatePaging {
	margin:0px 4px 0px 10px;
}
.largeBrick.viewer .updatePaging a {
	text-decoration:none;
	cursor:pointer;
}
.largeBrick.viewer .updatePaging a#newer {
	float:left;
	background:url(<?php echo $imageurl; ?>/arrow_viewer_left.gif) no-repeat left;
	width:9px;
	margin-right:8px;
}
.largeBrick.viewer .updatePaging a#newer:hover {
	background:url(<?php echo $imageurl; ?>/arrow_viewer_left_over2.gif) no-repeat left;
}
.largeBrick.viewer .updatePaging a#older {
	float:left;
	background:url(<?php echo $imageurl; ?>/arrow_viewer_right.gif) no-repeat right;
	width:9px;
}
.largeBrick.viewer .updatePaging a#older:hover {
	background:url(<?php echo $imageurl; ?>/arrow_viewer_right_over2.gif) no-repeat right;
}
.largeBrick .viewerContent {
	width:568px;
	border:solid 1px #cbd2d7;
	background-color:#000;
	text-align:center;
	margin:0px;
	padding:0px;
	line-height:0px;
}
.largeBrickLeft {
	background:url(<?php echo $imageurl; ?>/recentpost_bg_left2.gif) repeat-y;
	width:551px;
	margin:0px;
	float:left;
	padding:0px;
	position:relative;
}
.largeBrickLeft .brick_top {
	background:url(<?php echo $imageurl; ?>/recentpost_top_left2.gif) no-repeat;
	width:551px;
	height:54px;
}
.largeBrickLeft .brick_btm {
	background:url(<?php echo $imageurl; ?>/recentpost_btm_left2.gif) no-repeat;
	width:551px;
	height:44px;
}
.largeBrickLeft .brick_content {
	margin:-43px 0px -33px 17px;
	width:525px;
}
.emptyBubble,.emptyBubbleLarge {
	font-family:"Trebuchet MS";
	color:#999292;
}
.emptyBubble {
	background:url(<?php echo $imageurl; ?>/brick_emptybubble_small.gif) top left no-repeat;
	width:555px;
	height:39px;
	padding:10px 15px 10px 23px;
	font-size:15px;
}
.emptyBubbleLarge {
	background:url(<?php echo $imageurl; ?>/brick_emptybubble_large.gif) top left no-repeat;
	width:593px;
	height:52px;
	padding:10px 0px 10px 15px;
	font-size:20px;
}
.emptyBubbleText {
	font-size:18px;
}
.exchange_preview {
	float:left;
	width:510px;
	border-top:1px solid #d6e0e5;
	padding-top:10px;
	margin-bottom:5px;
	padding-left:15px;
}
.exchange_preview.indent {
	margin-left:15px;
	padding-left:0px;
}
.exchange_preview img {
	border:none 0px;
}
.exchange_preview .brick_byline {
	margin:0px;
	float:left;
	width:100%;
}
.exchange_preview a.content {
	float:left;
	color:#52575a;
	font-size:12px;
	line-height:14px;
	font-family:"Trebuchet MS";
	margin-bottom:3px;
}
.comment_to_me_exchange_preview {
	padding-bottom:5px;
	color:#52575a;
	font-family:"Trebuchet MS";
	font-size:12px;
}
.comment_to_me_exchange_preview .content a {
	color: #52575a;
}
.postBox {
	width:600px;
	height:49px;
	position:relative;
	display:block;
}
.postBox .comment_box {
	width:538px;
	height:49px;
	background:url(<?php echo $imageurl; ?>/comment_box.gif) no-repeat;
	position:absolute;
	top:0px;
	left:0px;
}
.postBox textarea,.postBox .disabled {
	border:1px solid #fff;
	width:512px;
	height:35px;
	margin-left:5px;
	display:inline;
	position:absolute;
	top:6px;
	left:6px;
	font-family:Arial,Helvetica,sans-serif;
	font-size:14px;
}
.postBox .disabled .cta {
	font-size:16px;
	font-style:normal;
	color:#000;
	padding-top:10px;
}
.postBox .postBtn {
	position:absolute;
	top:12px;
	left:545px;
}
.postBox .postBtn a {
	width:40px;
}
.postBox .postBtn a span {
	width:30px;
}
.largeBrick.viewer .postBox {
	float:left;
	width:569px;
	border:1px solid #7ad3f1;
	background-color:#fff;
	height:66px;
	margin-bottom:30px;
	position:relative;
	z-index: 0;
}
.largeBrick2.viewer .postBox {
	float:left;
	width:569px;
	border:1px solid #7ad3f1;
	background-color:#fff;
	height:66px;
	margin-bottom:30px;
	position:relative;
	z-index: 0;
}
.largeBrick.viewer .postBox .comment_box {
	width:560px;
	background:none;
}
.largeBrick2.viewer .postBox .comment_box {
	width:560px;
	background:none;
}
.largeBrick.viewer .postBox textarea,.postBox .disabled {
	width:561px;
	height:60px;
	padding:3px 0px 0px;
	left:auto;
	top:auto;
}
.largeBrick2.viewer .postBox textarea,.postBox .disabled {
	width:561px;
	height:60px;
	padding:3px 0px 0px;
	left:auto;
	top:auto;
}
.largeBrick.viewer .postBox .postBtn {
	position:absolute;
	bottom:-31px;
	right:0px;
	left:auto;
	top:auto;
}
.largeBrick2.viewer .postBox .postBtn {
	position:absolute;
	bottom:-31px;
	right:0px;
	left:auto;
	top:auto;
}
.largeBrick.viewer .postBox .postBtn.setWidth {
	width:569px;
}
.largeBrick2.viewer .postBox .postBtn.setWidth {
	width:569px;
}
.largeBrick.viewer .postBox .postBtn a {
	width:90px;
	font-weight:bold;
}
.largeBrick2.viewer .postBox .postBtn a {
	width:90px;
	font-weight:bold;
}
.largeBrick.viewer .postBox .postBtn a span {
	width:80px;
}
.largeBrick2.viewer .postBox .postBtn a span {
	width:80px;
}
.msgBox {
	width:260px;
	height:49px;
	position:relative;
	display:inline;
	float:left;
	margin:10px 0px 10px 8px;
}
.msgBox .comment_box {
	width:245px;
	height:49px;
	background:url(<?php echo $imageurl; ?>/message_box.gif) no-repeat;
	position:absolute;
	top:0px;
	left:0px;
}
.msgBox textarea,.msgBox .disabled {
	border:1px solid #fff;
	width:225px;
	height:35px;
	display:inline;
	position:absolute;
	top:6px;
	left:6px;
	font-family:Arial,Helvetica,sans-serif;
	font-size:12px;
}
.msgBox .sendBtn {
	position:absolute;
	top:12px;
	left:250px;
}
.postBox .defaultText,.postBox .disabled,.msgBox .defaultText,.msgBox .disabled {
	font-style:italic;
	color:#8f8f8f;
}
.postBox .commentText,.msgBox .msgText {
	font-family:"Trebuchet MS";
	font-style:normal;
	color:#000;
}
.postBox .disabled,.msgBox .disabled {
	text-align:center;
	line-height:35px;
}
.msgBox .disabled div {
	line-height:14px;
	margin-top:5px;
}
.postBoxContainer {
	float:left;
	display:inline;
	margin-left:22px;
	margin-top: 10px;
}
#bricks ul.viewallBricks {
	margin-top:0px;
	clear:both;
}
.userBrick {
	width:596px;
	margin:0px;
	float:left;
	padding:0px;
	background-color:#ececec;
}
.userBrick_top {
	background:url(<?php echo $imageurl; ?>/brick_user_top.gif) no-repeat;
	width:596px;
	height:19px;
	font-size:0px;
}
.userBrick_content {
	border:1px solid #cdcbcb;
	border-top:0px solid #cdcbcb;
	width:584px;
	padding:0px 5px 5px;
	margin-top:-14px;
	position:relative;
	float:left;
	display:inline;
}
.userBrick .userInfo {
	float:left;
	display:inline;
	font-size:12px;
	width:188px;
	overflow:hidden;
}
.userBrick .title {
	font-size:17px;
	margin-top:2px;
}
.userBrick .motto {
	font-size:13px;
	font-family:"Trebuchet MS";
	color:#6f6f6d;
	margin-bottom:18px;
}
.userBrick .infoline {
	position:absolute;
	bottom:5px;
	font-size:12px;
	color:#80807d;
}
.userBrick .infoline .settings {
	float:left;
	margin-right:6px;
}
.userBrick .infoline .pipe {
	float:left;
	margin:0px 6px;
	border-left:1px solid #cdcbcb;
	width:0px;
}
.userBrick .infoline .icon {
	float:left;
}
.userBrick .infoline .friendIcon {
	float:left;
	margin-top:-1px;
}
.userBrick .latestPost,.userBrick .latestPostDefault {
	background:url(<?php echo $imageurl; ?>/brick_user_postbubble.gif) no-repeat;
	width:276px;
	height:68px;
	margin:2px 1px 0px 0px;
	position:relative;
	float:right;
}
.userBrick .latestPostDefault {
	background:url(<?php echo $imageurl; ?>/brick_user_postbubble_default.gif) no-repeat;
}
.userBrick .latestPost_content {
	margin:7px 5px 5px 17px;
	text-align:left;
	overflow:hidden;
	color:#282e31;
}
.userBrick .latestPost .byline {
	clear:left;
	color:#91b9d8;
}
.userBrick .latestPost_icon {
	width:36px;
	height:27px;
	display:inline;
	float:left;
	border:solid 1px #cbd2d7;
	text-align:center;
	margin:1px 10px 5px 0px;
}
.userBrick .latestPost_title,.userBrick .latestPost_titleDefault {
	display:block;
	margin-bottom:5px;
}
.userBrick .latestPost_title a {
	color:#282e31;
	font-size:13px;
	font-family:"Trebuchet MS";
}
.userBrick .latestPost_titleDefault {
	color:#999292;
	font-size:14px;
	line-height:18px;
	font-style:italic;
	font-family:Arial,Helvetica,sans-serif;
	margin:10px 0px 0px 10px;
}
.userBrick .statusIcon {
	padding-bottom: 1px;
}
#rightPanel {
	position:relative;
	z-index:1;
	display:inline;
	float:right;
	width:328px;
	margin:15px 0px 0px 0px;
}
#rightPanel h2 {
	color:#f33408;
	font-size:12px;
	margin:0px 0px 10px 8px;
	display:inline;
	float:left;
}
#rightPanel h2 a {
	color:#f33408;
	text-decoration:none;
}
#rightPanel h2 a:hover {
	text-decoration:underline;
}
#rightPanel h2.sidebartop {
	display:block;
	width:292px;
	float:none;
}
#rightPanel h2.follow {
	display:block;
	width:292px;
	float:none;
}
#rightPanel h2.exchanging {
	display:block;
	float:none;
	margin-bottom:5px;
	margin-left:0px;
}
#rightPanel h2.viewAll {
	margin-bottom:5px;
}
#rightPanel h2.sidebargrid {
	margin-bottom:0px;
}
#rightPanel .userName {
	display:inline;
	font-size:20px;
	font-weight:normal;
	margin:0px 10px 0px 0px;
}
#rightPanel .userName a {
	color:#f33408;
	text-decoration:none;
}
#rightPanel .userName a:hover {
	text-decoration:underline;
}
#rightPanel .edit {
	line-height:12px;
	display:block;
	margin:10px 0px;
}
#rightPanel .edit a {
	font-size:11px;
	color:#0e6db7;
	font-weight:normal;
}
#rightPanel .edit img {
	vertical-align:middle;
}
#rightPanel .userPic,#rightPanel .userPicPreview {
	border:solid 1px #cbd2d7;
	margin:0px 60px 10px 8px;
	width:220px;
	height:165px;
	background:#fff;
	padding:1px;
	display:inline;
	text-align:center;
	float:left;
}
#rightPanel .userPicPreview {
	margin-right:0px;
	width:100px;
	height:75px;
}
#rightPanel .userPreviewInfo {
	float:left;
	display:inline;
	width:178px;
	margin-left:10px;
	color:#8b989a;
	font-size:11px;
}
#rightPanel .profilePic {
	border:solid 1px #cbd2d7;
	background:#fff;
	padding:1px;
	text-align:center;
}
#rightPanel .profilePic.mini {
	width:36px;
	height:27px;
}
#rightPanel .profilePic.small {
	width:60px;
	height:45px;
}
#rightPanel .profilePic.medium {
	width:100px;
	height:75px;
}
#rightPanel .profilePic.large {
	width:164px;
	height:123px;
}
#rightPanel .profilePic.channel {
	width:220px;
	height:165px;
}
#rightPanel .profilePic .middle {
	vertical-align:middle;
}
#rightPanel .profileAreaSmall {
	color:#8b989a;
	line-height:14px;
	width:292px;
}
#rightPanel .profileAreaSmall .pic {
	float:left;
	margin-right:8px;
}
#rightPanel .profileAreaSmall .content {
	float:left;
	width:220px;
}
#rightPanel .profileAreaSmall .content .name {
	font-size:12px;
	line-height:18px;
}
#rightPanel .profileAreaMedium {
	float:left;
}
#rightPanel .profileAreaMedium .pic {
	float:left;
	margin:0px 10px 0px 8px;
}
#rightPanel .profileAreaMedium .content {
	float:left;
	width:180px;
	color:#8b989a;
	font-size:12px;
	line-height:16px;
}
#rightPanel .userInfo {
	margin:7px 0px 0px 8px;
	font-size:12px;
	color:#8b989a;
}
#rightPanel .more,#rightPanel .view {
	font-size:11px;
	margin:0px 5px 0px 0px;
	text-align:right;
	float:right;
	display:inline;
}
#rightPanel .seeAll {
	display:inline;
	float:right;
	font-size:12px;
	margin:0px 5px 8px 0px;
	text-align:right;
}
#rightPanel .view {
	margin-top:6px;
	margin-right:0px;
}
#rightPanel .moreCircle {
	width:300px;
	line-height:18px;
	position:relative;
	display:inline;
	float:left;
	margin:10px 0px 0px 8px;
	vertical-align:top;
	text-align:left;
	font-size:12px;
	color:#8B989A;
}
#rightPanel .circleNew {
	vertical-align:middle;
	margin:-1px 8px 0px 0px;
}
#rightPanel .circleMore {
	position:absolute;
	bottom:0px;
	right:5px;
}
#rightPanel .more2 a {
	display:inline;
	font-size:11px;
	color:#0e6db7;
	font-weight:normal;
	margin:0px 0px 10px 0px;
}
#rightPanel .divider {
	background:url(<?php echo $imageurl; ?>/divider.gif) no-repeat;
	width:328px;
	height:3px;
	display:inline;
	float:right;
	font-size:0px;
	margin:12px 0px;
}
#rightPanel .dividerFilterAd {
	margin-top:0px;
	margin-bottom:16px;
}
#rightPanel .admin {
	width:320px;
	margin:0px;
}
#rightPanel .contentBlock {
	display:inline;
	float:left;
	width:300px;
	position:relative;
}
#rightPanel .contentBlock.withPaging {
	width:308px;
}
#rightPanel .contentBlock p {
	float:left;
	font-size:12px;
	margin:0px 8px;
}
#rightPanel .grid {
	width:300px;
	position:relative;
	display:inline;
	float:left;
	margin-left:8px;
	padding:0px;
}
#rightPanel .grid .gridItemContainer {
	position:relative;
	display:inline;
	float:left;
	margin:0px 9px 9px 0px;
	padding:0px;
	width:41px;
	height:31px;
}
#rightPanel .gridBottomSeparator {
	width:287px;
	position:relative;
	display:inline;
	float:left;
	border-bottom:1px solid #CBD2D7;
	margin:8px 5px 8px 8px;
	padding:0px;
}
#rightPanel .grid .selected a {
	border:2px solid #0e6db7;
}
#rightPanel .usersidebar.grid {
	clear:left;
}
#rightPanel .usersidebar.grid .gridItemContainer {
	margin:9px 9px 0px 0px;
}
#rightPanel .gridPaging {
	float:left;
	display:inline;
	width:308px;
	height:31px;
}
#rightPanel .gridPaging .grid {
	float:left;
	margin:0px;
	position:relative;
	width:292px;
}
#rightPanel .gridPaging .grid .selected {
	border:2px solid #0e6db7;
}
#rightPanel .gridPaging .grid .last {
	margin-right:0px;
}
#rightPanel .gridPaging .grid .gridItemContainer {
	margin-bottom:0px;
}
#rightPanel .gridPaging .nextArea,#rightPanel .gridPaging .prevArea {
	float:left;
	width:8px;
	margin-top:2px;
}
#rightPanel .gridPaging .nextArea a,#rightPanel .gridPaging .prevArea a {
	display:block;
	width:8px;
	height:27px;
	background-image:url(<?php echo $imageurl; ?>/arrow_scroll_right.gif);
}
#rightPanel .gridPaging .prevArea a {
	background-image:url(<?php echo $imageurl; ?>/arrow_scroll_left.gif);
}
#rightPanel .gridPaging .nextArea a:hover {
	background-image:url(<?php echo $imageurl; ?>/arrow_scroll_right_over.gif);
	text-decoration:none;
}
#rightPanel .gridPaging .prevArea a:hover {
	background-image:url(<?php echo $imageurl; ?>/arrow_scroll_left_over.gif);
	text-decoration:none;
}
#rightPanel .gridPaging #updateLoading {
	float:left;
	padding:5px 0px 0px 134px;
}
#rightPanel .byline,#rightPanel .bylineOwner {
	float:left;
	display:inline;
	color:#8b989a;
	font-size:11px;
	margin-left:8px;
}
#rightPanel .bylineOwner {
	margin:0px 0px 10px 8px;
	padding-top:8px;
}
#rightPanel .title {
	color:#282e31;
	font-size:14px;
	line-height:18px;
	display:block;
	font-family:"Trebuchet MS";
	margin:4px 0px 5px 8px;
}
#rightPanel .motto {
	clear:both;
	width:292px;
	float:left;
	display:inline;
	color:#282e31;
	font-size:14px;
	line-height:18px;
	font-family:"Trebuchet MS";
	margin:4px 0px 10px 8px;
}
#rightPanel .userPreviewInfo .motto {
	display:block;
	float:none;
	font-size:12px;
	line-height:normal;
	margin:0px 0px 5px 0px;
}
#rightPanel .whatsNewBubble {
	position:absolute;
	top:-3px;
	right:-2px;
}
#rightPanel ul.dashboard {
	margin:0px 0px 10px 8px;
	float:left;
	padding:0px;
	display:inline;
}
#rightPanel ul.dashboard.multiple {
	background-color:#f3f8fb;
}
#rightPanel ul.dashboard li {
	margin:0px;
	display:inline;
	background:url(<?php echo $imageurl; ?>/rounded_corners_blue_left.gif) top left no-repeat;
	padding-left:3px;
}
#rightPanel ul.dashboard li.first {
	margin-top:-3px;
}
#rightPanel ul.dashboard li .dashboardDivider {
	width:280px;
	margin:0px 5px;
	height:2px;
	background-color:#fff;
}
#rightPanel ul.dashboard li.last {
	margin-bottom:-3px;
	border:none;
}
#rightPanel ul.dashboard li .dashboardContent {
	background:url(<?php echo $imageurl; ?>/rounded_corners_blue_right.gif) top right no-repeat;
	width:280px;
	height:21px;
	padding:8px 6px 0px;
}
#rightPanel .shadedBrownAreaContent {
	width:283px;
	margin-left:8px;
	padding:5px;
	background:#F0EFEB;
	float:left;
	display:inline;
}
#rightPanel .shadedBrownAreaContent {
	width:283px;
	margin-left:8px;
	padding:5px;
	background:#F0EFEB;
	float:left;
	display:inline;
}
#rightPanel #scritterFeedsBox {
	float:left;
	display:inline;
	position:relative;
	background:url(<?php echo $imageurl; ?>/scritterfeeds_box.gif) top left no-repeat;
	width:284px;
	height:58px;
	margin:0px 0px 15px 8px;
	padding:10px 0px 0px 10px;
	font-size:15px;
	color:#8b989a;
}
#rightPanel #scritterFeedsBox a {
	position:absolute;
	top:14px;
	right:15px;
	font-size:11px;
}
#rightPanel .badge {
	margin:0px 0px -4px 5px;
}
#rightPanel .privacyAreaContent {
	width:169px;
	height:21px;
	padding-top:8px;
	padding-right:6px;
	text-align:center;
}
#rightPanel ul {
	padding:0px;
	list-style-type:none;
	margin:10px 0px 10px 8px;
}
#rightPanel li {
	display:inline;
	float:left;
	margin:0px 4px 10px 0px;
}
#rightPanel li.end {
	margin-right:0px;
}
#rightPanel #updateMap {
	float:left;
	width:298px;
	height:298px;
	margin-left:8px;
	border:1px solid #ccc;
}
#rightPanel #customBadge {
	float:left;
	margin:10px 0px 10px 8px;
}
#rightPanel.topicSidebar #customBadge,#rightPanel.viewer #customBadge {
	margin-bottom:15px;
}
#rightPanel #customBadge.topicViewer {
	margin-top:20px;
	margin-bottom: 3px;
}
ul#actionButtons {
	float:left;
	display:inline;
	margin:0px 0px 10px 8px;
}
ul#actionButtons li {
	margin:0px 8px 0px 0px;
	padding-right:8px;
	border-right:1px solid #a6a6a6;
	height:14px;
	line-height:14px;
}
ul#actionButtons li img {
	padding-right:7px;
	vertical-align:middle;
	text-decoration:none;
}
ul#actionButtons li a {
	text-decoration:none;
}
ul#actionButtons li a:hover span {
	text-decoration:underline;
}
ul#actionButtons li.end {
	border:none;
}
.largeBrick.viewer ul#actionButtons {
	margin:-19px 0px 0px 3px;
	width:571px;
}
.largeBrick.viewer ul#actionButtons.belowPost {
	margin-top:12px;
	margin-bottom:4px;
}
.largeBrick.viewer ul#actionButtons li {
	border-right-color:#91b9d8;
}
.largeBrick.viewer ul#actionButtons li img {
	padding-right:5px;
}
.largeBrick.viewer ul#actionButtons li.rating {
	color:#91b9d8;
}
.largeBrick.viewer ul#actionButtons li.rating img {
	padding-right:0px;
}
.largeBrick.viewer ul#actionButtons li.end {
	margin-right:0px;
}
#rightPanel .adArea {
	clear:both;
	margin-left:8px;
}
#rightPanel .adtext,#rightColumn .adtext,#rc .adtext {
	color:#8f8f8f;
	margin-bottom:6px;
}
#sidebarUserInfo {
	float:left;
	display:inline;
	font-size:11px;
	line-height:18px;
	margin:5px 0px 16px 8px;
	width:290px;
}
#sidebarUserInfo span {
	color:#8B989A;
}
#sidebarUserInfo div.spacer {
	clear:both;
}
#sidebarUserInfo span.linksLabel {
	clear:left;
	display:block;
	float:left;
}
#sidebarUserInfo div.links {
	float:left;
	margin-top:2px;
	margin-left:5px;
	line-height:14px;
}
#rightPanel #leaderBadgeArea {
	float:left;
	display:inline;
	width:300px;
	position:relative;
	margin:5px 0px 5px 8px;
}
#rightPanel #leaderBadgeArea.ownerView {
	margin-bottom: 12px;
}
.browsePagination {
	position:absolute;
	top:0px;
	right:10px;
}
.timeheading {
	margin:5px 0px;
	color:#b4b3b1;
	border-bottom:1px solid #b4b3b1;
	padding:5px 0px 5px 0px;
	width:180px;
	text-indent:23px;
}
#filter {
	margin-top:62px;
	display:inline;
	float:right;
}
#filter.viewAll {
	margin-top:35px;
	float:none;
	display:block;
}
#filter .arrow {
	background:url(<?php echo $imageurl; ?>/browse-arrow-out.gif) no-repeat;
	width:12px;
	height:14px;
	position:absolute;
	top:2px;
	left:-14px;
}
#filter .arrow-up {
	background:url(<?php echo $imageurl; ?>/browse-arrow-up.gif) no-repeat;
	width:12px;
	height:14px;
	position:absolute;
	top:2px;
	left:-14px;
}
#rightPanel #filter h2 {
	margin-bottom:4px;
}
#rightPanel #filter ul {
	margin:0px;
	padding:0px;
}
#rightPanel #filter li {
	margin:0px 0px 10px 20px;
	font-size:14px;
	display:inline;
	position:relative;
	width:308px;
	display:inline;
	padding:6px 0 4px;
	height:20px;
}
#rightPanel #filter li.btm {
	border:none;
	padding:0px;
}
#rightPanel #filter li.options {
	border:none;
	font-size:12px;
	margin-bottom:10px;
	padding:0px;
}
#rightPanel #filter li.options li {
	margin:0px 0px 10px 0px;
}
#rightPanel #filter li.options p {
	width:236px;
	background:#f0efeb;
	padding:10px 10px 10px 11px;
	margin:0px;
	float:left;
}
#rightPanel #filter li.options select {
	margin:0px 0px 5px 0px;
	width:235px;
}
#rightPanel #filter li.options .update {
	margin:10px 0px 0px 0px;
	float:right;
}
#rightPanel #filter li.options select.ziparea {
	width:140px;
	display:inline;
	float:right;
}
#rightPanel #filter li.options input.zipcode {
	width:80px;
	display:inline;
	margin:0px 5px 5px 0px;
	font-size:11px;
	float:left;
}
#rightPanel #filter li.options label {
	color:#8f8f8f;
	display:inline;
	padding-bottom:5px;
	float:left;
	width:240px;
}
#rightPanel #filter li.options label.hide {
	padding:0px;
}
#rightPanel #filter li.options input[type="radio"],#rightPanel #filter li.options input[type="checkbox"] {
	margin:2px 10px 0px 0px;
}
#rightPanel #filter li.selected {
	margin:-11px 0px 10px 0px;
	border:none;
	padding:0px;
	position:relative;
	height:30px;
}
#rightPanel #filter li.divider {
	width:328px;
	margin:0px 0px 10px 0px;
	border:none;
	padding:0px;
	height:3px;
}
#rightPanel #filter li.selectedfirst {
	margin-top:0px;
	float:left;
}
#rightPanel #filter li.selected h2 {
	background:url(<?php echo $imageurl; ?>/browse-tab.gif) no-repeat;
	position:absolute;
	width:328px;
	left:0px;
	text-indent:20px;
	color:#000;
	margin:0px;
	font-size:14px;
	padding:7px 0px 7px;
}
#rightPanel #filter li#advancedForm {
	height:auto;
}
#filter select {
	margin:0px 0px 10px 8px;
	width:275px;
	padding:0px;
	font-size:13px;
	height:25px;
	line-height:25px;
}
@media screen and (-webkit-min-device-pixel-ratio:0) {
	#filter select {
		background:none;
		border: 1px solid #ccc;
	}
}
#filter option {
	height:25px;
}
#filter span {
	display:inline;
}
#filter span.advanced {
	display: block;
}
#uploadBox {
	float:left;
	display:inline;
	position:relative;
}
#uploadBox.usermain {
	margin:10px 0px 0px 22px;
}
#uploadBox .post_brick {
	background:url(<?php echo $imageurl; ?>/brick_upload_bg_new.gif) repeat-y;
	width:592px;
	margin-left:0px;
	padding:0px;
	float:left;
	display:inline;
}
#uploadBox .post_brick_top {
	background:url(<?php echo $imageurl; ?>/brick_upload_top_new.gif) no-repeat;
	width:592px;
	height:56px;
}
#uploadBox .post_brick_btm,.post_brick_btm_left {
	background:url(<?php echo $imageurl; ?>/brick_upload_btm_new2.gif) no-repeat;
	width:592px;
	height:32px;
	float:left;
}
#uploadBox .post_brick_btm_left {
	background:url(<?php echo $imageurl; ?>/brick_upload_btm_left.gif) no-repeat;
}
#uploadBox .post_brick_content {
	margin:-47px 10px -14px;
	width:572px;
	position:relative;
	float:left;
	display:inline;
}
#uploadBox .post_brick_progress {
	position:absolute;
	top:0px;
	left:0px;
}
#uploadBox .post_brick #addButton,.post_brick #removeButton {
	width:76px;
	height:56px;
	text-align:center;
	float:left;
	padding-top:10px;
	margin-right:8px;
	font-size:11px;
}
#uploadBox .post_brick #addButton img,.post_brick #removeButton img {
	padding-bottom:3px;
}
#uploadBox .post_brick form {
	float:left;
	margin:0px;
	width:481px;
}
#uploadBox .post_brick #postTextBox {
	font-size:14px;
	border:1px solid #fff;
	width:481px;
	height:56px;
	padding:4px 0px 4px 4px;
}
#uploadBox .post_brick .defaultText {
	font-family:Arial,Helvetica,sans-serif;
	font-style:italic;
	color:#8f8f8f;
}
#uploadBox .post_brick .uploadText {
	font-family:"Trebuchet MS",Arial,Helvetica,sans-serif;
	font-style:normal;
	color:#000;
}
#uploadBox .post_brick .disabled {
	font-family:Arial,Helvetica,sans-serif;
	color:#000;
	font-size:16px;
	text-align:center;
	width:487px;
	float:left;
	padding-top:22px;
}
#uploadBox .post_brick #fileBrowse {
	margin:0px 0px 8px 5px;
	font-size:11px;
	font-family:Arial,Helvetica,sans-serif;
	width:368px;
}
#uploadBox .post_brick #uploadStatus {
	color:#000;
}
#uploadBox .post_brick #uploadStatus .progress {
	font-size:12px;
	padding-top:15px;
	text-align:center;
	float:left;
	width:82px;
	height:51px;
	text-transform:lowercase;
}
#uploadBox .post_brick #uploadStatus .progressMessage {
	font-family:Arial,Helvetica,sans-serif;
	font-style:italic;
	color:#8f8f8f;
	font-size:16px;
	margin-top:10px;
	padding:4px 0px 0px 15px;
	float:left;
}
#uploadBox .post_buttons_container {
	clear:both;
	float:left;
	width:592px;
}
#uploadBox .post_buttons {
	float:left;
	display:inline;
	margin-top:2px;
}
#uploadBox .post_buttons #updateButton,.post_buttons #cancelButton {
	width:84px;
}
#uploadBox .post_buttons #updateButton span,.post_buttons #cancelButton span {
	width:74px;
}
#uploadBox .post_button_content {
	float:left;
	margin:16px 0px 0px 15px;
}
#uploadBox .post_info_area {
	float:left;
	color:#8f8f8f;
	width:469px;
	font-size:12px;
	line-height:12px;
	margin:0px 10px 0px;
}
#uploadBox .post_info_area .address {
	font-weight:bold;
	color:#000;
}
#uploadBox .post_info_area a.helpLink {
	font-size:11px;
}
#uploadBox .post_info_area a.helpIcon img {
	margin:0px 0px -1px 3px;
}
#uploadBox .faqExpandPanel {
	margin:10px 0px;
}
#uploadBox .faqExpandPanel .heading {
	font-weight:bold;
	margin-bottom:8px;
}
#uploadBox .faqExpandPanel .column {
	float:left;
	width:264px;
}
#uploadBox .faqExpandPanel .column1 {
	padding-right:20px;
}
#uploadBox .faqExpandPanel .column1 .tag {
	float:left;
	font-style:italic;
	width:75px;
}
#uploadBox .faqExpandPanel .column1 .desc {
	float:left;
	width:189px;
	margin-bottom:10px;
}
#uploadBox .faqExpandPanel .column1 .desc.last {
	margin-bottom:0px;
}
#uploadBox .faqExpandPanel .column2 {
	border-left:1px solid #e5eaeb;
	padding-left:20px;
}
#uploadBox .faqExpandPanel .column2 .item {
	margin-bottom:8px;
}
#uploadBox .faqExpandPanel .column2 .item div {
	margin-left:15px;
}
#uploadBoxHeading {
	float:left;
	width:605px;
}
#uploadBoxHeading #heading {
	float:left;
	height:25px;
}
#uploadCounter {
	float:right;
}
#uploadCounter a {
	float:left;
	display:block;
	height:25px;
	color:#888;
	font-weight:bold;
}
#uploadCounter a:hover {
	text-decoration:none;
}
#uploadCounter #uploadCharCount {
	float:left;
	font-size:20px;
	height:20px;
	padding-top:5px;
}
#uploadCounter #uploadCharCount.colorChange {
	color: #000;
}
.withPaging {
	float:left
}
.pagingBlock {
	display:inline;
	float:right;
	margin:2px 5px 0px 10px;
}
.pagingBlock a.page,.pagingBlock .currentPage {
	display:block;
	float:left;
	text-decoration:none;
	margin-left:2px;
	padding:0px 5px;
	border:1px solid #e4e4e4;
	font-family:Arial,Helvetica,sans-serif;
	font-size:14px;
	font-weight:bold;
	color:#969fa4;
	line-height:22px;
}
.pagingBlock .currentPage,.pagingBlock a.page:hover {
	background-color:#b5d6e7;
	border-color:#b5d6e7;
	color:#fff;
}
.pagingBlock a.leftArrow,.pagingBlock a.rightArrow {
	width:16px;
	padding-left:4px;
}
.pagingBlock a.leftArrow span,.pagingBlock a.rightArrow span {
	width:12px;
	padding-right:4px;
}
.pagingBlock a.leftArrow {
	margin-right:8px;
}
.pagingBlock a.rightArrow {
	margin-left:8px;
}
.pagingBlock div.spacer {
	float:left;
	width:28px;
}
.pagingBlock img {
	margin-top: 8px;
}
.viewAllMsg {
	font-size:12px;
}
.viewAllMsgHeading {
	font-size:14px;
	font-weight:bold;
	clear:left;
}
#viewEmpty .heading {
	font-weight:bold;
	font-size:16px;
	width:100%;
}
#viewEmpty .viewEmptyBox_top {
	height:5px;
	line-height:5px;
	width:596px;
	background:url(<?php echo $imageurl; ?>/box_emptyaction_top.gif) top left no-repeat;
	float:left;
}
#viewEmpty .viewEmptyBox_middle {
	background:url(<?php echo $imageurl; ?>/box_emptyaction_middle.gif) top left repeat-y;
	float:left;
	width:556px;
	padding:15px 20px 0px;
	font-size:12px;
}
#viewEmpty .viewEmptyBox_bottom {
	height:21px;
	line-height:21px;
	width:596px;
	background:url(<?php echo $imageurl; ?>/box_emptyaction_bottom.gif) top left no-repeat;
	float:left;
}
#viewEmpty .content {
	height:32px;
	line-height:32px;
	margin-right:25px;
	float:left;
}
#viewEmpty a,#viewEmpty a span {
	display:block;
	float:left;
	height:32px;
	line-height:32px;
	text-decoration:none;
	font-family:Arial,Helvetica,sans-serif;
	font-size:18px;
	color:#000;
	text-align:center;
	white-space:nowrap;
	text-transform:lowercase;
	cursor:pointer;
}
#viewEmpty a {
	background:url(<?php echo $imageurl; ?>/btn_emptyaction_left.gif) top left no-repeat;
	padding-left:3px;
	width:169px;
}
#viewEmpty a span {
	background:url(<?php echo $imageurl; ?>/btn_emptyaction_right.gif) top right no-repeat;
	padding-right:3px;
	width:166px;
}
#viewEmpty a:hover {
	background-position:0% -32px;
	text-decoration:none;
}
#viewEmpty a:hover span {
	background-position:100% -32px;
	text-decoration: none;
}
.admin {
	color:green;
	font-weight:bold;
}
.admin-viewer {
	margin: 5px 0px 0px 23px;
}
#alertBox {
	width:930px;
	margin:18px 22px 0px;
	float:left;
	display:inline;
}
#alertBox .top {
	width:930px;
	background:url(<?php echo $imageurl; ?>/alertbox_top.gif) no-repeat;
	float:left;
}
#alertBox .bottom {
	width:930px;
	height:5px;
	background:url(<?php echo $imageurl; ?>/alertbox_btm.gif) no-repeat;
	float:left;
}
#alertBox .top .leftColumn {
	float:left;
	width:596px;
	margin-right:36px;
}
#alertBox .top .profilePic {
	float:left;
	border:1px solid #fff;
	margin:5px 0px 0px 5px;
	line-height:0px;
}
#alertBox .top .content {
	float:left;
	margin:12px 0px 5px 14px;
	font-size:14px;
	line-height:18px;
	color:#305538;
	width:582px;
}
#alertBox .top .content.withProfile {
	width:500px;
}
#alertBox .top .content .heading {
	font-size:18px;
	line-height:24px;
	color:#000;
	margin-bottom:5px;
}
#alertBox .top .rightColumn {
	float:left;
	color:#fff;
	font-size:12px;
	line-height:12px;
	margin-top:12px;
}
#alertBox .top .rightColumn .yellowButton span {
	width:146px;
}
#alertBox .top .rightColumn div.text {
	float:left;
	clear:left;
	margin-top:8px;
	white-space:nowrap;
}
#alertBox .top .rightColumn div.text img {
	margin-bottom: -1px;
}
#viewerAlert {
	width:926px;
	margin:15px 23px 0px;
	float:left;
	display:inline;
	border:1px solid #f33408;
}
#viewerAlert a {
	float:right;
	margin-top:5px;
}
#viewerAlert .top {
	background:url(<?php echo $imageurl; ?>/bg_red_gradient.gif) repeat-x;
	height:43px;
	line-height:43px;
	font-size:18px;
	color:#fff;
	padding:0px 15px 0px 20px;
}
#viewerAlert .bottom {
	height:50px;
	margin: 10px 99px;
}
#pageHeader {
	background:url(<?php echo $imageurl; ?>/page_header_left.gif) no-repeat;
	width:944px;
	height:50px;
	margin-bottom:20px;
	padding:18px 0px 0px 30px;
	font-size:24px;
	position:relative;
}
#pageHeader .headerTabs {
	float:right;
	margin:5px 7px 0px 0px;
}
#pageHeader .headerTabs div.tabSelected,#pageHeader .headerTabs div.tabSelected span,#pageHeader .headerTabs a.tab,#pageHeader .headerTabs a.tab span {
	display:block;
	float:left;
	height:35px;
	line-height:35px;
	text-decoration:none;
	font-family:Arial,Helvetica,sans-serif;
	font-size:15px;
	text-align:center;
	white-space:nowrap;
}
#pageHeader .headerTabs div.tabSelected {
	background:url(<?php echo $imageurl; ?>/tab_header_selected_left.gif) top left no-repeat;
	padding-left:12px;
}
#pageHeader .headerTabs div.tabSelected span {
	background:url(<?php echo $imageurl; ?>/tab_header_selected_right.gif) top right no-repeat;
	padding-right:12px;
}
#pageHeader .headerTabs div.tabSelected,a.tab {
	margin-right:4px;
}
#pageHeader .headerTabs a.tab {
	background:url(<?php echo $imageurl; ?>/tab_header_left.gif) top left no-repeat;
	padding-left:12px;
	cursor:pointer;
	color:#626060;
}
#pageHeader .headerTabs a.tab span {
	background:url(<?php echo $imageurl; ?>/tab_header_right.gif) top right no-repeat;
	padding-right:12px;
	cursor:pointer;
}
#pageHeader .headerTabs a.tab:hover {
	background-position:0% -35px;
	text-decoration:none;
}
#pageHeader .headerTabs a.tab:hover span {
	background-position:100% -35px;
	text-decoration:none;
}
#promoPage {
	float:left;
	display:inline;
	margin:0px 38px;
	color:#8b989a;
	font-size:12px;
}
#promoPage h1,#promoPage h2,#promoPage h3 {
	font-size:30px;
	color:#f33408;
	margin:0px;
	padding:0px;
}
#promoPage h2 {
	font-size:24px;
}
#promoPage h3 {
	font-size:18px;
	margin-bottom:20px;
}
#promoPage #left {
	float:left;
	width:619px;
	margin-right:41px;
}
#promoPage .right {
	float:left;
	width:236px;
	border:1px solid #d5effb;
	background-color:#f8feff;
	margin-bottom:15px;
	padding-top:27px;
}
#promoPage .right_content {
	float:left;
	display:inline;
	margin:18px 28px 0px;
	font-size:13px;
}
#promoPage .right_content .heading {
	color:#000;
	margin-bottom:8px;
}
#promoPage .right_button {
	float:right;
	display:inline;
}
#promoPage .yellowButton {
	width:155px;
}
#promoPage a.yellowButton {
	padding-left:25px;
}
#promoPage .yellowButton span {
	width:130px;
}
#promoPage .divider {
	float:left;
	display:inline;
	height:1px;
	line-height:1px;
	background-color:#d9d9d9;
	width:898px;
	margin:50px 0px 8px;
}
#promoPage #widgetPreviews {
	float:left;
	margin-top:21px
}
#promoPage #widgetPreviews img {
	float:left;
	margin-right:14px;
}
#promoPage #widgetPreviews ul {
	float:left;
	font-size:14px;
	color:#5b5b5b;
	width:245px;
	margin:0px;
	padding:0px;
}
#promoPage #widgetPreviews li {
	margin:0px 0px 0px 23px;
	padding-top:15px;
}
#promoPage #flitterPromoArea {
	float:left;
}
#promoPage #flitterPromo {
	float:left;
	position:relative;
	margin-top:16px;
	width:619px;
}
#promoPage #flitterPromo #twitter {
	float:left;
	margin-right:21px;
}
#promoPage #flitterPromo #twitterGirl {
	float:left;
	margin-left:59px;
}
#promoPage #flitterPromo #flickr {
	float:left;
	margin-bottom:15px;
}
#promoPage #flitterPromo #info {
	position:absolute;
	bottom:0px;
	right:0px;
	font-size:14px;
	color:#5b5b5b;
}
#promoPage #widgetWelcome {
	float:left;
	width:592px;
	margin-right:25px;
}
#promoPage #widgetWelcome img.number {
	float:left;
	margin-right:15px;
}
#promoPage #widgetWelcome h1 {
	float:left;
}
#promoPage #widgetWelcome .content {
	float:left;
	margin:15px 0px 20px;
	width:100%;
	line-height:16px;
}
#promoPage .howtoRight {
	display:inline;
	width:231px;
	float:left;
	padding:18px;
	background-color:#f8f8f8;
	height:300px;
	position:relative;
}
#promoPage .howto h3 {
	display:inline;
}
#promoPage .howto ol {
	margin-bottom:25px;
}
#promoPage .howto ol.headlines li {
	font-size:22px;
	color:#282E31;
	margin-bottom:15px;
}
#promoPage .howto ol li {
	font-size:14px;
	margin: 0px 0px 10px 0px;
}
#promoPage #leftConnect {
	float:left;
}
#promoPage .connectPage {
	margin-top:10px;
	height:103px;
}
#promoPage .connectPage p {
	color:#000;
	margin:5px 0px 5px;
	height:25px;
}
#leftConnect .row {
	width:100%;
	clear:both;
}
#leftConnect .row .imageArea {
	display:inline;
	float:left;
	width:161px;
	margin-right:20px;
}
#leftConnect .row .description {
	display:inline;
	float:left;
	width:520px;
	margin-right:15px;
}
#leftConnect .row p {
	margin-top:5px;
	font-size:14px;
	width:375px;
	line-height:18px;
}
#leftConnect .row .right_button {
	display:inline;
	float:right;
}
#leftConnect .line {
	border-bottom:1px solid #ccc;
	font-size:0px;
	margin:15px 0px 15px 0px;
}
#leftConnect h3 {
	margin-bottom: 10px;
}
#promoPage #columnLeft {
	width:571px;
	float:left;
	display:inline;
	margin-right:37px;
}
#promoPage #columnLeft h3 {
	margin:0;
}
#promoPage #columnLeft .feature {
	margin:18px 0px 0px 0px;
}
#promoPage #columnLeft .feature h4 {
	color:#000;
	font-size:14px;
	margin:0px 0px 2px 0px;
}
#promoPage #columnLeft .feature p {
	line-height:16px;
	font-size:12px;
	color:#888;
	margin:4px 0px 0px 0px;
}
#promoPage #columnLeft .feature ul {
	margin:4px 0px 0px 0px;
	padding:0px;
}
#promoPage #columnLeft .feature ul li {
	margin:0px 0px 4px 12px;
}
#promoPage #columnLeft .feature ol {
	margin:4px 0px 0px 0px;
	padding:12px 0px 12px 0px;
	background-color:#f0f0f0;
}
#promoPage #columnLeft .feature ol li {
	margin:0px 12px 4px 36px;
}
#promoPage #columnLeft .feature .specialText {
	font-family:courier;
}
#promoPage #sidebar {
	width:290px;
	margin:0;
	padding:0;
	float:left;
	display:inline;
}
#promoPage #sidebar .yellowButton {
	width:290px;
}
#promoPage #sidebar .yellowButton span {
	width: 250px;
}
#widget {
	float:left;
	margin:0px 30px 0px 40px;
}
#widget h2 {
	color:#f33408;
	font-size:18px;
	line-height:18px;
	margin:0px;
	padding:0px;
}
#widget .divider {
	height:1px;
	line-height:1px;
	background-color:#d9d9d9;
	width:100%;
	margin:5px 0px 15px;
}
#widget #settingsPane {
	width:260px;
	margin-right:32px;
	float:left;
	display:inline;
}
#widget #previewPane {
	width:610px;
	float:left;
	display:inline;
}
#widget input {
	font-size:14px;
	margin-right:4px;
}
#widget .coloroption {
	width:260xp;
	height:21px;
	line-height:21px;
	color:#8b989a;
	font-size:12px;
	margin-bottom:5px;
	clear:both;
}
#widget .colorhash {
	float:right;
	display:inline;
	margin-right:5px;
}
#widget .colorfield {
	float:right;
	display:inline;
}
#widget .colorswatch {
	float:right;
	display:inline;
	margin-right:20px;
	width:21px;
	height:21px;
}
#widget .subheading {
	font-size:14px;
}
#widget #color1Spacer,#widget #color2Spacer,#widget #color3Spacer {
	width:136px;
	height:136px;
}
#widget #color3Spacer {
	height:100px;
}
#widget #sizeError,#widget #sizeWarning,#widget #minSizeWarning {
	margin-top:15px;
}
#widget #sizeError2,#widget #sizeWarning2,#widget #minSizeWarning2 {
	float:left;
	margin: 7px 0px 0px 20px;
}
#scritterFeeds {
	float:left;
	font-size:12px;
	margin:0px 10px 25px;
}
#scritterFeeds .divider {
	float:left;
	margin:20px 0px 15px;
	height:1px;
	line-height:1px;
	width:340px;
	background-color:#d9d9d9;
}
#scritterFeeds .feed {
	float:left;
}
#scritterFeeds .feed .feedItem {
	float:left;
	width:100%;
	margin-top:10px;
}
#scritterFeeds .feed .feedItem img {
	float:left;
}
#scritterFeeds .button {
	float:right;
	margin-top:3px;
}
#scritterFeeds a.standardButton {
	width:125px;
}
#scritterFeeds a.standardButton span {
	width: 115px;
}
#commentReplyErrors {
	display:hidden;
	padding-bottom:10px;
	color:#8F8F8F;
	font-style:italic;
}
#commentReplyMain {
	padding-bottom:10px;
}
#commentReplyMain textarea {
	width: 357px;
}
#inbox {
	margin:0px 0px 0px 22px;
	border-bottom:4px solid #e6e6e6;
	float:left;
	padding-bottom:12px;
	display:inline;
}
#inboxEmpty {
	margin:0px 0px 0px 22px;
	float:left;
	padding-bottom:12px;
	display:inline;
	width:766px;
	height:600px;
}
#inbox a.action {
	position:absolute;
	top:9px;
	right:14px;
	z-index:10;
}
#sortby {
	background:url(<?php echo $imageurl; ?>/sortby_banner.gif) no-repeat;
	margin:10px 0px 12px 0px;
	width:776px;
	height:25px;
	position:relative;
	float:left;
	color:#8b989A;
}
#sortby p {
	text-indent:24px;
	line-height:24px;
	font-size:12px;
	margin:0px;
}
#sortby p span,#preview li span {
	color:#8b989A;
}
#sortby p span.sortLabel {
	color:#000;
}
#sortby .sorter a {
	position:absolute;
	top:7px;
	left:9px;
	width:9px;
	height:10px;
}
a.arrowUp {
	background:url(<?php echo $imageurl; ?>/sortup.gif) no-repeat;
	width:9px;
	height:10px;
}
a.arrowUp:hover {
	background:url(<?php echo $imageurl; ?>/sortup_on.gif) no-repeat;
	width:9px;
	height:10px;
}
a.arrowDn {
	background:url(<?php echo $imageurl; ?>/sortdown.gif) no-repeat;
	width:9px;
	height:10px;
}
a.arrowDn:hover {
	background:url(<?php echo $imageurl; ?>/sortdown_on.gif) no-repeat;
	width:9px;
	height:10px;
}
#preview {
	width:264px;
	float:left;
}
#preview ul {
	width:264px;
	margin:0px;
	padding:0px;
	list-style-type:none;
}
#preview li {
	background:url(<?php echo $imageurl; ?>/inboxbrick.gif) no-repeat;
	margin:-1px 0px 0px 0px;
	float:left;
	position:relative;
	width:264px;
	height:50px;
}
#preview li a.msg {
	width:264px;
	height:50px;
	position:absolute;
	top:-1px;
	left:0px;
	z-index:1;
	outline:none;
}
#preview li .bricktop {
	background:url(<?php echo $imageurl; ?>/inboxbrick_top.gif) no-repeat;
	width:264px;
	height:4px;
	position:absolute;
	top:0px;
	left:0px;
}
#preview li .brickbtm {
	width:264px;
	height:3px;
	position:absolute;
	bottom:0px;
	left:0px;
}
#preview li.selected {
	background:url(<?php echo $imageurl; ?>/inboxbrick_sel.gif) no-repeat;
	margin:-1px 0px 0px 0px;
	width:264px;
	height:500px;
	position:relative;
	height:50px;
}
#preview li.selected .bricktop {
	background:url(<?php echo $imageurl; ?>/inboxbrick_top_sel.gif) no-repeat;
	width:264px;
	height:4px;
	position:absolute;
	top:0px;
	left:0px;
}
#preview li.selected .brickbtm {
	background:none;
}
#preview li .byline {
	color:#8b989a;
	font-size:11px;
	position:absolute;
	top:6px;
	left:28px;
	z-index:2;
}
#preview li .status {
	position:absolute;
	top:6px;
	left:6px;
}
#preview li .read {
	background:url(<?php echo $imageurl; ?>/inbox_read.gif) no-repeat;
	width:15px;
	height:11px;
}
#preview li .unread {
	background:url(<?php echo $imageurl; ?>/inbox_unread.gif) no-repeat;
	width:15px;
	height:11px;
}
#preview li .readMedia {
	background:url(<?php echo $imageurl; ?>/inbox_read_media.gif) no-repeat;
	width:15px;
	height:11px;
}
#preview li .unreadMedia {
	background:url(<?php echo $imageurl; ?>/inbox_unread_media.gif) no-repeat;
	width:15px;
	height:11px;
}
#preview li .msgPreview {
	color:#000;
	font:14px trebuchet ms;
	position:absolute;
	top:25px;
	left:6px;
	z-index:0;
}
#preview li .msgPreviewPre {
	color:#000;
}
#preview li .msgPreviewTrunc {
	color:#000;
	white-space:nowrap;
}
#preview li .msgPreviewTruncMedia {
	color:#8b989A;
	white-space:nowrap;
}
#preview li .inboxChk {
	position:absolute;
	right:10px;
	top:3px;
	z-index:3;
}
#preview p {
	border-bottom:1px solid #e6e6e6;
	width:258px;
	color:#6f6f6f;
	font-size:13px;
	float:left;
	margin:7px 0px;
	line-height:20px;
}
#preview p span {
	text-align:right;
	float:right;
}
#thread {
	float:left;
	width:502px;
	background:url(<?php echo $imageurl; ?>/inbox_threadtop.gif) no-repeat;
	margin:0px 0px 0px 10px;
	position:relative;
}
#threadTitle {
	margin-left:23px;
}
#thread h2 {
	margin:0px 15px 0px 0px;
	padding-top:15px;
	height:31px;
	font-size:18px;
}
#thread .msgUser {
	margin:8px 8px 0px 0px;
	display:inline;
	float:left;
}
#thread .tinyUser {
	display:inline;
}
#threadBody {
	margin-left:7px;
	border:1px solid #efda9b;
	float:left;
	width:493px;
	height:501px;
	display:inline;
}
#threadBody span {
	color:#8b989a;
}
#threadBody .whatsNewIcon {
	vertical-align:middle;
	padding-left:5px;
}
#threadBody a span {
	color:#0e6db7;
}
#threadBody .headerPreviewSpan {
	color:#000;
	white-space:nowrap;
}
#threadBody .inviteButtons {
	padding-top:15px;
}
#threadSubject {
	margin:-1px 0px 0px 0px;
	float:left;
	width:493px;
	border-bottom:1px solid #efda9c;
	background-color:#fffec8;
	height:46px;
	position:relative;
}
#threadSubject .content {
	padding-top:8px;
	background:url(<?php echo $imageurl; ?>/inbox_threadheadbdr.gif) repeat-x;
	font:12px trebuchet ms;
	float:left;
	width:460px;
	position:absolute;
	top:0px;
	left:15px;
}
#threadSubject .re {
	display:inline;
	float:left;
	margin:0px 6px 0px 0px;
	line-height:15px;
}
#threadSubject .re span.byline {
	font:11px arial;
}
#threadBody #sendMsg {
	height:66px;
	border-bottom:1px solid #e5e5e5;
	position:relative;
	float:left;
	width:493px;
}
#threadBody a.postBtn {
	background:url(<?php echo $imageurl; ?>/inbox_postbtn.gif) no-repeat;
	display:block;
	position:absolute;
	top:9px;
	left:14px;
	padding:20px 8px 6px 10px;
	text-align:center;
	line-height:11px;
}
#threadBody .msgBox {
	display:block;
	position:absolute;
	top:9px;
	left:6px;
	width:415px;
	height:42px;
	border:2px solid #e5e5e5;
	font:16px trebuchet ms;
	margin:0px;
	padding:2px 0px 0px 5px;
	line-height:18px;
}
#threadBody ul {
	width:479px;
	list-style-type:none;
	margin:0px 0px 0px 14px;
	padding:0px;
	float:left;
	height:388px;
	overflow:auto;
	display:inline;
	position:relative;
}
#threadBody li.first {
	border:none;
	margin-top:0px;
}
#threadBody .brick_preview {
	background-color:#fff;
	width:auto;
	height:auto;
	line-height:0px;
}
#threadBody li.moreMsgs {
	background:url(<?php echo $imageurl; ?>/arrow_down.gif) no-repeat 217px 0px;
	padding:18px 0px 12px;
	text-align:center;
}
#threadBody li.moreMsgs a {
	margin-left:7px;
}
#threadBody ul li {
	border-top:1px solid #eaeaea;
	padding:17px 0px;
	width:452px;
	clear:left;
	margin:0px;
	float:left;
}
#threadBody li p {
	font:14px trebuchet ms;
	margin:7px 0px 0px 0px;
}
#loadingConversations {
	font:14px arial;
	color:#64abf9;
	text-align:center;
	position:relative;
	top:284px;
	width:100%;
	height:60px;
	text-transform:lowercase;
}
#loadingConversations progress {
	left:356px;
	width:64px;
	height:60px;
}
#loadingConversations p {
	display:block;
	margin-left:auto;
	margin-right:auto
}
#home {
	float:left;
	background:url(<?php echo $imageurl; ?>/bg_transparent2.png) no-repeat;
	width:884px;
	padding-left:45px;
    padding-right:45px;
    padding-bottom:45px;
    padding-top:15px;
	color:#a0a5a7;
	font-size:12px;
	line-height:16px;
}
#home h2 {
	font:20px Trebuchet MS;
	color:#f33408;
	margin:0px;
}
#home h3 {
	font:18px Arial,Helvetica,sans-serif,serif;
	color:#f33408;
	margin:0px;
}
#home #main {
	float:left;
	width:533px;
}
#home #illustration {
	float:left;
	display:inline;
	width:533px;
	height:302px;
	margin-bottom:25px;
}
#home #quotes {
	float:left;
	font-size:16px;
	color:#56666b;
}
#home #quotes div {
	float:left;
	line-height:30px;
	margin-bottom:10px;
	width:100%;
}
#home #quotes div img {
	float:left;
	margin-right:10px;
}
#home #sidebar {
	float:left;
	margin-left:33px;
	width:318px;
}
#home #sidebar h1 {
	font-size:28px;
	line-height:33px;
	color:#000;
	margin:-3px 0px 0px;
}
#home #sidebar a.underline {
	text-decoration:underline;
	cursor:pointer;
}
#home #sidebar .box {
	float:left;
	width:318px;
	margin-top:20px;
}
#home #sidebar .box .top {
	float:left;
	background:url(<?php echo $imageurl; ?>/greenbox_top.gif) no-repeat;
	width:288px;
	padding:15px 15px 0px;
	font-size:16px;
	line-height:20px;
	color:#305538;
}
#home #sidebar .box .bottom {
	float:left;
	background:url(<?php echo $imageurl; ?>/greenbox_btm.gif) no-repeat;
	width:318px;
	height:15px;
}
#home #sidebar .box .top .logo {
	margin-bottom:-2px;
}
#home #sidebar .box .legalText {
	float:left;
	font-size:11px;
	line-height:14px;
	color:#427733;
	width:190px;
	margin:2px 0px 0px 8px;
}
#home #sidebar .box input {
	float:left;
	width:210px;
	height:18px;
	padding:2px 0px 2px 4px;
	margin-bottom:5px;
	font-family:Trebuchet MS;
	font-size:14px;
	color:#000;
	border:1px solid #a0a5A7;
}
#home #sidebar .box input.default {
	font-family:Arial,Helvetica,sans-serif;
	font-style:italic;
	color:#b0b4b6;
}
#home #sidebar .box input.login {
	width:135px;
}
#home #sidebar .box select {
	float:left;
	margin-right:5px;
	border:1px solid #a0a5A7;
	font-family:Trebuchet MS;
	font-size:14px;
}
#home #sidebar .box .error {
	float:left;
	font-size:12px;
	line-height:14px;
	margin-bottom:2px;
	width:100%;
}
#home #sidebar #loginBox,#home #sidebar #joinBox {
	float:left;
	margin-bottom:5px;
	width:318px;
}
#home #sidebar #loginForm {
	float:left;
	margin:15px 0px 11px;
}
#home #sidebar .forgotLogin {
	float:left;
	font-size:11px;
	margin:5px 0px 0px 3px;
	width:300px;
}
#home #sidebar #joinForm {
	float:left;
	margin:10px 0px 15px;
}
#home #sidebar #joinForm #usernameStatus {
	display:block;
	float:left;
	font-size:11px;
	font-style:italic;
	margin-left:6px;
}
#home #sidebar #joinForm #usernameStatus.unavailable {
	color:#ff8a00;
}
#home #sidebar #joinForm .birthdayLabel {
	float:left;
	font-size:11px;
	width:288px;
	margin-left:2px;
}
#home #sidebar #joinForm .birthdayFields {
	float:left;
	width:290px;
}
#home #sidebar #loginForm input.hiddenSubmit,#home #sidebar #joinForm input.hiddenSubmit {
	width:0px;
	height:0px;
	padding:0px;
	margin:0px;
	border:none;
	position:absolute;
	bottom:0px;
	left:0px;
}
#home #sidebar .promo {
	float:left;
	clear:left;
	font-size:16px;
	margin-top:15px;
	width:318px;
}
#home #sidebar .promo img {
	float:left;
	margin-right:10px;
}
#home #sidebar .promo a {
	display:block;
	float:left;
	line-height:32px;
}
#home #sidebar .promoExpand {
	float:left;
	clear:left;
	width:318px;
}
#home #sidebar .promoExpand ul {
	margin:0px;
	padding-left:58px;
}
#home #sidebar .promoExpand li {
	font-size:12px;
	line-height:24px;
	color:#56666b;
}
#home #sidebar .promoExpand li span {
	font-size:16px;
}
#announcement {
	background-color:#ffdaa4;
	border-bottom:1px solid #f6c47b;
	width:952px;
	height:33px;
	line-height:33px;
	padding-left:22px;
	font-size:12px;
	color:#696969;
}
#announcement .label {
	float:left;
	margin:7px 10px 0px 0px;
}
#announcement .content {
	float:left;
	height:33px;
	line-height:33px;
}
#announcement a {
	font-size:11px;
}
#callToActions {
	margin:15px 0px 0px 22px;
}
#callToActions .actionBrick {
	background:url(<?php echo $imageurl; ?>/brick_cta_bg.gif) repeat-y;
	width:595px;
	float:left;
	margin-bottom:8px;
}
#callToActions .actionBrick .top {
	background:url(<?php echo $imageurl; ?>/brick_cta_top.gif) no-repeat;
	width:525px;
	height:16px;
	line-height:16px;
	font-size:16px;
	float:left;
	padding:18px 0px 0px 70px;
	margin:0px;
}
#callToActions .actionBrick .bottom {
	background:url(<?php echo $imageurl; ?>/brick_cta_btm.gif) no-repeat;
	width:595px;
	height:11px;
	line-height:11px;
	float:left;
}
#callToActions .actionBrick .body {
	float:left;
	padding:15px 0px 0px 70px;
	color:#559db5;
	font-size:18px;
	line-height:18px;
	width:525px;
}
#callToActions .actionBrick .body div.item {
	float:left;
	width:520px;
	margin-bottom:20px;
}
#callToActions .actionBrick .body div.item a {
	font-weight:bold;
}
#callToActions .actionBrick .body div.item a img {
	margin:-5px 0px -7px 5px;
}
#callToActions .actionBrick .body div.item a.last {
	margin-bottom:15px;
}
#callToActions .actionBrick .body div.item a.strike {
	color:#9badb0;
	text-decoration:line-through;
}
#callToActions .actionBrick .subHeading {
	margin-bottom:10px;
}
#callToActions ul#actionBrickPromo {
	margin:0px;
	padding:0px;
}
#callToActions ul#actionBrickPromo li {
	margin:0px 10px 10px 0px;
	width:252px;
}
#callToActions ul#actionBrickPromo .brick_title {
	font-size:12px;
	line-height:15px;
	margin-top:3px;
}
#callToActions ul#actionBrickPromo .brick_title a {
	font-size:12px;
	line-height:15px;
	margin-top:3px;
}
#callToActions ul#actionBrickPromo .brick_preview {
	width:50px;
	height:50px;
	background-color:#fff;
	padding:1px;
	margin-right:7px;
}
#callToActions ul#actionBrickPromo .brick_byline {
	color:#90afc1;
}
a.tooltip {
	position:relative;
	z-index:2002;
	outline:none;
	text-decoration:none;
}
a.tooltip div.tt,a.tooltip div.top,a.tooltip div.middle,a.tooltip div.bottom {
	display:none;
}
a.tooltip:hover div.tt {
	display:block;
	position:absolute;
	top:26px;
	right:-110px;
	width:254px;
	z-index:2003;
}
a.tooltip:hover div.top {
	display:block;
	height:20px;
	line-height:0px;
	background:url(<?php echo $imageurl; ?>/tooltip_top.png) no-repeat top;
}
a.tooltip:hover div.middle {
	display:block;
	padding:0px 13px 5px;
	margin-top:-2px;
	background:url(<?php echo $imageurl; ?>/tooltip_bg.png) repeat bottom;
	color:#000;
	font-weight:normal;
	font-size:12px;
	line-height:16px;
	text-align:left;
	white-space:normal;
}
a.tooltip:hover div.bottom {
	display:block;
	height:10px;
	line-height:0px;
	background:url(<?php echo $imageurl; ?>/tooltip_btm.png) no-repeat bottom;
}
a.tooltip.medium:hover div.tt {
	top:16px;
	right:-92px;
	width:204px;
}
a.tooltip.medium:hover div.top {
	background-image:url(<?php echo $imageurl; ?>/tooltip_top_med.png);
}
a.tooltip.medium:hover div.middle {
	background-image:url(<?php echo $imageurl; ?>/tooltip_bg_med.png);
	padding-bottom:2px;
	margin-top:-5px;
}
a.tooltip.medium:hover div.bottom {
	background-image:url(<?php echo $imageurl; ?>/tooltip_btm_med.png);
}
img.locationIcon {
	margin-bottom:-2px;
}
a.locationTooltip .note {
	margin-top:5px;
	color:#dfb145;
	font-size:11px;
	line-height:11px;
}
div.actionsPopupContainer {
	position:relative;
	float:left;
}
div.actionsPopup {
	float:left;
	width:250px;
	position:absolute;
	top:-7px;
	left:-254px;
}
div.actionsPopup .top {
	float:left;
	background:url(<?php echo $imageurl; ?>/popup_actions_top.gif) no-repeat;
	width:250px;
	height:19px;
}
div.actionsPopup .body {
	float:left;
	border:1px solid #353535;
	border-top:none;
	background-color:#fff;
	padding-bottom:6px;
	width:243px;
}
div.actionsPopup .body .content {
	float:left;
	text-align:left;
	margin-top:-10px;
}
div.actionsPopup .body .content a {
	float:left;
	display:block;
	margin:0px 8px;
	color:#036db7;
}
div.actionsPopup .body .content a.disabled {
	color:#8b989a;
	font-style:italic;
}
div.actionsPopup .body .content div.line {
	float:left;
	display:block;
	background-color:#dadad9;
	width:243px;
	height:1px;
	line-height:1px;
	margin:6px 0px;
}
.popupDisplayed {
	z-index:2;
}
.displayBehind {
	z-index: 0;
}
#followContributorsPopup {
	float:left;
	font-size:12px;
	color:#888;
}
#followContributorsPopup .option {
	float:left;
	clear:both;
}
#followContributorsPopup input[type=checkbox] {
	float:left;
	margin:0px 7px 6px 0px;
}
#followContributorsPopup label {
	float:left;
	margin-top:1px;
	color:#000;
}
#followContributorsPopup label.disabled {
	font-style:italic;
	color:#a9a9a9;
}
.topicSidebar .followAll {
	float:left;
	clear:left;
	margin:0px 0px 12px 8px;
}
.topicSidebar .contributorsArea {
	float:left;
	clear:left;
	display:inline;
	padding:0px 0px 8px 8px;
}
.topicSidebar .contributor {
	float:left;
	clear:left;
	margin-top:7px;
}
#topicHeading {
	float:left;
	position:relative;
}
#topicHeading h2 {
	float:left;
	margin-bottom:0px;
}
#topicHeading a#refreshOn,#topicHeading a#refreshOff {
	display:block;
	float:left;
	width:13px;
	height:13px;
	margin:7px 0px 0px 8px;
}
#topicHeading a#refreshOn {
	background:url(<?php echo $imageurl; ?>/icon_refresh_on2.gif) no-repeat top left;
}
#topicHeading a#refreshOff {
	background:url(<?php echo $imageurl; ?>/icon_refresh_off2.gif) no-repeat top left;
}
#topicHeading a#refreshOn:hover,#topicHeading a#refreshOff:hover {
	background-position:100% -13px;
}
#topicHeading .tabLine {
	float:left;
	height:27px;
	border-bottom:1px solid #cbd2d7;
}
#topicHeading .tabLine.line1 {
	position:absolute;
	bottom:0px;
	left:22px;
	width:134px;
}
#topicHeading .tabLine.line2 {
	width:3px;
}
#topicHeading .tabLine.line3 {
	width: 321px;
}
#topicHeading .tab {
	float:left;
	font-size:14px;
	height:14px;
	line-height:14px;
	text-align:center;
	border:1px solid #cbd2d7;
	padding:5px 8px;
	margin-top:2px;
}
#topicHeading .tab.first {
	margin-left:10px;
	width:46px;
}
#topicHeading .tab.first a#refreshOn,#topicHeading .tab.first a#refreshOff {
	margin:0px 0px 0px 4px
}
#topicHeading .tab.first .disabled {
	margin-left:4px;
}
#topicHeading .tab.second {
	width: 57px;
}
#topicHeading .tab.selected {
	border-bottom-color:#fff;
}
#topicHeading .tab a {
	display:block;
	float:left;
}
#topicHeading #mapHelp {
	float:right;
	margin:11px 2px 0px 0px;
}
#topicContents {
	float:left;
	width:620px;
}
#topicContents img.loading {
	margin:50px 0px 0px 305px;
}
#topicContents.noTabs .pagingArrows.top {
	margin-top:-15px;
}
#topicMapContainer {
	float:left;
	margin-top:10px;
	margin-left:22px;
}
#topicMapContainer #topicMapDescription {
	float:left;
	width:586px;
	border:1px solid #ccc;
	background-color:#eee;
	color:#000;
	font-size:12px;
	line-height:16px;
	padding:5px;
	margin-bottom:6px;
}
#topicMapContainer #mapLoading {
	position:absolute;
	top:0px;
	left:268px;
	width:62px;
	z-index:1;
	background-color:#fffa73;
	padding:5px 0px;
	font-weight:bold;
	text-align:center;
}
#topicMapContainer #topicMap {
	float:left;
	border:1px solid #ccc;
	width:596px;
	height:596px;
}
#topicMapContainer #topicMap .infoWindow .nav .info {
	float:left;
	text-align:center;
	color:#6f6f6f;
}
#topicMapContainer #topicMap .infoWindow .nav .arrow {
	float:left;
	width:9px;
}
#topicMapContainer #topicMap .infoWindow .nav a.newer {
	float:left;
	background:url(<?php echo $imageurl; ?>/arrow_paging_left2.gif) no-repeat top left;
	width:9px;
	height:12px;
}
#topicMapContainer #topicMap .infoWindow .nav a.newer:hover {
	background-position:100% -12px;
}
#topicMapContainer #topicMap .infoWindow .nav a.older {
	float:right;
	background:url(<?php echo $imageurl; ?>/arrow_paging_right2.gif) no-repeat top right;
	width:9px;
	height:12px
}
#topicMapContainer #topicMap .infoWindow .nav a.older:hover {
	background-position: 100% -12px;
}
#lbPageHeader {
	margin:0px 0px 10px 0px;
	width:593px;
	float:left;
	display:inline;
	line-height:20px;
}
#lbStamp {
	position:absolute;
	right:8px;
	top:5px;
	color:#a9a9a9;
}
#lbContents {
	margin-left:22px;
	width:593px;
	clear:both;
}
#lbFootnote {
	margin:22px 0px 0px 22px;
	width:593px;
	clear:both;
}
#lbHeader {
	height:26px;
}
#lbHeader .headerText {
	font-family:Arial,Helvetica,sans-serif;
	line-height:12px;
	font-size:12px;
	font-weight:bold;
	color:#fff;
	margin:8px 0px 0px 0px;
}
#lbRank {
	float:left;
	display:inline;
	width:47px;
	height:26px;
	background:url( <?php echo $imageurl; ?>/header_left.gif) no-repeat top left;
	text-align:center;
}
#lbRank .headerText {
}
#lbUser {
	float:left;
	display:inline;
	width:420px;
	height:26px;
	background:url( <?php echo $imageurl; ?>/header_middle.gif) no-repeat top left;
}
#lbUser .headerText {
	margin-left:6px;
}
#lbFollowers {
	float:left;
	display:inline;
	width:126px;
	height:26px;
	background:url( <?php echo $imageurl; ?>/header_right.gif) no-repeat top;
	left;
}
#lbFollowers .headerText {
	float:right;
	display:inline;
	margin-right:8px;
}
#moreLeaders {
	float:left;
	display:inline;
	margin:22px 0px 0px 90px;
	text-align:center;
	cursor:pointer;
}
#moreLeaders .moreText {
	cursor:pointer;
}
.lbRow {
	width:100%;
}
.lbRowGold {
	font-family:Arial,Helvetica,sans-serif;
	color:#000;
	height:92px;
	background:url( <?php echo $imageurl; ?>/gold_gradient.gif) repeat-x;
}
.lbRow .userContainer {
	margin:6px;
}
.lbRowPlain .userContainer {
	margin:2px 6px 6px 6px;
}
.lbRow .user {
	margin-top:0px;
}
.lbRow .user a {
	font-size:18px;
	color:#0e6db7;
}
.lbRowPlain .user {
	line-height:12px;
	float:left;
	display:inline;
}
.lbRowPlain .user a {
	font-size:12px;
	color:#0e6db7;
}
.lbRow .user a hover {
	text-decoration:underline;
}
.lbRow .motto {
	white-space:nowrap;
	font-size:14px;
	font-family:Trebuchet MS;
	line-height:20px;
}
.lbRowPlain .motto {
	white-space:nowrap;
	margin-left:3px;
	line-height:12px;
	float:left;
	display:inline;
	font-size:11px;
	font-family:Trebuchet MS;
	color:#000;
}
.lbRow .updatesCount {
	font-size:12px;
	color:#8B989A;
	position:absolute;
	bottom:0px;
}
.lbRow .followersCount {
	float:right;
	display:inline;
	margin:10px 10px 0px 0px;
	font-size:16px;
	text-align:center;
}
.lbRowGold .profilePicPreview {
	width:75px;
	height:75px;
	margin:0px 12px 0px 0px;
	padding:1px;
	text-align:center;
	display:inline;
	float:left;
	border:solid 1px #cbd2d7;
	position:relative;
}
.lbRowGold .userInfo {
	float:left;
	display:inline;
	height:72px;
	position:relative;
	width:300px;
}
.lbRowSilver .profilePicPreview {
	width:50px;
	height:50px;
	margin:0px 12px 0px 0px;
	padding:1px;
	text-align:center;
	display:inline;
	float:left;
	border:solid 1px #cbd2d7;
	position:relative;
}
.lbRowSilver .userInfo {
	float:left;
	display:inline;
	height:55px;
	position:relative;
	width:300px;
}
.lbRowBronze .profilePicPreview {
	width:50px;
	height:50px;
	margin:0px 12px 0px 0px;
	padding:1px;
	text-align:center;
	display:inline;
	float:left;
	border:solid 1px #cbd2d7;
	position:relative;
}
.lbRowBronze .userInfo {
	float:left;
	display:inline;
	height:55px;
	position:relative;
	width:300px;
}
.lbRowPlain .profilePicPreview {
	width:31px;
	height:31px;
	margin:0px 12px 0px 0px;
	padding:1px;
	text-align:center;
	display:inline;
	float:left;
	border:solid 1px #cbd2d7;
	position:relative;
}
.lbRowPlain .userInfo {
	margin-top:12px;
	float:left;
	display:inline;
	height:20px;
	position:relative;
	line-height:16px;
	text-align:center;
	width:300px;
}
.lbRowSilver {
	height:67px;
	background:url( <?php echo $imageurl; ?>/silver_gradient.gif) repeat-x;
}
.lbRowBronze {
	height:67px;
	background:url( <?php echo $imageurl; ?>/bronze_gradient.gif) repeat-x;
}
.lbRowPlain {
	height:39px;
	background:url( <?php echo $imageurl; ?>/plain_gradient.gif) repeat-x;
}
.lbRowPlain .followersCount {
	float:right;
	display:inline;
	margin:12px 10px 0px 0px;
	font-size:12px;
	text-align:center;
}
.lbRowGold .ribbon {
	margin:6px 0px 0px 6px;
	width:36px;
	height:45px;
	background:url(<?php echo $imageurl; ?>/first.gif) no-repeat top left;
	z-index:2;
}
.lbRowSilver .ribbon {
	margin:6px 0px 0px 6px;
	width:36px;
	height:46px;
	background:url(<?php echo $imageurl; ?>/second.gif) no-repeat top left;
	z-index:2;
}
.lbRowBronze .ribbon {
	margin:6px 0px 0px 6px;
	width:36px;
	height:46px;
	background:url(<?php echo $imageurl; ?>/third.gif) no-repeat top left;
	z-index:2;
}
.lbRowPlain .ribbon {
	font-size:12px;
	font-family:Arial,Helvetica,sans-serif;
	color:#000;
	padding-top:12px;
	text-align:center;
}
.lbColRank {
	float:left;
	display:inline;
	width:46px;
	border-color:#bdbdbd;
	border-style:solid;
	border-width:1px;
	border-top:none;
	border-right:none;
}
.lbColUser {
	float:left;
	display:inline;
	width:418px;
	border-color:#bdbdbd;
	border-style:solid;
	border-width:1px;
	border-top:none;
	border-right:none;
}
.lbColFollow {
	float:left;
	display:inline;
	width:125px;
	border-color:#bdbdbd;
	border-style:solid;
	border-width:1px;
	border-top:none;
}
#leaderboardArea {
	width:287px;
	float:left;
	clear:left;
	display:inline;
	padding:0px 0px 5px 8px;
}
#leaderboardArea .lbRow {
	height:42px;
	overflow:hidden;
	width:100%;
	border-color:#bdbdbd;
	border-style:solid;
	border-width:1px;
	margin-bottom:3px;
}
#leaderboardArea .lbRowGold {
	height:61px;
	background:url( <?php echo $imageurl; ?>/gold_gradient.gif) repeat-x;
}
#leaderboardArea .lbRowSilver {
	background:url( <?php echo $imageurl; ?>/silver_gradient.gif) repeat-x;
}
#leaderboardArea .lbRowBronze {
	background:url( <?php echo $imageurl; ?>/bronze_gradient.gif) repeat-x;
}
#leaderboardArea .lbRow .userContainer {
	margin:3px;
}
#leaderboardArea .lbRow .profilePicPreview {
	width:31px;
	height:31px;
	margin:0px 8px 0px 0px;
	padding:1px;
	text-align:center;
	display:inline;
	float:left;
	border:solid 1px #cbd2d7;
	position:relative;
}
#leaderboardArea .lbRowGold .profilePicPreview {
	width:50px;
	height:50px;
	margin:0px 8px 0px 0px;
	padding:1px;
	text-align:center;
	display:inline;
	float:left;
	border:solid 1px #cbd2d7;
	position:relative;
}
#leaderboardArea .ribbon {
	margin:3px 0px 0px 0px;
	float:right;
	display:inline;
}
#leaderboardArea .lbRowGold .ribbon {
	width:29px;
	height:36px;
	background:url(<?php echo $imageurl; ?>/first_mini.gif) no-repeat top left;
	z-index:2;
}
#leaderboardArea .lbRowSilver .ribbon {
	width:29px;
	height:36px;
	background:url(<?php echo $imageurl; ?>/second_mini.gif) no-repeat top left;
	z-index:2;
}
#leaderboardArea .lbRowBronze .ribbon {
	width:29px;
	height:36px;
	background:url(<?php echo $imageurl; ?>/third_mini.gif) no-repeat top left;
	z-index:2;
}
#leaderboardArea .lbRow .userInfo {
	width:150px;
	margin:3px 0px 0px 0px;
	float:left;
	display:inline;
	height:35px;
	position:relative;
}
#leaderboardArea .lbRow .lbRowGold .userInfo {
	height:50px;
}
#leaderboardDescription {
	color:#8B989A;
	font-size:12px;
	line-height:16px;
	width:287px;
	float:left;
	clear:left;
	display:inline;
	padding:0px 0px 8px 8px;
}
a.leaderBadge,a.leaderBadge span {
	display:block;
	float:left;
	height:40px;
	line-height:40px;
	text-decoration:none;
	font-family:Arial,Helvetica,sans-serif;
	font-size:12px;
	font-weight:bold;
	color:#000;
	text-align:center;
	white-space:nowrap;
	cursor:pointer;
}
a.leaderBadge_first {
	background:url(<?php echo $imageurl; ?>/web_first_l.gif) top left no-repeat;
	padding-left:40px;
}
a.leaderBadge_first span {
	background:url(<?php echo $imageurl; ?>/web_first_r.gif) top right no-repeat;
	padding-right:15px;
}
a.leaderBadge_second {
	background:url(<?php echo $imageurl; ?>/web_second_l.gif) top left no-repeat;
	padding-left:40px;
}
a.leaderBadge_second span {
	background:url(<?php echo $imageurl; ?>/web_second_r.gif) top right no-repeat;
	padding-right:15px;
}
a.leaderBadge_third {
	background:url(<?php echo $imageurl; ?>/web_third_l.gif) top left no-repeat;
	padding-left:40px;
}
a.leaderBadge_third span {
	background:url(<?php echo $imageurl; ?>/web_third_r.gif) top right no-repeat;
	padding-right:15px;
}
a.leaderBadge_top20 {
	background:url(<?php echo $imageurl; ?>/web_top20_l.gif) top left no-repeat;
	padding-left:50px;
}
a.leaderBadge_top20 span {
	background:url(<?php echo $imageurl; ?>/web_top20_r.gif) top right no-repeat;
	padding-right:15px;
}
a.leaderBadge_contributor {
	background:url(<?php echo $imageurl; ?>/web_contributor_l.gif) top left no-repeat;
	padding-left:15px;
}
a.leaderBadge_contributor span {
	background:url(<?php echo $imageurl; ?>/web_contributor_r.gif) top right no-repeat;
	padding-right:15px;
}
a.leaderBadge:hover {
	background-position:0% -40px;
	text-decoration:none;
}
a.leaderBadge:hover span {
	background-position:100% -40px;
	text-decoration: none;
}
.findFriends div.error {
	margin-bottom:4px;
	white-space:nowrap;
}
.findFriends .emailArea {
	float:left;
	font-size:11px;
	color:#a9a9a9;
	margin-bottom:10px;
}
.findFriends input.emailButton {
	float:left;
	width:14px;
	height:14px;

	margin:3px 8px 0px 0px;
}
.findFriends label.emailLabel {
	float:left;
	margin-right:30px;
}
.findFriends input.emailField {
	width:308px;
	font:14px "Trebuchet MS";
}
.findFriends label.emailFieldLabel {
	width:308px;
	display:block;
	margin-bottom:4px;
	font-size:11px;
	line-height:11px;
	color:#a9a9a9;
}
.findFriends .tipBubbleSmall {
	float:left;
	background:url(<?php echo $imageurl; ?>/tipbubble_small.gif) top left no-repeat;
	width:198px;
	height:67px;
	margin:8px 0px 0px 10px;
	padding:13px 8px 13px 18px;
	color:#777;
}
.findFriends .tipBubbleSmall div {
	color:#000;
	margin-bottom:5px;
}
.findFriends .tipBubbleSmall div img {
	margin:0px 6px -2px 0px
}
.findFriends #contactsList {
	float:left;
	width:562px;
	border:1px solid #dbdbdb;
}
.findFriends #contactsList input.check {
	float:left;
	width:15px;
	height:15px;
	margin:0px 8px 0px 0px;
}
.findFriends #contactsList .header {
	float:left;
	width:546px;
	height:15px;
	line-height:15px;
	padding:8px;
	background-color:#dbdbdb;
	color:#000;
}
.findFriends #contactsList #entryArea {
	float:left;
	width:562px;
	height:272px;
	overflow-y:scroll;
}
.findFriends #contactsList .entry {
	float:left;
	clear:left;
	width:529px;
	padding:8px;
	font-size:16px;
	line-height:18px;
	color:#000;
	overflow:hidden;
}
.findFriends #contactsList .entry span {
	font-size:14px;
	color:#7d7d7d;
}
.findFriends #contactsList.invite .entry {
	font-size:12px;
	height:15px;
	line-height:15px;
}
.findFriends #contactsList.invite .entry div {
	float:left;
	width:215px;
	padding-right:5px;
	overflow:hidden;
}
.findFriends #contactsList.invite .entry span {
	float:left;
	font-size:12px;
}
.findFriends #contactsList .entry.even {
	background-color:#f3f3f3;
}
.findFriends #contactsList .entry img {
	float:left;
	width:50px;
	height:50px;
	border:solid 1px #cbd2d7;
	margin-right:8px;
}
.findFriends #contactsList .entry.nocheck img {
	margin-left:21px;
}
.findFriends #contactsList .entry div.status {
	float:right;
	font-style:italic;
	font-size:14px;
	color:#388c05;
	margin-right:5px;
}
.findFriends #addressArea {
	float:left;
	width:562px;
	border:1px solid #dbdbdb;
	margin-top:10px;
}
.findFriends #addressArea.bound {
	height:310px;
	overflow-y:scroll;
}
.findFriends #addressArea .entry {
	float:left;
	clear:left;
	width:546px;
	padding:8px;
	height:15px;
	line-height:15px;
	font-size:12px;
	color:#7d7d7d;
}
.findFriends #addressArea.bound .entry {
	width:529px;
}
.findFriends #addressArea .entry.even {
	background-color:#f3f3f3;
}
.findFriends #addressArea .entry input.check {
	float:left;
	width:15px;
	height:15px;
	margin:0px 8px 0px 0px;
}
.findFriends #addressArea .entry div {
	float:left;
	overflow:hidden;
}
.findFriends #emailError,.findFriends #emptyEmailError {
	float:left;
	margin-top:15px;
	margin-bottom:-10px;
}
#processPage.findFriends .divider {
	margin:20px 0px 4px;
}
.findFriends .content {
	margin-bottom:15px;
}
.findFriends .nameErrors {
	float:left;
	width:430px;
}
.findFriends .nameErrors .firstName {
	float:left;
	width:225px;
}
.findFriends .nameErrors .lastName {
	float:right;
	width:205px;
}
.findFriends .nameFields {
	float:left;
	position:relative;
}
.findFriends .nameFieldArea {
	float:left;
	position:relative;
}
.findFriends input.nameField {
	width:205px;
}
#processPage.findFriends .nameFields .tipBubble {
	position:absolute;
	margin-left:10px;
}
.findFriends .fieldHeading {
	font-size:11px;
	height:12px;
	line-height:12px;
	color:#a9a9a9;
	margin-bottom:4px;
	white-space:nowrap;
}
#processPage.findFriends .fieldHeading h5 {
	display:inline;
	margin:0px 5px 0px 0px;
}
.findFriends .toHeading {
	clear:left;
	float:left;
	margin-top:15px;
}
.findFriends #invitePreview {
	float:left;
	width:542px;
	border:1px solid #dbdbdb;
	background-color:#f3f3f3;
	color:#000;
	padding:10px;
	margin-top:10px;
}
.findFriends #invitePreview label {
	font-size:11px;
	color:#a9a9a9;
}
.findFriends #invitePreview textarea.text {
	width:512px;
	height:52px;
	padding:2px;
	font-family:Arial,Helvetica,sans-serif;
	font-size:14px;
	font-style:italic;
}
.findFriends #invitePreview textarea.default {
	color:#8f8f8f;
}
.findFriends #previewHeading {
	clear:both;
	float:left;
	margin-top:20px;
	width:100%;
}
.findFriends #previewHeading #rightArrow,.findFriends #previewHeading #downArrow,.findFriends #previewHeading #heading {
	cursor:pointer;
}
.findFriends #previewHeading #rightArrow {
	margin-right:9px;
}
.findFriends #previewHeading #downArrow {
	margin:0px 5px 2px 0px;
}
.findFriends #previewHeading #heading {
	display:inline;
	margin-bottom:0px;
}
.findFriends .skip {
	float:left;
	margin-left:15px;
	margin-top:10px;
}
.findFriends .importCta {
	float:left;
	background-color:#f3f3f3;
	color:#7d7d7d;
	border:1px solid #dbdbdb;
	width:253px;
	padding:20px 18px;
	line-height:18px;
}
.findFriends .importCta img {
	margin:15px 0px 15px -18px;
}
.findFriends .importCta a.standardButton {
	width:243px;
}
.findFriends .importCta a.standardButton span {
	width:233px;
}
.statusMsg {
	float:left;
	background-color:#effce7;
	color:#5baf00;
	font-weight:bold;
	font-size:13px;
	line-height:19px;
	padding:3px 8px 4px 4px;
	margin:15px 22px 0px 22px;
}
.statusMsg img {
	float:left;
	margin-right:5px;
}
.findFriends .statusMsg {
	margin:0px 0px 15px 0px;
}
a.callToAction_connect {
	float:left;
	display:block;
	background:url(<?php echo $imageurl; ?>/cta_connect2.gif) top left no-repeat;
	width:290px;
	height:64px;
	cursor:pointer;
}
a.callToAction_connect:hover {
	background-position:100% -64px;
}
a.callToAction_invite {
	float:left;
	display:block;
	background:url(<?php echo $imageurl; ?>/cta_invite.gif) top left no-repeat;
	width:290px;
	height:32px;
	cursor:pointer;
}
a.callToAction_invite:hover {
	background-position:100% -32px;
}
.regHeader .steps {
	position:absolute;
	top:17px;
	right:11px;
}
.regHeader .steps .step1,.regHeader .steps .step2 {
	float:left;
	background:url(<?php echo $imageurl; ?>/process_step1.gif) top left no-repeat;
	width:24px;
	height:24px;
	line-height:24px;
	font-size:0px;
}
.regHeader .steps .step1 {
	margin-right:10px;
}
.regHeader .steps .step2 {
	background-image:url(<?php echo $imageurl; ?>/process_step2.gif);
}
.regHeader .steps .step1.selected,.regHeader .steps .step2.selected {
	background-position:100% -24px;
}
#viewerCommentExportArea {
	float:left;
	width:463px;
	height:24px;
	line-height:24px;
	background-color:#abd8ee;
	margin-right:6px;
	font-size:12px;
	color:#5e6870;
}
#replyCommentExportArea {
	float:left;
	margin-top:8px;
	height:24px;
	line-height:24px;
	font-size:12px;
}
#viewerCommentExportArea input,#replyCommentExportArea input {
	padding:0px;
	margin:-1px 8px 0px 5px;
	vertical-align:middle;
}
#replyCommentExportArea input {
	margin-top:-2px;
}
#replyCommentExportArea label.disabled {
	color: #a9a9a9;
}