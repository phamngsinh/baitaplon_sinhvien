<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
$theimgurl = $config['imageurl'];
	
$query = cleanit($_REQUEST['query']);
$page = intval($_REQUEST['page']);

if($_REQUEST['sfilterupdates'] == "1")
{
	$pics = "1";
	$asql = "AND profilepicture!=''";
	STemplate::assign('pics',$pics);
}
elseif($_REQUEST['sfilterupdates2'] == "1")
{
	$pics = "0";
	STemplate::assign('pics',$pics);
}

if($page=="")
{
	$page = "1";
}
$currentpage = $page;

if ($page >=2)
{
	$pagingstart = ($page-1)*$config['max_posts_newmem'];
}
else
{
	$pagingstart = "0";
}

$query1 = "SELECT USERID FROM members WHERE USERID>0 AND status='1' AND username like '%".mysql_real_escape_string($query)."%' $asql";	

$query2 = "SELECT username, USERID, saying, city, country, public FROM members WHERE USERID>0 AND status='1' AND username like '%".mysql_real_escape_string($query)."%' $asql order by USERID desc limit $pagingstart, $config[max_posts_newmem]";
		
$executequery1 = $conn->Execute($query1);

$totalposts = count($executequery1->getrows());
if ($totalposts > 0)
{
	if($totalposts<=$config['maximum_results'])
	{
		$total = $totalposts;
	}
	else
	{
		$total = $config[maximum_results];
	}
	
	$toppage = ceil($total/$config['max_posts_newmem']);
	if($toppage==0)
	{
		$xpage=$toppage+1;
	}
	else
	{
		$xpage = $toppage;
	}
	
	$executequery2 = $conn->Execute($query2);
	$posts = $executequery2->getrows();
	$beginning=$pagingstart+1;
	$ending=$pagingstart+$executequery2->recordcount();
	$pagelinks="";
	$k=1;
	$theprevpage=$currentpage-1;
	$thenextpage=$currentpage+1;
	
	if($pics == "1")
	{
		$ap = "&pics=1";
	}
	
	if ($currentpage > 0)
	{
		STemplate::assign('page',$currentpage);
		if($currentpage > 1) 
		{
			$pagelinks.="<a href='$thebaseurl/searchchannels.php?query=$query&page=$theprevpage$ap' class=\"standardButton leftArrow\"><span><img src=\"$theimgurl/arrow_left.gif\" width=\"4\" height=\"8\" /></span></a>";
		}
		
		$counter=0;
		
		$lowercount = $currentpage-5;
		if ($lowercount <= 0) $lowercount = 1;
		
		while ($lowercount < $currentpage)
		{
			$pagelinks.="<a href='$thebaseurl/searchchannels.php?query=$query&page=$lowercount$ap' class=\"page\">$lowercount</a>";
			$lowercount++;
			$counter++;
		}
		
		$pagelinks.="<div class=\"currentPage\">$currentpage</div>";
		
		$uppercounter = $currentpage+1;
		
		while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
		{
			$pagelinks.="<a href='$thebaseurl/searchchannels.php?query=$query&page=$uppercounter$ap' class=\"page\">$uppercounter</a>";
			$uppercounter++;
		}
		
		if($currentpage < $toppage) 
		{
			$pagelinks.="<a href='$thebaseurl/searchchannels.php?query=$query&page=$thenextpage$ap' class=\"standardButton rightArrow\"><span><img src=\"$theimgurl/arrow_right.gif\" width=\"4\" height=\"8\" /></span></a>";
		}
	}
}
STemplate::assign('posts',$posts);
$templateselect = "searchchannels.tpl";

if($query != "")
{
	$pagetitle = $query." - ";
}
$pagetitle .= $lang[311];
STemplate::assign('pagetitle',$pagetitle);

//TEMPLATES BEGIN
STemplate::assign('query',$query);
STemplate::assign('page',$page);
STemplate::assign('beginning',$beginning);
STemplate::assign('ending',$ending);
STemplate::assign('pagelinks',$pagelinks);
STemplate::assign('total',$total+0);
STemplate::assign('error',$error);
STemplate::display('header.tpl');
STemplate::display($templateselect);
STemplate::display('footer.tpl');
//TEMPLATES END
?>