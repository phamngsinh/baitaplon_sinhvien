<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
$theimgurl = $config['imageurl'];

$USERID = intval($_REQUEST['id']);
if($USERID > 0)
{	
	STemplate::assign('USERID',$USERID);
	$query = "SELECT * FROM members WHERE USERID='".mysql_real_escape_string($USERID)."' AND status='1'";
	$executequery = $conn->execute($query);
	$profilearray = $executequery->getarray();
	STemplate::assign('p',$profilearray[0]);
	$username = $profilearray[0]['username'];
	
	$public = $profilearray[0]['public'];
	
	if($public == "0")
	{
		$ME = intval($_SESSION['USERID']);
		if($ME > 0)
		{
			if($USERID == $ME)
			{
				$display = "1";
			}
			else
			{
				$is_fr = check_friend($ME, $USERID);
				if($is_fr == "1")
				{
					$display = "1";
				}
				else
				{
					$display = "2";
				}
			}
		}
		else
		{
			$display = "3";
		}
	}
	else
	{
		$ME = intval($_SESSION['USERID']);
		if($ME > 0)
		{
			$query="SELECT count(*) as total FROM block WHERE USERID='".mysql_real_escape_string($USERID)."' AND BID='".mysql_real_escape_string($ME)."'";
			$executequery=$conn->execute($query);
			$block_count = $executequery->fields[total];
			if($block_count > 0)
			{
				$display = "4";
			}
			else
			{
				$display = "1";
			}
		}
		else
		{
			$display = "1";
		}
	}
	
	
	if($display == "1")
	{
		get_bg($USERID);
		$templateselect = "widget.tpl";
	}
	else
	{
		if($display == "2")
		{
			if($_REQUEST['saddprifr'] == "1")
			{
				$FID = intval($_SESSION['USERID']);
				if($FID > 0)
				{
					$fquery="INSERT INTO messages_inbox SET MSGTO='".mysql_real_escape_string($USERID)."', MSGFROM='".mysql_real_escape_string($FID)."', type='fr', time='".time()."'";
					$conn->execute($fquery);
					$msg = $lang['216'];
				}
			}
		}
		STemplate::assign('display',$display);
		$templateselect = "profile_private.tpl";
	}
}
else
{
	header("Location:$config[baseurl]");exit;
}

if($username != "")
{
	$pagetitle = $username." - ";
}
$pagetitle .= $lang[344];
STemplate::assign('pagetitle',$pagetitle);

//TEMPLATES BEGIN
STemplate::assign('beginning',$beginning);
STemplate::assign('ending',$ending);
STemplate::assign('pagelinks',$pagelinks);
STemplate::assign('total',$total);
STemplate::assign('error',$error);
STemplate::display('header.tpl');
STemplate::display($templateselect);
STemplate::display('footer.tpl');
//TEMPLATES END
?>