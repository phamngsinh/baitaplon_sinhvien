<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
$theimgurl = $config['imageurl'];

$query = cleanit($_REQUEST['query']);

$mquery1 = "SELECT USERID FROM members WHERE USERID>0 AND status='1' AND username like '%".mysql_real_escape_string($query)."%'";	
$mquery2 = "SELECT username, USERID, saying, city, country, public FROM members WHERE USERID>0 AND status='1' AND username like '%".mysql_real_escape_string($query)."%' order by USERID desc limit 4";
$mexecutequery1 = $conn->Execute($mquery1);
$mtotal = count($mexecutequery1->getrows())+0;
STemplate::assign('mtotal',$mtotal);
if ($mtotal > 0)
{
	$mexecutequery2 = $conn->Execute($mquery2);
	$mposts = $mexecutequery2->getrows();
	STemplate::assign('mposts',$mposts);
}

$uquery1 = "SELECT DISTINCT A.ID FROM posts A, members B WHERE (A.USERID=B.USERID AND B.public='1' AND A.type='update' AND A.msg like '%".mysql_real_escape_string($query)."%')";	
$uquery2 = "SELECT DISTINCT A.*, B.username FROM posts A, members B WHERE (A.USERID=B.USERID AND B.public='1' AND A.type='update' AND A.msg like '%".mysql_real_escape_string($query)."%') order by A.ID desc limit 4";
$uexecutequery1 = $conn->Execute($uquery1);
$utotal = count($uexecutequery1->getrows())+0;
STemplate::assign('utotal',$utotal);
if ($utotal > 0)
{
	$uexecutequery2 = $conn->Execute($uquery2);
	$uposts = $uexecutequery2->getrows();
	STemplate::assign('uposts',$uposts);
}

$total = $mtotal + $utotal;
STemplate::assign('total',$total);
$templateselect = "search.tpl";

if($query != "")
{
	$pagetitle = $query." - ";
}
$pagetitle .= $lang['307'];
STemplate::assign('pagetitle',$pagetitle);

//TEMPLATES BEGIN
STemplate::assign('query',$query);
STemplate::assign('error',$error);
STemplate::display('header.tpl');
STemplate::display($templateselect);
STemplate::display('footer.tpl');
//TEMPLATES END
?>