<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
$theimgurl = $config['imageurl'];	

$page = intval($_REQUEST['page']);

$m = intval($_REQUEST['m']);
if($m == "1")
{
	$lastmonth = time() - (31 * 24 * 60 * 60);
	$asql = "AND A.time_added>$lastmonth";
}
elseif($m == "2")
{
	$lastweek = time() - (7 * 24 * 60 * 60);
	$asql = "AND A.time_added>$lastweek";
}
elseif($m == "3")
{
	$aday = time() - (24 * 60 * 60);
	$asql = "AND A.time_added>$aday";
}

if($_REQUEST['sfilterupdates'] == "1")
{
	$si = intval($_REQUEST['si']);
	$st = intval($_REQUEST['st']);
}
else
{
	$si = $_REQUEST['si'];
	if($si == "")
	{
		$si = "1";
	}
	else
	{
		$si = intval($_REQUEST['si']);
	}
	$st = $_REQUEST['st'];
	if($st == "")
	{
		$st = "1";
	}
	else
	{
		$st = intval($_REQUEST['st']);
	}
}

if($si == "0")
{
	$asql .= " AND A.pic=''";
}
if($st == "0")
{
	$asql .= " AND A.pic!=''";
}

if($page=="")
{
	$page = "1";
}
$currentpage = $page;

if ($page >=2)
{
	$pagingstart = ($page-1)*$config['max_posts_recentupdates'];
}
else
{
	$pagingstart = "0";
}

$query1 = "SELECT DISTINCT A.ID FROM posts A, members B WHERE B.status=1 AND (A.USERID=B.USERID AND B.public='1' AND A.type='update' $asql)";	

$query2 = "SELECT DISTINCT A.*, B.username FROM posts A, members B WHERE B.status=1 AND (A.USERID=B.USERID AND B.public='1' AND A.type='update' $asql) order by A.ID desc limit $pagingstart, $config[max_posts_recentupdates]";
		
$executequery1 = $conn->Execute($query1);

$totalposts = count($executequery1->getrows());
if ($totalposts > 0)
{
	if($totalposts<=$config['maximum_results'])
	{
		$total = $totalposts;
	}
	else
	{
		$total = $config[maximum_results];
	}
	
	$toppage = ceil($total/$config['max_posts_recentupdates']);
	if($toppage==0)
	{
		$xpage=$toppage+1;
	}
	else
	{
		$xpage = $toppage;
	}
	
	$executequery2 = $conn->Execute($query2);
	$posts = $executequery2->getrows();
	$beginning=$pagingstart+1;
	$ending=$pagingstart+$executequery2->recordcount();
	$pagelinks="";
	$k=1;
	$theprevpage=$currentpage-1;
	$thenextpage=$currentpage+1;
	
	if ($currentpage > 0)
	{
		if($currentpage > 1) 
		{
			$pagelinks.="<a href='$thebaseurl/recentupdates.php?m=$m&si=$si&st=$st&page=$theprevpage' class=\"standardButton leftArrow\"><span><img src=\"$theimgurl/arrow_left.gif\" width=\"4\" height=\"8\" /></span></a>";
		}
		
		$counter=0;
		
		$lowercount = $currentpage-5;
		if ($lowercount <= 0) $lowercount = 1;
		
		while ($lowercount < $currentpage)
		{
			$pagelinks.="<a href='$thebaseurl/recentupdates.php?m=$m&si=$si&st=$st&page=$lowercount' class=\"page\">$lowercount</a>";
			$lowercount++;
			$counter++;
		}
		
		$pagelinks.="<div class=\"currentPage\">$currentpage</div>";
		
		$uppercounter = $currentpage+1;
		
		while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
		{
			$pagelinks.="<a href='$thebaseurl/recentupdates.php?m=$m&si=$si&st=$st&page=$uppercounter' class=\"page\">$uppercounter</a>";
			$uppercounter++;
		}
		
		if($currentpage < $toppage) 
		{
			$pagelinks.="<a href='$thebaseurl/recentupdates.php?m=$m&si=$si&st=$st&page=$thenextpage' class=\"standardButton rightArrow\"><span><img src=\"$theimgurl/arrow_right.gif\" width=\"4\" height=\"8\" /></span></a>";
		}
	}
}
else
{
	$total = 0;	
}
STemplate::assign('posts',$posts);

$templateselect = "recentupdates.tpl";

$pagetitle .= $lang['296'];
STemplate::assign('pagetitle',$pagetitle);

//TEMPLATES BEGIN
STemplate::assign('page',$page);
STemplate::assign('m',$m);
STemplate::assign('si',$si);
STemplate::assign('st',$st);
STemplate::assign('ending',$ending);
STemplate::assign('pagelinks',$pagelinks);
STemplate::assign('total',$total);
STemplate::assign('error',$error);
STemplate::display('header.tpl');
STemplate::display($templateselect);
STemplate::display('footer.tpl');
//TEMPLATES END
?>