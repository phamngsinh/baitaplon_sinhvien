<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
$theimgurl = $config['imageurl'];

$USERID = $_SESSION['USERID'];
if ($USERID != "" && $USERID >= 0 && is_numeric($USERID))
{		
	if($_REQUEST['sprivacySettings'] == "1")
	{
		$private = intval(htmlentities(strip_tags($_REQUEST['private']), ENT_COMPAT, "UTF-8"));
		$query = "UPDATE members SET public='".mysql_real_escape_string($private)."' WHERE USERID='".mysql_real_escape_string($USERID)."'";
		$conn->execute($query);
		$msg = $lang['209'];
	}
	elseif($_REQUEST['sbForm'] == "1")
	{
		$buser = htmlentities(strip_tags($_REQUEST['buser']), ENT_COMPAT, "UTF-8");
		$query = "SELECT USERID FROM members WHERE username='".mysql_real_escape_string($buser)."'";
		$executequery=$conn->execute($query);
		$BID = $executequery->fields[USERID];
		if($BID == "")
		{
			$bmsg = $lang['259'];
		}
		elseif($BID == $USERID || $BID == "0")
		{
			$bmsg = $lang['260'];
		}
		else
		{
			$query = "DELETE FROM follow WHERE USERID='".mysql_real_escape_string($USERID)."' AND FID='".mysql_real_escape_string($BID)."'";
			$executequery=$conn->execute($query);
			$query = "DELETE FROM follow WHERE USERID='".mysql_real_escape_string($BID)."' AND FID='".mysql_real_escape_string($USERID)."'";
			$executequery=$conn->execute($query);
			$query = "INSERT INTO block SET USERID='".mysql_real_escape_string($USERID)."', BID='".mysql_real_escape_string($BID)."'";
			$executequery=$conn->execute($query);
			$bmsg = $lang['261'];
		}
	}
	elseif(intval($_REQUEST['ub']) > "0")
	{
		$BID = intval(htmlentities(strip_tags($_REQUEST['ub']), ENT_COMPAT, "UTF-8"));
		$query = "DELETE FROM block WHERE USERID='".mysql_real_escape_string($USERID)."' AND BID='".mysql_real_escape_string($BID)."'";
		$executequery=$conn->execute($query);
		$bmsg = $lang['262'];
	}
	
	$query = "SELECT public FROM members WHERE USERID='".mysql_real_escape_string($USERID)."'"; 
	$executequery = $conn->execute($query);
    $p = $executequery->getarray();
	STemplate::assign('p',$p[0]);
	
	$query = "SELECT DISTINCT A.BID, B.username FROM block A, members B WHERE A.USERID='".mysql_real_escape_string($USERID)."' AND A.BID=B.USERID"; 
	$executequery = $conn->execute($query);
    $b = $executequery->getarray();
	STemplate::assign('b',$b);
	
	$templateselect = "privacy.tpl";
}
else
{
	$redirect = base64_encode($config['baseurl']."/privacy.php");
	header("Location:$config[baseurl]/login.php?redirect=$redirect");exit;
}

$pagetitle = $lang[244];
STemplate::assign('pagetitle',$pagetitle);

//TEMPLATES BEGIN
$bname = htmlentities(strip_tags($_REQUEST['bname']), ENT_COMPAT, "UTF-8");
STemplate::assign('bname',$bname);
STemplate::assign('msg',$msg);
STemplate::assign('bmsg',$bmsg);
STemplate::display('header.tpl');
STemplate::display($templateselect);
STemplate::display('footer.tpl');
//TEMPLATES END
?>