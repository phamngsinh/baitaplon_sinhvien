<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");

$pagetitle = $lang['14'];
STemplate::assign('pagetitle',$pagetitle);

$redirect = base64_decode($_REQUEST['redirect']);
$redirect = escape($redirect);

if ($_SESSION['USERID'] != "" && $redirect != "")
{
	header("Location:$redirect");exit;
}

STemplate::assign('bdays',listdays(0));
STemplate::assign('bmonths',listmonths(0));
STemplate::assign('byears',listyears(0));
	
if($_REQUEST['register']!="")
{
	$username = cleanit($_REQUEST['username']);
	$username = str_replace("(", "", $username);
	$username = str_replace(")", "", $username);
	STemplate::assign('username',$username);
	$password = stripslashes(strip_tags($_REQUEST['password1']));
	STemplate::assign('pw',$password);
	$confirmpassword = stripslashes(strip_tags($_REQUEST['confirmpassword']));
	STemplate::assign('pw2',$confirmpassword);
	$imagecode = stripslashes($_REQUEST['imagecode']);
	$email = stripslashes(strip_tags($_REQUEST['email']));
	STemplate::assign('email',$email);
	$day1 = intval($_REQUEST['day']);
	$month1 = intval($_REQUEST['month']);
	$year1 = intval($_REQUEST['year']);
	STemplate::assign('bdays',listdays($day1));
	STemplate::assign('bmonths',listmonths($month1));
	STemplate::assign('byears',listyears($year1));
	
	if($username == "")
	{
		$err1 = "$lang[15]";
		STemplate::assign('err1',$err1);
	}
	elseif(!preg_match("/^[a-zA-Z0-9]*$/i",$username))
	{
		$err1 = "$lang[5]";
		STemplate::assign('err1',$err1);
	}
	elseif(!verify_email_username($username))
	{
		$err1 = "$lang[19]";
		STemplate::assign('err1',$err1);
	}
	
	if ($password == "")
	{
		$err2 = "$lang[20]";
		STemplate::assign('err2',$err2);
	}
	
	if ($confirmpassword == "")
	{
		$err3 = "$lang[23]";
		STemplate::assign('err3',$err3);
	}
	
	if ($password != "" && $confirmpassword != "" && $password != $confirmpassword)
	{
		$err2 = "$lang[22]";
		STemplate::assign('err2',$err2);
		$err3 = "$lang[21]";
		STemplate::assign('err3',$err3);
	}
	
	if ($email == "")
	{
		$err4 = "$lang[26]";
		STemplate::assign('err4',$err4);
	}
	elseif(!verify_valid_email($email))
	{
		$err4 = "$lang[24]";
		STemplate::assign('err4',$err4);
	}
	elseif (!verify_email_unique($email))
	{
		$err4 = "$lang[25]";
		STemplate::assign('err4',$err4);
	}
	
	if ($imagecode == "")
	{
		$err5 = "$lang[28]";
		STemplate::assign('err5',$err5);
	}
	elseif($imagecode != $_SESSION[imagecode])
	{
		if ($config['enable_captcha'] != "0")
		{
			$err5 = "$lang[27]";
			STemplate::assign('err5',$err5);
		}
	}
		
	if ($day1 <= "0")
	{
		$err6 = "$lang[32]";
		STemplate::assign('err6',$err6);
	}
	if ($month1 <= "0")
	{
		$err7 = "$lang[33]";
		STemplate::assign('err7',$err7);
	}
	if ($year1 <= "0")
	{
		$err8 = "$lang[34]";
		STemplate::assign('err8',$err8);
	}
	
	if($err1 == "" && $err2 == "" && $err3 == "" && $err4 == "" && $err5 == "" && $err6 == "" && $err7 == "" && $err8 == "")
	{
		$joinbday = $year1."-".$month1."-".$day1;
		$moresql .= ", birthday='".mysql_real_escape_string($joinbday)."'";
		
		$md5pass = md5($password);
		$query="INSERT INTO members SET email='".mysql_real_escape_string($email)."',username='".mysql_real_escape_string($username)."', password='".mysql_real_escape_string($md5pass)."', pwd='".mysql_real_escape_string($password)."', addtime='".time()."', lastlogin='".time()."', ip='".$_SERVER['REMOTE_ADDR']."', lip='".$_SERVER['REMOTE_ADDR']."' $moresql";
		$result=$conn->execute($query);
		$userid = mysql_insert_id();
		
		if($userid != "" && is_numeric($userid) && $userid > 0)
		{
			$query="SELECT USERID,email,username,verified from members WHERE USERID='".mysql_real_escape_string($userid)."'";
			$result=$conn->execute($query);
			
			$SUSERID = $result->fields['USERID'];
			$SEMAIL = $result->fields['email'];
			$SUSERNAME = $result->fields['username'];
			$SVERIFIED = $result->fields['verified'];
			$_SESSION[USERID]=$SUSERID;
			$_SESSION[EMAIL]=$SEMAIL;
			$_SESSION[USERNAME]=$SUSERNAME;
			$_SESSION[VERIFIED]=$SVERIFIED;
			
			// Generate Verify Code Begin
			$verifycode = generateCode(5).time();
			$query = "INSERT INTO members_verifycode SET USERID='".mysql_real_escape_string($SUSERID)."', code='$verifycode'";
            $conn->execute($query);
			if(mysql_affected_rows()>=1)
			{
				$proceedtoemail = true;
			}
			else
			{
				$proceedtoemail = false;
			}
			// Generate Verify Code End
			
			// Send Welcome E-Mail Begin
			if ($proceedtoemail)
			{
                STemplate::assign('verifycode',$verifycode);
                $sendto = $SEMAIL;
                $sendername = $config['site_name'];
				STemplate::assign('sendername',$sendername);
                $from = $config['site_email'];
				$query = "SELECT * FROM sendmail WHERE EID='welcomeemail'";
                $executequery = $conn->execute($query);
                $subject = $lang['275']." ".$sendername;
                $sendmailtemplate = $executequery->fields['template'];
                $sendmailbody=STemplate::fetch($sendmailtemplate);
                mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
				
				$esub = $lang['275']." ".$config['short_name'];
				$emsg = $lang['338'];
				$query = "INSERT INTO messages_inbox SET MSGTO='".mysql_real_escape_string($SUSERID)."', MSGFROM='0', subject='".mysql_real_escape_string($esub)."', message='".mysql_real_escape_string($emsg)."', time='".time()."'";
            	$conn->execute($query);
			}
			// Send Welcome E-Mail End
			
			if($redirect == "")
			{
				header("Location:$config[baseurl]/invite_friends.php");exit;
			}
			else
			{
				header("Location:$redirect");exit;
			}
		}
	}
}
STemplate::display('header.tpl');
STemplate::display('register.tpl');
STemplate::display('footer.tpl');
?>