<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");

if ($_REQUEST[c] != "")
{
	$code = htmlentities(strip_tags($_REQUEST[c]), ENT_COMPAT, "UTF-8");
	$query="SELECT * from members_verifycode WHERE code='".mysql_real_escape_string($code)."'";
	$result=$conn->execute($query);
	
	if($result->recordcount()>=1)
	{
		$userid = $result->fields['USERID'];
		$query="SELECT verified from members WHERE USERID='".mysql_real_escape_string($userid)."'";
		$result=$conn->execute($query);
		$verified = $result->fields['verified'];
		
		if ($verified == "1")
		{
			$msg = "$lang[194]";
		}
		else
		{
			$query="UPDATE members SET verified='1' WHERE USERID='".mysql_real_escape_string($userid)."'";
			$result=$conn->execute($query);
			$msg = "$lang[195]";
			
			if ($_SESSION[USERID] == $userid)
			{
				$_SESSION[VERIFIED] = "1";
			}
		}
	}
	else
	{
		$msg = "$lang[196]";
	}
}

$pagetitle = "$lang[193]";
STemplate::assign('pagetitle',$pagetitle);
STemplate::assign('msg',$msg);
//TEMPLATES BEGIN
STemplate::display('header.tpl');
STemplate::display('verify.tpl');
STemplate::display('footer.tpl');
//TEMPLATES END
?>