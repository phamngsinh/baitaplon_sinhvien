<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
$theimgurl = $config['imageurl'];

$USERID = $_SESSION['USERID'];
if ($USERID != "" && $USERID >= 0 && is_numeric($USERID))
{		
	if($_REQUEST['saform'] == "1")
	{
		$alert_com = intval(htmlentities(strip_tags($_REQUEST[alert_com]), ENT_COMPAT, "UTF-8"));
		$alert_msg = intval(htmlentities(strip_tags($_REQUEST[alert_msg]), ENT_COMPAT, "UTF-8"));
		$alert_fol = intval(htmlentities(strip_tags($_REQUEST[alert_fol]), ENT_COMPAT, "UTF-8"));
		$alert_fr = intval(htmlentities(strip_tags($_REQUEST[alert_fr]), ENT_COMPAT, "UTF-8"));
		$query = "UPDATE members SET alert_com='".mysql_real_escape_string($alert_com)."', alert_msg='".mysql_real_escape_string($alert_msg)."', alert_fol='".mysql_real_escape_string($alert_fol)."', alert_fr='".mysql_real_escape_string($alert_fr)."' WHERE USERID='".mysql_real_escape_string($USERID)."'";
		$conn->execute($query);
		$msg = $lang['209'];
	}
	
	$query = "SELECT alert_com, alert_msg, alert_fol, alert_fr FROM members WHERE USERID='".mysql_real_escape_string($USERID)."'"; 
	$executequery = $conn->execute($query);
    $p = $executequery->getarray();
	STemplate::assign('p',$p[0]);
	
	$templateselect = "alerts.tpl";
}
else
{
	$redirect = base64_encode($config['baseurl']."/alerts.php");
	header("Location:$config[baseurl]/login.php?redirect=$redirect");exit;
}

$pagetitle = $lang[151];
STemplate::assign('pagetitle',$pagetitle);

//TEMPLATES BEGIN
STemplate::assign('msg',$msg);
STemplate::display('header.tpl');
STemplate::display($templateselect);
STemplate::display('footer.tpl');
//TEMPLATES END
?>