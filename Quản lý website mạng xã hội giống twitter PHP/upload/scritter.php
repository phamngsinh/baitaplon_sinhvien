<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/
include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
$UID = intval($_REQUEST['id']);

function htmlentities2unicodeentities ($input) {
  $htmlEntities = array_values (get_html_translation_table (HTML_ENTITIES, ENT_QUOTES));
  $entitiesDecoded = array_keys   (get_html_translation_table (HTML_ENTITIES, ENT_QUOTES));
  $num = count ($entitiesDecoded);
  for ($u = 0; $u < $num; $u++) {
    $utf8Entities[$u] = '&#'.ord($entitiesDecoded[$u]).';';
  }
  return str_replace ($htmlEntities, $utf8Entities, $input);
} 
function scritter_updatecount($var)
{
        global $conn;
        $query="SELECT count(*) as total FROM posts WHERE USERID='".mysql_real_escape_string($var)."' and type='update'";
        $executequery=$conn->execute($query);
		$results = $executequery->fields[total];
		return $results+0;
}
function scritter_profileviews($var)
{
        global $conn;
        $query="SELECT profileviews FROM members WHERE USERID='".mysql_real_escape_string($var)."' limit 1";
        $executequery=$conn->execute($query);
		$results = $executequery->fields[profileviews];
		return $results+0;
}


$query = "SELECT A.*, B.username, B.saying FROM posts A, members B WHERE (A.USERID='".mysql_real_escape_string($UID)."' AND A.USERID=B.USERID AND A.type='update') order by A.ID desc limit 20";
header("Content-Type: text/xml"); 
header("Expires: 0"); 
print "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"; 
print "<data>\n"; 
print "<items>\n"; 
$db=mysql_connect ($DBHOST,$DBUSER,$DBPASSWORD) or die ('I cannot connect to the database because: ' . mysql_error());
mysql_select_db ($DBNAME); 
@mysql_query("SET NAMES 'UTF8'");
$result = mysql_query($query) or die ('Query Error: ' . mysql_error()); 
while ($results = mysql_fetch_array($result)) 
{ 
  	$description = $results['msg']; 
  	$getusername = $results['username'];
	$saying = $results['saying'];
  	$pic = $results['pic'];
	if($pic != "")
	{
		print "<item type=\"image\">\n";
		print "  <path>".$config['picurl']."/$pic</path>\n";
		print "  <date>";
		print date("j-n-Y", $results[time_added]);
		print "  </date>\n";
		print "  <title>".htmlentities2unicodeentities($description)."</title>\n";
	}
	else
	{
		print "<item type=\"text\">\n";	
		print "  <text>".htmlentities2unicodeentities($description)."</text>\n";
		print "  <date>";
		print date("j-n-Y", $results[time_added]);
		print "  </date>\n";
		print "  <title>".htmlentities2unicodeentities($description)."</title>\n";
	}
	$UID2 = $results['ID'];
	$queryc = "SELECT A.username, B.msg FROM members A, posts B WHERE B.UID='".mysql_real_escape_string($UID2)."' AND A.USERID=B.USERID AND B.type='com-update' order by B.ID desc";	
	$executequeryc = $conn->execute($queryc);
	$coms = $executequeryc->getarray();
	if(count($coms) > 0)
	{
		print "<comments>\n";
		foreach ($coms as &$value) 
		{
			print "<comment>".stripslashes($value['username']).": ".stripslashes($value['msg'])."</comment>\n";
		}
		print "</comments>\n";
	}
	print "</item>\n"; 
}  
print "</items>\n"; 
print "<settings>\n";
	print "<menuButton path=\"".$thebaseurl."/images/scritter_image.png\" />\n";
	print "<info>\n";
		print "<image>".$config['membersprofilepicurl']."/thumbs/".get_propic($UID)."</image>\n";
		print "<userName>".stripslashes($getusername)."</userName>\n";
		print "<profileLink>\n";
			print "<text>".$thebaseurl."/".stripslashes($getusername)."</text>\n";
			print "<link>".$thebaseurl."/".stripslashes($getusername)."</link>\n";
		print "</profileLink>\n";
		print "<detailsText>".scritter_updatecount($UID)." ".$lang['117']." | ".scritter_profileviews($UID)." ".$lang['119']."</detailsText>\n";
		print "<mobile>".stripslashes($saying)."</mobile>\n";
		print "<own>\n";
			print "<text>".$thebaseurl."</text>\n";
			print "<link>".$thebaseurl."</link>\n";
		print "</own>\n";
		print "<learn>\n";
			print "<text>".$lang['335']."</text>\n";
			print "<link>".$thebaseurl."/about_us.php</link>\n";
		print "</learn>\n";
	print "</info>\n";
print "</settings>\n";
print "</data>\n"; 
mysql_close();
?> 