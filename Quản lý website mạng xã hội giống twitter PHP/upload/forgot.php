<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");

if($_REQUEST['forgot']!="")
{
	$username = cleanit($_REQUEST['username']);
	$email = cleanit($_REQUEST['email']);
	
	if($username == "" && $email == "")
	{
		$error = $lang['326'];
	}
	else
	{
		if($username != "")
		{
			if(verify_email_username($username))
			{
				$error = $lang['327'];
				STemplate::assign('username',$username);
			}
			else
			{
				//e-mail
				$equery="SELECT email, pwd FROM members WHERE username='".mysql_real_escape_string($username)."'";
				$executequerye=$conn->execute($equery);
				$eusername = $username;
				$pwd = $executequerye->fields['pwd'];
				$sendto = $executequerye->fields['email'];
				$sendername = $config['site_name'];
				$from = $config['site_email'];
				$subject = $lang['329'];
				$sendmailbody = stripslashes($eusername).",<br><br>".$lang['330']."<br>".$lang['4'].": $eusername<br>".$lang['6'].": $pwd <br><br>".$lang['69'].",<br>".$sendername;
				mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
				//end email
				$error = $lang['331'];
			}
		}
		elseif($email != "")
		{
			if(!verify_valid_email($email))
			{
				$error = "$lang[24]";
				STemplate::assign('email',$email);
			}
			elseif (verify_email_unique($email))
			{
				$error = $lang['328'];
				STemplate::assign('email',$email);
			}
			else
			{
				//e-mail
				$equery="SELECT username, pwd FROM members WHERE email='".mysql_real_escape_string($email)."'";
				$executequerye=$conn->execute($equery);
				$sendto = $email;
				$pwd = $executequerye->fields['pwd'];
				$eusername = $executequerye->fields['username'];
				$sendername = $config['site_name'];
				$from = $config['site_email'];
				$subject = $lang['329'];
				$sendmailbody = stripslashes($eusername).",<br><br>".$lang['330']."<br>".$lang['4'].": $eusername<br>".$lang['6'].": $pwd <br><br>".$lang['69'].",<br>".$sendername;
				mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
				//end email
				$error = $lang['331'];
			}
		}
	}
}

$pagetitle = "$lang[324]";
STemplate::assign('pagetitle',$pagetitle);

STemplate::assign('error',$error);

//TEMPLATES BEGIN
STemplate::display('header.tpl');
STemplate::display('forgot.tpl');
STemplate::display('footer.tpl');
//TEMPLATES END
?>