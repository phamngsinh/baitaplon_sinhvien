				<div id="pageHeader">
    				<div class="floatLeft">{$lang148}</div>
					<div class="headerTabs">        
                    	<a href="{$baseurl}/myprofile.php" class="tab"><span>{$lang147}</span></a>    
						<a href="{$baseurl}/design.php" class="tab"><span>{$lang149}</span></a>
						<a href="{$baseurl}/privacy.php" class="tab"><span>{$lang150}</span></a>
                        <div class="tabSelected"><span>{$lang151}</span></div>
					</div>
				</div>

				<div id="processPage">
				    <div id="columnLeft" style="position: relative; width:580px; height: 300px;">
					
						{if $msg ne ""}
						<div id="" class="error mainError" style="">*** {$msg} ***</div>  
						{/if}
					
                        <h2>{$lang268}</h2>
                        <p style="margin-bottom:5px;">{$lang269}</p>
                        <form id="aform" name="aform" action="{$baseurl}/alerts.php" method="post">
                        <ul class="radioBlock" style="clear:both; margin-top:0px;">
                          <li style="width:100%;">
                              <input name="alert_com" class="radio" type="checkbox" value="1" {if $p.alert_com eq "1"}checked="checked"{/if}/>
                              <label class="radioLabel" for="commentAlert1">{$lang270}</label>
                              <br/>
                          </li>
                          <li style="width:100%;">
                              <input name="alert_msg" class="radio" type="checkbox" value="1" {if $p.alert_msg eq "1"}checked="checked"{/if}/>
                              <label class="radioLabel" for="privateMessagesAlert1">{$lang271}</label>
                              <br/>
                          </li>
                          <li style="width:100%;">
                              <input name="alert_fol" class="radio" type="checkbox" value="1" {if $p.alert_fol eq "1"}checked="checked"{/if}/>
                              <label class="radioLabel" for="followersAlert1">{$lang272}</label>
                              <br/>
                          </li>
                          <li style="width:100%;">
                              <input name="alert_fr" class="radio" type="checkbox" value="1" {if $p.alert_fr eq "1"}checked="checked"{/if}/>
                              <label class="radioLabel" for="friendInvitationAlert1">{$lang273}</label>
                              <br/>
                          </li>
                        </ul>
                        <input type="hidden" name="saform" value="1" />
                        </form>
                        
                        {if $smarty.session.VERIFIED ne "1"}
                        <div id="" class="error mainError" style="">*** {$lang274} ***</div>
                        {/if}
                        
                        <div class="divider" style="margin-bottom:4px;"></div>
                        <div class="buttonArea">
                            <a href="#" onclick="document.aform.submit();" style="margin-right:4px;" class="standardButton"><span>{$lang158}</span></a>
                            <a href="{$baseurl}/alerts.php" class="standardButton"><span>{$lang159}</span></a>
                        </div>
					</div>
				</div>

                </div>
                </div>