				<div id="pageHeader">
    				<div class="floatLeft">{$lang148}</div>
					<div class="headerTabs">        
                    	<a href="{$baseurl}/myprofile.php" class="tab"><span>{$lang147}</span></a>  
                        <a href="{$baseurl}/design.php" class="tab"><span>{$lang149}</span></a>  
						<div class="tabSelected"><span>{$lang150}</span></div>
						<a href="{$baseurl}/alerts.php" class="tab"><span>{$lang151}</span></a>
					</div>
				</div>

                <div id="processPage">
                    <div id="columnLeft">
                    	
                        {if $msg ne ""}
						<div id="" class="error mainError" style="">*** {$msg} ***</div>  
						{/if}
                        
                        <h2>{$lang245}</h2>
                        <p style="margin-bottom:5px">{$lang246} {$short_name}:</p>
                        <form id="privacySettings" name="privacySettings" action="{$baseurl}/privacy.php" method="post">
                        <ul class="radioBlock">
                            <li>
                                <input name="private" class="radio" style="margin-bottom:20px;" type="radio" value="0" {if $p.public eq "0"}checked="checked"{/if}/>
                                <label for="private1" class="black">{$lang247}</label><br/>
                                {$lang249}.
                            </li>
                        </ul>
                        <ul class="radioBlock">
                            <li>
                                <input name="private" class="radio" style="margin-bottom:20px;" type="radio" value="1" {if $p.public eq "1"}checked="checked"{/if}/>
                                <label for="private2" class="black">{$lang248}</label><br/>
                                {$lang250} {$short_name}.
                            </li>
                        </ul>
                        <input type="hidden" name="sprivacySettings" value="1" />
                        </form>
                        <div class="divider" style="margin-bottom: 4px;"></div>
                        <div class="buttonArea">
                            <a href="#" onclick="document.privacySettings.submit();" class="standardButton" style="margin-right:4px;"><span>{$lang158}</span></a>
                            <a href="{$baseurl}/privacy.php" class="standardButton"><span>{$lang159}</span></a>
                        </div>
                    </div>  
                    <div id="columnRight">
                        <div class="panelRight" id="blocked">
                            <h4>{$lang251}</h4>
                            <div class="black">
                                {$lang252}. {$lang253}.
                            </div>
                            <br />
                            <p>{$lang254}.</p>
                            
                            {if $bmsg ne ""}
                            <div id="" class="error mainError" style="">*** {$bmsg} ***</div>  
                            {/if}
                        
                            <span class="label">{$lang4}:</span>
                            <span>
                            	<form id="bForm" name="bForm" action="{$baseurl}/privacy.php" method="post">
                            	<input type="text" maxlength="80" id="buser" name="buser" value="{$bname}"/>
                                <input type="hidden" name="sbForm" value="1" />
                        		</form>
                            </span>
                            <a class="standardButton" onclick="document.bForm.submit();" href="#"><span>{$lang255}</span></a>
                            <div class="divider"> </div>
                            <div class="blocking">
                                <div class="heading">{$lang256}:</div>
                                {if $b|@count eq "0"}
                                <span class="label" id="noneBlocked" >{$lang257}</span>
                                {else}
                                <div id="blockedUsers">
                                	{section name=i loop=$b}
                                    <div class="blockedUser" id="div_rondello9">
                                    	<div class="floatLeft">
                                        	<a href="{$baseurl}/profile/{$b[i].BID}/{$b[i].username|stripslashes}">{$b[i].username|stripslashes}</a>
                                        </div>
                                        <div class="floatRight">
                                        	<a href="{$baseurl}/privacy.php?ub={$b[i].BID}">{$lang258}</a>
                                        </div>
                                    </div>
                                    {/section}
                                </div>
                                {/if}
                            </div>
                        </div>
                    </div>
                </div>

                    </div>
                </div>
