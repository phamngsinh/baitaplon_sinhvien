				<div id="bricks">
    				<h2 class="viewall withPaging" style="width: 600px;">
                        <div class="pagingBlock">
                            {$pagelinks}
                        </div>
                        {$lang297}
                    </h2>
                    
                    <div class="timeheading">{if $m eq "0"}{$lang299}{elseif $m eq "1"}{$lang300}{elseif $m eq "2"}{$lang301}{elseif $m eq "3"}{$lang302}{/if}</div>
    
				    <ul class="viewallBricks">
                        <li style="margin-bottom: 15px;">                        
                        </li>
            
                        {section name=i loop=$posts}
                        {insert name=get_propic2 value=var assign=propic USERID=$posts[i].USERID}
                        <li>
                        	<div class="userBrick">
                            	<div class="userBrick_top"></div>
                            	<div class="userBrick_content">
                                	<div class="brick_preview">
                                        <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                            <tr><td align="center" style="vertical-align:middle"><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}" title="{$posts[i].username|stripslashes}"><img src="{$membersprofilepicurl}/thumbs/{$propic}" /></a></td></tr>
                                        </table> 
	                               	</div>
									<div class="userInfo">
                                        <div class="title">
                                          <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}">{$posts[i].username|stripslashes}</a>
                                        </div>
                                    	<div class="motto">{if $posts[i].saying ne ""}"{$posts[i].saying|stripslashes}"{/if}</div> 
	                                    <div class="infoline">
                                            <div class="floatLeft">{if $posts[i].city ne ""}{$posts[i].city|stripslashes}, {/if}{$posts[i].country|stripslashes}</div>
                                            {if $posts[i].city ne "" OR $posts[i].country ne ""}<div class="pipe">&nbsp;</div>{/if}<a href="{$baseurl}/viewfollowers.php?id={$posts[i].USERID}"><img src="{$imageurl}/icon_friends.gif" width="18" height="13" alt="{$lang118}" title="{$lang118}" class="friendIcon"/></a></div>
                                        </div>
                                        {insert name=get_lastupdate value=var assign=up USERID=$posts[i].USERID}
                                        {if $up[0].ID ne ""}
                                        <div class="latestPost">
                                            <div class="latestPost_content">
                                            	{if $up[0].pic ne ""}
                                            	<div class="latestPost_icon outline">
                                                    <div class="tinyuser">
                                                    <a href="{$baseurl}/viewupdate.php?id={$up[0].ID}" title="{$up[0].msg|stripslashes|truncate:75:"...":true}"><img src="{$tpicurl}/small_{$up[0].pic}" /></a>
                                                    </div>
                                                </div>
												{/if}
                                                {if $posts[i].public eq "0"}
                                                <div class="latestPost_title">{$lang263}.</div>
                                                {else}
                                                <div class="latestPost_title"><a href="{$baseurl}/viewupdate.php?id={$up[0].ID}">{$up[0].msg|stripslashes|truncate:75:"...":true}</a></div>
                                                <div class="byline">{insert name=get_time_to_days_ago value=a time=$up[0].time_added}</div>
                                                {/if}
                                            </div>
                                        </div>
                                        {else}
                                        <div class="latestPostDefault">
                                            <div class="latestPost_content">
                                                <div class="latestPost_titleDefault">
                                                    {$lang306}
                                                </div> 
                                            </div>
                                        </div>
                                        {/if}
                                    </div>
			                </div>
            	        </li>
   						{/section}
    				</ul>

    				<div class="pagingBlock">
        				{$pagelinks}
                    </div>
                
                </div>

				<div id="rightPanel">
    				<div id="filter">
                        <ul>
                        	{if $m eq "0"}
                            <li value="0" class="selected selectedfirst"><h2>{$lang299}</h2></li>
                            {else}
                            <li value="0" class="btm"><a href="{$baseurl}/newmembers.php?m=0">{$lang299}</a></li>
                            {/if}
                            {if $m eq "1"}
                            <li value="1" class="selected selectedfirst"><h2>{$lang300}</h2></li>
                            {else}
                            <li value="1" class="btm"><a href="{$baseurl}/newmembers.php?m=1">{$lang300}</a></li>
                            {/if}
                            {if $m eq "2"}
                            <li value="2" class="selected selectedfirst"><h2>{$lang301}</h2></li>
                            {else}
                            <li value="2" class="btm"><a href="{$baseurl}/newmembers.php?m=2">{$lang301}</a></li>
                            {/if}
                            {if $m eq "3"}
                            <li value="3" class="selected selectedfirst"><h2>{$lang302}</h2></li>
                            {else}
                            <li value="3" class="btm"><a href="{$baseurl}/newmembers.php?m=3">{$lang302}</a></li>
                            {/if}
                        </ul>
                    </div>
    
    				<div class="divider dividerFilterAd"></div>

                    <div class="adArea">
                        {insert name=get_advertisement AID=1}
                    </div>
				</div>

            </div>
		</div>