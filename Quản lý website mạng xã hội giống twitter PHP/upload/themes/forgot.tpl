                    <div id="left_col" style="width:620px; float:left; margin:10px 0px 0px 0px; padding:0px; display:inline; background:#fff;">
                        <h2 style="font-size: 24px; color: #f33408; margin-left: 20px;">{$lang324}</h2>
                        
                        <h2 style="font-size: 16px; color: #999999; margin-left: 20px;">{$lang325}</h2>
                        
                        <div id="processPage">
                        <div id="columnLeft" style="margin-bottom: 30px;">
                        	<form id="forgotform" name="forgotform" action="{$baseurl}/forgot.php" method="post">
                    
                            <div id="regForm">                            
                                <div class="clear"></div>
        						
                                {if $error ne ""}
                                <div id="" class="error " style="">*** {$error} ***</div>
                                {/if}
                                <div class="label">
                                    <div>
                                        <div class="floatLeft">{$lang4}</div>
                                        <div id="usernameStatus" style="display: none;"></div>
                                    </div>
                                    <input id="username" name="username" class="text" type="text" value="{$username|stripslashes}" maxlength="20"/>
                                </div>
                                
                                <div class="clear" style="padding-bottom:10px;"></div>
                                <div class="label">
                                    <div>{$lang8}</div>
                                    <input id="email" name="email" class="text" type="text" value="{$email|stripslashes}" maxlength="40"/>
                                </div>
                                            
                                <div class="clear"></div>
                                        
                            </div>
                            
                            <div class="clear"></div>
                            <div class="divider"></div>
                                
                            <a id="submitButton" class="yellowButton" onclick="document.forgotform.submit();"><span>{$lang60}</span></a>

                            <input type="hidden" name="forgot" value="1" /> 
                            </form>
                        </div>
                    </div>
                    </div>
                    <div id="right_col" style="position:relative; display:inline; float:right; width:328px; margin:10px 0px 50px 0px;">
                        <div id="ads">
                            <div class="adtext">{$lang76}</div>
                            {insert name=get_advertisement AID=1}
                        </div>
                    </div>

                </div>
            </div>