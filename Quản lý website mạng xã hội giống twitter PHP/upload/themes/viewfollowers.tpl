				{if $smarty.session.USERID eq ""}
                <div id="alertBox">
                    <div class="top">
                        <div class="leftColumn">
                            <div class="profilePic"><img src="{insert name=get_propic value=var assign=propic1 USERID=$p.USERID}{$membersprofilepicurl}/thumbs/{$propic1}" alt="" /></div>
                            <div class="content withProfile">
                                <div class="heading">{$p.username|stripslashes} {$lang121}.</div>
                                {$lang122}<br />
                        		<a href="{$baseurl}/login.php">{$lang123}</a>
                            </div>
                        </div>
                        <div class="rightColumn">
                        	<a href="{$baseurl}/login.php" class="yellowButton"><span>{$lang0}</span></a>                                
                        </div>
                    </div>
                    <div class="bottom"></div>
                </div>
				{/if}
                
				<div id="bricks">
    				<h2 class="viewall withPaging" style="width: 600px;">
                        <div class="pagingBlock">
                            {$pagelinks}
                        </div>
                        <span>{$p.username|stripslashes}'s</span> {$lang118} ({$total})
                    </h2>
    
				    <ul class="viewallBricks">
                        <li style="margin-bottom: 15px;">                        
                        </li>
            
                        {section name=i loop=$posts}
                        {insert name=get_propic2 value=var assign=propic USERID=$posts[i].USERID}
                        <li>
                        	<div class="userBrick">
                            	<div class="userBrick_top"></div>
                            	<div class="userBrick_content">
                                	<div class="brick_preview">
                                        <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                            <tr><td align="center" style="vertical-align:middle"><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}" title="{$posts[i].username|stripslashes}"><img src="{$membersprofilepicurl}/thumbs/{$propic}" /></a></td></tr>
                                        </table> 
	                               	</div>
									<div class="userInfo">
                                        <div class="title">
                                          <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}">{$posts[i].username|stripslashes}</a>
                                        </div>
                                    	<div class="motto">{if $posts[i].saying ne ""}"{$posts[i].saying|stripslashes}"{/if}</div> 
	                                    <div class="infoline">
                                            <div class="floatLeft">{if $posts[i].city ne ""}{$posts[i].city|stripslashes}, {/if}{$posts[i].country|stripslashes}</div>
                                            <div class="pipe">&nbsp;</div><img src="{$imageurl}/icon_friends.gif" width="18" height="13" alt="{$lang136}" title="{$lang136}" class="friendIcon"/></div>
                                        </div>
                                        {insert name=get_lastupdate value=var assign=up USERID=$posts[i].USERID}
                                        {if $up[0].ID ne ""}
                                        <div class="latestPost">
                                            <div class="latestPost_content">
                                            	{if $up[0].pic ne ""}
                                            	<div class="latestPost_icon outline">
                                                    <div class="tinyuser">
                                                    <a href="{$baseurl}/viewupdate.php?id={$up[0].ID}" title="{$up[0].msg|stripslashes|truncate:75:"...":true}"><img src="{$tpicurl}/small_{$up[0].pic}" /></a>
                                                    </div>
                                                </div>
												{/if}
                                                {if $posts[i].public eq "0"}
                                                <div class="latestPost_title">{$lang263}.</div>
                                                {else}
                                                <div class="latestPost_title"><a href="{$baseurl}/viewupdate.php?id={$up[0].ID}">{$up[0].msg|stripslashes|truncate:75:"...":true}</a></div>
                                                <div class="byline">{insert name=get_time_to_days_ago value=a time=$up[0].time_added}</div>
                                                {/if}
                                            </div>
                                        </div>
                                        {else}
                                        <div class="latestPostDefault">
                                            <div class="latestPost_content">
                                                <div class="latestPost_titleDefault">
                                                    {$lang306}
                                                </div> 
                                            </div>
                                        </div>
                                        {/if}
                                    </div>
			                </div>
            	        </li>
   						{/section}
    				</ul>

    				<div class="pagingBlock">
        				{$pagelinks}
                    </div>
                
                </div>

				<div id="rightPanel">
    				<div id="filter" class="viewAll">
                        <h2>{$lang137} ({$total}):</h2>
                        <select onchange="window.location.href=this.options[this.selectedIndex].value">
                            <option value="">{$lang138}</option>
                            <option value="{$baseurl}/viewfollowers.php?id={$id}&page={$page}&sort=alpha" {if $smarty.request.sort eq "alpha" OR $smarty.request.sort eq ""}selected="selected"{/if}>{$lang140}</option>
                            <option value="{$baseurl}/viewfollowers.php?id={$id}&page={$page}&sort=date_added" {if $smarty.request.sort eq "date_added"}selected="selected"{/if}>{$lang139}</option>
                        </select>
    				</div>

                    <div class="divider"></div>
                    
                    {insert name=get_propic2 value=var assign=mypropic USERID=$p.USERID}
                    <div class="contentBlock" style="margin-bottom: 10px;">
                        <h2 class="viewAll"><div class="userName"><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$p.username|stripslashes}">{$p.username|stripslashes}</a></div></h2>
                        <div class="view"><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$p.username|stripslashes}">{$lang135}</a>&nbsp;<img src="{$imageurl}/arrow.gif" width="3" height="6" alt=""/></div>
                        <div class="clear">&nbsp;</div>
                        <div class="userPicPreview">
                            <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                <tr><td align="center" style="vertical-align:middle"><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$p.username|stripslashes}"><img src="{$membersprofilepicurl}/thumbs/{$mypropic}" /></a></td></tr>
                            </table>
                        </div>
                        <div class="userPreviewInfo">
                            <div class="motto">{if $p.saying ne ""}"{$p.saying|stripslashes}"{/if}</div>
                            {if $p.gender ne ""}{if $p.gender eq "1"}{$lang126}{elseif $p.gender eq "0"}{$lang127}{/if}<br />{/if}
			            </div>
        			</div>
    
    				<div class="divider dividerFilterAd"></div>

                    <div class="adArea">
                        {insert name=get_advertisement AID=1}
                    </div>
				</div>

            </div>
		</div>