			<div id="footer">
                <div id="links" class="longer">
                	<div style="color:#FFFFFF">
                        <a style="color:#FFFFFF; font-weight: bold" href="http://www.scritterscript.com" target="_blank">Powered by Scritter Script</a>
                    </div>
                	
                    <div>
                        <a href="{$baseurl}/contact_us.php">{$lang336}</a> |    
                        <a href="{$baseurl}/about_us.php">{$lang335}</a>
                    </div>
                    <div>
                        <a href="{$baseurl}/privacy_policy.php">{$lang334}</a> |
                        <a href="{$baseurl}/terms_of_use.php">{$lang333}</a>
                    </div>
                    <div>
                    	<form name="langselect" id="langselect" method="post">                        
                            <select name="language" onChange="document.langselect.submit()" style="font-family: Tahoma, Verdana; font-size: 11px"> 
                                <option value="english" {if $smarty.session.language eq "english" OR $smarty.session.language eq ""}selected{/if} >{$lang339|utf8_encode}</option> 
                                <option value="spanish" {if $smarty.session.language eq "spanish"}selected{/if}>{if $smarty.session.language eq "english" OR $smarty.session.language eq ""}{$lang340|utf8_encode}{else}{$lang340}{/if}</option> 
                                <option value="french" {if $smarty.session.language eq "french"}selected{/if}>{if $smarty.session.language eq "english" OR $smarty.session.language eq ""}{$lang341|utf8_encode}{else}{$lang341}{/if}</option> 
                            </select>
                        </form>
                    </div>
                </div>
                
                <div id="copyright">{$lang332} &copy; 2012 {$short_name}</div>
    		</div>       
    	</div>
    </div>
</body>
</html>