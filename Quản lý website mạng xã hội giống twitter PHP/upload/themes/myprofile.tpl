				<div id="pageHeader">
    				<div class="floatLeft">{$lang148}</div>
					<div class="headerTabs">            
						<div class="tabSelected"><span>{$lang147}</span></div>
						<a href="{$baseurl}/design.php" class="tab"><span>{$lang149}</span></a>
						<a href="{$baseurl}/privacy.php" class="tab"><span>{$lang150}</span></a>
						<a href="{$baseurl}/alerts.php" class="tab"><span>{$lang151}</span></a>
					</div>
				</div>

				<div id="processPage">
				    <div id="columnLeft">
					
						{if $error eq "1"}
						<div id="" class="error mainError" style="">*** {$lang152} ***</div>  
                        {elseif $msg ne ""}
						<div id="" class="error mainError" style="">*** {$msg} ***</div>  
						{/if}
					
						<h2>{$lang153}</h2>
						<div class="profilePicPreview">
						   <table width="100%" height="100%" cellpadding="0" cellspacing="0">
								<tr><td align="center" style="vertical-align:middle"><img src="{insert name=get_propic2 value=var assign=propic1 USERID=$p.USERID}{$membersprofilepicurl}/thumbs/{$propic1}" /></td></tr>
						   </table>
						</div>
						<form id="picform" name="picform" action="{$baseurl}/myprofile.php" enctype="multipart/form-data" method="post">
						<p style="margin-bottom:7px;">
							<span class="profileName">{$p.username|stripslashes}</span><br />
							<input type="file" name="ppic" id="ppic" /><div class="divider" style="margin-bottom:10px;"></div><a href="#" class="standardButton" style="margin-right:4px;" onclick="document.picform.submit();"><span>{$lang154}</span></a>
						</p>
                        <input type="hidden" name="spicform" value="1" />
						</form>
						
						<div class="divider" style="margin-bottom:10px;"></div>
						
						<form id="aboutform" name="aboutform" action="{$baseurl}/myprofile.php" method="post">
						<p class="label wide">{$lang155}
							<input id="saying" name="saying" class="userVoice text" style="width:100%" type="text" value="{$p.saying|stripslashes}" maxlength="50"/>
						</p>
						
						<p class="label wide">{$lang156}
							<textarea id="interests" name="interests" class="userVoice" style="width: 100%;" rows="3">{$p.interests|stripslashes}</textarea>
						</p>						
						
						<p class="label wide">{$lang157}
							<input id="website" name="website" class="userVoice text" style="width:100%" type="text" value="{$p.website|stripslashes}"/>
						</p>
                        <input type="hidden" name="saboutform" value="1" />
						</form>
						
						<div class="divider" style="margin-bottom:4px;"></div>

						<div class="buttonArea">
						  <a href="#" class="standardButton" style="margin-right:4px;" onclick="document.aboutform.submit();"><span>{$lang158}</span></a>
						  <a href="{$baseurl}/myprofile.php" class="standardButton"><span>{$lang159}</span></a>
						</div>
						
						<h2>{$lang160}</h2>
                        
                        <form id="persform" name="persform" action="{$baseurl}/myprofile.php" method="post">
						<p class="label">{$lang161}
						  <input id="firstName" name="firstName" class="userVoice text" type="text" value="{$p.firstname|stripslashes}"/>
                        </p>
						
						<p class="label">{$lang162}
						  <input id="lastName" name="lastName" class="userVoice text" type="text" value="{$p.lastname|stripslashes}"/>
                        </p>						
						
						<p class="label">{$lang163}
						  <input id="city" name="city" class="userVoice text" type="text" value="{$p.city|stripslashes} "/>
                        </p>
                        
                        <div class="divider" style="margin-bottom:0px;"></div>

						<p class="label select">{$lang164}
						  <select id="country" name="country">
							<option value="">-</option>
                            {$country}
						  </select>
						</p>
                        
                        <div class="divider" style="margin-bottom:0px;"></div>
          
						<p class="label" style="margin-bottom: 3px;">{$lang10}<br/>
                          <select id="day" name="day">
							<option value="">-</option>
							{$bdays}
						  </select>
						  <select id="month" name="month" style="width: 90px;">
							<option value="">-</option>
                            {$bmonths}
						  </select>
						  <select id="year" name="year">
							<option value="">-</option>
							{$byears}                
						  </select>
                          
						  <div class="clear" style="margin-bottom: 0px;"></div>
						  
						  <input id="showAge" name="showAge" class="radio" style="margin-top: 7px; width: 15px; clear:both;" type="checkbox" value="1" {if $p.showAge eq "1"}checked="checked"{/if}/><label style="margin-top: 7px;" class="radioLabel" for="showAge">{$lang177}</label>
						</p>
						
						<div class="clear" style="margin-bottom: 15px;"></div>
                        
						<p class="label wide">{$lang125}<br/>
						  <input id="gender" name="gender" class="radio" style="width: 15px;" type="radio" value="1" {if $p.gender eq "1"}checked="checked"{/if}/>
						  <label style="margin-right: 30px;" class="radioLabel" for="gender1">{$lang126}</label>
						
						  <input id="gender" name="gender" class="radio" style="width: 15px;" type="radio" value="0" {if $p.gender eq "0"}checked="checked"{/if}/>
						  <label style="margin-right: 30px;" class="radioLabel" for="gender2">{$lang127}</label>
                          
						  <input id="gender" name="gender" class="radio" style="width: 15px;" type="radio" value="" {if $p.gender eq ""}checked="checked"{/if}/>
						  <label class="radioLabel" for="gender3">{$lang178}</label>
						</p>
          
		  				<div style="clear:both;">
                        	{if $perror ne ""}
							<div id="" class="error error errorField" style="">*** {$perror} ***</div>
							{/if}
							<p class="label">{$lang179}<br/>
								<input id="mypassword1" name="mypassword1" class="userVoice text" type="password" value=""/>
                            </p>
							<p class="label">{$lang180}<br/>
								<input id="mypassword2" name="mypassword2" class="userVoice text" type="password" value=""/>
                            </p>
						</div>
                        <input type="hidden" name="spersform" value="1" />
                        </form>
						
						<div class="divider" style="margin-bottom:4px;"></div>
						<div class="buttonArea">
						  <a href="#" class="standardButton" style="margin-right:5px;" onclick="document.persform.submit();"><span>{$lang158}</span></a>
						  <a href="{$baseurl}/myprofile.php" class="standardButton"><span>{$lang159}</span></a>
						</div>
						
						<a name="contactInfo"></a>
						
						<h2>{$lang181}</h2>
                        <form id="eform" name="eform" action="{$baseurl}/myprofile.php" method="post">
						<div>
                          {if $merror ne ""}
							<div id="" class="error error errorField" style="">*** {$merror} ***</div>
						  {/if}
						  <p class="black">{$lang182}<br/>
						  	{$lang183}
                          </p>
						  
						  <p class="label">{$lang184} {if $p.verified ne "1"}<span style="color:#F33408;">({$lang185})</span>{/if}
						  	<input id="email" name="email" class="userVoice text" style="width:80%;margin-right:10px;" type="text" value="{$p.email|stripslashes}"/>
                            {if $p.verified ne "1"}<a href="{$baseurl}/myprofile.php?r=1">{$lang186}</a>{/if}
                          </p>
						  <div class="clear"></div>						  
						</div>
                        <input type="hidden" name="seform" value="1" />
                        </form>
						
						<div class="divider" style="margin-bottom:4px;"></div>
						
						<div class="buttonArea">
						  <a href="#" class="standardButton" style="margin-right:4px;" onclick="document.eform.submit();"><span>{$lang158}</span></a>
						  <a href="{$baseurl}/myprofile.php" class="standardButton"><span>{$lang159}</span></a>
						</div>
												
					</div>
				</div>

                </div>
                </div>