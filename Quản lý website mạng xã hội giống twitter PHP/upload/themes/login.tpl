                    <div id="left_col" style="width:620px; float:left; margin:10px 0px 0px 0px; padding:0px; display:inline; background:#fff;">
                        <h2 style="font-size: 24px; color: #f33408; margin-left: 20px;">{$lang75}</h2>
                        <div id="processPage">
                        <div id="columnLeft" style="margin-bottom: 30px;">
                        	<form id="loginCommand" name="loginCommand" action="{$baseurl}/login.php" method="post">
                    
                            <div id="regForm">                            
                                <div class="clear"></div>
        						
                                {if $error ne ""}
                                <div id="" class="error " style="">*** {$error} ***</div>
                                {/if}
                                <div class="label">
                                    <div>
                                        <div class="floatLeft">{$lang4}</div>
                                        <div id="usernameStatus" style="display: none;"></div>
                                    </div>
                                    <input id="username" name="username" class="text" type="text" value="{$username|stripslashes}" maxlength="20"/>
                                </div>
                                
                                <div class="clear"></div>
                                <div class="label">
                                    <div>{$lang6}</div>
                                    <input id="password1" name="password1" class="text" type="password" value="{$pw|stripslashes}" maxlength="20"/>
                                </div>
                                            
                                <div class="clear"></div>
                                
                                <a href="{$baseurl}/forgot.php">{$lang73}</a>
                                        
                            </div>
                            
                            <div class="clear"></div>
                            <div class="divider"></div>
                                
                            <a id="submitButton" class="yellowButton" onclick="document.loginCommand.submit();"><span>{$lang60}</span></a>
							<input type="hidden" name="redirect" value="{$r}" />
                            <input type="hidden" name="login" value="1" /> 
                            </form>
                        </div>
                    </div>
                    </div>
                    <div id="right_col" style="position:relative; display:inline; float:right; width:328px; margin:10px 0px 50px 0px;">
                        <div id="ads">
                            <div class="adtext">{$lang76}</div>
                            {insert name=get_advertisement AID=1}
                        </div>
                    </div>

                </div>
            </div>