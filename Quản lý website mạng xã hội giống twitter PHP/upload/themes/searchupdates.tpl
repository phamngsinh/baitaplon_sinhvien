				<div id="bricks">
    				<h2 class="viewall withPaging" style="width: 600px;">
                        <div class="pagingBlock">
                            {$pagelinks}
                        </div>
                        {$total} {$lang309} <font color="#dfdfdf">|</font> {$lang308} "{$query}"
                    </h2>
                        
				    <ul class="viewallBricks">
                        <li style="margin-bottom: 15px;">
                        </li>
                 		{section name=i loop=$posts}
                 		{insert name=get_propic1 value=var assign=propic USERID=$posts[i].USERID}
                        <li class="first">
                            <div class="tinyOutline">
                                <div class="tinyuser">
                                <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}"><img src="{$membersprofilepicurl}/thumbs/{$propic}" class="profilepic-{$posts[i].username|stripslashes}" alt="{$posts[i].username|stripslashes}" title="{$posts[i].username|stripslashes}" /></a>
                                </div>
                            </div>
                        </li>
                        <li>
                        <div class="brick">
                            <div class="brick_top"></div>
                            <div class="brick_content"> 
                            	{if $posts[i].pic ne ""}
                                <div class="brick_preview">
                                    <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                        <tr><td align="center" style="vertical-align:middle"><a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}"><img src="{$tpicurl}/{$posts[i].pic}" /></a></td></tr>
                                    </table> 
                                </div>
                                {/if}  
                                <div class="brick_title" id="content-{$posts[i].ID}">{insert name=get_post value=a assign=mpost post=$posts[i].msg}{$mpost|stripslashes}{if $posts[i].pic eq ""} {if $posts[i].type eq "update"}<a class="moreLink" href="{$baseurl}/viewupdate.php?id={$posts[i].ID}">[{$lang94}]</a>{/if}{/if}</div>
                                <div class="brick_byline">    
                                    {$lang343} <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}">{$posts[i].username|stripslashes}</a>, <a class="link" href="{$baseurl}/viewupdate.php?id={$posts[i].ID}">{insert name=get_time_to_days_ago value=a time=$posts[i].time_added}{if $posts[i].edited ne ""}&nbsp;&nbsp;&nbsp;<span class="edittime">[{$lang112}:{insert name=get_time_to_days_ago value=a time=$posts[i].edited}]</span>{/if}</a>
                                </div>     
                            </div>
                            <div class="brick_btm">
                                <div class="brick_actions">   
                                    <a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}#comment" class="comment_num">{insert name=com_count value=a assign=comco ID=$posts[i].ID}{$comco}</a><a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}#comment" class="commentIcon{if $comco GT "0"}{insert name=is_new_cpost value=a assign=ncpost id=$posts[i].ID}{if $ncpost eq "1"}WhatsNew{/if}{/if}"></a>
                                	<img src="{$imageurl}/icon_divider_blue.gif" width="1" height="15" alt="" class="brick_icon_divider"/><a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}&r=@{$posts[i].username|stripslashes}:" class="requiresLogin">{$lang97}</a>
                                </div>
                            </div>
                        </div>
                        </li>
                		{/section}
                    </ul>

    				<div class="pagingBlock">
        				{$pagelinks}
                    </div>
                
                </div>

				<div id="rightPanel" class="search">
    				<div class="seeMore">
                        <a href="{$baseurl}/searchchannels.php?query={$query}">{$lang316} "{$query}"</a>
                    </div>
            
                    <div class="divider"></div>
                                        
                    <h2>{$lang317}</h2>
                    <div class="options multiple" style="padding-bottom:10px;">
                    	<form method="post" name="filterupdates" id="filterupdates" action="{$baseurl}/searchupdates.php?query={$query}">
                        <div><input style="margin-left: 0px; vertical-align: middle;" type="checkbox" name="si" value="1" {if $si eq "1"}checked{/if}/><label for="si">{$lang303}</label></div>
                        <div><input style="margin-left: 0px; vertical-align: middle;" type="checkbox" name="st" value="1" {if $st eq "1"}checked{/if}/><label for="st">{$lang304}</label></div>
                        <input type="hidden" name="sfilterupdates" value="1" />
                        </form>
                        <a href="#" onclick="document.filterupdates.submit()" class="standardButton update"><span>{$lang305}</span></a>
                    </div>
    
    				<div class="divider dividerFilterAd"></div>

                    <div class="adArea">
                        {insert name=get_advertisement AID=1}
                    </div>
				</div>

            </div>
		</div>