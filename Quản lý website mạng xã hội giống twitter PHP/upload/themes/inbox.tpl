                <div id="bricks">
                    <h2 id="inboxCount" class="viewall">
                        {$lang217} (<span id="newMessages">{$newmail}</span>)<div class="more" style="margin-left:5px;">| <span id="totalMessages">{$total}</span> {$lang231}</div>
                    </h2>
                </div>
                <div id="inbox">
                    <div id="sortby">
                        <div class="sorter">
                        	{if $o eq "a"}
                        	<a href="{$baseurl}/inbox.php?s={$s}&page={$page}&o=z" class="arrowUp" id="arrowUp"></a>
                            {elseif $o eq "z"}
                            <a href="{$baseurl}/inbox.php?s={$s}&page={$page}&o=a" class="arrowDn" id="arrowDown"></a>
                            {/if}
                        </div> 
                        <p id="sortLinks"><span>sort by:&nbsp;</span>
                        	{if $s eq "r" OR $s eq ""}
                            <span class="sortLabel RECENT" id="sortRecentLabel">{$lang232}</span>,&nbsp;
                            {else}
                            <a class="sortLink RECENT" href="{$baseurl}/inbox.php?v={$v}&s=r&page={$page}&o={$o}" id="sortRecentLink">{$lang232}</a>,&nbsp;
                            {/if}
                            {if $s eq "u"}
                            <span class="sortLabel UNREAD" id="sortReadLabel">{$lang233}</span>,&nbsp;
                            {else}
                            <a class="sortLink UNREAD" href="{$baseurl}/inbox.php?v={$v}&s=u&page={$page}&o={$o}" id="sortReadLink">{$lang233}</a>,&nbsp;
                            {/if}
                            {if $s eq "s"}
                            <span class="sortLabel SENDER" id="sortSenderLabel">{$lang234}</span>,&nbsp;
                            {else}
                            <a class="sortLink SENDER" href="{$baseurl}/inbox.php?v={$v}&s=s&page={$page}&o={$o}" id="sortSenderLink">{$lang234}</a>,&nbsp;
                            {/if}
                            {if $s eq "t"}
                            <span class="sortLabel TYPE" id="sortTypeLabel">{$lang235}</span>,&nbsp;
                            {else}
                            <a class="sortLink TYPE" href="{$baseurl}/inbox.php?v={$v}&s=t&page={$page}&o={$o}" id="sortTypeLink">{$lang235}</a>
                            {/if}
                        </p>
                    </div>
                    
                    {if $msg ne ""}
                    <div id="" class="error mainError" style=" padding-bottom:10px;">*** {$msg} ***</div>
                    {/if}
                    
                    <div id="preview">
                        <ul id="userConversations">
                        	{section name=i loop=$m}
                            <li {if $m[i].MID eq $n[0].MID}class="selected"{/if} id="{$m[i].MID}">
                            	<a class="msg" href="{$baseurl}/inbox.php?v={$m[i].MID}&s={$s}&page={$page}&o={$o}"></a>
                                <div class="status {if $m[i].unread eq "1"}unread{else}read{/if}"></div>
                                <div class="byline">{$lang99} {if $m[i].type eq "afr"}{$lang221}{else}{if $m[i].USERID GT "0"}<a href="{$baseurl}/{insert name=get_seo_profile value=a username=$m[i].username|stripslashes}">{$m[i].username|stripslashes}</a>{else}{$m[i].username|stripslashes}{/if}{/if}, {insert name=get_time_to_days_ago value=a time=$m[i].time}</div>
                                <div class="msgPreview"><span class="msgPreviewTrunc"><span style="color: rgb(0, 0, 0);" class="trunc">{if $m[i].type eq "afr"}{insert name=trimme value=var limit=25 w=$m[i].username|stripslashes w2=$lang219}{elseif $m[i].type eq "fr"}{$lang218} {$m[i].username|stripslashes}{else}{$m[i].subject|stripslashes|truncate:20:"...":true}{/if}</span></span></div>
                            </li>
                            {/section}
                        </ul>
                        
                        <p>&nbsp;</p>
                        
                        <div id="conversationPaging" class="pagingBlock" style="margin: 0px; float: left;">
                        	{$pagelinks}
                        </div>
                        
                    </div>
                    <div id="thread">
                    	<form id="delform" name="delform" action="{$baseurl}/inbox.php?s={$s}&page={$page}&o={$o}" method="post">
                        <input type="hidden" name="FID" value="{$n[0].MSGFROM}" />
                        <input type="hidden" name="MID" value="{$n[0].MID}" />
                        <input type="hidden" name="sdelform" value="1" />
                        </form>
                        <a href="#" onclick="document.delform.submit();" class="action standardButton" style="margin-top:4px;"><span>{$lang96}</span></a>
                        <div id="threadTitle">
                        	{if $n[0].type ne "afr"}
                            <div id="profilePic" class="msgUser tinyOutline">
                                <div class="tinyuser"><a id="senderProfilePic" href="{$baseurl}/{insert name=get_seo_profile value=a username={$n[0].username|stripslashes}"><img id="senderProfilePicImg" src="{insert name=get_propic1 value=var assign=mypropic USERID=$n[0].MSGFROM}{$membersprofilepicurl}/thumbs/{$mypropic}" /></a></div>
                            </div>
                            {/if}
                            <h2 id="discussionTitle">{$lang220} {if $n[0].type eq "afr"}{$lang221}{else}{$n[0].username|stripslashes}{/if}</h2>
                        </div>
                        <div id="threadBody">
                            <div id="threadSubject" {if $n[0].type ne "msg"}style="display: none;"{/if}>
                                <div class="content">
                                    <div class="re">{$n[0].subject|stripslashes}</div>
                                    <div id="mediaPreview" class="re tinyOutline">
                                        <div id="headerThumbnail" class="tinyuser"></div>
                                    </div>
                                    <div class="re" id="parentHeader"><span id="parentBody" class="headerPreviewSpan"></span><br>
                                        <span id="parentByline" class="byline">{$lang99} {$n[0].username|stripslashes}, {insert name=get_time_to_days_ago value=a time=$n[0].time}</span>
                                    </div>
                                </div>
                            </div>
                            {if $n[0].type eq "msg"}
                            <div id="sendMsg">                              
                            	<form id="reform" name="reform" action="{$baseurl}/inbox.php?v={$v}&s={$s}&page={$page}&o={$o}" method="post">  
                                <textarea id="replytext" name="replytext" class="msgBox" style="width:385px;" cols="30" rows="2"></textarea>
                                <input type="hidden" name="FID" value="{$n[0].MSGFROM}" />
                                <input type="hidden" name="sreform" value="1" />
                                </form>
                                <br />
                                <br />
                                <a href="#" class="action standardButton" onclick="document.reform.submit();"><span>{$lang97}</span></a>
                            </div>
                            {/if}
                            <ul id="messages">
                                <li class="first" id="msg_12122478">
                                	<span>{$lang99} {if $n[0].type eq "afr"}{$lang221}{else}{$n[0].username|stripslashes}{/if}, {insert name=get_time_to_days_ago value=a time=$n[0].time}</span>
                                    {if $n[0].type eq "afr"}
                                    <p id="msgbody_12122478"><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$n[0].username|stripslashes}">{$n[0].username|stripslashes}</a> {$lang222}. </p>
                                    {elseif $n[0].type eq "fr"}
                                    <p id="msgbody_12063620">{$lang218} <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$n[0].username|stripslashes}">{$n[0].username|stripslashes}</a></p>
                                    <form id="aform" name="aform" action="{$baseurl}/inbox.php?s={$s}&page={$page}&o={$o}" method="post">
                                    <input type="hidden" name="FID" value="{$n[0].MSGFROM}" />
                                    <input type="hidden" name="MID" value="{$n[0].MID}" />
                                    <input type="hidden" name="saform" value="1" />
                                    </form>
                                    <form id="rform" name="rform" action="{$baseurl}/inbox.php?s={$s}&page={$page}&o={$o}" method="post">
                                    <input type="hidden" name="FID" value="{$n[0].MSGFROM}" />
                                    <input type="hidden" name="MID" value="{$n[0].MID}" />
                                    <input type="hidden" name="srform" value="1" />
                                    </form>
                                    <p style="margin: 0px;">
                                    	<div class="inviteButtons">
                                        	<a href="#" onclick="document.aform.submit();" class="standardButton"><span>{$lang223}</span></a>
                                            <a href="#" onclick="document.rform.submit();" class="standardButton" style="margin-left: 10px;"><span>{$lang224}</span></a>
                                        </div>
                                    </p>
                                    {else}
                                    <p id="msgbody_12122478">{insert name=get_stripped_phrase value=a assign=mg details=$n[0].message}{$mg|stripslashes}</p>
                                    {/if}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> 
                <div id="verticalAd">
                	{insert name=get_advertisement AID=6}
                </div>
 
 
                    </div>
                </div>
