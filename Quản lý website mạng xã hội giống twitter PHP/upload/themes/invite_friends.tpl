                    <div id="pageHeader" class="regHeader">{$lang1}
                        <div class="steps">
                            <div class="step1"></div>
                            <div class="step2 selected"></div>
                        </div>
                    </div>                   
                                
                    <div id="processPage" class="findFriends">
                    <form id="findFriendsCommand" name="findFriendsCommand" action="{$baseurl}/invite_friends.php" method="post">
                        <div id="columnLeft" style="margin-bottom: 50px;">
                            <h2>{$lang50}</h2>  
                            
                            <div class="clear"></div>
                            {if $message ne ""}
                            <div id="" class="error mainError" style="">*** {$message} ***</div>
                            {/if}
                            <div class="clear"></div>
                                      
                            <div style="clear: left; float: left;">
                            	{if $err0 ne ""}
                                <div id="" class="error " style="">*** {$err0} ***</div>
                                {/if}
                            	<div style="margin-bottom: 10px;">
                                    <label for="emailAddress" class="emailFieldLabel">{$lang56}</label>
                                    <input id="name" name="name" class="emailField" type="text" value="{$smarty.session.USERNAME|stripslashes}"/>
                                </div>
                                {if $err1 ne ""}
                                <div id="" class="error " style="">*** {$err1} ***</div>
                                {/if}
                                <div style="margin-bottom: 10px;">
                                    <label for="emailAddress" class="emailFieldLabel">{$lang51}</label>
                                    <input id="friend_1" name="friend_1" class="emailField" type="text" value="{$friend_1}"/>
                                </div>
                                {if $err2 ne ""}
                                <div id="" class="error " style="">*** {$err2} ***</div>
                                {/if}
                                <div style="margin-bottom: 10px;">
                                    <label for="emailAddress" class="emailFieldLabel">{$lang52}</label>
                                    <input id="friend_1" name="friend_2" class="emailField" type="text" value="{$friend_2}"/>
                                </div>
                                {if $err3 ne ""}
                                <div id="" class="error " style="">*** {$err3} ***</div>
                                {/if}
                                <div style="margin-bottom: 10px;">
                                    <label for="emailAddress" class="emailFieldLabel">{$lang53}</label>
                                    <input id="friend_1" name="friend_3" class="emailField" type="text" value="{$friend_3}"/>
                                </div>
                                {if $err4 ne ""}
                                <div id="" class="error " style="">*** {$err4} ***</div>
                                {/if}
                                <div style="margin-bottom: 10px;">
                                    <label for="emailAddress" class="emailFieldLabel">{$lang54}</label>
                                    <input id="friend_1" name="friend_4" class="emailField" type="text" value="{$friend_4}"/>
                                </div>
                                {if $err5 ne ""}
                                <div id="" class="error " style="">*** {$err5} ***</div>
                                {/if}
                                <div style="margin-bottom: 10px;">
                                    <label for="emailAddress" class="emailFieldLabel">{$lang55}</label>
                                    <input id="friend_1" name="friend_5" class="emailField" type="text" value="{$friend_5}"/>
                                </div>
                                <div>
                                    <label for="emailPassword" class="emailFieldLabel">{$lang57}</label>
                                    <textarea name="message" id="message" rows="5" cols="36" ></textarea>
                                </div>
                                {if $err6 ne ""}
                                <div id="" class="error " style="">*** {$err6} ***</div>
                                {/if}
                                <div>
                                    <label for="verification" class="emailFieldLabel">{$lang30}</label>
                                    <input id="verification" name="verification" class="emailField" type="text" value=""/><br />
                                    <img src="{$baseurl}/include/captcha.php" />
                                </div>
                            </div>
                    
                            <div class="tipBubbleSmall">
                                <div><img src="{$imageurl}/icon_lock_safe.gif" width="11" height="14" alt="" />{$lang58}</div>
                                {$lang59}
                            </div>
                            
                            <div class="clear"></div>
                    
                            <div class="divider"></div>
                            <a href="#" id="submitButton" class="yellowButton" onclick="document.findFriendsCommand.submit();"><span>{$lang60}</span></a>
                            
                            <div class="skip"><a href="{$baseurl}/home.php">{$lang61} &raquo;</a></div>
                        </div>
                        <input type="hidden" name="invitesubmit" value="1" /> 
                    </form>
                    </div>
                </div>
            </div>