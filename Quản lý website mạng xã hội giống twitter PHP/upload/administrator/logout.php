<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("../include/config.php");
$_SESSION['ADMINID'] = "";
$_SESSION['ADMINUSERNAME'] = "";
$_SESSION['ADMINPASSWORD'] = "";
session_destroy();
header("location: index.php");
?>
