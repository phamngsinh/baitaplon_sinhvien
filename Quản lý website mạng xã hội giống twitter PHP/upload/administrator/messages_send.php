<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

function insert_get_all_members()
{
    global $config,$conn;
	$query = "select USERID,username from members WHERE USERID>0 order by username asc"; 
	$results = $conn->execute($query);
	$returnthis = $results->getrows();
	return $returnthis;
}

function insert_get_all_members2()
{
    global $config,$conn;
	$query = "select USERID,username from members order by username asc"; 
	$results = $conn->execute($query);
	$returnthis = $results->getrows();
	return $returnthis;
}

// Send Message Begin
function send_message($sendmessagesubjectz,$sendmessagetextz,$userid,$profileid)
{
	global $conn, $config, $lang;
    $sendmessagesubjectz=htmlentities(strip_tags($sendmessagesubjectz),ENT_QUOTES,'UTF-8');
	$sendmessagetextz=htmlentities(strip_tags($sendmessagetextz),ENT_QUOTES,'UTF-8');
	
	if (is_numeric($userid) && is_numeric($profileid))
	{
		$query="INSERT INTO messages_inbox SET MSGTO='".mysql_real_escape_string($profileid)."', MSGFROM='".mysql_real_escape_string($userid)."', subject='".mysql_real_escape_string($sendmessagesubjectz)."', message='".mysql_real_escape_string($sendmessagetextz)."', time='".time()."'";
		$conn->execute($query);
		
		if(mysql_affected_rows()>=1)
		{			
			$query="SELECT email,username FROM members WHERE USERID='".mysql_real_escape_string($userid)."'";
			$executequery=$conn->execute($query);
			$tempusername = $executequery->fields[username];

			$equery="SELECT username, email, alert_msg FROM members WHERE USERID='".mysql_real_escape_string($profileid)."' AND status='1'";
			$executequerye=$conn->execute($equery);
			$eusername = $executequerye->fields['username'];
			$sendto = $executequerye->fields['email'];
			$alert_msg = $executequerye->fields['alert_msg'];
			if($alert_msg == "1")
			{
				$sendername = $config['site_name'];
				$from = $config['site_email'];
				$subject = $lang['282'];
				$link = $config['baseurl']."/inbox.php";
				$sendmailbody = stripslashes($eusername).",<br><br>".stripslashes($tempusername)." ".$lang['283']."<br><br>".$lang['284']."<br><a href=\"$link\" target=\"_blank\">$link</a><br><br>".$lang['69'].",<br>".$sendername;
				mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
			}
		}
	}
}
// Send Message End

if($_POST['submitform'] == "1")
{
	$TOUSERID = intval($_REQUEST['TOUSERID']);	
	$FROMUSERID = intval($_REQUEST['FROMUSERID']);
	$subject = escape($_REQUEST['subject']);
	$msg = escape($_REQUEST['msg']);
	$active = $_REQUEST['active'];
	$verified = $_REQUEST['verified'];
	
	if ($subject == "")
	{
		$error = "Error: Please enter a subject.";
	}
	elseif ($msg == "")
	{
		$error = "Error: Please enter a message.";
	}
	
	if ($error == "")
	{
		if($TOUSERID == "0")
		{
			if($active != "")
			{
				$addtosql = " AND status='1'";
			}
			if($verified != "")
			{
				$addtosql .= " AND verified='1'";
			}
			
			$query="SELECT USERID FROM members WHERE USERID>'0' $addtosql";
			$results = $conn->execute($query);
			$returnthis = $results->getrows();
			$mtotal = count($returnthis);
			for($i=0;$i<$mtotal;$i++)
			{
				$sendtoid = $returnthis[$i]['USERID'];
				send_message($subject,$msg,$FROMUSERID,$sendtoid);
			}			
		}
		else
		{
			send_message($subject,$msg,$FROMUSERID,$TOUSERID);
		}
		
		$message = "Message Successfully Sent.";
		Stemplate::assign('message',$message);
	}
	else
	{
		Stemplate::assign('error',$error);
	}
	
}

$mainmenu = "9";
$submenu = "1";
Stemplate::assign('mainmenu',$mainmenu);
Stemplate::assign('submenu',$submenu);
STemplate::display("administrator/global_header.tpl");
STemplate::display("administrator/messages_send.tpl");
STemplate::display("administrator/global_footer.tpl");
?>