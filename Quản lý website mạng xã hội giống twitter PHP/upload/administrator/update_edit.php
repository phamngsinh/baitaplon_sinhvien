<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

$ID = intval($_REQUEST['ID']);

if($_POST['submitform'] == "1")
{
	if($ID > 0)
	{
		$arr = array("msg");
		foreach ($arr as $value)
		{
			$sql = "update posts set $value='".mysql_real_escape_string($_POST[$value])."' where ID='".mysql_real_escape_string($ID)."'";
			$conn->execute($sql);
		}
		$message = "Update Successfully Edited.";
		Stemplate::assign('message',$message);
	}
}

if($ID > 0)
{
	$query = $conn->execute("select A.ID, B.username, A.msg, A.time_added, A.edited, A.pic from posts A, members B where A.ID='".mysql_real_escape_string($ID)."' AND A.USERID=B.USERID limit 1");
	$p = $query->getrows();
	Stemplate::assign('p', $p[0]);
}

$mainmenu = "4";
$submenu = "1";
Stemplate::assign('mainmenu',$mainmenu);
Stemplate::assign('submenu',$submenu);
STemplate::display("administrator/global_header.tpl");
STemplate::display("administrator/update_edit.tpl");
STemplate::display("administrator/global_footer.tpl");
?>