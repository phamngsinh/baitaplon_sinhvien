<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();
$adminurl = $config['adminurl'];

// DELETE 
if($_REQUEST[delete]=="1")
{
	$DID = intval($_REQUEST['MID']);
	if($DID > 0)
	{		
		$query="DELETE FROM messages_inbox WHERE MID='".mysql_real_escape_string($DID)."' limit 1";
		$result=$conn->execute($query);
		$message = "Message Successfully Deleted.";
		Stemplate::assign('message',$message);
	}
}
// DELETE 

if($_REQUEST['sortby']=="subject")
{
	$sortby = "subject";
	$sort =" order by A.subject";
	$add1 = "&sortby=subject";
}
elseif($_REQUEST['sortby']=="details")
{
	$sortby = "details";
	$sort =" order by A.message";
	$add1 = "&sortby=details";
}
elseif($_REQUEST['sortby']=="username")
{
	$sortby = "username";
	$sort =" order by B.username";
	$add1 = "&sortby=username";
}
elseif($_REQUEST['sortby']=="msgfrom")
{
	$sortby = "msgfrom";
	$sort =" order by C.username";
	$add1 = "&sortby=msgfrom";
}
elseif($_REQUEST['sortby']=="time_added")
{
	$sortby = "time_added";
	$sort =" order by A.time";
	$add1 = "&sortby=time_added";
}
else
{
	$sortby = "MID";
	$sort =" order by A.MID";
	$add1 = "&sortby=MID";
}

if($_REQUEST['sorthow']=="desc")
{
	$sorthow ="desc";
	$add1 .= "&sorthow=desc";
}
else
{
	$sorthow ="asc";
	$add1 .= "&sorthow=asc";
}

//Search
$fromid = intval($_REQUEST['fromid']);
$toid = intval($_REQUEST['toid']);
$subject = htmlentities(strip_tags($_REQUEST['subject']), ENT_COMPAT, "UTF-8");
$details = htmlentities(strip_tags($_REQUEST['details']), ENT_COMPAT, "UTF-8");
$username = htmlentities(strip_tags($_REQUEST['username']), ENT_COMPAT, "UTF-8");
$msgfrom = htmlentities(strip_tags($_REQUEST['msgfrom']), ENT_COMPAT, "UTF-8");
$add1 .= "&fromid=$fromid&toid=$toid&subject=$subject&details=$details&username=$username&msgfrom=$msgfrom";
if($_POST['submitform'] == "1" || ($_REQUEST['fromid']!="" || $toid>0 || $subject!="" || $details!="" || $username!="" || $msgfrom!=""))
{
	if($fromid > 0)
	{
		$addtosql = "AND A.MID>='".mysql_real_escape_string($fromid)."'";
		Stemplate::assign('fromid',$fromid);
	}
	else
	{
		$addtosql = "AND A.MID>'".mysql_real_escape_string($fromid)."'";
	}
	if($toid > 0)
	{
		$addtosql .= "AND A.MID<='".mysql_real_escape_string($toid)."'";
		Stemplate::assign('toid',$toid);
	}
	if($subject != "")
	{
		$addtosql .= "AND A.subject like'%".mysql_real_escape_string($subject)."%'";
		Stemplate::assign('subject',$subject);
	}
	if($details != "")
	{
		$addtosql .= "AND A.message like'%".mysql_real_escape_string($details)."%'";
		Stemplate::assign('details',$details);
	}
	if($username != "")
	{
		$addtosql .= "AND B.username like'%".mysql_real_escape_string($username)."%'";
		Stemplate::assign('username',$username);
	}
	if($msgfrom != "")
	{
		$addtosql .= "AND C.username like'%".mysql_real_escape_string($msgfrom)."%'";
		Stemplate::assign('msgfrom',$msgfrom);
	}
	Stemplate::assign('search',"1");
}
//Search End

$page = intval($_REQUEST['page']);
if($page=="")
{
	$page = "1";
}
$currentpage = $page;

if ($page >=2)
{
	$pagingstart = ($page-1)*$config['items_per_page'];
}
else
{
	$pagingstart = "0";
}

$queryselected = "select A.MID,B.username, C.username as msgfrom from messages_inbox A, members B, members C WHERE A.MID>0 AND A.MSGTO=B.USERID AND A.MSGFROM=C.USERID AND A.type='msg' $addtosql $sort $sorthow limit $config[maximum_results]";
$query2 = "select A.*,B.username, C.username as msgfrom from messages_inbox A, members B, members C WHERE A.MID>0 AND A.MSGTO=B.USERID AND A.MSGFROM=C.USERID AND A.type='msg' $addtosql $sort $sorthow limit $pagingstart, $config[items_per_page]";
$executequeryselected = $conn->Execute($queryselected);
$totalposts = $executequeryselected->rowcount();	
if ($totalposts > 0)
{
	if($totalposts<=$config[maximum_results])
	{
		$total = $totalposts;
	}
	else
	{
		$total = $config[maximum_results];
	}
	$toppage = ceil($total/$config[items_per_page]);
	if($toppage==0)
	{
		$xpage=$toppage+1;
	}
	else
	{
		$xpage = $toppage;
	}
	$executequery2 = $conn->Execute($query2);	
	$results = $executequery2->getrows();
	$beginning=$pagingstart+1;
	$ending=$pagingstart+$executequery2->recordcount();
	$pagelinks="";
	$k=1;
	$theprevpage=$currentpage-1;
	$thenextpage=$currentpage+1;
	if ($currentpage > 0)
	{	
		if($currentpage > 1) 
		{
			$pagelinks.="<a href='$adminurl/messages_manage.php?page=1$add1' title='first page'>First</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/messages_manage.php?page=$theprevpage$add1'>Previous</a>&nbsp;";
		};
		$counter=0;
		$lowercount = $currentpage-5;
		if ($lowercount <= 0) $lowercount = 1;
		while ($lowercount < $currentpage)
		{
			$pagelinks.="<a href='$adminurl/messages_manage.php?page=$lowercount$add1'>$lowercount</a>&nbsp;";
			$lowercount++;
			$counter++;
		}
		$pagelinks.=$currentpage."&nbsp;";
		$uppercounter = $currentpage+1;
		while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
		{
			$pagelinks.="<a href='$adminurl/messages_manage.php?page=$uppercounter$add1'>$uppercounter</a>&nbsp;";
			$uppercounter++;
		}
		if($currentpage < $toppage) 
		{
			$pagelinks.="<a href='$adminurl/messages_manage.php?page=$thenextpage$add1'>Next</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/messages_manage.php?page=$toppage$add1' title='last page'>Last</a>&nbsp;";
		};
	}
}
else
{
	$error = "Sorry, no messages were found.";
}

$mainmenu = "9";
$submenu = "1";
Stemplate::assign('mainmenu',$mainmenu);
Stemplate::assign('submenu',$submenu);
Stemplate::assign('sorthow',$sorthow);
Stemplate::assign('sortby',$sortby);
Stemplate::assign('currentpage',$currentpage);
STemplate::display("administrator/global_header.tpl");
STemplate::assign('beginning',$beginning);
STemplate::assign('ending',$ending);
STemplate::assign('pagelinks',$pagelinks);
STemplate::assign('total',$total+0);
STemplate::assign('results',$results);
Stemplate::assign('error',$error);
STemplate::display("administrator/messages_manage.tpl");
STemplate::display("administrator/global_footer.tpl");
?>