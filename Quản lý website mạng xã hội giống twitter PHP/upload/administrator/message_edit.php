<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

$ID = intval($_REQUEST['MID']);

if($_POST['submitform'] == "1")
{
	if($ID > 0)
	{
		$arr = array("message", "subject");
		foreach ($arr as $value)
		{
			$sql = "update messages_inbox set $value='".mysql_real_escape_string($_POST[$value])."' where MID='".mysql_real_escape_string($ID)."'";
			$conn->execute($sql);
		}
		$message = "Message Successfully Edited.";
		Stemplate::assign('message',$message);
	}
}

if($ID > 0)
{
	$query = $conn->execute("select A.MID, B.username, C.username as msgfrom, A.message, A.subject, A.time from messages_inbox A, members B, members C where A.MID='".mysql_real_escape_string($ID)."' AND A.MSGTO=B.USERID AND A.MSGFROM=C.USERID limit 1");
	$p = $query->getrows();
	Stemplate::assign('p', $p[0]);
}

$mainmenu = "9";
$submenu = "1";
Stemplate::assign('mainmenu',$mainmenu);
Stemplate::assign('submenu',$submenu);
STemplate::display("administrator/global_header.tpl");
STemplate::display("administrator/message_edit.tpl");
STemplate::display("administrator/global_footer.tpl");
?>