<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
$theimgurl = $config['imageurl'];

$USERID = $_SESSION['USERID'];
if ($USERID != "" && $USERID >= 0 && is_numeric($USERID))
{	
	$query = "SELECT birthday,country FROM members WHERE USERID='".mysql_real_escape_string($USERID)."'";
    $executequery = $conn->execute($query);
    $memberarray = $executequery->getarray();
	STemplate::assign('memberarray',$memberarray);
	
	STemplate::assign('country',listcountries($memberarray[0][country]));
	
	$birthday = explode("-",$memberarray[0][birthday]);
	STemplate::assign('bdays',listdays($birthday[2]));
	STemplate::assign('bmonths',listmonths($birthday[1]));
	STemplate::assign('byears',listyears($birthday[0]));
	
	if($_REQUEST['spicform'] == "1")
	{
		$uploadedimage = $_FILES['ppic']['tmp_name'];
		
		if($uploadedimage != "")
		{
			$theimageinfo = getimagesize($uploadedimage);
			$thepp = $USERID;
		
			if($theimageinfo[2] == 1)
			{
				$thepp .= "-o.gif";
				$thepp2 = ".gif";
			}
			elseif($theimageinfo[2] == 2)
			{
				$thepp .= "-o.jpg";
				$thepp2 = ".jpg";
			}
			elseif($theimageinfo[2] == 3)
			{
				$thepp .= "-o.png";
				$thepp2 = ".png";
			}
			else
			{
				$error = "$lang[198]";
			}
			
			if($error == "")
			{
				$myvideoimgnew=$config['membersprofilepicdir']."/".$thepp;
				if(file_exists($myvideoimgnew))
				{
					unlink($myvideoimgnew);
				}
				$myconvertimg = $_FILES['ppic']['tmp_name'];
				move_uploaded_file($myconvertimg, $myvideoimgnew);
				
				do_resize_image($myvideoimgnew, "220", "165", true, $config['membersprofilepicdir']."/".$USERID.$thepp2);
				do_resize_image($myvideoimgnew, "75", "75", true, $config['membersprofilepicdir']."/thumbs/".$USERID.$thepp2);
				do_resize_image($myvideoimgnew, "100", "75", true, $config['membersprofilepicdir']."/thumbs/".$USERID."-m".$thepp2);
				do_resize_image($myvideoimgnew, "36", "27", true, $config['membersprofilepicdir']."/thumbs/".$USERID."-s".$thepp2);
				
				if(file_exists($config['membersprofilepicdir']."/".$thepp))
				{
					$query = "UPDATE members SET profilepicture='$thepp2' WHERE USERID='".mysql_real_escape_string($USERID)."'";
					$conn->execute($query);
				}
				else
				{
					$error = "$lang[199]";
				}
			}
		}
		$msg = $lang['200'];
	}
	elseif($_REQUEST['saboutform'] == "1")
	{
		$saying = cleanit($_REQUEST[saying]);
		$interests = cleanit($_REQUEST[interests]);
		$website = cleanit($_REQUEST[website]);
		$query = "UPDATE members SET saying='".mysql_real_escape_string($saying)."', interests='".mysql_real_escape_string($interests)."', website='".mysql_real_escape_string($website)."' WHERE USERID='".mysql_real_escape_string($USERID)."'"; 
		$executequery = $conn->execute($query);
		$msg = $lang['187'];
	}
	elseif($_REQUEST['spersform'] == "1")
	{
		$firstName = cleanit($_REQUEST[firstName]);
		$lastName = cleanit($_REQUEST[lastName]);
		$city = cleanit($_REQUEST[city]);
		$country = cleanit($_REQUEST[country]);
		$showAge = htmlentities(strip_tags($_REQUEST[showAge]), ENT_COMPAT, "UTF-8");
		$gender = htmlentities(strip_tags($_REQUEST[gender]), ENT_COMPAT, "UTF-8");
		$mypassword1 = htmlentities(strip_tags($_REQUEST[mypassword1]), ENT_COMPAT, "UTF-8");
		$mypassword2 = htmlentities(strip_tags($_REQUEST[mypassword2]), ENT_COMPAT, "UTF-8");
		
		if (is_numeric($_REQUEST[day]) && is_numeric($_REQUEST[month]) && is_numeric($_REQUEST[year]))
		{
			$day2 = $_REQUEST[day];
			$month2 = $_REQUEST[month];
			$year2 = $_REQUEST[year];
			$joinbday = "$year2-$month2-$day2";
			$moresql .= ", birthday='".mysql_real_escape_string($joinbday)."'";
		}
		else
		{
			$moresql .= ", birthday='0000-00-00'";
		}
		
		if($mypassword1 != "")
		{
			if($mypassword1 != $mypassword2)
			{
				$error = "1";
				STemplate::assign('perror',$lang['22']);
			}
			else
			{
				$npass = md5($mypassword1);
				$moresql .= ", password='".mysql_real_escape_string($npass)."', pwd='".mysql_real_escape_string($mypassword1)."'";
			}
		}
		
		$query = "UPDATE members SET firstname='".mysql_real_escape_string($firstName)."', lastname='".mysql_real_escape_string($lastName)."', city='".mysql_real_escape_string($city)."', country='".mysql_real_escape_string($country)."', showAge='".mysql_real_escape_string($showAge)."', gender='".mysql_real_escape_string($gender)."' $moresql WHERE USERID='".mysql_real_escape_string($USERID)."'"; 
		$executequery = $conn->execute($query);
		$msg = $lang['187'];
		STemplate::assign('country',listcountries($country));
		STemplate::assign('bdays',listdays($day2));
		STemplate::assign('bmonths',listmonths($month2));
		STemplate::assign('byears',listyears($year2));
	}
	elseif($_REQUEST['seform'] == "1")
	{
		$email = htmlentities(strip_tags($_REQUEST[email]), ENT_COMPAT, "UTF-8");
		if ($email == "")
		{
			$merror = "$lang[26]";
			$error = "1";
		}
		elseif(!verify_valid_email($email))
		{
			$merror = "$lang[24]";
			$error = "1";
		}
		elseif (!verify_email_unique($email))
		{
			$merror = "$lang[25]";
			$error = "1";
		}
		else
		{
			$query = "UPDATE members SET email='".mysql_real_escape_string($email)."', verified='0' WHERE USERID='".mysql_real_escape_string($USERID)."'"; 
			$executequery = $conn->execute($query);
			$msg = $lang['197'];
			$_SESSION[EMAIL] = $email;
			$_SESSION[VERIFIED] = "0";
			
			$verifycode = generateCode(5).time();
			$query = "UPDATE members_verifycode SET code='".mysql_real_escape_string($verifycode)."' WHERE USERID='".mysql_real_escape_string($USERID)."'";		
			$conn->execute($query);
			
			if($verifycode != "")
			{
				STemplate::assign('verifycode',$verifycode);
				$sendto = $email;
				$sendername = $config['site_name'];
				STemplate::assign('sendername',$sendername);
				$from = $config['site_email'];
				$query = "SELECT * FROM sendmail WHERE EID='confirmemail'";
				$executequery = $conn->execute($query);
				$subject = $lang['276'];
				$sendmailtemplate = $executequery->fields['template'];
				$sendmailbody=STemplate::fetch($sendmailtemplate);
				mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
			}
		}
	}
	
	
	$query = "SELECT * FROM members WHERE USERID='".mysql_real_escape_string($USERID)."'"; 
	$executequery = $conn->execute($query);
    $p = $executequery->getarray();
	STemplate::assign('p',$p[0]);
	$templateselect = "myprofile.tpl";
	
	if($_REQUEST['r'] == "1")
	{
		$query = "SELECT code FROM members_verifycode WHERE USERID='".mysql_real_escape_string($USERID)."'";		
		$executequery = $conn->execute($query);
    	$carray = $executequery->getarray();
		$verifycode = $carray[0][code];
		
		if($verifycode != "")
		{
			STemplate::assign('verifycode',$verifycode);
			$sendto = $p[0][email];
			$sendername = $config['site_name'];
			STemplate::assign('sendername',$sendername);
			$from = $config['site_email'];
			$query = "SELECT * FROM sendmail WHERE EID='confirmemail'";
			$executequery = $conn->execute($query);
			$subject = $lang['276'];
			$sendmailtemplate = $executequery->fields['template'];
			$sendmailbody=STemplate::fetch($sendmailtemplate);
			mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
		}
		$msg = $lang['188'];
	}
}
else
{
	$redirect = base64_encode($config['baseurl']."/myprofile.php");
	header("Location:$config[baseurl]/login.php?redirect=$redirect");exit;
}

$pagetitle = $lang[147];
STemplate::assign('pagetitle',$pagetitle);

//TEMPLATES BEGIN
STemplate::assign('msg',$msg);
STemplate::assign('error',$error);
STemplate::assign('merror',$merror);
STemplate::display('header.tpl');
STemplate::display($templateselect);
STemplate::display('footer.tpl');
//TEMPLATES END
?>