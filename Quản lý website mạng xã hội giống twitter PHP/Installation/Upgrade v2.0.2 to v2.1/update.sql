ALTER TABLE `members` CHANGE `fil2` `fil2` VARCHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1',
CHANGE `fil4` `fil4` VARCHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1',
CHANGE `fil5` `fil5` VARCHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1';

CREATE TABLE IF NOT EXISTS `bans_ips` (
  `ip` varchar(20) NOT NULL,
  UNIQUE KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `members` ADD `ip` VARCHAR( 20 ) NOT NULL ;
ALTER TABLE `members` ADD `lip` VARCHAR( 20 ) NOT NULL ;

UPDATE `config` SET `value` = '2.1' WHERE CONVERT( `config`.`setting` USING utf8 ) = 'ver' LIMIT 1 ;