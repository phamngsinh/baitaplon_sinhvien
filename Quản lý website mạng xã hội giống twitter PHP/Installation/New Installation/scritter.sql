SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

-- --------------------------------------------------------

--
-- Table structure for table `administrators`
--

CREATE TABLE IF NOT EXISTS `administrators` (
  `ADMINID` bigint(20) NOT NULL auto_increment,
  `email` varchar(80) NOT NULL default '',
  `username` varchar(80) NOT NULL default '',
  `password` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`ADMINID`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `administrators`
--

INSERT INTO `administrators` (`ADMINID`, `email`, `username`, `password`) VALUES
(1, 'webmaster@scritterscript.com', 'Admin', 'f42c25af3d564dcdbad3c2f3134c5558');

-- --------------------------------------------------------

--
-- Table structure for table `advertisements`
--

CREATE TABLE IF NOT EXISTS `advertisements` (
  `AID` bigint(30) NOT NULL auto_increment,
  `description` varchar(200) NOT NULL default '',
  `code` text NOT NULL,
  `active` enum('1','0') NOT NULL default '1',
  PRIMARY KEY  (`AID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `advertisements`
--

INSERT INTO `advertisements` (`AID`, `description`, `code`, `active`) VALUES
(1, '300 x 250 pixels', '<div style="width:300px; height:250px; border:1px solid #DFDFDF;" align="center"><br/><br/>Insert Your<br/>Advertisement Here</div>', '1'),
(2, '120 x 728 pixels', '<div style="width:120px; height:728px; border:1px solid #DFDFDF;" align="center"><br/><br/>Insert Your Advertisement Here</div>', '1'),
(3, '728 x 90 pixels', '<div style="width:728px; height:90px; border:1px solid #DFDFDF;" align="center"><br/><br/>Insert Your Advertisement Here</div>', '1'),
(4, '350 x 90 pixels', '<div style="width:350px; height:90px; border:1px solid #DFDFDF;" align="center"><br/><br/>Insert Your Advertisement Here</div>', '1'),
(5, '300 x 100 pixels', '<div style="width:300px; height:100px; border:1px solid #DFDFDF;" align="center"><br/><br/>Insert Your Advertisement Here</div>', '1'),
(6, '120 x 600 pixels', '<div style="width:120px; height:600px; border:1px solid #DFDFDF;" align="center"><br/><br/>Insert Your Advertisement Here</div>', '1'),
(7, '280 x 250 pixels', '<div style="width:280px; height:250px; border:1px solid #ffffff;" align="center"><br/><br/>Insert Your<br/>Advertisement Here</div>', '1');

-- --------------------------------------------------------

--
-- Table structure for table `block`
--

CREATE TABLE IF NOT EXISTS `block` (
  `ID` bigint(20) NOT NULL auto_increment,
  `USERID` bigint(20) NOT NULL default '0',
  `BID` bigint(20) NOT NULL,
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `USERID` (`USERID`,`BID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `block`
--


-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `setting` varchar(60) NOT NULL default '',
  `value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`setting`, `value`) VALUES
('site_email', 'webmaster@yourdomain.com'),
('site_name', 'Scritter - Twitter Clone Script'),
('max_syndicate_results', '25'),
('maximum_results', '1000000'),
('emailsender', 'Admin'),
('max_img_size', '200'),
('max_posts_userhome', '10'),
('items_per_page', '10'),
('max_post_chars', '200'),
('approve_stories', '1'),
('metadescription', 'ScritterScript is a twitter clone script and micro blogging script.'),
('metakeywords', 'scritter, scritterscript, twitter, clone'),
('max_posts_userupdates', '15'),
('enable_captcha', '1'),
('ver', '2.3.2'),
('short_name', 'Scritter'),
('max_posts_userfollowing', '15'),
('max_posts_userfollowers', '15'),
('max_posts_recentupdates', '10'),
('max_posts_newmem', '10'),
('max_posts_srchchans', '10'),
('max_posts_srchupdates', '10');

-- --------------------------------------------------------

--
-- Table structure for table `follow`
--

CREATE TABLE IF NOT EXISTS `follow` (
  `ID` bigint(20) NOT NULL auto_increment,
  `USERID` bigint(20) NOT NULL default '0',
  `FID` bigint(20) NOT NULL,
  `friend` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `USERID` (`USERID`,`FID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `USERID` bigint(20) NOT NULL auto_increment,
  `email` varchar(80) NOT NULL default '',
  `username` varchar(80) NOT NULL default '',
  `password` varchar(50) NOT NULL default '',
  `pwd` varchar(50) NOT NULL,
  `firstname` varchar(60) NOT NULL default '',
  `lastname` varchar(60) NOT NULL default '',
  `birthday` date NOT NULL default '0000-00-00',
  `gender` varchar(6) NOT NULL default '',
  `city` varchar(80) NOT NULL default '',
  `country` varchar(100) NOT NULL default '',
  `profileviews` int(20) NOT NULL default '0',
  `addtime` varchar(20) NOT NULL default '',
  `lastlogin` varchar(20) NOT NULL default '',
  `verified` char(1) NOT NULL default '0',
  `status` enum('1','0') NOT NULL default '1',
  `profilepicture` varchar(100) NOT NULL default '',
  `saying` varchar(200) NOT NULL,
  `website` varchar(300) NOT NULL,
  `interests` varchar(300) NOT NULL,
  `showAge` bigint(1) NOT NULL default '1',
  `bg` varchar(100) NOT NULL,
  `showbg` varchar(1) NOT NULL default '0',
  `tile` varchar(1) NOT NULL default '0',
  `public` varchar(1) NOT NULL default '1',
  `alert_com` varchar(1) NOT NULL default '1',
  `alert_msg` varchar(1) NOT NULL default '1',
  `alert_fol` varchar(1) NOT NULL default '1',
  `alert_fr` varchar(1) NOT NULL default '1',
  `fil1` varchar(1) NOT NULL default '1',
  `fil2` varchar(1) NOT NULL default '1',
  `fil3` varchar(1) NOT NULL default '1',
  `fil4` varchar(1) NOT NULL default '1',
  `fil5` varchar(1) NOT NULL default '1',
  PRIMARY KEY  (`USERID`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`USERID`, `email`, `username`, `password`, `pwd`, `firstname`, `lastname`, `birthday`, `gender`, `city`, `country`, `profileviews`, `addtime`, `lastlogin`, `verified`, `status`, `profilepicture`, `saying`, `website`, `interests`, `showAge`, `bg`, `showbg`, `tile`, `public`, `alert_com`, `alert_msg`, `alert_fol`, `alert_fr`, `fil1`, `fil2`, `fil3`, `fil4`, `fil5`) VALUES
(0, 'system@yourdomain.com', 'SYSTEM', 'a3aca2964e72000eea4c56cb341002a4', '', '', '', '0000-00-00', '0', '', '', 0, '1246462321', '1256357359', '1', '1', '', '', '', '', 0, '', '0', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `members_passcode`
--

CREATE TABLE IF NOT EXISTS `members_passcode` (
  `USERID` bigint(20) NOT NULL default '0',
  `code` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`USERID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `members_verifycode`
--

CREATE TABLE IF NOT EXISTS `members_verifycode` (
  `USERID` bigint(20) NOT NULL default '0',
  `code` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`USERID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `messages_inbox`
--

CREATE TABLE IF NOT EXISTS `messages_inbox` (
  `MID` bigint(20) NOT NULL auto_increment,
  `MSGTO` bigint(20) NOT NULL default '0',
  `MSGFROM` bigint(20) NOT NULL default '0',
  `subject` varchar(100) NOT NULL default '',
  `message` text NOT NULL,
  `time` varchar(20) NOT NULL default '',
  `unread` char(1) NOT NULL default '1',
  `type` varchar(3) NOT NULL default 'msg',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `ID` bigint(20) NOT NULL auto_increment,
  `type` varchar(10) NOT NULL,
  `USERID` bigint(20) NOT NULL,
  `PID` bigint(20) NOT NULL,
  `UID` bigint(20) NOT NULL,
  `UIDO` bigint(20) NOT NULL,
  `msg` text NOT NULL,
  `pic` varchar(20) NOT NULL,
  `time_added` varchar(20) default NULL,
  `views` bigint(20) NOT NULL,
  `reply` bigint(20) NOT NULL,
  `edited` varchar(20) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sendmail`
--

CREATE TABLE IF NOT EXISTS `sendmail` (
  `EID` varchar(50) NOT NULL default '',
  `subject` varchar(255) NOT NULL default '',
  `template` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`EID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sendmail`
--

INSERT INTO `sendmail` (`EID`, `subject`, `template`) VALUES
('confirmemail', 'E-Mail Verification', 'sendmail/confirmemail.tpl'),
('sendnewpassword', 'Your new password for', 'sendmail/sendnewpassword.tpl'),
('welcomeemail', 'Welcome to', 'sendmail/welcomeemail.tpl'),
('confirmforgotpass', 'Reset Password Link', 'sendmail/confirmforgotpass.tpl');

-- --------------------------------------------------------

--
-- Table structure for table `static`
--

CREATE TABLE IF NOT EXISTS `static` (
  `ID` bigint(30) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `value` blob NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `static`
--

INSERT INTO `static` (`ID`, `title`, `value`) VALUES
(1, 'Terms Of Use', 0x496e7365727420796f7572207465726d73206f662075736520696e666f726d6174696f6e20686572652e3c62723e3c62723e0d0a0d0a48544d4c2069732061636365707465642e),
(2, 'Privacy Policy', 0x496e7365727420796f7572207072697661637920706f6c69637920696e666f726d6174696f6e20686572652e3c62723e3c62723e0d0a0d0a48544d4c2069732061636365707465642e),
(3, 'About Us', 0x496e7365727420796f75722061626f757420757320696e666f726d6174696f6e20686572652e3c62723e3c62723e0d0a0d0a48544d4c2069732061636365707465642e),
(5, 'Contact Us', 0x496e7365727420796f757220636f6e7461637420757320696e666f726d6174696f6e20686572652e3c62723e3c62723e0d0a0d0a48544d4c2069732061636365707465642e);

CREATE TABLE IF NOT EXISTS `bans_ips` (
  `ip` varchar(20) NOT NULL,
  UNIQUE KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `members` ADD `ip` VARCHAR( 20 ) NOT NULL ;
ALTER TABLE `members` ADD `lip` VARCHAR( 20 ) NOT NULL ;

CREATE TABLE IF NOT EXISTS `bans_words` (
  `word` varchar(100) NOT NULL,
  UNIQUE KEY `word` (`word`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
ALTER TABLE `posts` ADD `pip` VARCHAR( 20 ) NOT NULL ;