<?php /* Smarty version 2.6.18, created on 2011-03-19 18:44:39
         compiled from /home/nhadatan/public_html//backend/templates/district/regist.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frmdistrict;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frmdistrict;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frmdistrict" id="frmdistrict" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			<?php if ($this->_tpl_vars['message'] != ""): ?>
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;"><?php echo $this->_tpl_vars['message']; ?>
</td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endif; ?>
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;"><?php echo $this->_config[0]['vars']['DISTRICT_TITLES']; ?>
</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="districtid" id="districtid" value="<?php echo $this->_tpl_vars['districtid']; ?>
" />
					<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						<?php if ($this->_tpl_vars['districtid'] != ''): ?>
						<tr valign="top">
							<td width="30%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['FIELD_ID']; ?>
: </td>
							<td width="45%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<?php echo $this->_tpl_vars['districtid']; ?>

							</td>
						</tr>
						<?php endif; ?>
						<tr>
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['PROVINCE_NAME']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<select name="object[province_id]" id="object[province_id]" class="dropdown">
									<?php $_from = $this->_tpl_vars['provinceList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['provinces']):
?>
									<option value="<?php echo $this->_tpl_vars['provinces']['province_id']; ?>
" <?php if ($this->_tpl_vars['districtData']['province_id'] == $this->_tpl_vars['provinces']['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['provinces']['province_name']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['DISTRICT_NAME']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[district_name]" id="object[district_name]" value="<?php echo $this->_tpl_vars['districtData']['district_name']; ?>
" class="input_text" />
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['STATUS']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[status]" id="object[status]" class="dropdown">
									<?php $_from = $this->_tpl_vars['statusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['status']):
?>
									<option value="<?php echo $this->_tpl_vars['status']['value']; ?>
" <?php if ($this->_tpl_vars['districtData']['status'] == $this->_tpl_vars['status']['value'] && $this->_tpl_vars['districtData']['status'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['status']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="<?php echo $this->_config[0]['vars']['UPDATE']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=district/regist', 1)" />
					<input type="button" name="isback" id="isback" value="<?php echo $this->_config[0]['vars']['BACK']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=district/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>