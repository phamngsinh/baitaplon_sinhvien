<?php /* Smarty version 2.6.18, created on 2011-03-19 17:36:01
         compiled from /home/nhadatan/public_html//backend/templates/login.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $this->_config[0]['vars']['page_title']; ?>
</title>
</head>
<?php echo '
<style type="text/css">
	#root-rayout
	{
		width: 100%;
		text-align: center;
		height: 380px;
		margin-top: 190px;
	}
	
	.form-rayout
	{
		text-align: left;
		margin: auto;
	}
	.button 	{
		BORDER: #000 0px solid; 
		FONT-FAMILY: Arial,Verdana,Helvetica,sans-serif;
		FONT-WEIGHT: normal; 
		FONT-SIZE: 8pt; 
		COLOR: #000000; 
		FONT-FAMILY: Verdana, Arial; 
		height:22px;
		width:90px;
		background-image:url(./backend/images/button3.gif)
	}
	.input_text{
		font-family: Arial, Helvetica, sans-serif;
		font-size: 11px;	
		border: #E2E2E2 1px solid;
		height: 15px;
		width:150px;
	}
	.dropdown{
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		border: #E2E2E2 1px solid;
		height: 20px;
	}
</style>
<script language=javascript type="text/javascript">
	function focusTxtPassword(event) {
		theform = document.forms["frmLogin"];
		if ((window.event && window.event.keyCode == 13) || (event && event.which == 13)) {
			theform.account_pass.focus();
		}
	}

	function focusLoginButton(event) {
		theform = document.forms["frmLogin"];
		if ((window.event && window.event.keyCode == 13) || (event && event.which == 13)) {
			theform.login.focus();
		}
	}
	
	function loadForm() {
		theform = document.forms["frmLogin"];
		theform.account_name.focus();
	}
	
	function SubmitForm() {
		theform = document.forms["frmLogin"];
		theform.submit();
	}
</script>
'; ?>

<body onLoad="javascript: loadForm();">
<DIV id="root-rayout">
	<form action="?hdl=login" name="frmLogin" id="frmLogin" method="post">
  		<input type="hidden" name="islogin" value="TRUE" />
		<?php if ($this->_tpl_vars['message'] != ""): ?>
		<table align="center">
			<tr><td style="color:#FF0000"><?php echo $this->_tpl_vars['message']; ?>
</td></tr>
		</table>
		<?php endif; ?>
		<table align="center" style="border: 1px #999999 solid" cellpadding="0" cellspacing="0">
			<tr><td align="center">
				<table bgcolor="#F5F5F5">
					<tr><td colspan="2" align="center" style="font-size:16px;font-weight:bold;background-color:#DCDCDC;color:#0066FF;">LOGIN ADMIN</td></tr>
					<tr>
						<td style="font-size:13px;"><?php echo $this->_config[0]['vars']['ACCOUNT_NAME']; ?>
 :</td>
						<td><input type="text" name="account_name" id="account_name" value="<?php echo $this->_tpl_vars['account_name']; ?>
" onKeyDown="return focusTxtPassword(event);" class="input_text"/></td>
					</tr>
					<tr>
						<td style="font-size:13px;"><?php echo $this->_config[0]['vars']['ACCOUNT_PASS']; ?>
 :</td>
						<td><input type="password" name="account_pass" id="account_pass" value="<?php echo $this->_tpl_vars['account_pass']; ?>
" onKeyDown="return focusLoginButton(event);" class="input_text"/></td>
					</tr>
				</table>
			</td></tr>
			<tr><td align="center"><input type="button" name="login" id="login" value="<?php echo $this->_config[0]['vars']['ACCOUNT_LOGIN']; ?>
" class="button" onclick="return SubmitForm();"/></td></tr>
			<tr><td height="2px;"></td></tr>
		</table>
	</form>
</DIV>
</body>
</html>