<?php /* Smarty version 2.6.18, created on 2011-03-19 17:43:07
         compiled from /home/nhadatan/public_html//backend/templates/province/regist.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frmprovince;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frmprovince;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frmprovince" id="frmprovince" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			<?php if ($this->_tpl_vars['message'] != ""): ?>
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;"><?php echo $this->_tpl_vars['message']; ?>
</td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endif; ?>
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;"><?php echo $this->_config[0]['vars']['PROVINCE_TITLES']; ?>
</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="provinceid" id="provinceid" value="<?php echo $this->_tpl_vars['provinceid']; ?>
" />
					<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						<?php if ($this->_tpl_vars['provinceid'] != ''): ?>
						<tr valign="top">
							<td width="30%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['FIELD_ID']; ?>
: </td>
							<td width="45%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<?php echo $this->_tpl_vars['provinceid']; ?>

							</td>
						</tr>
						<?php endif; ?>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['PROVINCE_NAME']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[province_name]" id="object[province_name]" value="<?php echo $this->_tpl_vars['provinceData']['province_name']; ?>
" class="input_text" />
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['STATUS']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[status]" id="object[status]" class="dropdown">
									<?php $_from = $this->_tpl_vars['statusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['status']):
?>
									<option value="<?php echo $this->_tpl_vars['status']['value']; ?>
" <?php if ($this->_tpl_vars['provinceData']['status'] == $this->_tpl_vars['status']['value'] && $this->_tpl_vars['provinceData']['status'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['status']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">Thứ tự hiển thị: </td>
							<td align="left" valign="middle" class="tdregist">
								<select name="object[s_order]" id="object[s_order]" class="dropdown">
								<?php unset($this->_sections['s_order_loop']);
$this->_sections['s_order_loop']['name'] = 's_order_loop';
$this->_sections['s_order_loop']['loop'] = is_array($_loop=99) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['s_order_loop']['show'] = true;
$this->_sections['s_order_loop']['max'] = $this->_sections['s_order_loop']['loop'];
$this->_sections['s_order_loop']['step'] = 1;
$this->_sections['s_order_loop']['start'] = $this->_sections['s_order_loop']['step'] > 0 ? 0 : $this->_sections['s_order_loop']['loop']-1;
if ($this->_sections['s_order_loop']['show']) {
    $this->_sections['s_order_loop']['total'] = $this->_sections['s_order_loop']['loop'];
    if ($this->_sections['s_order_loop']['total'] == 0)
        $this->_sections['s_order_loop']['show'] = false;
} else
    $this->_sections['s_order_loop']['total'] = 0;
if ($this->_sections['s_order_loop']['show']):

            for ($this->_sections['s_order_loop']['index'] = $this->_sections['s_order_loop']['start'], $this->_sections['s_order_loop']['iteration'] = 1;
                 $this->_sections['s_order_loop']['iteration'] <= $this->_sections['s_order_loop']['total'];
                 $this->_sections['s_order_loop']['index'] += $this->_sections['s_order_loop']['step'], $this->_sections['s_order_loop']['iteration']++):
$this->_sections['s_order_loop']['rownum'] = $this->_sections['s_order_loop']['iteration'];
$this->_sections['s_order_loop']['index_prev'] = $this->_sections['s_order_loop']['index'] - $this->_sections['s_order_loop']['step'];
$this->_sections['s_order_loop']['index_next'] = $this->_sections['s_order_loop']['index'] + $this->_sections['s_order_loop']['step'];
$this->_sections['s_order_loop']['first']      = ($this->_sections['s_order_loop']['iteration'] == 1);
$this->_sections['s_order_loop']['last']       = ($this->_sections['s_order_loop']['iteration'] == $this->_sections['s_order_loop']['total']);
?>
								    <option value="<?php echo $this->_sections['s_order_loop']['iteration']; ?>
" 
								    	<?php if ($this->_tpl_vars['provinceData']['s_order'] == $this->_sections['s_order_loop']['iteration']): ?> selected="selected" <?php endif; ?>
								    	>
								    	<?php echo $this->_sections['s_order_loop']['iteration']; ?>

								<?php endfor; endif; ?>
								</select>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="<?php echo $this->_config[0]['vars']['UPDATE']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=province/regist', 1)" />
					<input type="button" name="isback" id="isback" value="<?php echo $this->_config[0]['vars']['BACK']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=province/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>