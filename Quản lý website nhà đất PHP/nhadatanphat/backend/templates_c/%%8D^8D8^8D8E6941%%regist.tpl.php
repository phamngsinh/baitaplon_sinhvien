<?php /* Smarty version 2.6.18, created on 2011-03-25 21:14:23
         compiled from /home/nhadatan/public_html//backend/templates/interior/regist.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'indent', '/home/nhadatan/public_html//backend/templates/interior/regist.tpl', 88, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frminterior;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frminterior;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frminterior" id="frminterior" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			<?php if ($this->_tpl_vars['message'] != ""): ?>
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;"><?php echo $this->_tpl_vars['message']; ?>
</td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endif; ?>
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;"><?php echo $this->_config[0]['vars']['TITLES_INTERIOR']; ?>
</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="interiorid" id="interiorid" value="<?php echo $this->_tpl_vars['interiorid']; ?>
" />
					<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						<?php if ($this->_tpl_vars['interiorid'] != ''): ?>
						<tr valign="top">
							<td width="30%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['FIELD_ID']; ?>
: </td>
							<td width="45%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<?php echo $this->_tpl_vars['interiorid']; ?>

							</td>
							<td rowspan="5" width="24%" align="center" valign="middle" class="tdregist">
								<?php if ($this->_tpl_vars['interiorData']['interior_image'] != ""): ?>
								<img border="1" style="border-color:#CAD5DB;" width="100px" src="<?php echo $this->_config[0]['vars']['IMG_INTERIOR_DIR']; ?>
thumb_<?php echo $this->_tpl_vars['interiorData']['interior_image']; ?>
" />
								<?php endif; ?>
							</td>
						</tr>
						<?php endif; ?>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['INTERIOR_NAME']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[interior_name]" id="object[interior_name]" value="<?php echo $this->_tpl_vars['interiorData']['interior_name']; ?>
" class="input_text" />
							</td>
						</tr>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['INTERIOR_ICON']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type="file" name="interior_image" id="interior_image" class="inputfield" />
								<input type="hidden" name="object[interior_image]" id="object[interior_image]" value="<?php echo $this->_tpl_vars['interiorData']['interior_image']; ?>
" />
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['INTERIOR_DES']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<textarea name="object[interior_description]" id="object[interior_description]" class="textarea"><?php echo $this->_tpl_vars['interiorData']['interior_description']; ?>
</textarea>
							</td>
						</tr>
						
						<tr valign="top">
							<td valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['LBL_CAT_SELECT']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<select name="object[category_id]" class="dropdown">
									<option value=""><?php echo $this->_config[0]['vars']['LBL_CAT_SELECT']; ?>
:</option>
									<?php $_from = $this->_tpl_vars['cat_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['cat']):
?>
									<option	value="<?php echo $this->_tpl_vars['cat']['category_id']; ?>
" <?php if ($this->_tpl_vars['interiorData']['category_id'] == $this->_tpl_vars['cat']['category_id']): ?> selected <?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['cat']['category_name'])) ? $this->_run_mod_handler('indent', true, $_tmp, $this->_tpl_vars['cat']['level'], "&#166;&nbsp;&nbsp;&nbsp;&nbsp;", "&#166;--&nbsp;&nbsp;") : smarty_modifier_indent($_tmp, $this->_tpl_vars['cat']['level'], "&#166;&nbsp;&nbsp;&nbsp;&nbsp;", "&#166;--&nbsp;&nbsp;")); ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>
							</td>
						</tr>
						
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['STATUS']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[status]" id="object[status]" class="dropdown">
									<?php $_from = $this->_tpl_vars['statusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['status']):
?>
									<option value="<?php echo $this->_tpl_vars['status']['value']; ?>
" <?php if ($this->_tpl_vars['interiorData']['status'] == $this->_tpl_vars['status']['value'] && $this->_tpl_vars['interiorData']['status'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['status']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="<?php echo $this->_config[0]['vars']['UPDATE']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=interior/regist', 1)" />
					<input type="button" name="isback" id="isback" value="<?php echo $this->_config[0]['vars']['BACK']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=interior/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>