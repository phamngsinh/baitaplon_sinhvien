<?php /* Smarty version 2.6.18, created on 2011-03-19 17:52:42
         compiled from /home/nhadatan/public_html//backend/templates/auth/list.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function ConfirmDeleteRecord(url, accountid, page) {
		var theform = document.frmaccount;
		if (confirm(\'Do you want to delete selected records?\')) {
			theform.action = url;
			theform.accountid.value = accountid;
			theform.page.value = page;
			theform.submit();
		} else return false;
	}
	function submitform(url, accountid, page) {
		var theform = document.frmaccount;
		theform.action = url;
		theform.accountid.value = accountid;
		theform.page.value = page;
		theform.submit();
	}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frmaccount" id="frmaccount" action="?hdl=auth/regist" method="post">
			<input type="hidden" name="accountid" id="accountid" value="" />
			<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="7"></td>
			</tr>
			<!--header-->
			<tr class="stdHeader" valign="top">
				<td width="4%" align="center" valign="middle" style="height:15px;"><?php echo $this->_config[0]['vars']['ACCOUNT_ID']; ?>
</td>
				<td width="10%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['ACCOUNT_NAME']; ?>
</td>
				<td width="15%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['ACCOUNT_FULL_NAME']; ?>
</td>
				<td width="15%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['ACCOUNT_EMAIL']; ?>
</td>
				<td width="30%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['ACCOUNT_ADDRESS']; ?>
</td>
				<td width="10%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['STATUS']; ?>
</td>
				<td width="10%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['FUNCTION']; ?>
</td>
			</tr>
			<!--If cls = 1 then strClass = "record" else strClass = "evenRecord" end if-->
			<?php if (count ( $this->_tpl_vars['accountList'] ) > 0): ?>
			<?php $_from = $this->_tpl_vars['accountList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['items']):
?>
			<?php $this->assign('i', $this->_tpl_vars['i']+1); ?>
			<?php if ($this->_tpl_vars['i']%2 == 0): ?>
				<?php $this->assign('class', 'record'); ?>
			<?php else: ?>
				<?php $this->assign('class', 'evenRecord'); ?>
			<?php endif; ?>
			<tr class="<?php echo $this->_tpl_vars['class']; ?>
" valign="top">
				<td align="center" style="padding:5px;" valign="middle"><?php echo $this->_tpl_vars['items']['account_id']; ?>
</td>
				<td align="left" style="padding:5px;" valign="middle">
					<?php echo $this->_tpl_vars['items']['account_name']; ?>

				</td>
				<td align="left" style="padding:5px;" valign="middle">
					<?php echo $this->_tpl_vars['items']['account_full_name']; ?>

				</td>
				<td align="left" style="padding:5px;" valign="middle">
					<?php echo $this->_tpl_vars['items']['account_email']; ?>

				</td>
				<td align="left" style="padding:5px;" valign="middle">
					<?php echo $this->_tpl_vars['items']['account_address']; ?>

				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<input type="checkbox" name="status[<?php echo $this->_tpl_vars['items']['account_id']; ?>
]" id="status" <?php if ($this->_tpl_vars['items']['status'] == 1): ?>checked="true"<?php endif; ?> onClick="return submitform('?hdl=auth/list',<?php echo $this->_tpl_vars['items']['account_id']; ?>
,<?php echo $this->_tpl_vars['page']; ?>
);"/>
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<table cellpadding="0" cellspacing="0" border="0px;">
					<tr>
					  	<td style="border: 0px;padding-right:5px;"><img src="backend/images/edit.gif" width="15" height="15" title="edit" onClick="return submitform('?hdl=auth/regist',<?php echo $this->_tpl_vars['items']['account_id']; ?>
,<?php echo $this->_tpl_vars['page']; ?>
);" class="button" /></td>
						<td style="border: 0px;padding-left:5px;"><img src="backend/images/delete.jpg" width="15" height="15" title="delete" onClick="return ConfirmDeleteRecord('?hdl=auth/list&delete=1',<?php echo $this->_tpl_vars['items']['account_id']; ?>
,<?php echo $this->_tpl_vars['page']; ?>
);" class="button" /></td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endforeach; endif; unset($_from); ?>
			<?php else: ?>
			<tr valign="top">
				<td colspan="7" align="center" style="padding:5px;" valign="middle">
				<?php echo $this->_tpl_vars['message']; ?>

				</td>
			</tr>
			<?php endif; ?>
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="7" style="height:15px;">
					<?php if ($this->_tpl_vars['count'] > 1): ?>
					<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
						<?php echo $this->_tpl_vars['paging']; ?>

					</div>
					<?php endif; ?>
				</td>
			</tr>
			<tr valign="top">
				<td colspan="7" align="center" style="padding:5px;" valign="middle">
					<input type="submit" name="addnew" id="addnew" value="<?php echo $this->_config[0]['vars']['REGIST1']; ?>
" class="button_new" />
				</td>
			</tr>
			</table>
		</form>
		</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>