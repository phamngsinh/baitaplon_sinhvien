<?php /* Smarty version 2.6.18, created on 2011-03-19 17:53:57
         compiled from /home/nhadatan/public_html//backend/templates/support/list.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function ConfirmDeleteRecord(url, supportid, page) {
		var theform = document.frmsupport;
		if (confirm(\'Do you want to delete selected records?\')) {
			theform.action = url;
			theform.supportid.value = supportid;
			theform.delete_.value = supportid;
			theform.page.value = page;
			theform.submit();
		} else return false;
	}
	function submitform(url, supportid, page, status) {
		var theform = document.frmsupport;
		theform.action = url;
		theform.supportid.value = supportid;
		theform.page.value = page;
		theform.status.value = status;
		theform.submit();
	}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frmsupport" id="frmsupport" action="?hdl=support/regist" method="post">
			<input type="hidden" name="supportid" id="supportid" value="" />
			<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
			<input type="hidden" name="status" id="status" value="0" />
			<input type="hidden" name="delete_" id="delete_" value="0" />
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<?php if ($this->_tpl_vars['exefalse'] != ''): ?>
			<tr>
				<td colspan="6" style="color: #FF0000;font-weight: bold;">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;"><?php echo $this->_tpl_vars['exefalse']; ?>
</td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endif; ?>
			<tr>
				<td colspan="6" style="padding: 5px;padding-left: 0px;font-weight: bold;">
					<table cellpadding="0" cellspacing="0">
					<tr>
						<td align="left" valign="middle" style="padding-left: 5px;">
							<?php echo $this->_config[0]['vars']['SUPPORT_ACCOUNT']; ?>
&nbsp;:&nbsp;<input type ="text" name="supportname" id="supportname" value="<?php echo $this->_tpl_vars['searchData']['supportname']; ?>
" class="input_text" />
						</td>
						<td align="left" valign="middle" style="padding-left: 5px;">
							<?php echo $this->_config[0]['vars']['STATUS']; ?>
&nbsp;:&nbsp;
							<select name="supportstatus" id="supportstatus" class="dropdown" style="font-weight:normal;">
								<option value=""><?php echo $this->_config[0]['vars']['SELECTED']; ?>
</option>
								<?php $_from = $this->_tpl_vars['statusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['status']):
?>
								<option value="<?php echo $this->_tpl_vars['status']['value']; ?>
" <?php if ($this->_tpl_vars['searchData']['supportstatus'] == $this->_tpl_vars['status']['value'] && $this->_tpl_vars['searchData']['supportstatus'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['status']['text']; ?>
</option>
								<?php endforeach; endif; unset($_from); ?>
							</select>					
						</td>
						<td align="left" valign="middle" style="padding-left: 5px;">
							<input type="button" name="search" id="search" value="<?php echo $this->_config[0]['vars']['SEARCH']; ?>
" class="button_new" onClick="return submitform('?hdl=support/list', 0, '1', '1');"/>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<!--header-->
			<tr valign="top">
				<td colspan="6" align="center" style="padding:5px;" valign="middle">
					<input type="submit" name="addnew" id="addnew" value="<?php echo $this->_config[0]['vars']['REGIST']; ?>
" class="button_new" />
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td width="4%" align="center" valign="middle" style="height:15px;"><?php echo $this->_config[0]['vars']['FIELD_ID']; ?>
</td>
				<td width="25%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['SUPPORT_ACCOUNT']; ?>
</td>
				<td width="30%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['SUPPORT_TITLE']; ?>
</td>
				<td width="20%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['SUPPORT_GENRE']; ?>
</td>
				<td width="10%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['STATUS']; ?>
</td>
				<td width="10%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['FUNCTION']; ?>
</td>
			</tr>
			<!--If cls = 1 then strClass = "record" else strClass = "evenRecord" end if-->
			<?php if (count ( $this->_tpl_vars['supportList'] ) > 0): ?>
			<?php $_from = $this->_tpl_vars['supportList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['items']):
?>
			<?php $this->assign('i', $this->_tpl_vars['i']+1); ?>
			<?php if ($this->_tpl_vars['i']%2 == 0): ?>
				<?php $this->assign('class', 'record'); ?>
			<?php else: ?>
				<?php $this->assign('class', 'evenRecord'); ?>
			<?php endif; ?>
			<tr class="<?php echo $this->_tpl_vars['class']; ?>
" valign="top">
				<td align="center" style="padding:5px;" valign="middle"><?php echo $this->_tpl_vars['items']['support_id']; ?>
</td>
				<td align="left" style="padding:5px;" valign="middle">
					<?php echo $this->_tpl_vars['items']['support_account']; ?>

				</td>
				<td align="left" style="padding:5px;" valign="middle">
					<?php echo $this->_tpl_vars['items']['support_title']; ?>

				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<?php echo $this->_tpl_vars['genreList'][$this->_tpl_vars['items']['support_genre']]['text']; ?>

				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<?php echo $this->_tpl_vars['statusList'][$this->_tpl_vars['items']['status']]['text']; ?>

				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<table cellpadding="0" cellspacing="0" border="0px;">
					<tr>
					  	<td style="border: 0px;padding-right:5px;"><img src="backend/images/edit.gif" width="15" height="15" title="edit" onClick="return submitform('?hdl=support/regist',<?php echo $this->_tpl_vars['items']['support_id']; ?>
,<?php echo $this->_tpl_vars['page']; ?>
,'2');" class="button" /></td>
						<td style="border: 0px;padding-left:5px;"><img src="backend/images/delete.jpg" width="15" height="15" title="delete" onClick="return ConfirmDeleteRecord('?hdl=support/list',<?php echo $this->_tpl_vars['items']['support_id']; ?>
,<?php echo $this->_tpl_vars['page']; ?>
);" class="button" /></td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endforeach; endif; unset($_from); ?>
			<?php else: ?>
			<tr valign="top">
				<td colspan="6" align="center" style="padding:5px;" valign="middle">
				<?php echo $this->_tpl_vars['message']; ?>

				</td>
			</tr>
			<?php endif; ?>
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="6" valign="middle" style="height:25px;">
					<?php if ($this->_tpl_vars['count'] > 1): ?>
					<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
						<?php echo $this->_tpl_vars['paging']; ?>

					</div>
					<?php endif; ?>
				</td>
			</tr>
			<tr valign="top">
				<td colspan="6" align="center" style="padding:5px;" valign="middle">
					<input type="submit" name="addnew" id="addnew" value="<?php echo $this->_config[0]['vars']['REGIST']; ?>
" class="button_new" />
				</td>
			</tr>
			</table>
		</form>
		</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>