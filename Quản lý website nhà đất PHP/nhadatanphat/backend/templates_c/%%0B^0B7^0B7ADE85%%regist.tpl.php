<?php /* Smarty version 2.6.18, created on 2011-03-25 21:12:24
         compiled from /home/nhadatan/public_html//backend/templates/category/regist.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frmcategory;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frmcategory;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frmcategory" id="frmcategory" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			<?php if ($this->_tpl_vars['message'] != ""): ?>
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;"><?php echo $this->_tpl_vars['message']; ?>
</td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endif; ?>
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;">Update category</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="categoryid" id="categoryid" value="<?php echo $this->_tpl_vars['categoryid']; ?>
" />
					<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						<?php if ($this->_tpl_vars['categoryid'] != ''): ?>
						<tr valign="top">
							<td width="30%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['FIELD_ID']; ?>
: </td>
							<td width="45%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<?php echo $this->_tpl_vars['categoryid']; ?>

							</td>
							<td rowspan="6" width="24%" align="center" valign="middle" class="tdregist">
								<?php if ($this->_tpl_vars['categoryData']['category_icon'] != ""): ?>
								<img border="1" style="border-color:#CAD5DB;" width="200px" src="<?php echo $this->_config[0]['vars']['IMG_CATEGORY_DIR']; ?>
<?php echo $this->_tpl_vars['categoryData']['category_icon']; ?>
" />
								<?php endif; ?>
							</td>
						</tr>
						<?php endif; ?>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['CATEGORY_NAME']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[category_name]" id="object[category_name]" value="<?php echo $this->_tpl_vars['categoryData']['category_name']; ?>
" class="input_text" />
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['CATEGORY_ICON']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type="file" name="category_icon" id="category_icon" class="inputfield" />
								<input type="hidden" name="object[category_icon]" id="object[category_icon]" value="<?php echo $this->_tpl_vars['categoryData']['category_icon']; ?>
" />
								<input type="hidden" name="object[parent_id]" id="object[parent_id]" value="0" />
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['CATEGORY_SORT']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[category_sort]" id="object[category_sort]" value="<?php echo $this->_tpl_vars['categoryData']['category_sort']; ?>
" class="input_text" />			
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['CATEGORY_HORIZONTAL']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[category_horizontal]" id="object[category_horizontal]" class="dropdown">
									<?php $_from = $this->_tpl_vars['Horizonta']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['items']):
?>
									<option value="<?php echo $this->_tpl_vars['items']['value']; ?>
" <?php if ($this->_tpl_vars['categoryData']['category_horizontal'] == $this->_tpl_vars['items']['value']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['items']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['CATEGORY_BOTTOM']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[category_bottom]" id="object[category_bottom]" class="dropdown">
									<?php $_from = $this->_tpl_vars['MNBottom']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['items']):
?>
									<option value="<?php echo $this->_tpl_vars['items']['value']; ?>
" <?php if ($this->_tpl_vars['categoryData']['category_bottom'] == $this->_tpl_vars['items']['value']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['items']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['CATEGORY_VERTICAL']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[category_vertical]" id="object[category_vertical]" class="dropdown">
									<?php $_from = $this->_tpl_vars['Vertical']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['items']):
?>
									<option value="<?php echo $this->_tpl_vars['items']['value']; ?>
" <?php if ($this->_tpl_vars['categoryData']['category_vertical'] == $this->_tpl_vars['items']['value'] && $this->_tpl_vars['categoryData']['category_vertical'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['items']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['CATEGORY_KIND']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[category_kind]" id="object[category_kind]" class="dropdown">
									<?php $_from = $this->_tpl_vars['CateKind']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['items']):
?>
									<option value="<?php echo $this->_tpl_vars['items']['value']; ?>
" <?php if ($this->_tpl_vars['categoryData']['category_kind'] == $this->_tpl_vars['items']['value'] && $this->_tpl_vars['categoryData']['category_kind'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['items']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['STATUS']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[status]" id="object[status]" class="dropdown">
									<?php $_from = $this->_tpl_vars['statusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['status']):
?>
									<option value="<?php echo $this->_tpl_vars['status']['value']; ?>
" <?php if ($this->_tpl_vars['categoryData']['status'] == $this->_tpl_vars['status']['value'] && $this->_tpl_vars['categoryData']['status'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['status']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="<?php echo $this->_config[0]['vars']['UPDATE']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=category/regist', 1)" />
					<input type="button" name="isback" id="isback" value="<?php echo $this->_config[0]['vars']['BACK']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=category/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>