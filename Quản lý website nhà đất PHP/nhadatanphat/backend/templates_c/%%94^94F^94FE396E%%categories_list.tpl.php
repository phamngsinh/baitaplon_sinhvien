<?php /* Smarty version 2.6.18, created on 2011-03-19 17:57:50
         compiled from /home/nhadatan/public_html//backend/templates/interior/categories_list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', '/home/nhadatan/public_html//backend/templates/interior/categories_list.tpl', 24, false),array('modifier', 'indent', '/home/nhadatan/public_html//backend/templates/interior/categories_list.tpl', 53, false),array('modifier', 'cat', '/home/nhadatan/public_html//backend/templates/interior/categories_list.tpl', 64, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<td width="80%" align="left" valign="top">
	<table width="100%" border="1" class="adminform" cellpadding="3"
		cellspacing="3">
		<tr>
			<td colspan="2" height="35" class="textdetail"
				style="padding-left: 5px;padding-top: 15px; padding-bottom: 5px;">
				<a href="?hdl=interior/list"><b><?php echo $this->_config[0]['vars']['INTERIOR_MANAGER']; ?>
</b>
				</a> |
				<a href="?hdl=interior/categories_list"><b><?php echo $this->_config[0]['vars']['CAT_INTERIOR_MANAGER']; ?>
</b>
				</a>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" valign="top">
				<fieldset>
					<legend>
						<?php echo $this->_config[0]['vars']['CAT_INTERIOR_LIST']; ?>

					</legend>
					<form id="frm_categories" name="frm_categories" method="post"
						action="" enctype="application/x-www-form-urlencoded">
						<div>
							<input type="text" name="key_word" class="input_text" size="25"
								maxlength="40" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['key_word'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" />
							<input type="submit" name="btn_search" class="button_new"
								value="<?php echo $this->_config[0]['vars']['SEARCH']; ?>
" />
							<br /><br />							
						</div>
						<table width="100%" border="1" class="dataTable">
							<thead>
								<tr class="stdHeader">
									<td width="5%" align="center">
										Order									</td>
									<td>
										<?php echo $this->_config[0]['vars']['LBL_CAT_NAME']; ?>
									</td>
									<td width="20%" align="center">
										<?php echo $this->_config[0]['vars']['LBL_CAT_DESC']; ?>
									</td>
									<td width="10%" align="center">
										<?php echo $this->_config[0]['vars']['LBL_CAT_ORDER']; ?>
									</td>
									<td width="10%" align="center">
										<?php echo $this->_config[0]['vars']['STATUS']; ?>
									</td>
									<td width="15%" align="center">
										<?php echo $this->_config[0]['vars']['FUNCTION']; ?>
									</td>
								</tr>
							</thead>
							<tbody>
								<?php $_from = $this->_tpl_vars['categories_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['order_no'] => $this->_tpl_vars['categories_obj']):
?>
								<tr <?php if ($this->_tpl_vars['categories_obj']['parent_id'] > 0): ?> class="record" <?php else: ?> class="evenRecord" <?php endif; ?>>
									<td align="center">
										<?php echo $this->_tpl_vars['start']+$this->_tpl_vars['order_no']; ?>
									</td>
									<td valign="top">
										<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['categories_obj']['category_name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)))) ? $this->_run_mod_handler('indent', true, $_tmp, $this->_tpl_vars['categories_obj']['level'], "&nbsp;&nbsp;&nbsp;&nbsp;", "&#166;--&nbsp;") : smarty_modifier_indent($_tmp, $this->_tpl_vars['categories_obj']['level'], "&nbsp;&nbsp;&nbsp;&nbsp;", "&#166;--&nbsp;")); ?>
									</td>
									<td>
										<?php echo ((is_array($_tmp=$this->_tpl_vars['categories_obj']['category_description'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
									</td>
									<td align="center">
										<?php echo ((is_array($_tmp=$this->_tpl_vars['categories_obj']['category_order'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
									</td>
									<td align="center">
										<?php echo $this->_tpl_vars['statusList'][$this->_tpl_vars['categories_obj']['category_active']]['text']; ?>
									</td>
									<td align="center">
										<table cellpadding="0" cellspacing="0" border="0px;">
										<tr>
										  	<td style="border: 0px;padding-right:5px;">
										  		<a href=<?php echo ((is_array($_tmp=$this->_tpl_vars['links_params'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['categories_obj']['category_id']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['categories_obj']['category_id'])); ?>
>
													<img src="backend/images/edit.gif" width="15" height="15"
														class="button" title="Edit" /> </a>
										  	</td>
											<td style="border: 0px;padding-left:5px;">
												<a 	href=<?php echo ((is_array($_tmp=$this->_tpl_vars['links_params_del'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['categories_obj']['category_id']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['categories_obj']['category_id'])); ?>
>
													<img src="backend/images/delete.jpg" width="15" height="15"
														title="Delete" class="button" /> </a>
											</td>
										</tr>
										</table>
									</td>
							    </tr>
								<?php endforeach; endif; unset($_from); ?>
							</tbody>
							<tfoot>
							
							<tr class="stdHeader" valign="top" colspan="6">
							<td align="center" colspan="6" valign="middle" style="height:25px;">
								<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
									<?php echo $this->_tpl_vars['pages_list']; ?>

								</div>								
							</td>
						</tr>							
								<tr>
									<td colspan="6" align="right" valign="middle">
										<input type="button" name="btn_add" id="btn_add"
											value="<?php echo $this->_config[0]['vars']['REGIST1']; ?>
" class="button_new"
											onclick="window.location='?hdl=interior/categories_edit'" />									</td>
								</tr>								
							</tfoot>
					  </table>
					</form>
				</fieldset>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" valign="top">&nbsp;
				
			</td>
		</tr>
	</table>
</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>