<?php /* Smarty version 2.6.18, created on 2011-03-19 17:57:52
         compiled from /home/nhadatan/public_html//backend/templates/interior/categories_edit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strpos', '/home/nhadatan/public_html//backend/templates/interior/categories_edit.tpl', 40, false),array('modifier', 'indent', '/home/nhadatan/public_html//backend/templates/interior/categories_edit.tpl', 41, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function goto_pages(url_) {
		window.location = url_;
	}
</script>
'; ?>

<td width="80%" align="left" valign="top">
<table width="100%" class="adminform">
	<tr>
	  <td  height="35" class="textdetail" style="padding-left: 5px;padding-top: 15px; padding-bottom: 5px;">
	  	<a href="?hdl=interior/list"><b><?php echo $this->_config[0]['vars']['INTERIOR_MANAGER']; ?>
</b></a> | <a href="?hdl=interior/categories_list"><b><?php echo $this->_config[0]['vars']['CAT_INTERIOR_MANAGER']; ?>
</b></a>	  
	  </td>
    </tr>
	<tr>
	  <td colspan="2" valign="top">
	  <fieldset class="adminform">
		<legend><?php echo $this->_config[0]['vars']['LBL_CAT_ADD']; ?>
</legend>
		<form id="frm_edit" name="frm_edit" action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="category_id" id="category_id" value="<?php echo $this->_tpl_vars['categories_obj']['category_id']; ?>
" />
		<?php if ($this->_tpl_vars['msg_system_errors'] == ''): ?>
		<input type="hidden" name="categories_obj[category_id]" id="categories_obj[category_id]" value="<?php echo $this->_tpl_vars['categories_obj']['category_id']; ?>
" />
		<table cellspacing="1" width="100%" class="admintable" bgcolor="#FFFFFF">
		  <?php if ($this->_tpl_vars['msg_errors']): ?>
		  <tr>
			<td colspan="2" style="padding-left:5px;" class="error"><?php echo $this->_tpl_vars['msg_errors']; ?>
</td>
		  </tr>
		  <?php endif; ?>
		  <tr>
			<td class="key"><?php echo $this->_config[0]['vars']['LBL_CAT_NAME']; ?>
</td>
			<td width="80%"><input type="text" name="categories_obj[category_name]" value="<?php echo $this->_tpl_vars['categories_obj']['category_name']; ?>
" size="25" maxlength="255" /></td>
		  </tr>          
		  <tr>
			<td class="key"><?php echo $this->_config[0]['vars']['LBL_CAT_SUB']; ?>
</td>
			<td>
			<select name="categories_obj[parent_id]">
            <option value="0"><?php echo $this->_config[0]['vars']['LBL_MAIN_MENU']; ?>
</option>
            <?php $_from = $this->_tpl_vars['category_parent_id_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['cat']):
?>
			<?php if (((is_array($_tmp=$this->_tpl_vars['cat']['id_path'])) ? $this->_run_mod_handler('strpos', true, $_tmp, ($this->_tpl_vars['categories_obj']['id_path'])."/") : strpos($_tmp, ($this->_tpl_vars['categories_obj']['id_path'])."/")) === false && $this->_tpl_vars['cat']['category_id'] != $this->_tpl_vars['categories_obj']['category_id'] || $this->_tpl_vars['categories_obj']['category_id'] == ''): ?>
			<option	value="<?php echo $this->_tpl_vars['cat']['category_id']; ?>
" <?php if ($this->_tpl_vars['categories_obj']['parent_id'] == $this->_tpl_vars['cat']['category_id']): ?>selected<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['cat']['category_name'])) ? $this->_run_mod_handler('indent', true, $_tmp, $this->_tpl_vars['cat']['level'], "&#166;&nbsp;&nbsp;&nbsp;&nbsp;", "&#166;--&nbsp;&nbsp;") : smarty_modifier_indent($_tmp, $this->_tpl_vars['cat']['level'], "&#166;&nbsp;&nbsp;&nbsp;&nbsp;", "&#166;--&nbsp;&nbsp;")); ?>
</option>
			<?php endif; ?>
			<?php endforeach; endif; unset($_from); ?>
            </select>            </td>
		  </tr>
                    
		  <tr>
			<td class="key"><?php echo $this->_config[0]['vars']['LBL_CAT_ORDER']; ?>
</td>
			<td><input type="text" name="categories_obj[category_order]" value="<?php echo $this->_tpl_vars['categories_obj']['category_order']; ?>
" size="5" maxlength="5" /></td>
		  </tr>
		  
		  <tr>
			<td class="key"><?php echo $this->_config[0]['vars']['STATUS']; ?>
</td>
			<td>
			<select name="categories_obj[category_active]" class="dropdown">
			<?php $_from = $this->_tpl_vars['statusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['status']):
?>
			<option value="<?php echo $this->_tpl_vars['status']['value']; ?>
" <?php if ($this->_tpl_vars['categories_obj']['category_active'] == $this->_tpl_vars['status']['value'] && $this->_tpl_vars['categories_obj']['category_active'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['status']['text']; ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
			</select>
			
			</td>
		  </tr>
		  <tr valign="middle">
			<td align="right"><input type="submit" align="left" name="btn_save" id="btn_add" value="<?php echo $this->_config[0]['vars']['UPDATE']; ?>
" class="button_new"/></td>
			<td align="left"><input type="button" name="btn_add" id="btn_add" value="<?php echo $this->_config[0]['vars']['BACK']; ?>
" class="button_new" onclick="javascript:goto_pages('<?php echo $this->_tpl_vars['link_back']; ?>
');"/></td>
		  </tr>
		</table>
		<?php else: ?>
		<div align="center" class="nt_txt_validator"><?php echo $this->_tpl_vars['msg_system_errors']; ?>
</div>
		<?php endif; ?>	
		</form>
	  </fieldset>      </td>
  </tr>
</table>
</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>