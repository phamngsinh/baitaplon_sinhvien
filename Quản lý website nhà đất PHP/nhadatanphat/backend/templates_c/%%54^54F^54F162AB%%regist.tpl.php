<?php /* Smarty version 2.6.18, created on 2011-03-19 18:00:08
         compiled from /home/nhadatan/public_html//backend/templates/company/regist.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frmcompany;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frmcompany;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frmcompany" id="frmcompany" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			<?php if ($this->_tpl_vars['message'] != ""): ?>
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;"><?php echo $this->_tpl_vars['message']; ?>
</td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endif; ?>
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;">Update company</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="companyid" id="companyid" value="<?php echo $this->_tpl_vars['companyid']; ?>
" />
					<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						<?php if ($this->_tpl_vars['companyid'] != ''): ?>
						<tr valign="top">
							<td width="30%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['FIELD_ID']; ?>
: </td>
							<td width="45%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<?php echo $this->_tpl_vars['companyid']; ?>

							</td>
							<td rowspan="7" width="24%" align="center" valign="middle" class="tdregist">
								<?php if ($this->_tpl_vars['companyData']['company_logo'] != ""): ?>
								<img border="1" style="border-color:#CAD5DB;" width="100px" height="100px" src="<?php echo $this->_config[0]['vars']['IMG_COMPANY_DIR']; ?>
thumb_<?php echo $this->_tpl_vars['companyData']['company_logo']; ?>
" />
								<?php endif; ?>
							</td>
						</tr>
						<?php endif; ?>
						<tr>
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['COMPANY_PROVINCE']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<select name="object[province_id]" id="object[province_id]" class="dropdown">
									<?php $_from = $this->_tpl_vars['provinceList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['provinces']):
?>
									<option value="<?php echo $this->_tpl_vars['provinces']['province_id']; ?>
" <?php if ($this->_tpl_vars['companyData']['province_id'] == $this->_tpl_vars['provinces']['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['provinces']['province_name']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['COMPANY_NAME']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[company_name]" id="object[company_name]" value="<?php echo $this->_tpl_vars['companyData']['company_name']; ?>
" class="input_text" />
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['COMPANY_BANNER']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type="file" name="company_logo" id="company_logo" class="inputfield" />
								<input type="hidden" name="object[company_logo]" id="object[company_logo]" value="<?php echo $this->_tpl_vars['companyData']['company_logo']; ?>
" />
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['COMPANY_WEBSITE']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[company_website]" id="object[company_website]" value="<?php echo $this->_tpl_vars['companyData']['company_website']; ?>
" class="input_text" />			
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['COMPANY_CONTENT']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<?php echo $this->_tpl_vars['com_content']; ?>
		
							</td>
						</tr>
						
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">
								Hiển thị trên trang chủ
							</td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<input type="checkbox" name="object[is_show_in_homepage]" 
									<?php if ($this->_tpl_vars['companyData']['is_show_in_homepage'] == '1'): ?> checked <?php endif; ?>
								/>
							</td>
						</tr>

						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">
								Hiển thị trang tỉnh thành
							</td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<input type="checkbox" name="object[is_show_in_province]" 
									<?php if ($this->_tpl_vars['companyData']['is_show_in_province'] == '1'): ?> checked <?php endif; ?>
								/>
							</td>
						</tr>

						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['STATUS']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[status]" id="object[status]" class="dropdown">
									<?php $_from = $this->_tpl_vars['statusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['status']):
?>
									<option value="<?php echo $this->_tpl_vars['status']['value']; ?>
" <?php if ($this->_tpl_vars['companyData']['status'] == $this->_tpl_vars['status']['value'] && $this->_tpl_vars['companyData']['status'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['status']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="<?php echo $this->_config[0]['vars']['UPDATE']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=company/regist', 1)" />
					<input type="button" name="isback" id="isback" value="<?php echo $this->_config[0]['vars']['BACK']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=company/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>