<?php $_config_vars = array (
  'PROMOTE_NAME' => 'Tên nhà quảng cáo',
  'PROMOTE_BANNER' => 'Banner',
  'PROMOTE_CONTENT' => 'Nội dung quảng cáo',
  'PROMOTE_WEBSITE' => 'Website quảng cáo',
  'PROMOTE_POSITION' => 'Vị trí',
  'PROMOTE_CLICKED' => 'Số lần Click',
  'PROMOTE_START' => 'Ngày bắt đầu',
  'PROMOTE_END' => 'Ngày kết thúc',
  'PROVINCE_NAME' => 'Tên Tỉnh/Thành',
); ?>