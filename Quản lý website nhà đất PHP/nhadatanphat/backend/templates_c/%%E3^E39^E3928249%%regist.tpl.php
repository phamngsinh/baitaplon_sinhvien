<?php /* Smarty version 2.6.18, created on 2011-03-19 17:52:46
         compiled from /home/nhadatan/public_html//backend/templates/auth/regist.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function submitform(value) {
		var theform = document.frmaccount;
		theform.issubmit.value = value;
		theform.submit();
	}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frmaccount" id="frmaccount" action="" method="post">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="2" style="height:15px;">
					Update account admin
				</td>
			</tr>
			<?php if ($this->_tpl_vars['message'] != ""): ?>
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;"><?php echo $this->_tpl_vars['message']; ?>
</td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endif; ?>
			<tr valign="top">
				<td width="30%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['ACCOUNT_ID']; ?>
: </td>
				<td width="69%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;">
					<?php echo $this->_tpl_vars['accountid']; ?>

					<input type="hidden" name="account_id" id="account_id" value="<?php echo $this->_tpl_vars['accountid']; ?>
" />
					<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_NAME']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<?php if ($this->_tpl_vars['accountid'] == ""): ?>
					<input type="text" name="account_name" id="account_name" value="<?php echo $this->_tpl_vars['accountData']['account_name']; ?>
" class="input_text" />
					<?php else: ?>
					<input type="hidden" name="account_name" id="account_name" value="<?php echo $this->_tpl_vars['accountData']['account_name']; ?>
" class="input_text" />
					<?php echo $this->_tpl_vars['accountData']['account_name']; ?>

					<?php endif; ?>
				</td>
			</tr>
			
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">Quyền người dùng: </td>
				<td align="left" valign="middle" class="tdregist">
					<select name="role_id" id="role_id" class="dropdown">
					<?php $_from = $this->_tpl_vars['userRole']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
						<option value="<?php echo $this->_tpl_vars['item']['value']; ?>
" <?php if ($this->_tpl_vars['accountData']['role_id'] == $this->_tpl_vars['item']['value']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['item']['text']; ?>
</option>
					<?php endforeach; endif; unset($_from); ?>
					</select>
				</td>
			</tr>
			
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_PASS']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="password" name="account_pass" id="account_pass" value="" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_PASS_CONFIRM']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="password" name="account_pass_confirm" id="account_pass_confirm" value="" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_FULL_NAME']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="text" name="account_full_name" id="account_full_name" value="<?php echo $this->_tpl_vars['accountData']['account_full_name']; ?>
" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_SEX']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<select name="account_sex" id="account_sex" class="dropdown">
					<?php $_from = $this->_tpl_vars['sexList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sex']):
?>
						<option value="<?php echo $this->_tpl_vars['sex']['value']; ?>
" <?php if ($this->_tpl_vars['accountData']['account_sex'] == $this->_tpl_vars['sex']['value']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['sex']['text']; ?>
</option>
					<?php endforeach; endif; unset($_from); ?>
					</select>					
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_EMAIL']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="text" name="account_email" id="account_email" value="<?php echo $this->_tpl_vars['accountData']['account_email']; ?>
" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_ADDRESS']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="text" name="account_address" id="account_address" value="<?php echo $this->_tpl_vars['accountData']['account_address']; ?>
" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_PHONE']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="text" name="account_phone" id="account_phone" value="<?php echo $this->_tpl_vars['accountData']['account_phone']; ?>
" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_MOBILE']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="text" name="account_mobile" id="account_mobile" value="<?php echo $this->_tpl_vars['accountData']['account_mobile']; ?>
" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['STATUS']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<select name="status" id="status" class="dropdown">
						<?php $_from = $this->_tpl_vars['statusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['status']):
?>
						<option value="<?php echo $this->_tpl_vars['status']['value']; ?>
" <?php if ($this->_tpl_vars['accountData']['status'] == $this->_tpl_vars['status']['value'] && $this->_tpl_vars['accountData']['status'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['status']['text']; ?>
</option>
						<?php endforeach; endif; unset($_from); ?>
					</select>					
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="2" style="height:15px;">
				</td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="<?php echo $this->_config[0]['vars']['UPDATE']; ?>
" class="button_new" onclick="javascript: submitform(1)" />
					<input type="button" name="isback" id="isback" value="<?php echo $this->_config[0]['vars']['BACK']; ?>
" class="button_new" onclick="javascript: submitform(0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>