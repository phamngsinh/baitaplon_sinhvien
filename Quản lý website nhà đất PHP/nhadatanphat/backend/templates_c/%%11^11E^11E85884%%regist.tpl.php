<?php /* Smarty version 2.6.18, created on 2011-04-09 19:50:12
         compiled from /home/nhadatan/public_html//backend/templates/news/regist.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frmnews;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frmnews;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frmnews" id="frmnews" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			<?php if ($this->_tpl_vars['message'] != ""): ?>
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;"><?php echo $this->_tpl_vars['message']; ?>
</td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endif; ?>
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;">Update news</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="newsid" id="newsid" value="<?php echo $this->_tpl_vars['newsid']; ?>
" />
					<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						<?php if ($this->_tpl_vars['newsid'] != ''): ?>
						<tr valign="top">
							<td width="20%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['FIELD_ID']; ?>
: </td>
							<td width="55%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<?php echo $this->_tpl_vars['newsid']; ?>

							</td>
							<td rowspan="3" width="24%" align="center" valign="middle" class="tdregist">
								<?php if ($this->_tpl_vars['newsData']['news_image'] != ""): ?>
								<img border="1" style="border-color:#CAD5DB;" width="100px" height="100px" src="<?php echo $this->_config[0]['vars']['IMG_NEWS_DIR']; ?>
thumb_<?php echo $this->_tpl_vars['newsData']['news_image']; ?>
" />
								<?php endif; ?>
							</td>
						</tr>
						<?php endif; ?>
						<tr valign="top">
							<td width="20%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['NEWS_NAME']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[news_name]" id="object[news_name]" value="<?php echo $this->_tpl_vars['newsData']['news_name']; ?>
" class="input_text" />
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['NEWS_TITLE']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<textarea name="object[news_title]" id="object[news_title]" class="textarea"><?php echo $this->_tpl_vars['newsData']['news_title']; ?>
</textarea>
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['NEWS_IMAGE']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist" colspan="3">
								<input type="file" name="news_image" id="news_image" class="inputfield" />
								<input type="hidden" name="object[news_image]" id="object[news_image]" value="<?php echo $this->_tpl_vars['newsData']['news_image']; ?>
" />
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['NEWS_CONTENT']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist" colspan="3">
									<?php echo $this->_tpl_vars['news_content']; ?>

							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['NEWS_GENRE']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								<select name="object[news_genre]" id="object[news_genre]" class="dropdown">
									<?php $_from = $this->_tpl_vars['newsGenre']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['genres']):
?>
									<option value="<?php echo $this->_tpl_vars['genres']['value']; ?>
" <?php if ($this->_tpl_vars['newsData']['news_genre'] == $this->_tpl_vars['genres']['value'] && $this->_tpl_vars['newsData']['news_genre'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['genres']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['STATUS']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								<select name="object[status]" id="object[status]" class="dropdown">
									<?php $_from = $this->_tpl_vars['statusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['status']):
?>
									<option value="<?php echo $this->_tpl_vars['status']['value']; ?>
" <?php if ($this->_tpl_vars['newsData']['status'] == $this->_tpl_vars['status']['value'] && $this->_tpl_vars['newsData']['status'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['status']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="<?php echo $this->_config[0]['vars']['UPDATE']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=news/regist', 1)" />
					<input type="button" name="isback" id="isback" value="<?php echo $this->_config[0]['vars']['BACK']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=news/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>