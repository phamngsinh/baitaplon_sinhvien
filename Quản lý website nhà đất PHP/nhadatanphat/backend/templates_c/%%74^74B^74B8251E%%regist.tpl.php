<?php /* Smarty version 2.6.18, created on 2011-03-19 18:19:24
         compiled from /home/nhadatan/public_html//backend/templates/estate/regist.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'money_format', '/home/nhadatan/public_html//backend/templates/estate/regist.tpl', 88, false),array('modifier', 'date_format', '/home/nhadatan/public_html//backend/templates/estate/regist.tpl', 113, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frmestate;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frmestate;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frmestate" id="frmestate" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			<?php if ($this->_tpl_vars['message'] != ""): ?>
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;"><?php echo $this->_tpl_vars['message']; ?>
</td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endif; ?>
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;">Update estate</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="estateid" id="estateid" value="<?php echo $this->_tpl_vars['estateid']; ?>
" />
					<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						<tr valign="top">
							<td width="24%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['FIELD_ID']; ?>
: </td>
							<td width="75%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<?php echo $this->_tpl_vars['estateid']; ?>

							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ESTATE_TITLE']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<?php echo $this->_tpl_vars['estateData']['estate_title']; ?>

							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ESTATE_CATEGORY']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<?php echo $this->_tpl_vars['estateData']['category_name']; ?>
<?php echo $this->_tpl_vars['categoryName']; ?>

							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ESTATE_ADDRESS']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<?php echo $this->_tpl_vars['estateData']['estate_address']; ?>
-<?php echo $this->_tpl_vars['estateData']['district_name']; ?>
-<?php echo $this->_tpl_vars['estateData']['province_name']; ?>

							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ESTATE_AREA']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<?php echo $this->_tpl_vars['estateData']['estate_area']; ?>

							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ESTATE_DERECTION']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<?php echo $this->_tpl_vars['directionList'][$this->_tpl_vars['estateData']['estate_direction']]['text']; ?>

							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ESTATE_PRICE']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<?php if ($this->_tpl_vars['estateData']['estate_price'] > 0): ?>
                           			<?php echo ((is_array($_tmp=$this->_tpl_vars['estateData']['estate_price'])) ? $this->_run_mod_handler('money_format', true, $_tmp, ".") : smarty_modifier_money_format($_tmp, ".")); ?>
&nbsp;
                           			
                           			<?php if ($this->_tpl_vars['estateData']['estate_price_type'] == 0): ?>
                           				VNĐ 
                           			<?php elseif ($this->_tpl_vars['estateData']['estate_price_type'] == 1): ?>
                           				SJC 
                           			<?php else: ?>
                           				USD 
                           			<?php endif; ?>
                           			<?php if ($this->_tpl_vars['estateData']['is_price_in_m2'] == '1'): ?>
                           			/m2
                           			<?php endif; ?>
                           			
                           			
                           			<?php if ($this->_tpl_vars['estateData']['is_price_negotiable'] == '1'): ?>
                           				(Có thể thỏa thuận)
                           			<?php endif; ?>
                           		<?php else: ?>
                           			(Thỏa thuận)
                           		<?php endif; ?>
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ESTATE_CREATE']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<?php echo ((is_array($_tmp=$this->_tpl_vars['estateData']['created_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d-%m-%Y") : smarty_modifier_date_format($_tmp, "%d-%m-%Y")); ?>

							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ESTATE_DURATION']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<?php echo ((is_array($_tmp=$this->_tpl_vars['estateData']['estate_duration'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d-%m-%Y") : smarty_modifier_date_format($_tmp, "%d-%m-%Y")); ?>

							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ESTATE_DELETE']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<?php echo ((is_array($_tmp=$this->_tpl_vars['estateData']['deleted_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d-%m-%Y") : smarty_modifier_date_format($_tmp, "%d-%m-%Y")); ?>

							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ESTATE_CONTENT']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<?php echo $this->_tpl_vars['estateData']['estate_note']; ?>

							</td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								File Video
							</td>
							<td align="left" valign="middle" class="tdregist">
								<?php if ($this->_tpl_vars['estateData']['estate_video'] != ''): ?>
									<object classid="CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95" 
											codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715" 
											standby="Loading Microsoft Windows Media Player components..." type="application/x-oleobject"
											viewastext="" width="190" height="190"
										>
										<param name="FileName" value="<?php echo $this->_config[0]['vars']['AUD_ESTATE_DIR']; ?>
<?php echo $this->_tpl_vars['estateData']['estate_video']; ?>
">
										<param name="TransparentAtStart" value="true">
										<param name="AutoStart" value="true">
										<param name="AnimationatStart" value="false">
										<param name="ShowControls" value="true">
										<param name="ShowDisplay" value="false">
										<param name="playCount" value="999">
										<param name="displaySize" value="0">
										<param name="Volume" value="100">
										<embed 	type="application/x-mplayer2" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" 
											src="<?php echo $this->_config[0]['vars']['AUD_ESTATE_DIR']; ?>
<?php echo $this->_tpl_vars['estateData']['estate_video']; ?>
" name="MediaPlayer" transparentatstart="0" 
											autostart="1" playcount="999" volume="100animationAtStart=0" displaysize="0" width="220" height="220">
									</object>
            					<?php endif; ?>
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">Ảnh minh họa: </td>
							<td align="left" valign="middle" class="tdregist">
								<?php if ($this->_tpl_vars['estateData']['estate_image1'] != ''): ?>
		                        <img src="<?php echo $this->_config[0]['vars']['IMG_ESTATE_DIR']; ?>
thumb_<?php echo $this->_tpl_vars['estateData']['estate_image1']; ?>
" width="150" class="border_img_white" />
		                        <?php endif; ?>
		                        <?php if ($this->_tpl_vars['estateData']['estate_image2'] != ''): ?>
		                        <img src="<?php echo $this->_config[0]['vars']['IMG_ESTATE_DIR']; ?>
thumb_<?php echo $this->_tpl_vars['estateData']['estate_image2']; ?>
" width="150" class="border_img_white" />
		                        <?php endif; ?>
		                        <?php if ($this->_tpl_vars['estateData']['estate_image3'] != ''): ?>
		                        <img src="<?php echo $this->_config[0]['vars']['IMG_ESTATE_DIR']; ?>
thumb_<?php echo $this->_tpl_vars['estateData']['estate_image3']; ?>
" width="150" class="border_img_white" />
		                        <?php endif; ?>
		                        <?php if ($this->_tpl_vars['estateData']['estate_image4'] != ''): ?>
		                        <img src="<?php echo $this->_config[0]['vars']['IMG_ESTATE_DIR']; ?>
thumb_<?php echo $this->_tpl_vars['estateData']['estate_image4']; ?>
" width="150" class="border_img_white" />
		                        <?php endif; ?>
		                        <?php if ($this->_tpl_vars['estateData']['estate_image5'] != ''): ?>
		                        <img src="<?php echo $this->_config[0]['vars']['IMG_ESTATE_DIR']; ?>
thumb_<?php echo $this->_tpl_vars['estateData']['estate_image5']; ?>
" width="150" class="border_img_white" />
		                        <?php endif; ?>
							</td>
						</tr>
				<!--
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">Thông tin liên hệ: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
               					<table width="98%" border="0" cellpadding="0" cellspacing="0">
               					<tr>
               						<td height="20" width="15%" align="left" valign="middle" style="font-weight: bold;">Họ và tên</td>
               						<td align="left">
               							:&nbsp;<?php echo $this->_tpl_vars['estateData']['contact_person']; ?>

               						</td>
               						<td height="20" width="15%" align="left" valign="middle" style="font-weight: bold;">Địa chỉ email</td>
               						<td align="left">
               							:&nbsp;<?php echo $this->_tpl_vars['estateData']['contact_mail']; ?>

               						</td>
               					</tr>
               					<tr>
               						<td height="20" width="15%" align="left" valign="middle" style="font-weight: bold;">Tên công ty</td>
               						<td align="left">
               							:&nbsp;<?php echo $this->_tpl_vars['estateData']['contact_company']; ?>

               						</td>
               						<td height="20" width="15%" align="left" valign="middle" style="font-weight: bold;">Số điện thoại</td>
               						<td align="left">
               							:&nbsp;<?php echo $this->_tpl_vars['estateData']['contact_phone']; ?>

               						</td>
               					</tr>
               					<tr>
               						<td height="20" width="15%" align="left" valign="middle" style="font-weight: bold;">Địa chỉ liên lạc</td>
               						<td align="left">
               							:&nbsp;<?php echo $this->_tpl_vars['estateData']['contact_address']; ?>

               						</td>
               						<td height="20" width="15%" align="left" valign="middle" style="font-weight: bold;">Website</td>
               						<td align="left">
               							:&nbsp;<?php echo $this->_tpl_vars['estateData']['contact_website']; ?>

               						</td>
               					</tr>
               					</table>
							</td>
						</tr>
					-->
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">Người đăng tin: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
               					<table width="98%" border="0" cellpadding="0" cellspacing="0">
               					<tr>
               						<td style="padding-right: 5px;">
               							<?php if ($this->_tpl_vars['logoImg'] != ""): ?>
											<img src="<?php echo $this->_config[0]['vars']['IMG_MEMBER_LOGO_DIR']; ?>
<?php echo $this->_tpl_vars['logoImg']; ?>
" width="150" class="border_img_white" />
										<?php endif; ?>
               						</td>
               						<td width="100%" align="left">
               							<table width="100%" border="0" cellpadding="0" cellspacing="0">
                 					<tr>
                 						<td height="19" width="30%" align="left" valign="middle" style="font-weight: bold;">Công ty / Cá nhân</td>
                 						<td align="left">
                 							:&nbsp;<?php echo $this->_tpl_vars['estateData']['member_name']; ?>

                 						</td>
                 					</tr>
                 					<tr>
                 						<td height="19" width="15%" align="left" valign="middle" style="font-weight: bold;">Địa chỉ email</td>
                 						<td align="left">
                 							:&nbsp;<a href="mailto:<?php echo $this->_tpl_vars['estateData']['member_mail']; ?>
" style="color: #D70B2E;"><?php echo $this->_tpl_vars['estateData']['member_mail']; ?>
</a>
                 						</td>
                 					</tr>
                 					<tr>
                 						<td height="19" width="15%" align="left" valign="middle" style="font-weight: bold;">Số điện thoại</td>
                 						<td align="left">
                 							:&nbsp;<?php echo $this->_tpl_vars['estateData']['member_phone']; ?>

                 						</td>
                 					</tr>
                 					<tr>
                 						<td height="19" width="15%" align="left" valign="middle" style="font-weight: bold;">Địa chỉ liên lạc</td>
                 						<td align="left">
                 							:&nbsp;<?php echo $this->_tpl_vars['estateData']['member_address']; ?>

                 						</td>
                 					</tr>
                 					<tr>
                 						<td height="19" width="15%" align="left" valign="middle" style="font-weight: bold;">Website</td>
                 						<td align="left">
                 							:&nbsp;<a href="<?php echo $this->_tpl_vars['estateData']['member_website']; ?>
" target="_blank" style="color: #0000FF;"><?php echo $this->_tpl_vars['estateData']['member_website']; ?>
</a>
                 						</td>
                 					</tr>
               							</table>
               						</td>
               					</tr>
               					</table>
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['ESTATE_GENRE']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								<?php echo $this->_tpl_vars['genreList'][$this->_tpl_vars['estateData']['estate_genre']]; ?>

							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['STATUS']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								<?php echo $this->_tpl_vars['statusList'][$this->_tpl_vars['estateData']['estate_status']]; ?>

							</td>
						</tr>
						
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">
								Loại tin:
							</td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								<?php if ($this->_tpl_vars['estateData']['is_fee_estate'] == '1'): ?>
									Tin có phí
								<?php else: ?>
									Tin miễn phí
								<?php endif; ?>
							</td>
						</tr>
						
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">
								Trạng thái tin của member
							</td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								<?php if ($this->_tpl_vars['estateData']['is_deleted_by_member'] == '0'): ?>
									Đang hoạt động
								<?php else: ?>
									<font color='red'>
										Tin này đã bị xóa
									</font>
								<?php endif; ?>
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">
								Số lần xem
							</td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								<?php echo $this->_tpl_vars['estateData']['viewed']; ?>

							</td>
						</tr>
						
					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="<?php echo $this->_config[0]['vars']['EDIT']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=estate/post', 0)" />
					<input type="button" name="isback" id="isback" value="<?php echo $this->_config[0]['vars']['BACK']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=estate/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
 <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>