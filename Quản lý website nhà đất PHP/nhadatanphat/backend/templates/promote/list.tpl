{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">

	function ConfirmDeleteRecord(url, promoteid, page) {
		var theform = document.frmpromote;
		if (confirm('Do you want to delete selected records?')) {
			theform.action = url;
			theform.promoteid.value = promoteid;
			theform.delete_.value = promoteid;
			theform.page.value = page;
			theform.submit();
		} else return false;
	}
	function submitform(url, promoteid, page, status) {
		var theform = document.frmpromote;
		theform.action = url;
		theform.promoteid.value = promoteid;
		theform.page.value = page;
		theform.status.value = status;
		theform.submit();
	}
</script>
{/literal}

<script language="javascript">
	var page = "{$page}";
	var status = "{$status}";
	{literal}
	function deleteRecord(){
		var theform = document.frmpromote;
		theform.action = '?hdl=promote/list';
		theform.delete_.value = "1";
		theform.page.value = page;
		theform.submit();
	}
	
	function checkAll(fmobj) {
	  var status = fmobj.checkStatus.value;
	  var checkStat = (status == "1"?true:false);

	  for (var i=0;i<fmobj.elements.length;i++) {
	    var e = fmobj.elements[i];
	    if (e.type == 'checkbox') {
	      e.checked = checkStat;
	    }
	  }
	  
	  fmobj.checkStatus.value = (fmobj.checkStatus.value=="1"?"0":"1");
	}
	{/literal}
</script>

{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frmpromote" id="frmpromote" action="?hdl=promote/regist" method="post">
			<input type="hidden" name="promoteid" id="promoteid" value="" />
			<input type="hidden" name="page" id="page" value="{$page}" />
			<input type="hidden" name="status" id="status" value="0" />
			<input type="hidden" name="delete_" id="delete_" value="0" />
			<input type="hidden" name="checkStatus" value="1" />
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="10"></td>
			</tr>
			{if $exefalse != ''}
			<tr>
				<td colspan="10" style="color: #FF0000;font-weight: bold;">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;">{$exefalse}</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr>
				<td colspan="10" style="padding: 5px;padding-left: 0px;font-weight: bold;">
					<table cellpadding="0" cellspacing="0">
					<tr>
						<td align="left" valign="middle" style="padding-left: 5px;">
							{#PROMOTE_NAME#}&nbsp;:&nbsp;<input type ="text" name="promotename" id="promotename" value="{$searchData.promotename}" class="input_text" />
						</td>
						<td align="left" valign="middle" style="padding-left: 5px;">
							{#STATUS#}&nbsp;:&nbsp;
							<select name="promotestatus" id="promotestatus" class="dropdown">
								<option value="">{#SELECTED#}</option>
								{foreach item=status from=$statusList}
								<option value="{$status.value}" {if $searchData.promotestatus eq $status.value &&  $searchData.promotestatus ne ''}selected="selected"{/if}>{$status.text}</option>
								{/foreach}
							</select>					
						</td>
						<td align="left" valign="middle" style="padding-left: 5px;">
							<input type="button" name="search" id="search" value="{#SEARCH#}" class="button_new" onClick="return submitform('?hdl=promote/list', 0, '1', '1');"/>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<!--header-->
			
			<tr valign="top">
				<td width="" align="right" valign="middle" style="height:15px;" colspan="8">
					<img src="backend/images/delete.jpg" width="15" height="15" 
						title="Xóa theo checkbox đã chọn" 
						onClick="if(confirm('Xóa các bản ghi đã chọn ở bảng?')) deleteRecord();"
						class="button" />
				</td>
			</tr>
			
			<tr class="stdHeader" valign="top">
				<td width="4%" align="center" valign="middle" style="height:15px;">{#FIELD_ID#}</td>
				<td width="15%" align="center" valign="middle">{#PROMOTE_NAME#}</td>
				<td width="10%" align="center" valign="middle">{#PROMOTE_START#}</td>
				<td width="10%" align="center" valign="middle">{#PROMOTE_END#}</td>
			<!--
				<td width="8%" align="center" valign="middle">{#PROMOTE_CLICKED#}</td>
			-->
				<td width="15%" align="center" valign="middle">{#PROMOTE_WEBSITE#}</td>
				<td width="12%" align="center" valign="middle">{#PROMOTE_POSITION#}</td>
				<td width="8%" align="center" valign="middle">{#STATUS#}</td>
				<td width="8%" align="center" valign="middle">
					Sửa/<a onClick="checkAll(document.frmpromote);" style="cursor:pointer;">Chọn</a>
				</td>
			</tr>
			<!--If cls = 1 then strClass = "record" else strClass = "evenRecord" end if-->
			{if count($promoteList) gt 0}
			{foreach key=key item=items from=$promoteList}
			{assign var="i" value=$i+1}
			{if $i%2 eq 0}
				{assign var="class" value="record"}
			{else}
				{assign var="class" value="evenRecord"}
			{/if}
			<tr class="{$class}" valign="top">
				<td align="center" style="padding:5px;" valign="middle"  width="20">
					{$items.promote_id}
				</td>
				<td align="left" style="padding:5px;" valign="middle" width="">
					{$items.promote_name}
				</td>
				<td align="center" style="padding:5px;" valign="middle" width="20">
					{$items.promote_start|date_format:"%d-%m-%Y"}
				</td>
				<td align="center" style="padding:5px;" valign="middle" width="20">
					{$items.promote_end|date_format:"%d-%m-%Y"}
				</td>
			<!--
				<td align="center" style="padding:5px;" valign="middle">
					{$items.promote_click}
				</td>
			-->
				<td align="left" style="padding:5px;" valign="middle" width="70">
					{$items.promote_website}
				</td>
				<td align="center" style="padding:5px;" valign="middle" width="40">
					{$positionList[$items.promote_position].text}
				</td>
				<td align="center" style="padding:5px;" valign="middle" width="30">
					{$statusList[$items.status].text}
				</td>
				<td align="center" style="padding:5px;" valign="middle" width="20">
					<table cellpadding="0" cellspacing="0" border="0px;">
					<tr>
					  	<td style="border: 0px;padding-right:5px;">
					  		<img src="backend/images/edit.gif" width="15" height="15" title="edit" onClick="return submitform('?hdl=promote/regist',{$items.promote_id},{$page},'2');" class="button" />
					  	</td>
						<td style="border: 0px;padding-left:5px;">
							<!--
								<img src="backend/images/delete.jpg" width="15" height="15" title="delete" onClick="return ConfirmDeleteRecord('?hdl=promote/list',{$items.promote_id},{$page});" class="button" />
							-->
							<input type="checkbox" name="deleteId[]" value="{$items.promote_id}" />
						</td>
					</tr>
					</table>
				</td>
			</tr>
			{/foreach}
			{else}
			<tr valign="top">
				<td colspan="10" align="center" style="padding:5px;" valign="middle">
				{$message}
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="10" valign="middle" style="height:25px;">
					{if $count gt 1}
					<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
						{$paging}
					</div>
					{/if}
				</td>
			</tr>
			<tr valign="top">
				<td colspan="10" align="center" style="padding:5px;" valign="middle">
					<input type="submit" name="addnew" id="addnew" value="{#REGIST#}" class="button_new" />
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}