{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frmpromote;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frmpromote;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frmpromote" id="frmpromote" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			{if $message != ""}
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;">{$message}</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;">Update promote</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="promoteid" id="promoteid" value="{$promoteid}" />
					<input type="hidden" name="page" id="page" value="{$page}" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						{if $promoteid != ''}
						<tr valign="top">
							<td width="30%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#PROMOTE_ID#}: </td>
							<td width="45%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								{$promoteid}
							</td>
							<td rowspan="5" width="24%" align="center" valign="middle" class="tdregist">
								{if $promoteData.promote_banner != ""}
								<img border="1" style="border-color:#CAD5DB;" width="100px" height="100px" src="{#IMG_PROMOTE_DIR#}{$promoteData.promote_banner}" />
								{/if}
							</td>
						</tr>
						{/if}
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#PROMOTE_NAME#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[promote_name]" id="object[promote_name]" value="{$promoteData.promote_name}" class="input_text" />
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#PROVINCE_NAME#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<select name="object[province_id]" id="object[province_id]" class="dropdown">
									<option value="0">{#SELECTED#}</option>
									{foreach item=provinces from=$provinceList}
									<option value="{$provinces.province_id}" {if $promoteData.province_id eq $provinces.province_id}selected="selected"{/if}>{$provinces.province_name}</option>
									{/foreach}
								</select>					
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext">{#PROMOTE_BANNER#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type="file" name="promote_banner" id="promote_banner" class="inputfield" />
								<input type="hidden" name="object[promote_banner]" id="object[promote_banner]" value="{$promoteData.promote_banner}" />
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext">{#PROMOTE_WEBSITE#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[promote_website]" id="object[promote_website]" value="{$promoteData.promote_website}" class="input_text" />			
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext">{#PROMOTE_CONTENT#}: </td>
							<td align="left" valign="middle" class="tdregist" colspan="3">
								<textarea name="object[promote_content]" id="object[promote_content]" class="textarea">{$promoteData.promote_content}</textarea>		
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#PROMOTE_POSITION#}: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								<select name="object[promote_position]" id="object[promote_position]" class="dropdown">
									{foreach item=positions from=$positionList}
									<option value="{$positions.value}" {if $promoteData.promote_position eq $positions.value}selected="selected"{/if}>{$positions.text}</option>
									{/foreach}
								</select>					
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#PROMOTE_START#}: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								{$promote_start}
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#PROMOTE_END#}: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								{$promote_end}
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#STATUS#}: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								<select name="object[status]" id="object[status]" class="dropdown">
									{foreach item=status from=$statusList}
									<option value="{$status.value}" {if $promoteData.status eq $status.value && $promoteData.status ne ''}selected="selected"{/if}>{$status.text}</option>
									{/foreach}
								</select>					
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="{#UPDATE#}" class="button_new" onclick="javascript: submitform('?hdl=promote/regist', 1)" />
					<input type="button" name="isback" id="isback" value="{#BACK#}" class="button_new" onclick="javascript: submitform('?hdl=promote/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}