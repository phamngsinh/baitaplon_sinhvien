{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function ConfirmDeleteRecord(url, categoryid, page) {
		var theform = document.frmcategory;
		if (confirm('Do you want to delete selected records?')) {
			theform.action = url;
			theform.categoryid.value = categoryid;
			theform.delete_.value = categoryid;
			theform.page.value = page;
			theform.submit();
		} else return false;
	}
	function submitform(url, categoryid, page, status) {
		var theform = document.frmcategory;
		theform.action = url;
		theform.categoryid.value = categoryid;
		theform.page.value = page;
		theform.status.value = status;
		theform.submit();
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frmcategory" id="frmcategory" action="?hdl=category/regist" method="post">
			<input type="hidden" name="categoryid" id="categoryid" value="" />
			<input type="hidden" name="page" id="page" value="{$page}" />
			<input type="hidden" name="status" id="status" value="0" />
			<input type="hidden" name="delete_" id="delete_" value="0" />
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="9"></td>
			</tr>
			{if $exefalse != ''}
			<tr>
				<td colspan="9" style="color: #FF0000;font-weight: bold;">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;">{$exefalse}</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr>
				<td colspan="9" style="padding: 5px;padding-left: 0px;font-weight: bold;">
					<table cellpadding="0" cellspacing="0">
					<tr>
						<td align="left" valign="middle" style="padding-left: 5px;">
							{#CATEGORY_NAME#}&nbsp;:&nbsp;<input type ="text" name="categoryname" id="categoryname" value="{$searchData.categoryname}" class="input_text" />
						</td>
						<td align="left" valign="middle" style="padding-left: 5px;">
							{#STATUS#}&nbsp;:&nbsp;
							<select name="categorystatus" id="categorystatus" class="dropdown">
								<option value="">{#SELECTED#}</option>
								{foreach item=status from=$statusList}
								<option value="{$status.value}" {if $searchData.categorystatus eq $status.value && $searchData.categorystatus ne ''}selected="selected"{/if}>{$status.text}</option>
								{/foreach}
							</select>					
						</td>
						<td align="left" valign="middle" style="padding-left: 5px;">
							<input type="button" name="search" id="search" value="{#SEARCH#}" class="button_new" onClick="return submitform('?hdl=category/list', 0, '1', '1');"/>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<!--header-->
			<tr valign="top">
				<td colspan="9" align="center" style="padding:5px;" valign="middle">
					<input type="submit" name="addnew" id="addnew" value="{#REGIST1#}" class="button_new" />
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td width="4%" align="center" valign="middle" style="height:15px;">{#FIELD_ID#}</td>
				<td width="14%" align="center" valign="middle">{#CATEGORY_NAME#}</td>
				<td width="14%" align="center" valign="middle">{#CATEGORY_HORIZONTAL#}</td>
				<td width="14%" align="center" valign="middle">{#CATEGORY_BOTTOM#}</td>
				<td width="14%" align="center" valign="middle">{#CATEGORY_VERTICAL#}</td>
				<td width="14%" align="center" valign="middle">{#CATEGORY_KIND#}</td>
				<td width="6%" align="center" valign="middle">{#CATEGORY_SORT#}</td>
				<td width="12%" align="center" valign="middle">{#STATUS#}</td>
				<td width="8%" align="center" valign="middle">{#FUNCTION#}</td>
			</tr>
			<!--If cls = 1 then strClass = "record" else strClass = "evenRecord" end if-->
			{if count($categoryList) gt 0}
			{foreach key=key item=items from=$categoryList}
			{assign var="i" value=$i+1}
			{if $i%2 eq 0}
				{assign var="class" value="record"}
			{else}
				{assign var="class" value="evenRecord"}
			{/if}
			<tr class="{$class}" valign="top">
				<td align="center" style="padding:5px;" valign="middle">{$items.category_id}</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.category_name}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$Horizonta[$items.category_horizontal].text}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$MNBottom[$items.category_bottom].text}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$Vertical[$items.category_vertical].text}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$CateKind[$items.category_kind].text}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$items.category_sort}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$statusList[$items.status].text}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<table cellpadding="0" cellspacing="0" border="0px;">
					<tr>
					  	<td style="border: 0px;padding-right:5px;"><img src="backend/images/edit.gif" width="15" height="15" title="edit" onClick="return submitform('?hdl=category/regist',{$items.category_id},{$page},'2');" class="button" /></td>
						<td style="border: 0px;padding-left:5px;"><img src="backend/images/delete.jpg" width="15" height="15" title="delete" onClick="return ConfirmDeleteRecord('?hdl=category/list',{$items.category_id},{$page});" class="button" /></td>
					</tr>
					</table>
				</td>
			</tr>
			{/foreach}
			{else}
			<tr valign="top">
				<td colspan="9" align="center" style="padding:5px;" valign="middle">
				{$message}
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="9" style="height:25px;">
					{if $count gt 1}
					<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
						{$paging}
					</div>
					{/if}
				</td>
			</tr>
			<tr valign="top">
				<td colspan="9" align="center" style="padding:5px;" valign="middle">
					<input type="submit" name="addnew" id="addnew" value="{#REGIST#}" class="button_new" />
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}