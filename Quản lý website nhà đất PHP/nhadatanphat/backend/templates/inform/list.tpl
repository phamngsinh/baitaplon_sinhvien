{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function ConfirmDeleteRecord(url, informid, page) {
		var theform = document.frminform;
		if (confirm('Do you want to delete selected records?')) {
			theform.action = url;
			theform.informid.value = informid;
			theform.delete_.value = informid;
			theform.page.value = page;
			theform.submit();
		} else return false;
	}
	function submitform(url, informid, page, status) {
		var theform = document.frminform;
		theform.action = url;
		theform.informid.value = informid;
		theform.page.value = page;
		theform.status.value = status;
		theform.submit();
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frminform" id="frminform" action="?hdl=inform/regist" method="post">
			<input type="hidden" name="informid" id="informid" value="" />
			<input type="hidden" name="page" id="page" value="{$page}" />
			<input type="hidden" name="status" id="status" value="0" />
			<input type="hidden" name="delete_" id="delete_" value="0" />
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="7"></td>
			</tr>
			{if $exefalse != ''}
			<tr>
				<td colspan="7" style="color: #FF0000;font-weight: bold;">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;">{$exefalse}</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr>
				<td colspan="7" style="padding: 5px;padding-left: 0px;font-weight: bold;">
					<table cellpadding="0" cellspacing="0">
					<tr>
						<td align="left" valign="middle" style="padding-left: 5px;">
							{#INFORM_NAME#}&nbsp;:&nbsp;<input type ="text" name="informname" id="informname" value="{$searchData.informname}" class="input_text" style="width: 250px;" />
						</td>
					<!--
						<td align="left" valign="middle" style="padding-left: 5px;">
							{#INFORM_CATEGORY#}&nbsp;:&nbsp;
							<select name="categoryid" id="categoryid" class="dropdown">
								<option value="">{#SELECTED#}</option>
								{foreach item=categories from=$categoryList}
								<option value="{$categories.category_id}" {if $searchData.categoryid eq $categories.category_id && $searchData.categoryid ne ''}selected="selected"{/if}>{$categories.category_name}</option>
								{/foreach}
							</select>					
						</td>
					-->
						<td align="left" valign="middle" style="padding-left: 5px;" colspan="2">
							{#STATUS#}&nbsp;:&nbsp;
							<select name="informstatus" id="informstatus" class="dropdown">
								<option value="">{#SELECTED#}</option>
								{foreach item=status from=$statusList}
								<option value="{$status.value}" {if $searchData.informstatus eq $status.value && $searchData.informstatus ne ''}selected="selected"{/if}>{$status.text}</option>
								{/foreach}
							</select>					
						</td>
						<td align="left" valign="middle" style="padding-left: 5px;">
							<input type="button" name="search" id="search" value="{#SEARCH#}" class="button_new" onClick="return submitform('?hdl=inform/list', 0, '1', '1');"/>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<!--header-->
			<tr valign="top">
				<td colspan="7" align="center" style="padding:5px;" valign="middle">
					<input type="submit" name="addnew" id="addnew" value="{#REGIST#}" class="button_new" />
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td width="4%" align="center" valign="middle" style="height:15px;">{#FIELD_ID#}</td>
				<td width="20%" align="center" valign="middle">{#INFORM_NAME#}</td>
				<td width="45%" align="center" valign="middle">{#INFORM_TITLE#}</td>
				<td width="12%" align="center" valign="middle">{#INFORM_CATEGORY#}</td>
				<td width="10%" align="center" valign="middle">{#STATUS#}</td>
				<td width="8%" align="center" valign="middle">{#FUNCTION#}</td>
			</tr>
			<!--If cls = 1 then strClass = "record" else strClass = "evenRecord" end if-->
			{if count($informList) gt 0}
			{foreach key=key item=items from=$informList}
			{assign var="i" value=$i+1}
			{if $i%2 eq 0}
				{assign var="class" value="record"}
			{else}
				{assign var="class" value="evenRecord"}
			{/if}
			<tr class="{$class}" valign="top">
				<td align="center" style="padding:5px;" valign="middle">{$items.inform_id}</td>
				<td align="left" style="padding:5px;" valign="middle">
					{if $items.isShowInHomePage eq "1"}<b>{/if}
						{$items.inform_name}
					{if $items.isShowInHomePage eq "1"}</b>{/if}
				</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.inform_title}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$items.category_name}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$statusList[$items.status].text}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<table cellpadding="0" cellspacing="0" border="0px;">
					<tr>
					  	<td style="border: 0px;padding-right:5px;"><img src="backend/images/edit.gif" width="15" height="15" title="edit" onClick="return submitform('?hdl=inform/regist',{$items.inform_id},{$page},'2');" class="button" /></td>
						<td style="border: 0px;padding-left:5px;"><img src="backend/images/delete.jpg" width="15" height="15" title="delete" onClick="return ConfirmDeleteRecord('?hdl=inform/list',{$items.inform_id},{$page});" class="button" /></td>
					</tr>
					</table>
				</td>
			</tr>
			{/foreach}
			{else}
			<tr valign="top">
				<td colspan="7" align="center" style="padding:5px;" valign="middle">
				{$message}
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="7" valign="middle" style="height:25px;">
					{if $count gt 1}
					<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
						{$paging}
					</div>
					{/if}
				</td>
			</tr>
			<tr valign="top">
				<td colspan="7" align="center" style="padding:5px;" valign="middle">
					<input type="submit" name="addnew" id="addnew" value="{#REGIST#}" class="button_new" />
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}