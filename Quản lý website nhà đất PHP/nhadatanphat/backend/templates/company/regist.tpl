{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frmcompany;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frmcompany;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frmcompany" id="frmcompany" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			{if $message != ""}
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;">{$message}</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;">Update company</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="companyid" id="companyid" value="{$companyid}" />
					<input type="hidden" name="page" id="page" value="{$page}" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						{if $companyid != ''}
						<tr valign="top">
							<td width="30%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#FIELD_ID#}: </td>
							<td width="45%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								{$companyid}
							</td>
							<td rowspan="7" width="24%" align="center" valign="middle" class="tdregist">
								{if $companyData.company_logo != ""}
								<img border="1" style="border-color:#CAD5DB;" width="100px" height="100px" src="{#IMG_COMPANY_DIR#}thumb_{$companyData.company_logo}" />
								{/if}
							</td>
						</tr>
						{/if}
						<tr>
							<td width="30%" valign="middle" class="tdregisttext">{#COMPANY_PROVINCE#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<select name="object[province_id]" id="object[province_id]" class="dropdown">
									{foreach item=provinces from=$provinceList}
									<option value="{$provinces.province_id}" {if $companyData.province_id eq $provinces.province_id}selected="selected"{/if}>{$provinces.province_name}</option>
									{/foreach}
								</select>					
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#COMPANY_NAME#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[company_name]" id="object[company_name]" value="{$companyData.company_name}" class="input_text" />
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext">{#COMPANY_BANNER#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type="file" name="company_logo" id="company_logo" class="inputfield" />
								<input type="hidden" name="object[company_logo]" id="object[company_logo]" value="{$companyData.company_logo}" />
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext">{#COMPANY_WEBSITE#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[company_website]" id="object[company_website]" value="{$companyData.company_website}" class="input_text" />			
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext">{#COMPANY_CONTENT#}: </td>
							<td align="left" valign="middle" class="tdregist">
								{$com_content}		
							</td>
						</tr>
						
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">
								Hiển thị trên trang chủ
							</td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<input type="checkbox" name="object[is_show_in_homepage]" 
									{if $companyData.is_show_in_homepage eq "1"} checked {/if}
								/>
							</td>
						</tr>

						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">
								Hiển thị trang tỉnh thành
							</td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<input type="checkbox" name="object[is_show_in_province]" 
									{if $companyData.is_show_in_province eq "1"} checked {/if}
								/>
							</td>
						</tr>

						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#STATUS#}: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[status]" id="object[status]" class="dropdown">
									{foreach item=status from=$statusList}
									<option value="{$status.value}" {if $companyData.status eq $status.value && $companyData.status ne ''}selected="selected"{/if}>{$status.text}</option>
									{/foreach}
								</select>					
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="{#UPDATE#}" class="button_new" onclick="javascript: submitform('?hdl=company/regist', 1)" />
					<input type="button" name="isback" id="isback" value="{#BACK#}" class="button_new" onclick="javascript: submitform('?hdl=company/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}