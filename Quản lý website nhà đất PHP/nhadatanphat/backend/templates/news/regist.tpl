{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frmnews;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frmnews;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frmnews" id="frmnews" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			{if $message != ""}
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;">{$message}</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;">Update news</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="newsid" id="newsid" value="{$newsid}" />
					<input type="hidden" name="page" id="page" value="{$page}" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						{if $newsid != ''}
						<tr valign="top">
							<td width="20%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#FIELD_ID#}: </td>
							<td width="55%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								{$newsid}
							</td>
							<td rowspan="3" width="24%" align="center" valign="middle" class="tdregist">
								{if $newsData.news_image != ""}
								<img border="1" style="border-color:#CAD5DB;" width="100px" height="100px" src="{#IMG_NEWS_DIR#}thumb_{$newsData.news_image}" />
								{/if}
							</td>
						</tr>
						{/if}
						<tr valign="top">
							<td width="20%" valign="middle" class="tdregisttext">{#NEWS_NAME#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[news_name]" id="object[news_name]" value="{$newsData.news_name}" class="input_text" />
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#NEWS_TITLE#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<textarea name="object[news_title]" id="object[news_title]" class="textarea">{$newsData.news_title}</textarea>
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext">{#NEWS_IMAGE#}: </td>
							<td align="left" valign="middle" class="tdregist" colspan="3">
								<input type="file" name="news_image" id="news_image" class="inputfield" />
								<input type="hidden" name="object[news_image]" id="object[news_image]" value="{$newsData.news_image}" />
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext">{#NEWS_CONTENT#}: </td>
							<td align="left" valign="middle" class="tdregist" colspan="3">
									{$news_content}
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#NEWS_GENRE#}: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								<select name="object[news_genre]" id="object[news_genre]" class="dropdown">
									{foreach item=genres from=$newsGenre}
									<option value="{$genres.value}" {if $newsData.news_genre eq $genres.value && $newsData.news_genre ne ''}selected="selected"{/if}>{$genres.text}</option>
									{/foreach}
								</select>					
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#STATUS#}: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								<select name="object[status]" id="object[status]" class="dropdown">
									{foreach item=status from=$statusList}
									<option value="{$status.value}" {if $newsData.status eq $status.value && $newsData.status ne ''}selected="selected"{/if}>{$status.text}</option>
									{/foreach}
								</select>					
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="{#UPDATE#}" class="button_new" onclick="javascript: submitform('?hdl=news/regist', 1)" />
					<input type="button" name="isback" id="isback" value="{#BACK#}" class="button_new" onclick="javascript: submitform('?hdl=news/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}