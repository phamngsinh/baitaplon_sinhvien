{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frminterior;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frminterior;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frminterior" id="frminterior" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			{if $message != ""}
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;">{$message}</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;">{#TITLES_INTERIOR#}</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="interiorid" id="interiorid" value="{$interiorid}" />
					<input type="hidden" name="page" id="page" value="{$page}" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						{if $interiorid != ''}
						<tr valign="top">
							<td width="30%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#FIELD_ID#}: </td>
							<td width="45%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								{$interiorid}
							</td>
							<td rowspan="5" width="24%" align="center" valign="middle" class="tdregist">
								{if $interiorData.interior_image != ""}
								<img border="1" style="border-color:#CAD5DB;" width="100px" src="{#IMG_INTERIOR_DIR#}thumb_{$interiorData.interior_image}" />
								{/if}
							</td>
						</tr>
						{/if}
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#INTERIOR_NAME#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[interior_name]" id="object[interior_name]" value="{$interiorData.interior_name}" class="input_text" />
							</td>
						</tr>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext">{#INTERIOR_ICON#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type="file" name="interior_image" id="interior_image" class="inputfield" />
								<input type="hidden" name="object[interior_image]" id="object[interior_image]" value="{$interiorData.interior_image}" />
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext">{#INTERIOR_DES#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<textarea name="object[interior_description]" id="object[interior_description]" class="textarea">{$interiorData.interior_description}</textarea>
							</td>
						</tr>
						
						<tr valign="top">
							<td valign="middle" class="tdregisttext">{#LBL_CAT_SELECT#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<select name="object[category_id]" class="dropdown">
									<option value="">{#LBL_CAT_SELECT#}:</option>
									{foreach from=$cat_list item="cat" }
									<option	value="{$cat.category_id}" {if $interiorData.category_id == $cat.category_id} selected {/if}>{$cat.category_name|indent:$cat.level:"&#166;&nbsp;&nbsp;&nbsp;&nbsp;":"&#166;--&nbsp;&nbsp;"}</option>
									{/foreach}
								</select>
							</td>
						</tr>
						
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#STATUS#}: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[status]" id="object[status]" class="dropdown">
									{foreach item=status from=$statusList}
									<option value="{$status.value}" {if $interiorData.status eq $status.value && $interiorData.status ne ''}selected="selected"{/if}>{$status.text}</option>
									{/foreach}
								</select>					
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="{#UPDATE#}" class="button_new" onclick="javascript: submitform('?hdl=interior/regist', 1)" />
					<input type="button" name="isback" id="isback" value="{#BACK#}" class="button_new" onclick="javascript: submitform('?hdl=interior/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}