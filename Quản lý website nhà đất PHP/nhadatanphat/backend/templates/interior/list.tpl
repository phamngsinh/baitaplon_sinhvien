<link href="../../styles/style.css" rel="stylesheet" type="text/css" />
{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function ConfirmDeleteRecord(url, interiorid, page) {
		var theform = document.frminterior;
		if (confirm('Do you want to delete selected records?')) {
			theform.action = url;
			theform.interiorid.value = interiorid;
			theform.delete_.value = interiorid;
			theform.page.value = page;
			theform.submit();
		} else return false;
	}
	function submitform(url, interiorid, page, status) {
		var theform = document.frminterior;
		theform.action = url;
		theform.interiorid.value = interiorid;
		theform.page.value = page;
		theform.status.value = status;
		theform.submit();
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frminterior" id="frminterior" action="?hdl=interior/regist" method="post">
			<input type="hidden" name="interiorid" id="interiorid" value="" />
			<input type="hidden" name="page" id="page" value="{$page}" />
			<input type="hidden" name="status" id="status" value="0" />
			<input type="hidden" name="delete_" id="delete_" value="0" />
			<table width="100%" cellpadding="3" cellspacing="3" class="dataTable" border="1">
			<tr>
				<td height="35" colspan="6" class="textdetail" style="padding-left: 5px;padding-top: 15px; padding-bottom: 5px;">
				<a href="?hdl=interior/list"><b>{#INTERIOR_MANAGER#}</b></a> | <a href="?hdl=interior/categories_list"><b>{#CAT_INTERIOR_MANAGER#}</b></a> 
				</td>
			</tr>
			{if $exefalse != ''}
			<tr>
				<td colspan="6" style="color: #FF0000;font-weight: bold;">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;">{$exefalse}</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr>
				<td colspan="6" style="padding: 5px;padding-left: 0px;font-weight: bold;">
					<table cellpadding="0" cellspacing="0">
					<tr>
						<td align="left" valign="middle" style="padding-left: 5px;">
							{#INTERIOR_NAME#}&nbsp;:&nbsp;<input type ="text" name="interiorname" id="interiorname" value="{$searchData.interiorname}" class="input_text" />
						</td>
						<td align="left" valign="middle" style="padding-left: 5px;" colspan="2">
							{#STATUS#}&nbsp;:&nbsp;
							<select name="interiorstatus" id="interiorstatus" class="dropdown" style="font-weight:normal;">
								<option value="">{#SELECTED#}</option>
								{foreach item=status from=$statusList}
								<option value="{$status.value}" {if $searchData.interiorstatus eq $status.value && $searchData.interiorstatus ne ''}selected="selected"{/if}>{$status.text}</option>
								{/foreach}
							</select>					
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr valign="top">
				<td colspan="6" align="center" style="padding:5px;" valign="middle">
					<input type="button" name="search" id="search" value="{#SEARCH#}" class="button_new" onClick="return submitform('?hdl=interior/list', 0, '1', '1');"/>
					<input type="submit" name="addnew" id="addnew" value="{#REGIST#}" class="button_new" />
				</td>
			</tr>
			<!--header-->
			<tr class="stdHeader" valign="top">
				<td width="4%" align="center" valign="middle" style="height:15px;">{#interior_ID#}</td>
				<td width="20%" align="center" valign="middle">{#INTERIOR_NAME#}</td>
				<td width="15%" align="center" valign="middle">{#INTERIOR_ICON#}</td>
				<td width="40%" align="center" valign="middle">{#INTERIOR_DES#}</td>
				<td width="10%" align="center" valign="middle">{#STATUS#}</td>
				<td width="10%" align="center" valign="middle">{#FUNCTION#}</td>
			</tr>
			<!--If cls = 1 then strClass = "record" else strClass = "evenRecord" end if-->
			{if count($interiorList) gt 0}
			{foreach key=key item=items from=$interiorList}
			{assign var="i" value=$i+1}
			{if $i%2 eq 0}
				{assign var="class" value="record"}
			{else}
				{assign var="class" value="evenRecord"}
			{/if}
			<tr class="{$class}" valign="top">
				<td align="center" style="padding:5px;" valign="middle">{$items.interior_id}</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.interior_name|escape}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<img border="1" style="border-color:#CAD5DB;" width="50px" height="50px"
						src="{#IMG_INTERIOR_DIR#}thumb_{$items.interior_image}" />
				</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.interior_description}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$statusList[$items.status].text}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<table cellpadding="0" cellspacing="0" border="0px;">
					<tr>
					  	<td style="border: 0px;padding-right:5px;"><img src="backend/images/edit.gif" width="15" height="15" title="edit" onClick="return submitform('?hdl=interior/regist',{$items.interior_id},0,'2');" class="button" /></td>
						<td style="border: 0px;padding-left:5px;"><img src="backend/images/delete.jpg" width="15" height="15" title="delete" onClick="return ConfirmDeleteRecord('?hdl=interior/list',{$items.interior_id},0);" class="button" /></td>
					</tr>
					</table>
				</td>
			</tr>
			{/foreach}
			{else}
			<tr valign="top">
				<td colspan="6" align="center" style="padding:5px;" valign="middle">
				{$message}
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="6" valign="middle" style="height:25px;">
					{if $count gt 1}
					<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
						{$paging}
					</div>
					{/if}
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}