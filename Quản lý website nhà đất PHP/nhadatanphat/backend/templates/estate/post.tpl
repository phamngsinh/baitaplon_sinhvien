{include file=$include_header} {literal}
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frmestate;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frmestate;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}
</script>
{/literal} {literal}
<script language="javascript" type="text/javascript">
		var ajaxObj = new XMLHTTP("services.php");

	function select_parent(val){
		ajaxObj.call('action=Category&act=SelectParent&category_id='+val, select_parent_resp);
	}
	function select_parent_resp(resp) {
		if(resp.result == '1') {
			var model = document.getElementById("estateObj[child_id]");
			ClearOption("estateObj[child_id]");
			var modelOption1 = document.createElement('option');
			modelOption1.text = "Lựa chọn";
  			modelOption1.value = '';
  			try {
    			model.add(modelOption1, null); // standards compliant; doesn't work in IE
  			}
  			catch(ex) {
    			model.add(modelOption1); // IE only
  			}
			if(resp.data){
			var id = resp.data.id;
			for(var i = 0; i < id.length; i++){
				var modelOption = document.createElement('option');
  				modelOption.text = resp.data.title[i];
  				modelOption.value = id[i];

  				try {
    				model.add(modelOption, null); 
  				}
  				catch(ex) {
    				model.add(modelOption); 
  				}
			}
			}
		} else {
			alert(resp.result);
		}
	}
	function select_province(val){
		ajaxObj.call('action=Province&act=SelectProvince&province_id='+val, select_province_resp);
	}
	function select_province_resp(resp) {
		ClearOption("estateObj[district_id]");
		if(resp.result == '1') {
			var model = document.getElementById("estateObj[district_id]");
			var modelOption1 = document.createElement('option');
			modelOption1.text = "Lựa chọn huyện";
  			modelOption1.value = '';
  			try {
    			model.add(modelOption1, null); // standards compliant; doesn't work in IE
  			}
  			catch(ex) {
    			model.add(modelOption1); // IE only
  			}
			if(resp.data){
			var id = resp.data.id;
			for(var i = 0; i < id.length; i++){
				var modelOption = document.createElement('option');
  				modelOption.text = resp.data.title[i];
  				modelOption.value = id[i];

  				try {
    				model.add(modelOption, null); 
  				}
  				catch(ex) {
    				model.add(modelOption); 
  				}
			}
			}
		} else {
			alert(resp.result);
		}
	}
	function ClearOption(id){
		var list = document.getElementById(id);
		for(var i=list.length-1;i>=0;i--){
			list.remove(i);
		}
	}
</script>
{/literal} 
{include file=$include_left}
<td width="80%" align="left" valign="top">
	<form name="frmestate" id="frmestate" action="" method="post" enctype="multipart/form-data">
		<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			{if $message != ""}
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" valign="middle" width="5%" class="error">
								<img src="backend/images/error.jpg" width="40" height="40"
									title="Errors !" />
							</td>
							<td valign="middle" class="error" style="border-left: 0px;">
								{$message}
							</td>
						</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;">
					Update estate
				</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="estateid" id="estateid" value="{$estateid}" />
					<input type="hidden" name="page" id="page" value="{$page}" />
					<table width="100%" cellpadding="0" cellspacing="0"
						class="dataTable">
						<tr valign="top">
							<td width="24%" align="left" valign="middle" class="tdregisttext"
								style="border-top: #CAD5DB 1px solid;">
								{#FIELD_ID#}
							</td>
							<td width="75%" align="left" valign="middle" class="tdregist"
								style="border-top: #CAD5DB 1px solid;" colspan="2">
								{$estateid}
							</td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								{#ESTATE_TITLE#}
							</td>
							<td align="left" valign="middle" class="tdregist">
								<input type="text" id="estateObj[estate_title]"
									name="estateObj[estate_title]" size="70" maxlength="255"
									value="{$estateObj.estate_title}" />
							</td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								Mô tả ngắn:
							</td>
							<td align="left" valign="middle" class="tdregist">
								<input type="text" id="estateObj[estate_intro]"
									name="estateObj[estate_intro]" size="70" maxlength="255"
									value="{$estateObj.estate_intro}" />
							</td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								Tên người liên hệ:
							</td>
							<td align="left" valign="middle" class="tdregist">
								<input type="text" id="estateObj[estate_phone_contact]"
									name="estateObj[estate_name_contact]" size="70" maxlength="255"
									value="{$estateObj.estate_name_contact}" />
							</td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								Điện thoại liên hệ:
							</td>
							<td align="left" valign="middle" class="tdregist">
								<input type="text" id="estateObj[estate_phone_contact]"
									name="estateObj[estate_phone_contact]" size="70" maxlength="255"
									value="{$estateObj.estate_phone_contact}" />
							</td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								{#ESTATE_CATEGORY#}
							</td>
							<td align="left" valign="middle" class="tdregist">
								<select name="estateObj[category_id]"
									id="estateObj[category_id]" class="dropdown"
									onChange="select_parent(this.options[selectedIndex].value)">
									{foreach item=items from=$CategoryList}
									<option value="{$items.category_id}" {if $estateObj.category_id
										eq $items.category_id && $estateObj.category_id
										ne ''}selected="selected"{/if}>
										{$items.category_name}
									</option>
									{/foreach}
								</select>
								<select name="estateObj[child_id]" id="estateObj[child_id]"
									class="dropdown">
									{foreach item=items from=$ChildList}
									<option value="{$items.child_id}" {if $estateObj.child_id
										eq $items.child_id && $estateObj.child_id
										ne ''}selected="selected"{/if}>
										{$items.child_name}
									</option>
									{/foreach}
								</select>
							</td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								{#ESTATE_ADDRESS#}
							</td>
							<td align="left" valign="middle" class="tdregist">
								<input type="text" name="estateObj[estate_address]"
									id="estateObj[estate_address]"
									value="{$estateObj.estate_address}" class="form_search"
									style="width: 200px;" />
								<select name="estateObj[province_id]"
									id="estateObj_province_id" class="dropdown"
									onChange="select_province(this.options[selectedIndex].value)">
									<option value="">
										Lựa chọn tỉnh
									</option>
									{foreach item=provinces from=$ProvinceList}
									<option value="{$provinces.province_id}"
										{if $estateObj.province_id
										eq $provinces.province_id && $estateObj.province_id
										ne ''} selected="selected"{/if}>
										{$provinces.province_name}
									</option>
									{/foreach}
								</select>
								<select name="estateObj[district_id]"
									id="estateObj[district_id]" class="dropdown">
									<option value="">
										Lựa chọn huyện
									</option>
									{foreach item=districts from=$DistrictList}
									<option value="{$districts.district_id}"
										{if $estateObj.district_id
										eq $districts.district_id } selected="selected"{/if}>
										{$districts.district_name}
									</option>
									{/foreach}
								</select>
							</td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								Diện tích mặt bằng:
							</td>
							<td align="left" valign="middle" class="tdregist">
								<input type="text" name="estateObj[estate_area]"
									id="estateObj[estate_area]" value="{$estateObj.estate_area}"
									class="form_search" />
							</td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								Diện tích sử dụng:
							</td>
							<td align="left" valign="middle" class="tdregist">
								<input type="text" name="estateObj[estate_area_use]"
									id="estateObj[estate_area_use]" value="{$estateObj.estate_area_use}"
									class="form_search" />
							</td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								{#ESTATE_DERECTION#}
							</td>
							<td align="left" valign="middle" class="tdregist">
								<select name="estateObj[estate_direction]"
									id="estateObj[estate_direction]" class="dropdown">
									<option value="">Chọn Hướng
									{foreach item=items from=$Direction}
									<option value="{$items.value}" {if $estateObj.estate_direction
										eq $items.value && $estateObj.estate_direction
										ne ''}selected="selected"{/if}>
										{$items.text}
									</option>
									{/foreach}
								</select>
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								{#ESTATE_PRICE#}
							</td>
							<td align="left" valign="middle" class="tdregist">
								<input type="text" name="estateObj[estate_price]"
									id="estateObj[estate_price]" value="{$estateObj.estate_price}"
									class="form_search" style="width: 100px;" />
								&nbsp;
								<select name="estateObj[estate_price_type]">
									<option value="0" {if $estateObj.estate_price_type==0}
										selected="selected"{/if}>
										VNĐ
									</option>
									<option value="1" {if $estateObj.estate_price_type==1}
										selected="selected"{/if}>
										SJC
									</option>
									<option value="2" {if $estateObj.estate_price_type==2}
										selected="selected"{/if}>
										USD
									</option>
								</select>
								<br />
								<input type="checkbox" name="estateObj[is_price_in_m2]" {if $estateObj.is_price_in_m2 eq '1'} checked {/if} id="is_price_in_m2_">
								Giá trên 1m2
								<br />
								<input type="checkbox" name="estateObj[is_price_in_month]" {if $estateObj.is_price_in_month eq '1'} checked {/if} id="is_price_in_month_">
								Giá thuê trên tháng
								<br />
								<input type="checkbox" name="estateObj[is_price_negotiable]" {if $estateObj.is_price_negotiable eq '1'} checked {/if} id="is_price_negotiable_">
								Có thể thỏa thuận
								
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								File video
							</td>
							<td align="left" valign="middle" class="tdregist">
								<input type="file" name="estate_video" id="estate_video"
									class="inputfield" /> ({$smarty.const.VIDEO_FORMAT})
								
								<!--
								{if $estateInitData.estate_video neq ''}
									<object classid="CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95" 
											codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715" 
											standby="Loading Microsoft Windows Media Player components..." type="application/x-oleobject"
											viewastext="" width="190" height="190"
										>
										<param name="FileName" value="../{#AUD_ESTATE_DIR#}{$estateInitData.estate_video}">
										<param name="TransparentAtStart" value="true">
										<param name="AutoStart" value="true">
										<param name="AnimationatStart" value="false">
										<param name="ShowControls" value="true">
										<param name="ShowDisplay" value="true">
										<param name="playCount" value="1">
										<param name="displaySize" value="0">
										<param name="Volume" value="100">
										<embed 	type="application/x-mplayer2" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" 
												src="../{#AUD_ESTATE_DIR#}{$estateInitData.estate_video}" name="MediaPlayer" transparentatstart="0" 
												autostart="1" playcount="999" volume="100animationAtStart=0" displaysize="0" width="220" height="220">
									</object>
            					{/if}
								-->
								<br />
								<input type="hidden" name="estateObj[estate_video]"
									id="estateObj[estate_video]" value="{$estateInitData.estate_video}" />
								{if $estateInitData.estate_video ne ''}
								<br/><input type="checkbox" id="del_6" name="del_6" value="1"/><label style="color: black;">Xóa</label>
								{/if}
							</td>
						</tr>

						<tr>
							<td class="tdregisttext">Ảnh mô tả lớn</td>
							<td align="left" valign="middle" class="tdregist">
								<input type="file" name="estate_image1" id="estate_image1"
									class="inputfield" />
								<input type="hidden" name="estate_image[1]" id="estate_image[1]"
									value="{$estateInitData.estate_image1}" />
							{if $estateInitData.estate_image1 ne ''}
		                    <br/>
		                    	<img src="{#IMG_ESTATE_DIR#}thumb_{$estateInitData.estate_image1}"
		                    		width="150" class="border_img_white" />
		                    <input type="checkbox" id="del_1" name="del_1" value="1"/><label style="color: black;">Xóa</label>
		                    {/if}
							</td>
						</tr>
						<tr>
							<td class="tdregisttext">Ảnh mô tả một</td>
							<td align="left" valign="middle" class="tdregist">
								<input type="file" name="estate_image2" id="estate_image2"
									class="inputfield" />
								<input type="hidden" name="estate_image[2]" id="estate_image[2]"
									value="{$estateInitData.estate_image2}" />
								{if $estateInitData.estate_image2 ne ''}
		                    	<br/><img src="{#IMG_ESTATE_DIR#}thumb_{$estateInitData.estate_image2}" width="150" class="border_img_white" />
		                    	<input type="checkbox" id="del_2" name="del_2" value="1"/><label style="color: black;">Xóa</label>
		                    	{/if}
							</td>
						</tr>
						<tr>
							<td class="tdregisttext">Ảnh mô tả hai</td>
							<td align="left" valign="middle" class="tdregist">
								<input type="file" name="estate_image3" id="estate_image3"
									class="inputfield" />
								<input type="hidden" name="estate_image[3]" id="estate_image[3]"
									value="{$estateInitData.estate_image3}" />
								{if $estateInitData.estate_image3 ne ''}
			                    <br/><img src="{#IMG_ESTATE_DIR#}thumb_{$estateInitData.estate_image3}" width="150" class="border_img_white" />
			                    <input type="checkbox" id="del_3" name="del_3" value="1"/><label style="color: black;">Xóa</label>
			                    {/if}
							</td>
						</tr>
						<tr>
							<td class="tdregisttext">Ảnh mô tả ba</td>
							<td align="left" valign="middle" class="tdregist">
								<input type="file" name="estate_image4" id="estate_image4"
									class="inputfield" />
								<input type="hidden" name="estate_image[4]" id="estate_image[4]"
									value="{$estateInitData.estate_image4}" />
								{if $estateInitData.estate_image4 ne ''}
		                    <br/><img src="{#IMG_ESTATE_DIR#}thumb_{$estateInitData.estate_image4}" width="150" class="border_img_white" />
		                    <input type="checkbox" id="del_4" name="del_4" value="1"/><label style="color: black;">Xóa</label>
		                    {/if}
							</td>
						</tr>
						<tr>
							<td class="tdregisttext">Ảnh mô tả bốn</td>
							<td align="left" valign="middle" class="tdregist">
								<input type="file" name="estate_image5" id="estate_image5"
									class="inputfield" />
								<input type="hidden" name="estate_image[5]" id="estate_image[5]"
									value="{$estateInitData.estate_image5}" />
								{if $estateInitData.estate_image5 ne ''}
		                    	<br/><img src="{#IMG_ESTATE_DIR#}thumb_{$estateInitData.estate_image5}" width="150" class="border_img_white" />
		                    	<input type="checkbox" id="del_5" name="del_5" value="1"/><label style="color: black;">Xóa</label>
		                    	{/if}
							</td>
						</tr>

						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								{#ESTATE_CREATE#}
							</td>
							<td align="left" valign="middle" class="tdregist">
								{$estateInitData.created_date|date_format:"%Y-%m-%d"}
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								{#ESTATE_DURATION#}
							</td>
							<td align="left" valign="middle" class="tdregist">
								{$duration}&nbsp;(Định dạng : D-M-Y)
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								{#ESTATE_DELETE#}
							</td>
							<td align="left" valign="middle" class="tdregist">
								{$deleted_date}&nbsp;(Định dạng : D-M-Y)
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								{#ESTATE_CONTENT#}
							</td>
							<td align="left" valign="middle" class="tdregist">
								{$estate_note}
							</td>
						</tr>
						
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext"
								style="border-top: #CAD5DB 1px solid;">
								{#ESTATE_GENRE#}<!--Nhóm tin rao-->
							</td>
							<td align="left" valign="middle" class="tdregist"
								style="border-top: #CAD5DB 1px solid;" colspan="3">
								<select name="estateObj[estate_genre]" id="estateObj[estate_genre]"
									class="dropdown">
									{foreach item=genres from=$genreList}
									<option value="{$genres.value}" {if $estateObj.estate_genre
										eq $genres.value && $estateObj.estate_genre
										ne ''}selected="selected"{/if}>
										{$genres.text}
									</option>
									{/foreach}
								</select>
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext"
								style="border-top: #CAD5DB 1px solid;">
								{#STATUS#}:
							</td>
							<td align="left" valign="middle" class="tdregist"
								style="border-top: #CAD5DB 1px solid;" colspan="3">
								<select name="estateObj[status]" id="estateObj[status]"
									class="dropdown">
									{foreach item=status from=$statusList}
									<option value="{$status.value}" {if $estateObj.estate_status
										eq $status.value && $estateObj.estate_status
										ne ''}selected="selected"{/if}>
										{$status.text}
									</option>
									{/foreach}
								</select>
							</td>
						</tr>

						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext"
								style="border-top: #CAD5DB 1px solid;">
								Ảnh nổi bật:
							</td>
							<td align="left" valign="middle" class="tdregist"
								style="border-top: #CAD5DB 1px solid;" colspan="3">
								<input type="checkbox" name="estateObj[is_image_enhance]" 
									{if $estateObj.is_image_enhance eq "1"}checked{/if}
								/>
							</td>
						</tr>
						
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext"
								style="border-top: #CAD5DB 1px solid;">
								Tin có phí:
							</td>
							<td align="left" valign="middle" class="tdregist"
								style="border-top: #CAD5DB 1px solid;" colspan="3">
								<input type="checkbox" name="estateObj[is_fee_estate]" 
									{if $estateObj.is_fee_estate eq "1"}checked{/if}
								/>
							</td>
						</tr>
						
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext"
								style="border-top: #CAD5DB 1px solid;">
								Tin bán gấp:
							</td>
							<td align="left" valign="middle" class="tdregist"
								style="border-top: #CAD5DB 1px solid;" colspan="3">
								<input type="checkbox" name="estateObj[estate_hot]" 
									{if $estateObj.estate_hot eq "1"}checked{/if}
								/>
							</td>
						</tr>
						
						
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext"
								style="border-top: #CAD5DB 1px solid;">
								Cập nhật ngày tháng:
							</td>
							<td align="left" valign="middle" class="tdregist"
								style="border-top: #CAD5DB 1px solid;" colspan="3">
									<input type="checkbox" name="estateObj[updateDate]" 
										{if $estateObj.updateDate eq "1"}checked{/if}
									/>
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">
								Trạng thái tin của member
							</td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								<input type="checkbox" name="estateObj[is_deleted_by_member]" 
									{if $estateObj.is_deleted_by_member eq "1"}checked{/if}
								/>
								(Check là bị xóa)
							</td>
						</tr>
						
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext"
								style="border-top: #CAD5DB 1px solid;">
								Người đăng tin
							</td>
							<td align="left" valign="middle" class="tdregist"
								style="border-top: #CAD5DB 1px solid;" colspan="3">
								<table width="98%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td style="padding-right: 5px;">
											{if $logoImg neq ""}
												<img src="{#IMG_MEMBER_LOGO_DIR#}{$logoImg}" width="150" class="border_img_white" />
											{/if}
										</td>
										<td width="100%" align="left">
											<table width="100%" border="0" cellpadding="0"
												cellspacing="0">
												<tr>
													<td height="19" width="30%" align="left" valign="middle"
														style="font-weight: bold;">
														Công ty / Cá nhân
													</td>
													<td align="left">
														:&nbsp;{$estateInitData.member_name}
													</td>
												</tr>
												<tr>
													<td height="19" width="15%" align="left" valign="middle"
														style="font-weight: bold;">
														Địa chỉ email
													</td>
													<td align="left">
														:&nbsp;
														<a href="mailto:{$estateInitData.member_mail}"
															style="color: #D70B2E;">{$estateInitData.member_mail}</a>
													</td>
												</tr>
												<tr>
													<td height="19" width="15%" align="left" valign="middle"
														style="font-weight: bold;">
														Số điện thoại
													</td>
													<td align="left">
														:&nbsp;{$estateInitData.member_phone}
													</td>
												</tr>
												<tr>
													<td height="19" width="15%" align="left" valign="middle"
														style="font-weight: bold;">
														Địa chỉ liên lạc
													</td>
													<td align="left">
														:&nbsp;{$estateInitData.member_address}
													</td>
												</tr>
												<tr>
													<td height="19" width="15%" align="left" valign="middle"
														style="font-weight: bold;">
														Website
													</td>
													<td align="left">
														:&nbsp;
														<a href="{$estateInitData.member_website}" target="_blank"
															style="color: #0000FF;">{$estateInitData.member_website}</a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>						
					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="{#UPDATE#}"
						class="button_new"
						onclick="javascript: submitform('?hdl=estate/post', 1)" />
					<input type="button" name="isback" id="isback" value="{#BACK#}"
						class="button_new"
						onclick="javascript: submitform('?hdl=estate/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
		</table>
	</form>
</td>
{include file=$include_footer}
