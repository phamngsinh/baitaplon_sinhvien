{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function ConfirmDeleteRecord(url, estateid, page) {
		var theform = document.frmestate;
		if (confirm('Do you want to delete selected records?')) {
			theform.action = url;
			theform.estateid.value = estateid;
			theform.delete_.value = estateid;
			theform.page.value = page;
			theform.submit();
		} else return false;
	}

	function submitform(url, estateid, page, status) {
		var theform = document.frmestate;
		theform.action = url;
		theform.estateid.value = estateid;
		theform.page.value = page;
		theform.status.value = status;
		theform.submit();
	}
</script>
{/literal}
	{literal}
	<script language="javascript" type="text/javascript">
		var ajaxObj = new XMLHTTP("services.php");
	
		function select_parent(val){
			ajaxObj.call('action=Category&act=SelectParent&category_id='+val, select_parent_resp);
		}
		function select_parent_resp(resp) {
			ClearOption("childid");
			if(resp.result == '1') {
				var model = document.getElementById("childid");
				var modelOption1 = document.createElement('option');
				modelOption1.text = "Lựa chọn danh mục con";
	  			modelOption1.value = '';
	  			try {
	    			model.add(modelOption1, null); // standards compliant; doesn't work in IE
	  			}
	  			catch(ex) {
	    			model.add(modelOption1); // IE only
	  			}
				if(resp.data){
				var id = resp.data.id;
				for(var i = 0; i < id.length; i++){
					var modelOption = document.createElement('option');
	  				modelOption.text = resp.data.title[i];
	  				modelOption.value = id[i];
	
	  				try {
	    				model.add(modelOption, null); 
	  				}
	  				catch(ex) {
	    				model.add(modelOption); 
	  				}
				}
				}
			} else {
				alert(resp.result);
			}
		}
		function select_province(val){
			ajaxObj.call('action=Province&act=SelectProvince&province_id='+val, select_province_resp);
		}
		function select_province_resp(resp) {
			ClearOption("districtid");
			if(resp.result == '1') {
				var model = document.getElementById("districtid");
				var modelOption1 = document.createElement('option');
				modelOption1.text = "Lựa chọn huyện";
	  			modelOption1.value = '';
	  			try {
	    			model.add(modelOption1, null); // standards compliant; doesn't work in IE
	  			}
	  			catch(ex) {
	    			model.add(modelOption1); // IE only
	  			}
				if(resp.data){
				var id = resp.data.id;
				for(var i = 0; i < id.length; i++){
					var modelOption = document.createElement('option');
	  				modelOption.text = resp.data.title[i];
	  				modelOption.value = id[i];
	
	  				try {
	    				model.add(modelOption, null); 
	  				}
	  				catch(ex) {
	    				model.add(modelOption); 
	  				}
				}
				}
			} else {
				alert(resp.result);
			}
		}
		function ClearOption(id){
			var list = document.getElementById(id);
			for(var i=list.length-1;i>=0;i--){
				list.remove(i);
			}
		}
		

		function checkAll(fmobj) {
		  var status = fmobj.checkStatus.value;
		  var checkStat = (status == "1"?true:false);

		  for (var i=0;i<fmobj.elements.length;i++) {
		    var e = fmobj.elements[i];
		    if ((e.type=='checkbox') && (e.name != "expiredEstate")&& (e.name != "isFeeEstate")) {
		      e.checked = checkStat;
		    }
		  }
		  
		  fmobj.checkStatus.value = (fmobj.checkStatus.value=="1"?"0":"1");
		}
	</script>
	{/literal}
	
	<script language="javascript">
		var page = "{$page}";
		var status = "{$status}";
		{literal}
		function deleteRecord(){
			var theform = document.frmestate;
			theform.delete_.value='1';
			theform.action = '?hdl=estate/list';
			theform.page.value = page;
			theform.status.value = status;
			theform.submit();
		}
		{/literal}
	</script>
	
{include file=$include_left}
		<td width="80%" align="left" valign="top"> 
		<form name="frmestate" id="frmestate" action="?hdl=estate/regist" method="post">
			<input type="hidden" name="checkStatus" value="1" />
			<input type="hidden" name="estateid" id="estateid" value="" />
			<input type="hidden" name="page" id="page" value="{$page}" />
			<input type="hidden" name="status" id="status" value="0" />
			<input type="hidden" name="delete_" id="delete_" value="" />
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="10"></td>
			</tr>
			<tr>
				<td colspan="8" style="padding: 5px;padding-left: 0px;font-weight: bold; line-height: 25px;">
					<table cellpadding="0" cellspacing="0" border='0'>
						<tr>
							<td align="left" valign="middle" style="padding-left: 1px;">{#ESTATE_TITLE#}</td>
							<td align="left" valign="middle" style="padding-left: 5px;">
								: <input type ="text" name="title" id="title" value="{$searchData.title}" class="input_text" style="width: 200px;" />
							</td>
						</tr>
						
						<tr>
							<td align="left" valign="middle" style="padding-left: 1px;">{#ESTATE_CREATE#}</td>
							<td align="left" valign="middle" style="padding-left: 5px;">
								&nbsp;:&nbsp;{$startdate}&nbsp;-&nbsp;{$enddate}
							</td>
						</tr>
						
						<tr>
							<td align="left" valign="middle" style="padding-left: 5px;">
								{#STATUS#}
							</td>
							<td align="left" valign="middle" style="padding-left: 5px;">
								: <select name="estatestatus" id="estatestatus" class="dropdown">
									<option value="">{#SELECTED#}</option>
									{foreach item=status from=$statusList}
									<option value="{$status.value}" {if $searchData.estatestatus eq $status.value &&  $searchData.estatestatus ne ''}selected="selected"{/if}>{$status.text}</option>
									{/foreach}
								</select>
							</td>
						</tr>
						<tr>
							<td align="left" valign="middle" style="padding-left: 5px;">
								{#ESTATE_ADDRESS#}</td>
							<td align="left" valign="middle" style="padding-left: 5px;">
								: <input type ="text" name="address" id="address" value="{$searchData.address}" class="input_text" style="width: 200px;" />
	                          	<select name="provinceid" id="provinceid" class="dropdown" onChange="select_province(this.options[selectedIndex].value)">
	                            	<option value="">Lựa chọn tỉnh</option>
	                            	{foreach item=provinces from=$ProvinceList}
	                            	<option value="{$provinces.province_id}" {if $searchData.provinceid eq $provinces.province_id && $searchData.provinceid ne ''} selected="selected" {/if}>{$provinces.province_name}</option>
	                            	{/foreach}
	                          	</select>
	                        	<select name="districtid" id="districtid" class="dropdown">
	                            	<option value="">Lựa chọn huyện</option>
	                           		{foreach item=districts from=$DistrictList}
	                           		<option value="{$districts.district_id}" {if $searchData.districtid eq $districts.district_id && $searchData.districtid ne ''} selected="selected" {/if}>{$districts.district_name}</option>
	                           		{/foreach}
	                        	</select>
							</td>
						</tr>
						
						
						<tr>
							<td align="left" valign="middle" style="padding-left: 1px;">
								{#ESTATE_GENRE#}
							</td>
							<td align="left" valign="middle" style="padding-left: 5px;">
								: <select name="genre" id="genre" class="dropdown">
									<option value="">{#SELECTED#}</option>
									{foreach item=genre from=$genreList}
									<option value="{$genre.value}" {if $searchData.genre eq $genre.value &&  $searchData.genre ne ''}selected="selected"{/if}>{$genre.text}</option>
									{/foreach}
								</select>
							</td>
						</tr>
						
						<tr>
							<td align="left" valign="middle" style="padding-left: 1px;">
								{#ESTATE_DERECTION#}
							</td>
							<td align="left" valign="middle" style="padding-left: 5px;">
								: <select name="direction" id="direction" class="dropdown">
									<option value="">{#SELECTED#}</option>
									{foreach item=items from=$directionList}
									<option value="{$items.value}" {if $searchData.direction eq $items.value &&  $searchData.direction ne ''}selected="selected"{/if}>{$items.text}</option>
									{/foreach}
								</select>
							</td>
						</tr>
						<tr>
							<td align="left" valign="middle" style="padding-left: 5px;">{#ESTATE_PRICE#}</td>
							<td align="left" valign="middle" style="padding-left: 5px;">
								:
								<input type ="text" name="pricestart" id="pricestart" value="{$searchData.pricestart}" class="input_text" style="width: 150px;" /> -
								<input type ="text" name="priceend" id="priceend" value="{$searchData.priceend}" class="input_text" style="width: 150px;" />
							</td>
						</tr>
						
						<tr>
							<td align="left" valign="middle" style="padding-left: 1px;">
								{#ESTATE_CATEGORY#}
							</td>
							<td align="left" valign="middle" style="padding-left: 5px;">
								: <select name="categoryid" id="categoryid" class="dropdown" onChange="select_parent(this.options[selectedIndex].value)">
									<option value="">Lựa chọn loại tin</option>
									{foreach item=items from=$CategoryList}
									<option value="{$items.category_id}" {if $searchData.categoryid eq $items.category_id && $searchData.categoryid ne ''}selected="selected"{/if}>{$items.category_name}</option>
									{/foreach}
								</select>
								<select name="childid" id="childid" class="dropdown" >
									<option value="">Lựa chọn danh mục con</option>
									{foreach item=items from=$ChildList}
									<option value="{$items.child_id}" {if $searchData.childid eq $items.child_id && $searchData.childid ne ''}selected="selected"{/if}>{$items.child_name}</option>
									{/foreach}
								</select>
							</td>
						</tr>
						
						<tr>
							<td align="left" valign="middle" style="padding-left: 1px;">
								Chỉ hiển thị tin hết hạn
							</td>
							<td align="left" valign="middle" style="padding-left: 5px;">
								: <input type="checkbox" name="expiredEstate" {if $expiredEstate eq "on"} checked {/if} />
							</td>
						</tr>
						<tr>
							<td align="left" valign="middle" style="padding-left: 1px;">
								Chỉ hiển thị tin có phí
							</td>
							<td align="left" valign="middle" style="padding-left: 5px;">
								: <input type="checkbox" name="isFeeEstate" {if $isFeeEstate eq "on"} checked {/if} />
							</td>
						</tr>
						<tr>
							<td align="left" valign="middle" style="padding-left: 1px;">
								Chỉ hiển thị tin có video
							</td>
							<td align="left" valign="middle" style="padding-left: 5px;">
								: <input type="checkbox" name="isVideoEstate" {if $isVideoEstate eq "on"} checked {/if} />
							</td>
						</tr>
						
						<tr>
							<td align="center" valign="middle" colspan="2">
								<input type="button" name="search" id="search" value="{#SEARCH#}" class="button_new" 
									onClick="return submitform('?hdl=estate/list', 0, '1', '1');"/>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!--header-->
			<tr valign="top">
				<td width="4%" align="center" valign="middle" style="height:15px;">&nbsp;</td>
				<td width="38%" align="center" valign="middle">&nbsp;</td>
				<td width="10%" align="center" valign="middle">&nbsp;</td>
				<td width="10%" align="center" valign="middle">&nbsp;</td>
				<td width="10%" align="center" valign="middle">&nbsp;</td>
				<td width="10%" align="center" valign="middle">&nbsp;</td>
				<td width="10%" align="center" valign="middle">&nbsp;</td>
				<td width="8%" align="right" valign="middle">
					<img src="backend/images/delete.jpg" width="15" height="15" 
						title="Xóa theo checkbox đã chọn" 
						{if $accessDenied eq ""}
							onClick="if(confirm('Xóa các bản ghi đã chọn ở bảng?')) deleteRecord();"
						{else}
							onClick="javascript:alert('Không có quyền xóa!!!');";
						{/if} 
						class="button" />
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td width="4%" align="center" valign="middle" style="height:15px;">{#FIELD_ID#}</td>
				<td width="38%" align="center" valign="middle">{#ESTATE_TITLE#}</td>
				<td width="10%" align="center" valign="middle">{#ESTATE_CATEGORY#}</td>
				<td width="10%" align="center" valign="middle">{#ESTATE_PRICE#}</td>
				<td width="10%" align="center" valign="middle">{#ESTATE_CREATE#}</td>
				<td width="10%" align="center" valign="middle">{#ESTATE_DELETE#}</td>
				<td width="10%" align="center" valign="middle">{#STATUS#}</td>
				
				<td width="8%" align="center" valign="middle">
					Sửa/<a onClick="checkAll(document.frmestate);" style="cursor:pointer;">Chọn</a>
				</td>
			</tr>
			<!--If cls = 1 then strClass = "record" else strClass = "evenRecord" end if-->
			{if count($estateList) gt 0}
			{foreach key=key item=items from=$estateList}
			{assign var="i" value=$i+1}
			{if $i%2 eq 0}
				{assign var="class" value="record"}
			{else}
				{assign var="class" value="evenRecord"}
			{/if}
			<tr class="{$class}" valign="top">
				<td align="center" style="padding:5px;" valign="middle">{$items.estate_id}</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.estate_title} ({$items.viewed})
				</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.category_name}
				</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.estate_price}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$items.created_date|date_format:"%d-%m-%Y"}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$items.estate_duration|date_format:"%d-%m-%Y"}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$statusList[$items.estStatus].text}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<table cellpadding="0" cellspacing="0" border="0px;">
					<tr>
					  	<td style="border: 0px;padding-right:5px;">
					  		<img src="backend/images/edit.gif" width="15" height="15" title="edit" onClick="return submitform('?hdl=estate/regist',{$items.estate_id},{$page},'2');" class="button" />
					  	</td>
						<td style="border: 0px;padding-left:5px;">
							<input type="checkbox" name="deleteId[]" value="{$items.estate_id}" />
						</td>
					</tr>
					</table>
				</td>
			</tr>
			{/foreach}
			{else}
			<tr valign="top">
				<td colspan="8" align="center" style="padding:5px;" valign="middle">
				{$message}
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="8" valign="middle" style="height:25px;">
					{if $count gt 1}
					<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
						{$paging}
					</div>
					{/if}
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}