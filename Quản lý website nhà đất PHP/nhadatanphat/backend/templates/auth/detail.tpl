{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function submitform(value) {
		var theform = document.frmaccount;
		theform.issubmit.value = value;
		theform.submit();
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frmaccount" id="frmaccount" action="" method="post">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="2" style="height:15px;">
					{#ACCOUNT_INFOMATION#}
				</td>
			</tr>
			<tr valign="top">
				<td width="30%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#ACCOUNT_ID#}: </td>
				<td width="69%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;">
					{$accountid}
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_NAME#}: </td>
				<td align="left" valign="middle" class="tdregist">
					{$accountData.account_name}
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_FULL_NAME#}: </td>
				<td align="left" valign="middle" class="tdregist">
					{$accountData.account_full_name}
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_SEX#}: </td>
				<td align="left" valign="middle" class="tdregist">
					{foreach item=sex from=$sexList}
						{if $accountData.account_sex eq $sex.value}{$sex.text}{/if}
					{/foreach}
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_EMAIL#}: </td>
				<td align="left" valign="middle" class="tdregist">
					{$accountData.account_email}
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_ADDRESS#}: </td>
				<td align="left" valign="middle" class="tdregist">
					{$accountData.account_address}
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_PHONE#}: </td>
				<td align="left" valign="middle" class="tdregist">
					{$accountData.account_phone}
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_MOBILE#}: </td>
				<td align="left" valign="middle" class="tdregist">
					{$accountData.account_mobile}
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#STATUS#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="checkbox" name="status" id="status" {if $accountData.status eq 1}checked="true"{/if} disabled="disabled"/>
					&nbsp;<i style="color:#FF0000">({#COMMENT_STATUS#})</i>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="2" style="height:15px;">
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}