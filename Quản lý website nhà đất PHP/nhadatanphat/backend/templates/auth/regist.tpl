{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function submitform(value) {
		var theform = document.frmaccount;
		theform.issubmit.value = value;
		theform.submit();
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frmaccount" id="frmaccount" action="" method="post">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="2" style="height:15px;">
					Update account admin
				</td>
			</tr>
			{if $message != ""}
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;">{$message}</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr valign="top">
				<td width="30%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#ACCOUNT_ID#}: </td>
				<td width="69%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;">
					{$accountid}
					<input type="hidden" name="account_id" id="account_id" value="{$accountid}" />
					<input type="hidden" name="page" id="page" value="{$page}" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_NAME#}: </td>
				<td align="left" valign="middle" class="tdregist">
					{if $accountid eq ""}
					<input type="text" name="account_name" id="account_name" value="{$accountData.account_name}" class="input_text" />
					{else}
					<input type="hidden" name="account_name" id="account_name" value="{$accountData.account_name}" class="input_text" />
					{$accountData.account_name}
					{/if}
				</td>
			</tr>
			
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">Quyền người dùng: </td>
				<td align="left" valign="middle" class="tdregist">
					<select name="role_id" id="role_id" class="dropdown">
					{foreach item=item from=$userRole}
						<option value="{$item.value}" {if $accountData.role_id eq $item.value}selected="selected"{/if}>{$item.text}</option>
					{/foreach}
					</select>
				</td>
			</tr>
			
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_PASS#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="password" name="account_pass" id="account_pass" value="" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_PASS_CONFIRM#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="password" name="account_pass_confirm" id="account_pass_confirm" value="" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_FULL_NAME#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="text" name="account_full_name" id="account_full_name" value="{$accountData.account_full_name}" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_SEX#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<select name="account_sex" id="account_sex" class="dropdown">
					{foreach item=sex from=$sexList}
						<option value="{$sex.value}" {if $accountData.account_sex eq $sex.value}selected="selected"{/if}>{$sex.text}</option>
					{/foreach}
					</select>					
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_EMAIL#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="text" name="account_email" id="account_email" value="{$accountData.account_email}" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_ADDRESS#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="text" name="account_address" id="account_address" value="{$accountData.account_address}" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_PHONE#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="text" name="account_phone" id="account_phone" value="{$accountData.account_phone}" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_MOBILE#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="text" name="account_mobile" id="account_mobile" value="{$accountData.account_mobile}" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#STATUS#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<select name="status" id="status" class="dropdown">
						{foreach item=status from=$statusList}
						<option value="{$status.value}" {if $accountData.status eq $status.value && $accountData.status ne ''}selected="selected"{/if}>{$status.text}</option>
						{/foreach}
					</select>					
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="2" style="height:15px;">
				</td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="{#UPDATE#}" class="button_new" onclick="javascript: submitform(1)" />
					<input type="button" name="isback" id="isback" value="{#BACK#}" class="button_new" onclick="javascript: submitform(0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}