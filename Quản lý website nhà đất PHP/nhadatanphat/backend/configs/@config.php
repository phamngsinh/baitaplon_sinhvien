<?php 
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: @config.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
if (!defined("CONFIFG_BACKEND_INC") ) {
	Define("CONFIFG_BACKEND_INC", 	TRUE);
	
	require_once "constants.inc";
	require_once "init_smarty_backend.inc";
	require_once "validate".DS."@validate.inc";
	require_once "message".DS."@message.inc";
}
?>