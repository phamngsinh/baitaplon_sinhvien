<?php
	/*****************************************************************
	 * @project_name: localframe
	 * @file_name: @validate.php
	 * @descr:
	 * 
	 * @author 	Khuong Van Chien - khuongchien@gmail.com
	 * @version 1.0
	 *****************************************************************/
	Define("GENERAL_VALIDATION_FILE", 		BACKEND_CONF_PATH.DS."validate".DS."generals_validation.ini");
	Define("ACCOUNT_VALIDATION_FILE", 		BACKEND_CONF_PATH.DS."validate".DS."account_validation.ini");
	Define("NEWS_VALIDATION_FILE", 			BACKEND_CONF_PATH.DS."validate".DS."news_validation.ini");
	Define("PROMOTE_VALIDATION_FILE", 		BACKEND_CONF_PATH.DS."validate".DS."promote_validation.ini");
	Define("SUPPORT_VALIDATION_FILE", 		BACKEND_CONF_PATH.DS."validate".DS."support_validation.ini");
	Define("CATEGORY_VALIDATION_FILE", 		BACKEND_CONF_PATH.DS."validate".DS."category_validation.ini");
	Define("INTERIOR_VALIDATION_FILE", 		BACKEND_CONF_PATH.DS."validate".DS."interior_validation.ini");
	Define("INFORM_VALIDATION_FILE", 		BACKEND_CONF_PATH.DS."validate".DS."inform_validation.ini");
	Define("PROVINCE_VALIDATION_FILE", 		BACKEND_CONF_PATH.DS."validate".DS."province_validation.ini");
	Define("DISTRICT_VALIDATION_FILE", 		BACKEND_CONF_PATH.DS."validate".DS."district_validation.ini");
	Define("KEYWORD_VALIDATION_FILE", 		BACKEND_CONF_PATH.DS."validate".DS."keyword_validation.ini");
	Define("COMPANY_VALIDATION_FILE", 		BACKEND_CONF_PATH.DS."validate".DS."company_validation.ini");
	Define("CHILD_VALIDATION_FILE", 		BACKEND_CONF_PATH.DS."validate".DS."child_validation.ini");
	
	Define("CAT_INTERIOR_VALIDATION_FILE", 	BACKEND_CONF_PATH.DS."validate".DS."cat_interior_register.ini");
	Define("CONFIG_VALIDATION_FILE", 		BACKEND_CONF_PATH.DS."validate".DS."config_register.ini");
	Define("ESTATE_VALIDATION_FILE", 		BACKEND_CONF_PATH.DS."validate".DS."estate_validation.ini");
	Define("MEMBER_VALIDATION_FILE", 		BACKEND_CONF_PATH.DS."validate".DS."member_validation.ini");

?>