<?php
Define("ESTATE_IMAGE_ERRORS", 		"Một trong các ảnh mô tả hoặc file Video của bạn không đúng định dạng.");
Define("ESTATE_IMAGE_SIZE", 		"Một trong các ảnh mô tả hoặc file Video của bạn không nhỏ hơn ");
Define("ESTATE_TITLE_LENGTH", 		"Tiêu đề tin là cần phải nhập không quá 255 ký tự.");
Define("ESTATE_CATEGORY_NULL", 		"Loại tin rao là cần phải chọn cả hai thông tin.");
Define("ESTATE_CHILD_NULL", 		"Loại tin rao là cần phải chọn cả hai thông tin.");
Define("ESTATE_ADDRESS_LENGTH", 	"Vị trí là cần phải nhập không quá 255 ký tự.");
Define("ESTATE_PROVINCE_NULL", 		"Tên Tỉnh/Thành trong địa chỉ địa ốc là cần phải chọn.");
Define("ESTATE_DISTRICT_NULL", 		"Tên Quận/Huyện trong địa chỉ địa ốc là cần phải chọn.");
Define("ESTATE_PRICE_ERROR", 		"Giá tiền bạn mong muốn có được là cần phải nhập kiểu số không quá 20 ký tự (VD: 1.000.000.000).");
Define("ESTATE_AREA_LENGTH", 		"Diện tích phải nhập không quá 255 ký tự.");
Define("ESTATE_PERSON_LENGTH", 		"Tên người liên hệ là cần phải nhập không quá 255 ký tự.");
Define("ESTATE_COMPANY_LENGTH", 	"Tên công ty liên hệ phải nhập không quá 255 ký tự.");
Define("ESTATE_ADDPER_LENGTH", 		"Địa chỉ liên hệ là cần phải nhập không quá 255 ký tự.");
Define("ESTATE_EMAIL_NULL", 		"Địa chỉ mail liên hệ là cần phải nhập không quá 100 ký tự.");
Define("ESTATE_EMAIL_ERRORS", 		"Địa chỉ mail liên hệ nhập không đúng định dạng.");
Define("ESTATE_PHONE_NULL", 		"Số điện thoại của bạn là cần phải nhập không quá 255 ký tự.");
Define("ESTATE_PHONE_ERRORS", 		"Số điện thoại bạn nhập không đúng định dạng.");
Define("ESTATE_WEBSITE_LENGTH", 	"Địa chỉ website nhập không quá 255 ký tự.");
Define("ESTATE_WEBSITE_ERRORS", 	"Địa chỉ website bạn nhập không đúng định dạng.");
Define("ESTATE_DURATION_ERROR", 	"Ngày ngừng rao tin bạn nhập không đúng định dạng.");
?>