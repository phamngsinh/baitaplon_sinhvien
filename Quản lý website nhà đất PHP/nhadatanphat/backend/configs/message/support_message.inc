<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: support_message.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
if (!Defined("SUPPORT_MESSAGE_INC") ) {
	Define("SUPPORT_MESSAGE_INC", 		TRUE);
	
	Define("SUPPORT_ACCOUNT_EXISTED", 	"Tài khoản support đã tồn tại trong database");
	Define("SUPPORT_ACCOUNT_NULL", 		"Tài khoản không được trống");
	Define("SUPPORT_ACCOUNT_LENGTH",	"Tên tài khoản phải nhỏ hơn 50 ký tự.");
	Define("SUPPORT_TITLE_NULL", 		"Tiêu đề tài khoản không được trống");
	Define("SUPPORT_TITLE_LENGTH", 		"Tiêu đề tài khoản phải nhỏ hơn 255 ký tự.");
	Define("SUPPORT_SORT_ERROR",		"Thứ tự hiển thị là kiểu số nguyên lớn hơn 0 và nhỏ hơn 9999999999.");
}	
?>
