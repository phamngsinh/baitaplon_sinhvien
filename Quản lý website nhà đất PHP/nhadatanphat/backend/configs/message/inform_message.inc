<?php
/*****************************************************************
 * @project_name: localproject
 * @package: package_name
 * @file_name: inform_message.inc
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
if (!Defined("INFORM_MESSAGE_INC") ) {
	Define("INFORM_MESSAGE_INC", 			TRUE);
	
	Define("INFORM_NAME_EXISTED", 	"Tên thông tin công ty đã tồn tại trong database.");
	Define("INFORM_IMAGE_ERROR", 	"Ảnh mô tả thông tin không đúng định dạng.");
	Define("INFORM_IMAGE_SIZE", 	"Ảnh mô tả thông tin phải nhỏ hơn ");
	Define("INFORM_NAME_NULL", 		"Tên thông tin không được trống");
	Define("INFORM_NAME_LENGTH", 	"Tên thông tin phải nhỏ hơn 255 ký tự.");
	Define("INFORM_TITLE_NULL", 	"Tiêu đề thông tin không được trống");
	Define("INFORM_TITLE_LENGTH", 	"Tiêu đề thông tin phải nhỏ hơn 500 ký tự.");
	Define("INFORM_CONTENT_NULL", 	"Nội dung thông tin không được trống");
	Define("INFORM_IMAGE_NULL", 	"Tên file ảnh mô tả thông tin không được trống");
	Define("INFORM_IMAGE_LENGTH", 	"Tên file ảnh mô tả thông phải nhỏ hơn 255 ký tự.");
	Define("INFORM_SORT_ERROR",		"Thứ tự hiển thị là kiểu số nguyên lớn hơn 0 và nhỏ hơn 9999999999.");
}
?>