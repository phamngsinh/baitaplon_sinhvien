<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: promote_message.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
if (!Defined("PROMOTER_MESSAGE_INC") ) {
	Define("PROMOTER_MESSAGE_INC", 	TRUE);

	Define("PROMOTER_NAME_EXISTED", 	"Tên nhà quảng cáo đã tồn tại trong database.");
	Define("PROMOTER_BANNER_ERROR", 	"File banner quảng cáo không đúng định dạng.");
	Define("PROMOTER_BANNER_SIZE", 		"File banner quảng cáo phải nhỏ hơn <= ");
	Define("PROMOTER_NAME_NULL", 		"Tên nhà quảng cáo không được trống");
	Define("PROMOTER_NAME_LENGTH", 		"Tên nhà quảng cáo phải nhỏ hơn 255 ký tự.");
	Define("PROMOTER_BANNER_LENGTH", 	"Banner phải nhỏ hơn 255 ký tự.");
	Define("PROMOTER_CONTENT_NULL", 	"Nội dung quảng cáo không được trống.");
	Define("PROMOTER_CONTENT_LENGTH", 	"Nội dung quảng cáo phải nhỏ hơn 500 ký tự.");
	Define("PROMOTER_WEBSITE_LENGTH", 	"Tên website quảng cáo phải nhỏ hơn 255 ký tự.");
	Define("PROMOTER_START_NULL", 		"Ngày bắt đầu hiển thị là cần phải nhập.");
	Define("PROMOTER_START_ERRORS", 	"Ngày bắt đầu hiển thị không đúng định dạng.");
	Define("PROMOTER_END_ERRORS", 		"Ngày kết thúc hiển thị không đúng định dạng.");
}	
?>
