<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: genre_message.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
if (!defined("GENRE_MESSAGE_INC") ) {
	Define("GENRE_MESSAGE_INC", 	TRUE);
	
	Define("MESSAGE_RESULT_NULL", 	"Not record to do display.");
	Define("MESSAGE_LOGIN", 		"Username or Password your it's false.");
	Define("MESSAGE_INSERT_FALSE", 	"Execute query insert data your it's false.");
	Define("MESSAGE_UPDATE_FALSE", 	"Execute query update data your it's false.");
	Define("MESSAGE_DELETE_FALSE", 	"Execute query delete data your it's false.");
}
?>
