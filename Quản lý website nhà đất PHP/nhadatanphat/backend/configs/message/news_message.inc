<?php
/*****************************************************************
 * @project_name: sportsifu_v2
 * @package: package_name
 * @file_name: news_message.inc
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
if (!Defined("NEWS_MESSAGE_INC") ) {
	Define("NEWS_MESSAGE_INC", 			TRUE);
	
	Define("NEWS_NAME_EXISTED", 	"Tên tin này đã tồn tại trong database.");
	Define("NEWS_IMAGE_ERROR", 		"Ảnh mô tả thông tin không đúng định dạng.");
	Define("NEWS_IMAGE_SIZE", 		"File ảnh phải nhỏ hơn ");
	Define("NEWS_NAME_NULL", 		"Tên tin không được trống");
	Define("NEWS_NAME_LENGTH", 		"Tên tin phải nhỏ hơn 100 ký tự.");
	Define("NEWS_TITLE_NULL", 		"Tiêu đề tin không được trống");
	Define("NEWS_TITLE_LENGTH", 	"Tiêu đề tin phải nhỏ hơn 500 ký tự.");
	Define("NEWS_CONTENT_NULL", 	"Nội dung tin không được trống");
	Define("NEWS_IMAGE_NULL", 		"Tên file ảnh không được trống");
	Define("NEWS_IMAGE_LENGTH", 	"Tên file ảnh phải nhỏ hơn 255 ký tự.");
}
?>