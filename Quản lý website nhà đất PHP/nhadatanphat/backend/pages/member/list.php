<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: list.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("member.conf");
		
		$page	 	= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: null;
		$memberid	= isset($_REQUEST["memberid"]) ? 	trim($_REQUEST["memberid"])	: null;
		$status	 	= isset($_POST["status"]) ? 		trim($_POST["status"]) 			: null;
		$delete	 	= isset($_POST["delete_"]) ? 		trim($_POST["delete_"]) 		: null;
		$memberName	= isset($_POST["memberName"]) ? 		trim($_POST["memberName"]) 		: null;
		$startdate	= new DateTimes("startdate", 		START_DATE, END_DATE, DATE_PHP, FALSE, "dropdown");
		$enddate	= new DateTimes("enddate", 			START_DATE, END_DATE, DATE_PHP, FALSE, "dropdown");
		$enddate->setEmpty(NULL, "");

		if (is_null($status) && !strlen($page) && isset($_SESSION["SEARCH_MEMBER"]))
			$_SESSION["SEARCH_MEMBER"] = null;
		
		if (!empty($status)) {
			$_SESSION["SEARCH_MEMBER"]["startdate"]		= $startdate->getDateTime();
			$_SESSION["SEARCH_MEMBER"]["enddate"]		= $enddate->getDateTime();
			$_SESSION["SEARCH_MEMBER"]["memberName"]		= $memberName;
			$_SESSION["SEARCH_MEMBER"]["memberstatus"]	= isset($_REQUEST["memberstatus"]) ? 	trim($_REQUEST["memberstatus"])	: null;
		}
		if (!isset($_SESSION["SEARCH_MEMBER"]))
			$_SESSION["SEARCH_MEMBER"] = null;

		if (empty($page))
			$page = 1;

		if(!empty($_SESSION["SEARCH_MEMBER"]["startdate"]))
			$startdate->setSelected($_SESSION["SEARCH_MEMBER"]["startdate"]);
			
		if(!empty($_SESSION["SEARCH_MEMBER"]["enddate"]))
			$enddate->setSelected($_SESSION["SEARCH_MEMBER"]["enddate"]);

		$memberDao = new MemberDao();
		if (!empty($delete)) {
			$memberDao->beginTrans();
			try {
				$memberDao->delete_member($memberid);
				$memberDao->commitTrans();
			} catch (Exception $ex) {
				$memberDao->rollbackTrans();
				$smarty->assign("exefalse", MESSAGE_DELETE_FALSE);
			}
		}
		$offset 	   	= (int)($page-1)*LIMIT_RECORD;
		$count		   	= $memberDao->getCountMember($_SESSION["SEARCH_MEMBER"]["startdate"], $_SESSION["SEARCH_MEMBER"]["enddate"], $_SESSION["SEARCH_MEMBER"]["memberstatus"], $_SESSION["SEARCH_MEMBER"]["memberName"]);
		if ($offset >= $count) {
			$offset		= (int)$offset - (int)LIMIT_RECORD;
			$page		= $page - 1;
		}
		$memberList = $memberDao->getAllMembers($_SESSION["SEARCH_MEMBER"]["startdate"], $_SESSION["SEARCH_MEMBER"]["enddate"], $_SESSION["SEARCH_MEMBER"]["memberstatus"], (int)LIMIT_RECORD, $offset, $_SESSION["SEARCH_MEMBER"]["memberName"]);
		foreach ($memberList as $key => $values) $memberList[$key]["member_content"] 	= Utils::cutTitle($values["member_content"]);

		//$smarty->assign("paging",   		PagerUtils::PagerSmarty($page, $count, "?hdl=member/list", (int)LIMIT_RECORD, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, LIMIT_RECORD, array());
		$smarty->assign("paging",			$pageAll["all"]);
		$smarty->assign("memberList",   	$memberList);
		$smarty->assign("statusList",  		Status::getList($textlang));
		$smarty->assign("startdate",		$startdate->render());
		$smarty->assign("enddate", 			$enddate->render());
		$smarty->assign("count", 			$count);
		$smarty->assign("page", 			$page);
		$smarty->assign("searchData", 		$_SESSION["SEARCH_MEMBER"]);
		$smarty->assign("message", 			MESSAGE_RESULT_NULL);

		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>