<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: regist.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("keyword.conf");
		
		$keywordid   = isset($_REQUEST["keywordid"]) ? trim($_REQUEST["keywordid"])	: null;
		$page	  	 = isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: 1;
		$issubmit 	 = isset($_POST["issubmit"]) ? 		trim($_POST["issubmit"]) 		: null;
		$message  	 = null;
		$keywordDao  = new KeywordDao();
		$keywordData = array();
		
		if (is_null($issubmit)) { // Is load default
			if ($keywordid) $keywordData = $keywordDao->getKeywordById($keywordid);
		} else if ($issubmit) { // Is edit data
			$keywordData 	= isset($_POST["object"]) ? $_POST["object"]: null;

		 	$validate = new validate("keyword_validate", KEYWORD_VALIDATION_FILE);
			$message .= $validate->execute(trim($keywordData["keyword_start"]), trim($keywordData["keyword_end"]));

			if ($message == "") {
				$keywordDao->beginTrans();
				try {
					$objects = $keywordData;
					if (!is_null($keywordid) && $keywordid != "") { // Is update data
						$keywordDao->update_keyword($objects, $keywordid);
					} else { // Is regist date
						$objects["created_date"] = date(DATE_PHP);
						$keywordDao->insert_keyword($objects);
					}
					$keywordDao->commitTrans();
					header("Location: ?hdl=keyword/list&page=$page");
				} catch (Exception $ex) {
					$keywordDao->rollbackTrans();
					if (!is_null($keywordid) && $keywordid != "") $message .= MESSAGE_UPDATE_FALSE.BR_TAG;
					else $message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
		}

		$smarty->assign("keywordData",  $keywordData);
		$smarty->assign("keywordid",   	$keywordid);
		$smarty->assign("page",   		$page);
		$smarty->assign("message", 		$message);

		$keywordDao->doClose();

		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>