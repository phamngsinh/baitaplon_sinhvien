<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: list.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("keyword.conf");
		
		$page	 	= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: null;
		$keywordid	= isset($_REQUEST["keywordid"]) ? 	trim($_REQUEST["keywordid"])	: null;
		$status	 	= isset($_POST["status"]) ? 		trim($_POST["status"]) 			: null;
		$delete	 	= isset($_POST["delete_"]) ? 		trim($_POST["delete_"]) 		: null;

		if (is_null($status) && !strlen($page) && isset($_SESSION["SEARCH_KEYWORD"])) $_SESSION["SEARCH_KEYWORD"] = null;
		if (!empty($status)) {
			$_SESSION["SEARCH_KEYWORD"]["keywordstart"]	= isset($_REQUEST["keywordstart"]) ? 	trim($_REQUEST["keywordstart"])		: null;
			$_SESSION["SEARCH_KEYWORD"]["keywordend"]	= isset($_REQUEST["keywordend"]) ? 		trim($_REQUEST["keywordend"])		: null;
			$_SESSION["SEARCH_KEYWORD"]["keywordstatus"]= isset($_REQUEST["keywordstatus"]) ? 	trim($_REQUEST["keywordstatus"])	: null;
			$_SESSION["SEARCH_KEYWORD"]["keywordstart"]	= Utils::getNumberic($_SESSION["SEARCH_KEYWORD"]["keywordstart"]);
			$_SESSION["SEARCH_KEYWORD"]["keywordend"]	= Utils::getNumberic($_SESSION["SEARCH_KEYWORD"]["keywordend"]);
		}
		if (!isset($_SESSION["SEARCH_KEYWORD"])) $_SESSION["SEARCH_KEYWORD"] = null;
		if (empty($page)) $page = 1;

		$keywordDao = new KeywordDao();
		if (!empty($delete)) {
			$keywordDao->beginTrans();
			try {
				$keywordDao->delete_keyword($keywordid);
				$keywordDao->commitTrans();
			} catch (Exception $ex) {
				$keywordDao->rollbackTrans();
				$smarty->assign("exefalse", MESSAGE_DELETE_FALSE);
			}
		}
		$offset 	   	= (int)($page-1)*LIMIT_RECORD;
		$count		   	= $keywordDao->getCountKeyword($_SESSION["SEARCH_KEYWORD"]["keywordstart"], $_SESSION["SEARCH_KEYWORD"]["keywordend"], $_SESSION["SEARCH_KEYWORD"]["keywordstatus"]);
		if ($offset >= $count) {
			$offset		= (int)$offset - (int)LIMIT_RECORD;
			$page		= $page - 1;
		}
		$keywordList = $keywordDao->getAllKeywords($_SESSION["SEARCH_KEYWORD"]["keywordstart"], $_SESSION["SEARCH_KEYWORD"]["keywordend"], $_SESSION["SEARCH_KEYWORD"]["keywordstatus"], (int)LIMIT_RECORD, $offset);

		//$smarty->assign("paging",   		PagerUtils::PagerSmarty($page, $count, "?hdl=keyword/list", (int)LIMIT_RECORD, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, LIMIT_RECORD, array());
		$smarty->assign("paging",			$pageAll["all"]);
		$smarty->assign("keywordList",   	$keywordList);
		$smarty->assign("count", 			$count);
		$smarty->assign("page", 			$page);
		$smarty->assign("searchData", 		$_SESSION["SEARCH_KEYWORD"]);
		$smarty->assign("message", 			MESSAGE_RESULT_NULL);

		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>