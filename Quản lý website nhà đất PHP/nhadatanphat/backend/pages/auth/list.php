<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: #core.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("account.conf");
		
		$page	 	= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 	: 1;
		$accountid	= isset($_REQUEST["accountid"]) ? 	trim($_REQUEST["accountid"]): null;
		$delete	 	= isset($_GET["delete"]) ? 			trim($_GET["delete"]) 		: null;

		$accountDao  = new AccountDao();
		if ($delete != "") {
			if($accountid != $account_info_["account_id"]){
				$accountDao->delete_account($accountid);
			}
		}

		if (!empty($accountid)) {
			$status	= isset($_POST["status"][$accountid]) ? $_POST["status"][$accountid]: null;
			$accountData["status"] 		= !empty($status) ? 1 : 0;

			if($accountid == $account_info_["account_id"]){
				$accountData["status"] = Status::active;
			}
			$accountDao->update_account($accountData, $accountid);
		}
		$offset 	   	= (int)($page-1)*LIMIT_RECORD;
		$count		   	= $accountDao->getCountaccount();
		if ($offset >= $count) {
			$offset		= (int)$offset - (int)LIMIT_RECORD;
			$page		= $page - 1;
		}
		$accountList = $accountDao->getAllAccounts((int)LIMIT_RECORD, $offset);

		//$smarty->assign("paging",   		PagerUtils::PagerSmarty($page, $count, "?hdl=auth/list", (int)LIMIT_RECORD, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, LIMIT_RECORD, array());
		$smarty->assign("paging",			$pageAll["all"]);
		$smarty->assign("accountList",   	$accountList);
		$smarty->assign("count", 			$count);
		$smarty->assign("page", 			$page);
		$smarty->assign("message", 			MESSAGE_RESULT_NULL);

		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>
