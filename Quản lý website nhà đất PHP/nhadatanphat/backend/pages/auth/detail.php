<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: #core.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined('DS') or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("account.conf");

		$accountid   = isset($_GET["accountid"]) ? 	trim($_GET["accountid"]) : null;
		
		$permissionList = UserRole::getRoleList();
		$permissionList = $permissionList[$_SESSION["_LOGIN_BACKEND_"]['role_id']];
		if(is_array($permissionList) && count($permissionList) > 0)
			$accountid = $_SESSION["_LOGIN_BACKEND_"]['account_id'];
		
		$accountData = null;
		if ($accountid) {
			$accountDao  = new AccountDao();
			$accountData = $accountDao->getAccountById((int)$accountid);
		}

		$smarty->assign("accountData",   	$accountData);
		$smarty->assign("accountid",   		$accountid);
		$smarty->assign("sexList",  		SexFlag::getList($textlang));
		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>