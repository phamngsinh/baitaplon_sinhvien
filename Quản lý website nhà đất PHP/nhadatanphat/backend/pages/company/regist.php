<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: regist.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("company.conf");
		
		$companyid  = isset($_REQUEST["companyid"]) ? 	trim($_REQUEST["companyid"])	: null;
		$page	  	= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: 1;
		$issubmit 	= isset($_POST["issubmit"]) ? 		trim($_POST["issubmit"]) 		: null;
		
		$oFCKeditor = new FCKeditor("object[company_content]");
		$oFCKeditor->Value = "" ;
		
		$message  	= null;
		$companyDao = new CompanyDao();
		$provinceDao= new ProvinceDao();
		$companyData= array();
		
		if (is_null($issubmit)) { // Is load default
			if ($companyid) $companyData = $companyDao->getCompanyById($companyid);
		} else if ($issubmit) { // Is edit data
			$companyData 	= isset($_POST["object"]) ? $_POST["object"]: null;
			
			if($companyData['is_show_in_homepage'] == 'on'){
				$companyData['is_show_in_homepage'] = '1';
			}else{
				$companyData['is_show_in_homepage'] = '0';
			}
			if($companyData['is_show_in_province'] == 'on'){
				$companyData['is_show_in_province'] = '1';
			}else{
				$companyData['is_show_in_province'] = '0';
			}

			$filesize 		= FileUtils::returnMB(MAX_UPLOAD_FILE_IMG*1024);
			$fileupload 	= new FileUpload($FILE);
		 	if ($fileupload->getName() == "") $company_logo = $companyData["company_logo"];
			else $company_logo	= "company_".date("YmdHsi").$fileupload->getExtFile();

		 	$validate = new validate("company_validate", COMPANY_VALIDATION_FILE);	 	
			$message .= $validate->execute($companyData["company_name"], $company_logo, $companyData["company_website"], $companyData["company_content"]);

			if ($message == "" && $fileupload->getName() != "") {
				if (!FileUtils::checkFileType($fileupload->getName(), IMG_FORMAT))
					$message .= COMPANY_LOGO_ERROR.BR_TAG;
				else if (FileUtils::returnMB($fileupload->getSize())> FileUtils::getUploadMaxFilesizeOfSite($filesize)) 
					$message .= COMPANY_LOGO_SIZE.(FileUtils::getUploadMaxFilesizeOfSite($filesize)*1024)." KB.".BR_TAG;	
			}
			if ($message == "" && $companyDao->checkExistedCompanyName($companyData["company_name"], $companyid))
				$message = COMPANY_NAME_EXISTED;

			if ($message == "") {
				//$companyDao->beginTrans();
				try {
					$objects = $companyData;
					$objects["company_logo"] 	= $company_logo;
					$objects["created_date"] 	= date("Y-m-d H:i");
					if (!is_null($companyid) && $companyid != "") { // Is update data
						if ($fileupload->getName() == ""){
							$companyDao->update_company($objects, $companyid);
						}else{
							$fileupload->uploaded_file(IMG_PATH.DS."company".DS, $company_logo);
							Utils::createThumbnailsImage(IMG_PATH.DS."company".DS.$company_logo, IMG_PATH.DS."company".DS."thumb_".$company_logo, 61, $fileupload->getExtFile());
							$companyDao->update_company($objects, $companyid);
							FileUtils::deleteFile(IMG_PATH.DS."company".DS, $companyData["company_logo"]);
							FileUtils::deleteFile(IMG_PATH.DS."company".DS, "thumb_".$companyData["company_logo"]);
						}
					} else { // Is regist date
						$companyDao->insert_company($objects);
						$fileupload->uploaded_file(IMG_PATH.DS."company".DS, $company_logo);
						Utils::createThumbnailsImage(IMG_PATH.DS."company".DS.$company_logo, IMG_PATH.DS."company".DS."thumb_".$company_logo, 61, $fileupload->getExtFile());
					}
					//$companyDao->commitTrans();
					header("Location: ?hdl=company/list&page=$page");
				} catch (Exception $ex) {
					//$companyDao->rollbackTrans();
					if (!is_null($companyid) && $companyid != "") $message .= MESSAGE_UPDATE_FALSE.BR_TAG;
					else $message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
		}
		
		$oFCKeditor->Value	= isset($companyData['company_content']) ? $companyData['company_content'] : null;
		$com_content		= $oFCKeditor->CreateHtml() ;

		$smarty->assign("com_content", $com_content);
		$smarty->assign("provinceList", $provinceDao->getAllProvinces(null, Status::active));
		$smarty->assign("companyData",  $companyData);
		$smarty->assign("statusList",  	Status::getList($textlang));
		$smarty->assign("companyid",   	$companyid);
		$smarty->assign("page",   		$page);
		$smarty->assign("message", 		$message);
		$companyDao->doClose();
		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>