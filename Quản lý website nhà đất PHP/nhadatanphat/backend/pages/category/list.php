<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: list.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("category.conf");
		
		$page	 	= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: null;
		$categoryid	= isset($_REQUEST["categoryid"]) ? 	trim($_REQUEST["categoryid"])	: null;
		$status	 	= isset($_POST["status"]) ? 		trim($_POST["status"]) 			: null;
		$delete	 	= isset($_POST["delete_"]) ? 		trim($_POST["delete_"]) 		: null;

		if (is_null($status) && !strlen($page) && isset($_SESSION["SEARCH_CATEGORY"])) $_SESSION["SEARCH_CATEGORY"] = null;
		if (!empty($status)) {
			$_SESSION["SEARCH_CATEGORY"]["categoryname"]	= isset($_REQUEST["categoryname"]) ? 	trim($_REQUEST["categoryname"])		: null;
			$_SESSION["SEARCH_CATEGORY"]["categorystatus"]	= isset($_REQUEST["categorystatus"]) ? 	trim($_REQUEST["categorystatus"])	: null;
		}
		if (!isset($_SESSION["SEARCH_CATEGORY"])) $_SESSION["SEARCH_CATEGORY"] = null;
		if (empty($page)) $page = 1;

		$categoryDao = new CategoryDao();
		if (!empty($delete)) {
			$categoryData 	= $categoryDao->getCategoryById($categoryid);
			$categoryDao->beginTrans();
			try {
				$categoryDao->delete_category($categoryid);
				$categoryDao->commitTrans();
				FileUtils::deleteFile(IMG_PATH.DS."category".DS, $categoryData["category_icon"]);
			} catch (Exception $ex) {
				$categoryDao->rollbackTrans();
				$smarty->assign("exefalse", MESSAGE_DELETE_FALSE);
			}
		}
		$offset 	   	= (int)($page-1)*LIMIT_RECORD;
		$count		   	= $categoryDao->getCountCategories($_SESSION["SEARCH_CATEGORY"]["categoryname"], $_SESSION["SEARCH_CATEGORY"]["categorystatus"]);
		if ($offset >= $count) {
			$offset		= (int)$offset - (int)LIMIT_RECORD;
			$page		= $page - 1;
		}
		$categoryList = $categoryDao->getAllCategories($_SESSION["SEARCH_CATEGORY"]["categoryname"], $_SESSION["SEARCH_CATEGORY"]["categorystatus"], (int)LIMIT_RECORD, $offset);

		//$smarty->assign("paging",   		PagerUtils::PagerSmarty($page, $count, "?hdl=category/list", (int)LIMIT_RECORD, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, LIMIT_RECORD, array());
		$smarty->assign("paging",			$pageAll["all"]);
		$smarty->assign("categoryList",   	$categoryList);
		$smarty->assign("Horizonta",  		MenuTop::getList($textlang));
		$smarty->assign("MNBottom",  		MenuBottom::getList($textlang));
		$smarty->assign("Vertical",  		Vertical::getList($textlang));
		$smarty->assign("CateKind",  		CateKind::getList($textlang));
		$smarty->assign("count", 			$count);
		$smarty->assign("page", 			$page);
		$smarty->assign("searchData", 		$_SESSION["SEARCH_CATEGORY"]);
		$smarty->assign("message", 			MESSAGE_RESULT_NULL);

		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>