<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: regist.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("category.conf");
		
		$categoryid  = isset($_REQUEST["categoryid"]) ? trim($_REQUEST["categoryid"])	: null;
		$page	  	 = isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: 1;
		$issubmit 	 = isset($_POST["issubmit"]) ? 		trim($_POST["issubmit"]) 		: null;
		$message  	 = null;
		$categoryDao = new CategoryDao();
		$categoryData= array();
		
		if (is_null($issubmit)) { // Is load default
			if ($categoryid) $categoryData = $categoryDao->getCategoryById($categoryid);
		} else if ($issubmit) { // Is edit data
			$categoryData 	= isset($_POST["object"]) ? $_POST["object"]: null;

			$filesize 		= FileUtils::returnMB(MAX_UPLOAD_FILE_IMG*1024);
			$fileupload 	= new FileUpload($FILE);
		 	if ($fileupload->getName() == "") $category_icon = $categoryData["category_icon"];
			else $category_icon	= "category_".date("YmdHsi").$fileupload->getExtFile();

		 	$validate = new validate("category_validate", CATEGORY_VALIDATION_FILE);	 	
			$message .= $validate->execute($categoryData["category_name"], $category_icon, $categoryData["category_sort"]);

			if ($message == "" && $fileupload->getName() != "") {
				if (!FileUtils::checkFileType($fileupload->getName(), IMG_FORMAT)) $message .= CATEGORY_ICON_ERROR.BR_TAG;
				else if (FileUtils::returnMB($fileupload->getSize())> FileUtils::getUploadMaxFilesizeOfSite($filesize)) $message .= CATEGORY_ICON_SIZE.(FileUtils::getUploadMaxFilesizeOfSite($filesize)*1024)." KB.".BR_TAG;	
			}
			if ($message == "" && $categoryDao->checkExistedCategoriesName($categoryData["category_name"], $categoryid)) $message = CATEGORY_NAME_EXISTED;

			if ($message == "") {
				$categoryDao->beginTrans();
				try {
					$objects = $categoryData;
					$objects["category_icon"] 	= $category_icon;
					$objects["created_date"] 	= date(DATE_PHP);
					if (!is_null($categoryid) && $categoryid != "") { // Is update data
						if ($fileupload->getName() == "") $categoryDao->update_category($objects, $categoryid);
						else if ($fileupload->uploaded_file(IMG_PATH.DS."category".DS, $category_icon)) {
							$categoryDao->update_category($objects, $categoryid);
							FileUtils::deleteFile(IMG_PATH.DS."category".DS, $categoryData["category_icon"]);
						}
					} else { // Is regist date
						if($fileupload->getName() == "" || $fileupload->uploaded_file(IMG_PATH.DS."category".DS, $category_icon)) {
							$categoryDao->insert_category($objects);
						}
					}
					$categoryDao->commitTrans();
					header("Location: ?hdl=category/list&page=$page");
				} catch (Exception $ex) {
					$categoryDao->rollbackTrans();
					if (!is_null($categoryid) && $categoryid != "") $message .= MESSAGE_UPDATE_FALSE.BR_TAG;
					else $message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
		}

		$smarty->assign("categoryData",   	$categoryData);
		$smarty->assign("Horizonta",  		MenuTop::getList($textlang));
		$smarty->assign("MNBottom",  		MenuBottom::getList($textlang));
		$smarty->assign("Vertical",  		Vertical::getList($textlang));
		$smarty->assign("CateKind",  		CateKind::getList($textlang));
		$smarty->assign("categoryid",   	$categoryid);
		$smarty->assign("page",   			$page);
		$smarty->assign("message", 			$message);
		$categoryDao->doClose();
		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>