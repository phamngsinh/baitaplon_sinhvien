<?php
	/**
	 * @note:	file default.php
	 * 
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	defined('DS') or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;
	$smarty->config_load("estate.conf");
	if (file_exists($template)) {
		require_once BACKEND_PAGE_PATH.DS."include".DS."categories.php";
		
		$estateid  		= isset($_REQUEST["estateid"]) ?	$_REQUEST["estateid"]	: null;
		$issubmit  		= isset($_POST["issubmit"]) ?	$_POST["issubmit"]	: null;
		
		$EstateDao = new EstateDao();
		$CategoryDao = new CategoryDao();
		$duration	= new DateTimes("estate_duration", 	date("Y"), date("Y")+5, DATE_PHP, FALSE, "dropdown");
		$deleted_date	= new DateTimes("deleted_date", 	date("Y"), date("Y")+5, DATE_PHP, FALSE, "dropdown");			
		$duration->setEmpty();
		$deleted_date->setEmpty();
		
		$estateObj = array();
		
		$oFCKeditor = new FCKeditor("estateObj[estate_note]") ;
		
		if ($issubmit != '1'):
			$estateObj	= $EstateDao->getEstateById($estateid);
			$duration->setSelected($estateObj["estate_duration"]);
			$deleted_date->setSelected($estateObj["deleted_date"]);
			//$oFCKeditor->Value	= $estateObj["estate_note"];
			
			$estate_image[1]	= $estate["estate_image1"];
			$estate_image[2]	= $estate["estate_image2"];
			$estate_image[3]	= $estate["estate_image3"];
			$estate_image[4]	= $estate["estate_image4"];
			$estate_image[5]	= $estate["estate_image5"];
			
		else:
			
			$estateObj 	= isset($_POST["estateObj"]) ? $_POST["estateObj"] : array();
			$message  	= null;
			$fileupload = new FileUpload($FILE);
			
			$filearray	= $fileupload->getKeys();
			$estateObj["estate_duration"]	= $duration->getDateTime();
			$estateObj["deleted_date"]	=   $deleted_date->getDateTime();
		 	$validate = new validate("estate_validate", ESTATE_VALIDATION_FILE);	 	
			
			$message .= $validate->execute(	$estateObj["estate_title"],
											$estateObj["category_id"],
											$estateObj["child_id"],
											$estateObj["estate_address"], 
											$estateObj["province_id"],
											$estateObj["district_id"],
											$estateObj["estate_price"],
											$estateObj["estate_area"], 
											$estateObj["estate_duration"],
											$estateObj["deleted_date"]);
			
			foreach ($filearray as $key => $filename) {
			 	$fileupload->setKeyName($filename);
			 	if ($fileupload->getName() != "") {
			 		
			 		if ($filename == "estate_video") {
			 			$filetype = AUD_FORMAT;
			 			$filesize = FileUtils::returnMB(MAX_UPLOAD_FILE_FLV);
			 			$_estate_video_old = $estateObj['estate_video'];
			 			$estateObj["estate_video"]	= "estate_".$key."_".date("YmdHsi").$fileupload->getExtFile();			 			
			 		} else {
			 			$filetype = IMG_FORMAT;
			 			$filesize = FileUtils::returnMB(MAX_UPLOAD_FILE_IMG);
			 			$estateObj["estate_image".$key]	= "estate_".$key."_".date("YmdHsi").$fileupload->getExtFile();	
			 		}			 		
			 		
			 		if (!FileUtils::checkFileType($fileupload->getName(), $filetype))
						$message .= ESTATE_IMAGE_ERRORS.BR_TAG;
					elseif (FileUtils::returnMB($fileupload->getSize()) > FileUtils::getUploadMaxFilesizeOfSite($filesize)) {
						if ($filename == "estate_video") {
							$message .= "File Video phải nhỏ hơn ".(FileUtils::getUploadMaxFilesizeOfSite($filesize))." MB.".BR_TAG;
						} else {
							if ($key == 1)
								$message .= " File ảnh lớn phải nhỏ hơn ".(FileUtils::getUploadMaxFilesizeOfSite($filesize)*1024)." KB.".BR_TAG;
							else
								$message .= " File ảnh ".($key -1)." phải nhỏ hơn ".(FileUtils::getUploadMaxFilesizeOfSite($filesize)*1024)." KB.".BR_TAG;
						}
					}	
			 	}
			 	//if ($message != "") break;
			}
			
			if ($message == "") {
				$EstateDao->beginTrans();
				try {
					$objects = $estateObj;
					$objects["estate_price"]		= Utils::formatPrice(Utils::getNumberic($objects["estate_price"]));
					$objects["estate_image1"] 		= strlen($estateObj["estate_image1"]) ? $estateObj["estate_image1"] : $estate_image[1];
					$objects["estate_image2"] 		= strlen($estateObj["estate_image2"]) ? $estateObj["estate_image2"] : $estate_image[2];
					$objects["estate_image3"] 		= strlen($estateObj["estate_image3"]) ? $estateObj["estate_image3"] : $estate_image[3];
					$objects["estate_image4"] 		= strlen($estateObj["estate_image4"]) ? $estateObj["estate_image4"] : $estate_image[4];
					$objects["estate_image5"] 		= strlen($estateObj["estate_image5"]) ? $estateObj["estate_image5"] : $estate_image[5];
					
					foreach ($filearray as $key => $filename) {
						$fileupload->setKeyName($filename);
						if ($fileupload->getName() != "" && $filename == "estate_video") {
							$fileupload->uploaded_file(AUD_PATH."estate".DS, $objects["estate_video"]);
							if ($_estate_video_old != '')
								@FileUtils::deleteFile(AUD_PATH."estate".DS,$_estate_video_old);																					
						}
						if ($fileupload->getName() != "" && $filename != "estate_video")
							$fileupload->uploaded_file(IMG_PATH."estate".DS, $objects["estate_image".$key]);
					}
					
					// Xoa cac file du lieu cu
					for($i =0; $i < count($filearray); $i++ ) {
						if (isset($_POST['del_'.($i+1)]) && ($_POST['del_'.($i+1)] == 1)) {
							if ( ($i+1) == 6 ) {
								@FileUtils::deleteFile(AUD_PATH."estate".DS,$objects["estate_video"]);
								$objects["estate_video"] = '';
							} else {
								@FileUtils::deleteFile(IMG_PATH."estate".DS,$objects["estate_image".($i+1)]);
								$objects["estate_image".($i+1)] = '';	
							}						
						}
					}

					if (!is_null($estateid) && $estateid != "") { // Is update data
						$EstateDao->update_estate($objects, $estateid);
					}
					$EstateDao->commitTrans();
					
					header("LOCATION: admin.php?hdl=estate/regist&estateid=$estateid");
					
					//$message = MESSAGE_INSERT_SUCCESS;
				} catch (Exception $ex) {
					$EstateDao->rollbackTrans();
					if (!is_null($estateid) && $estateid != "") $message .= MESSAGE_UPDATE_FALSE.BR_TAG;
					else $message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
			
			$smarty->assign("message", $message);
		endif;
		
		
		
		$CategoryList = $CategoryDao->getAllCategoryByKind(CateKind::estate, Status::active);
		//$CategoryList = $CategoryDao->getCategoriesList(array("kind"=>CateKind::estate, "status"=>Status::active));
		$parentid = empty($estateObj["category_id"]) ? $CategoryList[0]["category_id"] : $estateObj["category_id"];
		
		$categories = new Categories();
		$childDao	= new ChildDao();
		$smarty->assign("CategoryList",	$CategoryList);
		$smarty->assign("ChildList", $categories->ShowCategories($childDao, $parentid, 0, null, Status::active));
		
		$ProvinceDao= new ProvinceDao();
		$DistrictDao= new DistrictDao();
		$ProvinceId		= isset($estateObj["province_id"]) ? $estateObj["province_id"] : null;
		$ProvinceList	= $ProvinceDao->getAllProvinces(null,Status::active);
		$DistrictList	= $DistrictDao->getAllDistricts($ProvinceId, Status::active);
		$smarty->assign("ProvinceList",	$ProvinceList);
		$smarty->assign("DistrictList",	$DistrictList);
		$smarty->assign("Direction",	Direction::getList($textlang));
		
		$smarty->assign("estateObj",$estateObj);
		$oFCKeditor->Value	= isset($estateObj["estate_note"]) ? $estateObj["estate_note"] : null;
		$smarty->assign("estate_note",	$oFCKeditor->CreateHtml());
		$smarty->assign("duration", $duration->render());
		$smarty->assign("deleted_date", $deleted_date->render());
		$smarty->assign("genreList",  	EstateGenre::getList($textlang));
		$smarty->assign("estateid",  	$estateid);
		
		if ($estateid > 0) {
			$estateInitData	= $EstateDao->getEstateById($estateid);
			$smarty->assign("estateInitData", $estateInitData);
		}
		
		return $smarty->display($template);
	}
?>