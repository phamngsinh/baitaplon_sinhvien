<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: list.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("news.conf");
		
		$deleteIdArr	= isset($_REQUEST["deleteId"])? $_REQUEST["deleteId"]:array();

		$page	= isset($_REQUEST["page"]) ? 	trim($_REQUEST["page"]) 	: null;
		$newsid	= isset($_REQUEST["newsid"]) ? 	trim($_REQUEST["newsid"])	: null;
		$status	= isset($_POST["status"]) ? 	trim($_POST["status"]) 		: null;
		$delete	= isset($_POST["delete_"]) ? 	trim($_POST["delete_"]) 	: null;

		if (is_null($status) && !strlen($page) && isset($_SESSION["SEARCH_NEWS"])) $_SESSION["SEARCH_NEWS"] = null;
		if (!empty($status)) {
			$_SESSION["SEARCH_NEWS"]["newsname"]	= isset($_REQUEST["newsname"]) ? 	trim($_REQUEST["newsname"])		: null;
			$_SESSION["SEARCH_NEWS"]["newsgenre"]	= isset($_REQUEST["newsgenre"]) ? 	trim($_REQUEST["newsgenre"])	: null;
			$_SESSION["SEARCH_NEWS"]["newsstatus"]	= isset($_REQUEST["newsstatus"]) ? 	trim($_REQUEST["newsstatus"])	: null;
		}

		if (!isset($_SESSION["SEARCH_NEWS"]))
			$_SESSION["SEARCH_NEWS"] = null;

		if (empty($page))
			$page = 1;

		$newsDao = new NewsDao();
		if (!empty($delete)) {
			
			$imageList = array();
			for($i = 0; $i < count($deleteIdArr); $i ++){
				$newsData 	= $newsDao->getNewsById($deleteIdArr[$i]);
				$imageList[] = $newsData["promote_banner"];
			}

			//$newsDao->beginTrans();
			try {
				$newsDao->delete_news($deleteIdArr);
				//$newsDao->commitTrans();
				for($i = 0; $i < count($deleteIdArr); $i ++){
					FileUtils::deleteFile(IMG_PATH.DS."news".DS, $imageList[$i]);
				}
			} catch (Exception $ex) {
				//$newsDao->rollbackTrans();
				$smarty->assign("exefalse", MESSAGE_DELETE_FALSE);
			}
		}
		$offset 	   	= (int)($page-1)*LIMIT_RECORD;
		$count		   	= $newsDao->getCountNews($_SESSION["SEARCH_NEWS"]["newsname"], $_SESSION["SEARCH_NEWS"]["newsgenre"], $_SESSION["SEARCH_NEWS"]["newsstatus"]);
		if ($offset >= $count) {
			$offset		= (int)$offset - (int)LIMIT_RECORD;
			$page		= $page - 1;
		}
		$newsList = $newsDao->getAllNews($_SESSION["SEARCH_NEWS"]["newsname"], $_SESSION["SEARCH_NEWS"]["newsgenre"], $_SESSION["SEARCH_NEWS"]["newsstatus"], (int)LIMIT_RECORD, $offset);

		//$smarty->assign("paging",   	PagerUtils::PagerSmarty($page, $count, "?hdl=news/list", (int)LIMIT_RECORD, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, LIMIT_RECORD, array());
		$smarty->assign("paging",			$pageAll["all"]);
		$smarty->assign("newsList",   	$newsList);
		$smarty->assign("newsGenre",  	NewsGenre::getList($textlang));
		$smarty->assign("statusList",  	Status::getList($textlang));
		$smarty->assign("count", 		$count);
		$smarty->assign("page", 		$page);
		$smarty->assign("searchData", 	$_SESSION["SEARCH_NEWS"]);
		$smarty->assign("message", 		MESSAGE_RESULT_NULL);

		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>