<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: list.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		require_once BACKEND_PAGE_PATH.DS."include".DS."categories.php";
		$smarty->config_load("estate.conf");
		
		$page	 	= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: null;
		$estateid	= isset($_REQUEST["estateid"]) ? 	trim($_REQUEST["estateid"])	: null;
		$status	 	= isset($_POST["status"]) ? 		trim($_POST["status"]) 			: null;
		$startdate	= new DateTimes("startdate", 		START_DATE, END_DATE, DATE_PHP, FALSE, "dropdown");
		$enddate	= new DateTimes("enddate", 			START_DATE, END_DATE, DATE_PHP, FALSE, "dropdown");

		$deleteIdArr	= isset($_REQUEST["deleteId"])? $_REQUEST["deleteId"]:array();
//		print_r($deleteIdArr);die();
		$delete	 		= isset($_POST["delete_"]) ? trim($_POST["delete_"]):null;
		$expiredEstate	= isset($_POST["expiredEstate"]) ? $_POST["expiredEstate"]:"";
		$isFeeEstate	= isset($_POST["isFeeEstate"]) ? $_POST["isFeeEstate"]:"";
		$isVideoEstate	= isset($_POST["isVideoEstate"]) ? $_POST["isVideoEstate"]:"";
	
		$enddate->setEmpty(NULL, "");

		if (is_null($status) && !strlen($page) && isset($_SESSION["SEARCH_ESTATE"])) $_SESSION["SEARCH_ESTATE"] = null;
		if (!empty($status)) {
			$_SESSION["SEARCH_ESTATE"]["createstart"]	= $startdate->getDateTime();
			$_SESSION["SEARCH_ESTATE"]["createend"]		= $enddate->getDateTime();
			$_SESSION["SEARCH_ESTATE"]["title"]			= isset($_REQUEST["title"]) ? 		trim($_REQUEST["title"])		: null;
			$_SESSION["SEARCH_ESTATE"]["estatestatus"]	= isset($_REQUEST["estatestatus"]) ? trim($_REQUEST["estatestatus"]): null;
			$_SESSION["SEARCH_ESTATE"]["address"]		= isset($_REQUEST["address"]) ? 	trim($_REQUEST["address"])		: null;
			$_SESSION["SEARCH_ESTATE"]["provinceid"]	= isset($_REQUEST["provinceid"]) ? 	trim($_REQUEST["provinceid"])	: null;
			$_SESSION["SEARCH_ESTATE"]["districtid"]	= isset($_REQUEST["districtid"]) ? 	trim($_REQUEST["districtid"])	: null;
			$_SESSION["SEARCH_ESTATE"]["genre"]			= isset($_REQUEST["genre"]) ? 		trim($_REQUEST["genre"])		: null;
			$_SESSION["SEARCH_ESTATE"]["categoryid"]	= isset($_REQUEST["categoryid"]) ? 	trim($_REQUEST["categoryid"])	: null;
			$_SESSION["SEARCH_ESTATE"]["childid"]		= isset($_REQUEST["childid"]) ? 	trim($_REQUEST["childid"])		: null;
			$_SESSION["SEARCH_ESTATE"]["pricestart"]	= isset($_REQUEST["pricestart"]) ? 	trim($_REQUEST["pricestart"])	: null;
			$_SESSION["SEARCH_ESTATE"]["priceend"]		= isset($_REQUEST["priceend"]) ? 	trim($_REQUEST["priceend"])		: null;
			$_SESSION["SEARCH_ESTATE"]["direction"]		= isset($_REQUEST["direction"]) ? 	trim($_REQUEST["direction"])	: null;
			$_SESSION["SEARCH_ESTATE"]["expiredEstate"]	= $expiredEstate;
			$_SESSION["SEARCH_ESTATE"]["isFeeEstate"]	= $isFeeEstate;
			$_SESSION["SEARCH_ESTATE"]["isVideoEstate"]	= $isVideoEstate;
		}
		
		if (!isset($_SESSION["SEARCH_ESTATE"]))
			$_SESSION["SEARCH_ESTATE"] = null;
		
		if (empty($page))
			$page = 1;
		
		if (!empty($_SESSION["SEARCH_ESTATE"]["createstart"]))
			$startdate->setSelected($_SESSION["SEARCH_ESTATE"]["createstart"]);
		
		if (!empty($_SESSION["SEARCH_ESTATE"]["createend"]))
			$enddate->setSelected($_SESSION["SEARCH_ESTATE"]["createend"]);

		$estateDao = new EstateDao();
		$permissionList = UserRole::getRoleList();
		$permissionList = $permissionList[$_SESSION["_LOGIN_BACKEND_"]['role_id']];
		
		$tmpTitle = $_SESSION["SEARCH_ESTATE"]["title"];
		$_SESSION["SEARCH_ESTATE"]["title"] = Utils::vietnameseToASCII($_SESSION["SEARCH_ESTATE"]["title"]);
		
		if(is_array($permissionList) && count($permissionList) == UserRole::admin){
			if (!empty($delete)) {
				$estateDao->beginTrans();
				try {
					$estateDao->delete_estate($deleteIdArr);
					$estateDao->commitTrans();
				} catch (Exception $ex) {
					$estateDao->rollbackTrans();
					$smarty->assign("exefalse", MESSAGE_DELETE_FALSE);
				}
			}
		}else{
			$smarty->assign("accessDenied", "YES");
		}
		$offset 	   	= (int)($page-1)*LIMIT_RECORD;
		$count		   	= $estateDao->getCountEstateByParams($_SESSION["SEARCH_ESTATE"], $_SESSION["SEARCH_ESTATE"]["estatestatus"]);

		if ($offset >= $count) {
			$offset		= (int)$offset - (int)LIMIT_RECORD;
			$page		= $page - 1;
		}

		$estateList = $estateDao->getAllEstateByParams($_SESSION["SEARCH_ESTATE"], $_SESSION["SEARCH_ESTATE"]["estatestatus"], (int)LIMIT_RECORD, $offset);
		//Delete Expired records:
		$estateDao->deleteExpiredRecord();

		$ProvinceDao	= new ProvinceDao();
		$DistrictDao	= new DistrictDao();
		$CategoryDao	= new CategoryDao();
		$ChildDao		= new ChildDao();
		$categories		= new Categories();
		$ProvinceList	= $ProvinceDao->getAllProvinces(null, Status::active);
		if (!empty($_SESSION["SEARCH_ESTATE"]["provinceid"])) $DistrictList	= $DistrictDao->getAllDistricts(null, $_SESSION["SEARCH_ESTATE"]["provinceid"], Status::active);
		else $DistrictList	= null;
		$CategoryList	= $CategoryDao->getAllCategoryByKind(CateKind::estate, Status::active);
		$ChildList		= $categories->ShowCategories($ChildDao, $_SESSION["SEARCH_ESTATE"]["categoryid"], 0, null, Status::active);

		$smarty->assign("ChildList", 		$ChildList);
		//$smarty->assign("paging",   		PagerUtils::PagerSmarty($page, $count, "?hdl=estate/list", (int)LIMIT_RECORD, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, LIMIT_RECORD, array());
		$smarty->assign("paging",			$pageAll["all"]);
		$smarty->assign("estateList",   	$estateList);
		$smarty->assign("ProvinceList", 	$ProvinceList);
		$smarty->assign("DistrictList", 	$DistrictList);
		$smarty->assign("CategoryList", 	$CategoryList);
		$smarty->assign("ChildList", 		$ChildList);
		$smarty->assign("statusList",  		Status::getList($textlang));
		$smarty->assign("genreList",  		EstateGenre::getList($textlang));
		$smarty->assign("directionList",  	Direction::getList($textlang));
		$smarty->assign("startdate",		$startdate->render());
		$smarty->assign("enddate", 			$enddate->render());
		$smarty->assign("count", 			$count);
		$smarty->assign("page", 			$page);
		$smarty->assign("status", 			$status);
		
		$_SESSION["SEARCH_ESTATE"]["title"] = $tmpTitle;
		$smarty->assign("searchData", 		$_SESSION["SEARCH_ESTATE"]);

		$smarty->assign("message", 			MESSAGE_RESULT_NULL);
		$smarty->assign("expiredEstate", 	$expiredEstate);
		$smarty->assign("isFeeEstate", 		$isFeeEstate);
		$smarty->assign("isVideoEstate", 	$isVideoEstate);

		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>