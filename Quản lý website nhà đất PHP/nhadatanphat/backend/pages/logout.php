<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: logout.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined('DS') or die("Errors System");
	
	if (isset($_SESSION["_LOGIN_BACKEND_"])) unset($_SESSION["_LOGIN_BACKEND_"]);
	header("Location: ?hdl=login");
?>
