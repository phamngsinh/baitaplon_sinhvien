<?php
	/**
	 * @project_name: ntmusic
	 * @file_name: categories_list.php
	 * @descr:
	 * 
	 * @author Thunn
	 * @version 1.0	 2008/04/07, Thunn, created
	 **/
	defined('DS') or die("Errors System");
	
	$smarty->config_load("interior.conf");
	$limit = LIMIT_RECORD;
	$page = isset ($_REQUEST['page']) ? ($_REQUEST['page'] > 0 ? $_REQUEST['page'] : 1) : 1;
	$key_word = isset ($_REQUEST['key_word']) ? $_REQUEST['key_word'] : '';
	
	$categoriesDao = new CatInteriorDao();
	
	if (isset($_REQUEST['process']) && ($_REQUEST['process'] == 'del')) {
		$category_id = isset ($_REQUEST['category_id']) ? $_REQUEST['category_id'] : '';
		if ($category_id > 0) {
			$categoriesDao->delete_categories((int)$category_id);
		}
	}
	
	// get total records
	$totalRecords = $categoriesDao->get_categories_count($key_word);
	
	$totalPages = ($totalRecords % $limit == 0) ? (int) ($totalRecords / $limit) : (int) ($totalRecords / $limit +1);
	$page = ($page > $totalPages) ? 1 : $page;
	
	// get categories list
	$categories_list = $categoriesDao->get_categories_list(trim($key_word), (int) $limit, (int) $page);
	$start = $limit * ($page -1) + 1;
	
	$smarty->assign("key_word", $key_word);
	$smarty->assign("categories_list", $categories_list);
	$smarty->assign("totalRecords", $totalRecords);
	$smarty->assign("statusList",  		Status::getList($textlang));
	$smarty->assign("start", $start);
	
	$smarty->assign("links_params", "?hdl=interior/categories_edit&key_word=$key_word&page={$page}&category_id=");
	$smarty->assign("links_params_del", "?hdl=interior/categories_list&process=del&key_word=$key_word&page={$page}&category_id=");
	
	// get pages
	//$smarty->assign("pages_list", PagerUtils::PagerSmarty($page, $totalRecords, "?hdl=interior/list", (int)LIMIT_RECORD, (int)PAGES_SIZES));
	$pageAll = PagerUtils::PagerPear($count, $page, LIMIT_RECORD, array());
	$smarty->assign("pages_list",			$pageAll["all"]);
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;
	if (file_exists($template)) {
		$smarty->display($template);
	} else {
		$smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>