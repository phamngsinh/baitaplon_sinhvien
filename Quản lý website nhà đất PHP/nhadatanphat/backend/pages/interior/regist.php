<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: regist.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("interior.conf");
		
		$categoriesDao = new CatInteriorDao();
		$categories_list = $categoriesDao->get_categories_list();

		$smarty->assign("cat_list", $categories_list);
		
		$interiorid  = isset($_REQUEST["interiorid"]) ? trim($_REQUEST["interiorid"])	: null;
		$page	  	 = isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: 1;
		$issubmit 	 = isset($_POST["issubmit"]) ? 		trim($_POST["issubmit"]) 		: null;
		$message  	 = null;
		$interiorDao = new InteriorDao();
		$interiorData= array();
		
		if (is_null($issubmit)){ // Is load default
			if ($interiorid) $interiorData = $interiorDao->getInteriorById($interiorid);
			
		} else if ($issubmit){ // Is edit data
			$interiorData 	= isset($_POST["object"]) ? $_POST["object"]: null;
			
			if ($interiorDao->checkExistedInteriorName($interiorData["interior_name"], $interiorid))
				$message = INTERIOR_NAME_EXISTED;
			
			$filesize 		= FileUtils::returnMB(MAX_UPLOAD_FILE_IMG*1024);
			$fileupload 	= new FileUpload($FILE);
		 	if ($fileupload->getName() == "") $interior_image = $interiorData["interior_image"];
			else $interior_image	= "interior_".date("YmdHsi").$fileupload->getExtFile();

		 	$validate = new validate("interior_validate", INTERIOR_VALIDATION_FILE);
			$message .= $validate->execute($interiorData["interior_name"], $interior_image, $interiorData["interior_description"]);

			if ($message == "" && $fileupload->getName() != "") {
				if (!FileUtils::checkFileType($fileupload->getName(), IMG_FORMAT))
					$message .= INTERIOR_IMAGE_ERROR.BR_TAG;
				else if (FileUtils::returnMB($fileupload->getSize())> FileUtils::getUploadMaxFilesizeOfSite($filesize))
					$message .= INTERIOR_IMAGE_SIZE.(FileUtils::getUploadMaxFilesizeOfSite($filesize)*1024)." KB.".BR_TAG;	
			}

			if ($message == ""){
				$interiorDao->beginTrans();
				try {
					$objects = $interiorData;
					$objects["interior_image"] 	= $interior_image;
					if (!is_null($interiorid) && $interiorid != "") { // Is update data
						if ($fileupload->getName() == "") $interiorDao->update_interior($objects, $interiorid);
						else{
							$fileupload->uploaded_file(IMG_PATH.DS."interior".DS, $interior_image);
							Utils::createThumbnailsImage(IMG_PATH.DS."interior".DS.$interior_image, IMG_PATH.DS."interior".DS."thumb_".$interior_image, THUMB_IMG_SIZE, $fileupload->getExtFile());
							
							$interiorDao->update_interior($objects, $interiorid);
							FileUtils::deleteFile(IMG_PATH.DS."interior".DS, $interiorData["interior_image"]);
							FileUtils::deleteFile(IMG_PATH.DS."interior".DS, "thumb_".$interiorData["interior_image"]);
						}
					} else { // Is regist date
						if ($fileupload->getName() == ""){
							$interiorDao->insert_interior($objects);
						}else{
							$fileupload->uploaded_file(IMG_PATH.DS."interior".DS, $interior_image);
							Utils::createThumbnailsImage(IMG_PATH.DS."interior".DS.$interior_image, IMG_PATH.DS."interior".DS."thumb_".$interior_image, THUMB_IMG_SIZE, $fileupload->getExtFile());
							$objects["created_date"] 	= date(DATE_PHP);
							$interiorDao->insert_interior($objects);
						}
					}
					$interiorDao->commitTrans();
					header("Location: ?hdl=interior/list&page=$page");
				} catch (Exception $ex) {
					$interiorDao->rollbackTrans();
					if (!is_null($interiorid) && $interiorid != "") $message .= MESSAGE_UPDATE_FALSE.BR_TAG;
					else $message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
		}
		
		$smarty->assign("interiorData",   	$interiorData);
		$smarty->assign("statusList",  		Status::getList($textlang));
		$smarty->assign("interiorid",   	$interiorid);
		$smarty->assign("page",   			$page);
		$smarty->assign("message", 			$message);
		
		$interiorDao->doClose();
		
		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>