<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: regist.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("support.conf");
		
		$supportid   = isset($_REQUEST["supportid"]) ? trim($_REQUEST["supportid"])	: null;
		$page	  	 = isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: 1;
		$issubmit 	 = isset($_POST["issubmit"]) ? 		trim($_POST["issubmit"]) 		: null;
		$message  	 = null;
		$supportDao  = new SupportDao();
		$supportData = array();
		
		if (is_null($issubmit)) { // Is load default
			if ($supportid) $supportData = $supportDao->getSupportById($supportid);
		} else if ($issubmit) { // Is edit data
			$supportData 	= isset($_POST["object"]) ? $_POST["object"]: null;

		 	$validate = new validate("support_validate", SUPPORT_VALIDATION_FILE);
			$message .= $validate->execute($supportData["support_account"], $supportData["support_title"]);
			if ($message == "" && $supportDao->checkExistedSupportAccount($supportData["support_account"], $supportid)) $message = SUPPORT_ACCOUNT_EXISTED;

			if ($message == "") {
				$supportDao->beginTrans();
				try {
					$objects = $supportData;
					$objects["created_date"] 	= date(DATE_PHP);
					if (!is_null($supportid) && $supportid != "") { // Is update data
						$supportDao->update_support($objects, $supportid);
					} else { // Is regist date
						$supportDao->insert_support($objects);
					}
					$supportDao->commitTrans();
					header("Location: ?hdl=support/list&page=$page");
				} catch (Exception $ex) {
					$supportDao->rollbackTrans();
					if (!is_null($supportid) && $supportid != "") $message .= MESSAGE_UPDATE_FALSE.BR_TAG;
					else $message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
		}

		$smarty->assign("supportData",  $supportData);
		$smarty->assign("genreList",  	SupportGenre::getList($textlang));
		$smarty->assign("supportid",   	$supportid);
		$smarty->assign("page",   		$page);
		$smarty->assign("message", 		$message);

		$supportDao->doClose();

		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>