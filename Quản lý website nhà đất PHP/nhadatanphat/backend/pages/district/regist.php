<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: regist.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("district.conf");
		
		$districtid  = isset($_REQUEST["districtid"]) ? trim($_REQUEST["districtid"])	: null;
		$page	  	 = isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: 1;
		$issubmit 	 = isset($_POST["issubmit"]) ? 		trim($_POST["issubmit"]) 		: null;
		$message  	 = null;
		$districtDao = new DistrictDao();
		$provinceDao = new ProvinceDao();
		$districtData= array();
		
		if (is_null($issubmit)) { // Is load default
			if ($districtid) $districtData = $districtDao->getDistrictById($districtid);
		} else if ($issubmit) { // Is edit data
			$districtData 	= isset($_POST["object"]) ? $_POST["object"]: null;

		 	$validate = new validate("district_validate", DISTRICT_VALIDATION_FILE);	 	
			$message .= $validate->execute($districtData["district_name"]);

			if ($message == "" && $districtDao->checkExisteddistrictName($districtData["district_name"], $districtid)) $message = DISTRICT_NAME_EXISTED;

			if ($message == "") {
				$districtDao->beginTrans();
				try {
					$objects = $districtData;
					$objects["created_date"] 	= date(DATE_PHP);
					if (!is_null($districtid) && $districtid != "") { // Is update data
						$districtDao->update_district($objects, $districtid);
					} else { // Is regist date
						$districtDao->insert_district($objects);
					}
					$districtDao->commitTrans();
					header("Location: ?hdl=district/list&page=$page");
				} catch (Exception $ex) {
					$districtDao->rollbackTrans();
					if (!is_null($districtid) && $districtid != "") $message .= MESSAGE_UPDATE_FALSE.BR_TAG;
					else $message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
		}

		$smarty->assign("districtData",   	$districtData);
		$smarty->assign("provinceList",   	$provinceDao->getAllProvinces(null, Status::active));
		$smarty->assign("statusList",  		Status::getList($textlang));
		$smarty->assign("districtid",   	$districtid);
		$smarty->assign("page",   			$page);
		$smarty->assign("message", 			$message);
		$districtDao->doClose();
		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>