<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: regist.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("contact.conf");
		
		$contactid  	= isset($_REQUEST["contactid"]) ? 	trim($_REQUEST["contactid"])	: null;
		$page	  		= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: 1;
		$issubmit 		= isset($_POST["issubmit"]) ? 		trim($_POST["issubmit"]) 		: null;
		$message  		= null;
		$contactDao 	= new ContactDao();
		$contactData	= array();
		if (is_null($issubmit)) { // Is load default
			if ($contactid) {
				$contactData = $contactDao->getContactById($contactid);
			}
		} else if ($issubmit) { // Is edit data
			$contactData 	= isset($_POST["object"]) ? $_POST["object"]: null;

			if ($message == "") {
				$contactDao->beginTrans();
				try {
					$objects = $contactData;
					$objects["created_date"] 	= date(DATE_PHP);
					if (!is_null($contactid) && $contactid != "") $contactDao->update_contact($objects, $contactid);
					$contactDao->commitTrans();
					header("Location: ?hdl=contact/list&page=$page");
				} catch (Exception $ex) {
					$contactDao->rollbackTrans();
					if (!is_null($contactid) && $contactid != "") $message .= MESSAGE_UPDATE_FALSE.BR_TAG;
					else $message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
		}

		$smarty->assign("contactData", 	$contactData);
		$smarty->assign("statusList",  	Status::getList($textlang));
		$smarty->assign("contactid",   	$contactid);
		$smarty->assign("page",   		$page);
		$smarty->assign("message", 		$message);
		$contactDao->doClose();
		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>