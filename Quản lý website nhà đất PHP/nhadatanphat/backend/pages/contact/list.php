<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: list.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("contact.conf");
		
		$page	 	= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: null;
		$contactid	= isset($_REQUEST["contactid"]) ? 	trim($_REQUEST["contactid"])	: null;
		$status	 	= isset($_POST["status"]) ? 		trim($_POST["status"]) 			: null;
		$delete	 	= isset($_POST["delete_"]) ? 		trim($_POST["delete_"]) 		: null;
		$startdate	= new DateTimes("startdate", 		START_DATE, END_DATE, DATE_PHP, FALSE, "dropdown");
		$enddate	= new DateTimes("enddate", 			START_DATE, END_DATE, DATE_PHP, FALSE, "dropdown");
		$enddate->setEmpty(NULL, "");

		if (is_null($status) && !strlen($page) && isset($_SESSION["SEARCH_CONTACT"])) $_SESSION["SEARCH_CONTACT"] = null;
		if (!empty($status)) {
			$_SESSION["SEARCH_CONTACT"]["startdate"]		= $startdate->getDateTime();
			$_SESSION["SEARCH_CONTACT"]["enddate"]			= $enddate->getDateTime();
			$_SESSION["SEARCH_CONTACT"]["contactstatus"]	= isset($_REQUEST["contactstatus"]) ? 	trim($_REQUEST["contactstatus"])	: null;
		}
		if (!isset($_SESSION["SEARCH_CONTACT"])) $_SESSION["SEARCH_CONTACT"] = null;
		if (empty($page)) $page = 1;
		if (!empty($_SESSION["SEARCH_CONTACT"]["startdate"]))	$startdate->setSelected($_SESSION["SEARCH_CONTACT"]["startdate"]);
		if (!empty($_SESSION["SEARCH_CONTACT"]["enddate"])) 	$enddate->setSelected($_SESSION["SEARCH_CONTACT"]["enddate"]);

		$contactDao = new ContactDao();
		if (!empty($delete)) {
			$contactDao->beginTrans();
			try {
				$contactDao->delete_contact($contactid);
				$contactDao->commitTrans();
			} catch (Exception $ex) {
				$contactDao->rollbackTrans();
				$smarty->assign("exefalse", MESSAGE_DELETE_FALSE);
			}
		}
		$offset 	   	= (int)($page-1)*LIMIT_RECORD;
		$count		   	= $contactDao->getCountContact($_SESSION["SEARCH_CONTACT"]["startdate"], $_SESSION["SEARCH_CONTACT"]["enddate"], $_SESSION["SEARCH_CONTACT"]["contactstatus"]);
		if ($offset >= $count) {
			$offset		= (int)$offset - (int)LIMIT_RECORD;
			$page		= $page - 1;
		}
		$contactList = $contactDao->getAllContacts($_SESSION["SEARCH_CONTACT"]["startdate"], $_SESSION["SEARCH_CONTACT"]["enddate"], $_SESSION["SEARCH_CONTACT"]["contactstatus"], (int)LIMIT_RECORD, $offset);
		foreach ($contactList as $key => $values) $contactList[$key]["contact_content"] 	= Utils::cutTitle($values["contact_content"]);

		//$smarty->assign("paging",   		PagerUtils::PagerSmarty($page, $count, "?hdl=contact/list", (int)LIMIT_RECORD, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, LIMIT_RECORD, array());
		$smarty->assign("paging",			$pageAll["all"]);
		$smarty->assign("contactList",   	$contactList);
		$smarty->assign("statusList",  		Status::getList($textlang));
		$smarty->assign("startdate",		$startdate->render());
		$smarty->assign("enddate", 			$enddate->render());
		$smarty->assign("count", 			$count);
		$smarty->assign("page", 			$page);
		$smarty->assign("searchData", 		$_SESSION["SEARCH_CONTACT"]);
		$smarty->assign("message", 			MESSAGE_RESULT_NULL);

		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>