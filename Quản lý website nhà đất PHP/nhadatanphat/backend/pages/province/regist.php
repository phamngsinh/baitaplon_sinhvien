<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: regist.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("province.conf");
		
		$provinceid  = isset($_REQUEST["provinceid"]) ? trim($_REQUEST["provinceid"])	: null;
		$page	  	 = isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: 1;
		$issubmit 	 = isset($_POST["issubmit"]) ? 		trim($_POST["issubmit"]) 		: null;
		$message  	 = null;
		$provinceDao = new provinceDao();
		$provinceData= array();
		
		if (is_null($issubmit)) { // Is load default
			if ($provinceid) $provinceData = $provinceDao->getprovinceById($provinceid);
		} else if ($issubmit) { // Is edit data
			$provinceData 	= isset($_POST["object"]) ? $_POST["object"]: null;

		 	$validate = new validate("province_validate", PROVINCE_VALIDATION_FILE);	 	
			$message .= $validate->execute($provinceData["province_name"]);

			if ($message == "" && $provinceDao->checkExistedProvinceName($provinceData["province_name"], $provinceid)) $message = PROVINCE_NAME_EXISTED;

			if ($message == "") {
				$provinceDao->beginTrans();
				try {
					$objects = $provinceData;
					$objects["created_date"] 	= date(DATE_PHP);
					if (!is_null($provinceid) && $provinceid != "") { // Is update data
						$provinceDao->update_province($objects, $provinceid);
					} else { // Is regist date
						$provinceDao->insert_province($objects);
					}
					$provinceDao->commitTrans();
					header("Location: ?hdl=province/list&page=$page");
				} catch (Exception $ex) {
					$provinceDao->rollbackTrans();
					if (!is_null($provinceid) && $provinceid != "") $message .= MESSAGE_UPDATE_FALSE.BR_TAG;
					else $message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
		}

		$smarty->assign("provinceData",   	$provinceData);
		$smarty->assign("provinceid",   	$provinceid);
		$smarty->assign("statusList",  		Status::getList($textlang));
		$smarty->assign("page",   			$page);
		$smarty->assign("message", 			$message);
		$provinceDao->doClose();
		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>