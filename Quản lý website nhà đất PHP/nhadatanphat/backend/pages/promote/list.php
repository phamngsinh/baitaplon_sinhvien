<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: list.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("promote.conf");
		
		$deleteIdArr	= isset($_REQUEST["deleteId"])? $_REQUEST["deleteId"]:array();
		$page	 	= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: null;
		$promoteid	= isset($_REQUEST["promoteid"]) ? 	trim($_REQUEST["promoteid"])	: null;
		$status	 	= isset($_POST["status"]) ? 		trim($_POST["status"]) 			: null;
		$delete	 	= isset($_POST["delete_"]) ? 		trim($_POST["delete_"]) 		: null;

		if (is_null($status) && !strlen($page) && isset($_SESSION["SEARCH_PROMOTE"])) $_SESSION["SEARCH_PROMOTE"] = null;
		if (!empty($status)) {
			$_SESSION["SEARCH_PROMOTE"]["promotename"]	= isset($_REQUEST["promotename"]) ? 	trim($_REQUEST["promotename"])		: null;
			$_SESSION["SEARCH_PROMOTE"]["promotestatus"]= isset($_REQUEST["promotestatus"]) ? 	trim($_REQUEST["promotestatus"])	: null;
		}
		if (!isset($_SESSION["SEARCH_PROMOTE"]))
			$_SESSION["SEARCH_PROMOTE"] = null;
		if (empty($page))
			$page = 1;

		$promoteDao = new PromoteDao();
		if (!empty($delete)){
			$imageList = array();
			for($i = 0; $i < count($deleteIdArr); $i ++){
				$promoteData 	= $promoteDao->getPromoteById($deleteIdArr[$i]);
				$imageList[] = $promoteData["promote_banner"];
			}

			//$promoteDao->beginTrans();
			try {
				$promoteDao->delete_promote($deleteIdArr);
				for($i = 0; $i < count($deleteIdArr); $i ++){
					FileUtils::deleteFile(IMG_PATH.DS."promote".DS, $imageList[$i]);
				}
				//$promoteDao->commitTrans();
			} catch (Exception $ex) {
				//$promoteDao->rollbackTrans();
				$smarty->assign("exefalse", MESSAGE_DELETE_FALSE);
			}
		}

		$offset 	   	= (int)($page-1)*LIMIT_RECORD;

		$count		   	= $promoteDao->getCountPromotes($_SESSION["SEARCH_PROMOTE"]["promotename"], $_SESSION["SEARCH_PROMOTE"]["promotestatus"]);

		if ($offset >= $count){
			$offset		= (int)$offset - (int)LIMIT_RECORD;
			$page		= $page - 1;
		}

		$promoteList = $promoteDao->getAllPromotes($_SESSION["SEARCH_PROMOTE"]["promotename"], $_SESSION["SEARCH_PROMOTE"]["promotestatus"], (int)LIMIT_RECORD, $offset);
		foreach ($promoteList as $key => $values)
			$promoteList[$key]["promote_content"] 	= Utils::cutTitle($values["promote_content"]);

		//$smarty->assign("paging",   		PagerUtils::PagerSmarty($page, $count, "?hdl=promote/list", (int)LIMIT_RECORD, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, LIMIT_RECORD, array());
		$smarty->assign("paging",			$pageAll["all"]);
		$smarty->assign("promoteList",   	$promoteList);
		$smarty->assign("positionList",		Position::getList($textlang));
		$smarty->assign("statusList",  		Status::getList($textlang));
		$smarty->assign("count", 			$count);
		$smarty->assign("page", 			$page);
		$smarty->assign("searchData", 		$_SESSION["SEARCH_PROMOTE"]);
		$smarty->assign("message", 			MESSAGE_RESULT_NULL);

		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>