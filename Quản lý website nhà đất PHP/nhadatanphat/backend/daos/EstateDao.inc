<?php
/**
 * @project_name: localframe
 * @file_name: EstateDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("ESTATE_DAO_INC")) {

	define("ESTATE_DAO_INC",1);
	
	class EstateDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_estate ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " estate_id ";
		}
		
		public function getEstateGroupProvince() {
			$strSQL = " SELECT DISTINCT province_name, estate.province_id, COUNT(estate_id) AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS estate ";
			$strSQL.= " INNER JOIN tbl_member AS member ON member.member_id = estate.member_id ";
			$strSQL.= " INNER JOIN tbl_category AS category ON category.category_id = estate.category_id ";
			$strSQL.= " INNER JOIN tbl_child AS child ON child.child_id = estate.child_id ";
			$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = estate.province_id ";
			$strSQL.= " INNER JOIN tbl_district AS district ON district.district_id = estate.district_id ";
			$strSQL.= " WHERE estate.deleted_date IS NULL AND (estate.estate_duration > now() OR estate.estate_duration IS NULL) ";
			$strSQL.= "	AND estate.status = 1 AND estate.estate_state = 1 AND province.status = 1 AND district.status = 1 ";
			$strSQL.= " GROUP BY province_name, estate.province_id ";
			$strSQL.= " ORDER BY count DESC ";

			$this->prepare($strSQL);
			$result = $this->exec(array());
		 	
		 	return is_array($result) ? $result : array();
		}

		public function getEstateGroupCategory($provinceid = null) {
			$params	= array();
			$strSQL = " SELECT DISTINCT category_name, estate.category_id, COUNT(estate_id) AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS estate ";
			$strSQL.= " INNER JOIN tbl_member AS member ON member.member_id = estate.member_id ";
			$strSQL.= " INNER JOIN tbl_category AS category ON category.category_id = estate.category_id ";
			$strSQL.= " INNER JOIN tbl_child AS child ON child.child_id = estate.child_id ";
			$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = estate.province_id ";
			$strSQL.= " INNER JOIN tbl_district AS district ON district.district_id = estate.district_id ";
			$strSQL.= " WHERE estate.deleted_date IS NULL AND (estate.estate_duration > now() OR estate.estate_duration IS NULL) ";
			$strSQL.= "	AND estate.status = 1 AND province.status = 1 AND district.status = 1 ";
			if (strlen($provinceid)) {
				$strSQL.= " AND estate.province_id = ? ";
				array_push($params, $provinceid);
			}
			$strSQL.= " GROUP BY category_name, estate.category_id ";
			$strSQL.= " ORDER BY count DESC ";

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return is_array($result) ? $result : array();
		}

		public function getAllEstateByParams($paraminput, $status = null, $limit = null, $offset = null) {
			$params = array();
			$strSQL = " SELECT estate.status as estStatus, estate.*, member.*, estate.created_date, category_name, child_name, province_name, district_name ";
			$strSQL.= " FROM ".$this->getTableName()." AS estate ";
			$strSQL.= " INNER JOIN tbl_member AS member ON member.member_id = estate.member_id ";
			$strSQL.= " INNER JOIN tbl_category AS category ON category.category_id = estate.category_id ";
			$strSQL.= " INNER JOIN tbl_child AS child ON child.child_id = estate.child_id ";
			$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = estate.province_id ";
			$strSQL.= " LEFT JOIN tbl_district AS district ON district.district_id = estate.district_id ";
			$strSQL.= " WHERE 1 = 1 ";
			
			if (isset($paraminput["expiredEstate"]) && strlen($paraminput["expiredEstate"])) {
				$strSQL.= " AND  estate.estate_duration < now() ";
			}
			if (isset($paraminput["isFeeEstate"]) && strlen($paraminput["isFeeEstate"])) {
				$strSQL.= " AND  estate.is_fee_estate=1 ";
			}
			if (isset($paraminput["isVideoEstate"]) && strlen($paraminput["isVideoEstate"])) {
				$strSQL.= " AND (estate.estate_video IS NOT NULL AND estate.estate_video <> '')";
			}
			
			if (strlen($status)) {
				$strSQL.= " AND estate.status = ? AND province.status = 1 AND district.status = 1 ";
				array_push($params, $status);
			}
			if (isset($paraminput["title"]) && strlen($paraminput["title"])) {
				$strSQL.= " AND estate.estate_title_ascii LIKE ? ";
				array_push($params, '%'. $paraminput["title"] .'%');
			}
			if (isset($paraminput["address"]) && strlen($paraminput["address"])) {
				$strSQL.= " AND LOWER(estate.estate_address) LIKE ? ";
				array_push($params, '%'.strtolower($paraminput["address"]).'%');
			}
			if (isset($paraminput["categoryid"]) && strlen($paraminput["categoryid"])) {
				$strSQL.= " AND category.category_id = ? ";
				array_push($params, $paraminput["categoryid"]);
			}
			if (isset($paraminput["childid"]) && strlen($paraminput["childid"])) {
				$strSQL.= " AND estate.child_id = ? ";
				array_push($params, $paraminput["childid"]);
			}
			if (isset($paraminput["provinceid"]) && strlen($paraminput["provinceid"])) {
				$strSQL.= " AND estate.province_id = ? ";
				array_push($params, $paraminput["provinceid"]);
			}
			if (isset($paraminput["districtid"]) && strlen($paraminput["districtid"])) {
				$strSQL.= " AND estate.district_id = ? ";
				array_push($params, $paraminput["districtid"]);
			}
			if (isset($paraminput["direction"]) && strlen($paraminput["direction"])) {
				$strSQL.= " AND estate.estate_direction = ? ";
				array_push($params, (int)$paraminput["direction"]);
			}
			if (isset($paraminput["pricestart"]) && strlen($paraminput["pricestart"])) {
				$strSQL.= " AND REPLACE(estate.estate_price, '.', '') >= ? ";
				array_push($params, (int)$paraminput["pricestart"]);
			}
			if (isset($paraminput["priceend"]) && strlen($paraminput["priceend"])) {
				$strSQL.= " AND REPLACE(estate.estate_price, '.', '') <= ? ";
				array_push($params, (int)$paraminput["priceend"]);
			}
			if (isset($paraminput["genre"]) && strlen($paraminput["genre"])) {
				$strSQL.= " AND estate.estate_genre = ? ";
				array_push($params, $paraminput["genre"]);
			}
			if (isset($paraminput["durationstart"]) && strlen($paraminput["durationstart"])) {
				$strSQL.= " AND (estate.estate_duration >= ? OR estate.estate_duration IS NULL) ";
				array_push($params, $paraminput["durationstart"]);
			}
			if (isset($paraminput["durationend"]) && strlen($paraminput["durationend"])) {
				$strSQL.= " AND (estate.estate_duration <= ? OR estate.estate_duration IS NULL) ";
				array_push($params, $paraminput["durationend"]);
			}
			$strSQL.= " ORDER BY estate.created_date DESC ";
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	return is_array($result) ? $result : array();
		}

		public function deleteExpiredRecord(){
			$strSQL = "DELETE FROM tbl_estate where deleted_date < now();";
			$this->prepare($strSQL);
			$result = $this->exec();
		}

		public function getCountEstateByParams($paraminput, $status = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS estate ";
			$strSQL.= " INNER JOIN tbl_member AS member ON member.member_id = estate.member_id ";
			$strSQL.= " INNER JOIN tbl_category AS category ON category.category_id = estate.category_id ";
			$strSQL.= " INNER JOIN tbl_child AS child ON child.child_id = estate.child_id ";
			$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = estate.province_id ";
			$strSQL.= " INNER JOIN tbl_district AS district ON district.district_id = estate.district_id ";
			$strSQL.= " WHERE 1 = 1 ";
			
			if (isset($paraminput["expiredEstate"]) && strlen($paraminput["expiredEstate"])) {
				$strSQL.= " AND  estate.estate_duration < now() ";
			}
			if (isset($paraminput["isFeeEstate"]) && strlen($paraminput["isFeeEstate"])) {
				$strSQL.= " AND  estate.is_fee_estate=1 ";
			}
			if (isset($paraminput["isVideoEstate"]) && strlen($paraminput["isVideoEstate"])) {
				$strSQL.= " AND (estate.estate_video IS NOT NULL AND estate.estate_video = '')";
			}
			if(strlen($status)){
				$strSQL.= " AND estate.status = ? AND province.status = 1 AND district.status = 1 ";
				array_push($params, $status);
			}
			if (isset($paraminput["title"]) && strlen($paraminput["title"])) {
				$strSQL.= " AND estate.estate_title_ascii LIKE ? ";
				array_push($params, '%'. $paraminput["title"] .'%');
			}
			if (isset($paraminput["address"]) && strlen($paraminput["address"])) {
				$strSQL.= " AND LOWER(estate.estate_address) LIKE ? ";
				array_push($params, '%'.strtolower($paraminput["address"]).'%');
			}
			if (isset($paraminput["categoryid"]) && strlen($paraminput["categoryid"])) {
				$strSQL.= " AND category.category_id = ? ";
				array_push($params, $paraminput["categoryid"]);
			}
			if (isset($paraminput["childid"]) && strlen($paraminput["childid"])) {
				$strSQL.= " AND estate.child_id = ? ";
				array_push($params, $paraminput["childid"]);
			}
			if (isset($paraminput["provinceid"]) && strlen($paraminput["provinceid"])) {
				$strSQL.= " AND estate.province_id = ? ";
				array_push($params, $paraminput["provinceid"]);
			}
			if (isset($paraminput["districtid"]) && strlen($paraminput["districtid"])) {
				$strSQL.= " AND estate.district_id = ? ";
				array_push($params, $paraminput["districtid"]);
			}
			if (isset($paraminput["direction"]) && strlen($paraminput["direction"])) {
				$strSQL.= " AND estate.estate_direction = ? ";
				array_push($params, (int)$paraminput["direction"]);
			}
			if (isset($paraminput["pricestart"]) && strlen($paraminput["pricestart"])) {
				$strSQL.= " AND REPLACE(estate.estate_price, '.', '') >= ? ";
				array_push($params, (int)$paraminput["pricestart"]);
			}
			if (isset($paraminput["priceend"]) && strlen($paraminput["priceend"])) {
				$strSQL.= " AND REPLACE(estate.estate_price, '.', '') <= ? ";
				array_push($params, (int)$paraminput["priceend"]);
			}
			if (isset($paraminput["genre"]) && strlen($paraminput["genre"])) {
				$strSQL.= " AND estate.estate_genre = ? ";
				array_push($params, $paraminput["genre"]);
			}
			if (isset($paraminput["durationstart"]) && strlen($paraminput["durationstart"])) {
				$strSQL.= " AND (estate.estate_duration >= ? OR estate.estate_duration IS NULL) ";
				array_push($params, $paraminput["durationstart"]);
			}
			if (isset($paraminput["durationend"]) && strlen($paraminput["durationend"])) {
				$strSQL.= " AND (estate.estate_duration <= ? OR estate.estate_duration IS NULL) ";
				array_push($params, $paraminput["durationend"]);
			}
			
			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_estate
		 * 
		 * @return	Array	: Data of table tbl_estate
		 * @version 1.0
		 */
		public function getEstateById($oid) {
			$params = array();
			$strSQL = " SELECT estate.*, estate.status as estate_status, DATE_FORMAT(estate.estate_duration, '%d-%m-%Y') as duration, DATE_FORMAT(estate.deleted_date, '%d-%m-%Y') as deletedDate, member.*, estate.created_date, category_name, child_name, province_name, district_name ";
			$strSQL.= " FROM ".$this->getTableName()." AS estate ";
			$strSQL.= " INNER JOIN tbl_member AS member ON member.member_id = estate.member_id ";
			$strSQL.= " INNER JOIN tbl_category AS category ON category.category_id = estate.category_id ";
			$strSQL.= " INNER JOIN tbl_child AS child ON child.child_id = estate.child_id ";
			$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = estate.province_id ";
			$strSQL.= " LEFT JOIN tbl_district AS district ON district.district_id = estate.district_id ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return isset($result[0]) ? $result[0] : array();
		}

		/**
		 * @note:	function insert data for table tbl_estate
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function insert_estate($objects) {
			return $this->insert_db($this->getTableName(), $objects);
		}

		/**
		 * @note:	function update data for table tbl_estate
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function update_estate($objects, $oid) {
			return $this->update_db($this->getTableName(), $objects, $this->getKeys()." = ? ", array($oid));
		}
		
		public function getGenreList($estateId, $genreValue, $limit){
			$params = array();

			$strSQL = "
				SELECT estate_id FROM tbl_estate
				WHERE estate_genre=?
				OR estate_id = ?
				ORDER BY created_date DESC LIMIT ?
			";
			array_push($params, $genreValue);
			array_push($params, $estateId);
			array_push($params, $limit);

			$this->prepare($strSQL);
			$result = $this->exec($params);

			return $result;
		}
		
		/**
		 * Update genre, set value not be the emphase estate.
		 */
		public function updateGenreStatus($genreIdList, $genreValue, $updateNum){
			$params = array();
			$strSQL = "
				UPDATE tbl_estate SET estate_genre=?
				WHERE estate_id NOT IN(";
				array_push($params, $genreValue);
				
				foreach($genreIdList as $row){
					$strSQL .= "?, ";
					array_push($params, $row['estate_id']);
				}
				
			$strSQL .= "-1) AND estate_genre=?;";
			array_push($params, $updateNum);
			$this->prepare($strSQL);
			$result = $this->exec($params);
		}

		/**
		 * @note:	function restore data for table tbl_estate
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function restore_estate($oid) {
			return $this->update_db($this->getTableName(), array("deleted_date" =>null), $this->getKeys()." = ? ", array($oid));
		}

		/**
		 * @note:	function delete data for table tbl_estate
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function delete_estate($oidArr) {
			$params = array();
			$strSQL = " DELETE FROM tbl_estate WHERE estate_id IN(";
			$strIdHolder = "";
			foreach($oidArr as $key=>$val){
				$strIdHolder .= "?,";
				array_push($params, $val);
			}
			
			$strIdHolder .= "?";
			array_push($params, -1);
			
			$strSQL .= $strIdHolder . ")";
			
			$this->prepare($strSQL);
			$result = $this->exec($params);
			return $result;
		}
	}// end class		
 }
?>