<?php
/**
 * @project_name: localframe
 * @file_name: InteriorDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("INTERIOR_DAO_INC")) {

	define("INTERIOR_DAO_INC",1);
	
	class InteriorDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_interior ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " interior_id ";
		}
		
		public function getAllInteriors($interiorname = null, $status = null, $limit = null, $offset = null) {
			$params = array();
			$strSQL = " SELECT interior.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS interior ";
			$strSQL.= " WHERE LOWER(interior.interior_name) LIKE ? ";
			array_push($params, '%'.strtolower($interiorname).'%');
			if (strlen($status)) {
				$strSQL.= " AND interior.status = ? ";
				array_push($params, $status);
			}
			$strSQL.= " ORDER BY created_date DESC ";
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		public function getCountInterior($interiorname = null, $status = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS interior ";
			$strSQL.= " WHERE LOWER(interior.interior_name) LIKE ? ";
			array_push($params, '%'.strtolower($interiorname).'%');
			if (strlen($status)) {
				$strSQL.= " AND interior.status = ? ";
				array_push($params, $status);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_interior
		 * 
		 * @return	Array	: Data of table tbl_interior
		 * @version 1.0
		 */
		public function getInteriorById($oid) {
			$params = array();
			$strSQL = " SELECT interior.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS interior ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}

		public function checkExistedInteriorName($interiorname, $oid) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS interior ";
			//$strSQL.= " WHERE interior.interior_name = ? AND ".$this->getKeys()." <> ? ";
			
			$strSQL.= " WHERE interior.interior_name = ? ";
			array_push($params, $interiorname);
			
			if ($oid > 0) {
				$strSQL.= " AND ".$this->getKeys()." <> ? ";
				array_push($params, $oid);
			}
			$this->prepare($strSQL);
			$result = $this->exec($params);
			
		 	return ($result[0]["count"] > 0) ? true : false;
		}
		
		/**
		 * @note:	function insert data for table tbl_interior
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function insert_interior($objects) {
			return $this->insert_db($this->getTableName(), $objects);
		}

		/**
		 * @note:	function update data for table tbl_interior
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function update_interior($objects, $oid) {
			return $this->update_db($this->getTableName(), $objects, $this->getKeys()." = ? ", array($oid));
		}

		/**
		 * @note:	function delete data for table tbl_interior
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function delete_interior($oid) {
			//$this->delete_db(" tbl_district ", $this->getKeys()." = ? ", array($oid));
			return $this->delete_db($this->getTableName(), $this->getKeys()." = ? ", array($oid));
		}
	}// end class		
 }
?>