<?php
/**
 * @project_name: localframe
 * @file_name: CompanyDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("COMPANY_DAO_INC")) {

	define("COMPANY_DAO_INC",1);
	
	class CompanyDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_company ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " company_id ";
		}
		
		public function getAllCompanies($companyname = null, $status = null, $limit = null, $offset = null) {
			$params = array();
			$strSQL = " SELECT company.*, province_name ";
			$strSQL.= " FROM ".$this->getTableName()." AS company ";
			$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = company.province_id ";
			$strSQL.= " WHERE LOWER(company.company_name) LIKE ? ";
			array_push($params, '%'.strtolower($companyname).'%');
			if (strlen($status)) {
				$strSQL.= " AND company.status = ? ";
				array_push($params, $status);
			}
			$strSQL.= " ORDER BY created_date DESC ";
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		public function getCountCompany($companyname = null, $status = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS company ";
			$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = company.province_id ";
			$strSQL.= " WHERE LOWER(company.company_name) LIKE ? ";
			array_push($params, '%'.strtolower($companyname).'%');
			if (strlen($status)) {
				$strSQL.= " AND company.status = ? ";
				array_push($params, $status);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_Company
		 * 
		 * @return	Array	: Data of table tbl_Company
		 * @version 1.0
		 */
		public function getCompanyById($oid) {
			$params = array();
			$strSQL = " SELECT company.*, province_name ";
			$strSQL.= " FROM ".$this->getTableName()." AS company ";
			$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = company.province_id ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}

		public function checkExistedCompanyName($Companyname, $oid) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS company ";
			$strSQL.= " WHERE company.company_name = ? AND ".$this->getKeys()." <> ? ";
			array_push($params, $Companyname, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return intval($result[0]["count"]);
		}
		
		/**
		 * @note:	function insert data for table tbl_company
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function insert_company($objects) {
			return $this->insert_db($this->getTableName(), $objects);
		}

		/**
		 * @note:	function update data for table tbl_company
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function update_company($objects, $oid) {
			return $this->update_db($this->getTableName(), $objects, $this->getKeys()." = ? ", array($oid));
		}

		/**
		 * @note:	function delete data for table tbl_company
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function delete_company($oid) {
			return $this->delete_db($this->getTableName(), $this->getKeys()." = ? ", array($oid));
		}
	}// end class		
 }
?>