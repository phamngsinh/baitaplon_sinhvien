<?php
/**
 * @project_name: localframe
 * @file_name: KeywordDao.inc
 * @descr:
 * 
 * @author	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 **/ 
 if (!defined("KEYWORD_DAO_INC")) {

	define("KEYWORD_DAO_INC",1);
	
	class KeywordDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_keyword ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " keyword_id ";
		}
		
		public function getAllKeywords($start = null, $end = null, $status = null, $limit = null, $offset = null) {
			$params = array();
			$strSQL = " SELECT keyword.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS keyword ";
			$strSQL.= " WHERE 1 = 1 ";
			if (strlen($start)) {
				$strSQL.= " AND REPLACE(keyword.keyword_start, '.', '') >= ? ";
				array_push($params, (int)$start);
			}
			if (strlen($end)) {
				$strSQL.= " AND REPLACE(keyword.keyword_end, '.', '') <= ? ";
				array_push($params, (int)$end);
			}
			if (strlen($status)) {
				$strSQL.= " AND keyword.status = ? ";
				array_push($params, $status);
			}
			$strSQL.= " ORDER BY keyword.keyword_id ";
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		public function getCountKeyword($start = null, $end = null, $status = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS keyword ";
			$strSQL.= " WHERE 1 = 1 ";
			if (strlen($start)) {
				$strSQL.= " AND REPLACE(keyword.keyword_start, '.', '') >= ? ";
				array_push($params, (int)$start);
			}
			if (strlen($end)) {
				$strSQL.= " AND REPLACE(keyword.keyword_end, '.', '') <= ? ";
				array_push($params, (int)$end);
			}
			if (strlen($status)) {
				$strSQL.= " AND keyword.status = ? ";
				array_push($params, $status);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_category
		 * 
		 * @return	Array	: Data of table tbl_category
		 * @version 1.0
		 */
		public function getKeywordById($oid) {
			$params = array();
			$strSQL = " SELECT keyword.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS keyword ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}

		/**
		 * @note:	function insert data for table tbl_keyword
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function insert_keyword($objects) {
			return $this->insert_db($this->getTableName(), $objects);
		}

		/**
		 * @note:	function update data for table tbl_keyword
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function update_keyword($objects, $oid) {
			return $this->update_db($this->getTableName(), $objects, $this->getKeys()." = ? ", array($oid));
		}

		/**
		 * @note:	function delete data for table tbl_keyword
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function delete_keyword($oid) {
			return $this->delete_db($this->getTableName(), $this->getKeys()." = ? ", array($oid));
		}
	}// end class		
 }
?>