<?php
/**
 * @project_name: localframe
 * @file_name: AccountDao.inc
 * @descr:
 * 
 * @author	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 **/ 
 if (!defined("ACCOUNT_DAO_INC")) {

	define("ACCOUNT_DAO_INC",1);
	
	class AccountDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_account ";	
		}

		public function getAccountDataByAccountNameAndAccountPass($accountname, $accountpass) {
			$param   = array();
			$strSQL  = " SELECT * FROM ".$this->getTableName()." WHERE status = 1 ";
			$strSQL .= " AND account_name = ? AND account_pass = ? ";
			array_push($param, $accountname, $accountpass);

			$this->prepare($strSQL);
			$result = $this->execRun($param);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}
		
		public function getAllAccounts($limit = null, $offset = null) {
			$param  = array();
			$strSQL = "SELECT * FROM ".$this->getTableName()." ORDER BY account_id DESC ";
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($param, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($param);
		 	
		 	return is_array($result) ? $result : array();
		}

		public function getCountAccount() {
			$param  = array();
			$strSQL = "SELECT COUNT(account_id) AS count FROM ".$this->getTableName()." ";

			$this->prepare($strSQL);
			$result = $this->exec($param);
		 	
		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_account
		 * 
		 * @return	Array	: Data of table tbl_account
		 * @version 1.0
		 */
		public function getAccountById($accountid) {
			$param   = array();
			$strSQL  = " SELECT * FROM ".$this->getTableName()." ";
			$strSQL .= " WHERE account_id = ? ";
			array_push($param, $accountid);

			$this->prepare($strSQL);
			$result = $this->exec($param);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}

		public function checkExistedAccountName($accountname, $accountid) {
			$param   = array();
			$strSQL  = " SELECT COUNT(account_id) AS count FROM ".$this->getTableName()."  ";
			$strSQL .= " WHERE account_name = ? AND account_id <> ? ";
			array_push($param, $accountname, $accountid);

			$this->prepare($strSQL);
			$result = $this->exec($param);
		 	
		 	return intval($result[0]["count"]);
		}
		
		/**
		 * @note:	function insert data for table tbl_account
		 * @param	Object: accountobj
		 * @version 1.0
		 */
		public function insert_account($accountobj) {
			return $this->insert_db($this->getTableName(), $accountobj);
		}

		/**
		 * @note:	function update data for table tbl_account
		 * @param	Object: accountobj
		 * @version 1.0
		 */
		public function update_account($accountobj, $accountid) {
			return $this->update_db($this->getTableName(), $accountobj, " account_id = ? ", array($accountid));
		}

		/**
		 * @note:	function delete data for table tbl_account
		 * @param	Object: accountobj
		 * @version 1.0
		 */
		public function delete_account($accountid) {
			return $this->delete_db($this->getTableName(), " account_id = ? ", array($accountid));
		}
	}// end class		
 }
?>