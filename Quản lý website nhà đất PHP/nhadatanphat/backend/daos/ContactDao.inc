<?php
/**
 * @project_name: localframe
 * @file_name: contactDao.inc
 * @descr:
 * 
 * @author	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 **/ 
 if (!defined("CONTACT_DAO_INC")) {

	define("CONTACT_DAO_INC",1);
	
	class ContactDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_contact ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " contact_id ";
		}
		
		public function getAllContacts($startdate = null, $enddate = null, $status = null, $limit = null, $offset = null) {
			$params = array();
			$strSQL = " SELECT contact.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS contact ";
			$strSQL.= " WHERE 1 = 1 ";
			if (strlen($startdate)) {
				$strSQL.= " AND DATE_FORMAT(contact.created_date, '".DATE_MYSQL."') >= ? ";
				array_push($params, $startdate);
			}
			if (strlen($enddate)) {
				$strSQL.= " AND DATE_FORMAT(contact.created_date, '".DATE_MYSQL."') <= ? ";
				array_push($params, $enddate);
			}
			if (strlen($status)) {
				$strSQL.= " AND contact.status = ? ";
				array_push($params, $status);
			}
			$strSQL.= " ORDER BY contact.contact_id ";
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		public function getCountContact($startdate = null, $enddate = null, $status = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS contact ";
			$strSQL.= " WHERE 1 = 1 ";
			if (strlen($startdate)) {
				$strSQL.= " AND DATE_FORMAT(contact.created_date, '".DATE_MYSQL."') >= ? ";
				array_push($params, $startdate);
			}
			if (strlen($enddate)) {
				$strSQL.= " AND DATE_FORMAT(contact.created_date, '".DATE_MYSQL."') <= ? ";
				array_push($params, $enddate);
			}
			if (strlen($status)) {
				$strSQL.= " AND contact.status = ? ";
				array_push($params, $status);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_category
		 * 
		 * @return	Array	: Data of table tbl_category
		 * @version 1.0
		 */
		public function getContactById($oid) {
			$params = array();
			$strSQL = " SELECT contact.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS contact ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}

		/**
		 * @note:	function update data for table tbl_contact
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function update_contact($objects, $oid) {
			return $this->update_db($this->getTableName(), $objects, $this->getKeys()." = ? ", array($oid));
		}

		/**
		 * @note:	function delete data for table tbl_contact
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function delete_contact($oid) {
			return $this->delete_db($this->getTableName(), $this->getKeys()." = ? ", array($oid));
		}
	}// end class		
 }
?>