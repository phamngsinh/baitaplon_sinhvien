<?php
/**
 * @project_name: localframe
 * @file_name: NewsDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("NEWS_DAO_INC")) {

	define("NEWS_DAO_INC",1);
	
	class NewsDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_news ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " news_id ";
		}
		
		public function getAllNews($newsname = null, $newgenre = null, $status = null, $limit = null, $offset = null) {
			$params = array();
			$strSQL = " SELECT news.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS news ";
			$strSQL.= " WHERE LOWER(news.news_name) LIKE ? ";
			array_push($params, '%'.strtolower($newsname).'%');
			if (strlen($newgenre)) {
				$strSQL.= " AND news.news_genre = ? ";
				array_push($params, $newgenre);
			}
			if (strlen($status)) {
				$strSQL.= " AND news.status = ? ";
				array_push($params, $status);
			}
			$strSQL.= " ORDER BY news.created_date DESC ";
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return is_array($result) ? $result : array();
		}

		public function getCountNews($newsname = null, $newgenre = null, $status = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS news ";
			$strSQL.= " WHERE LOWER(news.news_name) LIKE ? ";
			array_push($params, '%'.strtolower($newsname).'%');
			if (strlen($newgenre)) {
				$strSQL.= " AND news.news_genre = ? ";
				array_push($params, $newgenre);
			}
			if (strlen($status)) {
				$strSQL.= " AND news.status = ? ";
				array_push($params, $status);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_news
		 * 
		 * @return	Array	: Data of table tbl_news
		 * @version 1.0
		 */
		public function getNewsById($oid) {
			$params = array();
			$strSQL = " SELECT news.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS news ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}

		public function checkExistedNewsName($newsname, $oid) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS news ";
			$strSQL.= " WHERE news.news_name = ? AND ".$this->getKeys()." <> ? ";
			array_push($params, $newsname, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function insert data for table tbl_news
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function insert_news($objects) {
			return $this->insert_db($this->getTableName(), $objects);
		}

		/**
		 * @note:	function update data for table tbl_news
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function update_news($objects, $oid) {
			return $this->update_db($this->getTableName(), $objects, $this->getKeys()." = ? ", array($oid));
		}

		/**
		 * @note:	function delete data for table tbl_news
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function delete_news($oidArr) {
			$params = array();
			$strSQL = " DELETE FROM " . $this->getTableName() . " WHERE " . $this->getKeys() . " IN(";
			$strIdHolder = "";
			foreach($oidArr as $key=>$val){
				$strIdHolder .= "?,";
				array_push($params, $val);
			}
			
			$strIdHolder .= "?";
			array_push($params, -1);
			
			$strSQL .= $strIdHolder . ")";
			
			$this->prepare($strSQL);
			$result = $this->exec($params);
			return $result;
		}
	}// end class		
 }
?>