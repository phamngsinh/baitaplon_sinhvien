<?php
/**
 * @project_name: localframe
 * @file_name: contactDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("MEMBER_DAO_INC")) {

	define("MEMBER_DAO_INC",1);
	
	class MemberDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_member ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " member_id ";
		}
		
		public function getAllMembers($startdate = null, $enddate = null, $status = null, $limit = null, $offset = null, $memberName="") {
			$params = array();
			$strSQL = " SELECT member.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS member ";
			$strSQL.= " WHERE 1 = 1 ";
			
			if (strlen($memberName)) {
				$strSQL.= " AND (member.member_name LIKE ? OR member.member_user LIKE ?)";
				array_push($params, "%" . $memberName . "%");
				array_push($params, "%" . $memberName . "%");
			}
			
			if (strlen($startdate)) {
				$strSQL.= " AND DATE_FORMAT(member.created_date, '".DATE_MYSQL."') >= ? ";
				array_push($params, $startdate);
			}
			if (strlen($enddate)) {
				$strSQL.= " AND DATE_FORMAT(member.created_date, '".DATE_MYSQL."') <= ? ";
				array_push($params, $enddate);
			}
			if (strlen($status)) {
				$strSQL.= " AND member.status = ? ";
				array_push($params, $status);
			}
			$strSQL.= " ORDER BY member.member_id ";
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		public function getCountMember($startdate = null, $enddate = null, $status = null, $memberName="") {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS member ";
			$strSQL.= " WHERE 1 = 1 ";
			
			if (strlen($memberName)) {
				$strSQL.= " AND (member.member_name LIKE ? OR member.member_user LIKE ?)";
				array_push($params, "%" . $memberName . "%");
				array_push($params, "%" . $memberName . "%");
			}
			
			if (strlen($startdate)) {
				$strSQL.= " AND DATE_FORMAT(member.created_date, '".DATE_MYSQL."') >= ? ";
				array_push($params, $startdate);
			}
			if (strlen($enddate)) {
				$strSQL.= " AND DATE_FORMAT(member.created_date, '".DATE_MYSQL."') <= ? ";
				array_push($params, $enddate);
			}
			if (strlen($status)) {
				$strSQL.= " AND member.status = ? ";
				array_push($params, $status);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_category
		 * 
		 * @return	Array	: Data of table tbl_category
		 * @version 1.0
		 */
		public function getMemberById($oid) {
			$params = array();
			$strSQL = " SELECT member.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS member ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}

		/**
		 * @note:	function update data for table tbl_member
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function update_member($objects, $oid) {
			return $this->update_db($this->getTableName(), $objects, $this->getKeys()." = ? ", array($oid));
		}

		/**
		 * @note:	function delete data for table tbl_member
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function delete_member($oid) {
			return $this->delete_db($this->getTableName(), $this->getKeys()." = ? ", array($oid));
		}
	}// end class		
 }
?>