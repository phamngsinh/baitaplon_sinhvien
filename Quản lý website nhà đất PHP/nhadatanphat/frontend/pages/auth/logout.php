<?php
	/**
	 * @note:	file logout.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	defined('DS') or die("Errors System");
	$logined  = isset($_REQUEST["logined"]) ?  trim($_REQUEST["logined"])  : null;
	
	if (!empty($logined) && $logined == "FALSE") { /*************Click logouted*************/
		$_SESSION["_LOGIN_ESTATE_"] 	= null;
	}
?>