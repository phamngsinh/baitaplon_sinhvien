<?php
	/**
	 * @note:	file default.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	defined('DS') or die("Errors System");
	$template = FRONTEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$cateid   	= isset($_REQUEST["cateid"]) ?		$_REQUEST["cateid"] 	: null;
		//$province  	= isset($_REQUEST["province"]) ?	$_REQUEST["province"] 	: null;
		$district  	= isset($_REQUEST["district"]) ?	$_REQUEST["district"] 	: null;
		$direction 	= isset($_REQUEST["direction"]) ?	$_REQUEST["direction"] 	: null;
		$price  	= isset($_REQUEST["price"]) ?		$_REQUEST["price"] 		: null;
		$estateid  	= isset($_REQUEST["estateid"]) ?	$_REQUEST["estateid"]	: null;
		$s  	= isset($_REQUEST["s"]) ?	$_REQUEST["s"]	: null;
		$page	 	= isset($_GET["page"]) ? 			trim($_GET["page"])		: 1;
		$genre 			= isset($_GET["genre"]) ?	$_GET["genre"] 	: "2";
		$EstateDao 	= new EstateDao();
		$ChildDao	= new ChildDao();
		$categories = new Categories();
		
		$EstateDao		= new EstateDao();
		$sqlparam	= array("isVipGroup"=>1);
		$sqlparam['genreSearch']=1;
		$pageSize =70;
		
		
		
		
		$EstateGenreList1 = $EstateDao->getAllEstateByParams(array("genre"=>$genre, "state"=>1, "feeEstateOnly"=>1), Status::active, 4, 0);
		$smarty->assign("EstateGenreList", 	$EstateGenreList1);
//		print_r($EstateGenreList1);die('xxxx');
		
//		print_r($EstateHot);die('xxx');

		$estate			= $EstateDao->getEstateById($estateid);
		$EstateDao->updateViewCounter($estate["viewed"]+1, $estateid);
		$categoryNames 	= $categories->getCategoryName($ChildDao, $estate["child_id"]);
		$categoryNames 	= array_values($categoryNames);
		for ($i = count($categoryNames); $i > 0 ; $i--) $categoryName .= " >> ".$categoryNames[$i-1];
		$smarty->assign("categoryName",	$categoryName);

		$smarty->assign("Direction",	Direction::getList($textlang));
		$smarty->assign("estate",		$estate);
		$smarty->assign("page_title",		$estate['estate_title']);
		
		$smarty->assign("cateid",		isset($estate["category_id"]) ? $estate["category_id"] : 0);

		$params = null;
		if (strlen($cateid))
			$params .= "&cateid=$cateid";

		$prices	 	= split('[-]', $price);
		$pricestart	= $prices[0];
		$priceend	= isset($prices[1]) ? $prices[1] : null;

		$sqlparam	= array(/*"currentNewsDate"=>$estate['estate_last_updated'], */"isNewNews"=>1, "oid"=>$estateid, "categoryid"=>$cateid, "provinceid"=>$estate['province_id']);

		$offset 	= (int)($page-1)*ESTATE_RECORD;
		$count		= $EstateDao->getCountEstateByParams($sqlparam, Status::active);
		if ($offset >= $count) {
			$offset	= (int)$offset - (int)ESTATE_RECORD;
			$page	= $page - 1;
		}

		$EstateList	= $EstateDao->getAllEstateByParams($sqlparam, Status::active, (int)ESTATE_RECORD, (int)$offset);
//		print_r($EstateList);die('xx');
		$EstateListNewNews = array();
		
		$todayDate = mktime(0, 0, 0, date("d"), date("m"), date("Y"));
		
		$size = count($EstateList);
		for($i = 0; $i < $size; $i ++){
			 $row = $EstateList[$i];
			$estateDate = explode("-", $row["estate_created_date"]);
			$estateDate = mktime(0, 0, 0, $estateDate[2], $estateDate[1], $estateDate[0]);
			if($todayDate == $estateDate){
				$EstateListNewNews[] = $row;
				unset($EstateList[$i]);
			}
		}
		$n=count($EstateListNewNews);
		/********************************************************************/
		$limits=(int)ESTATE_RECORD+$n;
		$EstateList1	= $EstateDao->getAllEstateByParams($sqlparam, Status::active, $limits, (int)$offset);
		$EstateListNews = array();
		
		$todayDate1 = mktime(0, 0, 0, date("d"), date("m"), date("Y"));
		
		$size = count($EstateList1);
		for($i = 0; $i < $size; $i ++){
			 $row = $EstateList1[$i];
			$estateDate1 = explode("-", $row["estate_created_date"]);
			$estateDate1 = mktime(0, 0, 0, $estateDate1[2], $estateDate1[1], $estateDate1[0]);
			if($todayDate1 != $estateDate1){
				$EstateListNews[] = $row;
				unset($EstateList[$i]);
			}
		}
		
		/********************************************************************/
//		$province = $estate['province_id'];
		$provinceTmp = -1;//toan quoc.
		if(isset($province)  && strlen($province) > 0){
			$provinceTmp = $province;
		}
		
		
		$PositionSix 	= $PromoteDao->getAllPromotes($province, Status::active, Position::positionsix, 2, 0);
//		print_r($PositionSix); die('x');
		$PositionFou 	= $PromoteDao->getAllPromotes($province, Status::active, Position::positionfour, 0, 0);
		$PositionThr 	= $PromoteDao->getAllPromotes($province, Status::active, Position::positionthree, 0, 0);
		$PositionTC 	= $PromoteDao->getAllPromotes($province, Status::active, Position::positionone, 1, 0);
		$PositionTR 	= $PromoteDao->getAllPromotes($province, Status::active, Position::positiontwo, 1, 0);

//		if ($s) {
//			$relaObj['estate_id'] = $estateid;
//			$relaObj['member_id'] = $_SESSION["_LOGIN_ESTATE_"]["member_id"];
//			$EstateDao->save_news($relaObj);		
//		}

		//$s_status = $EstateDao->check_history($estateid);
		//$smarty->assign("paging",   	PagerUtils::PagerSmarty($page, $count, "?hdl=detail$params&estateid=$estateid", (int)ESTATE_RECORD, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, ESTATE_RECORD, array());
		$smarty->assign("paging",		$pageAll["all"]);
		$smarty->assign("EstateList",   $EstateList);
		
		if($count > (int)($page-1)*ESTATE_RECORD){
			$smarty->assign("showPagingCount",   $count - ((int)($page-1)*ESTATE_RECORD));
		}
		
		$smarty->assign("EstateListCountCurrent", count($EstateListNews));
		$smarty->assign("EstateListNewNews",   $EstateListNewNews);
		$smarty->assign("EstateListNews",   $EstateListNews);
		$smarty->assign("EstateListNewNewsCount",   count($EstateListNewNews));
		$smarty->assign("PositionSix",	$PositionSix);
		$smarty->assign("Position4", 	$PositionFou);
		$smarty->assign("Position3", 	$PositionThr);
		$smarty->assign("PositionTopCenter", $PositionTC[0]);
		$smarty->assign("PositionTopRight", $PositionTR[0]);
		$smarty->assign("page",			$page);
		$smarty->assign("cateid",		$cateid);
		$smarty->assign("estateid",		$estateid);
		$smarty->assign("params",		$params);
		//$smarty->assign("s_status",		$s_status);

		if(file_exists(LOGO_PATH.$estate['member_id'].".jpg")){
			$memberLogo = $estate['member_id'].".jpg";
			$smarty->assign("memberLogo",		$memberLogo);
		}

		return $smarty->display($template);
	}
?>