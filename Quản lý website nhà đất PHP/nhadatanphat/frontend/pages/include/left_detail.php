<?php
	/**
	 * @note:	file left.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	if ($mod != "member"){
		$kind   	= isset($_REQUEST["kind"]) ?		$_REQUEST["kind"] 		: null;
		$cate   	= isset($_REQUEST["cate"]) ?		$_REQUEST["cate"] 		: null;
		$genre  	= isset($_REQUEST["genre"]) ?		$_REQUEST["genre"] 		: null;
		$province  	= isset($_REQUEST["province"]) ?	$_REQUEST["province"] 	: null;
		$district  	= isset($_REQUEST["district"]) ?	$_REQUEST["district"] 	: null;
		$direction 	= isset($_REQUEST["direction"]) ?	$_REQUEST["direction"] 	: null;
		$price  	= isset($_REQUEST["price"]) ?		$_REQUEST["price"] 		: null;
		
		$EstateDao		= new EstateDao();
		$sqlparam	= array("isVipGroup"=>1);
		$sqlparam['genreSearch']=1;
		$pageSize =70;
		$EstateVIPList	= $EstateDao->getAllEstateByParams($sqlparam, null, /*(int)VIP_ESTATE*/$pageSize, 0, $configObj['usd_vnd'], $configObj['sjc_vnd']);
		$smarty->assign("EstateVIPList", 	$EstateVIPList);
//		print_r($EstateVIPList);die();
		/***************************Get all category estate****************************/
		$CategoryDao	= new CategoryDao();
		$CategoryEstate	= $CategoryDao->getAllCategories(array("kind"=>CateKind::estate, "status"=>Status::active));
		$smarty->assign("CategoryEstate",	$CategoryEstate);
		/******************************************************************************/
		
		/******************************Get all province********************************/
		$ProvinceDao 	= new ProvinceDao();
		$ProvinceList 	= $ProvinceDao->getAllProvinces(Status::active);
		$smarty->assign("ProvinceList",		$ProvinceList);

		unset($ProvinceDao);
		/******************************************************************************/

		/**************************Get district by province****************************/
		$DistrictDao 	= new DistrictDao();
		$DistrictList 	= array();
		if (!empty($province)) $DistrictList = $DistrictDao->getAllDistricts($province, Status::active, null, null);
		$smarty->assign("DistrictList",		$DistrictList);
		unset($DistrictDao);
		/******************************************************************************/
		
		/********************************Get Direction*********************************/
		$smarty->assign("DirectionList",	Direction::getList($textlang));
		/******************************************************************************/
		
		/********************************Get all support*******************************/
		$SupportDao	 	= new SupportDao();
		$SupportList 	= $SupportDao->getAllSupports(null, Status::active);
		$smarty->assign("SupportList", 		$SupportList);
		unset($SupportDao);
		/******************************************************************************/
		
		/********************************Get news left*********************************/
		$InformDao		= new InformDao();
		$CateLeft		= $CategoryDao->getAllCategories(array("vertical"=>Vertical::yes, "status"=>Status::active), 1, 0);
		$cateleftid		= isset($CateLeft[0]["category_id"]) ? $CateLeft[0]["category_id"] : 0;
		$NewsProject	= $InformDao->getAllInforms(/*$cateleftid*/"", Status::active, null, 1, 0);

		$smarty->assign("NewsProject", 		$NewsProject);
		unset($CategoryDao);
		/******************************************************************************/

		/**************************Get estate news group left**************************/
		$province  		= isset($_REQUEST["province"]) ?	$_REQUEST["province"] 	: null;

		$EstateDao		= new EstateDao();
		$childDao		= new ChildDao();
		$GroupEstate 	= $EstateDao->getEstateGroupProvince($province, 6);
		$GroupCategory 	= $EstateDao->getEstateGroupCategory($province);

		$childGroupCategory = $childDao->getChildGroupCategory($province);
		
		$AllGroupCategory 	= $EstateDao->getAllEstateGroupCategory();

		$smarty->assign("GroupEstate",		$GroupEstate);
		$smarty->assign("childGroupCategory",	$childGroupCategory);
		$smarty->assign("childGroupCategoryCount",	count($childGroupCategory));
		$smarty->assign("AllGroupCategory",	$AllGroupCategory);

		$groupCategoryTmp = array();
		foreach($GroupCategory as $row){
			$groupCategoryTmp[$row['category_id']] = array("category_id"=>$row['category_id'], "category_name"=>$row['category_name'], "count"=>$row['count']);
		}
		$smarty->assign("GroupCategory",	$groupCategoryTmp);		

		unset($EstateDao);
		unset($childDao);
		/******************************************************************************/
		
		$catInteriorDao = new CatInteriorDao();
		$catInteriorList = $catInteriorDao->get_categories_list();
		$smarty->assign("catInteriorList",		$catInteriorList);
		unset($catInteriorDao);
		/*******************************Get all keyword********************************/
		$KeywordDao		= new KeywordDao();
		$KeywordList	= $KeywordDao->getAllKeywords(null, null, Status::active);
		$smarty->assign("KeywordList",		$KeywordList);
		/******************************************************************************/
		$include_left_detail	= FRONTEND_TEMPLATE_PATH."include".DS."left_detail".TPL_TYPE;
	}else{
		$include_left_detail	= FRONTEND_TEMPLATE_PATH."member".DS."left_detail".TPL_TYPE;
	}

	$smarty->assign("include_left_detail", 	$include_left_detail);
?>