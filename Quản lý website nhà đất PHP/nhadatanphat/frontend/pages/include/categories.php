<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: categories.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined('DS') or die("Errors System");

	class Categories {
		public $DHTML = array();
		public $NamesCategories = array();

		function ShowCategories($CategoryDao, $categoryid = null, $parentid="0", $insert_text="", $status = null, $languageid = null) {
			$categories = $CategoryDao->getAllCategoryByParentId($categoryid, $parentid, $status, $languageid);

			foreach ($categories as $value) {
				foreach ($value as $key => $items) {
					if ($key != "child_name") $this->DHTML[$value["child_id"]][$key] .= $items;
					else $this->DHTML[$value["child_id"]][$key] .= $insert_text.$items;
				}
				$this->ShowCategories($CategoryDao, $categoryid, $value["child_id"],$insert_text."----", $status = null, $languageid = null);
			}
			return $this->DHTML;
		}

		public function getCategoryName($CategoryDao, $categoryid) {
			$categories = $CategoryDao->getCategoryById($categoryid);
			if (!empty($categories)) {
				$this->NamesCategories[$categoryid] = $categories["child_name"];
				$this->getCategoryName($CategoryDao, $categories["parent_id"]);
			}
			return $this->NamesCategories;
		}

		public function setDataSourceCombox($categoryDao) {
		 	$categoryList 	= $this->ShowCategories($categoryDao);
		 	$result = array(""=>"-----Select-----");
		 	if (sizeof($categoryList) > 0) {
		 		foreach ($categoryList as $values)
		 			$result[$values["child_id"]] = $values["child_name"];
		 	}

		 	return $result;
		}

	}	
?>
