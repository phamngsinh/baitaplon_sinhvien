<?php
	/**
	 * @note:	file order.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	defined('DS') or die("Errors System");
	$template = FRONTEND_TEMPLATE_PATH.$hdl.TPL_TYPE;
	
	if (file_exists($template)) {
		$message  		= null;
		$ContactDao 	= new ContactDao();

		$sendcontact 	= isset($_POST["sendcontact"]) ? $_POST["sendcontact"] : null;
		if (!empty($sendcontact)) {
		 	$object   = isset($_POST["object"]) ? $_POST["object"]: null;
		 	$validate = new validate("contact_validate", CONTACT_VALIDATION_FILE);	 	
			$message .= $validate->execute($object["contact_name"], $object["contact_mail"], $object["contact_title"], $object["contact_content"]);
			if ($message == "") {
				$ContactDao->beginTrans();
				try {
					$objects = $object;
					$objects["created_date"]= date(DATE_PHP);
					$ContactDao->insert_contact($objects);
					$ContactDao->commitTrans();
					$object = null;
					$smarty->assign("sendsuccess", 	CONTACT_SUCCESS);
				} catch (Exception $ex) {
					$ContactDao->rollbackTrans();
					$message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
			$smarty->assign("object", 	$object);
			$smarty->assign("message", 	$message);
		}

		$ContactDao->doClose();
		
		return $smarty->display($template);
	}
?>