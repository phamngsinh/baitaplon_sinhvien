<?php
	/**
	 * @note:	file province.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	$promoteid 		= isset($_REQUEST["promoteid"]) ?	$_REQUEST["promoteid"] : null;
	$PromoteDao 	= new PromoteDao();
	$PromoteDetail	= $PromoteDao->getPromoteById($promoteid);
	$PromoteDao->ClickedPromote($promoteid);
	unset($PromoteDao);
	if (isset($PromoteDetail["promote_website"])) header("Location: ".$PromoteDetail["promote_website"]);
?>