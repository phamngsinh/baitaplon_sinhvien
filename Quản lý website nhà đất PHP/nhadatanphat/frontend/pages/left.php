<?php
	/**
	 * @note:	file left.php
	 * 
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	if ($mod != "member") {
		$kind   	= isset($_REQUEST["kind"]) ?		$_REQUEST["kind"] 		: null;
		$cate   	= isset($_REQUEST["cate"]) ?		$_REQUEST["cate"] 		: null;
		$genre  	= isset($_REQUEST["genre"]) ?		$_REQUEST["genre"] 		: null;
		$province  	= isset($_REQUEST["province"]) ?	$_REQUEST["province"] 	: null;
		$district  	= isset($_REQUEST["district"]) ?	$_REQUEST["district"] 	: null;
		$direction 	= isset($_REQUEST["direction"]) ?	$_REQUEST["direction"] 	: null;
		$price  	= isset($_REQUEST["price"]) ?		$_REQUEST["price"] 		: null;

		/***************************Get all category estate****************************/
		$CategoryDao	= new CategoryDao();
		$CategoryEstate	= $CategoryDao->getAllCategories(array("kind"=>CateKind::estate, "status"=>Status::active));
		$smarty->assign("CategoryEstate",	$CategoryEstate);
		/******************************************************************************/
		
		/******************************Get all province********************************/
		$ProvinceDao 	= new ProvinceDao();
		$ProvinceList 	= $ProvinceDao->getAllProvinces(Status::active);
		$smarty->assign("ProvinceList",		$ProvinceList);
		unset($ProvinceDao);
		/******************************************************************************/
		
		/**************************Get district by province****************************/
		$DistrictDao 	= new DistrictDao();
		$DistrictList 	= array();
		if (!empty($province)) $DistrictList = $DistrictDao->getAllDistricts($province, Status::active, null, null);
		$smarty->assign("DistrictList",		$DistrictList);
		unset($DistrictDao);
		/******************************************************************************/
		
		/********************************Get Direction*********************************/
		$smarty->assign("DirectionList",	Direction::getList($textlang));
		/******************************************************************************/
		
		/********************************Get all support*******************************/
		$SupportDao	 	= new SupportDao();
		$SupportList 	= $SupportDao->getAllSupports(null, Status::active);
		$smarty->assign("SupportList", 		$SupportList);
		unset($SupportDao);
		/******************************************************************************/
		
		/********************************Get news left*********************************/
		$InformDao		= new InformDao();
		$CateLeft		= $CategoryDao->getAllCategories(array("vertical"=>Vertical::yes, "status"=>Status::active), 1, 0);
		$cateleftid		= isset($CateLeft[0]["category_id"]) ? $CateLeft[0]["category_id"] : 0;
		$NewsProject	= $InformDao->getAllInforms($cateleftid, Status::active, null, 1, 0);
		$smarty->assign("NewsProject", 		isset($NewsProject[0]) ? $NewsProject[0] : null);
		unset($CategoryDao);
		/******************************************************************************/

		/**************************Get estate news group left**************************/
		$province  		= isset($_REQUEST["province"]) ?	$_REQUEST["province"] 	: null;
		$EstateDao		= new EstateDao();
		$GroupEstate 	= $EstateDao->getEstateGroupProvince();
		$GroupCategory 	= $EstateDao->getEstateGroupCategory($province);
		$smarty->assign("GroupEstate",		$GroupEstate);
		$smarty->assign("GroupCategory",	$GroupCategory);
		unset($EstateDao);
		/******************************************************************************/
		/******************************************************************************/
//		$EstateDao		= new EstateDao();
//		$EstateHot = $EstateDao->getAllEstateByParams(array("genre"=>3, "state"=>1), Status::active, 20, 0);
//		$smarty->assign("$EstateHot", 	$EstateHot);
//		print_r($EstateHot);die('xxx');
		/******************************************************************************/
		$catInteriorDao = new CatInteriorDao();
		$catInteriorList = $catInteriorDao->get_categories_list();
		$smarty->assign("catInteriorList",		$catInteriorList);
		
		/*******************************Get all keyword********************************/
		$KeywordDao		= new KeywordDao();
		$KeywordList	= $KeywordDao->getAllKeywords(null, null, Status::active);
		$smarty->assign("KeywordList",		$KeywordList);
		/******************************************************************************/
		
		$include_left	= FRONTEND_TEMPLATE_PATH."include".DS."left".TPL_TYPE;
	} else {
		$include_left	= FRONTEND_TEMPLATE_PATH."member".DS."left".TPL_TYPE;
	}

	$smarty->assign("include_left", 	$include_left);
?>