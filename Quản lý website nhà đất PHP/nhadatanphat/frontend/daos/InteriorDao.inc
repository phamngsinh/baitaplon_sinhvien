<?php
/**
 * @project_name: localframe
 * @file_name: InteriorDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("INTERIOR_DAO_INC")) {

	define("INTERIOR_DAO_INC",1);
	
	class InteriorDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_interior ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " interior_id ";
		}
		
		public function getAllInteriors($status = null, $cat_interior_id = '', $limit = null, $offset = null, $isNeedRandom=0) {
			$params = array();
			$strSQL = " SELECT interior.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS interior ";
			$strSQL.= " WHERE 1 = 1 ";
			
			if (strlen($status)) {
				$strSQL.= " AND interior.status = ? ";
				array_push($params, $status);
			}
			
			if ($cat_interior_id > 0) {
				$strSQL.= " AND interior.category_id = ? ";
				array_push($params, (int)$cat_interior_id);
			}
			if($isNeedRandom == 1)
				$strSQL.= " ORDER BY RAND() ";
			else
				$strSQL.= " ORDER BY created_date DESC ";
				
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		public function getCountInterior($status = null, $cat_interior_id = '') {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS interior ";
			$strSQL.= " WHERE 1 = 1 ";
			if (strlen($status)) {
				$strSQL.= " AND interior.status = ? ";
				array_push($params, $status);
			}
			
			if ($cat_interior_id > 0) {
				$strSQL.= " AND interior.category_id = ? ";
				array_push($params, (int)$cat_interior_id);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_interior
		 * 
		 * @return	Array	: Data of table tbl_interior
		 * @version 1.0
		 */
		public function getInteriorById($oid) {
			$params = array();
			$strSQL = " SELECT interior.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS interior ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}
	}// end class		
 }
?>