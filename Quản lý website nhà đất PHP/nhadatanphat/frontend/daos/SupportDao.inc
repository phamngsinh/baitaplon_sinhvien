<?php
/**
 * @project_name: localframe
 * @file_name: SupportDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("SUPPORT_DAO_INC")) {

	define("SUPPORT_DAO_INC",1);
	
	class SupportDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_support ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " support_id ";
		}
		
		public function getAllSupports($account = null, $status = null, $limit = null, $offset = null) {
			$params = array();
			$strSQL = " SELECT support.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS support ";
			$strSQL.= " WHERE LOWER(support.support_account) LIKE ? ";
			array_push($params, '%'.strtolower($account).'%');
			if (strlen($status)) {
				$strSQL.= " AND support.status = ? ";
				array_push($params, $status);
			}
			$strSQL.= " ORDER BY support.support_genre, support.support_id ";
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		public function getCountSupport($account = null, $status = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS support ";
			$strSQL.= " WHERE LOWER(support.support_account) LIKE ? ";
			array_push($params, '%'.strtolower($account).'%');
			if (strlen($status)) {
				$strSQL.= " AND support.status = ? ";
				array_push($params, $status);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_category
		 * 
		 * @return	Array	: Data of table tbl_category
		 * @version 1.0
		 */
		public function getSupportById($oid) {
			$params = array();
			$strSQL = " SELECT support.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS support ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}
	}// end class		
 }
?>