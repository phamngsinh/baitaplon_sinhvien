<?php
/**
 * @project_name: localframe
 * @file_name: @daos.php
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("DAO_FILE_INC")) {
	define("DAO_FILE_INC",1);

	require_once "MemberDao.inc";
	require_once "ProvinceDao.inc";
	require_once "DistrictDao.inc";
	require_once "CompanyDao.inc";
	require_once "NewsDao.inc";
	require_once "PromoteDao.inc";
	require_once "InteriorDao.inc";
	require_once "SupportDao.inc";
	require_once "EstateDao.inc";
	require_once "InformDao.inc";
	require_once "CategoryDao.inc";
	require_once "ChildDao.inc";
	require_once "ContactDao.inc";
	require_once "KeywordDao.inc";
	require_once "ConfigDao.inc";
	
	require_once "CatInteriorDao.inc";
 }
?>