<?php 
/**
 * @project_name: localframe
 * @file_name: DistrictDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("CONFIG_DAO_INC")) {

	define("CONFIG_DAO_INC",1);
	
	class ConfigDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " mst_configs ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " config_id ";
		}
		
		public function getConfig() {
			$params = array();
			$strSQL = " SELECT * ";
			$strSQL.= " FROM ".$this->getTableName();
			$strSQL.= " LIMIT 1";

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return $result[0];
		}
		
	}// end class		
 }
?>