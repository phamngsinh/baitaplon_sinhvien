<?php
/**
 * @project_name: localframe
 * @file_name: CategoryDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("CATEGORY_DAO_INC")) {

	define("CATEGORY_DAO_INC",1);
	
	class CategoryDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_category ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " category_id ";
		}
		
		public function getAllCategories($sqlparam = null, $limit = null, $offset = null) {
			$params = array();
			$strSQL = " SELECT category.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS category ";
			$strSQL.= " WHERE 1 = 1 ";
			if (isset($sqlparam["horizontal"]) && strlen($sqlparam["horizontal"])) {
				$strSQL.= " AND category.category_horizontal = ? ";
				array_push($params, $sqlparam["horizontal"]);
			}
			if (isset($sqlparam["vertical"]) && strlen($sqlparam["vertical"])) {
				$strSQL.= " AND category.category_vertical = ? ";
				array_push($params, $sqlparam["vertical"]);
			}
			if (isset($sqlparam["bottom"]) && strlen($sqlparam["bottom"])) {
				$strSQL.= " AND category.category_bottom = ? ";
				array_push($params, $sqlparam["bottom"]);
			}
			if (isset($sqlparam["kind"]) && strlen($sqlparam["kind"])) {
				$strSQL.= " AND category.category_kind = ? ";
				array_push($params, $sqlparam["kind"]);
			}
			if (isset($sqlparam["status"]) && strlen($sqlparam["status"])) {
				$strSQL.= " AND category.status = ? ";
				array_push($params, $sqlparam["status"]);
			}
			$strSQL.= " ORDER BY category.category_sort ";
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		/**
		 * @note:	function get data of tabla tbl_category
		 * 
		 * @return	Array	: Data of table tbl_category
		 * @version 1.0
		 */
		public function getCategoryById($oid) {
			$params = array();
			$strSQL = " SELECT category.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS category ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}
	}// end class		
 }
?>