<?php
/**
 * @project_name: localframe
 * @file_name: NewsDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("NEWS_DAO_INC")) {

	define("NEWS_DAO_INC",1);
	
	class NewsDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_news ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " news_id ";
		}
		
		public function getAllNews($newgenre = null, $status = null, $newsid = null, $limit = null, $offset = null) {
			$params = array();
			$strSQL = " SELECT news.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS news ";
			$strSQL.= " WHERE 1 = 1 ";
			if (strlen($newgenre)) {
				$strSQL.= " AND news.news_genre = ? ";
				array_push($params, $newgenre);
			}
			if (strlen($status)) {
				$strSQL.= " AND news.status = ? ";
				array_push($params, $status);
			}
			if (strlen($newsid)) {
				$strSQL.= " AND news.news_id <> ? ";
				array_push($params, $newsid);
			}
			
			$strSQL.= " ORDER BY news.created_date DESC ";

			if ($offset < 0) 
				$offset = 0;

			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return is_array($result) ? $result : array();
		}

		public function getCountNews($newgenre = null, $status = null, $newsid = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS news ";
			$strSQL.= " WHERE 1 = 1 ";
			if (strlen($newgenre)) {
				$strSQL.= " AND news.news_genre = ? ";
				array_push($params, $newgenre);
			}
			if (strlen($status)) {
				$strSQL.= " AND news.status = ? ";
				array_push($params, $status);
			}
			if (strlen($newsid)) {
				$strSQL.= " AND news.news_id <> ? ";
				array_push($params, $newsid);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_news
		 * 
		 * @return	Array	: Data of table tbl_news
		 * @version 1.0
		 */
		public function getNewsById($oid) {
			$params = array();
			$strSQL = " SELECT news.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS news ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}
	}// end class		
 }
?>