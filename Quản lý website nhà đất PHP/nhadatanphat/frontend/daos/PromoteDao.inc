<?php
/**
 * @project_name: localframe
 * @file_name: PromoteDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("PROMOTE_DAO_INC")) {

	define("PROMOTE_DAO_INC",1);
	
	class PromoteDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_promote ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " promote_id ";
		}
		
		public function getAllPromotes($provinceid = null, $status = null, $position = null, $limit = null, $offset = null) {

			$params = array();
			$strSQL = " SELECT promote.*, province_name ";
			$strSQL.= " FROM ".$this->getTableName()." AS promote ";
			$strSQL.= " LEFT JOIN tbl_province AS province ON province.province_id = promote.province_id ";
			$strSQL.= " WHERE promote.promote_start <= NOW() AND (promote.promote_end >= NOW() OR promote.promote_end IS NULL OR DATE_FORMAT(promote.promote_end, '%Y%m%d') = 0) ";
			
			if (strlen($provinceid)) {
				$strSQL.= " AND promote.province_id = ? ";
				array_push($params, $provinceid);
			}
			
			if (strlen($status)) {
				$strSQL.= " AND promote.status = ? ";
				array_push($params, $status);
			}
			if (strlen($position)) {
				$strSQL.= " AND promote.promote_position = ? ";
				array_push($params, $position);
			}
			
			//if($limit <= 1){
				$strSQL.= " ORDER BY RAND() ";
			//}else{
			//	$strSQL.= " ORDER BY promote.created_date DESC ";
			//}
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	return is_array($result) ? $result : array();
		}

		public function getCountPromotes($provinceid = null, $status = null, $position = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS promote ";
			$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = promote.province_id ";
			$strSQL.= " WHERE 1 = 1 ";
			if (strlen($status)) {
				$strSQL.= " AND promote.status = ? ";
				array_push($params, $status);
			}
			if (strlen($provinceid)) {
				$strSQL.= " AND promote.province_id = ? ";
				array_push($params, $provinceid);
			}
			if (strlen($position)) {
				$strSQL.= " AND promote.promote_position = ? ";
				array_push($params, $position);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		public function getPromoteById($oid) {
			$params = array();
			$strSQL = " SELECT promote.*, province_name ";
			$strSQL.= " FROM ".$this->getTableName()." AS promote ";
			$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = promote.province_id ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}

		public function ClickedPromote($oid) {
			$params = array();
			$strSQL = " UPDATE ".$this->getTableName()." SET promote_click = promote_click + 1 ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}
	}// end class		
 }
?>