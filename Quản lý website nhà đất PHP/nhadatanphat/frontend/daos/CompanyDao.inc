<?php
/**
 * @project_name: localframe
 * @file_name: CompanyDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("COMPANY_DAO_INC")) {

	define("COMPANY_DAO_INC",1);
	
	class CompanyDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_company ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " company_id ";
		}
		
		public function getAllCompanies($companyid = null, $provinceid = null, $status = null, $limit = null, $offset = null, $additionalParams=array()){
			$params = array();
			$strSQL = " SELECT company.*, province_name ";
			$strSQL.= " FROM ".$this->getTableName()." AS company ";
			$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = company.province_id ";
			$strSQL.= " WHERE 1 = 1 ";
			if (strlen($companyid)) {
				$strSQL.= " AND company.company_id <> ? ";
				array_push($params, $companyid);
			}

			if (isset($provinceid) && strlen($provinceid)) {
				$strSQL.= " AND company.province_id = ? ";
				array_push($params, $provinceid);
			}
			if($additionalParams['isShowInHomePage'] == 1){
				$strSQL.= " AND company.is_show_in_homepage=1 ";
			}
			if($additionalParams['isShowInProvince'] == 1){
				$strSQL.= " AND company.is_show_in_province=1 ";
			}
			
			if (strlen($status)) {
				$strSQL.= " AND company.status = ? ";
				array_push($params, $status);
			}
			
			if(!isset($additionalParams["isRandom"]))
				$strSQL.= " ORDER BY company.is_show_in_homepage DESC, company.is_show_in_province DESC, company.created_date DESC, company.company_name ";
			else
				$strSQL.= " ORDER BY RAND() ";

			if ($offset < 0)
				$offset = 0;

			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		public function getCountCompany($companyid= null, $provinceid = null, $status = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS company ";
			$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = company.province_id ";
			$strSQL.= " WHERE 1 = 1 ";
			if (strlen($companyid)) {
				$strSQL.= " AND company.company_id <> ? ";
				array_push($params, $companyid);
			}
			if (strlen($provinceid)) {
				$strSQL.= " AND company.province_id = ? ";
				array_push($params, $provinceid);
			}
			if (strlen($status)) {
				$strSQL.= " AND company.status = ? ";
				array_push($params, $status);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_Company
		 * 
		 * @return	Array	: Data of table tbl_Company
		 * @version 1.0
		 */
		public function getCompanyById($oid) {
			$params = array();
			$strSQL = " SELECT company.*, province_name ";
			$strSQL.= " FROM ".$this->getTableName()." AS company ";
			$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = company.province_id ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}
	}// end class		
 }
?>