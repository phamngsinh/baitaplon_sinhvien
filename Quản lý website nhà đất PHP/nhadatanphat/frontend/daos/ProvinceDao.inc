<?php
/**
 * @project_name: localframe
 * @file_name: ProvinceDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("PROVINCE_DAO_INC")) {

	define("PROVINCE_DAO_INC",1);
	
	class ProvinceDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_province ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " province_id ";
		}
		
		public function getAllProvinces($status = null, $limit = 0, $offset = 0) {
			$params = array();
			$strSQL = " SELECT province.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS province ";
			$strSQL.= " WHERE 1 = 1 ";
			if (strlen($status)) {
				$strSQL.= " AND province.status = ? ";
				array_push($params, $status);
			}
			$strSQL.= " ORDER BY  province.s_order, province.province_name ";
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	return is_array($result) ? $result : array();
		}

		public function getCountProvince($status = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS province ";
			$strSQL.= " WHERE 1 = 1 ";
			if (strlen($status)) {
				$strSQL.= " AND province.status = ? ";
				array_push($params, $status);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_province
		 * 
		 * @return	Array	: Data of table tbl_province
		 * @version 1.0
		 */
		public function getProvinceById($oid) {
			$params = array();
			$strSQL = " SELECT province.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS province ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}
	}// end class		
 }
?>