<?php
/**
 * @project_name: localframe
 * @file_name: CategoryDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("CHILD_DAO_INC")) {

	define("CHILD_DAO_INC",1);
	
	class ChildDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_child ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " child_id ";
		}
		
		public function getAllCategories($categoryname = null, $languageid = null, $status = null, $limit = null, $offset = null) {
			$params = array();
			$strSQL = " SELECT category.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS category ";
			$strSQL.= " WHERE LOWER(category.category_name) LIKE ? ";
			array_push($params, '%'.strtolower($categoryname).'%');
			if (strlen($languageid)) {
				$strSQL.= " AND category.language_id = ? ";
				array_push($params, $languageid);
			}
			if (strlen($status)) {
				$strSQL.= " AND category.status = ? ";
				array_push($params, $status);
			}
			$strSQL.= " ORDER BY category.category_sort ";
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		public function getCountCategories($categoryname = null, $languageid = null, $status = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS category ";
			$strSQL.= " WHERE LOWER(category.category_name) LIKE ? ";
			array_push($params, '%'.strtolower($categoryname).'%');
			if (strlen($languageid)) {
				$strSQL.= " AND category.language_id = ? ";
				array_push($params, $languageid);
			}
			if (strlen($status)) {
				$strSQL.= " AND category.status = ? ";
				array_push($params, $status);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		public function getAllCategoryByParentId($categoryid, $parentId, $status = null, $languageid = null) {
		 	$params = array();
		 	$strSQL = " SELECT child.*, category.category_name ";
			$strSQL.= " FROM ". $this->getTableName() ." child  ";
			$strSQL.= " INNER JOIN tbl_category AS category ON category.category_id = child.category_id ";
			$strSQL.= " WHERE child.parent_id = ? ";
			array_push($params, $parentId);
			if (strlen($categoryid)) {
				$strSQL.= " AND child.category_id = ? ";
				array_push($params, $categoryid);
			}
			if (strlen($status)) {
				$strSQL.= " AND child.status = ? ";
				array_push($params, $status);
			}
			$strSQL.= " ORDER BY child.category_id, child.child_sort";

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		public function getCountCategoryByParentId($parentId, $languageid = null) {
		 	$params = array();
		 	$strSQL = " SELECT COUNT(category.child_id) AS count ";
			$strSQL.= " FROM ". $this->getTableName() ." category  ";
			$strSQL.= " WHERE category.parent_id = ? ";
			array_push($params, $parentId);
			if (strlen($languageid)) {
				$strSQL.= " AND category.language_id = ? ";
				array_push($params, $languageid);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_category
		 * 
		 * @return	Array	: Data of table tbl_category
		 * @version 1.0
		 */
		public function getCategoryById($oid) {
			$params = array();
			$strSQL = " SELECT category.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS category ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}

		public function checkExistedCategoriesName($categoryname, $oid, $languageid = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS category ";
			$strSQL.= " WHERE category.category_name = ? AND ".$this->getKeys()." <> ? ";
			array_push($params, $categoryname, $oid);
			if (strlen($languageid)) {
				$strSQL.= " AND category.language_id = ? ";
				array_push($params, $languageid);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return intval($result[0]["count"]);
		}
		
		public function getChildGroupCategory($province){
			$params	= array();
			$strSQL = " SELECT DISTINCT this.child_id, this.child_name, this.category_id, ";
			$strSQL .= "(";
				$strSQL.= " SELECT COUNT(estate_id) AS count ";
				$strSQL.= " FROM tbl_estate AS estate ";
				$strSQL.= " INNER JOIN tbl_member AS member ON member.member_id = estate.member_id ";
				$strSQL.= " LEFT JOIN tbl_category AS category ON category.category_id = estate.category_id ";
				$strSQL.= " LEFT JOIN tbl_child AS child ON child.child_id = estate.child_id ";
				$strSQL.= " LEFT JOIN tbl_province AS province ON province.province_id = estate.province_id ";
				$strSQL.= " LEFT JOIN tbl_district AS district ON district.district_id = estate.district_id AND district.status = 1 ";
				$strSQL.= " WHERE estate.estate_duration > now() ";
				$strSQL.= "	AND estate.status = 1 AND province.status = 1 ";
				$strSQL.= "	AND estate.child_id = this.child_id ";
				if (strlen($province)){
					$strSQL.= " AND estate.province_id = ? ";
					array_push($params, $province);
				}

				$strSQL.= " AND (estate.deleted_date > now() OR estate.deleted_date IS NULL)";
				$strSQL.= " AND estate.is_deleted_by_member = 0";

				$strSQL.= " GROUP BY estate.child_id";

			$strSQL .= ") as childCount";
			$strSQL.= " FROM ".$this->getTableName()." this ";
			$strSQL.= " INNER JOIN tbl_category on this.category_id = tbl_category.category_id ";
			$strSQL.= " ORDER BY this.child_sort, this.child_name ASC ";

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}
		
		/**
		 * @note:	function insert data for table tbl_category
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function insert_category($objects) {
			return $this->insert_db($this->getTableName(), $objects);
		}

		/**
		 * @note:	function update data for table tbl_category
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function update_category($objects, $oid) {
			return $this->update_db($this->getTableName(), $objects, $this->getKeys()." = ? ", array($oid));
		}

		/**
		 * @note:	function delete data for table tbl_category
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function delete_category($oid) {
			$this->delete_db(" tbl_inform ", $this->getKeys()." = ? ", array($oid));
			return $this->delete_db($this->getTableName(), $this->getKeys()." = ? ", array($oid));
		}
	}// end class		
 }
?>