<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: message.inc
 * @descr:
 * 
 * @author 	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 *****************************************************************/
if (!defined("MESSAGE_FRONTEND_INC") ) {
	Define("MESSAGE_FRONTEND_INC", 		"TRUE");
	
	Define("MESSAGE_RESULT_NULL", 		"Not record to do display.");
	Define("MESSAGE_LOGIN", 			"Username or Password invalid.");
	Define("MESSAGE_INSERT_FALSE", 		"Execute query insert data fail.");
	Define("MESSAGE_UPDATE_FALSE", 		"Execute query update data fail.");
	Define("MESSAGE_DELETE_FALSE", 		"Execute query delete data fail.");
	Define("MESSAGE_INSERT_SUCCESS", 	"Bạn đã thực hiện thành công một tin rao.");
	Define("MESSAGE_REGIST_SUCCESS", 	"Bạn đã đăng ký thành công trên website <a href='www.nhadatanphat.vn'>www.nhadatanphat.vn</a>.");

	Define("ESTATE_IMAGE_ERRORS", 		"Một trong các ảnh mô tả hoặc file Video của bạn không đúng định dạng.");
	Define("ESTATE_IMAGE_SIZE", 		"Một trong các ảnh mô tả hoặc file Video của bạn không nhỏ hơn ");
	Define("ESTATE_TITLE_LENGTH", 		"Tiêu đề tin là cần phải nhập không quá 255 ký tự.");
	Define("ESTATE_CATEGORY_NULL", 		"Loại tin rao là cần phải chọn cả hai thông tin.");
	Define("ESTATE_CHILD_NULL", 		"Loại tin rao là cần phải chọn cả hai thông tin.");
	Define("ESTATE_ADDRESS_LENGTH", 	"Vị trí là cần phải nhập không quá 255 ký tự.");
	Define("ESTATE_PROVINCE_NULL", 		"Tên Tỉnh/Thành trong địa chỉ địa ốc là cần phải chọn.");
	Define("ESTATE_DISTRICT_NULL", 		"Tên Quận/Huyện trong địa chỉ địa ốc là cần phải chọn.");
	Define("ESTATE_PRICE_ERROR", 		"Giá tiền bạn mong muốn có được là cần phải nhập kiểu số không quá 20 ký tự (VD: 1.000.000.000).");
	Define("ESTATE_AREA_LENGTH", 		"Diện tích phải nhập không quá 255 ký tự.");
	Define("ESTATE_PERSON_LENGTH", 		"Tên người liên hệ là cần phải nhập không quá 255 ký tự.");
	Define("ESTATE_COMPANY_LENGTH", 	"Tên công ty liên hệ phải nhập không quá 255 ký tự.");
	Define("ESTATE_ADDPER_LENGTH", 		"Địa chỉ liên hệ là cần phải nhập không quá 255 ký tự.");
	Define("ESTATE_EMAIL_NULL", 		"Địa chỉ mail liên hệ là cần phải nhập không quá 100 ký tự.");
	Define("ESTATE_EMAIL_ERRORS", 		"Địa chỉ mail liên hệ nhập không đúng định dạng.");
	Define("ESTATE_PHONE_NULL", 		"Số điện thoại của bạn là cần phải nhập không quá 255 ký tự.");
	Define("ESTATE_PHONE_ERRORS", 		"Số điện thoại bạn nhập không đúng định dạng.");
	Define("ESTATE_WEBSITE_LENGTH", 	"Địa chỉ website nhập không quá 255 ký tự.");
	Define("ESTATE_WEBSITE_ERRORS", 	"Địa chỉ website bạn nhập không đúng định dạng.");
	Define("ESTATE_DURATION_ERROR", 	"Ngày ngừng rao tin bạn nhập không đúng định dạng.");

	Define("PASS_CONFIRM_ERRORS", 	"Mật khẩu và mật khẩu xác nhận của bạn không giống nhau.");
	Define("PASS_OLD_ERRORS", 		"Mật khẩu cũ của bạn không đúng.");
	Define("USERNAME_EXISTED", 		"Tên đăng nhập này đã tồn tại. Bạn hãy chọn tên đăng nhập khác.");
	Define("EMAIL_EXISTED", 		"Địa chỉ mail này đã tồn tại. Bạn hãy chọn địa chỉ mail khác.");
	Define("USERNAME_NULL", 		"Tên đăng nhập là cần phải nhập lớn hơn 6 và không quá 50 ký tự.");
	Define("USERNAME_ERRORS", 		"Tên đăng nhập bạn nhập không đúng định dạng.");
	Define("PASSWORD_NULL", 		"Mật khẩu là cần phải nhập lớn hơn 6 và không quá 50 ký tự.");
	Define("PASSWORD_ERRORS", 		"Mật khẩu là cần phải nhập lớn hơn 6 và không quá 50 ký tự.");
	Define("FIRSTNAME_NULL", 		"Họ của bạn là cần phải nhập không quá 50 ký tự.");
	Define("FIRSTNAME_LENGTH", 		"Họ của bạn là cần phải nhập không quá 50 ký tự.");
	Define("LASTNAME_NULL", 		"Tên đệm của bạn là cần phải nhập không quá 50 ký tự.");
	Define("LASTNAME_LENGTH", 		"Tên đệm của bạn là cần phải nhập không quá 50 ký tự.");
	Define("NAME_NULL", 			"Tên của bạn là cần phải nhập không quá 50 ký tự.");
	Define("NAME_LENGTH", 			"Tên của bạn là cần phải nhập không quá 50 ký tự.");
	Define("EMAIL_NULL", 			"Địa chỉ mail của bạn là cần phải nhập không quá 100 ký tự.");
	Define("EMAIL_ERRORS", 			"Địa chỉ mail của bạn nhập không đúng định dạng.");
	Define("COMPANY_NULL", 			"Họ tên/Tên công ty là cần phải nhập không quá 255 ký tự.");
	Define("COMPANY_LENGTH", 		"Họ tên/Tên công ty là phải nhập không quá 255 ký tự.");
	Define("ADDRESS_NULL", 			"Địa chỉ của bạn là cần phải nhập không quá 255 ký tự.");
	Define("ADDRESS_LENGTH", 		"Địa chỉ của bạn là cần phải nhập không quá 255 ký tự.");
	Define("PHONE_NULL", 			"Số điện thoại của bạn là cần phải nhập không quá 255 ký tự.");
	Define("PHONE_ERRORS", 			"Số điện thoại bạn nhập không đúng định dạng.");
	Define("WEBSITE_LENGTH", 		"Địa chỉ website là phải nhập không quá 255 ký tự.");
	Define("WEBSITE_ERRORS", 		"Địa chỉ website bạn nhập không đúng định dạng.");
	
	Define("CONTACT_PERSON", 		"Tên của bạn là cần phải nhập không lớn hơn 255 ký tự.");
	Define("CONTACT_MAIL", 			"Địa chỉ mail của bạn là cần phải nhập đúng định dạng không lớn hơn 100 ký tự.");
	Define("CONTACT_TITLE", 		"Tiêu đề liên hệ là cần phải nhập không lớn hơn 255 ký tự.");
	Define("CONTACT_CONTENT", 		"Nội dung liên hệ là cần phải nhập.");
	Define("CONTACT_SUCCESS", 		"Cảm ơn bạn, bạn đã gửi thông tin thành công.<br />Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất.");
}
?>
