<?php
	/*****************************************************************
	 * @project_name: localframe
	 * @file_name: #validate.php
	 * @descr:
	 * 
	 * @author 	Khuong Van Chien - khuongchien@gmail.com
	 * @version 1.0
	 *****************************************************************/
	Define("MEMBER_VALIDATION_FILE", 	FRONTEND_CONF_PATH.DS."validate".DS."member_validation.ini");
	Define("ESTATE_VALIDATION_FILE", 	FRONTEND_CONF_PATH.DS."validate".DS."estate_validation.ini");
	Define("CONTACT_VALIDATION_FILE", 	FRONTEND_CONF_PATH.DS."validate".DS."contactInfo_validation.ini");
	Define("CONTACT_VALIDATION_FILE", 	FRONTEND_CONF_PATH.DS."validate".DS."contact_validation.ini");

?>