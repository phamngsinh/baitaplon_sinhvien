<?php /* Smarty version 2.6.18, created on 2011-03-31 18:07:45
         compiled from /home/nhadatan/public_html//frontend/templates/news.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', '/home/nhadatan/public_html//frontend/templates/news.html', 58, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<!-- Begin center area -->
     	<tr>
       		<td>
       			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<!-- Begin left area -->
		            <td width="200" valign="top">
						<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	            	</td>
					<!-- End left area -->
					
					<!-- Begin right area -->
	            	<td align="right" valign="top">
		            	<table width="99%" border="0" cellpadding="0" cellspacing="0">
		              	<tr>
		               		<td>
		               			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			                  	<tr>
				                    <td>
				                    	<table width="100%" border="0" cellpadding="0" cellspacing="0" background="frontend/images/c3.jpg">
				                        <tr>
				                          	<td><img src="frontend/images/c1.jpg" width="7" height="32" /></td>
				                          	<td width="100%" align="left" class="text_red">
				                          		<table width="100%" border="0" cellpadding="0" cellspacing="0">
				                              	<tr>
					                				<td align="left" id="menu1" dir="ltr" style="color: #CC0303;">
					                					<?php if ($this->_tpl_vars['genre'] == '1'): ?>TIN PHONG THỦY<?php elseif ($this->_tpl_vars['genre'] == '0'): ?>BẢN TIN NHÀ ĐẤT<?php elseif ($this->_tpl_vars['genre'] == '2'): ?>VĂN BẢN PHÁP LUẬT<?php else: ?>TIN TỨC<?php endif; ?>
					                				</td>
				                              	</tr>
				                            	</table>
				                            </td>
				                          	<td><img src="frontend/images/c2.jpg" width="7" height="32" /></td>
			                        	</tr>
			                    		</table>
									</td>
								</tr>
				                <tr>
	                    			<td>
	                    				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				                        <tr>
				                          	<td width="3" background="frontend/images/c4.jpg"></td>
				                          	<td align="center" bgcolor="#F5F9FD">
				                          		<table width="100%" border="0" cellpadding="0" cellspacing="0">
	                            				<tr>
					                              	<td colspan="3" height="10"></td>
					                            </tr>
					                            <?php if ($this->_tpl_vars['newsid'] != ''): ?>
	                            				<tr>
					                              	<td colspan="3" align="left">
					                              		<table width="98%" border="0" cellpadding="0" cellspacing="0">
					                              		<tr>
					                              			<td align="left" style="padding-left: 10px;">
					                              				<span style="font-size:15px;font-weight: bold;display:inline">
					                              					<?php echo $this->_tpl_vars['NewsDetail']['news_name']; ?>

					                              				</span>
																
																(<?php echo ((is_array($_tmp=$this->_tpl_vars['NewsDetail']['created_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d-%m-%Y") : smarty_modifier_date_format($_tmp, "%d-%m-%Y")); ?>
)

																<?php $this->assign('now', ((is_array($_tmp=time())) ? $this->_run_mod_handler('date_format', true, $_tmp) : smarty_modifier_date_format($_tmp))); ?>
																<?php $this->assign('createdDate', ((is_array($_tmp=$this->_tpl_vars['NewsDetail']['created_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp) : smarty_modifier_date_format($_tmp))); ?>
																<?php if ($this->_tpl_vars['now'] == $this->_tpl_vars['createdDate']): ?>
																	&nbsp;<img border="0" src="frontend/images/new_icon.gif">
																<?php endif; ?>
					                              			</td>
					                              		</tr>
					                              	<!--
					                              		<tr>
					                              			<td width="10%" align="left" valign="top" style="padding-left: 10px; padding-top: 15px;">
					                              				<a id="thumb<?php echo $this->_tpl_vars['NewsDetail']['news_image']; ?>
" href="<?php echo $this->_config[0]['vars']['IMG_NEWS_DIR']; ?>
<?php echo $this->_tpl_vars['NewsDetail']['news_image']; ?>
" class="highslide" onclick="return hs.expand(this)">
					                              				<img src="<?php echo $this->_config[0]['vars']['IMG_NEWS_DIR']; ?>
thumb_<?php echo $this->_tpl_vars['NewsDetail']['news_image']; ?>
" align="left" style="max-width: 200px;" class="border_img" hspace="5" /></a>
					                              				<font name="arial" style="font-weight:bold;"><?php echo $this->_tpl_vars['NewsDetail']['news_title']; ?>
</font>
					                              			</td>
					                              		</tr>
													-->
					                              		<tr>
					                              			<td align="left" style="padding-left: 10px; padding-top: 10px;">
					                              				<p style="padding-left: 5px;"><?php echo $this->_tpl_vars['NewsDetail']['news_content']; ?>
</p>
					                              			</td>
					                              		</tr>
					                              		<tr>
					                              			<td>
					                              				<div style="padding-left: 10px; font-weight: bold; padding-bottom: 10px;">Các tin tức khác</div>
					                              				<table width="98%" border="0" cellpadding="0" cellspacing="0">
					                              				<?php $_from = $this->_tpl_vars['NewsList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['keys'] => $this->_tpl_vars['items']):
?>
					                              				<tr>
							                              			<td width="10" align="left" valign="middle" style="padding-right: 5px; padding-left: 10px;">
							                              				<img src="frontend/images/new.gif" width="10" height="8" />
							                              			</td>
					                              					<td align="left">
																		<a href="?hdl=news&genre=<?php echo $this->_tpl_vars['items']['news_genre']; ?>
&newsid=<?php echo $this->_tpl_vars['items']['news_id']; ?>
&page=<?php echo $this->_tpl_vars['page']; ?>
" class="text_blue"><?php echo $this->_tpl_vars['items']['news_name']; ?>
</a>
																		<?php $this->assign('now_', ((is_array($_tmp=time())) ? $this->_run_mod_handler('date_format', true, $_tmp) : smarty_modifier_date_format($_tmp))); ?>
																		<?php $this->assign('created_Date', ((is_array($_tmp=$this->_tpl_vars['items']['created_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp) : smarty_modifier_date_format($_tmp))); ?>
																		<?php if ($this->_tpl_vars['now_'] == $this->_tpl_vars['created_Date']): ?>
																			&nbsp;<img border="0" src="frontend/images/new_icon.gif">
																		<?php endif; ?>
																		</td>
					                              				</tr>
					                              				<?php endforeach; endif; unset($_from); ?>
					                              				<tr>
					                              					<td align="center" colspan="2" style="padding-top: 5px;"><?php echo $this->_tpl_vars['paging']; ?>
</td>
					                              				</tr>
					                              				</table>
					                              			</td>
					                              		</tr>
					                              		</table>
					                              	</td>
					                            </tr>
					                            <?php else: ?>
												<?php $_from = $this->_tpl_vars['NewsList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['keys'] => $this->_tpl_vars['items']):
?>
				                            	<?php if ($this->_tpl_vars['keys'] % 2 == 0): ?><!-- Mo the -->
				                            	<tr>
				                            	<?php endif; ?>
				                              		<td width="50%" align="center" valign="top">
				                              			<table width="98%" border="0" cellpadding="0" cellspacing="0">
				                                		<tr>
					                                        <td valign="top" style="padding-top: 5px;">
					                                        	<a id="thumb<?php echo $this->_tpl_vars['items']['news_image']; ?>
" href="<?php echo $this->_config[0]['vars']['IMG_NEWS_DIR']; ?>
<?php echo $this->_tpl_vars['items']['news_image']; ?>
" class="highslide" onclick="return hs.expand(this)">
					                                        	<img src="<?php echo $this->_config[0]['vars']['IMG_NEWS_DIR']; ?>
thumb_<?php echo $this->_tpl_vars['items']['news_image']; ?>
" width="92" class="border_img" align="left" hspace="5" />
					                                        	</a>
					                                        	<a href="?hdl=news&genre=<?php echo $this->_tpl_vars['items']['news_genre']; ?>
&newsid=<?php echo $this->_tpl_vars['items']['news_id']; ?>
" class="text_blue"><strong><?php echo $this->_tpl_vars['items']['news_name']; ?>
</strong></a>
					                                        	<div style="padding-top: 7px;"><?php echo $this->_tpl_vars['items']['news_title']; ?>
</div>
																<?php $this->assign('now_', ((is_array($_tmp=time())) ? $this->_run_mod_handler('date_format', true, $_tmp) : smarty_modifier_date_format($_tmp))); ?>
																<?php $this->assign('created_Date', ((is_array($_tmp=$this->_tpl_vars['items']['created_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp) : smarty_modifier_date_format($_tmp))); ?>
																<?php if ($this->_tpl_vars['now_'] == $this->_tpl_vars['created_Date']): ?>
																	&nbsp;<img border="0" src="frontend/images/new_icon.gif">
																<?php endif; ?>
					                                        </td>
				                                		</tr>
	                              						</table>
	                              					</td>
	                              				<?php if (( $this->_tpl_vars['keys']+1 ) % 2 == 0): ?><!-- Dong the -->
	                              				</tr>
	                              				<?php endif; ?>
	                              				<?php endforeach; endif; unset($_from); ?>
	                              				<?php if (( $this->_tpl_vars['keys']+1 ) % 2 != 0): ?><!-- Dong the -->
	                              				</tr>
	                              				<?php endif; ?>
	                            				<tr>
					                              	<td colspan="3" align="center" style="padding-top: 5px;"><?php echo $this->_tpl_vars['paging']; ?>
</td>
					                            </tr>
					                            <?php endif; ?>
	                            				<tr>
					                              	<td colspan="3" height="10"></td>
					                            </tr>
					                          	</table>
					                       	</td>
					                        <td width="4" background="frontend/images/c5.jpg"></td>
					                   	</tr>
					                   	</table>
									</td>
								</tr>
	                  			<tr>
	                    			<td>
	                    				<table width="100%" border="0" cellpadding="0" cellspacing="0" background="frontend/images/c8.jpg">
				                        <tr>
				                          	<td><img src="frontend/images/c6.jpg" width="8" height="8" /></td>
				                          	<td width="100%"></td>
				                          	<td><img src="frontend/images/c7.jpg" width="7" height="8" /></td>
				                        </tr>
	                    				</table>
	                    			</td>
				                </tr>
				                </table>
							</td>
						</tr>
						<tr>
				        	<td height="5"></td>
				        </tr>
						</table>
					</td>
					<!-- End right area -->
				</tr>
				</table>
			</td>
		</tr>
		<!-- End center area -->
		
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>