<?php
	/**
	 * @project_name: localframe
	 * @file_name: index.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **/
	session_start();

	define("DS", DIRECTORY_SEPARATOR);
	set_include_path("include_path");

	Define("ACTION", 	"index");
	Define("WRITELOG", 	FALSE);
	
	require_once "library".DS."@library.php";
	require_once SRC_FRONTEND.DS."configs".DS."@config.php";
	require_once SRC_FRONTEND.DS."daos".DS."@daos.php";
	Define("LOGS_DIR", FRONTEND_LOG_PATH);
	
	$hdl  = isset($_REQUEST["hdl"]) ?  trim($_REQUEST["hdl"])  : "index";
	$kind = isset($_REQUEST["kind"]) ? trim($_REQUEST["kind"]) : "";

	$page = FRONTEND_PAGE_PATH.$hdl.".php";

	$posIndx = strpos($hdl, "member");
	if($posIndx == 0){
		$EstateDao 	= new EstateDao();
		$paraminput["member"]	= (int)$_SESSION["_LOGIN_ESTATE_"]["member_id"];
		if ($state == 0) $paraminput["state"]	= 0;	// Tin da luu
		if ($state == 1) $paraminput["state"]	= 1;	// Tin da rao
		if ($state == 2) $paraminput["duration"]= TRUE;	// Tin het han
		if ($state == 3) $status				= 0;	// Tin cho duyet
		if ($state == 4) $paraminput["delete"]	= TRUE;	// Tin da xoa
		
		$paraminput["exceptCategoryId"]="yes";
		$paraminput["state"] = 0;//Saved news
		$paraminput["discardDeleteDate"]= TRUE;
		$paraminput["isDeletedByMember"]	= 0;
		$countSavedNews = $EstateDao->getCountEstateMemberAreaByParams($paraminput, $status);

		unset($paraminput["state"]);
		unset($paraminput["discardDeleteDate"]);
		$paraminput["isDeletedByMember"]	= 0;
		$status = 1;//Active news
		$countActiveNews = $EstateDao->getCountEstateMemberAreaByParams($paraminput, $status);

		unset($paraminput["state"]);//Expired news
		$paraminput["duration"]= TRUE;
		$paraminput["discardDeleteDate"]= TRUE;
		$paraminput["isDeletedByMember"] = 0;
		$countExpiredNews = $EstateDao->getCountEstateMemberAreaByParams($paraminput, $status);

		unset($paraminput["state"]);
		unset($paraminput["duration"]);//Wait for approve news
		$status = 0;
		$paraminput["discardDeleteDate"]= TRUE;
		$paraminput["isDeletedByMember"]	= 0;
		$countWaitForApproveNews = $EstateDao->getCountEstateMemberAreaByParams($paraminput, $status);

		unset($paraminput["state"]);//Deleted news
		unset($paraminput["duration"]);
		$status = null;
		$paraminput["isDeletedByMember"]	= 1;
		$paraminput["discardDeleteDate"]	= TRUE;
		$paraminput["isDiscardDuration"] 	= 1;
		$paraminput["discardStatus"]		= 1;
		$countDeletedNews = $EstateDao->getCountEstateMemberAreaByParams($paraminput, $status);
		$menuLeftCountItem = array("1Pos"=>$countActiveNews, "2Pos"=>$countExpiredNews, "3Pos"=>$countWaitForApproveNews, "0Pos"=>$countSavedNews, "4Pos"=>$countDeletedNews);

		$smarty->assign("countItem", $menuLeftCountItem);
		
		unset($state);
		unset($paraminput);
		unset($status);
	}

	if(file_exists($page)) {
		$textlang = "text_vn";
		
		list($mod) = split('[/]', $hdl);
		$smarty->assign("mod", 	$mod);

		$smarty->config_load("lang_en.conf");
		include_once FRONTEND_PAGE_PATH.DS."include".DS."categories.php";
		include_once FRONTEND_PAGE_PATH.DS."auth".DS."logout.php";
		include_once FRONTEND_PAGE_PATH.DS."auth".DS."login.php";
		include_once FRONTEND_PAGE_PATH.DS."include".DS."left.php";
		include_once FRONTEND_PAGE_PATH.DS."include".DS."left_detail.php";
		
		if ($mod == "member" && empty($_SESSION["_LOGIN_ESTATE_"])) header("Location: index.php");

		/************************************Get all Promote************************************/
		$province  		= isset($_REQUEST["province"]) ?	$_REQUEST["province"] 	: null;
		$PromoteDao 	= new PromoteDao();
		$provinceTmp = -1;//toan quoc.
		$provinceTmps = -1;
		if(isset($province)  && strlen($province) > 0){
			$provinceTmp = $province;
		}
		
		$PositionTC 	= $PromoteDao->getAllPromotes($provinceTmp, Status::active, Position::positionone, 1, 0);
		$PositionTR 	= $PromoteDao->getAllPromotes($provinceTmp, Status::active, Position::positiontwo, 1, 0);
		$PositionThr 	= $PromoteDao->getAllPromotes($provinceTmp, Status::active, Position::positionthree, 0, 0);
		$PositionFou 	= $PromoteDao->getAllPromotes($provinceTmp, Status::active, Position::positionfour, 0, 0);
		
		$PositionFive 	= $PromoteDao->getAllPromotes($provinceTmps, Status::active, Position::positionfive, 3, 0);
		
		$PositionFls 	= $PromoteDao->getAllPromotes($provinceTmp, Status::active, Position::positionsix, 1, 0);
		$PositionSeven 	= $PromoteDao->getAllPromotes($provinceTmps, Status::active, Position::positionseven, 1, 0);
//		print_r($PositionFive); die();
		$PositionSix 	= $PromoteDao->getAllPromotes($province, Status::active, Position::positionsix, 2, 0);
		$PositionEight 	= $PromoteDao->getAllPromotes($provinceTmp, Status::active, Position::positioneight, 1, 0);
		$smarty->assign("PositionTopCenter", $PositionTC[0]);
		$smarty->assign("PositionTopRight", $PositionTR[0]);
		$smarty->assign("Position3", 	$PositionThr);
		$smarty->assign("Position4", 	$PositionFou);
//		$smarty->assign("Position5", 	$PositionFive);
		$smarty->assign("PositionFls", 	$PositionFls);
		$smarty->assign("PositionSeven", 	$PositionSeven);
		$smarty->assign("PositionSix", 	$PositionSix);
//		print_r($PositionSix);die('x');
//		$PositionSeven = isset($PositionSeven[0])?$PositionSeven[0]:array();
//		$smarty->assign("PositionSeven", 	$PositionSeven);
//		$PositionEight = isset($PositionEight[0])?$PositionEight[0]:array();
//		$smarty->assign("PositionEight", 	$PositionEight);
		$smarty->assign("FileSWF", 		isset($PositionFls[0]["promote_banner"]) ? FileUtils::getFileExtension($PositionFls[0]["promote_banner"]) : null);
		/***************************************************************************************/
		
		$province  		= isset($_REQUEST["province"]) ?	$_REQUEST["province"] 	: null;
		$EstateDao		= new EstateDao();
		$GroupEstate 	= $EstateDao->getEstateGroupProvince();
//		print_r($GroupEstate);die();	
		$smarty->assign("GroupEstate", 	$GroupEstate);
		$EstateHot = $EstateDao->getAllEstateByParams(array("estate_hot"=>1, "state"=>1), Status::active, 20, 0);
		$smarty->assign("EstateHot", 	$EstateHot);	
//		print_r($EstateHot);die('xxxx');
//		$GroupCategory 	= $EstateDao->getEstateGroupCategory($province);
//		$params=array('estate_hot'=>1,'status'=>1);
//		$EstateHot = $EstateDao->getAllEstateHot($params);
//		$smarty->assign("EstateHot", 	$EstateHot);	
//		print_r($EstateHot); die('x');
		/********************************Get all Category menu top******************************/
		$CategoryDao= new CategoryDao();
		$CategoryHZL= $CategoryDao->getAllCategories(array("horizontal"=>MenuTop::yes, "status"=>Status::active));
		$smarty->assign("CategoryHZL", 	$CategoryHZL);
		$CategoryFoo= $CategoryDao->getAllCategories(array("bottom"=>MenuBottom::yes, "status"=>Status::active));
		$smarty->assign("CategoryFoo", 	$CategoryFoo);
		/***************************************************************************************/
		$configDao = new ConfigDao();
		$configObj = $configDao->getConfig();
		$smarty->assign("configObj", $configObj);
		
		$include_header 	= FRONTEND_TEMPLATE_PATH."include".DS."header".TPL_TYPE;
		$include_footer 	= FRONTEND_TEMPLATE_PATH."include".DS."footer".TPL_TYPE;
		$smarty->assign("include_header", 	$include_header);
		$smarty->assign("include_footer", 	$include_footer);
		$smarty->assign("hdl", 				$hdl);
		$smarty->assign("kind", 			$kind);
		include_once $page;
	} else {
		$hdl = "default";
		include_once FRONTEND_PAGE_PATH.DS."default.php";
	}
	
?>