<?php
/**
 * @project_name: localframe
 * @file_name: dateutils.inc
 * @descr:
 * 
 * @author 	ChienKV - khuongchien@gmail.com
 * @version 1.0
 **/ 
if (!defined("DATEUTILS_INC") ) {
 	define("DATEUTILS_INC",1);

	class DateUtils {
		public static function splitDate($date){
			$date = date("Y-m-d", strtotime($date));
			if (strlen($date) >= 8 && strlen($date) <= 10) {
				$date			= str_replace(array("/", ".", "-"), "/", $date);
				$date 			= explode("/",$date);
				if (count($date) == 3) { 
					if (!is_numeric($date[0]) || !is_numeric($date[1]) || !is_numeric($date[2])) {
						return null;
					} else {
						$val["year"] 	= $date[2];
						$val["month"] 	= $date[1];
						$val["day"] 	= $date[0];
						return $val;
					}
				} else {
					return null;
				}
			}
		}
		public static function splitDatetime($date){
			//$date = date("Y-m-d H:i:s", strtotime($date));
			$date 			= explode(" ",$date);
			$date[0] 		= explode("-",$date[0]);
			$val['year'] 	= $date[0][0];
			$val['month'] 	= $date[0][1];
			$val['day'] 	= $date[0][2];
			if (isset($date[1])) {
				$date[1] 		= explode(":",$date[1]);
				$val['hour'] 	= $date[1][0];
				$val['minute'] 	= $date[1][1];
				$val['second'] 	= $date[1][2];
			}
			return($val);
		}
		
		public static function joinDate($year = "0000",$month = "00",$day = "00"){
			
			$year  = ($year  != "")?($year):("0000");
			$month = ($month != "")?($month):("00");
			$day   = ($day   != "")?($day):("00");
			
			$format = "$year-$month-$day";
			return $format;
		
		}
		
		public static function join_datetime($year,$month,$day,$hour,$minute,$second){
			$head = DateUtils::joinDate($year,$month,$day);
			$tail = "$hour:$minute:$second";
			$format= $head." ".$tail;
			return $format;
		
		}
		
	    /**
		 * @note:	function isCheckDatetime() 
		 * 
		 * @author ChienKV(ntsc279@hotmail.com)
		 * @param String $str_datetime - YYYY/MM/DD
		 * @return boolean
		 **/
		 public static function isCheckDatetime ($str_datetime) {
		 	$str_datetime = date("Y-m-d", strtotime($str_datetime));
		 	list($year,$month,$day) = split('[/.-]', $str_datetime);
		 	return checkdate($month,$day,$year);
		 }
		 
		 /**
		 * function isDatetimeCompare
		 * @desc: to compare $str_date_1 and $str_date_2
		 * 
		 * @author ChienKV(ntsc279@hotmail.com)
		 * @param String $str_date_1 - YYYY/MM/DD
		 * @param String $str_date_2 - YYYY/MM/DD
		 * @return Boolean - True if $str_date_1 > $str_date_2
		 * 		   False if if $str_date_1 <= $str_date_2
		 **/
		public static function isDatetimeCompare($str_date_1, $str_date_2) {
			$str_date_1 = date("Y-m-d", strtotime($str_date_1));
			$str_date_2 = date("Y-m-d", strtotime($str_date_2));
			list($year1,$month1,$day1) = split('[/.-]', $str_date_1);
			list($year2,$month2,$day2) = split('[/.-]', $str_date_2);
			
			if (mktime(0, 0, 0, $month1, $day1, $year1) - mktime(0, 0, 0, $month2, $day2, $year2) > 0)
				return true;
			else
				return false;
			
		}

		/**
		 * Get day of week
		 * 
		 * @param string date allowed format: yyyy-MM or yyyy-MM-dd
		 * @param int day
		 * 
		 * @return string day of week
		 */
		public static function getDayOfWeek($date = "", $day = "") {
			if (!strlen($date)) $date = date("Y-m-d");
			else $date = date("Y-m-d", strtotime($date));

			$aNameOfDay = array("CN", "2", "3", "4", "5", "6", "7");

			if (strlen($date) === 7) $date .= "-01";
			else if( (strlen($date) >= 10) && (is_null($day) || ($day === '')) ) $day = (substr($date,8,2));
			$time = strtotime($date);
			
			// Get day of the week in month of year
	        $jd = cal_to_jd(CAL_GREGORIAN,date("m",$time),$day,date("Y",$time));
	        // Get day of the week 
	        $nday = jddayofweek($jd,3);
	        
	        return $aNameOfDay[$nday];
		}

		public static function getCalender($format = null) {
			$day = DateUtils::getDayOfWeek();
			if (!strlen($format)) {
				if ($day != "CN") return "Thứ ".$day.", ngày ".date("d")." tháng ".date("m")." năm ".date("Y");
				else return "Chủ nhật, ngày ".date("d")." tháng ".date("m")." năm ".date("Y");
			} else {
				if ($day != "CN") return "Thứ ".$day.", ".date("d")." / ".date("m")." / ".date("Y");
				else return "Chủ nhật, ".date("d")." / ".date("m")." / ".date("Y");
			}
		}
	}
}// end defined
?>
