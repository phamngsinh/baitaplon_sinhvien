<?php
/**
 * @note:	
 * 
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0 
 */
include_once LIB_PATH."ip_files".DS."countries.php";

class AddressUrlCountries {

	/**
	 * @note:	function get name country
	 * @param	String:	ipclient
	 * @return	String:	Name of contry
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	static function getNameCountry($ipclient) {
		$codecountry = AddressUrlCountries::getCodeCountry($ipclient);
		$countries	 = new Countries();
		return $countries->getCountryName($codecountry);
	}

	/**
	 * @note:	function get code country
	 * @param	String:	ipclient
	 * @return	String:	Code of contry
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	static function getCodeCountry($ipclient) {
	    $numbers = preg_split( "/\./", $ipclient);
	    include_once LIB_PATH."ip_files".DS.$numbers[0].".php";
	    $ranges = $ranges;
	    $code	= ($numbers[0] * 16777216) + ($numbers[1] * 65536) + ($numbers[2] * 256) + ($numbers[3]);
	    foreach($ranges as $key => $value){
	        if($key <= $code){
	            if($ranges[$key][0] >= $code){
	            	$country_code = $ranges[$key][1];
	            	break;
	            }
	        }
	    }
	    if ($country_code == "") $country_code="UNKOWN";
	
	    return $country_code;
	}
}
?>
