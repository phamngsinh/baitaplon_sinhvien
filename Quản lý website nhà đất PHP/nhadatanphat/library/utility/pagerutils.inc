<?php
/**
 * @project_name: localframe
 * @file_name: dateutils.inc
 * @descr:
 * 
 * @author 	Thunn - thunn84@gmail.com
 * @version 1.0
 **/ 
if (!defined("PAGERUTILS_INC") ) {
 	define("PAGERUTILS_INC",1);

	class PagerUtils {
		public static function PagerPear($totalrecord, $pagecurrent, $limitpage, $varparam = array()) {
			/****************************Paging pear******************************/
			$params = array(
			    "mode"     		=> "Sliding",
			    "perPage" 		=> $limitpage,
			    "delta"  		=> 4,
			    "expanded "		=> false,
				"append" 		=> true,
				"urlVar"		=> "page",
				"extraVars"		=> $varparam,
				"separator"  	=> " ",
			    "totalItems"    => $totalrecord,
			    "linkClass"		=> "pageLink"
			);
			
			$pager 		= Pager::factory($params);
			$pageset 	= $pager->getPageData();
			$paging		= $pager->getLinks($pagecurrent);
			//$smarty->assign("paging", $paging["all"]);
			
			return $paging;
			/*****************************End paging********************************/
		}
		
		public static function PagerSmarty($page, $totalrecord, $url, $limitrecord = 20, $pagesize = 20, $backtext = "Trang trước", $nexttext = "Trang sau") {
			$paginghtml = "<table border='0' cellpadding='0' cellspacing='0'><tr style='line-height: 18px;'>";
			$startpoint	= (int)($page - $pagesize) < 0 ? 0 : $page - $pagesize;
			$pagecount 	= ($totalrecord % (int)$limitrecord == 0 ) ? (int)($totalrecord/(int)$limitrecord) : (int)($totalrecord/(int)$limitrecord + 1);
			
			if($pagesize >= $pagecount)
				$pagesize = $pagecount;

			if($pagesize > 1){
				if ($page > 1)
					$paginghtml .= "<td style='background: #E5EBF7; border: #C0C9E2 1px solid; padding-left: 5px; padding-right: 5px;'><a href='$url&page=".($page-1)."'>$backtext</a></td>";
				
				$pageLoop = $pagesize + $startpoint;

				if($startpoint + 5 < $page){
					$startpoint += 5;
				}
				
//				if(($startpoint + 5 - $page) < 5){
//					$startpoint = $startpoint - 5;
//				}
//				if($startpoint < 0)
//					$startpoint = 0;
				
				$flgMax = 0;
				$cntLoop = 0;
				
				for ($i = ($startpoint+1); $cntLoop < 10 || $i <= $pageLoop; $i++){
					$cntLoop = (int)$cntLoop + 1;

					if ($page == $i)
						$paginghtml .= "<td style='background: #E5EBF7; border: #C0C9E2 1px solid; padding-left: 5px; padding-right: 5px;'><b>{$i}</b></td>";
					else
						$paginghtml .= "<td style='background: #E5EBF7; border: #C0C9E2 1px solid; padding-left: 5px; padding-right: 5px;'><a href='$url&page=$i'>$i</a></td>";

					if($i == $pagecount){//if($i == $pagecount-1){
						$flgMax = 1;
						break;
					}
				}

				if ($page < $pagecount && $flgMax == 0){
					$paginghtml .= "<td style='background: #E5EBF7; border: #C0C9E2 1px solid; padding-left: 5px; padding-right: 5px;'><a href='$url&page=".($page+1)."'>$nexttext</a></td>";
				}

			}
			$paginghtml.= "</tr></table>";

			return $paginghtml;
		}
	}
}// end defined
?>