<?php
/**
 * @file:	parsefile.php
 * 
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
class ParseFile {
	public  $filename 	= NULL;
	public  $identify 	= TRUE;
	private $values 	= NULL;
	
	public function __construct($filemess = NULL, $identify = TRUE) {
		$this->filename = $filemess;
		$this->identify = $identify;
		$this->parsefile();
	}
	
	private function parsefile() {
		$this->values = parse_ini_file($this->filename, $this->identify);
	}
	
	public function getValue($key = NULL) {
		return (is_null($key) || !isset($this->values[$key])) ? NULL : $this->values[$key];
	}

	public function getValues() {
		return $this->values;
	}
}
?>