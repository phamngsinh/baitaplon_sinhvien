<?php
/**
 * @project_name: localproject
 * @package: package_name
 * @file_name: LogWriter.inc
 * @descr:
 * 
 * @author 	Thunn - thunn84@gmail.com
 * @version 1.0
 **/
 
 if ( !defined("LOGWRITER_INC") ) {
 	define("LOGWRITER_INC",1);
 	
 	class LogWriter
 	{
 		var $fp;
		var $logDir;
		var $date_f;
		var $level;
		
		function LogWriter($filename,$date_f = 1,$level = 0) {
			$this->date_f 	= $date_f;
			$this->level 	= $level;			
			//$this->open($filename);
		}
		
		function open($filename) {
			if ($this->fp) return 0;
			if (!file_exists($filename)) $f = true;
			else $f = false;
			
			$this->fp = fopen($filename,"a");
			if ($f) chmod($filename,0777);
		}
		
		/**
		* write
		**/	
		function write($msg,$level = 0) {
			return "";
			if (defined(WRITELOG) && !(int)WRITELOG) return;
			else {
				if ($this->level < $level) return;
				if ($this->date_f) fwrite($this->fp,date("[Y-m-d H:i:s]"));
				fwrite($this->fp,"$msg\n");
			}
		}
		
		/**
		 * close file
		 **/
		function close(){
			if ($this->fp) fclose($this->fp);
		}				
 	}// end class	
 } // end defined
?>
