<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: catekind.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
class CateKind {
	const general 	= 0;
	const estate	= 1;
	
	/**
	 * @note:	function get all vertical
	 * @return	List	: Vertical
	 * @version 1.0
	 */
	public static function getList($textlang = null) {
		$result = array(
						self::general 	=> array("value"=>self::general, 	"text_en"=>"General",	"text_vn"=>"Danh mục tin tức"),
						self::estate 	=> array("value"=>self::estate, 	"text_en"=>"Estate",	"text_vn"=>"Danh mục tin địa ốc")
		);

		$returnslist = array();
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";
		foreach ($result as $key => $values) {
			$returnslist[$key]["value"] = $values["value"];
			$returnslist[$key]["text"] 	= $values[$text];
		}
		return $returnslist;
	}
	
	/**
	 * @note: 	function get text of vertical value
	 * @param	Int		: value
	 * @param	Int		: textlang	[0: English, 1: Vietname]
	 * 
	 * @return	String	: Text of vertical value
	 * @version 1.0
	 */
	public function getToText($value, $textlang = null) {
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";

		$returnText = "";
		$list = self::getList();
		 
		switch($value){
			case self::general;
				$returnText = $list[self::general][$text];
				break;
			case self::estate;
				$returnText = $list[self::estate][$text];
				break;
			default:
				break;
		}
		return $returnText;
	}
}
?>
