<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: newsgenre.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
class NewsGenre {
	const general 	= 0;
	const geomancy	= 1;
	const capacity	= 2;
	/**
	 * @note:	function get all NewsGenre
	 * @return	List	: NewsGenre
	 * @version 1.0
	 */
	public static function getList($textlang = null) {
		$result = array(
						self::general 	=> array("value"=>'0', "text_en"=>"News general", 	"text_vn"=>"Tin tổng hợp"),
						self::geomancy 	=> array("value"=>'1', "text_en"=>"News geomancy", 	"text_vn"=>"Tin phong thủy"),
						self::capacity 	=> array("value"=>'2', "text_en"=>"News capacity", 	"text_vn"=>"Văn bản pháp luật")
		);

		$returnslist = array();
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";
		foreach ($result as $key => $values) {
			$returnslist[$key]["value"] = $values["value"];
			$returnslist[$key]["text"] 	= $values[$text];
		}
		return $returnslist;
	}

	/**
	 * @note: 	function get text of NewsGenre
	 * @param	Int		: value
	 * @return	String	: Text of NewsGenre value
	 * @version 1.0
	 */
	public function getToText($value, $textlang = null) {
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";

		$returnText = "";
		$list = self::getList();
		 
		switch($value){
			case self::general;
				$returnText = $list[self::general][$text];
				break;
			case self::geomancy;
				$returnText = $list[self::geomancy][$text];
				break;
			case self::capacity;
				$returnText = $list[self::capacity][$text];
				break;	
			default:
				break;
		}
		return $returnText;
	}
}
?>
