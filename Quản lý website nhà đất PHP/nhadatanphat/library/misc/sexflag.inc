<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: sexflag.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
class SexFlag {
	const male 		= 0;
	const female	= 1;
	
	/**
	 * @note:	function get all sex
	 * @return	List	: Sexs
	 * @version 1.0
	 */
	public static function getList($textlang = null) {
		$result = array(
						self::male 		=> array("value"=>self::male, 	"text_en"=>"Male",		"text_vn"=>"Nam giới"),
						self::female 	=> array("value"=>self::female, "text_en"=>"Female",	"text_vn"=>"Nữ giới")
		);

		$returnslist = array();
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";
		foreach ($result as $key => $values) {
			$returnslist[$key]["value"] = $values["value"];
			$returnslist[$key]["text"] 	= $values[$text];
		}
		return $returnslist;
	}
	
	/**
	 * @note: 	function get text of sex value
	 * @param	Int		: value
	 * @param	Int		: textlang	[0: English, 1: Vietname]
	 * 
	 * @return	String	: Text of sex value
	 * @version 1.0
	 */
	public function getToText($value, $textlang = null) {
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";

		$returnText = "";
		$list = self::getList();
		 
		switch($value){
			case self::male;
				$returnText = $list[self::male][$text];
				break;
			case self::female;
				$returnText = $list[self::female][$text];
				break;
			default:
				break;
		}
		return $returnText;
	}
}
?>
