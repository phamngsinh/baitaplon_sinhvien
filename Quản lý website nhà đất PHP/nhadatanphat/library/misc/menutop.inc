<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: horizonta.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
class MenuTop {
	const yes 	= 1;
	const nos	= 0;
	
	/**
	 * @note:	function get all horizonta
	 * @return	List	: Horizonta
	 * @version 1.0
	 */
	public static function getList($textlang = null) {
		$result = array(
						self::yes 	=> array("value"=>self::yes, 	"text_en"=>"Show horizontal",	"text_vn"=>"Có hiển thị"),
						self::nos 	=> array("value"=>self::nos, 	"text_en"=>"Not horizontal",	"text_vn"=>"Không hiển thị")
		);

		$returnslist = array();
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";
		foreach ($result as $key => $values) {
			$returnslist[$key]["value"] = $values["value"];
			$returnslist[$key]["text"] 	= $values[$text];
		}
		return $returnslist;
	}
	
	/**
	 * @note: 	function get text of horizonta value
	 * @param	Int		: value
	 * @param	Int		: textlang	[0: English, 1: Vietname]
	 * 
	 * @return	String	: Text of horizonta value
	 * @version 1.0
	 */
	public function getToText($value, $textlang = null) {
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";

		$returnText = "";
		$list = self::getList();
		 
		switch($value){
			case self::yes;
				$returnText = $list[self::yes][$text];
				break;
			case self::nos;
				$returnText = $list[self::nos][$text];
				break;
			default:
				break;
		}
		return $returnText;
	}
}
?>
