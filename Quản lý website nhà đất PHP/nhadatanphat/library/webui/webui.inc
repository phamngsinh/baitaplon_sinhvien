<?php
class WebUI {
	/**
	 * @note:	function make date time controls
	 */
	public static function makeDateTime() {
		$year  	= date("Y");
		$month 	= date("m");
		$day	= date("d");
		 
	}
	
	/*****************************************************************
	 * Make option control
	 * @param $from
	 * @param $to
	 * @param $default
	 * @param $preline
	 ****************************************************************/
	public static function makeOption($from, $to, $default, $preline='') {
		$retstr = "";
		for($i = $from ; $i <= $to ; $i++) {
			if($default == $i){
				$retstr .= $preline."<option value=$i selected>$i\n";
			}else{
				$retstr .= $preline."<option value=$i>$i\n";
			}
		}
		return($retstr);
	}

	/**
	 * Make option list
	 */
	public static function makeOptList($list, $defval = null, $tab = '') {
		$retstr = "";
		reset($list);
		while( list($key,$val) = each($list) )
		{
			$value = $key; //sprintf("%02d", $key);
			$retstr .= $tab;
			if( $defval == $key )
			{
				$retstr .= "<option value=\"$value\" selected>$val</option>\n";
			}
			else
			{
				$retstr .= "<option value=\"$value\">$val</option>\n";
			}
		}
		reset($list);
		return( $retstr);
	}

	
    public static function makeOptListbyID($list,$index=0) {
		$retstr = "";
		reset($list);
        $i = 0;
		while( list($key,$val) = each($list) )
		{
            $retstr .= "";
            if($i == $index)
            {
                $retstr .= "<option value=\"$key\" selected>$val</option>\n";
            }
            else
            {
                $retstr .= "<option value=\"$key\">$val</option>\n";
            }
            $i = $i++;
		}
		reset($list);
		return( $retstr);
	}

	public static function makeRadio($name,$value,$checkVal) {
		$checked = ( $value==$checkVal ? "checked" : "");
		$tag = "<input name=\"".$name."\" type=\"radio\" ";
		$tag .= "value=\"".$value."\" ".$checked.">\n";
		return( $tag );
	}

    public static function tep_draw_hidden_field($name, $value = '', $parameters = '') {
        $field = '<input type="hidden" name="' . DBescape($name) . '"';

        if (!empty($value)) {
            $field .= ' value="' . UtilsClass::DBescape($value) . '"';
        } elseif (isset($GLOBALS[$name])) {
            $field .= ' value="' . UtilsClass::DBescape(stripslashes($GLOBALS[$name])) . '"';
        }

        if (!empty($parameters)) $field .= ' ' . $parameters;

            $field .= '>';

        return $field;
    }
    
    // Output a form input field
    public static function tep_draw_input_field($name, $value = '', $parameters = '', $type = 'text', $reinsert_value = true) {
        $field = '<input type="' . UtilsClass::DBescape($type) . '" name="' . DBescape($name) . '"';

        if ( (isset($value)) && ($reinsert_value == true) ) {
            $field .= ' value="' . UtilsClass::DBescape(stripslashes($value)) . '"';
        } elseif (!empty($value)) {
            $field .= ' value="' . UtilsClass::DBescape($value) . '"';
        }

        if (!empty($parameters)) $field .= ' ' . $parameters;

        $field .= '>';

        return $field;
    }
    
    // Output a form pull down menu
    public static function tep_draw_pull_down_menu($name, $arr_values, $default = '', $parameters = '', $required = false) {
        $field = '<select name="' . UtilsClass::DBescape($name) . '"';

        if (!empty($parameters)) $field .= ' ' . $parameters;

        $field .= '>';

        //if (empty($default) && isset($GLOBALS[$name])) $default = stripslashes($GLOBALS[$name]);

        for ($i=0, $n=sizeof($arr_values); $i<$n; $i++) {
            $field .= '<option value="' . UtilsClass::DBescape($arr_values[$i]['id']) . '"';
            if ($default == $arr_values[$i]['id']) {
            $field .= ' SELECTED';
            }

            $field .= '>' . UtilsClass::DBescape($arr_values[$i]['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;')) . '</option>';
        }
        $field .= '</select>';

        if ($required == true) $field .= 'TEXT_FIELD_REQUIRED';

        return $field;
    }
    
    /**
    * Return a string check box
    */
    public static function makeCheckbox($name,$value,$checkVal)	{
        
		if(is_array($checkVal)){
            $checked = ( (in_array($value, $checkVal)) ? "checked" : "");
        } 
        else $checked = ( $value==$checkVal ? "checked" : "");
        
		$tag = "<input name=\"".$name."\" type=\"checkbox\" ";
		$tag .= "value=\"".$value."\" ".$checked.">\n";
		return( $tag );
	}
	
 } // end class
?>
