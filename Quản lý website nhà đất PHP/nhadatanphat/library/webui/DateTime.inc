<?php
/*
 * Created on Jul 2, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
if (!defined("DATETIME_INC") ) {
  define("DATETIME_INC",1);
class DateTimes {
	var $viewstate 	= array();
	var $name 		= null;
	
	public function __construct($_name = "datetime", $_startyear = null, $_endyear = null, $_dateformat = null, $_showtime = FALSE, $_class = null) {
		$this->name = $_name;
		$this->setStartYear($_startyear);
		$this->setEndYear($_endyear);
		$this->setDateFormat($_dateformat);
		$this->setShowTime($_showtime);
		$this->setClass($_class);
		$this->setStyle();
		$this->update();
	}
	
	public function setStyle($style = null) {
		$this->viewstate["style"] = $style;
	}
	
	public function setStartYear($_startyear = null) {
		if (empty($_startyear)) $this->viewstate["year"]["start"] = date("Y");
		else $this->viewstate["year"]["start"] = $_startyear;
	}
	
	public function setEndYear($_endyear = null) {
		if (empty($_endyear)) $this->viewstate["year"]["end"] = date("Y");
		else $this->viewstate["year"]["end"] = $_endyear;
	}
	
	public function setDateFormat($_dateformat = null) {
		$this->viewstate["dateformat"] = str_replace(array('h','H','i','I','s','S'),array('hh','hh','ii','II','ss','SS'), $_dateformat);
	}
	
	public function setShowTime($_showtime = FALSE) {
		$this->viewstate["showtime"] = $_showtime;
	}
	
	public function setClass($_class = null) {
		$this->viewstate["class"] = $_class;
	}
	
	public function setYear($_year = null) {
		$this->viewstate["year"]["select"] = $_year;
	}
	
	public function setMonth($_month = null) {
		$this->viewstate["month"]["select"] = $_month;
	}

	public function setDay($_day = null) {
		$this->viewstate["day"]["select"] = $_day;
	}

	public function setHour($_hour = null) {
		$this->viewstate["hour"]["select"] = $_hour;
	}

	public function setMinute($_minute = null) {
		$this->viewstate["minute"]["select"] = $_minute;
	}

	public function setSecond($_second = null) {
		$this->viewstate["second"]["select"] = $_second;
	}

	/**
	 * @note:	The function set empty datetime value
	 * @param	String:	_value
	 * @param	String:	_text
	 * 
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	public function setEmpty($_value = "", $_text = "") {
		$this->viewstate["empty"]["status"] = TRUE;
		$this->viewstate["empty"]["value"] 	= $_value;
		$this->viewstate["empty"]["text"] 	= $_text;
	}
	
	/**
	 * @note:	The function set selected datetime value
	 * @param	String:	_currentvalue
	 * 
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	public function setSelected($_currentvalue) {
		if (empty($_currentvalue)) return false;
		$values = explode(" ", $_currentvalue);
		$dates	= $values[0];
		$times	= isset($values[1]) ? $values[1] : "00:00:00";
		$dates 	= split('[/.-]', $dates);
		$times 	= split('[:]', 	 $times);
		$dateformat = str_replace(array('-','/','.'),array('','',''), $this->viewstate["dateformat"]);
		if (strtoupper($dateformat) == "YMD") {
			$this->setYear($dates[0]);
			$this->setMonth($dates[1]);
			$this->setDay($dates[2]);
		} else if (strtoupper($dateformat) == "DMY") {
			$this->setYear($dates[2]);
			$this->setMonth($dates[1]);
			$this->setDay($dates[0]);
		} else if (strtoupper($dateformat) == "MDY") {
			$this->setYear($dates[2]);
			$this->setMonth($dates[0]);
			$this->setDay($dates[1]);
		} else {
			
		}
		$this->setHour($times[0]);
		$this->setMinute($times[1]);
		$this->setSecond($times[2]);
	}
	
	/**
	 * @note:	The function get year value
	 * 
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	public function getYear() {
		return isset($_POST[$this->name."_y"]) ? $_POST[$this->name."_y"] : null;
	}
	
	/**
	 * @note:	The function get month value
	 * 
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	public function getMonth() {
		return isset($_POST[$this->name."_m"]) ? $_POST[$this->name."_m"] : null;
	}

	/**
	 * @note:	The function get day value
	 * 
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	public function getDay() {
		return isset($_POST[$this->name."_d"]) ? $_POST[$this->name."_d"] : null;
	}

	/**
	 * @note:	The function get hour value
	 * 
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	public function getHour() {
		return isset($_POST[$this->name."_h"]) ? $_POST[$this->name."_h"] : null;
	}

	/**
	 * @note:	The function get minute value
	 * 
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	public function getMinute() {
		return isset($_POST[$this->name."_i"]) ? $_POST[$this->name."_i"] : null;
	}

	/**
	 * @note:	The function get second value
	 * 
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	public function getSecond() {
		return isset($_POST[$this->name."_s"]) ? $_POST[$this->name."_s"] : null;
	}

	/**
	 * @note:	The function get datetime value
	 * @param	String:	_dateformat
	 * 
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	public function getDateTime($_dateformat = null) {
		$date	= null;
		$_year	= $this->getYear();
		$_month = $this->getMonth();
		$_day 	= $this->getDay();
		$_hour 	= $this->getHour();
		$_minute= $this->getMinute();
		$_second= $this->getSecond();
		if ($this->viewstate["showtime"]) $_time = (($_hour > 9) ? $_hour : "0".$_hour).":".(($_minute > 9) ? $_minute : "0".$_minute).":".(($_second > 9) ? $_second : "0".$_second);
		else $_time = null;

		if (empty($_dateformat)) $_dateformat = $this->viewstate["dateformat"];
		$dateformat = str_replace(array('-','/','.'),array('','',''), $_dateformat);
		
		if (strtoupper($dateformat) == "YMD") 		$date = $_year."-".(($_month > 9) ? $_month : "0".$_month)."-".(($_day > 9) ? $_day : "0".$_day)." ".$_time;
		else if (strtoupper($dateformat) == "DMY") 	$date = (($_day > 9) ? $_day : "0".$_day)."-".(($_month > 9) ? $_month : "0".$_month)."-".$_year." ".$_time;
		else if (strtoupper($dateformat) == "MDY") 	$date = (($_month > 9) ? $_month : "0".$_month)."-".(($_day > 9) ? $_day : "0".$_day)."-".$_year." ".$_time;
		else $date = $_year."-".$_month."-".$_day." ".$_time;

		return !empty($_year) ? trim($date) : null;
	}
	
	/**
	 * @note:	The function update selected
	 * 
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	public function update() {
		$this->setYear($this->getYear());
		$this->setMonth($this->getMonth());
		$this->setDay($this->getDay());
		$this->setHour($this->getHour());
		$this->setMinute($this->getMinute());
		$this->setSecond($this->getSecond());
	}
	
	/**
	 * @note:	The function render html
	 * 
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	public function render() {
		$dhtml  = null;
		$_time  = null;

		$_year  = "<select name='".$this->name."_y' id='".$this->name."_y' class='".$this->viewstate["class"]."' style='".$this->viewstate["style"]."'>";
		if (isset($this->viewstate["empty"]["status"])) $_year .= "<option value='".$this->viewstate["empty"]["value"]."'>".$this->viewstate["empty"]["text"]."</option>";
		for ($i = $this->viewstate["year"]["start"]; $i <= $this->viewstate["year"]["end"]; $i++) {
			$_year .= "<option value='".$i."' ".(($this->viewstate["year"]["select"] == $i) ? "selected='selected'" : null).">".$i."</option>";
		}
		$_year .= "</select>";

		$_month = "<select name='".$this->name."_m' id='".$this->name."_m' class='".$this->viewstate["class"]."' style='".$this->viewstate["style"]."'>";
		if (isset($this->viewstate["empty"]["status"])) $_month .= "<option value='".$this->viewstate["empty"]["value"]."'>".$this->viewstate["empty"]["text"]."</option>";
		for ($i = 1; $i <= 12; $i++) {
			$_month .= "<option value='".$i."' ".(($this->viewstate["month"]["select"] == $i) ? "selected='selected'" : null).">".(($i > 9) ? $i : "0".$i)."</option>";
		}
		$_month .= "</select>";

		$_day = "<select name='".$this->name."_d' id='".$this->name."_d' class='".$this->viewstate["class"]."' style='".$this->viewstate["style"]."'>";
		if (isset($this->viewstate["empty"]["status"])) $_day .= "<option value='".$this->viewstate["empty"]["value"]."'>".$this->viewstate["empty"]["text"]."</option>";
		for ($i = 1; $i <= 31; $i++) {
			$_day .= "<option value='".$i."' ".(($this->viewstate["day"]["select"] == $i) ? "selected='selected'" : null).">".(($i > 9) ? $i : "0".$i)."</option>";
		}
		$_day .= "</select>";
		
		if (!empty($this->viewstate["showtime"])) {
			$_time .= "<select name='".$this->name."_h' id='".$this->name."_h' class='".$this->viewstate["class"]."' style='".$this->viewstate["style"]."'>";
			if (isset($this->viewstate["empty"]["status"])) $_time .= "<option value='".$this->viewstate["empty"]["value"]."'>".$this->viewstate["empty"]["text"]."</option>";
			for ($i = 0; $i <= 23; $i++) {
				$_time .= "<option value='".$i."' ".(($this->viewstate["hour"]["select"] == $i && strlen($this->viewstate["hour"]["select"])) ? "selected='selected'" : null).">".(($i > 9) ? $i : "0".$i)."</option>";
			}
			$_time .= "</select>";
			$_time .= "&nbsp;<select name='".$this->name."_i' id='".$this->name."_i' class='".$this->viewstate["class"]."' style='".$this->viewstate["style"]."'>";
			if (isset($this->viewstate["empty"]["status"])) $_time .= "<option value='".$this->viewstate["empty"]["value"]."'>".$this->viewstate["empty"]["text"]."</option>";
			for ($i = 0; $i <= 59; $i++) {
				$_time .= "<option value='".$i."' ".(($this->viewstate["minute"]["select"] == $i && strlen($this->viewstate["minute"]["select"])) ? "selected='selected'" : null).">".(($i > 9) ? $i : "0".$i)."</option>";
			}
			$_time .= "</select>";
			$_time .= "&nbsp;<select name='".$this->name."_s' id='".$this->name."_s' class='".$this->viewstate["class"]."' style='".$this->viewstate["style"]."'>";
			if (isset($this->viewstate["empty"]["status"])) $_time .= "<option value='".$this->viewstate["empty"]["value"]."'>".$this->viewstate["empty"]["text"]."</option>";
			for ($i = 0; $i <= 59; $i++) {
				$_time .= "<option value='".$i."' ".(($this->viewstate["second"]["select"] == $i && strlen($this->viewstate["second"]["select"])) ? "selected='selected'" : null).">".(($i > 9) ? $i : "0".$i)."</option>";
			}
			$_time .= "</select>";
		}
		$dateformat = str_replace(array('-','/','.'),array('','',''), $this->viewstate["dateformat"]);

		if (strtoupper($dateformat) == "YMD") 		$dhtml = $_year."&nbsp;".$_month."&nbsp;".$_day."&nbsp;".$_time;
		else if (strtoupper($dateformat) == "DMY") 	$dhtml = $_day."&nbsp;".$_month."&nbsp;".$_year."&nbsp;".$_time;
		else if (strtoupper($dateformat) == "MDY") 	$dhtml = $_month."&nbsp;".$_day."&nbsp;".$_year."&nbsp;".$_time;
		else $dhtml = $_year."&nbsp;".$_month."&nbsp;".$_day."&nbsp;".$_time;

		return $dhtml;
	}

	/**
	 * @note:	The function build html
	 * 
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	public function buildhtml() {
		return $this->render();
	}
}
}
?>
