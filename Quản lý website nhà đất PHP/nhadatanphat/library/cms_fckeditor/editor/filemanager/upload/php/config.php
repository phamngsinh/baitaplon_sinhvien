<?php
$dir_images = $_SERVER['DOCUMENT_ROOT'].'/sportsifu/images/';
$url_image = 'http://'.$_SERVER['HTTP_HOST'].'/sportsifu/images/';

global $Config ;
$Config['Enabled'] = true ;
$Config['UseFileType'] = false ;
$Config['UserFilesPath'] = $url_image;
$Config['UserFilesAbsolutePath'] = $dir_images;
$Config['ForceSingleExtension'] = true ;

$Config['AllowedExtensions']['File']	= array() ;
$Config['DeniedExtensions']['File']		= array('html','htm','php','php2','php3','php4','php5','phtml','pwml','inc','asp','aspx','ascx','jsp','cfm','cfc','pl','bat','exe','com','dll','vbs','js','reg','cgi','htaccess','asis','sh','shtml','shtm','phtm') ;

$Config['AllowedExtensions']['Image']	= array('jpg','gif','jpeg','png') ;
$Config['DeniedExtensions']['Image']	= array() ;

$Config['AllowedExtensions']['Flash']	= array('swf','fla') ;
$Config['DeniedExtensions']['Flash']	= array() ;

?>