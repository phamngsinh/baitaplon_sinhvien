<?php
/*************************************** Define site ****************************************/
	define("SITE_NAME", "");
	//define("ROOT_PATH", $_SERVER["DOCUMENT_ROOT"].DS."/home/nhadatan/public_html".DS.SITE_NAME.DS);
	define("ROOT_PATH", "/home/nhadatan/public_html".DS);
	//define("ROOT_PATH", $_SERVER["DOCUMENT_ROOT"].DS."nhadatvm".DS);
	if (ACTION == "index") Define("TPL_TYPE", 	".html");
	else Define("TPL_TYPE", 	".tpl");
	/********************************************************************************************/
	
	/*************************************** FOR EU ****************************************/
	//Define("URL_FRONTEND", 			"http://localhost/estate/");
	Define("URL_FRONTEND", 				"http://localhost/nhadatvm/");
	Define("SRC_FRONTEND", 				"frontend");
	Define("FRONTEND_CONF_PATH", 		ROOT_PATH.DS.SRC_FRONTEND.DS."configs".DS);
	Define("FRONTEND_PAGE_PATH", 		ROOT_PATH.DS.SRC_FRONTEND.DS."pages".DS);
	Define("FRONTEND_TEMPLATE_PATH", 	ROOT_PATH.DS.SRC_FRONTEND.DS."templates".DS);
	Define("FRONTEND_TEMPLATE_C_PATH", 	ROOT_PATH.DS.SRC_FRONTEND.DS."templates_c".DS);
	Define("FRONTEND_TEMPLATE_ERR", 	FRONTEND_TEMPLATE_PATH."errors".DS."errors.".TPL_TYPE);
	Define("FRONTEND_LOG_PATH", 		ROOT_PATH.DS.SRC_FRONTEND.DS."logs".DS);	
	
	Define("FRONTEND_JS_MENU", 			ROOT_PATH.DS.SRC_FRONTEND.DS."js_menu".DS);
	/***************************************************************************************/
	
	/************************************** FOR ADMIN **************************************/
	Define("SRC_BACKEND", 				"backend");
	Define("BACKEND_CONF_PATH", 		ROOT_PATH.SRC_BACKEND.DS."configs");
	Define("BACKEND_PAGE_PATH", 		ROOT_PATH.DS.SRC_BACKEND.DS."pages".DS);
	Define("BACKEND_TEMPLATE_PATH", 	ROOT_PATH.DS.SRC_BACKEND.DS."templates".DS);
	Define("BACKEND_TEMPLATE_C_PATH", 	ROOT_PATH.DS.SRC_BACKEND.DS."templates_c".DS);
	Define("BACKEND_TEMPLATE_ERR", 		BACKEND_TEMPLATE_PATH."errors".DS."errors".TPL_TYPE);
	Define("BACKEND_LOG_PATH", 			ROOT_PATH.DS.SRC_BACKEND.DS."logs".DS);
	/***************************************************************************************/
	
	/*************************************** FOR GENERAL ***********************************/
	Define("SRC_DATA", 		"data");
	Define("CACHE_PATH", 	ROOT_PATH.DS."cache".DS);
	Define("LIB_PATH", 		ROOT_PATH."library".DS);
	Define("DATA_PATH", 	ROOT_PATH.SRC_DATA.DS);
	Define("IMG_PATH", 		DATA_PATH."images".DS);
	Define("FLV_PATH", 		DATA_PATH."flashfile".DS);
	Define("AUD_PATH", 		DATA_PATH."audios".DS);
	Define("PDF_PATH", 		DATA_PATH."filepdf".DS);
	Define("LOGO_PATH", 	DATA_PATH."logo".DS);
	Define("THUMB_IMG_SIZE", 	85);
	
	Define("SITE_ROOT_URL", "http://".$_SERVER["HTTP_HOST"]."/".SITE_NAME."/");	
	Define("FRONTEND_URL",  "http://".$_SERVER["HTTP_HOST"]."/".SITE_NAME."/frontend/");	
	Define("IMG_URL", 		"http://".$_SERVER["HTTP_HOST"]."/".SITE_NAME."/data/images/");
	Define("AUD_URL", 		"http://".$_SERVER["HTTP_HOST"]."/".SITE_NAME."/data/audios/");
	Define("PDF_URL", 		"http://".$_SERVER["HTTP_HOST"]."/".SITE_NAME."/data/filepdf/");
	Define("LOGO_URL", 		"http://".$_SERVER["HTTP_HOST"]."/".SITE_NAME."/data/logo/");
	
	define("PUBLIC_FILE_PATH", DATA_PATH."public".DS);
	define("PUBLIC_FILE_URL", 	"http://".$_SERVER["HTTP_HOST"]."/".SITE_NAME."/data/public/");
	define("ConstUserFilesAbsolutePath", ROOT_PATH.DS.DS."data".DS.DS."public");
	
	define("CONFIG_MAIL", 	"Thunn84@gmail.com");
	/***************************************************************************************/
?>