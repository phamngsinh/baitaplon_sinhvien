<?php
	$library_dir = dirname(__FILE__);
	
	/*************** Config ***************/
	require_once $library_dir . DS."config.inc";
	/*************** Connect to DB ***************/
	require_once $library_dir . DS."connection.inc";
	/*************** Constant ***************/
	require_once $library_dir . DS."constants.inc";
	/*************** Init smarty ***************/
	require_once $library_dir . DS."Smarty".DS."Smarty.class.php";
	/*************** PEAR ***************/
	require_once $library_dir . DS."PEAR".DS."PEAR.php";
	/*************** DB of PEAR ***************/
	require_once $library_dir . DS."PEAR".DS."DB.php";
	/*************** ADODB ***************/
	require_once $library_dir . DS."PEAR".DS."adodb".DS."dbpear.inc";
	/*************** PAGER ***************/
	require_once $library_dir . DS."PEAR".DS."Pager".DS."Pager.php";
	/*************** FCK Editor ***************/
	//require_once $library_dir . DS."fckeditor".DS."FCKeditor.class.php";
	
	require_once $library_dir . DS."cms_fckeditor".DS."FCKeditor.class.php";

	
	/*************** Validate ***************/
	require_once $library_dir . DS."validate".DS."@validate.php";
	/*************** Utility ***************/
	require_once $library_dir . DS."utility".DS."fileUpload.inc";
	require_once $library_dir . DS."utility".DS."mailSend.inc";
	require_once $library_dir . DS."utility".DS."util.inc";
	require_once $library_dir . DS."utility".DS."logWriter.inc";
	require_once $library_dir . DS."utility".DS."parsefile.inc";
	require_once $library_dir . DS."utility".DS."fileutils.inc";
	require_once $library_dir . DS."utility".DS."dateutils.inc";
	require_once $library_dir . DS."utility".DS."pagerutils.inc";
	/*************** Misc file ***************/
	require_once $library_dir . DS."misc".DS."sexflag.inc";
	require_once $library_dir . DS."misc".DS."status.inc";
	require_once $library_dir . DS."misc".DS."position.inc";
	require_once $library_dir . DS."misc".DS."newsgenre.inc";
	require_once $library_dir . DS."misc".DS."supportgenre.inc";
	require_once $library_dir . DS."misc".DS."catekind.inc";
	require_once $library_dir . DS."misc".DS."menutop.inc";
	require_once $library_dir . DS."misc".DS."menubottom.inc";
	require_once $library_dir . DS."misc".DS."vertical.inc";
	require_once $library_dir . DS."misc".DS."direction.inc";
	require_once $library_dir . DS."misc".DS."estategenre.inc";
	require_once $library_dir . DS."misc".DS."userRole.inc";
	/*************** web ui class ***************/
	require_once $library_dir . DS."webui".DS."DateTime.inc";
	
	unset($library_dir);
?>