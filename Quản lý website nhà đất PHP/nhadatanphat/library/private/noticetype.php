<?php
/**
 * @note:	file define array language
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
if (!defined("ARRAY_NOTICE_TYPE_INC") ) {
	Define("ARRAY_NOTICE_TYPE_INC", "TRUE");
	/**
	 * @note:	array list notice type
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	$languagelist 		= array(
								1=> array("value" => 1, "lang_flag" => "UK.gif", "lang_name" => "Cần mua", "file_inform" => "lang_en"), 
								2=> array("lang_id" => 2, "lang_flag" => "VN.gif", "lang_name" => "Cần bán", "file_inform" => "lang_vn"),
								3=> array("lang_id" => 3, "lang_flag" => "JP.gif", "lang_name" => "Cần thuê", "file_inform" => "lang_jp"),
								4=> array("lang_id" => 3, "lang_flag" => "JP.gif", "lang_name" => "Cho thuê", "file_inform" => "lang_jp")
						   );
}						   
?>
