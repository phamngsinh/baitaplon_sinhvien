<?php
// $Id: pager_post_tests.php,v 1.1 2008/06/19 14:32:37 Thunn Exp $

require_once 'simple_include.php';
require_once 'pager_include.php';

$test = &new GroupTest('Pager POST tests');
$test->addTestFile('pager_post_test.php');
$test->addTestFile('pager_post_test_simple.php');
exit ($test->run(new HTMLReporter()) ? 0 : 1);

?>