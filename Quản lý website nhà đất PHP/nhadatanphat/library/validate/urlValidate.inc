<?php
/**
 * @note:	
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
class urlValidate extends normalizeValidate {
	
	/**
	 * @note:	
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	function __construct()
	{
		
	}

	/**
	 * @note:	
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	public function execute($value,$setting)
	{
		$boolean = $this->normalize($value,"^http:\/\/[0-9a-zA-Z_-]+\.[0-9a-zA-Z_-]+[0-9a-zA-Z_\?#=/\.\-]*$");
		if($boolean){
			return;
		} else {
			if($setting['normalizeerrormsg']) {
				$error = $setting['normalizeerrormsg'];
			} else {
				$error = "%name%���œ�͂��Ă��������B";
			}
			return $error;
		}
	}
}
?>