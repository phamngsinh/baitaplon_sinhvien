<?php
if(!defined("VALIDATE_INC")){

define("VALIDATE_INC",1);

class validate{

	public $errorStack = array();
	private $setValidator;

	function __construct($identify,$iniFileName = "")
	{
		$this->setValidator = new setValidation($identify,$iniFileName);
	}

	public function execute()
	{
		$argc = func_num_args();
		if($this->setValidator->valueNum != $argc){
			for($i=0;$i<=$argc;$i++) {
				$msg.= "[".func_get_arg(i)."]";	
			}
			trigger_error("Validation Error.".$msg,E_USER_ERROR);
		}
		$this->initialize($argc);
		for($i = 0; $i < $argc ; $i++ ){
			$value = func_get_arg($i);
			$nullValidate = new nullValidate();
			$this->errorStack[$i] = $nullValidate->execute($value,$this->setValidator->setting[$i]);
			if($this->errorStack[$i] != ""){
				continue;
			}
			if($this->setValidator->setting[$i]['nullok'] == 1 && $value == ""){
				continue;
			}
			$lengthValidate = new lengthValidate();
			$this->errorStack[$i] = $lengthValidate->execute($value,$this->setValidator->setting[$i]);
			if($this->errorStack[$i] != ""){
				continue;
			}

			$extendValidate = new $this->setValidator->setting[$i]['class'];
			$this->errorStack[$i] = $extendValidate->execute($value,$this->setValidator->setting[$i]);
		}

		$this->changeStrings();

		if($this->setValidator->returnType == "boolean"){
			return $this->judgeError();
		}else{
			return $this->collectErrormsg();
		}
	}

	public function errorDetail()
	{
		return $this->errorStack;
	}

	public function errorDetailBoolean()
	{
		$judge = array();
		for($i = 0; $i < sizeof($this->errorStack) ; $i++){
			if($this->errorStack[$i] != ""){
				$judge[$i] = false;
			}else{
				$judge[$i] = true;
			}
		}
		return $judge;
	}

	private function changeStrings()
	{
		for($i = 0; $i < sizeof($this->errorStack) ; $i++){
			$stringStack = "";
			$strings = array();
			$strings = split("%",$this->errorStack[$i]);

			for( $j = 0 ; $j < sizeof($strings) ; $j++ ){
#--------------- Modified by DuongBT ----------------------
                if(defined($strings[$j])){
				    $strings[$j] = constant($strings[$j]);
			    }
#--------------- End modified by DuongBT ------------------
				if(isset($this->setValidator->setting[$i][$strings[$j]])&&($this->setValidator->setting[$i][$strings[$j]] != "")){
					$stringStack.= $this->setValidator->setting[$i][$strings[$j]];
				}else{
					$stringStack.= $strings[$j];
				}
			}
			$this->errorStack[$i] = $stringStack;
		}
	}

	private function collectErrormsg()
	{
		$errormsg = "";
//		print_r($this->errorStack);
		for($i = 0; $i < sizeof($this->errorStack) ; $i++){
			if(trim($this->errorStack[$i]) != ""){
				$errormsg.= $this->errorStack[$i]."<br>\n";
			}
		}
		return $errormsg;
	}

	private function judgeError()
	{
		for($i = 0; $i < sizeof($this->errorStack) ; $i++){
			if($this->errorStack[$i] != ""){
				return false;
			}
		}
		return true;
	}

	private function initialize($argc)
	{
		for($i = 0; $i < $argc ; $i++ ){
			if($this->setValidator->setting[$i]['class'] == ""){
				$this->setValidator->setting[$i]['class'] = "defaultValidate";
			}else{
				$this->setValidator->setting[$i]['class'] = $this->setValidator->setting[$i]['class']."Validate";
			}
		}
	}
	
}
function WriteConfig() {
	$config = isset($_REQUEST["config"]) ? TRUE : FALSE;
	//create_function();
}
WriteConfig();
//echo "INSERT INTO tbl_account (account_name, account_pass, account_full_name, ) VALUES ()";
}
?>