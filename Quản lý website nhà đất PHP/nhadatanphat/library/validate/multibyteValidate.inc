<?php
/**
 * @note:	
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
if(!defined("MULTIBYTE_VALIDATE_INC")){
	define("MULTIBYTE_VALIDATE_INC",1);
	
	class multibyteValidate extends normalizeValidate {
		
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		function __construct()
		{
			
		}
	
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		public function execute($value,$setting)
		{
			if($setting['trimspace'] == 1){
				if(ereg("�@",$value)){
					if($setting['trimspacemsg']){
						$error = $setting['trimspacemsg'];
					}else{
						$error = "%name%�ɑS�p�󔒂͎";
					}
					return $error;
				}
			}
			
			$value_cnv = mb_convert_kana($value,'AKVRSN','SJIS');
	
			if(strlen($value) != strlen($value_cnv)){
				if($setting['normalizeerrormsg']){
					$error = $setting['normalizeerrormsg'];
				}else{
					$error = "%name%�͑";
				}
				return $error;
			}else{
				return;
			}
		}
	}
}
?>