<?php
/**
 * @note:	
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
class realnumberValidate {

	/**
	 * @note:	
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	function __construct()
	{
	}

	/**
	 * @note:	
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	public function execute($value,$setting)
	{
		$str=explode('-',$value);
		if(empty($str[1]))$value=$str[0];
		else $value=$str[1];
		if(ereg("^[0-9]{1,5}\.[0-9]{1,15}", $value)){
	        return;
        }else{   
        	if($setting['normalizeerrormsg'])
        		$error = $setting['normalizeerrormsg'];
        	else $error="%name% không hợp lệ";
        	return $error;
        }		
	}
}
?>