<?php
/**
 * @note:	
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
if(!defined("NORMALIZE_VALIDATE_INC")){
	define("NORMALIZE_VALIDATE_INC",1);
	
	class normalizeValidate{
	
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		function __construct()
		{
			
		}
	
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		public function execute($value,$setting)
		{
			$boolean = $this->normalize($value,$setting['normalize']);
			if($boolean){
				return;
			}else{
				if(isset($setting['normalizeerrormsg']) && $setting['normalizeerrormsg']!=""){
					$error = $setting['normalizeerrormsg'];
				}else{
					$error = "%name%";
				}
				return $error;
			}
		}
	
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		protected function normalize($value,$pattern)
		{
	        if(ereg($pattern, $value)){
		        return true;
	        }else{
	        	return false;
	        }
		}
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		protected function mb_normalize($value,$pattern)
		{
	        if(mb_ereg($pattern, $value)){
		        return true;
	        }else{
	        	return false;
	        }
		}
	}
}
?>