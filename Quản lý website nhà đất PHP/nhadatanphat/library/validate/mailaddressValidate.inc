<?php
/**
 * @note:	
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
if(!defined("MAILADDRESS_VALIDATE_INC")){
	define("MAILADDRESS_VALIDATE_INC",1);
	
	class mailaddressValidate extends normalizeValidate {
		
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		function __construct() {
		}
	
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		public function execute($value,$setting)
		{
			$boolean = $this->normalize($value,"^[0-9A-Za-z][0-9a-zA-Z_\\.\\?\\/\\-]+@[0-9A-Za-z][0-9a-zA-Z_\\\\-]+\.[0-9A-Za-z][0-9a-zA-Z_\\-]");
			if($boolean){
				return;
			}else{
				if(isset($setting['normalizeerrormsg']) && $setting['normalizeerrormsg']!=""){
					return $setting['normalizeerrormsg'];
				}else{
					return "Địa chỉ E-Mail không hợp lệ";
				}
			}
		}
	}
}
?>