<?php
/**
 * @note:	
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
if(!defined("BRITHDAY_VALIDATE_INC")){
define("BRITHDAY_VALIDATE_INC",1);
	
	class birthdayValidate {
		
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		function __construct() {
		}
	
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		public function execute($value,$setting) {
		
			$date = DateUtils::splitDate($value);

			try {
				if(empty($date) || !checkdate($date['month'],$date['day'],$date['year'])){
					if($setting['dateerrormsg']){
						$errormsg = $setting['dateerrormsg'];
					}else{
						$errormsg = "%name%";
					}
					return $errormsg;
				} else {
					if ($date['year'].$date['month'].$date['day'] >date("Ymd"))
						return $setting['brithdayerrormsg'];
				}
				return;
			} catch (Exception $ex) {
				if($setting['dateerrormsg']){
					$errormsg = $setting['dateerrormsg'];
				}else{
					$errormsg = "%name%";
				}
				return $errormsg;
			}
		}
	}
}
?>