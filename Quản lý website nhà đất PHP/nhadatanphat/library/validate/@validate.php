<?php
	/*****************************************************************
	 * @project_name: localframe
	 * @file_name: #validate.php
	 * @descr:
	 * 
	 * @author 	Khuong Van Chien - khuongchien@gmail.com
	 * @version 1.0
	 *****************************************************************/
	
	require_once 'validate.inc';
	require_once 'setValidation.inc';
	
	require_once 'normalizeValidate.inc';
	require_once 'alphabetValidate.inc';
	require_once 'alphaNumricValidate.inc';
	require_once 'birthdayValidate.inc';
	require_once 'datetimeValidate.inc';
	require_once 'defaultValidate.inc';
	require_once 'lengthValidate.inc';
	require_once 'mailaddressValidate.inc';
	require_once 'multibyteValidate.inc';
	require_once 'nullValidate.inc';
	require_once 'numberValidate.inc';
	require_once 'numricValidate.inc';
	require_once 'passwordValidate.inc';
	require_once 'urlValidate.inc';
	require_once 'zipValidate.inc';
	require_once 'telValidate.inc';
?>