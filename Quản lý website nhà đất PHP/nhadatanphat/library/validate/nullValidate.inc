<?php
/**
 * @note:	
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
if(!defined("NULL_VALIDATE_INC")){
	define("NULL_VALIDATE_INC",1);

	class nullValidate{
	
		private $nullok;
	
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		function __construct()
		{
			$this->nullok = 0;
		}
	
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		public function execute($value, $setting)
		{
			if($setting['nullok']){
				$this->nullok = $setting['nullok'];
			}
			if($this->nullok == 1){
				return;
			}
			$value = ereg_replace("�@", "", $value);
			$value = trim($value);
			if($value == "" || $value == NULL){
				if($setting['nullerrormsg']){
					$errormsg = $setting['nullerrormsg'];
				}else{
					$errormsg = "%name%���͂��Ă��������B";
				}
				return $errormsg;
			}
			return;
		}
	}
}
?>