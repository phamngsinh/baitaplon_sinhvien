<?php
	class Province {
		var $queryVars;
		var $resp = array ( );
		
		function Province($queryVars) {
			$this->queryVars = $queryVars;
			switch ($queryVars ["act"]) {
				case "SelectProvince" :
					$id = (int)$queryVars["province_id"];
					$this->SelectProvince($id);
					break;
				default :
					return null;
			}
		}
		
		function SelectProvince($id) {
			if (empty($id)) $this->resp ["result"] = "You are select province !";
			else {
				$DistrictDao	= new DistrictDao();
				$DistrictList	= $DistrictDao->getAllDistricts(null, $id, Status::active);
				
				$this->resp ["result"] = "1";
				$i = 0;
				foreach ($DistrictList as $value) {
					$this->resp ["data"]["id"][$i] 		= $value["district_id"];
					$this->resp ["data"]["title"][$i] 	= stripslashes($value["district_name"]);
					$i ++;
				}
			}
		}
		
		/**
		 * Method to return the status of the AJAX transaction
		 *
		 * @return  string A string of raw HTML fetched from the Server
		 */
		function return_response() {
			return $this->resp;
		}
		
		function is_authorized() {
			return true;
		}
	}
?>