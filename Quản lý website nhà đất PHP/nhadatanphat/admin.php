<?php
	/**
	 * @project_name: localframe
	 * @file_name: admin.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **/
	session_start();
	define("DS", DIRECTORY_SEPARATOR);
	set_include_path("include_path");

	Define("ACTION", 	"admin");
	Define("WRITELOG", 	FALSE);

	require_once "library".DS."@library.php";
	require_once SRC_BACKEND.DS."configs".DS."@config.php";
	require_once SRC_BACKEND.DS."daos".DS."@daos.php";
	require_once BACKEND_PAGE_PATH.DS."include".DS."left.php";
	define("LOGS_DIR", BACKEND_LOG_PATH);
	
if (get_magic_quotes_gpc()) {
    function stripslashes_deep($value)
    {
        $value = is_array($value) ?
                    array_map('stripslashes_deep', $value) :
                    stripslashes($value);

        return $value;
    }

    $_POST = array_map('stripslashes_deep', $_POST);
    $_GET = array_map('stripslashes_deep', $_GET);
    $_COOKIE = array_map('stripslashes_deep', $_COOKIE);
    $_REQUEST = array_map('stripslashes_deep', $_REQUEST);
}
global $FILE;
	$textlang = "text_vn";
	
	$hdl = isset($_GET["hdl"]) ? trim($_GET["hdl"]): "index";
	$smarty->assign("hdl", $hdl);

	$account_info_ = isset($_SESSION["_LOGIN_BACKEND_"]) ? $_SESSION["_LOGIN_BACKEND_"] : null;
	
	if (empty($account_info_["account_id"]))
		$hdl = "login";
	
	if(count($account_info_)){//session cotnains
		$hdlAuthStr = str_replace("\\", "/", $hdl);
		$roleId = $account_info_['role_id'];
		$permissionList = UserRole::getRoleList();
		$permissionList = $permissionList[$roleId];
		if(is_array($permissionList) && count($permissionList) > 0){
			if (!in_array($hdlAuthStr, $permissionList)){
			 	$hdl = 'access denied';
			 }
		}//Else full control...
		
		$smarty->assign("roleId", 	$roleId);
	}
	
	$page = BACKEND_PAGE_PATH.$hdl.".php";

	//$Str 	= "asd sas dsf sd fsdf s f 192.168.1.2 192.168.1.3.1 192.168.1.4 sfdsdf";
	//print_r( Utils::getIPNumber($Str) );
	
	if(file_exists($page)) {
		$smarty->config_load("generals.conf");
		$include_header = BACKEND_TEMPLATE_PATH."include".DS."header".TPL_TYPE;
		$include_footer = BACKEND_TEMPLATE_PATH."include".DS."footer".TPL_TYPE;
		$include_left 	= BACKEND_TEMPLATE_PATH."include".DS."left".TPL_TYPE;
		
		$smarty->assign("include_header", 	$include_header);
		$smarty->assign("include_footer", 	$include_footer);
		$smarty->assign("include_left",   	$include_left);
		$smarty->assign("account_info", 	$account_info_);
		
		$smarty->assign("statusList",  	Status::getList($textlang));
		include($page);
	} else {
		include(BACKEND_PAGE_PATH.DS."default.php");
	}
?>