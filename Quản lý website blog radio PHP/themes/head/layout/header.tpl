<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	{THEME_PAGE_TITLE}
    {THEME_META_TAGS}
<link rel="Stylesheet" href="{NV_BASE_SITEURL}themes/{TEMPLATE}/css/style.css" type="text/css" />
		{THEME_CSS}
        {THEME_SITE_RSS}
        {THEME_SITE_JS}
<script src="{NV_BASE_SITEURL}themes/{TEMPLATE}/js/spri.js"> </script>
<script type="text/javascript" src="{NV_BASE_SITEURL}themes/{TEMPLATE}/js/cf.js"> </script>
<script type="text/javascript" src="{NV_BASE_SITEURL}themes/{TEMPLATE}/js/jquery.bpopup.min.js"> </script>
<script type="text/javascript">
	$(window).load(function(){
	 $('#siteloading').fadeOut(50).remove();
	 });
$(document).ready(function (e) {
		e("#slidebox").hide();
		e(function (){
			e(window).scroll(function () {
				if (e(this).scrollTop() > 500) {
					e("#slidebox").fadeIn()
				} else {
					e("#slidebox").fadeOut()
				}
			});
				return false
		});
		e(function (){
			e(window).scroll(function () {
				if (e(this).scrollTop() > 450) {
					e("#top-right").fadeOut()
				} else {
					e("#top-right").fadeIn()
				}
			});
				return false
		});
		e(function() {
            e('#login').bind('click', function(m) {
                m.preventDefault();
                e('#popup').bPopup({
					speed: 800,
					transition: 'slideDown'
                });
            });
         });
	});
</script>

<!--[if IE]><div style="background-color:#ab0909;color:#fff;font-size:15px;left:0;padding:7px;position:fixed;top:0;width:100%;text-align: center;z-index: 999999;">Lưu ý: Bạn đang sử dụng <a href="http://windows.microsoft.com/en-US/internet-explorer/downloads/ie" style="font-weight: bold;color: #70F085" target="_blank" rel="nofollow">Internet Explorer</a>, Vui lòng dùng <a href="http://www.mozilla.org/vi/firefox/new" style="font-weight: bold;color: #70F085" target="_blank" rel="nofollow">Mozilla Firefox</a> hoặc <a href="http://www.google.com/intl/vi/chrome" style="font-weight: bold;color: #70F085" target="_blank" rel="nofollow">Google Chrome</a> để lướt web nhanh và an toàn hơn.</div><![endif]-->
<!--[if IE]><script src="{NV_BASE_SITEURL}themes/{TEMPLATE}/js/html5.js"></script><![endif]-->
</head>
<body class="home page">
<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
<div  id="snow" class="wrapper">
<div class="centerdiv">
<div id="header">
<a class="logo" href="{THEME_SITE_HREF}" title="{THEME_LOGO_TITLE}"></a>
<div id="siteloading"></div>
<div id="top-right">
[LOGIN]
</div>
<div class="clear"></div>
</div>
</div>
		<!-- BEGIN: cay -->
		{FILE "cay.tpl"}
		<!--  END: cay -->

		<!-- BEGIN: maybay -->
		{FILE "maybay.tpl"}
		<!--  END: maybay -->

		<!-- BEGIN: cuoica -->
		{FILE "cuoi-ca.tpl"}
		<!--  END: cuoica -->

		<!-- BEGIN: dapxe -->
		{FILE "dap-xe.tpl"}
		<!--  END: dapxe -->

		<!-- BEGIN: noel -->
		{FILE "noel.tpl"}
		<!--  END: noel -->

<div id="pagecont_head"></div>
<div id="pagecont">
<div id="pagecont_title">
<a class="pagecont_title cf" href="{THEME_SITE_HREF}" title="{THEME_LOGO_TITLE}">{THEME_LOGO_TITLE}</a>
</div>
<div class="clear"> </div>
[THEME_ERROR_INFO]
	<noscript>
			<div id="nojavascript">{THEME_NOJS}</div>
	</noscript>