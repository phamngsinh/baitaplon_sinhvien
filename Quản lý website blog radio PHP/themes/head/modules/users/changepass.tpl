<!-- BEGIN: main -->
<div id="users">
    <div class="page-header">
		<h3>{LANG.change_pass}</h3>
	</div>
	<ul class="top-option">
		<li><a class="info-user" href="{URL_HREF}editinfo">{LANG.editinfo}</a></li>
		<li class="ui-tabs-selected"><a  class="info-user" href="{URL_HREF}changepass">{LANG.changepass_title}</a></li>
		<li><a class="info-user" href="{URL_HREF}editinfo&amp;changequestion">{LANG.question2}</a></li>
		<!-- BEGIN: allowopenid --><li><a class="info-user" href="{URL_HREF}openid">{LANG.openid_administrator}</a></li><!-- END: allowopenid -->
		<!-- BEGIN: regroups --><li><a class="info-user" href="{URL_HREF}regroups">{LANG.in_group}</a></li><!-- END: regroups -->
		<!-- BEGIN: logout --><li><a class="info-user" href="{URL_HREF}logout">{LANG.logout_title}</a></li><!-- END: logout -->
	</ul>
	<div class="clear"> </div>
	<form id="changePassForm" action="{USER_CHANGEPASS}" method="post">
		<p>{DATA.change_info}</p>
        <!-- BEGIN: passEmpty -->
        <div class="rows">
            <label>
                {LANG.pass_old}
            </label>
            <input type="password" id="nv_password_iavim" name="nv_password" value="{DATA.nv_password}" class="required password input" maxlength="{PASS_MAXLENGTH}" />
        </div>
        <!-- END: passEmpty -->
        <div class="rows">
            <label>
                {LANG.pass_new}
            </label>
            <input type="password" id="new_password_iavim" name="new_password" value="{DATA.new_password}" class="required password input" maxlength="{PASS_MAXLENGTH}" />
        </div>
        <div class="rows">
            <label>
                {LANG.pass_new_re}
            </label>
            <input type="password" id="re_password_iavim" name="re_password" value="{DATA.re_password}" class="required password input" maxlength="{PASS_MAXLENGTH}" />
        </div>
		<div class="rows">
			<label>
                &nbsp;
            </label>
			<input type="hidden" name="checkss" value="{DATA.checkss}" />
			<input type="submit" value="{LANG.change_pass}" class="button" />
		</div>
        
    </form>	
</div>
<!-- END: main -->