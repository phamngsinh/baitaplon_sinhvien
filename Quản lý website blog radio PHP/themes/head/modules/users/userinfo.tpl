<!-- BEGIN: main -->
<div id="users">
    <div class="page-header">
		<h1 class="cf">{LANG.user_info}</h1>
	</div>
	<ul class="top-option">
		<li><a class="info-user" href="{URL_HREF}editinfo">{LANG.editinfo}</a></li>
		<li><a class="info-user" href="{URL_HREF}changepass">{LANG.changepass_title}</a></li>
		<li><a class="info-user" href="{URL_HREF}editinfo&amp;changequestion">{LANG.question2}</a></li>
		<!-- BEGIN: allowopenid --><li><a class="info-user" href="{URL_HREF}openid">{LANG.openid_administrator}</a></li><!-- END: allowopenid -->
		<!-- BEGIN: regroups --><li><a class="info-user" href="{URL_HREF}regroups">{LANG.in_group}</a></li><!-- END: regroups -->		
		<!-- BEGIN: logout --><li><a class="info-user" href="{URL_HREF}logout">{LANG.logout_title}</a></li><!-- END: logout -->
	</ul> 	
		<div class="clear"> </div>
    <div class="box-border-shadow">
		<div class="content-box h-info">
			<div class="left fl">
				<img src="{SRC_IMG}" alt="" class="s-border" />
			</div>
			<div class="fl">
				{LANG.account2}: <strong>{USER.username}</strong> ({USER.email})<br />
				{USER.current_mode}<br />
				{LANG.current_login}: {USER.current_login}<br />
				{LANG.ip}: {USER.current_ip}
			</div>	
			<div class="clear"></div>
		</div>
        <!-- BEGIN: change_login_note -->
        <p>
            <strong>&raquo; {USER.change_name_info}</strong>
        </p>
        <!-- END: change_login_note -->
        <!-- BEGIN: pass_empty_note -->
        <p>
            <strong>&raquo; {USER.pass_empty_note}</strong>
        </p>
        <!-- END: pass_empty_note -->
        <!-- BEGIN: question_empty_note -->
        <p>
            <strong>&raquo; {USER.question_empty_note}</strong>
        </p>
        <!-- END: question_empty_note -->
            <dl>
        	   <dt class="fl">{LANG.name}:</dt>
               <dd class="fl">{USER.full_name}</dd>
            </dl>
            <dl>
        	   <dt class="fl">{LANG.birthday}:</dt>
               <dd class="fl">{USER.birthday}</dd>
            </dl>
            <dl>
        	   <dt class="fl">{LANG.gender}:</dt>
               <dd class="fl">{USER.gender}</dd>
            </dl>
            <dl>
        	   <dt class="fl">{LANG.address}:</dt>
               <dd class="fl">{USER.location}</dd>
            </dl>
            <dl>
        	   <dt class="fl">{LANG.website}:</dt>
               <dd class="fl">{USER.website}</dd>
            </dl>
            <dl>
        	   <dt class="fl">{LANG.yahoo}:</dt>
               <dd class="fl">{USER.yim}</dd>
            </dl>
            <dl>
        	   <dt class="fl">{LANG.phone}:</dt>
               <dd class="fl">{USER.telephone}</dd>
            </dl>
            <dl>
        	   <dt class="fl">{LANG.fax}:</dt>
               <dd class="fl">{USER.fax}</dd>
            </dl>
            <dl>
        	   <dt class="fl">{LANG.mobile}:</dt>
               <dd class="fl">{USER.mobile}</dd>
            </dl>
            <dl>
        	   <dt class="fl">{LANG.showmail}:</dt>
               <dd class="fl">{USER.view_mail}</dd>
            </dl>
            <dl>
        	   <dt class="fl">{LANG.regdate}:</dt>
               <dd class="fl">{USER.regdate}</dd>
            </dl>
            <dl>
        	   <dt class="fl">{LANG.st_login2}:</dt>
               <dd class="fl">{USER.st_login}</dd>
            </dl>
            <dl>
        	   <dt class="fl">{LANG.last_login}:</dt>
               <dd class="fl">{USER.last_login}</dd>
            </dl>
    </div>
</div>
<!-- END: main -->