<!-- BEGIN: main -->
<link rel="Stylesheet" href="{NV_BASE_SITEURL}themes/{TEMPLATE}/css/detail.css" type="text/css" />

<div id="prodesc">          
<h2 class="protitle cf"><a href="{DETAIL.link}">{DETAIL.title}</a></h2>
<span class="note">{LANG.pubtime}: {DETAIL.publtime}</span>
 <!-- BEGIN: post_name -->
<span class="note"> - {LANG.post_name}: {DETAIL.post_name}</span>
<!-- END: post_name -->
<span class="note"> -  {LANG.view}: {DETAIL.hitstotal}</span>
</div>

<div id="shareicon">
<a target="_blank" title="Share on Facebook" rel="nofollow" href="http://www.facebook.com/share.php?u={SELFURL}"><img  src="{NV_BASE_SITEURL}themes/{TEMPLATE}/images/facebook.png" /></a>

<a target="_blank" title="Share on Yahoo!Msg" rel="nofollow" href="ymsgr:im?+&msg=Xem trang này hay lắm nè: {DETAIL.title} - {SELFURL}"><img  src="{NV_BASE_SITEURL}themes/{TEMPLATE}/images/yahoo.png" /></a>

<a target="_blank" title="Share on Zing Me" rel="nofollow" href="http://link.apps.zing.vn/share?url={SELFURL}"><img  src="{NV_BASE_SITEURL}themes/{TEMPLATE}/images/zing.png" /></a>

<br />
<div class="fb-like" data-href="{SELFURL}" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></div>
</div>
<div style="clear:both"></div>
<div class="bodytext">
 {DETAIL.bodytext}
</div>
<div class="clear"></div>
    <!-- BEGIN: author -->
        <div class="source">
            <!-- BEGIN: name -->
            	<strong>{LANG.author}: </strong>{DETAIL.author}
            <!-- END: name -->
            <!-- BEGIN: source -->
                <br/>
                <strong>{LANG.source}: </strong>{DETAIL.source}
            <!-- END: source -->
        </div>
    <!-- END: author -->    
    <!-- BEGIN: copyright -->
        <div class="copyright">
            {COPYRIGHT}
        </div>
    <!-- END: copyright -->    
    <!-- BEGIN: adminlink -->
    <p style="text-align: right;">
        {ADMINLINK}
    </p>
    <!-- END: adminlink -->
	<div id="share-publish">
	<a target="_blank" title="Share on Yahoo!Msg" rel="nofollow" href="ymsgr:im?+&msg=Xem trang này hay lắm nè: {DETAIL.title} - {SELFURL}"><img alt=""  src="{NV_BASE_SITEURL}themes/{TEMPLATE}/images/yahoo.png" /></a>
	<a target="_blank" title="Share on Zing Me" rel="nofollow" href="http://link.apps.zing.vn/share?url={SELFURL}"><img alt=""  src="{NV_BASE_SITEURL}themes/{TEMPLATE}/images/zing.png" /></a>
	<a target="_blank" title="Share on Facebook" rel="nofollow" href="http://www.facebook.com/share.php?u={SELFURL}"><img alt=""  src="{NV_BASE_SITEURL}themes/{TEMPLATE}/images/facebook.png" /></a>
	<div style="float: left; padding-top:3px;" class="fb-like" data-href="{SELFURL}" data-send="false" data-width="300" data-show-faces="false"></div>
	</div>
	<div class="clear"> </div>
        <!-- BEGIN: keywords -->
        <div class="oop">
            <div class="header-oop">
                {LANG.keywords}:
            </div>
            <p>
                <!-- BEGIN: loop --><a title="{KEYWORD}" href="{LINK_KEYWORDS}">{KEYWORD}</a>{SLASH} <!-- END: loop -->
            </p>
        </div>
        <!-- END: keywords -->
    
	<!-- BEGIN: topic -->
    <div class="other-news">
        <h4>{LANG.topic}</h4>
        <ul>
            <!-- BEGIN: loop -->
            <li>
                <a title="{TOPIC.title}" href="{TOPIC.link}">{TOPIC.title}</a>
                <span class="date">({TOPIC.time})</span>
            </li>
            <!-- END: loop -->
        </ul>       
    </div>
    <!-- END: topic -->
	<!-- BEGIN: related_new -->
    <div class="other-news">
        <h4>{LANG.related_new}</h4>
        <ul>
            <!-- BEGIN: loop -->
            <li>
                <a title="{RELATED_NEW.title}" href="{RELATED_NEW.link}">{RELATED_NEW.title}</a>
                <span class="date">({RELATED_NEW.time})</span>
            </li>
            <!-- END: loop -->
        </ul>
    </div>
    <!-- END: related_new -->
	<!-- BEGIN: related -->
    <div class="other-news">
        <h4>{LANG.related}</h4>
        <ul>
            <!-- BEGIN: loop -->
            <li>
                <a title="{RELATED.title}" href="{RELATED.link}">{RELATED.title}</a>
                <span class="date">({RELATED.time})</span>
            </li>
            <!-- END: loop -->
        </ul>
    </div>
    <!-- END: related -->
	<!-- BEGIN: comment -->
    <div id="comment">
			<div class="clear"></div>
        <div id="showcomment" class="list-comments">
		 <h3>{LANG.comment}</h3>
		<!-- BEGIN: fb -->	
			<div class="fb-comments" data-href="{SELFURL}" data-width="610" data-num-posts="10"></div>
			{COMMENTCONTENT}
		<!-- END: fb -->
		</div>
        <div id="formcomment" class="comment-form">
            <!-- BEGIN: form -->
            <div class="box-border content-box">
                <div class="box-user">
                    <input id="commentname" class="input input-c" type="text" value="{NAME}" {DISABLED}  onblur="if(this.value=='')this.value='{LANG.comment_name}';" onclick="if(this.value=='{LANG.comment_name}')this.value='';"/>
                    <input id="commentemail_iavim" class="input input-c" type="text" value="{EMAIL}" {DISABLED}  onblur="if(this.value=='')this.value='{LANG.comment_email}';" onclick="if(this.value=='{LANG.comment_email}')this.value='';"/>
                </div>
                <p>
                    <textarea id="commentcontent" class="commentcontent" cols="1" rows="1" onblur="if(this.value=='')this.value='{LANG.comment_content}';" onclick="if(this.value=='{LANG.comment_content}')this.value='';">{LANG.comment_content}</textarea>
                </p>
                <div style="height: 50px; margin-left: 20px;">
                    <img style="float: left;padding: 5px;" id="vimg" alt="{N_CAPTCHA}" src="{SRC_CAPTCHA}" width="{GFX_WIDTH}" height="25px" /><img style="float: left;padding: 2px; width: 25px; height: 35px;" alt="{CAPTCHA_REFRESH}" src="{CAPTCHA_REFR_SRC}" width="16" height="16" class="refresh" onclick="nv_change_captcha('vimg','commentseccode_iavim');"/>
					<input id="commentseccode_iavim" type="text" class="input capcha" />
					<input id="buttoncontent" type="submit" value="{LANG.comment_submit}" onclick="sendcommment('{NEWSID}', '{NEWSCHECKSS}', '{GFX_NUM}');" class="button" />
				</div>
            </div>

    		<!-- END: form -->
            <!-- BEGIN: form_login-->
            {COMMENT_LOGIN}
			<!-- END: form_login -->
        </div>
    </div>
	<!-- END: comment -->
<!-- END: main -->

<!-- BEGIN: no_permission -->
    <div id="no_permission">
        <p>
            {NO_PERMISSION}
        </p>
    </div>
<!-- END: no_permission -->