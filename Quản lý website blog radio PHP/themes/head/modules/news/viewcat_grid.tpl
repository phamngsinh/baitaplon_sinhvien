<!-- BEGIN: main -->
<script src="{NV_BASE_SITEURL}themes/{TEMPLATE}/js/transform.js"></script>
<script src="{NV_BASE_SITEURL}themes/{TEMPLATE}/js/3.js"></script>
<script type="text/javascript" language="javascript" charset="utf-8">
$(document).ready(function ($) {
    $('li div.back').hide().css('left', 0);
    
    function mySideChange(front) {
        if (front) {
            $(this).parent().find('div.front').show();
            $(this).parent().find('div.back').hide();
            
        } else {
            $(this).parent().find('div.front').hide();
            $(this).parent().find('div.back').show();
        }
    }
    
    $('li').hover(
        function () {
            $(this).find('div').stop().rotate3Di('flip', 250, {direction: 'clockwise', sideChange: mySideChange});
        },
        function () {
            $(this).find('div').stop().rotate3Di('unflip', 500, {sideChange: mySideChange});
        }
    );
});
</script>
	<ul id="list-post">
	<!-- BEGIN: viewcatloop -->
		<li class="list-post" onclick="javascript: location.href='{CONTENT.link}'">

			<div class="front color-type{RANDOM}">
				<img alt="" src="{HOMEIMG1}" />
				<h2 class="cf"> <a href="{CONTENT.link}">{CONTENT.title}</a> </h2>
			</div>
			<div class="back">
				<h2 class="cf"> <a href="{CONTENT.link}">{CONTENT.title}</a> </h2>
				<p class="cf">{CONTENT.hometext}</p>
				<a class="more cf" href="{CONTENT.link}" > </a>
			</div>

		</li>
	<!-- END: viewcatloop -->
	</ul>
	<div class="clear"> </div>
<!-- BEGIN: generate_page -->
<div class="generate_page">
	{GENERATE_PAGE}
</div>
<!-- END: generate_page -->
<!-- END: main -->