<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Smilies & AGCodes</TITLE>
<META content="text/html; charset=utf-8" http-equiv=Content-Type>
<style type="text/css">
<!--
td {  font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8pt}
-->
</style>
</HEAD>
<BODY bgColor=#ffffff link=#000080 text=#000000 vLink=#000080>
<CENTER>
  <table width="95%" border="0" cellspacing="1" cellpadding="0">
    <tr>
      <td height="25">S M I L I E S &nbsp;&nbsp;&nbsp;L À&nbsp;&nbsp;&nbsp;G Ì ?</td>
    </tr>
    <tr>
      <td>
        <p>'Smilies' là những hình ảnh nhỏ mà có thể là được dùng để biểu thị một
          cảm xúc hay cảm giác. Nếu bạn có sử dụng email hay Internet tán gẫu, thì bạn
          đã quen với khái niệm smilie. Để chèn smiles vào bài viết bạn phải nhập 1 chuỗi kí tự thay thế(Code) cho ảnh đó mà Admin đã quy định.</p>
        <p> Ở đây là danh sách các smilies và chuỗi quy định thay thế(Code) hiện thời được chấp nhận: </p>
      </td>
    </tr>
  </table>
  <table bgcolor=#f7f7f7 border=0 width="95%" cellspacing="1" cellpadding="4">
    <tbody>
    <tr>
      <td bgcolor="#996699"><font color=#ffffff><b>Chuỗi thay thế</b></font></td>
      <td bgcolor="#996699"><font color=#ffffff><b>Emotion sẽ hiển thị cảm xúc</b></font></td>
      <td bgcolor="#996699"><font color=#ffffff><b>Ảnh sẽ hiển thị thay thế</b></font></td>
    </tr>

<?php include ("./smilies.inc"); ?>

    </tbody>
  </table>
  <br>
  <table width="95%" border="0" cellspacing="1" cellpadding="0">
    <tr>
      <td height="25">A G &nbsp;&nbsp;&nbsp;C O D E &nbsp;&nbsp;&nbsp;L À &nbsp;&nbsp;&nbsp;G Ì &nbsp;&nbsp; ? </td>
    </tr>
    <tr>
      <td>
        <p>AGCode là một sự biến đổi trên những thẻ HTML. Bạn có thể đã quen thuộc
          với nó. Về cơ bản, nó cho phép bạn thêm chức năng kiểu văn bản nào đó mà các bạn
          thường dùng HTML. Bạn có thể sử dụng mã AGCode dù
          HTML không được cho phép nội dung lưu bút. Bạn có thể cảm thấy sử dụng
          AGCode như bị chống đối Tới HTML, bởi vì có ít sự mã hóa hơn. HTML ít được cho phép nhập vào lưu bút mà thay thế bằng AG Code,
          vì nó an toàn hơn đối với sự sử dụng ( sai sự mã hóa cú pháp sẽ không dẫn tới như nhiều vấn đề).
        <p>AGCodes hiện tại:
      </td>
    </tr>
  </table>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="95%" align="center">
  <TBODY>
  <TR>
    <TD bgColor=#000000>
      <TABLE border=0 cellPadding=4 cellSpacing=1 width="100%">
        <TBODY>
        <TR bgColor=#0099CC>
          <TD><b><font color="#FFFFFF">Siêu liên kết URL</font></b></TD>
        </TR>
        <TR bgColor=#ffffff>
          <TD>Nếu mã  AG được mở, bạn có thể dùng thẻ 
            [URL] để chèn 1 siêu liên kết. Đơn giản gõ URL đầy đủ Trong
            thẻ [URL] và siêu liên kết sẽ được tạo ra
            tự động:
            <UL>VD bạn muốn chèn liên kết đến lưu bút của tanphu.net có địa chỉ như sau:
              <LI><font color="#800000">http://www.tanphu.net/luubut/ </font>
              <LI><FONT color=#800000>www.tanphu.net/luubut/ </FONT><br>
			  Thì bạn sẽ chèn mã sau vào nội dung lưu bút: <BR>
                [url=http://www.tanphu.net/luubut/]Lưu bút TânPhú.Net[/url]<BR>
				hoặc là: [url=www.tanphu.net/luubut/]Lưu bút TânPhú.Net[/url]<BR>
				~~> Thì nội dung trên lưu bút chỉ hiển thị là <b>Lưu bút TânPhú.Net</b> và khi người độc bấm vào chữ <b>Lưu bút TânPhú.Net</b> thì nó sẽ mở trang lưu bút của tân phú.net lên có địa chỉ là <b>http://www.tanphu.net/luubut/</b> hoặc <b>www.tanphu.net/luubut/</b> tuỳ lúc bạn chèn.
              <LI>Lưu ý: <br>Bạn phải chèn sau <b>[url=</b> là <b>http://</b> hoặc <b>www</b>. nếu ko sẽ ko được<br>VD cách chèn sau là sai:<b>[url=tanphu.net/luubut/]Lưu bút TânPhú.Net[/url]</b>
       <br><br>
              <LI>Ngoài cách chèn trên thì bạn có thể chèn theo kiểu sau:

                  <CENTER>
                    <FONT color=#ff0000>[url]</FONT>http://www.tanphu.net/luubut/<FONT color=#ff0000>[/url]</FONT>
                  </CENTER>
                <P>~~> Thì nội dung trên lưu bút chỉ hiển thị là <b>http://www.tanphu.net/luubut/</b> và khi người độc bấm vào chữ <b>Lưu bút TânPhú.Net</b> thì nó sẽ mở trang lưu bút của tân phú.net lên có địa chỉ là <b>http://www.tanphu.net/luubut/</b>. </P>
              </LI>
            </UL>
          </TD>
        <TR bgColor=#0099CC>
          <TD><b><font color="#FFFFFF">Email Links</font></b></TD>
        </TR>
        <TR bgColor=#ffffff>
          <TD>Để chèn Email của ai đó vào trong nội dung lưu bút của bạn thì bạn có thể chèn theo cách sau:
            
            <P>
              <CENTER>
                <FONT color=#ff0000>[email]</FONT>vuthanhlai@gmail.com<FONT color=#ff0000>[/email]</FONT>
              </CENTER>
            <P>~~>Thì trên nội dung lưu bút sẽ hiển thị là<b>vuthanhlai@gmail.com</b>, Khi nguời đọc bấm vào chữ<b>vuthanhlai@gmail.com</b> thì sẽ tự động mở hộp soạn thư gửi tới vuthanhlai@gmail.com. </P>
          </TD>
        </TR>
        <TR bgColor=#0099CC>
          <TD><b><font color="#FFFFFF">Chữ đậm và chữ nghiêng</font></b></TD>
        </TR>
        <TR bgColor=#ffffff>
          <TD>Bạn có thể chèn chữ đâm hoặc nghiêng hoặc cả 2 vào nội dung lưu bút bằng thẻ sau: [b] [/b] hoặc [i] [/i] .Bạn có thể xem ví dụ sau:
            <P>
              <CENTER>
                Chào, <FONT color=#ff0000>[b]</FONT><B>Vũ Thanh Lai</B><FONT color=#ff0000>[/b]</FONT><BR><br>
                Chào, <FONT color=#ff0000>[i]</FONT><I>Admin TânPhú.Net</I><font color="#FF0000">[i]</font>
              </CENTER><br>
			  ~~>Kết quả là:<br>Chào,<b>Vũ Thanh Lai</b><br>Chào,<i>Admin TânPhú.Net</i>
          </TD>
        </TR>
        <TR bgColor=#0099CC>
          <TD><b><font color="#FFFFFF">Chèn ảnh vào nội dung lưu bút</font></b></TD>
        </TR>
        <TR bgColor=#ffffff>
          <TD>Bạn có thể chèn ảnh vào nội dung lưu bút bằng cách sử dụng thẻ [img]  [/img].
            <P>
              <CENTER>
                <FONT color=#ff0000>[img]</FONT>http://tanphu.net/laidata/LKTP.gif<FONT color=#ff0000>[/img]</FONT>
              </CENTER>
            <P>~~>Kết quả: <img border="0" src="http://tanphu.net/laidata/LKTP.gif"><br>
			Bạn cần chú ý là link ảnh trong thẻ phải bắt đầu bằng <b>http://</b> đó . Tức là mã chèn vào luôn có dạng là bắt đầu bằng <FONT color=#ff0000>[img]http://</FONT> .</P>
          </TD>
        </TR>
        </TBODY>
      </TABLE>
    </TD>
  </TR>
  </TBODY>
</TABLE>
<table width="95%" border="0" cellspacing="1" cellpadding="4" align="center">
  <tr>
    <td><font color=#800000>Một số lưu ý:</font><br>
      AG co de ko phân biệt thẻ chữ hoa hay thường, VD bạn có thẻ sử dụng <font color=#ff0000>[URL]</font>
      hay <font color=#ff0000>[url]</font>đều được.<br><br>
      <font color="#800000">Cách dùng mã AG sai:</font> <br>
       <font color="#ff0000">[url]</font> www.tanphu.net <font color=#ff0000>[/url]</font> -TRong giữa hai thẻ ko có khỏang trống đâu nha.<br>
        <br>
        <font color="#ff0000">[email]</font>vuthanhlai@gmail.com<font color=#ff0000>[email]</font> - Thẻ đòng phải kết thúc co 1 dấu gạch chéo tức là đoạn mã vừa rồi thiếu dấu gạch chéo (<font color=#ff0000>[/email]</font>)
    </td>
  </tr>
</table>
</CENTER>
<BR>
<center><b>Thân chào, Chúc bạn online vui vẻ.<br>Admin : Vũ Thanh Lai | Email: VuThanhLai@Gmail.Com | Y!M: KiUcTinhYeu_1811</b></center>
</BODY>
</HTML>
