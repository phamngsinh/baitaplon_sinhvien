<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
*		Vietnamese Translation by DCV Team - Discuz.vn 2/9/2012
 *      $Id: lang_email.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$lang = array
(
	'email_name' => 'Xác nhận Email',
	'email_desc' => 'Bạn cần xác nhận Email và sau đó nhận thưởng.',
	'email_view' => '<strong>Hãy làm theo hướng dẫn dưới đây</strong>
		<ul>
		<li><a href="home.php?mod=spacecp&ac=profile&op=password" target="_blank">Vào trang cài đặt tài khoản</a></li>
		<li>Điền email và mật khẩu.</li>
		<li>Vài phút hệ thống sẽ gửi email xác nhận cho bạn. Hãy tiếp tục làm theo hướng dẫn trong email đó.</li>
		<li>Nếu không nhận được email bấm gửi lại và vào Junk Email để kiểm tran</li>
		</ul>',
);

?>