<?php defined('_JEXEC') or die('Restricted access'); 
$TableName = JRequest::getCmd('table');
JHTML::_('behavior.tooltip');
jimport('joomla.html.pane');
//1st Parameter: Specify 'tabs' as appearance 
//2nd Parameter: Starting with third tab as the default (zero based index)
//open one!
$pane =& JPane::getInstance('tabs', array('startOffset'=>0)); 

?>
form
<script type="text/javascript" src="components/com_propiedades/scripts/mootools.js"></script>
<?php
//echo '$TableName: '.$TableName.'<br>';
switch ($TableName)
{
case 'country' :		
?>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="col100">
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'Details' ); ?></legend>
		<table class="admintable">
		<tr>
			<td width="100" align="right" class="key">
				<label for="name">
							<?php echo JText::_( 'Country Name' ); ?>:
						</label>
			</td>
			<td>
				<input class="text_area" type="text" name="name" id="name" size="60" maxlength="250" value="<?php echo $this->datos->name;?>" />
			</td>
		</tr>   
        
        			<tr>
			<td width="100" align="right" class="key">
				<label for="name">
							<?php echo JText::_( 'Alias' ); ?>:
						</label>
			</td>
			<td>
				<input class="text_area" type="text" name="alias" id="alias" size="60" maxlength="250" value="<?php echo $this->datos->alias;?>" />
			</td>
		</tr> 
        
        <tr>
					<td class="key"><label for="user_id"><?php echo JText::_( 'Published' ); ?>:</label></td>
					<td >
<?php $chequeado0 = $this->datos->published ? JText::_( '' ) : JText::_( 'checked="checked"' );?>
<?php $chequeado1 = $this->datos->published ? JText::_( 'checked="checked"' ) : JText::_( '' );?>
                    <input name="published" id="published0" value="0" <?php echo $chequeado0;?> type="radio">
	<label for="published0"><?php echo JText::_( 'No' );?></label>
	<input name="published" id="published1" value="1" <?php echo $chequeado1;?> type="radio">
	<label for="published1"><?php echo JText::_( 'Yes' );?></label>  
					</td>
				</tr>         
	</table>
	</fieldset>
</div>
<div class="clr"></div>
<input type="hidden" name="option" value="com_properties" />
<input type="hidden" name="table" value="<?php echo $TableName; ?>" />
<input type="hidden" name="id" value="<?php echo $this->datos->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="property" />
</form>
<?php		
	break;
	
	
	case 'state' :		
?>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="col100">
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'Details' ); ?></legend>
		<table class="admintable">
		<tr>
			<td width="100" align="right" class="key">
				<label for="name">
							<?php echo JText::_( 'State Name' ); ?>:
						</label>
			</td>
			<td>
				<input class="text_area" type="text" name="name" id="name" size="60" maxlength="250" value="<?php echo $this->datos->name;?>" />
			</td>
		</tr>   
        
        			<tr>
			<td width="100" align="right" class="key">
				<label for="name">
							<?php echo JText::_( 'Alias' ); ?>:
						</label>
			</td>
			<td>
				<input class="text_area" type="text" name="alias" id="alias" size="60" maxlength="250" value="<?php echo $this->datos->alias;?>" />
			</td>
		</tr> 
        
        
        <tr>
			<td width="100" align="right" class="key">
				<label for="name">
							<?php echo JText::_( 'Parent' ); ?>:
						</label>
			</td>
			<td>
            <?php echo SelectHelper::Select( $this->datos,'country' ); ?>
				
			</td>
		</tr>   
        <tr>
					<td class="key"><label for="user_id"><?php echo JText::_( 'Published' ); ?>:</label></td>
					<td >
<?php $chequeado0 = $this->datos->published ? JText::_( '' ) : JText::_( 'checked="checked"' );?>
<?php $chequeado1 = $this->datos->published ? JText::_( 'checked="checked"' ) : JText::_( '' );?>
                    <input name="published" id="published0" value="0" <?php echo $chequeado0;?> type="radio">
	<label for="published0"><?php echo JText::_( 'No' );?></label>
	<input name="published" id="published1" value="1" <?php echo $chequeado1;?> type="radio">
	<label for="published1"><?php echo JText::_( 'Yes' );?></label>  
					</td>
				</tr>       
                
                <tr>
			<td width="100" align="right" class="key">
				<label for="name">
							<?php echo JText::_( 'Map id' ); ?>:
						</label>
			</td>
			<td>
				<input class="text_area" type="text" name="mid" id="mid" size="60" maxlength="250" value="<?php echo $this->datos->mid;?>" />
			</td>
		</tr>   
                
                  
	</table>
	</fieldset>
</div>
<div class="clr"></div>
<input type="hidden" name="option" value="com_properties" />
<input type="hidden" name="table" value="<?php echo $TableName; ?>" />
<input type="hidden" name="id" value="<?php echo $this->datos->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="property" />
</form>
<?php		
	break;
	case 'locality' :	
	
?>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="col100">
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'Details' ); ?></legend>
		<table class="admintable">
		<tr>
			<td width="100" align="right" class="key">
				<label for="name">
							<?php echo JText::_( 'Loacality Name' ); ?>:
						</label>
			</td>
			<td>
				<input class="text_area" type="text" name="name" id="name" size="60" maxlength="250" value="<?php echo $this->datos->name;?>" />
			</td>            
		</tr> 
        
        			<tr>
			<td width="100" align="right" class="key">
				<label for="name">
							<?php echo JText::_( 'Alias' ); ?>:
						</label>
			</td>
			<td>
				<input class="text_area" type="text" name="alias" id="alias" size="60" maxlength="250" value="<?php echo $this->datos->alias;?>" />
			</td>
		</tr> 
        
        
        <tr>
			<td width="100" align="right" class="key">
				<label for="name">
							<?php echo JText::_( 'Parent' ); ?>:
						</label>
			</td>
			<td>
            <?php echo SelectHelper::Select( $this->datos,'state' ); ?>
				
			</td>
		</tr>   
        <tr>
					<td class="key"><label for="user_id"><?php echo JText::_( 'Published' ); ?>:</label></td>
					<td >
<?php $chequeado0 = $this->datos->published ? JText::_( '' ) : JText::_( 'checked="checked"' );?>
<?php $chequeado1 = $this->datos->published ? JText::_( 'checked="checked"' ) : JText::_( '' );?>
                    <input name="published" id="published0" value="0" <?php echo $chequeado0;?> type="radio">
	<label for="published0"><?php echo JText::_( 'No' );?></label>
	<input name="published" id="published1" value="1" <?php echo $chequeado1;?> type="radio">
	<label for="published1"><?php echo JText::_( 'Yes' );?></label>   
					</td>
				</tr>
	</table>
	</fieldset>
</div>
<div class="clr"></div>
<input type="hidden" name="option" value="com_properties" />
<input type="hidden" name="table" value="<?php echo $TableName; ?>" />
<input type="hidden" name="id" value="<?php echo $this->datos->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="property" />
</form>
<?php	
	break;
}
?>