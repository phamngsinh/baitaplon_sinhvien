<?php defined('_JEXEC') or die('Restricted access'); 
$option = JRequest::getVar('option');
$view = JRequest::getVar('view');

JHTML::_('behavior.tooltip');
	$ordering = ($this->lists['order'] == 'ordering');	
$component = JComponentHelper::getComponent( 'com_properties' );
$params = new JParameter( $component->params );
$currencyformat=$params->get('FormatPrice');	

?>
<form action="index.php" method="post" name="adminForm">
<table width="100%">
	<tr>
		<td align="left"  width="100%">
			<?php echo JText::_( 'Filter' ); ?>:
			<input type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
			<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
			<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_state').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
		</td>
       <td></td>       
         <td nowrap="nowrap" align="right">
          <td nowrap="nowrap" align="right">
         <?php echo Filter::getUser( $this->items[0],'profiles','products' ); ?>
         </td>
         <?php echo Filter::FilterCountry( $this->items[0],'country','products' ); ?>
         </td>
         
         
         <td nowrap="nowrap" align="right">
         <?php echo Filter::FilterSid( $this->items[0],'sid','products' ); ?>
         </td>
         <td nowrap="nowrap" align="right">
         <?php echo Filter::FilterLocality( $this->items[0],'locality','products' ); ?>
         </td>	
    </tr>
    <tr>
	    <td></td>
	    <td nowrap="nowrap" align="right">
	    <?php echo Filter::FilterCategory( $this->items[0],'category','products' ); ?>
	    </td>
	    <td nowrap="nowrap" align="right">
	    <?php echo Filter::FilterType( $this->items[0],'type','products' ); ?>
	    </td>
	    <td nowrap="nowrap" align="right">
	<?php echo $this->lists['state']; ?>
	    </td> 
	     <td nowrap="nowrap" align="right" style="display:none">
	    <?php //echo Filter::FilterFeatured( $this->items[0],'featured','products' ); ?>
	    </td>
            	
	</tr>
</table>
<div id="editcell">
<table class="adminlist">
<thead>
		<tr>
			<th width="5">
				<?php echo JText::_( 'NUM' ); ?>
			</th>
			<th width="5">
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
			</th>
			<th  class="title" >
				<?php echo JHTML::_('grid.sort',   'Title', 'name', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>
            <!--<th  class="title" width="15%">
				<?php echo JHTML::_('grid.sort',   'Category', 'cid', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>-->
            <th  class="title" width="20%">
				<?php echo JHTML::_('grid.sort',   'Type', 'type', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>
            <th width="5%" align="center">
				<?php echo JHTML::_('grid.sort',   'Publ', 'published', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>
	   <th width="7%" align="center">
				<?php echo JHTML::_('grid.sort',   'End date', 'enddate', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>	
             <th nowrap="nowrap" width="7%">
						<?php echo JHTML::_('grid.sort',   'Order by', 'ordering', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
						<?php echo JHTML::_('grid.order',  $this->items ); ?>
					</th>
            <th width="5%" align="center">
				<?php echo JHTML::_('grid.sort',   'ID', 'id', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>
            <th width="5%" align="center">
				<?php echo JHTML::_('grid.sort',   'AiD', 'agent_id', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>
            <th width="5%" align="center">
				Clone
			</th> 

		</tr>
	</thead> 
<tbody>
	<?php
	//echo "<pre>";print_r($this->items);
	$k = 0;
	for ($i=0, $n=count( $this->items ); $i < $n; $i++)
	{
		$row = &$this->items[$i];
$link 		= JRoute::_( 'index.php?option='.$option.'&view='.$view.'&layout=form&task=edit&cid[]='. $row->id);	
		
		$checked 	= JHTML::_('grid.id',  $i, $row->id );
		$published 	= JHTML::_('grid.published', $row, $i );
		$destacado = CatTreeHelper::Destacado( $row, $i);
	?>
		<tr class="<?php echo "row$k"; ?>">
			<td>
				<?php echo $this->pagination->getRowOffset( $i ); ?>
			</td>
			<td>
				<?php echo $checked; ?>
			</td>
            
<td>
          <?php
   $img_path = JURI::root().'images/properties/images/'.$row->id.'/'.$this->Images[$row->id][0]->name;
   
   ?>
	<span class="editlinktip hasTip" title="Description::<?php echo $row->name;//description; ?>"  >
	<a href="<?php echo $link; ?>" <?php if ($row->published == 0) echo 'style="color: red;" '; ?> ><?php echo $row->name; ?></a></span>
</td>
<!--<td align="center"><?php echo $row->cid.':'.$row->name_category; ?></td>-->
<td align="center"><?php echo $row->name_type;?></td>
<td align="center">
<?php 
//echo JText::_( 'DETAILS_MARKET'.$row->available );

  ?>
  <?php echo $published;?>
 
</td>
<td align="center">
<?php 
//echo JText::_( 'DETAILS_MARKET'.$row->available );

  ?>
 
  <?php echo $row->enddate;?>
</td>
<td class="order">
	<span><?php echo $this->pagination->orderUpIcon( $i, true, 'orderup', 'Move Up', $ordering ); ?></span>
	<span><?php echo $this->pagination->orderDownIcon( $i, $n, true, 'orderdown', 'Move Down', $ordering ); ?></span>
	<?php $disabled = $ordering ?  '' : 'disabled="disabled"'; ?>
	<input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" <?php echo $disabled ?> class="text_area" style="text-align: center" />
    <input type="hidden" name="itemid[]" value="<?php echo $row->id;?>" />
</td>
<td align="center"><?php echo $row->id; ?></td>   			
 		<td align="center"><?php echo $row->agent_id; ?></td>  
        
        <td align="center"><a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i; ?>','clonar')" title="Clonar Item">
		Clonar</a></td>
		</tr>
		<?php
			$k = 1 - $k;
		}
		?>
	</tbody>
    <tfoot>
    <tr>
      <td colspan="16"><?php echo $this->pagination->getListFooter(); ?></td>
    </tr>
  </tfoot>
	</table>
</div>

	<input type="hidden" name="option" value="<?php echo $option; ?>" />
     <input type="hidden" name="view" value="<?php echo $view; ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<input type="hidden" name="controller" value="products" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>	
<?php 
jimport( 'joomla.application.application' );
global $mainframe, $option;
$mainframe->setUserState("$option.filter_order", NULL);
?>