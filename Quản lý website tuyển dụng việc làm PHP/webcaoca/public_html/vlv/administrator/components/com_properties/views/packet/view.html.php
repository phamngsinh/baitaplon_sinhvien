<?php
/**
* @copyright	Copyright(C) 2008-2010 Fabio Esteban Uzeltinger
* @license 		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @email		admin@com-property.com
**/
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view' );

class PropertiesViewPacket extends JView{
	
	function display($tpl = null)
	{
		$option = JRequest::getVar('option');
		if(JRequest::getVar('task')=='edit' or JRequest::getVar('task')=='add'){
			$packet		=& $this->get('packet');

			$db = JFactory::getDBO();
			$query = ' SELECT * FROM #__properties_typepacket ';				
			$db->setQuery( $query );
			$listtypes = $db->loadObjectList();
			$arr_secl = explode(',',$packet->list_type);
			
			$lists .= '<select id="price_listtype" name="price_listtype" multiple="multiple" size="4">';
			foreach ($listtypes as $listtype) {
				$selected = in_array($listtype->id, $arr_secl) ? ' selected' : '';
				$lists .= '<option '.$selected.' id="'.$listtype->id.'" value="'.$listtype->id.'">'.$listtype->name.'</option>';
			} 
			$lists .= '</select>';
			
			$isNew = ($type->id < 1);
			$text = $isNew ? JText::_( 'New' ) : JText::_( 'Edit' );
			JToolBarHelper::title(   JText::_( 'packet' ).': <small><small>[ ' . $text.' ]</small></small>' );
			JToolBarHelper::custom('save2new', 'new.png', 'new_f2.png', 'Save and new', false);
			JToolBarHelper::apply();
			JToolBarHelper::save();
			
			if ($isNew)  {
				JToolBarHelper::cancel();
			} else {
				// for existing items the button is renamed `close`
				JToolBarHelper::cancel( 'cancel', 'Close' );
			}			
			$this->assignRef('packet',		$packet);			
			$this->assignRef('lists',		$lists);
			parent::display($layout);
			
		}else{
			JToolBarHelper::publishList();
			JToolBarHelper::unpublishList();
			JToolBarHelper::deleteList();
			JToolBarHelper::editListX();
			JToolBarHelper::addNewX();
	
			$items		= & $this->get( 'Data');
			$pagination =& $this->get('Pagination');
			$lists = & $this->get('List');
			
			$this->assignRef('items',		$items);
			$this->assignRef('pagination', $pagination);
			$this->assignRef('lists', $lists);
			parent::display($tpl);
	
	
			}
	}	
}