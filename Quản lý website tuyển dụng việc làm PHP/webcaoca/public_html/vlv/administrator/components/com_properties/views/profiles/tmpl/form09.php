<?php 
/**
* @copyright	Copyright(C) 2008-2010 Fabio Esteban Uzeltinger
* @license 		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @email		admin@com-property.com
**/
defined('_JEXEC') or die('Restricted access'); 
$option = JRequest::getCmd('option');
JHTML::_('behavior.tooltip');
JHTML::_('behavior.tooltip');
jimport('joomla.html.pane');
$pane =& JPane::getInstance('tabs', array('startOffset'=>0)); 
?>

<?php
require_once( JPATH_COMPONENT.DS.'helpers'.DS.'menu_left.php' );
?>
<table width="100%">
	<tr>
		<td align="left" width="200" valign="top">
<?php echo MenuLeft::ShowMenuLeft();?>
		</td>
        <td align="left" valign="top">        
<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<div class="col100">

		<table class="admintable">
<tr><td align="left" valign="top">
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'User Information' ); ?></legend>
	<table>   
			<?php ?>
         		<tr>
					<td  class="key"><label for="name"><?php echo JText::_( 'Client' ); ?>:</label></td>
					<td>
                    <div style="float:left;">
						 <?php echo SelectHelper::SelectCliente( $this->profile,'users',$this->profile->cid); ?>
                        </div>                         
					<div style="float:left; margin-left:10px; " id="AjaxCliente">                   
                    </div><div id="progressR"></div>  </td>
				</tr>  
			<?php ?>
        	<tr>
				<td class="key">
					<label for="name">
						<?php echo JText::_( 'Name' ); ?>:
					</label>
				</td>
				<td >
					<select size="1" class="inputbox" id="salutation" name="salutation">
						<option value="Mr.">Mr.</option>
						<option value="Mrs.">Mrs.</option>
					</select>
					<input class="text_area" type="text" name="name" id="name" size="20" maxlength="255" value="<?php echo $this->profile->name; ?>" />
				</td>
			</tr>  
			<!--<tr>
				<td class="key">
					<label for="name">
						<?php echo JText::_( 'Mail' ); ?>:
					</label>
				</td>
				<td >
					<input class="text_area" type="text" name="email" id="email" size="20" maxlength="255" value="<?php echo $this->profile->email; ?>" />
				</td>
			</tr> 
			<tr>
				<td class="key">
					<label for="name">
						<?php echo JText::_( 'Password' ); ?>:
					</label>
				</td>
				<td >
					<input class="text_area" type="password" name="pass" id="pass" size="20" maxlength="255" value="<?php echo $this->profile->pass; ?>" />
				</td>
			</tr>  
			--><!--<tr>
				<td class="key">
					<label for="name">
						<?php echo JText::_( 'Xac Nhan Password' ); ?>:
					</label>
				</td>
				<td >
					<input class="text_area" type="text" name="pass1" id="pass1" size="20" maxlength="255" value="" />
				</td>
			</tr>
			--></table>   
			</fieldset>
			<fieldset class="adminform">
			<legend><?php echo JText::_( 'Company Information' ); ?></legend>      
			<table> 
			<tr>
				<td class="key">
					<label for="name">
						<?php echo JText::_( 'Company Name' ); ?>:
					</label>
				</td>
				<td >
					<input class="text_area" type="text" name="company" id="company" size="20" maxlength="255" value="<?php echo $this->profile->company; ?>" />
				</td>
			</tr>
             <tr>
				<td width="100" align="right" class="key">
					<label for="name">
						<?php echo JText::_( 'Number of Company' ); ?>:
					</label>
				</td>
				<td>
					<?php echo CatTreeHelper::getPersions( $this->profile->c_name,'persions'); ?>
				</td>
			</tr>
			<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Country' ); ?>:
						</label>
					</td>
					<td >
						<?php echo SelectHelper::SelectAjaxPaises( $this->profile,'country',$this->profile->cyid); ?>
					</td>
				</tr> 
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Street Address' ); ?>:
						</label>
					</td>
					<td >
						<input class="text_area" type="text" name="address1" id="address1" size="20" maxlength="255" value="<?php echo $this->profile->address1; ?>" />
					</td>
				</tr> 
				  
              	<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Contact Name' ); ?>:
						</label>
					</td>
					<td >
						<input class="text_area" type="text" name="con_name" id="con_name" size="20" maxlength="255" value="<?php echo $this->profile->con_name; ?>" />
					</td>
				</tr>
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Website Company' ); ?>:
						</label>
					</td>
					<td >
						<input class="text_area" type="text" name="web" id="web" size="20" maxlength="255" value="<?php echo $this->profile->web; ?>" />
					</td>
				</tr>
                <tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Phone' ); ?>:
						</label>
					</td>
					<td >
						<input class="text_area" type="text" name="phone" id="phone" size="20" maxlength="255" value="<?php echo $this->profile->phone; ?>" />
					</td>
				</tr>   
                <tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Fax' ); ?>:
						</label>
					</td>
					<td >
						<input class="text_area" type="text" name="fax" id="fax" size="20" maxlength="255" value="<?php echo $this->profile->fax; ?>" />
					</td>
				</tr>   
                <tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Mobile' ); ?>:
						</label>
					</td>
					<td >
						<input class="text_area" type="text" name="mobile" id="mobile" size="20" maxlength="255" value="<?php echo $this->profile->mobile; ?>" />
					</td>
				</tr>             
        		<tr>    
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Published' ); ?>:
						</label>
					</td>
                    <td>
<?php $chequeado0 = $this->profile->published ? JText::_( '' ) : JText::_( 'checked="checked"' );?>
<?php $chequeado1 = $this->profile->published ? JText::_( 'checked="checked"' ) : JText::_( '' );?>
                    <input name="published" id="published0" value="0" <?php echo $chequeado0;?> type="radio">
	<label for="published0"><?php echo JText::_( 'No' ); ?></label>
	<input name="published" id="published1" value="1" <?php echo $chequeado1;?> type="radio">
	<label for="published1"><?php echo JText::_( 'Yes' ); ?></label>  
					</td>
				</tr>       
           		<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Time life' ); ?>:
						</label>
					</td>
					<td>
						<?php echo JText::_('From') ?>
						<?php 
							echo JHTML::calendar($this->profile->startdate,'startdate','startdate','%Y-%m-%d');
						?>
	                    <?php echo JText::_('To') ?>
						<?php 
							echo JHTML::calendar($this->profile->enddate,'enddate','enddate','%Y-%m-%d');
						?>   
						<?php  //echo Calendar::ShowCalendar( $this->datos,'category','products' ); ?>   
	                </td>
				</tr>            
	</table>
	</fieldset>
  </td>
  <td valign="top"> 
   <fieldset class="adminform">
   	<legend><?php echo JText::_( 'Logo Company' ); ?></legend>  
				<?php
                    $profile_path = $mainframe->getSiteURL().'images/properties/profiles/';
                ?>   
		<table>  	         
                <tr>
                    <td class="key"><label>
								<?php echo JText::_( 'Image' ); ?>:
							</label></td>
                    <td>                    
                    <img src="<?php echo $profile_path.$this->profile->image; ?>" width="140" height="91" /><br />
                </tr>				
                <tr>
                    <td class="key"><label>
								<?php echo JText::_( 'Change Logo Image' ); ?>:
                                <br />  Max. 140x45
							</label></td>
                    <td>
                    <input class="input_box" id="image" name="image" type="file" />
                    </td>              
                </tr>
	</table> 
    </fieldset>
    <fieldset class="adminform">
				<legend><?php echo JText::_( 'Privacy Setting' ); ?></legend>

				<table class="admintable">
				<tbody><tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Show Name' ); ?>:
						</label>
					</td>
					<td>
						
	<input type="radio" value="n" id="show_namen" name="show_name" <?php if ($this->profile->show_name=='n')echo 'checked="checked"'?>>
	<label for="show_namen">Không</label>
	<input type="radio" value="y" id="show_namey" name="show_name" <?php if ($this->profile->show_name=='y')echo 'checked="checked"'?>>
	<label for="show_namey">Yes</label>
					</td>
				</tr>
				
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Show Phone' ); ?>:
						</label>
					</td>
					<td>
	<input type="radio" value="n" id="show_phonen" name="show_phone" <?php if ($this->profile->show_phone=="n"){echo 'checked="checked"';}//;else echo ''?>>
	<label for="show_phonen">Không</label>
	<input type="radio" value="y" id="show_phoney" name="show_phone" <?php if ($this->profile->show_phone=="y"){echo 'checked="checked"';}//else echo ''?>>
	<label for="show_phoney">Yes</label>
					</td>
				</tr>
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Show Fax' ); ?>:
						</label>
					</td>
					<td>
						
	<input type="radio" value="n" id="show_faxn" name="show_fax" <?php if ($this->profile->show_fax=="n")echo 'checked="checked"'?>>
	<label for="show_faxn">Không</label>
	<input type="radio" value="y" id="show_faxy" name="show_fax" <?php if ($this->profile->show_fax=='y')echo 'checked="checked"'?>>
	<label for="show_faxy">Yes</label>
					</td>
				</tr>
				
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Show email' ); ?>:
						</label>
					</td>
					<td>
						
	<input type="radio" value="n" id="show_emailn" name="show_email" <?php if ($this->profile->show_email=='n')echo 'checked="checked"'?>>
	<label for="show_emailn">Không</label>
	<input type="radio" value="y" id="show_emaily" name="show_email" <?php if ($this->profile->show_email=='y')echo 'checked="checked"'?>>
	<label for="show_emaily">Yes</label>
					</td>
				</tr>
				
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Show Addrees' ); ?>:
						</label>
					</td>
					<td>
						
	<input type="radio" value="n" id="show_addrn" name="show_addr" <?php if ($this->profile->show_addr=='n')echo 'checked="checked"'?>>
	<label for="show_addrn">Không</label>
	<input type="radio" value="y" id="show_addry" name="show_addr" <?php if ($this->profile->show_addr=='y')echo 'checked="checked"'?>>
	<label for="show_addry">Yes</label>
					</td>
				</tr>
				
			    </tbody></table>
			</fieldset>
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Plus / Minus  Credit' ); ?></legend>

				<table class="admintable">
				<tbody>
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Credit' ); ?>:
						</label>
					</td>
					<td >
						<input class="text_area" type="textarea" name="credit" id="credit" size="20" maxlength="255" value="<?php echo $this->profile->credit; ?>" /> -1 = <?php echo JText::_( 'unlimited' ); ?>
					</td>
                   	<td width="56">
		  				<?php echo JHTML::_('tooltip', JText::_( 'You can modify what amount of properties they can add. -1 = unlimited.' )); ?>	  
            		</td>
				</tr>
				
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Kiểu' ); ?>:
						</label>
					</td>
					<td>
					<select name="type_credit">
						<option value="p">Plus</option>
						<option value="m">Minus</option>
					</select>
					
					</td>
				</tr>
				
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Sự miêu tả' ); ?>:
						</label>
					</td>
					<td>
					<textarea rows="2" cols="30" name="desc_credit" id="desc_credit"><?php echo $this->profile->desc_credit; ?></textarea>
						
					</td>
				</tr>
				
				<input type="hidden" name="ordering" value="1" />
			    </tbody></table>
			</fieldset>
 </td>
				</tr>  
	</table>       
    <div>
    <fieldset>
    	<legend><?php echo JText::_( 'Company Summary' ); ?></legend>
    
		<?php 
			$editor = &JFactory::getEditor();		
			echo $editor->display('c_summary', $this->profile->c_summary, '50%', '50', '40', '20');
		?>
	</fieldset>
    </div>
</div>
<div class="clr"></div>
<input type="hidden" name="option" value="<?php echo $option; ?>" />
<input type="hidden" name="table" value="<?php echo 'profiles'; ?>" />
<input type="hidden" name="id" value="<?php echo $this->profile->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="profiles" />
</form>
	</td>
		</tr>
			</table> 