<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view' );

class PropertiesViewMenu extends JView{
	
	function display($tpl = null)
	{
	$option = JRequest::getVar('option');
				
		JToolBarHelper::apply();
		JToolBarHelper::save();
		
		$Categories=$this->getCategories();	
		$this->assignRef('Categories',		$Categories);		
		parent::display($tpl);
		
		
	}	
	
	function getCategories()
		{
		
		global $mainframe,$option;
		$filter_category		= $mainframe->getUserStateFromRequest( "$option.filter_category",		'filter_category',		'',		'int' );
		$db =& JFactory::getDBO();		
		
		if (!$row->parent) {
			$row->parent = 0;
		}
		$query = 'SELECT * ' .
				' FROM #__properties_category ' .				
				' WHERE published != -2' .				
				' ORDER BY parent, ordering';
		$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		
		$children = array();

		if ( $mitems )
		{			
			foreach ( $mitems as $v )
			{
				$pt 	= $v->parent;
				$list 	= @$children[$pt] ? $children[$pt] : array();
				array_push( $list, $v );
				$children[$pt] = $list;
			}
		}
		$list = JHTML::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0 );
		/*echo '<pre>';
		print_r($list);
		echo '</pre>';*/
		$mitems 	= array();
		$mitems[] 	= 'PROPERTIES';

		foreach ( $list as $item ) {
			$mitems[] = '&nbsp;&nbsp;&nbsp;'. $item->treename ;
		
		}
		
		foreach ( $mitems as $item ) {
			
			$output .= '<br>'. $item;
		}

		return $output;
		
		}
}