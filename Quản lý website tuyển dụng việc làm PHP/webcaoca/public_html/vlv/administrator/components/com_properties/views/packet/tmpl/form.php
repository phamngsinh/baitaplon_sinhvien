<?php 
/**
* @copyright	Copyright(C) 2008-2010 Fabio Esteban Uzeltinger
* @license 		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @email		admin@com-property.com
**/
defined('_JEXEC') or die('Restricted access'); 
$option = JRequest::getCmd('option');
JHTML::_('behavior.tooltip');
jimport('joomla.html.pane');
$pane =& JPane::getInstance('tabs', array('startOffset'=>0)); 
?>
<?php
require_once( JPATH_COMPONENT.DS.'helpers'.DS.'menu_left.php' );
?>
<table width="100%">
	<tr>
		<td align="left" width="200" valign="top">
<?php echo MenuLeft::ShowMenuLeft();?>
		</td>
        <td align="left" valign="top">        
<form action="index.php?option=<?php echo $option ?>&view=packet" method="post" name="adminForm" id="adminForm" onsubmit="getListType(adminForm);">
<div class="col100">
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'Packet Details' ); ?></legend>
		<table class="admintable">
         <tr>
			<td width="100" align="right" class="key">
				<label for="name">
					<?php echo JText::_( 'Packet Name' ); ?>:
				</label>
			</td>
			<td>            
            <textarea class="text_area" type="text" name="name" id="name"   cols="34" rows="1"><?php echo $this->packet->name; ?></textarea>				
			</td>
		</tr>        
        <tr>
			<td width="100" align="right" class="key">
				<label for="name">
					<?php echo JText::_( 'Alias' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="alias" id="alias" size="60" maxlength="250" value="<?php echo $this->packet->alias;?>" />
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key">
				<label for="name">
					<?php echo JText::_( 'Price Type' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="price_packet" id="alias" size="60" maxlength="250" value="<?php echo $this->packet->price_packet;?>" />
			</td>
		</tr>
		<script type="text/javascript">
		function getListType(frm){
			var cat = frm.price_listtype;
			var list_type = document.getElementById('list_type');
			
			var list = '';
			for (var i=0;i<cat.options.length; i++) {
				if(i!=0) { spe = ','; } else { spe = ''; }
				if (cat.options[i].selected) {
					list += spe + cat.options[i].value;
				}
			}
			list_type.value = list;
		}
		</script>
		<tr>
			<td width="100" align="right" class="key">
				<label for="name">
					<?php echo JText::_( 'Type Packet' ); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists;?>
			</td>
		</tr>
		<input type="hidden" name="list_type" value="" id="list_type"  />
        <tr>    
			<td class="key">
				<label for="name">
					<?php echo JText::_( 'Published' ); ?>:
				</label>
			</td>
        <td>
<?php $chequeado0 = $this->packet->published ? JText::_( '' ) : JText::_( 'checked="checked"' );?>
<?php $chequeado1 = $this->packet->published ? JText::_( 'checked="checked"' ) : JText::_( '' );?>
<?php if($this->type->published==''){$chequeado1 = JText::_( 'checked="checked"' );}?>    
    <input name="published" id="published0" value="0" <?php echo $chequeado0;?> type="radio">
	<label for="published0"><?php echo JText::_( 'No' ); ?></label>
	<input name="published" id="published1" value="1" <?php echo $chequeado1;?> type="radio" onse>
	<label for="published1"><?php echo JText::_( 'Yes' ); ?></label>  
					</td>
				</tr>       
           <tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Ordering' ); ?>:
						</label>
					</td>
					<td >
						<input class="text_area" type="text" name="ordering" id="ordering" size="3" maxlength="5" value="<?php echo $this->packet->ordering; ?>" />
					</td>
				</tr>            
	</table>
	</fieldset>
</div>
<div class="clr"></div>
<input type="hidden" name="option" value="<?php echo $option; ?>" />
<input type="hidden" name="table" value="<?php echo $TableName; ?>" />
<input type="hidden" name="id" value="<?php echo $this->packet->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="packet" />
</form>
	</td>
		</tr>
			</table> 