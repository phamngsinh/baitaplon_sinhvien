<?php 
class CatTreeHelper
{	
	function checkUserLoginSeecker($userid){
		$db 	= JFactory::getDBO();
		$qry 	= "SELECT count(id) FROM #__properties_seeker WHERE mid=$userid AND published = 1";
		$db->setQuery($qry);
		return $db->loadResult();
	}
	function checkUserLoginEmploy($userid){
		$db 	= JFactory::getDBO();
		$qry 	= "SELECT id FROM #__properties_profiles WHERE mid=$userid AND published = 1";
		$db->setQuery($qry);
		return $db->loadObject();
	}
	function getJobBuyPacket(&$row,$TableName ){
		//echo "<pre>";print_r($row);exit;
		$db =& JFactory::getDBO();	
		$component_name = 'properties';
		$query = 'SELECT * ' .
				' FROM #__'.$component_name.'_packet as j' .				
				' WHERE published = 1' .				
				' ORDER BY ordering';
		$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		//print_r($mitems);
		/*$arr = array();
		$javascript = 'onChange="ChangePacket(this.value)"';
		foreach ($mitems as $mitem) {
			array_push($arr,$mitem);
		}
		$output = JHTML::_('select.genericlist',   $arr, 'jpacket', 'class="inputbox required" size="1"'.$javascript, 'id', 'name', $row );
		*/return $mitems;
	}
	//thanhtd
	function getPositions(&$row,$TableName ){
		//echo "<pre>";print_r($row);exit;
		$db =& JFactory::getDBO();	
		$component_name = 'properties';
		$query = 'SELECT * ' .
				' FROM #__'.$component_name.'_positions as e' .				
				' WHERE published = 1' .				
				' ORDER BY ordering';
		$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		$arr = array();
		$arr[0]->id = '0';
		$arr[0]->name = JText::_('Chọn chức vụ'); 
		
		foreach ($mitems as $mitem) {
			array_push($arr,$mitem);
		}
		$pid = 'pid';
		if ($TableName == 'search_pid'){
			$javascript = 'onChange="Positions_Search(this.value)"';
			$pid = 's_pid';	
		}
		$sel_row = ($row)?$row:0;
		$output = JHTML::_('select.genericlist',   $arr, $pid, 'class="inputbox required" size="1"'.$javascript, 'id', 'name', $sel_row );
		return $output;
	}
	function getEducation(&$row,$TableName ){
		//echo "<pre>";print_r($row);
		$db =& JFactory::getDBO();	
		$component_name = 'properties';
		$query = 'SELECT * ' .
				' FROM #__'.$component_name.'_education as e' .				
				' WHERE published = 1' .				
				' ORDER BY ordering';
		$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		$arr = array();
		$arr[0]->id = '0';
		$arr[0]->name = JText::_('Chọn trinh độ'); 
		
		foreach ($mitems as $mitem) {
			array_push($arr,$mitem);
		}
		$javascript = 'onChange="Education_Search(this.value)"';
		$sel_edu = ($row)?$row:0;
		$output = JHTML::_('select.genericlist',   $arr, 'eid', 'class="inputbox required" size="1"'.$javascript, 'id', 'name', $sel_edu );
		return $output;
	}
	function getJobTime(&$row,$TableName ){
		//echo "<pre>";print_r($row);exit;
		$db =& JFactory::getDBO();	
		$component_name = 'properties';
		$query = 'SELECT * ' .
				' FROM #__'.$component_name.'_jobtime as j' .				
				' WHERE published = 1' .				
				' ORDER BY ordering';
		$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		//print_r($mitems);
		$arr = array();
		foreach ($mitems as $mitem) {
			array_push($arr,$mitem);
		}
		
		$output = JHTML::_('select.genericlist',   $arr, 'jid', 'class="inputbox required" size="1"'.$javascript, 'id', 'name', $row );
		return $output;
	}
	function getJobPacket(&$row,$TableName ){
		//echo "<pre>";print_r($row);exit;
		$db =& JFactory::getDBO();	
		$component_name = 'properties';
		$query = 'SELECT * ' .
				' FROM #__'.$component_name.'_packet as j' .				
				' WHERE published = 1' .				
				' ORDER BY ordering';
		$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		//print_r($mitems);
		$arr = array();
		$javascript = 'onChange="ChangePacket(this.value)"';
		foreach ($mitems as $mitem) {
			array_push($arr,$mitem);
		}
		
		$output = JHTML::_('select.genericlist',   $arr, 'jpacket', 'class="inputbox required" size="1"'.$javascript, 'id', 'name', $row );
		return $output;
	}
	function getCheckJob(&$pack_id,&$listpack,$TableName ){
		$db 		=& JFactory::getDBO();
		//$pack_id 	= JRequest::getVar('pack_id');
		$query 		= 	"SELECT * from #__properties_packet where published = 1 and id = ".$pack_id;
		$db->setQuery( $query );				
		$resPack = $db->loadObject();
		$list_types = explode(',',$resPack->list_type);
		foreach ($list_types as $list_type) {
			if ($list_type !='')
				$arr .= $list_type.',';
		}
		$arr 	= substr($arr,0,-1);
		//echo $arr;
		$qry = "SELECT * from #__properties_typepacket where published = 1 and id IN ($arr)"; 
		$db->setQuery( $qry );				
		$res = $db->loadObjectList();
		//echo "<pre>";print_r($res);exit($qry);
		$html = '';
		$html .= '<div id="j_pack">';
		$i = 0;
		$lists = explode(',',$listpack);
		//$listpack = implode(',')
		if ($res){
			foreach ($res as $re) {
				$chek = (in_array($re->id,$lists))?'checked="checked"':'';
				$html .= $re->name.'<input class="j_p" '.$chek.' type="checkbox" id="'.$re->id.'" name="jp'.$i.'" value="'.$re->id.'" onclick="check_jp(this.id)" >';
				$i++;
			}
		}	
		$html .= '</div>';
		return $html;
	}
	function getYouKnow(&$row,$TableName ){
		//echo "<pre>";print_r($row);exit;
		$db =& JFactory::getDBO();	
		$component_name = 'properties';
		$query = 'SELECT * ' .
				' FROM #__'.$component_name.'_youknow ' .				
				' WHERE published = 1' .				
				' ORDER BY ordering';
		$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		//print_r($mitems);
		$arr = array();
		foreach ($mitems as $mitem) {
			array_push($arr,$mitem);
		}
		
		$output = JHTML::_('select.genericlist',   $arr, 'youknow', 'class="inputbox required" size="1"'.$javascript, 'id', 'name', $row );
		return $output;
	}
	function getSalary(&$row,$TableName ){
		//echo "<pre>";print_r($row);exit;
		$db =& JFactory::getDBO();	
		$component_name = 'properties';
		$query = 'SELECT * ' .
				' FROM #__'.$component_name.'_salary ' .				
				' WHERE published = 1' .				
				' ORDER BY ordering';
		$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		//print_r($mitems);
		$arr = array();
		foreach ($mitems as $mitem) {
			array_push($arr,$mitem);
		}
		
		$output = JHTML::_('select.genericlist',   $arr, 'salary', 'class="inputbox required" size="1"'.$javascript, 'id', 'name', $row );
		return $output;
	}
	function getPersions(&$row,$TableName ){
		//echo "<pre>";print_r($row);exit;
		$db =& JFactory::getDBO();	
		$component_name = 'properties';
		$query = 'SELECT * ' .
				' FROM #__'.$component_name.'_persions ' .				
				' WHERE published = 1' .				
				' ORDER BY ordering';
		$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		//print_r($mitems);
		$arr = array();
		foreach ($mitems as $mitem) {
			array_push($arr,$mitem);
		}
		
		$output = JHTML::_('select.genericlist',   $arr, 'c_name', 'class="inputbox required" size="1"'.$javascript, 'id', 'name', $row );
		return $output;
	}
	function Parent( &$row,$TableName )
	{
		$db =& JFactory::getDBO();	
		$component_name = 'properties';	
		if ( $row->id ) {
			$id = ' AND id != '.(int) $row->id;
		} else {
			$id = null;
		}
		
		if (!$row->parent) {
			$row->parent = 0;
		}
		
		$query = 'SELECT * ' .
				' FROM #__'.$component_name.'_category as c' .				
				' WHERE published != -2' .				
				' ORDER BY parent, ordering';
		$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		
		$children = array();

		if ( $mitems )
		{
			
			foreach ( $mitems as $v )
			{
				$pt 	= $v->parent;
				$list 	= @$children[$pt] ? $children[$pt] : array();
				array_push( $list, $v );
				$children[$pt] = $list;
			}
		}
		
		$list = JHTML::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0 );
print_r($datos);
		
		$mitems 	= array();
		$mitems[] 	= JHTML::_('select.option',  '0', JText::_( 'Top' ) );

		foreach ( $list as $item ) {
			$mitems[] = JHTML::_('select.option',  $item->id, '&nbsp;&nbsp;&nbsp;'. $item->treename );
		}
		
$size=5;
$field_name='parent';
$style = 'style="width: 255px;"';
if($TableName == 'articles'){$size=10;$field_name='cid';$style = 'style="width: 272px;"';}



		$output = JHTML::_('select.genericlist',   $mitems, $field_name, $style.'class="inputbox" size="'.$size.'"', 'value', 'text', $row->parent );		
		
		return $output;
	}

function ParentCategory( &$row,$TableName )
	{
		$db =& JFactory::getDBO();
		$component_name = 'properties';
		/*
		if ( $row->id ) {
			$id = ' AND id != '.(int) $row->id;
		} else {
			$id = null;
		}
		*/
		if (!$row->parent) {
			$row->parent = 0;
		}
		
		$query = 'SELECT * ' .
				' FROM #__'.$component_name.'_category as c' .				
				' WHERE published != -2' .
				$id .
				' ORDER BY parent, ordering';
		$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		
		$children = array();

		if ( $mitems )
		{
			
			foreach ( $mitems as $v )
			{
				$pt 	= $v->parent;
				$list 	= @$children[$pt] ? $children[$pt] : array();
				array_push( $list, $v );
				$children[$pt] = $list;
			}
		}
		$list = JHTML::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0 );
		print_r($datos);
		
		$mitems 	= array();
		$mitems[] 	= JHTML::_('select.option',  '0', JText::_( 'Top' ) );

		foreach ( $list as $item ) {
			$mitems[] = JHTML::_('select.option',  $item->id, '&nbsp;&nbsp;&nbsp;'. $item->treename );
		}


		$size=5;
		$field_name='parent';
		$style = 'style="width: 258px;"';
		if($TableName == 'articles'){$size=10;$field_name='cid';$style = 'style="width: 272px;"';}
		if($TableName == 'type'){$size=10;$field_name='parent';$style = 'style="width: 272px;"';}
		$output = JHTML::_('select.genericlist',   $mitems, $field_name, $style.'class="inputbox" size="'.$size.'"', 'value', 'text', $row->parent );	
		return $output;
	}



function ParentCategoryType( &$row,$TableName,$call )
	{
		//echo "<pre>";print_r($row);exit;
		$db =& JFactory::getDBO();		
		if ( $row->id ) {
			if($call=='category'){$id = ' AND id != '.(int) $row->id;}
		} else {
			$id = null;
		}
		if (!$row->cid) {
			$row->cid = 0;
		}
		$query = 'SELECT * ' .
				' FROM #__properties_'.$TableName.' ' .				
				' WHERE published != -2' .
				$id .
				' ORDER BY parent, ordering';
		$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		//echo "<pre>";print_r($mitems);exit;
		$children = array();

		if ( $mitems )
		{		
			foreach ( $mitems as $v )
			{
				$pt 	= $v->parent;
				$list 	= @$children[$pt] ? $children[$pt] : array();
				array_push( $list, $v );
				$children[$pt] = $list;
			}
		}
		
		$list = JHTML::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0 );
		//echo "<pre>";print_r($list);exit;
		$mitems 	= array();
		$mitems[] 	= JHTML::_('select.option',  '', JText::_( 'Top' ) );

		foreach ( $list as $item ) {
			$mitems[] = JHTML::_('select.option',  $item->id, '&nbsp;&nbsp;&nbsp;'. $item->treename );
		}
		//echo "<pre>";print_r($mitems);exit;
		$javascript = 'onChange="ChangeType(this.value)"';
		$output = JHTML::_('select.genericlist',   $mitems, 'cid', 'class="inputbox required" size="1"'.$javascript, 'value', 'text', $row->cid );
		//exit($output);	
		return $output;
	}
//thanhtd - search
function ParentCategorySearch( &$row,$TableName,$call )
	{
		//echo "<pre>";print_r($row);exit;
		$db =& JFactory::getDBO();		
		if ( $row->id ) {
			if($call=='category'){$id = ' AND id != '.(int) $row->id;}
		} else {
			$id = null;
		}
		if (!$row->cid) {
			$row->cid = 0;
		}
		$query = 'SELECT * ' .
				' FROM #__properties_'.$TableName.' ' .				
				' WHERE published != -2' .
				$id .
				' ORDER BY parent, ordering';
		$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		//echo "<pre>";print_r($mitems);exit;
		$children = array();

		if ( $mitems )
		{		
			foreach ( $mitems as $v )
			{
				$pt 	= $v->parent;
				$list 	= @$children[$pt] ? $children[$pt] : array();
				array_push( $list, $v );
				$children[$pt] = $list;
			}
		}
		
		$list = JHTML::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0 );
		//echo "<pre>";print_r($list);exit;
		$mitems 	= array();
		$mitems[] 	= JHTML::_('select.option',  '', JText::_( 'Chọn ngành nghề' ) );

		foreach ( $list as $item ) {
			$mitems[] = JHTML::_('select.option',  $item->id, '&nbsp;&nbsp;&nbsp;'. $item->treename );
		}
		//echo "<pre>";print_r($mitems);exit;
		$javascript = 'onChange="ChangeTypeSearch(this.value)"';
		$output = JHTML::_('select.genericlist',   $mitems, 'cid_search', 'class="inputbox required" size="1"'.$javascript, 'value', 'text', $row->cid );
		//exit($output);	
		return $output;
	}

	function MultiParent( &$row,$TableName )
	{
		$db =& JFactory::getDBO();

		$query = 'SELECT * ' .
				' FROM #__properties_'.$TableName.' as c' .				
				' WHERE published = 1' .				
				' ORDER BY name';
		$db->setQuery( $query );
		$mitems = $db->loadObjectList();

		$children = array();
		if ( $mitems )
		{			
			foreach ( $mitems as $v )
			{
				$pt 	= $v->parent;
				$list 	= @$children[$pt] ? $children[$pt] : array();
				array_push( $list, $v );
				$children[$pt] = $list;
			}
		}
			//	if ( $cid[0] ) {		
			$query = 'SELECT categoryid AS value'
			. ' FROM #__properties_product_category'
			. ' WHERE productid = '.(int) $row->id
			;
			$db->setQuery( $query );
			$lookup = $db->loadObjectList();
			if (empty( $lookup )) {
				$lookup = array( JHTML::_('select.option',  '-1' ) );
				$row->pages = 'none';
			} elseif (count($lookup) == 1 && $lookup[0]->value == 0) {
				$row->pages = 'all';
			} else {
				$row->pages = null;
			}			
	/*	} else {
			$lookup = array( JHTML::_('select.option',  0, JText::_( 'All' ) ) );
			$row->pages = 'all';
		}
		*/
					
		$list = JHTML::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0 );

		$mitems 	= array();
		$mitems[] 	= JHTML::_('select.option',  '0', JText::_( 'Varies Categories' ) );

		foreach ( $list as $item ) {
			$mitems[] = JHTML::_('select.option',  $item->id, '&nbsp;&nbsp;&nbsp;'. $item->treename );
		}
	
		$output	= JHTML::_('select.genericlist',   $mitems, 'selections[]', 'class="inputbox" size="7" multiple="multiple"', 'value', 'text', $lookup, 'selections' );	
		//print_r($mitems);
		return $output;
	}


	function Target( &$row )
	{
		$click[] = JHTML::_('select.option',  '0', JText::_( 'Parent Window With Browser Navigation' ) );
		$click[] = JHTML::_('select.option',  '1', JText::_( 'New Window With Browser Navigation' ) );
		$click[] = JHTML::_('select.option',  '2', JText::_( 'New Window Without Browser Navigation' ) );
		$target = JHTML::_('select.genericlist',   $click, 'browserNav', 'class="inputbox" size="4"', 'value', 'text', intval( $row->browserNav ) );

		return $target;
	}

function Published( &$row )
	{
		$put[] = JHTML::_('select.option',  '0', JText::_( 'No' ));
		$put[] = JHTML::_('select.option',  '1', JText::_( 'Yes' ));

		// If not a new item, trash is not an option
		if ( !$row->id ) {
			$row->published = 1;
		}
		$published = JHTML::_('select.radiolist',  $put, 'published', '', 'value', 'text', $row->published );
		return $published;
	}


	



	
function Show( &$row, $i,  $imgY = 'tick.png', $imgX = 'publish_x.png', $prefix='' )
	{
		//$i=$row->id;
		$img 	= $row->show ? $imgY : $imgX;
		$task 	= $row->show ? 'unshow' : 'show';
		$alt 	= $row->show ? JText::_( 'Show' ) : JText::_( 'Unshow' );
		$action = $row->show ? JText::_( 'Unshow' ) : JText::_( 'Show' );

		$href = '
		<a href="javascript:void(0);" onclick="return listItemTask(\'cb'. $i .'\',\''. $prefix.$task .'\')" title="'. $action .'">
		<img src="images/'. $img .'" border="0" alt="'. $alt .'" /></a>'
		;

		return $href;
	}	


	function Destacado( &$row, $i,  $imgY = 'tick.png', $imgX = 'publish_x.png', $prefix='' )
	{
		//$i=$row->id;
		$img 	= $row->featured ? $imgY : $imgX;
		$task 	= $row->featured ? 'nodestacado' : 'destacado';
		$alt 	= $row->featured ? JText::_( 'destacado' ) : JText::_( 'nodestacado' );
		$action = $row->featured ? JText::_( 'nodestacado' ) : JText::_( 'destacado' );

		$href = '
		<a href="javascript:void(0);" onclick="return listItemTask(\'cb'. $i .'\',\''. $prefix.$task .'\')" title="'. $action .'">
		<img src="images/'. $img .'" border="0" alt="'. $alt .'" /></a>'
		;

		return $href;
	}
	
		
}