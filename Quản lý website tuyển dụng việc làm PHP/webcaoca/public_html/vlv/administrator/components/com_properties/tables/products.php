<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
class Tableproducts extends JTable
{    
	/*id 	name 	alias 	parent 	agent_id 	agent 	ref 	type 	cid 	eid 	jid 	cyid 	postcode 	address 	
	description 	text 	price 	published 	use_booking 	ordering 	panoramic 	video 	lat 	lng 	available 	featured years 	bedrooms 	
	bathrooms 	garage 	area 	covered_area 	hits 	startdate 	enddate 	refresh_time 	checked_out 	checked_out_time
	*/
	var $id = null;
	var $name = null;
	var $alias = null;
	var $agent_id = null;
	var $agent = null;
	//var $ref = null;
	var $type = null;
	//var $parent = null;
	var $cid = null;
	var $sid = null;
	var $eid = null;
	var $pid = null;
	var $jid = null;
	var $cyid = null;	
	
	var $jobyear = null;
	var $salary = null;
	var $m_salary = null;
	var $i_curren = null;
	
	var $description = null;
	var $text = null;
	var $quyenloi = null;
	var $panoramic = null;	
	//var $price = null;
	var $published = null;
	//var $use_booking = null;
	var $ordering = null;
	//var $panoramic = null;	
	//var $video = null;	
	//var $lat = null;	
	//var $lng = null;	
	//var $available = null;	
	//var $featured = null;	
	//var $years = null;	
	//var $bedrooms = null;	
	//var $bathrooms = null;	
	//var $garage = null;	
	//var $area = null;	
	//var $covered_area = null;	
	var $jpacket = null;
	var $jp = null;
	var $p1 	= null;
	var $p2 	= null;
	var $p3 	= null;
	var $date1 	= null;
	var $date2 	= null;
	var $date3 	= null;
	var $hits = null;	
	var $startdate = null;	
	var $enddate = null;	
	var $refresh_time = null;	
	var $checked_out = null;	
	var $checked_out_time = null;
  
   function __construct(&$db)
  {
    parent::__construct( '#__properties_products', 'id', $db );
  }
  function check()
	{		
		if(empty($this->alias)) {
			$this->alias = $this->name;
		}
		$this->alias = JFilterOutput::stringURLSafe($this->alias);
		if(trim(str_replace('-','',$this->alias)) == '') {
			$datenow =& JFactory::getDate();
			$this->alias = $datenow->toFormat("%Y-%m-%d-%H-%M-%S");
		}
		return true;
	}
}
?>