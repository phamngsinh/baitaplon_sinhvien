<?php
defined('_JEXEC') or die('Truy cap khong hop le');

jimport('joomla.application.component.controller');

class ProductControllerCategories extends ProductController
{
 	function __construct() {
		parent::__construct();	
		$this->registerTask('unpublish', 'publish');
		$this->registerTask('add', 'edit');
	}
	function view(){
		JRequest::setVar('model','categories');
		JRequest::setVar('view','categories');
		JRequest::setVar('layout','default');
		parent::display();
	}
	function edit(){
		$model =& $this->getModel('categories');
		$view  = $this->getView('categories','edit');
		$view->setModel($model, true);
		$view->edit('edit');
	}
	function delete() {
		$cid = JRequest::getVar('cid', array(0), 'method', 'array');
		$model = $this->getModel('categories');
		foreach ($cid as $id) {
			$model->delete($id);
		}
		// Return to extensions page
		$this->setRedirect('index.php?option=com_product&view=categories', JTEXT::_('delete'));
	}
	function publish() {
		$model = $this->getModel('categories');
		$cid = JRequest::getVar ('cid', array(0), 'method', 'array');
		foreach ($cid as $id) {
			$model->publish($id);
		}

		$this->setRedirect('index.php?option=com_product&view=categories');
	}
	function save() {

		$id = JRequest::getVar('id', NULL, 'method', 'int');
		$model = $this->getModel('categories');
//		var_dump($model->save($id));die;
		if (!$model->save($id)) {
			return JError::raiseWarning(500,$url_record->getError());
		}
		// Return to extensions page
			$this->setRedirect('index.php?option=com_product&view=categories', JTEXT::_('Saved'));
	}
	function apply() {

		$id = JRequest::getVar('id', 0, 'method', 'int');
		$model = $this->getModel('categories');

		if (!$model->apply($id)) {
			return JError::raiseWarning(500, $url_record->getError());
		}

		$this->setRedirect('index.php?option=com_product&controller=categories&task=edit&cid[]='.$id, JTEXT::_('Applied'));
	}
	function cancel() {
		$this->setRedirect('index.php?option=com_product&view=categories');
	}
}

?>