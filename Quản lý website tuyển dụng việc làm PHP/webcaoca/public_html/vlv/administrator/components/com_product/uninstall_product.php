<?php
// ensure a valid entry point
defined('_JEXEC') or die('Restricted Access');
$db = &JFactory::getDBO();

//1. DROP TABLE #__link
$query 	= "DROP TABLE IF EXISTS `jos_product`;";
$db->setQuery($query);
$db->query();

//2. DROP TABLE #__desc
$query 	= "DROP TABLE IF EXISTS `jos_cate`;";
$db->setQuery($query);
$db->query();

?>