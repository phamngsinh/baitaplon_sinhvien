<?php
//install table config
$query = "CREATE TABLE IF NOT EXISTS `jos_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `categories` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `img_thump` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `img_original` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL,
  `metadesc` text COLLATE utf8_unicode_ci NOT NULL,
  `metakey` text COLLATE utf8_unicode_ci NOT NULL,
  `published` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ; ";
	$db->setQuery($query);
	if(!$db->query()){
		echo $db->getErrorMsg();
	}
	
$query = "CREATE TABLE IF NOT EXISTS `jos_cate` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `parent` int(9) NOT NULL,
  `description` text NOT NULL,
  `img_thumb` varchar(255) NOT NULL,
  `img_original` varchar(255) NOT NULL,
  `pathway` varchar(255) NOT NULL,
  `ordering` int(9) NOT NULL,
  `metadesc` text NOT NULL,
  `metakey` text NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  KEY `ordering` (`ordering`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=77 ;";
	$db->setQuery($query);
	if(!$db->query()){
		echo $db->getErrorMsg();
	}
	
?>