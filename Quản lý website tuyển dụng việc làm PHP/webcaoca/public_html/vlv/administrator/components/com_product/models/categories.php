<?php
defined('_JEXEC') or die('Truy cap khong hop le');

jimport('joomla.application.component.model');

class ProductModelCategories extends JModel {

    var $_data 			= null;

	var $_total 		= null;

	var $_pagination 	= null;

	var $_xml			= null;

	// Main constructer

	function __construct()	{

		parent::__construct();
	
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$state = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_state', '', 'string');
		$this->setState('filter.state', $state);
	}

	

	// (un)Publish Tours

	function publish($id) {
		$category =& JTable::getInstance('cate', 'Table');
		$category->load($id);
		// first remove the Joomla user account

		if ($category->published == 0) {
			$category->published = 1;
		} else {
			$category->published = 0;
		}
		$category->store();
	}

	function delete($id) {
		$category_record =& JTable::getInstance('cate', 'Table');
		if (!$category_record->load($id)) {
			return JError::raiseWarning(500, $category_record->getError());
		}

		return $category_record->delete($id);
	}
	
	function save($id) {
		$tour_record =& JTable::getInstance('cate', 'Table');
		$post = JRequest::get('post');		
		$post['description'] = JRequest::getVar( 'description', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$catid = JRequest::getVar('categories');		
		$post['categories'] = $catid;

		if (!$tour_record->load($id) && JRequest::getVar('task')=='edit' ) {

			return JError::raiseWarning(500, $tour_record->getError());
		}
		if (!$tour_record->bind($post)) {

			return JError::raiseWarning(500, $tour_record->getError());

		}
		return $tour_record->store();
	}

	

	// Apply changes

	function apply($id) {		

		$tour_record =& JTable::getInstance('cate', 'Table');

		$post = JRequest::get('post');

		// fix up special html fields

		$post['description'] = JRequest::getVar( 'description', '', 'post', 'string', JREQUEST_ALLOWRAW );

		$post['content'] = JRequest::getVar( 'content', '', 'post', 'string', JREQUEST_ALLOWRAW );
		
		if (!$tour_record->load($id)) {

			return JError::raiseWarning(500, $tour_record->getError());

		}

		if (!$tour_record->bind($post)) {

			return JError::raiseWarning(500, $tour_record->getError());

		}

		return $tour_record->store();

	}

	

	// Get edit Categories
	function &edit() {

		if (empty($this->_categories)){

			$query = 'SELECT * FROM #__cate WHERE published = 1';

			$this->_categories = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));

		}

		return $this->_categories;

	}

	function &getEditcategories() {

		if (empty($this->_categories)){

			$query = 'SELECT id,parent,name FROM #__cate WHERE published = 1';

			$this->_categories = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));

		}

		return $this->_categories;

	}

	

	// Get edit Types

	

	// Edit extension

	function &getEditdata() {

		$cid = JRequest::getVar('cid', array(0), 'method', 'array');
		
		$this->_id = $cid[0];
//		echo "<pre>";print_r($this->_id);exit;
		$tour_record =& JTable::getInstance('cate', 'Table');

		$tour_record->load($this->_id);

		$tour_record->params =  $this->getParams($tour_record->params, $tour_record->categories);

		// Read the parametes;
		return ($tour_record);

	}

	

	// Get Params

	function &getParams($params, $extension_name)	{

		$params	= new JParameter($params);



		if ($xml =& $this->_getXML($extension_name)) {

			if ($ps = & $xml->document->params) {

				foreach ($ps as $p)	{

					$params->setXML($p);

				}

			}

		}

		return $params;

	}



	// Get XML

	function &_getXML($extension_name) {

		if (!$this->_xml) {

			$xmlfile = JPATH_SITE.DS."administrator".DS."components".DS."com_product".DS.$extension_name.'.xml';



			if (file_exists($xmlfile)) {

				$xml =& JFactory::getXMLParser('Simple');

				if ($xml->loadFile($xmlfile)) {

					$this->_xml = &$xml;

				}

			}

		}

		return $this->_xml;

	}

	

	// Get data about extensions

	function &getData() {

		if (empty($this->_data)){

			$query = $this->_buildViewQuery();

			$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));

		}

		return $this->_data;

	}

	

	// Get total extensions

	function &getTotal() {

		if (empty($this->_total)){

			$query = $this->_buildViewQuery();

			$this->_total = $this->_getListCount($query);

		}

		return $this->_total;

	}

	

	// Get pagination

	function &getPagination(){

		if (empty($this->_pagination)) {

			jimport('joomla.html.pagination');

			$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));

		}

		return $this->_pagination;

	}		

	

	// Finally build query

	function _buildViewQuery() {

		$where = $this->_buildViewWhere();

		$query = 'SELECT * FROM #__cate '.$where.' ORDER BY ordering';

		return $query;

	}

	

	// Filters function

	function _buildViewWhere() {

		global $mainframe, $option;



//		$filter_state		= $mainframe->getUserStateFromRequest($option.'filter_state',		'filter_state',		'',				'word');
//
//		$search				= $mainframe->getUserStateFromRequest($option.'search',				'search',			'',				'string');
//
//		$search				= JString::strtolower($search);

		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context.'search', 'search');
		$this->setState('filter.search', $search);

		$state = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_state', '', 'string');
		$this->setState('filter.state', $state);

		$where = array();

		if ($search) {

			$where[] = 'LOWER(name) LIKE '.$this->_db->Quote('%'.$search.'%');

		}

		if ($state) {

			if ($state == 'P') {

				$where[] = 'published = 1';

			} elseif ($state == 'U') {

				$where[] = 'published = 0';

			}

		}

		$where = (count($where) ? ' WHERE '. implode(' AND ', $where) : '');

		return $where;

	}


}


?>