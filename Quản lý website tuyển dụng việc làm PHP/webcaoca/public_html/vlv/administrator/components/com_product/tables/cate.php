<?php
defined('_JEXEC') or die('Restricted access');

// Include library dependencies
jimport('joomla.filter.input');

class Tablecate extends JTable {

	var $id 	 		= null;
	var $name			= null;
	var $alias			= null;
	var $parent			= null;
	var $title			= null;
	var $description	= null;
	var $img_thumb		= null;
	var $img_original	= null;
	var $pathway		= null;
	var $ordering		= null;
	var $metadesc		= null;
	var $metakey		= null;
	var $published		= null;	
	
	function Tablecate (& $db) {
		parent::__construct('#__cate', 'id', $db);
	}

	function delete($id){	
		$k = $this->_tbl_key;
		if ($id) {
			$this->$k = intval($id);
		}
		if (parent::delete($id)) {			
			$db =& JFactory::getDBO();
			$db->setQuery("DELETE FROM `#__cate` WHERE id=$id");
			$db->query();					
		}
	}	
	function bind($array) {
		if (is_array($array['params'])) {
			$registry = new JRegistry();
			$registry->loadArray($array['params']);
			$array['params'] = $registry->toString();
		}
		return parent::bind($array);
	}
}
?>