<?php

// no direct access

defined('_JEXEC') or die('Restricted access');
// Include library dependencies

jimport('joomla.filter.input');

class Tableproduct extends JTable {
	var $id 	 		= null;
	var $name			= null;
	var $alias			= null;
	var $email			= null;
	var $website			= null;
	var $phone			= null;
	var $title			= null;
	var $description			= null;
	var $content			= null;
	var $categories			= null;	
	var $created=null;
	var $img_thump		= null;

	var $img_original	= null;
	var $ordering		= null;

	var $metadesc		= null;
	var $metakey		= null;

	var $published		= null;
	var $state=null;

	function Tableproduct (& $db) {

		parent::__construct('#__product', 'id', $db);

	}

	function delete($id){
	
		$k = $this->_tbl_key;

		if ($id) {
			$this->$k = intval($id);
		}

		if (parent::delete($id)) {
			// Make some updates after uninstall

			$db =& JFactory::getDBO();

			// Remove already created URLs for this extension from database

			$db->setQuery("DELETE FROM `#__product` WHERE id=$id");

			$db->query();					
		}
	}

	function bind($array) {

		if (is_array($array['params'])) {

			$registry = new JRegistry();

			$registry->loadArray($array['params']);

			$array['params'] = $registry->toString();

		}
		return parent::bind($array);
	}
}

?>