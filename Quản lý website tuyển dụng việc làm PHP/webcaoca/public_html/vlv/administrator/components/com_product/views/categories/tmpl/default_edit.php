<?php
// No Restricted access
defined( '_JEXEC' ) or die( 'Restricted access' );

$bar =& JToolBar::getInstance();
JToolBarHelper::title(JText::_('Edit Categories: ').' '.$this->row->name , 'generic');
JToolBarHelper::save('save');
//JToolBarHelper::apply('apply');
JToolBarHelper::cancel('cancel');

JRequest::setVar('hidemainmenu', 1);
$editor =& JFactory::getEditor();

jimport('joomla.filter.output');
JFilterOutput::objectHTMLSafe($row, ENT_QUOTES, 'content');

$document =& JFactory::getDocument();

JHTML::_('behavior.combobox');

jimport('joomla.html.pane');
$pane =& JPane::getInstance('sliders');
//onSubmit="return setCatVal();"
JHTML::_('behavior.tooltip');
?>
<script language="javascript">
	function setCatVal(){
		var pathway = document.getElementById('pathway');
		var parent= document.getElementById('parent');
		var last_comma = pathway.value.lastIndexOf(',');
		parent.value = pathway.value.substring(last_comma+1, pathway.value.length)
		return true;
	}
	function outputSelected(selRef) {
		var typeslist = document.getElementById('typeslist');
		var types = document.getElementById('types');
		var lists = '';
		for (var i=0;i<selRef.options.length; i++) {
			if(i!=0) { spe = ','; } else { spe = ''; }
			if (selRef.options[i].selected) {
				lists+= spe + selRef.options[i].value;
			}
		}
		typeslist.value = lists;
		types.value = lists;
	 }
</script>
<form action="index.php" method="post" name="adminForm" >
	<div class="col width-70">
		<fieldset class="adminform">
			<legend><?php echo JText::_('Details'); ?></legend>

		<table class="admintable" cellspacing="1">
			<tr>
				<td valign="top" class="key">
					<?php echo JText::_('Title'); ?>:
				</td>
				<td>
					<input type="text" name="name" id="name" value="<?php echo $this->row->name; ?>" class="require inputbox" size="140">
				</td>
			</tr>
			<tr>
				<td valign="top" class="key">
					<?php echo JText::_('Alias'); ?>:
				</td>
				<td>
					<input type="text" name="alias" id="alias" value="<?php echo $this->row->alias; ?>" class="require inputbox" size="140">
				</td>
			</tr>
						
			<tr>
				<td valign="top">
					<?php echo JText::_('Published'); ?>:
				</td>
				<td>
					<?php echo $this->lists['published']; ?>
				</td>
			</tr>
			<tr>
				<td valign="top" class="key">
					<?php echo JText::_('Parent Categories'); ?>:
				</td>
				<td>
					<input type="hidden" name="parent" id="parent" value="<?php echo $this->row->parent; ?>">
					
					<?php echo $this->lists['pathway']; ?>
				</td>
			</tr>				
			<tr>
				<td valign="top" class="key">
					<?php echo JText::_('Description'); ?>:
				</td>
				<td>
					<?php echo $editor->display( 'description',  $this->row->description, '600', '150', '60', '10', array('pagebreak', 'readmore') ) ; ?>
				</td>
			</tr>
			</table>
		</fieldset>
	</div>
	
	<div class="clr">
	</div>
	<input type="hidden" name="option" value="com_product" />
	<input type="hidden" name="controller" value="categories" />
	<input type="hidden" name="task" value="edit" />
	<input type="hidden" name="id" value="<?php echo $this->row->id; ?>" />
</form>