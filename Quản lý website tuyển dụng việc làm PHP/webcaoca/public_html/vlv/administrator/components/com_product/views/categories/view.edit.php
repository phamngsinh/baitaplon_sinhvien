<?php

// no direct access
defined('_JEXEC') or die('Restricted Access');

// Import View
jimport('joomla.application.component.view');

class ProductViewCategories extends JView {

	// Edit category
	function edit ($tpl = NULL) {
		$this->_layout = 'edit';
		$row 		= & $this->get('Editdata');
		$categories = & $this->get('Editcategories');
		
		$params 	= $row->params;
		$lists['published'] = JHTML::_('select.booleanlist',  'published', 'class="inputbox"', $row->published);
		$sl_cat->id=0;	
		$sl_cat->name='Home'; 
		$sl_cat->pathway=0;	
		$first_cat[] = $sl_cat;
		$cats=array_merge($first_cat, $categories);
			foreach($categories as $otherCat) {
				if($otherCat->parent ==0) {
					$parentcats[] = $otherCat;	
						
				} else {
					$subcats[] = $otherCat;
					
				}
			}
			//if ($parentcats){
			foreach($parentcats as $parentcat) {
				if($subcats){
					foreach($subcats as $subcat) {
						if($subcat->parent == $parentcat->id) {
							$parentcat->subs[] = $subcat;			
						} else {			
							$re[] = $subcat;
						}
					}
				} 
				$newcats[] = $parentcat;
				
			}
		//echo "<pre>";print_r($newcats);exit;
		
		$lists['pathway'] = '<select name="parent" id="parent" size="10">';
			$lists['pathway'] .= '<option value="0" '.$row->parent.'==0 ? " selected " : ""; >Home</option>';
			 foreach ($newcats as $cat){
				$selected = ($cat->id==$row->parent)?'selected="selected"':"";
				$lists['pathway'] .= '<option value="'.$cat->id.'" '.$selected.'>
					&nbsp;&nbsp;&nbsp;&nbsp;|_&nbsp;'.$cat->name.'</option>';	
											
				if(count($cat->subs) >0){
					 foreach ($cat->subs as $otherSub) {
					 	$db = &JFactory::getDBO();
						$query = "SELECT id, parent, name FROM #__cate WHERE parent='".$otherSub->id."' ORDER BY id";
						$db->setQuery($query);
						$listCatSubs = $db->loadObjectList();
						$subselect = ($otherSub->id==$row->parent)?'selected="selected"':""; 
					 	$lists['pathway'] .= '<option value="'.$otherSub->id.'" '.$subselect.'>
				 			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|_&nbsp;'.$otherSub->name.'</option>';
					 	foreach ($listCatSubs as $listCatSub) {
							$lists['pathway'] .= '<option value="'.$listCatSub->id.'" >
				 			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|_&nbsp;'.$listCatSub->name.'</option>';
						}
					 }
				}
				
			}
		$lists['pathway'] .= '</select>';
		$this->assignRef('categories', $categories);
		$this->assignRef('row', 		$row);
		$this->assignRef('lists',		$lists);
		$this->assignRef('params',		$params);

		$this->_layout = 'default';

		parent::display($tpl);
	}
}
?>