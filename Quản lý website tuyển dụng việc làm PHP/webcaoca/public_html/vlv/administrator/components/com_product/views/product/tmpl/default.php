<?php

defined('_JEXEC') or die('Restricted access');

JToolBarHelper::title(JText::_('Product'), 'generic.png');

JToolBarHelper::addNewX();

JToolBarHelper::publishList('publish', JTEXT::_('Publish'));

JToolBarHelper::unpublishList('unpublish', JTEXT::_('UnPublish'));

JToolBarHelper::deleteList(JTEXT::_('Delete'), 'delete');

//JToolBarHelper::preferences('com_product');

require_once JPATH_COMPONENT.DS.'assets'.DS.'helperProduct.php';

?>
<script>

function submitbutton(pressbutton) {
	submitform(pressbutton);
}
function submitform(pressbutton){
	if (pressbutton) {
		document.adminForm.task.value=pressbutton;
	}
	if (typeof document.adminForm.onsubmit == "function") {
		document.adminForm.onsubmit();
	}
	document.adminForm.submit();
}
</script>
<form action="index.php?option=com_product" method="post" name="adminForm">
	<table>
		<tr>
			<td align="left" width="100%">

				<?php echo JText::_( 'Filter' ); ?>:

				<input type="text" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />

				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>

				<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_catid').value='0';this.form.getElementById('filter_state').value='';this.form.submit();"><?php echo JText::_( 'Filter Reset' ); ?></button>

			</td>

			<td nowrap="nowrap">
				<?php
				echo $lists['categories'];
				echo $lists['state'];
				?>

			</td>
		</tr>

	</table>

	<div id="editcell">

		<table class="adminlist">

		<thead>
			<tr>
				<th width="5">
					<?php echo JText::_('Row'); ?>
				</th>

				<th width="20">
					<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->items); ?>);" />
				</th>

				<th class="name">

					<?php echo  JTEXT::_('Name Company'); ?>

				</th>		

				<th class="website">

					<?php echo  JTEXT::_('Website'); ?>

				</th>

				
				<th class="description">

					<?php echo  JTEXT::_('Description'); ?>

				</th>

				<th width="5%" nowrap="nowrap">

					<?php echo JTEXT::_( 'Published' ); ?>

				</th>

				<th width="10%" class="categories">

					<?php echo JTEXT::_('Categories'); ?>

				</th>

				<th width="5%" nowrap="nowrap">

					<?php echo JTEXT::_( 'Item' ); ?>

				</th>
			</tr>

		</thead>
		
		<tfoot>
			<tr>
				<td colspan="9">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>

		</tfoot>

		<tbody>

		<?php

		$k = 0;

		for ($i=0, $n=count($this->items); $i < $n; $i++) {

			$row = &$this->items[$i];

			$link 	= JRoute::_('index.php?option=com_product&controller=product&task=edit&cid[]='.$row->id);
			$checked 		= JHTML::_('grid.checkedout', $row, $i);
//			echo('<pre>');print_r($checked);exit(' Ã¡dg');
			$published 		= JHTML::_('grid.published', $row, $i);
			//$mod = $this->getModule('product');
			$state = HelperProduct::Destacado($row, $i);
			?>

			<tr class="<?php echo "row$k"; ?>">
				<td>
					<?php echo $this->pagination->getRowOffset($i); ?>
				</td>
				<td>
					<?php echo $checked; ?>
				</td>
				
				<td>
					<a href="<?php echo $link; ?>" title="<?php echo JText::_('Name Company'); ?>">
						<?php echo $row->title; ?>
					</a>
				</td>		
				
				<td>
					<a href="<?php echo $row->website?>" title="<?php echo JText::_('Website'); ?>">
						<?php echo $row->website; ?>
					</a>
				</td>		
				
				<td>
						<?php echo $row->description; ?>
				</td>
				
				<td align="center">
					<a title="<?php echo JText::_('Published'); ?>">
					<?php echo $published; ?></a>
				</td>

				<td align="center">
					<?php echo $row->catname; ?>
				</td>

				<td align="center"><?php echo $row->id; ?></td>

			</tr>

			<?php
			$k = 1 - $k;
		}
			?>

		</tbody>

		</table>

	</div>

	<input type="hidden" name="option" value="com_product" />

	<input type="hidden" name="controller" value="product" />

	<input type="hidden" name="task" value="view" />

	<input type="hidden" name="boxchecked" value="0" />

	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />

	<input type="hidden" name="filter_order_Dir" value="" />

</form>