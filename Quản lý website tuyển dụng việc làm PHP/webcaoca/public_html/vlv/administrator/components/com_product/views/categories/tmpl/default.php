<?php

defined('_JEXEC') or die('Restricted access');

JToolBarHelper::title(JText::_('Categories'), 'generic.png');

JToolBarHelper::addNewX();

JToolBarHelper::publishList('publish', JTEXT::_('Publish'));

JToolBarHelper::unpublishList('unpublish', JTEXT::_('UnPublish'));

JToolBarHelper::deleteList(JTEXT::_('delete'), 'delete');

//JToolBarHelper::preferences('com_product');

?>

<form action="index.php?option=com_product" method="post" name="adminForm">

	<table>

		<tr>

			<td align="left" width="100%">

				<?php echo JText::_( 'Filter' ); ?>:

				<input type="text" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />

				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>

				<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_catid').value='0';this.form.getElementById('filter_state').value='';this.form.submit();"><?php echo JText::_( 'Filter Reset' ); ?></button>

			</td>

			<td nowrap="nowrap">

				<?php

				echo $lists['categories'];

				echo $lists['state'];

				?>

			</td>

		</tr>

	</table>

	<div id="editcell">

		<table class="adminlist">

		<thead>

			<tr>

				<th width="5">

					<?php echo JText::_('row'); ?>

				</th>

				<th width="20">

					<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->items); ?>);" />

				</th>

				<th class="categories">

					<?php echo  JTEXT::_('Categories'); ?>

				</th>				

				<th width="10%" nowrap="nowrap" align="center">

					<?php echo JTEXT::_( 'Access Level' ); ?>

				</th>

				<th width="10%" nowrap="nowrap" align="center">

					<?php echo JTEXT::_( 'Parent Categories' ); ?>

				</th>			

				<th width="10%" nowrap="nowrap" align="center">

					<?php echo JTEXT::_( 'Id' ); ?>

				</th>

			</tr>

		</thead>

		<tfoot>

			<tr>

				<td colspan="9">

					<?php echo $this->pagination->getListFooter(); ?>

				</td>

			</tr>

		</tfoot>

		<tbody>

		<?php

		$k = 0;

		for ($i=0, $n=count($this->items); $i < $n; $i++) {

			$row = &$this->items[$i];

			$link 	= JRoute::_('index.php?option=com_product&controller=categories&task=edit&cid[]='.$row->id);
			$checked 		= JHTML::_('grid.checkedout', $row, $i);

			$published 		= JHTML::_('grid.published', $row, $i);
		?>

			<tr class="<?php echo "row$k"; ?>">

				<td>

					<?php echo $this->pagination->getRowOffset($i); ?>

				</td>

				<td>

					<?php echo $checked; ?>

				</td>

				<td>

					<a href="<?php echo $link; ?>" title="<?php echo JText::_('Edit Categories'); ?>">

						<?php echo $row->name; ?>

					</a>

				</td>				

				<td align="center">

					<?php echo $published; ?>

				</td>

				<td align="center">

					<?php echo $row->parent; ?>

				</td>

				<td align="center"><?php echo $row->id; ?></td>

			</tr>

			<?php

			$k = 1 - $k;

		}

		?>

		</tbody>

		</table>

	</div>

	<input type="hidden" name="option" value="com_product" />

	<input type="hidden" name="controller" value="categories" />

	<input type="hidden" name="task" value="view" />

	<input type="hidden" name="boxchecked" value="0" />

	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />

	<input type="hidden" name="filter_order_Dir" value="" />

</form>