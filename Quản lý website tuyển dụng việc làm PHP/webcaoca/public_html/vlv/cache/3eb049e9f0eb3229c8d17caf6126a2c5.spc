a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:37:"The Software Freedom Law Center Blog.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:36:"http://www.softwarefreedom.org/blog/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:45:"All blogs at the Software Freedom Law Center.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"en-us";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:13:"lastBuildDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 14 Aug 2010 06:00:00 -0500";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:10:{i:0;a:6:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:25:"The BlackBerry Emergency
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:69:"http://www.softwarefreedom.org/blog/2010/aug/14/blackberry-emergency/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4938:"<p><i>Blog post by <strong>Mishi Choudhary</strong>.  Please email any comments on this entry to <a href="mailto:Mishi@softwarefreedom.org">&lt;Mishi@softwarefreedom.org&gt;</a>.</i></p>
<p>According to the Government of India, private service providers like AirTel and Vodafone are failing in their legal obligations under the Information Technology Act, hastily
 amended in the days immediately following the Mumbai 7/11 attacks, by not providing
 access to the content of emails and texts sent to or from BlackBerry
 users.  As a lawyer, I have some doubt about this legal position, no
 doubt under discussion between GoI and the service providers.  But there is no
 doubt that the Government has failed to make clear the context of
 this dispute, or the real consequences of the demands it is making.</p>


<p>BlackBerry devices use the wireless networks of the local service providers to
 deliver email and texts through servers operated by Research in Motion
 located outside India.  If you or I as individuals buy a BlackBerry
 through one of the offering service providers, our email and text traffic will
 not be encrypted, and GoI will have whatever access to our
 communications the law requires.  If, however, your BlackBerry was
 given to you as an employee of an MNC or a large local enterprise, for
 work use, those emails and texts will be encrypted so that only the
 sender and receiver, but not Research in Motion (RIM) and not the local Indian wireless
 service provider, will be able to read them.  Since these parties do not have access to the content of encrypted messages, and
 therefore cannot provide what Government says the Act requires, the
 Government now threatens to force a halt to their services as of
 August 31.</p>



<p>Unless a ring of terrorists is embedded entirely within some MNC, and
 is using its email and messaging system to plan terrorist attacks or other
 crimes using corporate BlackBerries, such a service cut would not be
 likely to prevent the planning or execution of any attacks.  What it
 would do, however, is effectively cut off India from the global
 financial system.  The ability of banks, insurance companies, law
 firms, consultancies and other professional service enterprises to
 operate around the globe depends entirely on the flow of confidential intra-firm communications.
 People cannot do business anywhere unless they can be sure that their
 firm's business communications are not being overheard by competitors
 or other parties using breaches in communications networks.  So
 every such enterprise relies upon mechanisms that ensure complete
 confidentiality on which the movement of trillions of crores every day
 in the world economy depend.  BlackBerry provides one portion of that
 network to a large subset of that market.  Any country which shuts
 off encrypted BlackBerry communications has shut down its place in the
 global economy.</p>



<p>Government knows, what the extent of its threat implies if our connection with the global economy is

temporarily lost.  But if the Government were clear with the public now about the small security benefit to gain
 and the magnitude of the harm  it will cause if its threat is carried out, its dis-proportionality would raise questions in the mind of the public. Apparently GoI believes that such a threat can, from its very desperate
 dramatic quality, induce a useful result.
 Unfortunately, this too is wrong.  Because nobody but the enterprises themselves have an access to the decrypted information, Government must get inside the BlackBerry itself if it is to read the messages.</p>


</p>Thus, it is likely that GoI is pressurizing the local service providers like Airtel and Vodafone to put spyware within the
 BlackBerries attached to their networks. Thus, an arriving investment
 banker or CEO from New York or Frankfurt would have his BlackBerry
 subject to the introduction of spyware by the network, along with all
 the BlackBerries used by Indian financial services firms.  There is
 precedent for this effort.  One UAE wireless company, Etisalat, was
 caught installing spyware on more than 100,000 enterprise BlackBerries
 in the Emirates last year.  Research in Motion was required by its
 customers to bear the cost of software upgrades to the system to
remove the spyware and secure their business communications.  Etisalat
 has been fundamentally injured in its credibility in international
 business, and is in some danger of becoming a global pariah.</p>



<p>GoI is making threats that could only be fulfilled at
 cataclysmic cost to the economy. It will in effect result in causing immense harm to India's telecommunications sector and our
 reputation in the global financial services economy, where so many of
 our jobs are being created. In the end, it would inflict immense damage, much greater than any terrorist could ever cause scarcely achieving any
 additional security.</p>




";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:43:"Mishi@softwarefreedom.org (Mishi Choudhary)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 14 Aug 2010 06:00:00 -0500";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:69:"http://www.softwarefreedom.org/blog/2010/aug/14/blackberry-emergency/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:43:"Software Patents Post Bilski: A Look Ahead
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:90:"http://www.softwarefreedom.org/blog/2010/jul/16/software-patents-post-ibilskii-look-ahead/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:10598:"<p><i>Blog post by <strong>Michael A. Spiegel</strong>.  Please email any comments on this entry to <a href="mailto:mspiegel@softwarefreedom.org">&lt;mspiegel@softwarefreedom.org&gt;</a>.</i></p>
<P ALIGN=LEFT STYLE="margin-bottom: 0in; line-height: 150%"><SPAN STYLE="font-style: normal">	</SPAN><SPAN STYLE="font-style: normal">In
the haze of confusion surrounding the Supreme Court’s recent
decision in </SPAN><I>Bilski v. Kappos</I><SPAN STYLE="font-style: normal">,
the appeals board of the United States Patent and Trademark Office
issued a ruling last week </SPAN><SPAN STYLE="font-style: normal">that</SPAN><SPAN STYLE="font-style: normal">
takes a definitive stand against the worst kinds of patents </SPAN><SPAN STYLE="font-style: normal">that</SPAN><SPAN STYLE="font-style: normal">
threaten software developers every day. </SPAN>
</P>
<P ALIGN=LEFT STYLE="margin-bottom: 0in; line-height: 150%"><SPAN STYLE="font-style: normal">	Despite
the </SPAN><SPAN STYLE="font-style: normal">Court's failure to
provide much guidance or adopt a bright-line test for patentable
subject matter in </SPAN><I>Bilski</I><SPAN STYLE="font-style: normal">,
the appeals board </SPAN><A HREF="http://www.awakenip.com/?p=456">ruling</A>

in <I>Ex parte Proudler</I> is a sign of the growing skepticism
towards software patents that continually test the boundary between
acceptable technological innovation and impermissible abstraction. In<I>
Proudler</I><SPAN STYLE="font-style: normal">, the Board rejected a
number of claims to a software invention, citing </SPAN><I>Bilski </I><SPAN STYLE="font-style: normal">in
its </SPAN><SPAN STYLE="font-style: normal">reasoning</SPAN><SPAN STYLE="font-style: normal">.
</SPAN>
</P>
<P ALIGN=LEFT STYLE="margin-bottom: 0in; line-height: 150%"><SPAN STYLE="font-style: normal">	</SPAN><SPAN STYLE="font-style: normal">This
rejection is particularly noteworthy as it dispenses with a number of
fig leaves that patent attorneys have been using for years to make
software inventions seem less like abstract ideas, </SPAN><SPAN STYLE="font-style: normal">and
therefore patentable</SPAN><SPAN STYLE="font-style: normal">. </SPAN><SPAN STYLE="font-style: normal">While
the USPTO has always held that software is unpatentable, patent
attorneys were usually able to get software patents </SPAN><SPAN STYLE="font-style: normal">granted</SPAN><SPAN STYLE="font-style: normal">

by </SPAN><SPAN STYLE="font-style: normal">adding </SPAN><SPAN STYLE="font-style: normal">such
seemingly magical phrases as </SPAN><SPAN STYLE="font-style: normal">“a
computer readable medium containing computer executable instructions”
</SPAN><SPAN STYLE="font-style: normal">to a series of data
processing  steps, thus </SPAN><SPAN STYLE="font-style: normal">transforming
software into a </SPAN><SPAN STYLE="font-style: normal">patentable
physical component of a computer. </SPAN>
</P>
<P ALIGN=LEFT STYLE="margin-bottom: 0in; line-height: 150%">	Taking a
page from the same playbook<SPAN STYLE="font-style: normal">, </SPAN><SPAN STYLE="font-style: normal">the</SPAN><SPAN STYLE="font-style: normal">
</SPAN><I>Proudler </I><SPAN STYLE="font-style: normal">application</SPAN><SPAN STYLE="font-style: normal">

claims </SPAN><SPAN STYLE="font-style: normal">a</SPAN><SPAN STYLE="font-style: normal">
“method of controlling processing of data in a computer apparatus”
and </SPAN><SPAN STYLE="font-style: normal">a </SPAN><SPAN STYLE="font-style: normal">“computer
program stored on computer readable media for instructing a
programmable computer to implement a method of controlling the
processing of data.” </SPAN><SPAN STYLE="font-style: normal">Since
the Federal Circuit’s infamous 1998  </SPAN><I>State Street
</I><SPAN STYLE="font-style: normal">decision which opened the
floodgates to software and business method patents</SPAN><SPAN STYLE="font-style: normal">,
such claims have been deemed patentable without question. Although
the Board could have justified the July 7th decision on other
grounds, it took particular pains to reject </SPAN><SPAN STYLE="font-style: normal">the
</SPAN><I>Proudler</I><SPAN STYLE="font-style: normal"> </SPAN><SPAN STYLE="font-style: normal">application
for</SPAN><SPAN STYLE="font-style: normal"> </SPAN><SPAN STYLE="font-style: normal">claiming</SPAN><SPAN STYLE="font-style: normal">

unpatentable software</SPAN><SPAN STYLE="font-style: normal">.</SPAN><SPAN STYLE="font-style: normal"><SPAN STYLE="text-decoration: none">
</SPAN></SPAN><SPAN STYLE="font-style: normal"><U>“A claim that
recites no more than software, logic or a data structure (i.e., an
abstraction) does not fall within any statutory category,” the
Board said in its rejection.</U></SPAN><SPAN STYLE="font-style: normal"><SPAN STYLE="text-decoration: none">
</SPAN></SPAN><SPAN STYLE="font-style: normal">In its <A HREF="http://www.awakenip.com/postbilski/proudler.pdf">rejection</A>,
t</SPAN><SPAN STYLE="font-style: normal">he Board cited both recent
Supreme Court cases (including </SPAN><I>Bilski</I><SPAN STYLE="font-style: normal">)
as well as pre-</SPAN><I>State Street </I><SPAN STYLE="font-style: normal">decisions
from the Federal Circuit. </SPAN>
</P>
<P ALIGN=LEFT STYLE="margin-bottom: 0in; line-height: 150%"><SPAN STYLE="font-style: normal">	</SPAN><SPAN STYLE="font-style: normal">I
believe the Board correctly deduced two notable things from </SPAN><I>Bilski</I><SPAN STYLE="font-style: normal">,

</SPAN><SPAN STYLE="font-style: normal">in an interpretation </SPAN><SPAN STYLE="font-style: normal">which
hopefully will take root in </SPAN><SPAN STYLE="font-style: normal">the
</SPAN><SPAN STYLE="font-style: normal">courts as well.</SPAN></P>
<P ALIGN=LEFT STYLE="margin-bottom: 0in; line-height: 150%"><SPAN STYLE="font-style: normal">	First,
the “machine-or-transformation test”, in which a patentable
process must either be closely tied to a particular machine or
transform a particular article into a different state of thing,
</SPAN><SPAN STYLE="font-style: normal">survives as a </SPAN><SPAN STYLE="font-style: normal">primary
test for routine use</SPAN><SPAN STYLE="font-style: normal">.
Although the </SPAN><SPAN STYLE="font-style: normal">Supreme</SPAN><SPAN STYLE="font-style: normal">
Court held that “the machine-or-transformation test </SPAN><SPAN STYLE="font-style: normal">is
not the sole test for deciding whether an invention is a
patent-eligible ‘process’,” it did characterize the test as “a
useful and important clue.” </SPAN><SPAN STYLE="font-style: normal">Without</SPAN><SPAN STYLE="font-style: normal">
any hint of what processes, if any, may fail this test while still
being eligible for a patent, the burden </SPAN><SPAN STYLE="font-style: normal">will
lie</SPAN><SPAN STYLE="font-style: normal"> </SPAN><SPAN STYLE="font-style: normal">with</SPAN><SPAN STYLE="font-style: normal">

software patent applicants to prove that their processes are
patentable after failing the machine-or-transformation test.</SPAN></P>
<P STYLE="margin-bottom: 0in; line-height: 150%"> 	Second, the <I>Bilski</I>
decision effectively kills the “useful, tangible, and concrete”
test long favored by patent attorneys who sought to overcome
rejections for ineligible subject matter. As the USPTO has tightened
its guidelines for computer-related inventions over the years, many
applicants cleverly decided to draft their software claims to appear
as if their process was tied to a particular machine. However, for
those patents which were granted on the basis that their inventions
were “useful, tangible, and concrete”?most notably software
patents granted immediately following <I>State Street</I>?the
Court’s decision in <I>Bilski</I> makes these patents especially
vulnerable. 
</P>
<P ALIGN=LEFT STYLE="margin-bottom: 0in; line-height: 150%"><SPAN STYLE="font-style: normal">	Assuming
this </SPAN><SPAN STYLE="font-style: normal">characterization</SPAN><SPAN STYLE="font-style: normal">

of </SPAN><I>Bilski</I><SPAN STYLE="font-style: normal"> survives </SPAN><SPAN STYLE="font-style: normal">on
appeal</SPAN><SPAN STYLE="font-style: normal">, I think we will start
to see the USPTO take more aggressive steps to stanch the flow of the
worst types of software patents. One need only look at the specific
claims in the </SPAN><A HREF="http://www.awakenip.com/?p=456"><I>Proudler</I></A><A HREF="http://www.awakenip.com/?p=456"><SPAN STYLE="font-style: normal">
application</SPAN></A><SPAN STYLE="font-style: normal"> to see how
patents of this type </SPAN><SPAN STYLE="font-style: normal">stifle
innovation in computer technology. I can</SPAN><SPAN STYLE="font-style: normal">not</SPAN><SPAN STYLE="font-style: normal">
think of any computer program that does not have “logically related
data items” processed by associated rules. The rest of the claim
language contain unintelligible nonsense—the perfect weapon to use
against small software developers who can’t afford to defend
themselves against patent aggression.</SPAN></P>
<P STYLE="margin-bottom: 0in; line-height: 150%"> 	So what will the
status of software patents be going forward?   Given the Supreme
Court’s reluctance to categorically exclude any type of invention
from patent-eligibility in the face of unforeseen developments in
technology, I believe that at least some forms of software will
remain patentable, barring any (unlikely) legislative response to the
software patent issue. 

</P>
<P STYLE="margin-bottom: 0in; line-height: 150%"> 	Expect to see
software patent claims for “computer readable mediums” encoded
with “processor executable instructions” challenged under the
machine-or-transformation test as not being tied to a specific
machine. These challenges may prove successful in invalidating the
most abstract of computer software patents, to which patent
applicants will respond by drafting process claims with ever
increasing references to specific computer hardware. I also expect to
see an increasing reliance on system claims with generic hardware
elements, such as a processor or computer network, configured to
perform steps performed in software. The  patentability question for
computer software running on general-purpose computers will be
decided on the Court’s clear disfavor for “abstract ideas” as
seen in the context of the reaffirmed <I>Benson</I>-<I>Flook</I>-<I>Diehr</I>
trilogy of Supreme Court cases. The language in these cases which
deny patents to “algorithms” and “mental processes” may prove
useful in invalidating the most harmful of software patents.</P>
</BODY>
</HTML>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:49:"mspiegel@softwarefreedom.org (Michael A. Spiegel)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 16 Jul 2010 12:17:00 -0500";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:90:"http://www.softwarefreedom.org/blog/2010/jul/16/software-patents-post-ibilskii-look-ahead/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:45:"The 300th Anniversary of the Statute of Anne
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:79:"http://www.softwarefreedom.org/blog/2010/apr/10/300th-anniversary-statute-anne/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3911:"<p><i>Blog post by <strong>Laura Moy</strong>.  Please email any comments on this entry to <a href="mailto:Laura Moy@softwarefreedom.org">&lt;Laura Moy@softwarefreedom.org&gt;</a>.</i></p>
<p>This weekend marks the 300th anniversary of the Statute of Anne, widely considered to be the foundation of modern copyright law. The Statute, formally titled “An Act for the Encouragement of Learning," is often praised for placing intellectual property rights in the hands of individual authors and ushering in the era of public interest copyright law. In reality, however, the law's most lasting legacy has been the misguided proposition that “the Encouragement of Learned Men to Compose and Write useful Books" cannot be accomplished without copyright: a system which restricts the flow and use of information, chills collaboration, and presupposes that property rights are necessary to encourage innovation.</p>

<p>The passage of the Statute represents the culmination of a centuries-long partnership between the monarchy and publishers, the effects of which continue to be felt today. In 16th century England, religious and political unrest, coupled with the proliferation of printed media, threatened to compromise the crown's ability to control the masses. Queen Mary and King Philip engineered a clever media censorship scheme in response. In 1556 they struck a Machiavellian deal with the guild of London publishers: the publishers would cooperate with the crown to prevent the publication of undesirable works, and in exchange, the crown would grant them a monopoly over intellectual property rights and the power to destroy unlawful books and presses.</p>

</p>By the end of the 17th century, this partnership lapsed, threatening the publishers' monopoly. The publishers tried repeatedly to reinstitute the scheme, but amidst the growing importance of the electorate and an increasing hostility to private monopolies, all their efforts failed. The publishers had to change their strategy. If they were unable to reestablish copyright all for themselves, the next best thing for them would be to assign property rights directly to authors, who, unable to print and distribute their works on their own, would have no choice but to contract with the publishers. Publishers could then bargain with the authors to get exclusive publication rights, in essence perpetuating their monopoly over books.</p>

<p>With this goal in mind, the publishers convinced Parliament that the creation of strong intellectual property rights was essential to encourage the advancement of learning.</p>

<p>So the Statute of Anne was born, and on April 10, 1710, became law. The Statute of Anne, which, like modern copyright, granted exclusive rights directly to authors for limited terms, was intended by Parliament to promote “the Encouragement of Learned Men to Compose and Write useful Books.” Intellectual property, which restricts individuals' use of each other's ideas, had formerly always been conceived as a tool of censorship. That it was now suddenly championed as a tool to encourage information sharing denotes nothing less than a radical shift and is testament to the success of the publishers in convincing Parliament that no one does anything for free.</p>

</p>This unfounded presumption that is now so deeply ingrained in modern conceptions of intellectual property, is the unfortunate legacy of the Statute of Anne. Hence the misguided challenge posed to the FLOSS movement with frustrating frequency: How can Free Software possibly provide adequate incentives to developers in the absence of strong proprietary intellectual property rights? Of course, our model does work. The fundamental flaw informing this question lies not in the shortcomings of the collaborative innovation model, but in the ancient assumption that only property rights can provide an effective incentive to write and publish.</p>

";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:41:"Laura Moy@softwarefreedom.org (Laura Moy)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 10 Apr 2010 11:57:00 -0500";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:79:"http://www.softwarefreedom.org/blog/2010/apr/10/300th-anniversary-statute-anne/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:49:"Gene Patenting and Free Software: A Breakthrough
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:94:"http://www.softwarefreedom.org/blog/2010/apr/06/gene-patenting-and-free-software-breaktrhough/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5431:"<p><i>Blog post by <strong>Eben Moglen</strong>.  Please email any comments on this entry to <a href="mailto:eben@softwarefreedom.org">&lt;eben@softwarefreedom.org&gt;</a>.</i></p>
<p><em>[Crossposted
  from <a href="http://opensource.com/law/10/4/gene-patenting-and-free-software-breakthrough">Opensource.com</a>].</em></p>

<p>Last week, to the surprise of patent lawyers and the biotechnology industry, advocates for technological freedom won an enormous victory against socially harmful distortions of patent law.  The Federal District Court for the Southern District of New York held invalid patents owned by Myriad Genetics on diagnostic testing for genetic susceptibility to the most common hereditary forms of breast and ovarian cancer.  By "patenting" the right to determine whether the BRCA1 and BRCA2 genes are present in the relevant mutated form in a women's genome, Myriad Genetics has been able to exclude all other laboratories from conducting the test.  Patients and their insurers have paid much more, and women and their families have waited crucial weeks longer than necessary for information relevant to treatment and potentially affecting survival.</p> 

<p>The Public Patent Foundation and the American Civil Liberties Union challenged the patent on the ground that the Act does not permit the patenting of "facts of nature."  <a href="http://www.aclu.org/files/assets/2010-3-29-AMPvUSPTO-Opinion.pdf">In a lengthy and carefully argued opinion</a> granting summary judgment, Judge Robert Sweet agreed. Judge Sweet rejected the basic premise on which gene testing patents such as the one granted to Myriad have been justified: that the amplification of naturally-occurring DNA sequences is a patentable transformation of the DNA molecule.  Instead, Judge Sweet adopted the view put forward by Myriad's own expert witnesses, that DNA is a special molecule, "a physical carrier of information," and therefore held that the reading of such naturally-occurring information is not patentable subject matter.  Whether posed as a new composition of matter, or as a method for "analyzing" or "comparing" DNA sequences, Judge Sweet held, Myriad's attempt to gain a monopoly on looking at a particular DNA sequence to find out what it says falls outside the permissible scope of patent law.</p>

<p>In reaching his legal conclusions, Judge Sweet relied significantly on the recent opinion of the Court of Appeals for the Federal Circuit, which has primary responsibility for interpreting the nation's patent law, <a href="http://www.softwarefreedom.org/resources/2009/bilski-resources.html">In re Bilski, 535 F.3d 943 (2008)</a>, now pending in the Supreme Court. Bilski, as readers here will know, raises issues concerning the patentability of business methods and computer software, on essentially the same basic ground: that, as the Supreme Court has said, "phenomena of nature, though just discovered, mental processes, and abstract intellectual concepts are not patentable, as they are the basic tools of scientific and technological work." Gottschalk v. Benson, 409 U.S. 63, 67 (1972).  Judge Sweet's opinion may be said to raise the stakes on Bilski slightly, but the parts of the Federal Circuit opinion on which Judge Sweet relies are not about the "specialized machine or transformation of matter" test adopted by the Federal Circuit to distinguish patentable from unpatentable inventions involving computer software and methods of doing business. Judge Sweet followed the Federal Circuit closely in its expression of the settled law of patent scope, making it more unlikely that the Federal Circuit, which will hear the inevitable appeal from Judge Sweet's judgment, will be inclined to disturb the conclusion.</p>

</p>Instead, Judge Sweet's ruling shows the beginning of a broader front in the judicial determination to reign in patenting that has gone too far, turning information that should be free to all into property exclusively held by a few.  Neither our patent law itself, nor the guarantees of freedom to learn and teach protected by the First Amendment, can tolerate the widespread creation of statutory monopolies on ideas.  Judge Sweet's conclusion with respect to gene patenting confirms and supports the position taken by the amici curiae in Bilski, <a href="http://www.softwarefreedom.org/resources/2009/bilski-amicus-brief.html">including the Software Freedom Law Center</a>, that computer software standing by itself, another carrier of information about algorithms, or mental processes, is not within the scope of patent law.  Judge Sweet's opinion illuminates another of the large classes of human knowledge presently being made the subject of statutory monopolies through the patent system, but which cannot legally be made monopolies at all.</p>

<p>Americans have begun to understand a little bit about how, in the last two decades, corporations and their servants turned more and more of our society's opportunities into property for themselves.  The sorrow and anger that is entering our politics, as honorable working people realize how badly they were had, will not soon abate.  That the patent system too was gamed by the powerful at the expense of everybody else has not been fully grasped yet.  But it will be.  Time will show that Judge Sweet was more than courageous in his ruling, that he was also speaking with the voice of America behind him, as all great judges do.</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:38:"eben@softwarefreedom.org (Eben Moglen)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 06 Apr 2010 14:50:00 -0500";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:94:"http://www.softwarefreedom.org/blog/2010/apr/06/gene-patenting-and-free-software-breaktrhough/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:25:"Free Software: Phase Two
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:72:"http://www.softwarefreedom.org/blog/2010/mar/29/free-software-phase-two/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4146:"<p><i>Blog post by <strong>Lysandra Ohrstrom</strong>.  Please email any comments on this entry to <a href="mailto:Lohrstrom@softwarefreedom.org">&lt;Lohrstrom@softwarefreedom.org&gt;</a>.</i></p>
<p>Free software is ubiquitous. It runs everywhere on (almost) everything. The question that dominated most of the  discussions at the Libre Planet Conference in Boston about a week ago is what now? How can the community capitalize on its achievements to make the movement more inclusive and reconceive the relationship between free software and privacy?</p>

<p>Most attendees seem to agree that it's time to proselytize to the non-hacker masses and get them to care about the privacy, freedom, and control they sacrifice when buying proprietary technology. At John Gilmore's group discussion on the future of free software Saturday morning, people proposed making the user interface more friendly; addressing freedom in the browser space; developing a solid gaming platform for free software. "My experience is that if you give people who play games the option to improve them they will," one attendee said. "I know people who became programmers so they could improve the games they played."</p> 

<p>The SFLC's founding director, Eben Moglen, said in his talk that the movement has reached "a point of inflection." The challenge it will face in "Free Software: Phase Two" is to explain the relationship between privacy, the integrity of human personality, and free software. The movement will have to figure out how to convince people they need a solution to a problem they don't know exists, he said. "It's not about we're done. The war is over. It's about, what's next."</p> 

<p>I think ordinary people who don't write computer code or think about the consequences of proprietary technology are aware that there is a problem; that they are forfeiting too much control over the tasks and relationships that make-up daily life to unknown forces in the digital world. The cyber security threats, malicious hacker attacks in China, car malfunctions, and voting booth problems that dominate the news cycle can all be traced back to potential software glitches. People know about these problems. They just don't know how the problems are connected or how to fix them.</p> 

<p>The challenge facing the free software community is to explain how the Toyota recall and Google's withdrawal from China can be traced to one cause: namely giving for-profit companies monopolistic access to the source code of the thousands of software programs that we are increasingly reliant upon. What's lost in the mainstream media coverage of these seemingly unrelated events is an explanation of a solution that already exists: open, auditable source code that anyone can view and detect flaws in.</p> 

<p>The first step for the SFLC and attendees of Libre Planet is to connect the various news headlines to a central problem: restricted access to source code. Then explain that it doesn't have to be this way. The technology wasn't designed to infantilize users. It was designed to give users a set of tools to collaborate and communicate in a digital world.</p> 

</p>I think the long-term challenge facing the free software community is even more fundamental than telling people they need a solution to a problem they did not know existed. You need to remind people that they don't know how the machines they interact with every day work. You need to open the doors of the community to the end-users for whom the difference between C++ and Java Script is as foreign as Dari and Pashto. The challenge is to encourage people who are used to having a passive relationship with the technology they use to search inside their computers; to show them that digital citizenship is about more than curating the photos you post on Facebook and limiting the information you transmit online. It's about self-sufficiency. It's about first understanding how computers work and customizing them, and then taking control of your own information and the activities you have entrusted to Wizard of Oz-type entities for no other reason than you didn't think you had a choice.</p> 

";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:49:"Lohrstrom@softwarefreedom.org (Lysandra Ohrstrom)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 29 Mar 2010 10:52:00 -0500";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:72:"http://www.softwarefreedom.org/blog/2010/mar/29/free-software-phase-two/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:42:"New Export Rules Promote Internet Freedom
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:90:"http://www.softwarefreedom.org/blog/2010/mar/12/new-export-rules-promote-internet-freedom/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4987:"<p><i>Blog post by <strong>James Vasile</strong>.  Please email any comments on this entry to <a href="mailto:vasile@softwarefreedom.org">&lt;vasile@softwarefreedom.org&gt;</a>.</i></p>
<p>Earlier this week, the <a href="http://www.ustreas.gov/ofac">Office
of Foreign Assets Control</a> announced
the <a href="http://www.ustreas.gov/press/releases/tg577.htm">
relaxation</a> of rules prohibiting export of software to Iran and
Sudan.  The new exemptions build on a recent <a href="
http://www.ustreas.gov/offices/enforcement/ofac/legal/regs/CACR_Amendments_09032009.pdf">easing
of some rules governing exporting telecommunications technology to
Cuba</a>.  These moves are surely an attempt to capitalize on the
Iranian election demonstrations last summer that some called
the <a href="http://www.washingtonpost.com/wp-dyn/content/discussion/2009/06/17/DI2009061702232.html">"Twitter
Revolution"</a>.  They are also a sign that the Obama Administration
is carrying out its plans to make internet freedom a
<a href=http://www.foreignpolicy.com/articles/2010/01/21/internet_freedom>pillar
of US diplomacy</a>.</p>

<p>I hope the revised OFAC rules are the beginning of a broad and
nuanced re-examination of US technology export policy. They are
certainly good news for Free Software developers who are currently
prohibited from distributing their software in embargoed
countries.</p>

<p>Generally speaking, US companies have been prohibited from trading
with Iran and Sudan, despite some exemptions for humanitarian and
medical reasons. To my knowledge, those exemptions have never been
applied to Free Software.  Against that backdrop of general
prohibition, OFAC
<a href="http://www.ustreas.gov/offices/enforcement/ofac/programs/iran/gls/soc_net.pdf">promulgated
new exceptions</a> allowing the exportation of "certain services and
software incident to the exchange of personal communications over the
Internet."  As examples, OFAC lists "instant messaging, chat and
e-mail, social networking, sharing of photos and movies, web browsing
and blogging."</p>

<p>That list, however, is not exhaustive.  The exemption is broad and
applies to a wide range of technology. I asked an OFAC representative
how broadly to interpret "incident to," and he confirmed that the
exemption includes such software as web servers, content management
systems or operating systems on which one could run an IM client.  If
those are incident to personal communication, presumably much other
Free Software is too.</p>

<p>Though the decision to loosen export regulations is a step in the
right direction, it falls far short of what the Free Software world
really needs: permission to publicly distribute Free Software
everywhere on the planet without jumping through invisible and
innumerable hoops. First, the new rules are limited to Iran and Sudan.
Other embargoed countries are still off limits.  Second, while
"incident to personal communication" applies to a lot of software, it
does not describe everything, and its limits are unclear. Third, the
exception is limited to software that is publicly available at no cost
to the user.</p>

<p>This last point deserves some extra attention.  If you charge for
Free Software, the exemption does not apply.  Also, other export
controls are still in effect.  This is true no matter how you
structure it.  A download fee to recoup the cost of distribution would
be a problem.  A paid service agreement to support the software would
be a problem.  Accepting donations from embargoed countries could be
problematic.  In other words, if your project gives away software but
raises money by accepting money for ancillary goods or services, those
ancillary charges can run afoul of the embargo rules.</p>

<p>Other caveats exist (especially regarding software that does
encryption), and this post isn't meant to be legal advice on how to
comply.  My objective is to shed light on a decision that could signal
the beginning of a gradual shift in US technology policy and a shift
in the Free Software development landscape.  If your project is trying
to comply with export regulation, you should seek legal advice.</p>

<p>Export regulation is a patchwork quilt of mystifying rules.  Most
projects are wholly unaware that putting their code on the web might
put them at risk of violating export laws they've never heard of.
Those that try to comply
face <a href="http://sourceforge.net/blog/some-good-news-sourceforge-removes-blanket-blocking/">many
difficulties</a>. It's heartening to see inter-agency cooperation
aimed at simplifying the legal environment for Free Software
developers. By broadening exemptions to include certain communication
technologies, the government acknowledges that internet access is a
fundamental human right. These are welcome signs for the SFLC and the
free software community overall. I hope the Commerce Department joins
in and extends the personal communication technology exemption to
include trade with Cuba and Syria as well.</p>

";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:41:"vasile@softwarefreedom.org (James Vasile)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 12 Mar 2010 06:00:00 -0500";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:90:"http://www.softwarefreedom.org/blog/2010/mar/12/new-export-rules-promote-internet-freedom/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:63:"The Toyota Recall and the Case for Open, Auditable Source Code
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:98:"http://www.softwarefreedom.org/blog/2010/feb/19/toyota-recall-and-case-open-auditable-source-code/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4388:"<p><i>Blog post by <strong>Michael A. Spiegel</strong>.  Please email any comments on this entry to <a href="mailto:mspiegel@softwarefreedom.org">&lt;mspiegel@softwarefreedom.org&gt;</a>.</i></p>
<h3>Public Safety is not a matter of Private Concern</h3>

<p>In a recent <a href="http://www.slate.com/id/2244887/">article</a>,
Slate's Farhad Manjoo attempts to play down fears of faulty software
in car braking systems as a potential cause of traffic
accidents. Citing numerous studies which conclude that &#8220;the
overwhelming reason we get in crashes is driver error,&#8221; Manjoo
reasons that &#8220;the less driving people do, the fewer people will
die on the roads.&#8221;</p>

<p>While it may certainly be true that most crashes occur because of
intoxication, distraction, or driver fatigue, and that computer
controlled cars may decrease driver error, Manjoo doesn't seem to see
the obvious implication of his own assumptions -- &#8220;opaque&#8221;
and &#8220;inherently buggy&#8221; software which could endanger
public safety should be subject to review.</p>

<p>If Toyota truly wanted to repair its public image and reputation
for quality, it would make its source code available to anyone
interested, not just a single government regulator. The public is far
more likely to discover bugs and suggest improvements than a
relatively small number of overworked and potentially inexperienced
government employees. As a former patent examiner at the US Patent and
Trademark Office, I have seen the problems that arise when the amount of information and technical
expertise available to the government is far outstripped by that of the private firms
seeking government approval. Currently, the USPTO is attempting to
deal with this imbalance of information by publishing patent
applications before they are granted and by considering various
proposals to incorporate public feedback as a means to improve patent
quality. The National Highway Traffic Safety Administration should
consider similar measures to allow the public to assist in its
work.</p>

<p>Toyota should take their cue from another industry recently wracked
by a loss of confidence in the integrity of their product -- the
voting machine industry. Looking back on the controversies that surrounded voting
irregularities in the past few elections, it seems like the public
cares a great deal about the integrity of the voting process. A
seemingly endless amount of ink was spilled by the press and
blogosphere expressing outrage over the various security flaws found
in Diebold voting machines, especially after the CEO of Diebold
Inc. <a
href="http://www.commondreams.org/headlines03/0828-08.htm">wrote</a>
that he is &#8220;committed to helping Ohio deliver its electoral
votes to the president next year.&#8221; The media attention
surrounding this issue culminated in the HBO documentary <a
href="http://www.hackingdemocracy.com/">&#8220;Hacking
Democracy&#8221;</a>, in which filmmakers Simon Ardizzone &amp;
Russell Michaels chronicled the efforts of activists who exposed and
attempted to fight the proliferation of insecure voting machines.</p>

<p> Finally, in response to the controversy, Sequoia Voting
Systems <a href="http://www.sequoiavote.com/press.php?ID=85">announced</a>
last October that their new voting machines would be based on publicly
available source code and open architectures, noting that
&#8220;[s]ecurity through obfuscation and secrecy is not
security&#8221; and that &#8220;[f]ully disclosed source code is the
path to true transparency and confidence in the voting process for all
involved.&#8221;</p>

<p>I find it curious how proprietary software became a major concern
to the media as well as various state legislatures when our democratic
process was threatened, but when <a
href="http://www.usatoday.com/money/autos/2010-02-17-toyota17_ST_N.htm">at
least 37 lives have been lost</a> due to malfunctioning Toyota
vehicles, there is no similar outcry for greater transparency in the
proprietary braking and accelerating software that is crucial to
keeping people safe on the road.</p>

<p>Given the cost of its 8.5 million car recall and the potentially
irrecoverable damage to its brand, Toyota should seriously reconsider
the value of maintaining a business based on trade secrets and realize
that ensuring public safety should not be purely a matter of private
concern.</p>

";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:49:"mspiegel@softwarefreedom.org (Michael A. Spiegel)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 19 Feb 2010 17:12:00 -0500";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:98:"http://www.softwarefreedom.org/blog/2010/feb/19/toyota-recall-and-case-open-auditable-source-code/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:46:"Letter to the Editor(s) of The New York Times
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:78:"http://www.softwarefreedom.org/blog/2010/jan/22/letter-editors-new-york-times/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4598:"<p><i>Blog post by <strong>Lysandra Ohrstrom</strong>.  Please email any comments on this entry to <a href="mailto:Lohrstrom@softwarefreedom.org">&lt;Lohrstrom@softwarefreedom.org&gt;</a>.</i></p>
<p><i>New York Times</i> reporters John Markoff and Ashlee Vance correctly pointed out that nations, private corporations, and even bands of rogue programmers are capable of covertly tunneling into information systems, by exploiting bugs in a program's source code in their January 20th story, <a href=http://www.nytimes.com/2010/01/20/technology/20code.html>"Fearing Hackers Who Leave no Trace."</a></p> 
<p>Unfortunately, this paragraph appears more than half way through the story. Rather than address the real reasons that proprietary systems are so vulnerable to security breaches or the comparative protections afforded by free software, Vance and Markoff stuck to the usual formula of the <i>Times</i>' technology stories: the "intellectual property"  parable. The good guys ("innovative,"  commercial technology companies like Google and Cisco) struggle to protect the integrity of their source code--their "crown jewels"--from the nefarious designs of the bad guys (hackers).</p>
  
<p>"If hackers could steal those key instructions and copy them, they could easily dull the company's competitive edge in the marketplace," Vance and Markoff write. "More insidiously, if attackers were able to make subtle, undetected changes to that code, they could essentially give themselves secret access to everything the company and its customers did with that software.”</p> 
  
<p>The ability to make "subtle, undetected changes" to a system's source code is indeed the cause of frequent security breaches, but it is much harder for ill-intentioned hackers to "leave no trace" in free software. The solution is not to block access to source code, as the authors imply, but keep it open so that anyone can detect modifications, see who made them, and why. The reason closed, proprietary systems have proven to be less secure than free software alternatives over the years is more to do with the absence of a third-party audit function and accountability trail, than threats from "hackers."</p>
  
</p>Markoff and Vance conclude their story with a quote from a security expert who acknowledges that technology companies have implemented "more complex systems for viewing and changing source code" lately to combat this problem, but "one of the greatest vulnerabilities remains the
people element." Free software has proven that  the people element can also be a source of security.</p>
  
<p>I can only assume the confusing tangle of inaccuracies, contradictory conclusions, and false assumptions Markoff and Vance reported were fed to them by a PR agent or cobbled together after a day-long conference hosted by the Homeland Security Council. Vance quotes a member of the Council's advisory board, Jeff Moss, in the article he co-wrote with Markoff and in a separate story published under his own byline on January 21, <a href=http://www.nytimes.com/2010/01/21/technology/21password.html>"If your Password is 123456</a>, Just Make It HackMe."  Vance describes him as a  security expert in the first story and the founder of  a popular hackers conference in the second.</p>
  
<p>As a non-profit law firm for Free and Open Source Software programmers, the SFLC admittedly has a stake in promoting the projects developed by its clients. So rather than take my word for it, listen to the comments of readers like Wai Yip Tung from San Francisco. "This article have a very confused idea around source code and security," Tung writes in a comment. "Have you ever heard of Open Source software? (e.g. Firefox) Everyone has access to its source code but it does not make it any less secure. In fact security expert have an opposite opinion. It is critical that source code is available for audit by a wide range of people before we can have confidence of its security. This is a worthless article in my opinion. It stroke fear in the wrong places and shed little light on where the vulnerability really is.</p>
  
<p>I have to disagree that their effort was entirely worthless. Markoff and Vance inadvertently ended up writing an unqualified endorsement of free software in <i>The New York Times</i>.</p>

<p><i>Clarification: The "hackers" who break into computers with the intent to cause harm are distinct from from the community of programmers who create clever solutions to bugs in source code. A more accurate label for the activity in this story is malicious hacking or cracking.</i></p> 
  
  
  


";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:49:"Lohrstrom@softwarefreedom.org (Lysandra Ohrstrom)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 22 Jan 2010 16:06:00 -0500";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:78:"http://www.softwarefreedom.org/blog/2010/jan/22/letter-editors-new-york-times/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:69:"CES 2010: The Best of Times and the Worst of Times for Free Software
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:98:"http://www.softwarefreedom.org/blog/2010/jan/15/ces-2010-best-times-and-worst-times-free-software/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3977:"<p><i>Blog post by <strong>Lysandra Ohrstrom</strong>.  Please email any comments on this entry to <a href="mailto:Lohrstrom@softwarefreedom.org">&lt;Lohrstrom@softwarefreedom.org&gt;</a>.</i></p>
<p>The 2010 Consumer Electronics Show (CES) was the best of times and the worst of times for the free software movement.</p>
  
<p>It proved that the first stage of the revolution that the SFLC, and many others, have long predicted, is complete: free software has been embraced by commercial developers and is now powering a much wider range of embedded devices than any single proprietary program. Behind any one of the smartphones, eBook Readers, netbooks and LCD-televisions that debuted last week at CES is almost certainly a Free and Open Source Software (FOSS) application or platform.</p>
  
<p>But it also highlighted the extent to which proprietary software developers have taken advantage of the ability to adapt, improve, and customize free software in embedded devices that deny customers these very same freedoms. This is the paradox of free software's growing popularity: anyone can view, modify, and distribute source code, whatever their intentions.</p>
  
<p>Now that free software is inside almost everything, it is time for users to learn about their rights under free software licenses to protect their freedom to share, tinker, and adapt the devices they buy.</p>
  
<p>These values, ingrained into our consciousness in kindergarten, are the ones that drive free software innovation and that many of the commercial developers at CES have benefited from, yet hope their customers will forget.</p>
  
<p>Most of the eBook Readers at CES use free software in locked-down devices that restrict customers' access to certain publications, prevent them from sharing, and violate their privacy. Telecommunications companies have harnessed Android in the battle for a larger share of the smartphone market and collaborated on applications with FOSS programmers while preventing customers the right to chose between carriers. These companies have a vested interest in limiting the functionality of the devices they sell so consumers buy the next model in a couple of years, rather than improve the one they already own.</p>
  
<p>Individuals can reclaim the rights they take for granted in other commodities by educating themselves and choosing to buy the most free devices whenever possible.</p>
  
<p>While none of the smartphones or e-readers on the market is perfect, there are options that allow users to retain more choice and control of how a device functions and what content they can access. Choosing a Nokia N900 or a free Android phone over a carrier-subsidized, locked-down model gives customers the option of running any application and making modifications so it is more useful. Readers can chose an open-format e-reader over an alternative that only allows them to download books in proprietary format.</p>

<p>Owners of open-format e-readers can download public-domain content, such as  "<i>A Tale of Two Cities</i>,"  for free from a variety of sources and be able to read and re-read it whenever they want, on multiple devices, forever. Amazon, on the other hand, charges Kindle-owners a license fee for the privilege of reading the free Dickens' classic on their e-readers. Amazon also reserves the right to revoke this license at any time and yank the free book from Kindle-customers who already paid to download it.</p>
  
<p>At CES 2011, the SFLC predicts that commercial software developers will continue to distract users from these basic rights with sleeker, smaller, and smarter devices that run on free software. The SFLC will continue to fight against people who violate the terms of the GPL by using FOSS in locked-down devices that restrict, rather than empower users. But the second stage of the revolution will be won by individuals who educate themselves about their rights in order to realize the full benefit of free software.</p>
  
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:49:"Lohrstrom@softwarefreedom.org (Lysandra Ohrstrom)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 15 Jan 2010 10:08:00 -0500";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:98:"http://www.softwarefreedom.org/blog/2010/jan/15/ces-2010-best-times-and-worst-times-free-software/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:39:"The European Commission and Oracle-Sun
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:83:"http://www.softwarefreedom.org/blog/2009/dec/14/european-commission-and-oracle-sun/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:857:"<p><i>Blog post by <strong>Eben Moglen</strong>.  Please email any comments on this entry to <a href="mailto:eben@softwarefreedom.org">&lt;eben@softwarefreedom.org&gt;</a>.</i></p>
<p>I spent last Thursday and Friday in Brussels, attending the European Commission’s Oral Hearing in the competition investigation of the acquisition of Sun Microsystems by Oracle. The proceedings at the Oral Hearing were confidential; I cannot write about the presentations made there by others. I can, however, summarize the three points I made during my brief presentation on Friday; my previous written submission to the commission is already available. I want to explain what I said and where I think we stand now that the Oral Hearing is over.</p>

<a href="http://emoglen.law.columbia.edu/blog/cases/oracle-sun/ec-hearing-and-after.html?seemore=y">Full post here</a>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:38:"eben@softwarefreedom.org (Eben Moglen)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 14 Dec 2009 11:18:00 -0500";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:83:"http://www.softwarefreedom.org/blog/2009/dec/14/european-commission-and-oracle-sun/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";a:14:{s:4:"date";s:29:"Sat, 11 Dec 2010 10:38:30 GMT";s:6:"server";s:96:"Apache/2.2.8 (Ubuntu) DAV/2 SVN/1.5.1 mod_python/3.3.1 Python/2.5.2 mod_ssl/2.2.8 OpenSSL/0.9.8g";s:7:"expires";s:29:"Sat, 11 Dec 2010 10:48:30 GMT";s:4:"vary";s:6:"Cookie";s:13:"last-modified";s:29:"Sat, 11 Dec 2010 10:38:30 GMT";s:4:"etag";s:32:"7ddad8a04013e51ffd85521884e86f3a";s:13:"cache-control";s:11:"max-age=600";s:12:"content-type";s:19:"application/rss+xml";s:3:"age";s:3:"122";s:14:"content-length";s:5:"55817";s:7:"x-cache";s:22:"HIT from john-marshall";s:14:"x-cache-lookup";s:27:"HIT from john-marshall:3128";s:3:"via";s:42:"1.1 john-marshall:3128 (squid/2.7.STABLE9)";s:10:"connection";s:5:"close";}s:5:"build";s:14:"20090627192103";}