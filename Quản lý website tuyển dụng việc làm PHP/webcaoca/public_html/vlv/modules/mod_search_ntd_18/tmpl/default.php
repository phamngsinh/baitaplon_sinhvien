<?php // no direct access
defined('_JEXEC') or die('Restricted access');
$link_js = JURI::root().'media/system/js/mootools.js';
$link_js_search = JURI::root().'modules/mod_search_ntd/assets/js-search.js';
$doc = JFactory::getDocument();
$doc->addScript($link_js);
$doc->addScript($link_js_search);

require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_properties'.DS.'helpers'.DS.'helper.php');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_properties'.DS.'helpers'.DS.'select.php');

$row_product = '';
$row_product->cid = 0;
$row_product->type = 0;
$row_product->sid = 0;
$row_product->eid = 0;
$row_product->pid = 0;
?>
<script type="text/javascript">
/*window.addEvent('domready', function() {
    alert('The DOM is ready!');
});*/
</script>
<?php 
	$Itemid = '123';
	$aug = '';
	if(JRequest::getVar('cs_search'))$cs_search		= JRequest::getVar('cs_search', 0, '', 'int');	
	$aug .= '&cs_search='.$cs_search;
	//if(JRequest::getVar('cid_search'))$cid_search		= JRequest::getVar('cid_search', 0, '', 'int');	
	//$aug .= '&cid_search='.$cid_search;
	if(JRequest::getVar('type_search'))$type_search	= JRequest::getVar('type_search', 0, '', 'int');	
	$aug .= '&type_search='.$type_search;
	if(JRequest::getVar('eid'))$eid			= JRequest::getVar('eid', 0, '', 'int');			
	$aug .= '&eid='.$eid;
	//if(JRequest::getVar('pid'))$pid			= JRequest::getVar('pid', 0, '', 'int');			
	//$aug .= '&pid='.$pid;
	//if(JRequest::getVar('work_experience'))$work_experience	= JRequest::getVar('work_experience', 0, '', 'int');	
	//$aug .= '&work_experience='.$work_experience;
	if(JRequest::getVar('price_currency'))$price_currency		= JRequest::getVar('price_currency', 0, '', 'int');	
	$aug .= '&price_currency='.$price_currency;
	$link_search = JRoute::_('index.php?option=com_properties&view=search_ntd'.$aug.'&Itemid='.$Itemid);
?>
<style type="text/css">
	#ket_qua_img {
		background:url("modules/mod_search_left/tmpl/ajaxloading.gif") no-repeat scroll 100% 60% transparent;
		float:right;
		height:16px;
		width:16px;
	}
</style>
<div class="box-left">
<h3><span>Tìm CV ứng viên</span></h3>
<div class="search_viec">
	<form action="<?php echo $link_search;?>" method="get">
		<input type="hidden" value="com_properties" name="option" />
		<input type="hidden" value="search_ntd" name="view" />
		<p>
			<?php 
				$row->id=0;
				$row->sid = $cs_search;//$row_product->sid;
				echo SelectHelper::SelectAjaxContries( $row,'country',$row_product->cyid); ?>
			<div id="progressccc"></div>
		</p>
		<?php /*?>
		<div id="AjaxState" style="float:left" >
	          <?php 
			  $row->id=0; 
			  $row->cyid = $row_product->cyid;
			  $row->sid = $row_product->sid;
			  echo SelectHelper::SelectAjaxStates( $row,'states',$row_product->sid ); 
			  ?>
        </div>
		<?php */?>
        <div id="progressS"></div>
        <?php /*?>
		<p>
			<?php 
				$row->id=0;
				$row->cid = $cid_search;
				echo CatTreeHelper::ParentCategorySearch( $row,'category','products' ); ?>
		</p>
		<?php */?>
		<p>
             <?php 
			  $row->id=0;
			  $row->parent = $row_product->cid;
			  $row->type = $type_search;
			  echo SelectHelper::SelectType_Search( $row,'type','form_products' ); 
		  ?>              
  		</p>
  		<div id="progressTSearch"></div> 
		<p>
			<?php echo CatTreeHelper::getEducation( $eid,'education'); ?>
		</p>
		<p style="display: none;">
			<?php echo CatTreeHelper::getPositions( $pid,'search_pid'); ?>
		</p>
			<p>
		<?php 
			
		?>
		<select id="price_currency" name="price_currency" onChange="Price_Search(this.value)">
			<option value="0" <?php if($price_currency == 0) echo 'selected="selected"';?>>Chọn mức lương</option>
			<option value="1" <?php if($price_currency == 1) echo 'selected="selected"';?>>Thỏa Thuận</option>
			<option value="2" <?php if($price_currency == 2) echo 'selected="selected"';?>>1 triệu - 3 triệu</option>
			<option value="3" <?php if($price_currency == 3) echo 'selected="selected"';?>>3 triệu - 5 triệu</option>
			<option value="4" <?php if($price_currency == 4) echo 'selected="selected"';?>>5 triệu - 10 triệu</option>
			<option value="5" <?php if($price_currency == 5) echo 'selected="selected"';?>>trên 10 triệu</option>
		</select>
		</p>
			<p style="display: none;">
		<select id="work_experience" name="work_experience" onchange="Experience_Search(this.value)">
				  <option value="0" <?php if($work_experience == 0) echo 'selected="selected"';?>>Chọn số năm kinh nghiệm</option>
				  <option value="1" <?php if($work_experience == 1) echo 'selected="selected"';?>>Dưới 1 năm</option>
				  <option value="2" <?php if($work_experience == 2) echo 'selected="selected"';?>>1 năm</option>
				  <option value="3" <?php if($work_experience == 3) echo 'selected="selected"';?>>2 năm</option>
				  <option value="4" <?php if($work_experience == 4) echo 'selected="selected"';?>>3 năm</option>
				  <option value="5" <?php if($work_experience == 5) echo 'selected="selected"';?>>4 năm</option>
				  <option value="6" <?php if($work_experience == 6) echo 'selected="selected"';?>>Hơn 5 năm</option>
		</select>
		</p>
	<div class="buton_shearch" align="right">
		<div align="left">
			<?php echo JText::_('Kết quả ')?> :<span id="ket_qua"></span><span id="ket_qua_img" style="visibility:hidden"></span>
		</div>
		<button type="submit" value="Search" id="search_properties">Search</button>
	</div>
	<input type="hidden" value="" name="ketqua_hidd" id="ketqua_hidd" />
	<?php echo JHTML::_( 'form.token' ); ?>
	</form>
</div>
</div>