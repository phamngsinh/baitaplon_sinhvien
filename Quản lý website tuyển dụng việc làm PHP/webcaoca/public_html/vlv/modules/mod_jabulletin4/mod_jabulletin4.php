<?php



/*



# ------------------------------------------------------------------------


# JA Teline III Stable - Version 1.2 - Licence Owner JA115884


# ------------------------------------------------------------------------


# Copyright (C) 2004-2009 J.O.O.M Solutions Co., Ltd. All Rights Reserved.


# @license - Copyrighted Commercial Software


# Author: J.O.O.M Solutions Co., Ltd


# Websites:  http://www.joomlart.com -  http://www.joomlancers.com


# This file may not be redistributed in whole or significant part.


# ------------------------------------------------------------------------



*/ 



// no direct access



defined('_JEXEC') or die('Restricted access');







if (!defined ('_MODE_JABULLETIN_ASSETS_')) {
	define ('_MODE_JABULLETIN_ASSETS_', 1);
	JHTML::stylesheet('style.css','modules/'.$module->module.'/assets/');
	if (is_file(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'css'.DS.$module->module.".css"))
	JHTML::stylesheet($module->module.".css",'templates/'.$mainframe->getTemplate().'/css/');
}
// Include the syndicate functions only once

$title = $params->get('title');
require_once (dirname(__FILE__).DS.'helper.php');
$list = modJABulletin4::getList($params);
$listOthers = modJABulletin4::getOtherArticle($params);
//$cyid_test = JRequest::getVar('cyid');
//if (!$cyid_test)
	require(JModuleHelper::getLayoutPath('mod_jabulletin4'));


