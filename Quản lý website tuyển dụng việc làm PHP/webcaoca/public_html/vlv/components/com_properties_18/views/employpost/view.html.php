<?php
// ensure a valid entry point
defined('_JEXEC') or die('Restricted Access');

jimport('joomla.application.component.view');

class JLordCoreViewEmploypost extends JView
{
	function display($tpl = null)
	{
		global $option,$mainframe;
		$doc =& JFactory::getDocument();
		$doc->addScript(JURI::root().'components/com_properties/assets/cty.js');
		
		$user = JFactory::getUser();
		$db = JFactory::getDBO();
		$query 	= 'select * from #__properties_profiles where mid =  '. $user->get('id') .' and published = 1';
		$db->setQuery( $query );
		$check_user	= $db->loadObject();
		//require_once(JURI::root().'components/com_properties/helpers/helper.php');
		//require_once(JURI::root().'components/com_properties/helpers/select.php');
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_properties'.DS.'helpers'.DS.'helper.php');
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_properties'.DS.'helpers'.DS.'select.php');
		
		JHTML::_('behavior.tooltip');	
		$task = JRequest::getVar('task');		
		$permis = $this->get('Permission');
		if ($task == 'edit' or $task =='add') {
			$res		=& $this->get('EditData');
		}else if ($task == 'jobapply') {
			$res 	= $this->get('ShowDataApply'); 
		}else if ($task == 'show_seecker') {
			$res 	= $this->get('ShowDataSeecker'); 
		}else if ($task == 'cv_seecker') {
			$res 	= $this->get('ShowDataCVSeecker'); 
		}else if ($task == 'cv_seecker_apply') {
			$res 	= $this->get('ShowDataCVSeeckerApply'); 
		}else if ($task == 'cv_seecker_save') {
			$res 	= $this->get('ShowDataCVSeeckerSave'); 
		}else if ($task == 'buy_packet') {
			$res 	= $this->get('getDataPacket'); 
		}else if ($task == 'list_packet') {
			$res 	= $this->get('ListPacket'); 
		}else if ($task == 'buy_packetedit') {
			$res 	= $this->get('ListPacketEdit'); 
		}else {
			$res 	= $this->get('ShowData');
		}
		$this->assignRef('res',$res);
		$this->assignRef('check_user',$check_user);
		$this->assignRef('permis',$permis);
		$option		= JRequest::getVar('option','com_properties');
		$controller	= JRequest::getVar('controller','employpost');
		$this->assignRef('params',$params);
		$this->assignRef('controller',$controller);
		parent::display($tpl);
	}
} // end class
?>