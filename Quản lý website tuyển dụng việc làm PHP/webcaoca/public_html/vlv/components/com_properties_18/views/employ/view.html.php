<?php
// ensure a valid entry point
defined('_JEXEC') or die('Restricted Access');

jimport('joomla.application.component.view');

class JLordCoreViewEmploy extends JView
{
	function display($tpl = null)
	{
		global $option,$mainframe;
		//require_once(JPATH_SITE.DS.'components'.DS.'com_properties'.DS.'class'.DS.'libFunction.php');
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_properties'.DS.'helpers'.DS.'helper.php');
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_properties'.DS.'helpers'.DS.'select.php');
		
		JHTML::_('behavior.tooltip');	
		$task = JRequest::getVar('task');		
		$permis = $this->get('Permission');
		if ($task == 'edit' or $task =='add') {
			$res 	= $this->get('EditData');
		} else {
			$res 	= $this->get('ShowData'); 
		} 
		$this->assignRef('res',$res);
		$this->assignRef('permis',$permis);
		$option		= JRequest::getVar('option','com_properties');
		$controller	= JRequest::getVar('controller','employ');
		$this->assignRef('params',$params);
		$this->assignRef('controller',$controller);
		parent::display($tpl);
	}
} // end class
?>