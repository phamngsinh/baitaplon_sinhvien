<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();
jimport('joomla.application.component.model');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('joomla.installer.helper');
jimport( 'joomla.application.component.view');
JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_properties'.DS.'tables');
//exit(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_properties'.DS.'tables');
class JlordCoreModelDetail extends JModel
{
	//var $_db = NULL;administrator/components/com_properties/tables
	var $_userid = NULL;
	var $_data = NULL;
	var $TableName = null;
	function __construct() {
		parent::__construct();
		 global $mainframe, $option;
        
        
        $limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', 20, 'int');
        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
 
        // In case limit has been changed, adjust it
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
 
        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);
    }
	function getTotal()
	{
		$type = JRequest::getVar('type');
		if (empty($this->_total))
		{
			$query = $this->_buildQuery($type);
			$this->_total = $this->_getListCount($query);
		}
		return $this->_total;
	}
	function _buildQuery($type)
	{
        //$query = "SELECT id,agent_id,name,published,enddate,sid,type FROM #__properties_products WHERE (type LIKE '%$type%' OR type LIKE '%,$type%') AND published=1 ORDER BY p3 DESC,name ASC" ;
        $query = "SELECT id,agent_id,name,published,enddate,sid,type FROM #__properties_products WHERE type = $type AND published=1 ORDER BY p3 DESC,name ASC" ;
        return $query;
	}
	function _buildViewQuery($cid) {
		$where = $this->_buildViewWhere($cid);
		$ordering = $this->_buildViewOrdering();
		$query = 'SELECT * FROM #__properties_products'.$where.$ordering;
		return $query;
	}
	function getPagination()
  	{
        if (empty($this->_pagination)) {
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
        }
        return $this->_pagination;
  	}
	
	// Filters function
	function _buildViewWhere($cid) {
		global $mainframe, $option;
		
		$where = ($cid==0) ? ' WHERE published=1' : ' WHERE parent='.$cid.' AND published=1';
		return $where;
	}
	function _buildViewOrdering() {
		global $mainframe, $option;
		$ordering = ' ORDER BY ordering';
		return $ordering;
	}
	function getLastModif()
	{
		$TableName 	= 'profiles';
		$query = ' SELECT id FROM #__properties_'.$TableName.' ORDER BY id desc LIMIT 1';
	 $this->_db->setQuery( $query );	
			$this->_data = $this->_db->loadResult();
	
	//print_r($this->_data);
	 return $this->_data;


	}
 	function store($data)
	{
	$row =& $this->getTable('profiles');
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());			
			return false;
		}		
		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());			
			return false;
		}
		
		if($TableName=='profiles'){
	//	$this->Notification();	
		}
		//exit('3');
		return true;
	}
	function getData(){
		$type =JRequest::getVar('type',0);
		$query = $this->_buildQuery($type);
			//echo $query;
			$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		return $this->_data;
	}
}
?>