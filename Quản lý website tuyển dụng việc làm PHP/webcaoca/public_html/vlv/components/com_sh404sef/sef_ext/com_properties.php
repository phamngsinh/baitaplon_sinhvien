<?php
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

// ------------------  standard plugin initialize function - don't change ---------------------------
global $sh_LANG;
$sefConfig = & shRouter::shGetConfig();
$shLangName = '';
$shLangIso = '';
$title = array();
$shItemidString = '';
$dosef = shInitializePlugin( $lang, $shLangName, $shLangIso, $option);
if ($dosef == false) return;
// ------------------  standard plugin initialize function - don't change ---------------------------


// do something about that Itemid thing
if (!preg_match( '/Itemid=[0-9]+/i', $string)) { // if no Itemid in non-sef URL
  //global $Itemid;
  if ($sefConfig->shInsertGlobalItemidIfNone && !empty($shCurrentItemid)) {
    $string .= '&Itemid='.$shCurrentItemid;  // append current Itemid
    $Itemid = $shCurrentItemid;
    shAddToGETVarsList('Itemid', $Itemid); // V 1.2.4.m
  }
  if ($sefConfig->shInsertTitleIfNoItemid)
  $title[] = $sefConfig->shDefaultMenuItemName ?
  $sefConfig->shDefaultMenuItemName : getMenuTitle($option, null, $shCurrentItemid );
  $shItemidString = $sefConfig->shAlwaysInsertItemid ?
  COM_SH404SEF_ALWAYS_INSERT_ITEMID_PREFIX.$sefConfig->replacement.$shCurrentItemid
  : '';
} else {  // if Itemid in non-sef URL
  $shItemidString = $sefConfig->shAlwaysInsertItemid ?
  COM_SH404SEF_ALWAYS_INSERT_ITEMID_PREFIX.$sefConfig->replacement.$Itemid
  : '';
}

// for contact page we always add something before contact name
$view = isset($view) ? @$view : null;
$task = isset($task) ? @$task : null;
$Itemid = isset($Itemid) ? @$Itemid : null;
$id = isset($id) ? $id : null;
$catid = isset($catid) ? $catid : null;
$shName = shGetComponentPrefix( $option);
/*$shContactCat = shGetContactCategory( $id, $shLangName);
$shName = empty($shName) && empty( $shContactCat) ? getMenuTitle($option, (isset($view) ? @$view : null), $Itemid ) : $shName;
if (!empty($shName) && $shName != '/') $title[] = $shName;
if( !empty( $shContactCat)) {
  $title[] = $shContactCat;
  option=com_properties&controller=ajax&format=raw&task=resultsearch
}*/

switch ($view) {
	case 'ajax':
		$title[] = $view;
		break;
  case 'chitiet' :
    // fetch cat namee
    if (!empty($agent_id)) {
    	shRemoveFromGETVarsList('agent_id');
    	//$title[] = '';
    }
    if (!empty($id)) {
	    shRemoveFromGETVarsList('id');
	    $query  = "SELECT name, id, type FROM #__properties_products" ;
	    $query .= "\n WHERE id=".$database->Quote($id);
	    $database->setQuery( $query );
	    if (shTranslateUrl($option, $shLangName))
	    	$result = $database->loadObject();
	    else
	    	$result = $database->loadObject( false);

	    if (!empty($result)){
	    	$query_cat  = "SELECT name, id FROM #__properties_type" ;
		    $query_cat .= "\n WHERE id=".$result->type;
		    $database->setQuery( $query_cat );
		    
		    if (shTranslateUrl($option, $shLangName))
		    	$result_cat = $database->loadObject();
		    else 
		    	$result_cat = $database->loadObject( false);
	    	
		    if (!empty($result_cat)){
		    	$title[] = $result_cat->name;
		    }else{
		    	$title[] = $result->type;
	    	}
	    	$title[] = $result->id;
	    	$title[] = $result->name;
	    }
	    else
	    	$title[] = $id;
    } else {
	    if(empty($title)) {
	      $title[] = $view;
	    }
    }
    shRemoveFromGETVarsList('id');
    shRemoveFromGETVarsList('agent_id');
    //$title[] = '/';
    break;
  case 'detail' :
    if (!empty($type)) {
      shRemoveFromGETVarsList('type');
      $query  = "SELECT name, id FROM #__properties_type" ;
      $query .= "\n WHERE id=".$database->Quote($type);
      $database->setQuery( $query );
      if (shTranslateUrl($option, $shLangName))
      	$result = $database->loadObject();
      else 
      	$result = $database->loadObject( false);
      if (!empty($result))  {
        $title[] = $result->name;
      }	else {
        $title[] = $detail;
      }
    }
    break;
    //sef thanh vien
    case 'seecker' :
	    if ($task == 'add') {
	    	$title[] = 'Đăng ký thành viên';
	    }
	    else if ($task=='edit' && !empty($mid)){
	    	shRemoveFromGETVarsList('mid');
	    	$title[] = 'Sửa thông tin thành viên';
	    }
	    else if (!empty($mid)) {
     		shRemoveFromGETVarsList('mid');
     		$title[] = 'ứng viên';
	    }else{
	    	$title[] = 'CV ứng viên';
	    }
    break;
    case 'search' :
	    $title[] = 'Search-viec-lam';
    break;
    //sef nha tuyen dung 
    case 'search_ntd' :
	    $title[] = 'Search-nha-tuyen-dung';
    break;
    case 'employ' :
	    if ($task == 'add') {
	    	$title[] = 'Đăng ký nhà tuyển dụng';
	    }
		if ($task == 'edit') {
			shRemoveFromGETVarsList('mid');
	    	$title[] = 'Sửa thông tin nhà tuyển dụng';
	    }
	    if($task == 'Checkmail'){
	    	$title[] = $task;
	    }
	    if ($task == 'Checkname'){
	    	$title[] = $task;
	    }
    break;
    case 'employpost' :
    	$title[] = 'Nhà tuyển dụng';
    	shRemoveFromGETVarsList('mid');
		if ($task == 'cv_seecker_apply'){
			if (!empty($cvs_id_product) && !empty($cvs_id_employ)) {
		    	shRemoveFromGETVarsList('cvs_id_product');
		    	shRemoveFromGETVarsList('cvs_id_employ');
		    	$title[] = 'Xem ứng viên apply';
		    }
		}
		if ($task == 'cv_seecker_save') {
			shRemoveFromGETVarsList('idgoi_cv');
    		$title[] = 'Danh sách hồ sơ ứng viên đã chọn';
    		$title[] = $idgoi_cv;
    		$query  = "SELECT id,name_packet,type_packet FROM #__properties_buypacket" ;
		    $query .= "\n WHERE id=".$database->Quote($idgoi_cv);
		    $database->setQuery( $query );
		    if (shTranslateUrl($option, $shLangName))
		    	$result = $database->loadObject();
		    else
		    	$result = $database->loadObject( false);
		    if (!empty($result)){
		    	$title[] = $result->type_packet.'-'.$result->name_packet;
		    }
    	}
		if ($task == 'cv_seecker') {
    		$title[] = 'Xem hồ sơ ứng viên';
    	}
		if ($task == 'show_seecker') {
			shRemoveFromGETVarsList('id_seecker');
			shRemoveFromGETVarsList('id_cv');
			shRemoveFromGETVarsList('click');
    		$title[] = 'Xem chi tiết hồ sơ ứng viên';
    		$title[] = $id_cv;
    		if (!empty($click))
    			$title[] = $click;
    		else
    			$title[] = '0';
    	}
		if ($task == 'jobapply') {
    		$title[] = 'Xem hồ sơ đã chọn';
    	}
		if ($task == 'buy_packetedit') {
    		$title[] = 'Sửa gói hồ sơ';
    		shRemoveFromGETVarsList('cid');
    	}
		if ($task == 'buy_packet') {
    		$title[] = 'Mua gói hồ sơ';
    		shRemoveFromGETVarsList('cid');
    	}
		if ($task == 'list_packet') {
    		$title[] = 'Danh sách gói hồ sơ';
    	}
    	
    	if ($task == 'add') {
    		$title[] = 'Tạo mới việc làm';
    	}
		if ($task == 'edit') {
			shRemoveFromGETVarsList('cid');
	    	$title[] = 'Sửa việc làm';
		    $query  = "SELECT name, id, type FROM #__properties_products" ;
		    $query .= "\n WHERE id=".$database->Quote($cid[0]);
		    $database->setQuery( $query );
		    if (shTranslateUrl($option, $shLangName))
		    	$result = $database->loadObject();
		    else
		    	$result = $database->loadObject( false);
	
		    if (!empty($result)){
		    	$query_cat  = "SELECT name, id FROM #__properties_type" ;
			    $query_cat .= "\n WHERE id=".$result->type;
			    $database->setQuery( $query_cat );
			    
			    if (shTranslateUrl($option, $shLangName))
			    	$result_cat = $database->loadObject();
			    else 
			    	$result_cat = $database->loadObject( false);
		    	
			    if (!empty($result_cat)){
			    	$title[] = $result_cat->name;
			    }else{
			    	$title[] = $result->type;
		    	}
		    	$title[] = $result->id;
		    	$title[] = $result->name;
		    }
		    else
		    	$title[] = $cid[0];
	    }
	    /*if ($task == 'add') {
	    	$title[] = 'Đăng ký nhà tuyển dụng';
	    }*/
    break;
    //&mid=83
    //sef search all work 
    case 'all' :
    	$title[] = 'Danh sách việc làm';
    break;
    //sef post cv 
    case 'postcv' :
    	if ($task == 'list'){
    		$title[] = 'Danh sách CV ứng viên';
    	} else if($task == 'edit'){
    		shRemoveFromGETVarsList('id');
    		shRemoveFromGETVarsList('mid');
    		$title[] = 'Sửa CV ứng viên';
    		if (!empty($mid) && !empty($id)){
    			$query  = "SELECT vitri, id, mid FROM #__properties_seekekjop" ;
			    $query .= "\n WHERE id=".$database->Quote($id);
			    $query .= "\n AND mid=".$database->Quote($mid);
			    $database->setQuery($query);
			    if (shTranslateUrl($option, $shLangName))
			    	$result = $database->loadObject();
			    else
			    	$result = $database->loadObject( false);
			    if (!empty($result)){
			    	$title[] = $result->id;
			    	$title[] = $result->vitri;
			    }else{
			    	$title[] = $id;
			    }
    		}
    		//&id=23&mid=84&task=
    	}else{
    		$title[] = 'Đăng hồ sơ';
    	}
    break;
    //seecker&mid=84
}

shRemoveFromGETVarsList('option');
if (!empty($Itemid))
shRemoveFromGETVarsList('Itemid');
shRemoveFromGETVarsList('lang');
if (!empty($view))
shRemoveFromGETVarsList('view');
if (!empty($task))
shRemoveFromGETVarsList('task');
// ------------------  standard plugin finalize function - don't change ---------------------------
if ($dosef){
  $string = shFinalizePlugin( $string, $title, $shAppendString, $shItemidString,
  (isset($limit) ? @$limit : null), (isset($limitstart) ? @$limitstart : null),
  (isset($shLangName) ? @$shLangName : null));
}
// ------------------  standard plugin finalize function - don't change ---------------------------

?>
