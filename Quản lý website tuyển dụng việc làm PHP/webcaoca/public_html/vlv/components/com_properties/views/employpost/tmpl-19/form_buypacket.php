<?php
defined('_JEXEC') or die('Restricted Access');
global $mainframe,$option;
$view = JRequest::getVar('view');
$rows 		= $this->res->rows;
$lists		= $this->res->lists;
$pageNav	= $this->res->pageNav;
$tmp		= '';
$user = &JFactory::getUser();
//check data
$db = JFactory::getDBO();
$qry = "select count(id) from #__properties_buypacket where (published=1 or published=0) and (checkbuy = 1 or checkbuy = 3) and agent_id=".$user->get('id');
$db->setQuery($qry);
$resl = $db->loadResult();
//echo $resl;
?>
<link rel="stylesheet" type="text/css" href="<?php echo JURI::base()?>components/com_properties/assets/css/style.css"></link>
<script type="text/javascript">
function submitform(pressbutton){
	if (pressbutton) {
		document.adminForm.task.value=pressbutton;
	}
	if (typeof document.adminForm.onsubmit == "function") {
		document.adminForm.onsubmit();
	}
	document.adminForm.submit();
}
	function submitbutton(pressbutton) {
	  	submitform(pressbutton);
	}
</script>
<div class="box-center">
	<?php 
		
		//link menu employ
		$link_new_job 			= "index.php?option=com_properties&view=employpost&task=add";
		$link_job_listing 		= 'index.php?option=com_properties&view=employpost';
		$link_job_buy_packet 	= 'index.php?option=com_properties&view=employpost&task=buy_packet';
		$link_job_list_buy_packet 	= 'index.php?option=com_properties&view=employpost&task=list_packet';
		$link_job_apply 		= "index.php?option=com_properties&view=employpost&task=jobapply";
		$link_job_seecker 		= "index.php?option=com_properties&view=employpost&task=cv_seecker";
		$link_edit_profile_employ = "index.php?option=com_properties&view=employ&task=edit&mid=".$user->get('id');
		$link_doc = "#";
	?>
	<h3>
		<?php echo JText::_('Mua gói hồ sơ')?>	
	</h3>
	<ul id="tpj_topmenu">
		<li><a href="<?php echo $link_new_job; ?>"><?php echo JText::_('Đăng  tuyển dụng'); ?></a></li>
		<li><?php echo JText::_('Quản lý hồ sơ'); ?>
			<ul>
				<li><a href="<?php echo $link_job_listing; ?>"><?php echo JText::_('Công việc đã đăng'); ?></a>
				<li><a href="<?php echo $link_job_buy_packet; ?>"><?php echo JText::_('Mua gói hồ sơ'); ?></a>
				<li><a href="<?php echo $link_job_list_buy_packet; ?>"><?php echo JText::_('DS hồ sơ đã mua'); ?></a>
			</ul>
		</li>
		<li><?php echo JText::_('Xem hồ sơ'); ?>
			<ul>
				<li><a href="<?php echo $link_job_apply; ?>"><?php echo JText::_('Hồ sơ apply'); ?></a>
				<li><a href="<?php echo $link_job_seecker; ?>"><?php echo JText::_('Hồ sơ ứng viên'); ?></a>
			</ul>
		</li>
		<li><a href="<?php echo $link_edit_profile_employ; ?>"><?php echo JText::_('Thông tin nhà tuyển dụng'); ?></a></li>
		<li><a href="<?php echo $link_doc; ?>"><?php echo JText::_('Giá và dịch vụ'); ?></a></li>
		<li style="display:none;"><a href="index.php?option=com_properties&view=all"><?php echo JText::_('Xem tất cả công việc'); ?></a>
          	</li>
	</ul><br />
<?php if (!$resl){?>
<form action= "index.php?option=<?php echo $option;?>&view=employpost" method="POST" name="adminForm" id="adminForm">
<?php echo JHTML::_( 'form.token' ); ?>
<div class="css_dangky">
	<div class="tctt_left2">
				<script type="text/javascript">
					function Change_cong_viec(a){
						var chk = $(a);
						if(chk.checked==true ){
							$('goi_cong_viec').setStyle('display', 'block');
						}
						else if(chk.checked==false){
							$('goi_cong_viec').setStyle('display', 'none');
						}
					}
					function Change_cv(a){
						var chk = $(a);
						if(chk.checked==true ){
							$('goi_cv').setStyle('display', 'block');
						}
						else if(chk.checked==false){
							$('goi_cv').setStyle('display', 'none');
						}
					}
				</script>
				<table>
					 <tbody>
						 <tr style="display: none;">
							<td class="dn_0">
								Gói công việc 
								<input onclick="Change_cong_viec(this.id);" id="gcviec" type="checkbox" name="gcviec" value="gcviec<?php //echo $lists_packet->id?>" />
							</td>
							<td style="display:none;" id="goi_cong_viec">
								<p><input disabled="disabled" type="checkbox" id="1" name="top_dau" value="1"  />&nbsp; <?php echo JText::_('Tốp đầu')?> <?php //echo JHTML::_('tooltip', JText::_( 'TOOLTIP_CHANGE_TOP_DAU')); ?></p>
								<p><input disabled="disabled" type="checkbox" id="2" name="boi_dam_do" value="2"  />&nbsp; <?php echo JText::_('Bôi đậm đỏ')?> <?php //echo JHTML::_('tooltip', JText::_( 'TOOLTIP_CHANGE_BOI_DAM_DO')); ?></p>
								<p><input disabled="disabled" type="checkbox" id="3" name="vl_tot_nhat" value="3"  />&nbsp; <?php echo JText::_('Việc làm tốt nhất')?> <?php //echo JHTML::_('tooltip', JText::_( 'TOOLTIP_CHANGE_VIEC_LAM_TOT_NHAT')); ?></p>
		              		</td>
						 </tr>
						 <tr>
							<td class="dn_0">
								Gói Xem CV
								<input onclick="Change_cv(this.id);" id="goicv" type="checkbox" name="goicv" value="goicv<?php //echo $lists_packet->id?>" />
							</td>
							<td style="display:none;" id="goi_cv">
								<p><input type="radio" id="4" name="goi_cv" value="40"  />&nbsp; <?php echo JText::_('Gói 40 CV')?> <?php //echo JHTML::_('tooltip', JText::_( 'TOOLTIP_CHANGE_TOP_DAU')); ?></p>
								<p><input type="radio" id="6" name="goi_cv" value="60"  />&nbsp; <?php echo JText::_('Gói 60 CV')?> <?php //echo JHTML::_('tooltip', JText::_( 'TOOLTIP_CHANGE_BOI_DAM_DO')); ?></p>
								<p><input type="radio" id="9" name="goi_cv" value="90"  />&nbsp; <?php echo JText::_('Gói 90 CV')?> <?php //echo JHTML::_('tooltip', JText::_( 'TOOLTIP_CHANGE_VIEC_LAM_TOT_NHAT')); ?></p>
		              		</td>
						 </tr>
					</tbody>
				</table>
				
				<table class="admintable" style="display:none;">
				<tbody>
				<?php //echo CatTreeHelper::getJobPacket( $this->datos->jpacket,'jpacket'); ?>
				<?php 
					$lists_packets = CatTreeHelper::getJobBuyPacket($this->datos->jpacket,'jpacket');
					//print_r($lists_packets);
					$i=0;
					foreach ($lists_packets as $lists_packet) {
						?>
						<tr>
							<td >
								<input onclick="ChangeBuyPacket(this.value,this.id);" id="<?php echo $i;?>" type="checkbox" name="packets" value="<?php echo $lists_packet->id?>" /> &nbsp;<?php echo $lists_packet->name;?>
							</td>
							<td>
								<div id="AjaxBuyPack<?php echo $i;?>">
								</div>
							</td>
						</tr>
						<?php 
					$i++;}
				?>
				<tr>
			   		<td>
			   			<?php 
			   				
			   				foreach ($lists_packets as $lists_packet) {
							?>
								<div id="AjaxPack" style="float:left">
					   				<?php echo CatTreeHelper::getCheckJob( $this->datos->jpacket,$this->datos->jp,'jpacket'); ?>
				      		 	</div>
				      		<?php 
							}
			   			?>
			   			<div id="AjaxPack" style="float:left">
			   				<?php echo CatTreeHelper::getCheckJob( $this->datos->jpacket,$this->datos->jp,'jpacket'); ?>
		      		 	</div>
		      		 	
		      		 	<div id="progressJP"></div>
		      		 	
			   		</td>
		   		</tr>
				</tbody>
				</table>
				<button type="button" onclick="javascript: submitbutton('editbuy_packet')">Save</button>
				<button type="button" onclick="javascript: submitbutton('cancel')">Cancel</button>
	</div>
</div>
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="userid" value="<?php echo $user->get('id')?>" />
	<input type="hidden" name="option" value="<?php echo $option;?>" />
	<input type="hidden" name="view" value="<?php echo $view;?>">
	<input type="hidden" name="boxchecked" value="0" />
</form>

<?php }else{
	echo JText::_('Gói hồ sơ của bạn đang hoạt động.');
	?>
	<strong><a href="<?php echo $link_job_list_buy_packet;?>"><?php echo JText::_('Click')?></a></strong>
	<?php echo JText::_(' để xem danh sách các gói mà bạn đã mua .')?>
	<?php 
}
	?>
</div>