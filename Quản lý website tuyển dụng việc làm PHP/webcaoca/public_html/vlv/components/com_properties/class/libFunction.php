<?php
// no direct access
//defined('_JEXEC') or die('Restricted access');
class LibFunction
{
	function getCid($cid) {
		$db = & JFactory::getDBO();
		$qry = "select id,name from #__properties_category where id = $cid";
		$db->setQuery($qry);
		$listCid = $db->loadObject();
		return $listCid;
	}
	function getTy($type) {
		$db = & JFactory::getDBO();
		$qry = "select id,name from #__properties_type where id = $type";
		$db->setQuery($qry);
		$listCid = $db->loadObject();
		return $listCid;
	}
	function getCyid($cyid) {
		$db = & JFactory::getDBO();
		$qry = "select id,name from #__properties_country where id = $cyid";
		$db->setQuery($qry);
		$listCid = $db->loadObject();
		return $listCid;
	}
	function getSid($sid) {
		$db = & JFactory::getDBO();
		$qry = "select id,name from #__properties_state where id = $sid";
		$db->setQuery($qry);
		$listCid = $db->loadObject();
		return $listCid;
	}
	function getLid($lid) {
		$db = & JFactory::getDBO();
		$qry = "select id,name from #__properties_locality where id = $lid";
		$db->setQuery($qry);
		$listCid = $db->loadObject();
		return $listCid;
	}
	//get List other for selected
//	locality
	function getLocality($script,$postlocality_selected) {
		$db = & JFactory::getDBO();
		$qry = "select id,name from #__properties_locality where published =1";//" and id = $idCategories";
		$db->setQuery($qry);
		$listTypes = $db->loadObjectList();
		if ($script == 'admin')
			$js = 'onchange="document.adminForm.submit();" ';
		else 
			$js = '';
			
		//$postlocality[] = array();
		$postlocality[] = JHTML::_('select.option',  '0','-Select Locality-', 'id', 'name');
		if (count($listTypes))
			$postlocality	= array_merge($postlocality, $listTypes);
		$lists['postlocality'] = JHTML::_('select.genericlist',  $postlocality, 'lid', 'class="inputbox" size="1"  '.$js.' ', 'id', 'name',$postlocality_selected);
		return $lists['postlocality']; 
	}
	//	state
	function getState($script,$poststate_selected) {
		$db = & JFactory::getDBO();
		$qry = "select id,name from #__properties_state where published =1";//" and id = $idCategories";
		$db->setQuery($qry);
		$listTypes = $db->loadObjectList();
		if ($script == 'admin')
			$js = 'onchange="document.adminForm.submit();" ';
		else 
			$js = '';
			
		//$poststate[] = array();
		$poststate[] = JHTML::_('select.option',  '0','-Select State-', 'id', 'name');
		if (count($listTypes))
			$poststate	= array_merge($poststate, $listTypes);
		$lists['poststate'] = JHTML::_('select.genericlist',  $poststate, 'sid', 'class="inputbox" size="1"  '.$js.' ', 'id', 'name',$poststate_selected);
		return $lists['poststate']; 
	}
	//	Country
	function getCountry($script,$postcountry_selected) {
		$db = & JFactory::getDBO();
		$qry = "select id,name from #__properties_country where published =1";//" and id = $idCategories";
		$db->setQuery($qry);
		$listTypes = $db->loadObjectList();
		if ($script == 'admin')
			$js = 'onchange="document.adminForm.submit();" ';
		else 
			$js = '';
			
		//$postcountry[] = array();
		$postcountry[] = JHTML::_('select.option',  '0','-Select Country-', 'id', 'name');
		if (count($listTypes))
			$postcountry	= array_merge($postcountry, $listTypes);
		$lists['postcountry'] = JHTML::_('select.genericlist',  $postcountry, 'cyid', 'class="inputbox" size="1"  '.$js.' ', 'id', 'name',$postcountry_selected);
		return $lists['postcountry']; 
	}
	//get type
	function getType($script,$posttype_selected) {
		$db = & JFactory::getDBO();
		$qry = "select id,name from #__properties_type where published =1";//" and id = $idCategories";
		$db->setQuery($qry);
		$listTypes = $db->loadObjectList();
		if ($script == 'admin')
			$js = 'onchange="document.adminForm.submit();" ';
		else 
			$js = '';
		//$posttype[] = array();
		$posttype[] = JHTML::_('select.option',  '0','-Select Type-', 'id', 'name');
		if (count($listTypes))
			$posttype	= array_merge($posttype, $listTypes);
		$lists['posttype'] = JHTML::_('select.genericlist',  $posttype, 'type_post', 'class="inputbox" size="1" '.$js.' ', 'id', 'name',$posttype_selected);
		return $lists['posttype']; 
	}
	//get categories
	function getCategories($postcatid_selected) {
		$db = & JFactory::getDBO();
		$qry = "select id,name from #__properties_category where published =1";//" and id = $idCategories";
		$db->setQuery($qry);
		$listCategories = $db->loadObjectList();
		
		//$categori[] = array();
		$postcatid[] = JHTML::_('select.option',  '0','-Select Categories-', 'id', 'name');
		if (count($listCategories))
			$postcatid	= array_merge($postcatid, $listCategories);
		$lists['postcatid'] = JHTML::_('select.genericlist',  $postcatid, 'cid', 'class="inputbox" size="1" ', 'id', 'name',$postcatid_selected);
		return $lists['postcatid']; 
	}

 }
  
