<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
class Tableemploy extends JTable
{    
	var $id = null;
	var $name = null;
	var $alias = null;
	var $agent_id = null;
	var $agent = null;
	var $ref = null;
	var $type = null;
	var $parent = null;
	var $cid = null;
	var $lid = null;
	var $sid = null;
	var $cyid = null;	
	var $postcode = null;
	var $address = null;
	var $description = null;
	var $text = null;
	var $price = null;
	var $price_type = null;
	var $published = null;
	var $use_booking = null;
	var $ordering = null;
	var $panoramic = null;	
	var $video = null;	
	var $lat = null;	
	var $lng = null;	
	var $available = null;	
	var $featured = null;	
	var $years = null;	
	var $bedrooms = null;	
	var $bathrooms = null;	
	var $garage = null;	
	var $area = null;	
	var $covered_area = null;	
	var $hits = null;	
	var $listdate = null;	
	var $refresh_time = null;	
	var $checked_out = null;	
	var $checked_out_time = null;
  var $direction = null;
  var $certificate = null;
  var $floor = null;
  var $kittchen = null;
  var $livingroom = null;
  var $pwidth = null;
  var $plong = null;
  var $yearmodify = null;
  var $quality = null;
  var $street = null;
  var $fixprice = null;
  var $project = null;
  var $invester = null;
  var $consultant = null;
  var $performer = null;
  var $totalinvest = null;
  var $startdate = null;
  var $enddate = null;
  var $projectarea = null;
  
   function __construct(&$db)
  {
    parent::__construct( '#__properties_products', 'id', $db );
  }
}
?>