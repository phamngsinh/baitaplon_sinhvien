<?php
// ensure a valid entry point
defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.controller');
//JTable::addIncludePath( JPATH_COMPONENT_ADMINISTRATOR.DS.'tables' );
JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_properties'.DS.'tables');
$id 	= JRequest::getVar('id', 0, 'get', 'int');
class JLORDCOREControllerPostcv extends JController
{
	function __construct()
	{
		parent::__construct();
	}
	function display()
	{
		global $mainframe;
		$cid 	= JRequest::getVar('cid', array(0), 'post', 'array');
		$user 	= & JFactory::getUser(); 
		JArrayHelper::toInteger($cid, array(0));
		$mName 		= 'postcv';	
		$vName 		= 'postcv';	
		$vLayout 	= 'default';
		$option 	= JRequest::getVar('option');	
		$task = JRequest::getVar('task');
		switch ($task){
			case 'add':	
			case 'edit':
				JRequest::setVar( 'hidemainmenu', 1 );				
				$vLayout = 'default';				
				//JRequest::setVar('edit',true);
				break;
			case 'save':
			case 'apply':
				$this->savePostcv();
				break;
			case 'list':
				JRequest::setVar( 'hidemainmenu', 1 );				
				$vLayout = 'list';				
				//JRequest::setVar('edit',true);
				break;
			case 'unpublish':
			case 'publish':
				$this->publish();
				break;
			case 'editpost':
				$this->editPost();
				break;
			case 'remove':
				$this->removePost();
				break;
			case 'cancel':
				$this->cancelpost();
				break;	
			case 'Checkmail':
				$this->Checkmail();
				break;
			default:
				$vName 		= 'postcv';	
				$vLayout 	= 'default';							
		}
		//exit($vLayout);
		$document 	= JFactory::getDocument();
		$vType 		= $document->getType();		
		$view = $this->getView($vName, $vType);
		if ($model 	= &$this->getModel($mName)) {
			$view->setModel($model, true);
		}
		$view->setLayout($vLayout);		
		$view->display();		
	}
	function removePost(){		
		global $mainframe;
		$cid 	= JRequest::getVar('id');
		$db 	= &JFactory::getDBO();
		$query 	= "DELETE FROM `#__properties_seekekjop` WHERE `id` = $cid";
		//exit($query);
		$db->setQuery($query);
		$msg = "Deleted items ($cid)";
		if (!$db->query()) {
			echo "<script> alert('".$db->getErrorMsg()."');
			window.history.go(-1); </script>\n";
		}
		$mainframe->redirect('index.php?option=com_properties&view=postcv&task=list',$msg);
	}
	function cancelpost(){
		global $mainframe;
		$db 	= &JFactory::getDBO();
		$option = JRequest::getVar('option');
		// Initialize variables
		$mainframe->redirect('index.php?option='.$option.'&view=postcv&task=list');
	}
	//send mail
	function char_makerand ()
	{
		$charset 	 = "abcdefghijklmnopqrstuvwxyz";
		$charset 	.= "0123456789";
		$char = $charset[(mt_rand( 0,(strlen( $charset )-1)) )];
		return $char;
	}
	
	function str_makerand()
	{
	  	$key = "";
	  	for ( $i=0; $i < 50; $i++ ) {
	   		$key .= char_makerand();
	  	}
	  	return $key;
	}
function publish(){	
		$cid		= JRequest::getVar( 'cid', array(), '', 'array' );
		$this->publish	= ( $this->getTask() == 'publish' ? 1 : 0 );		
	
		JArrayHelper::toInteger($cid);
		if (count( $cid ) < 1)		{
			$action = $publish ? 'publish' : 'unpublish';		
			JError::raiseError(500, JText::_( 'Select an item to' .$action, true ) );
		}
		$this->cids = implode( ',', $cid );
		
		$query = 'UPDATE #__properties_seekekjop'
		. ' SET published = ' . (int) $this->publish
		. ' WHERE id IN ( '. $this->cids .' )'		
		;    
		
		$db 	=& JFactory::getDBO();
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}
		$link = 'index.php?option=com_properties&view=postcv&task=list';
		$this->setRedirect($link, $msg);		
	}
	function savePostcv(){
		global $mainframe;
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.file');
		JRequest::checkToken() or jexit('Invalid Token');
		$db		=& JFactory::getDBO();
		$user = JFactory::getUser();
		$mid = $user->get('id');//exit($mid.'---');
		$post   = JRequest::get('post');
		
		$name_cv = JRequest::getVar( 'name_cv', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$name_cv		= str_replace( '<br>', '<br />', $name_cv );		
		$post['name_cv'] = $name_cv;
		
		$cv = JRequest::getVar( 'cv', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$cv		= str_replace( '<br>', '<br />', $cv );		
		$post['cv'] = $cv;
		
		require_once(JPATH_SITE.DS.'configuration.php');
		$component_name = 'properties';
		$datos = new JConfig();	
		$basedatos = $datos->db;
		$dbprefix = $datos->dbprefix;
		
		$query = "SHOW TABLE STATUS FROM `".$basedatos."` LIKE '".$dbprefix.$component_name."_seekekjop'";
		//echo $query;
		$db->setQuery( $query );		
		$nextAutoIndex = $db->loadObject();
		//print_r($nextAutoIndex);
		$destino_imagen = JPATH_SITE.DS.'images'.DS.'properties'.DS.'profiles'.DS;		
		if(JRequest::getVar('id')){
			$id_agent=JRequest::getVar('id');
		}else{
			$id_agent=$nextAutoIndex->Auto_increment;
		}
		//exit($_FILES['name_file'].'--'.$_FILES['name_file']['name']);
		if (isset($_FILES['name_file'])){
			$name = $_FILES['name_file']['name'];	
			if (!empty($name)) {
				$ext = '.'.JFile::getExt($name);
				//$ext2 = JFile::stripExt($name);exit($ext.'-'.$ext2);
				$personal_name_file = JPATH_SITE.DS.'images'.DS.'properties'.DS.'cv'.DS;//exit($personal_image);
				move_uploaded_file($_FILES['name_file']['tmp_name'],	$personal_name_file.$id_agent.'_cv'.$ext); 
				$name_file = $id_agent.'_cv'.$ext;
				$post['name_file'] = $name_file;
				/*$AnchoLogo=140;
				$AltoLogo=200;
				$destinoCopia=$personal_name_file;
				$destinoNombre=$post['name_file'];
				$destino = $personal_name_file;
				$this->CambiarTamanoExacto($post['name_file'],$AnchoLogo,$AltoLogo,$destinoCopia,$destinoNombre,$destino);*/
			}
		}
		
		$post['mid'] = $mid;
		$post['published'] = 1;
		//exit(date('Y-m-d').date());
		$post['start_date'] = date('Y-m-d');
		$option = JRequest::getVar('option');
		//echo"<pre>";print_r($post);exit;
		$row 	=& JTable::getInstance('seekekjop', 'Table');
		//$row['mid']=$mid;
		
		if (!$row->bind($post)) {

			return JError::raiseWarning(500, $row->getError());

		}
	 	$row->store();
		$mainframe->redirect('index.php?option='.$option.'&view=postcv&task=list');
	}
	function editPost(){
		global $mainframe;
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.file');
		$db		=& JFactory::getDBO();
		$id 	= JRequest::getVar('id');
		$post   = JRequest::get('post');
		
		$bangcapkhac = JRequest::getVar( 'bangcapkhac', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$bangcapkhac		= str_replace( '<br>', '<br />', $bangcapkhac );		
		$post['bangcapkhac'] = $bangcapkhac;
		
		$cv = JRequest::getVar( 'cv', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$cv		= str_replace( '<br>', '<br />', $cv );		
		$post['cv'] = $cv;
		
		/*require_once(JPATH_SITE.DS.'configuration.php');
		$component_name = 'properties';
		$datos = new JConfig();	
		$basedatos = $datos->db;
		$dbprefix = $datos->dbprefix;
		
		$query = "SHOW TABLE STATUS FROM `".$basedatos."` LIKE '".$dbprefix.$component_name."_seekekjop'";
		//echo $query;
		$db->setQuery( $query );		
		$nextAutoIndex = $db->loadObject();*/
		//echo "<pre>";print_r($nextAutoIndex);
		
		$destino_imagen = JPATH_SITE.DS.'images'.DS.'properties'.DS.'profiles'.DS;		
		if(JRequest::getVar('id')){
			$id_agent=JRequest::getVar('id');
		}else{
			$id_agent=$nextAutoIndex->Auto_increment;
		}
		$up = "
			 pid='".$post['pid']."',
			 jid='".$post['jid']."',
			 eid='".$post['eid']."',
			 nganhhoc='".$post['nganhhoc']."',
			 hocluc='".$post['hocluc']."',
			 ngoaingu='".$post['ngoaingu']."',
			 tinhoc='".$post['tinhoc']."',
			 bangcapkhac='".$post['bangcapkhac']."',				 
			 cid='".$post['cid']."',
			 type='".$post['type']."',
			 vitri='".$post['vitri']."',
			 capbac ='".$post['capbac']."',
			 cyid ='".$post['cyid']."',
			 sid='".$post['sid']."',
			 experience_year ='".$post['experience_year']."',
			 culture='".$post['culture']."',
			 salary ='".$post['salary']."',
			 cv ='".$post['cv']."'
		";
		if (isset($_FILES['name_file'])){
			$name = $_FILES['name_file']['name'];	
			if (!empty($name)) {//sim.doc
				$ext = '.'.JFile::getExt($name);//.doc
				//$ext2 = JFile::stripExt($name);exit($ext.'-'.$ext2);sim
				$personal_name_file = JPATH_SITE.DS.'images'.DS.'properties'.DS.'cv'.DS;//exit($personal_image);
				move_uploaded_file($_FILES['name_file']['tmp_name'],	$personal_name_file.$id_agent.'_cv'.$ext); 
				$name_file = $id_agent.'_cv'.$ext;
				$post['name_file'] = $name_file;
				$up .= ",name_file ='".$post['name_file']."'";
			}
			
		}
		
		$sql = "UPDATE #__properties_seekekjop SET 
				 $up
				 WHERE id=".$id;
		//exit($sql);
		$db->setQuery($sql);
		$db->query();
		$mainframe->redirect('index.php?option=com_properties&view=postcv&task=list');
	}
	function CambiarTamanoExacto($nombre,$max_width,$max_height,$destinoCopia,$destinoNombre,$destino){
		$InfoImage=getimagesize($destino.$nombre);               
        $width=$InfoImage[0];
        $height=$InfoImage[1];
		$type=$InfoImage[2];						 
							
		$x_ratio = $max_width / $width;
		$y_ratio = $max_height / $height;
	
		if (($width <= $max_width) && ($height <= $max_height) ) {
			$tn_width = $width;
			$tn_height = $height;
		} else if (($x_ratio * $height) < $max_height) {
			$tn_height = ceil($x_ratio * $height);
			$tn_width = $max_width;
		} else {
			$tn_width = ceil($y_ratio * $width);
			$tn_height = $max_height;
		}
		$width=$tn_width;
		$height	=$tn_height;	
			 
		switch($type)
	     {
	       case 1: //gif
	        {
             $img = imagecreatefromgif($destino.$nombre);
             $thumb = imagecreatetruecolor($width,$height);
             imagecopyresampled($thumb,$img,0,0,0,0,$width,$height,imagesx($img),imagesy($img));
             ImageGIF($thumb,$destinoCopia.$destinoNombre,100);
	
	           break;
	        }
	       case 2: //jpg,jpeg
	        {					 
	             $img = imagecreatefromjpeg($destino.$nombre);
	             $thumb = imagecreatetruecolor($width,$height);
	            imagecopyresampled($thumb,$img,0,0,0,0,$width,$height,imagesx($img),imagesy($img));
	            ImageJPEG($thumb,$destinoCopia.$destinoNombre);
	           break;
	        }
	       case 3: //png
	        {
	             $img = imagecreatefrompng($destino.$nombre);
	             $thumb = imagecreatetruecolor($width,$height);
	           	imagecopyresampled($thumb,$img,0,0,0,0,$width,$height,imagesx($img),imagesy($img));
	           	ImagePNG($thumb,$destinoCopia.$destinoNombre);
	           break;
	        }
	     } // switch				  
	}
	//ajax check mail
	function Checkmail(){
		/*$db 	= JFactory::getDBO();
		$email 	= JRequest::getVar('email');
		$qry 	= "SELECT email FROM #__users WHERE email = '$email'";
		$db->setQuery($qry);
		$res 	= $db->loadResult();
		if (count($res))
			exit('Dia chi mail da dc dang ky');
		else 
			exit('Dia chi mail co the su dung');*/	
		$msg = 'thanhtd';
		echo $msg;
	}
} // end class
?>