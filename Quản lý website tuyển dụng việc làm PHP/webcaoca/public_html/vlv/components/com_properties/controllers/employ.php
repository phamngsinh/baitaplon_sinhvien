<?php
// ensure a valid entry point
defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.controller');
jimport('joomla.application.helper');
jimport('joomla.utilities.utility' );
//require_once(JApplicationHelper::getPath('html'));
JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_properties'.DS.'tables');

//JTable::addIncludePath( JPATH_COMPONENT_ADMINISTRATOR.DS.'tables' );
$id 	= JRequest::getVar('id', 0, 'get', 'int');
class JLORDCOREControllerEmploy extends JController
{
	function __construct()
	{
		parent::__construct();
	}
	function display()
	{
		global $mainframe;
		$cid 	= JRequest::getVar('cid', array(0), 'post', 'array');
		$user 	= & JFactory::getUser(); 
		JArrayHelper::toInteger($cid, array(0));
		$mName 		= 'employ';	
		$vName 		= 'employ';	
		$vLayout 	= 'default';
		$option 	= JRequest::getVar('option');	
		$task = JRequest::getVar('task');
		switch ($task){
			case 'add':				
				JRequest::setVar( 'hidemainmenu', 1 );				
				$vLayout = 'new';				
				//JRequest::setVar('edit',true);
				break;
			case 'edit':
				JRequest::setVar( 'hidemainmenu', 1 );				
				$vLayout = 'edit_new';				
				break;
			case 'save':
			case 'apply':
				$this->saveEmploy();
				break;
			case 'edit_employ':
				$this->saveEditEmploy();
				break;
			case 'remove':
				$this->removePost();
				break;
			case 'cancel':
				$this->cancelpost();
				break;	
			case 'Checkmail':
				$this->CheckEmploy();
				break;
			default:
				$vName 		= 'employ';	
				$vLayout 	= 'default';							
		}
		
		$document 	= JFactory::getDocument();
		$vType 		= $document->getType();		
		$view = $this->getView($vName, $vType);
		if ($model 	= &$this->getModel($mName)) {
			$view->setModel($model, true);
		}
		$view->setLayout($vLayout);		
		$view->display();		
	}
	function removePost(){
		global $mainframe;
		$cid 	= JRequest::getVar('cid', array(), '', 'array');
		$db 	= &JFactory::getDBO();
		$t_cid 	= count($cid);  
		//exit($cid[0]." = 0");
		if($t_cid){
			$cids 	= implode(',', $cid);
			$query 	= "DELETE FROM `#__properties_products` WHERE `id` IN ($cids)";
			$db->setQuery($query);
			$msg = "Deleted ($t_cid) items";
			if (!$db->query()) {
				echo "<script> alert('".$db->getErrorMsg()."');
				window.history.go(-1); </script>\n";
			}
		}
		$mainframe->redirect('index.php?option=com_properties&view=employ',$msg);
	}
	function cancelpost(){
		global $mainframe;
		$db 	= &JFactory::getDBO();
		$option = JRequest::getVar('option');
		// Initialize variables
		$mainframe->redirect('index.php?option='.$option.'&view=employpost');
	}
	//send mail
	function char_makerand ()
	{
		$charset 	 = "abcdefghijklmnopqrstuvwxyz";
		$charset 	.= "0123456789";
		$char = $charset[(mt_rand( 0,(strlen( $charset )-1)) )];
		return $char;
	}
	
	function str_makerand()
	{
	  	$key = "";
	  	for ( $i=0; $i < 50; $i++ ) {
	   		$key .= char_makerand();
	  	}
	  	return $key;
	}
	
	function saveEditEmploy(){
		global $mainframe;
		$post = JRequest::get("post");
		$capcha = $mainframe->getUserState("security_code");
		if($capcha!=$post["captcha"]){
			$mid = $post['mid'];
			echo "<script>alert('Sai mã hình ảnh !');
			document.location.href='index.php?option=com_properties&view=employ&task=edit&mid=$mid';
			</script>";
		}else{
		// Check for request forgeries
			JRequest::checkToken() or jexit('Invalid Token');
			// Initialize variables
			$db		=& JFactory::getDBO();
			$row	=& JTable::getInstance('profiles', 'Table');
			$post   = JRequest::get('post');
			//$userid = $usern->id;
			
			//global $mainframe;
			jimport('joomla.filesystem.folder');
			jimport('joomla.filesystem.file');
			$component_name = 'properties';
			$option = JRequest::getVar('option');	
			
			$usern = clone(JFactory::getUser());
			$qry  = "update #__users set `name` = '".$post['name']."' where id =".$usern->get('id');
			//exit($qry);
			$db->setQuery($qry);
			
			$db->query();
			//exit($usern->get('id'));
			//$post['mid'] = $userid;
			require_once(JPATH_SITE.DS.'configuration.php');
			$datos = new JConfig();	
			$basedatos = $datos->db;
			$dbprefix = $datos->dbprefix;
			
			$query = "SHOW TABLE STATUS FROM `".$basedatos."` LIKE '".$dbprefix.$component_name."_".$this->TableName."';";
			$db->setQuery( $query );		
			$nextAutoIndex = $db->loadObject();
			$destino_imagen = JPATH_SITE.DS.'images'.DS.'properties'.DS.'profiles'.DS;		
			if(JRequest::getVar('id')){
				$id_agent=JRequest::getVar('id');
			}else{
				$id_agent=$nextAutoIndex->Auto_increment;
			}
			//$id_agent = 0;
			if (isset($_FILES['image'])){
				$name = $_FILES['image']['name'];	
				if (!empty($name)) {
					$ext = '.'.JFile::getExt($name);
				$personal_image = JPATH_SITE.DS.'images'.DS.'properties'.DS.'profiles'.DS;//exit($personal_image);
				move_uploaded_file($_FILES['image']['tmp_name'],	$personal_image.$id_agent.'_p'.$ext); 
				$img_logo = $id_agent.'_logo'.$ext;
				$post['image'] = $img_logo;
				$AnchoLogo=140;
				$AltoLogo=200;
				$destinoCopia=$personal_image;
				$destinoNombre=$post['image'];
				$destino = $personal_image;
				$this->CambiarTamanoExacto($post['image'],$AnchoLogo,$AltoLogo,$destinoCopia,$destinoNombre,$destino);
				}
			}
			//get var agu
			$text 		= JRequest::getVar( 'c_summary', '', 'post', 'string', JREQUEST_ALLOWRAW );
			$text		= str_replace( '<br>', '<br />', $text );	
			$post['c_summary'] = $text;
			
			if (!$row->bind( $post )) {
			JError::raiseError(500, $row->getError() );
			}
			if (!$row->check()) {
				JError::raiseError(500, $row->getError() );
			}
			// save the changes
			if (!$row->store()) {
				JError::raiseError(500, $row->getError() );
			}
			$row->checkin();
			// Send registration confirmation mail
			//$password = JRequest::getString('password', '', 'post', JREQUEST_ALLOWRAW);
			//$password = preg_replace('/[\x00-\x1F\x7F]/', '', $password); //Disallow control chars in the email
			//JLORDCOREControllerEmploy::CrSendMail($usern, $password);
			//$msg = "save employ";
			switch (JRequest::getVar('task')){
				case 'edit_employ':
					if($useractivation){
						$msg = "Your account has been created, but you need to activate your account. Please check you email to get the activation code.";
					}
					else{
						$msg = "Your account has been created, please login with your username and password.";
					}
					$mainframe->redirect('index.php?option=com_properties&view=employpost',JText::_($msg));
					break;
				default:
					break;	
			}
		}
	}
	function saveEmploy(){
		global $mainframe;
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		// Initialize variables
		$db		=& JFactory::getDBO();
		$row	=& JTable::getInstance('profiles', 'Table');
		$post   = JRequest::get('post');
		
		// Get required system objects
		$usern = clone(JFactory::getUser());
		$pathway =& $mainframe->getPathway();
		$config =& JFactory::getConfig();
		$authorize =& JFactory::getACL();
		$document =& JFactory::getDocument();
		
		$lang =& JFactory::getLanguage();
	 	$lang->load('com_user', JPATH_SITE);
	
		// If user registration is not allowed, show 403 not authorized.
		$usersConfig = &JComponentHelper::getParams('com_users');
		/*echo $usersConfig->get('useractivation');
		echo "<pre>";print_r($usersConfig);exit;*/
		if ($usersConfig->get('allowUserRegistration') == '0') {
			JError::raiseError( 403, JText::_('ACCESS FORBIDDEN'));
			
			return;
		}
		
		// Initialize new usertype setting
		$newUsertype = $usersConfig->get('new_usertype');
		if (!$newUsertype) {
			$newUsertype = 'Registered';
		}
		
		// Bind the post array to the user object
		if (!$usern->bind( $post, 'usertype')) {
			JError::raiseError( 500, $usern->getError());
		}
		//exit('1');
		// Set some initial user values
		$usern->set('id', 0);
		$usern->set('usertype', '');
		$usern->set('gid', $authorize->get_group_id('', $newUsertype, 'ARO'));
		$date =& JFactory::getDate();
		$usern->set('registerDate', $date->toMySQL());
		
		// If user activation is turned on, we need to set the activation information
		$useractivation = $usersConfig->get('useractivation');
		if ($useractivation == '1')
		{
			jimport('joomla.user.helper');
			$usern->set('activation', JUtility::getHash( JUserHelper::genRandomPassword()) );
			$usern->set('block', '1');
		}
		//exit('123');
		// If there was an error with registration, set the message and display form
		//echo "<pre>";print_r($usern);exit();
		if ( !$usern->save() )
		{
			$msg = JText::_( $usern->getError());
			$link = JRoute::_('index.php?option=com_properties&view=employ&task=add');	
			$mainframe->redirect( JRoute::_($link), $msg );
			return false;
		}
		
		$userid = $usern->id;
		
		//global $mainframe;
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.file');
		$component_name = 'properties';
		$option = JRequest::getVar('option');	
		
		$post['mid'] = $userid;
		//$component = JComponentHelper::getComponent( 'com_properties' );
		//$params = new JParameter( $component->params );
		//$AutoCoord=$params->get('AutoCoord',0);
		//$db 	=& JFactory::getDBO();
		//exit('122');
		require_once(JPATH_SITE.DS.'configuration.php');
		$datos = new JConfig();	
		$basedatos = $datos->db;
		$dbprefix = $datos->dbprefix;
		
		$query = "SHOW TABLE STATUS FROM `".$basedatos."` LIKE '".$dbprefix.$component_name."_".$this->TableName."';";
		$db->setQuery( $query );		
		$nextAutoIndex = $db->loadObject();
		$destino_imagen = JPATH_SITE.DS.'images'.DS.'properties'.DS.'profiles'.DS;		
		if(JRequest::getVar('id')){
			$id_agent=JRequest::getVar('id');
		}else{
			$id_agent=$nextAutoIndex->Auto_increment;
		}
		//$id_agent = 0;
		if (isset($_FILES['image'])){
			$name = $_FILES['image']['name'];	
			if (!empty($name)) {
				$ext = '.'.JFile::getExt($name);
			$personal_image = JPATH_SITE.DS.'images'.DS.'properties'.DS.'profiles'.DS;
			move_uploaded_file($_FILES['image']['tmp_name'],	$personal_image.$id_agent.'_p'.$ext); 
			$img_logo = $id_agent.'_logo'.$ext;
			$post['image'] = $img_logo;
			$AnchoLogo=140;
			$AltoLogo=200;
			$destinoCopia=$personal_image;
			$destinoNombre=$post['image'];
			$destino = $personal_image;
			$this->CambiarTamanoExacto($post['image'],$AnchoLogo,$AltoLogo,$destinoCopia,$destinoNombre,$destino);
			}
		}
		//get var agu
		$text 		= JRequest::getVar( 'c_summary', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$text		= str_replace( '<br>', '<br />', $text );	
		$post['c_summary'] = $text;
		
		if (!$row->bind( $post )) {
		JError::raiseError(500, $row->getError() );
		}
		if (!$row->check()) {
			JError::raiseError(500, $row->getError() );
		}
		
		// save the changes
		if (!$row->store()) {
			JError::raiseError(500, $row->getError() );
			
		}
		$row->checkin();
		
		// Send registration confirmation mail
		$password = JRequest::getString('password', '', 'post', JREQUEST_ALLOWRAW);
		$password = preg_replace('/[\x00-\x1F\x7F]/', '', $password); //Disallow control chars in the email
		JLORDCOREControllerEmploy::_sendMail($usern, $password);
		//$msg = "save employ";
		switch (JRequest::getVar('task')){
			case 'save':
				if($useractivation){
					$msg = "Your account has been created, but you need to activate your account. Please check you email to get the activation code.";
				}
				else{
					$msg = "Your account has been created, please login with your username and password.";
				}
				///$mainframe->redirect('index.php?option=com_properties&view=employpost',JText::_($msg));
				$mainframe->redirect('index.php',JText::_($msg));
				break;
			default:
				break;	
		}
	}
	//SendMail user register
	function _sendMail(&$usern, $password)
	{
		global $mainframe;
	
		$lang =& JFactory::getLanguage();
	 	$lang->load('com_user', JPATH_SITE);
	
		$db		=& JFactory::getDBO();
	
		$name 		= $usern->get('name');
		$email 		= $usern->get('email');
		$username 	= $usern->get('username');
	
		$usersConfig 	= &JComponentHelper::getParams('com_users');
		$sitename 		= $mainframe->getCfg('sitename');
		$useractivation = $usersConfig->get('useractivation');
		$mailfrom 		= $mainframe->getCfg('mailfrom');
		$fromname 		= $mainframe->getCfg('fromname');
		$siteURL		= JURI::base();
	
		$subject 	= sprintf ( JText::_('ACCOUNT DETAILS FOR'), $name, $sitename);
		$subject 	= html_entity_decode($subject, ENT_QUOTES);
		//echo $useractivation;
		if ( $useractivation == 1 ){
			$message = sprintf ( JText::_('SEND_MSG_ACTIVATE'), $name, $sitename, $siteURL."index.php?option=com_user&task=activate&activation=".$usern->get('activation'), $siteURL, $username, $password);
		} else {
			$message = sprintf ( JText::_('SEND_MSG'), $name, $sitename, $siteURL);
		}
		//exit($message.'-thanhtd');
		$message = html_entity_decode($message, ENT_QUOTES);
	
		//get all super administrator
		$query = 'SELECT name, email, sendEmail' .
				' FROM #__users' .
				' WHERE LOWER( usertype ) = "super administrator"';
		$db->setQuery( $query );
		$rows = $db->loadObjectList();
	
		// Send email to user
		if ( ! $mailfrom  || ! $fromname ) {
			$fromname = $rows[0]->name;
			$mailfrom = $rows[0]->email;
		}
		//exit($mailfrom."<br>".$fromname."<br>".$email."<br>".$subject."<br>".$message.'--thanhtd--');
		JUtility::sendMail($mailfrom, $fromname, $email, $subject, $message);
	
		// Send notification to all administrators
		$subject2 = sprintf ( JText::_('ACCOUNT DETAILS FOR'), $name, $sitename);
		$subject2 = html_entity_decode($subject2, ENT_QUOTES);
	
		$lang =& JFactory::getLanguage();
	 	$lang->load('com_user', JPATH_SITE);
	
		// get superadministrators id
		foreach ( $rows as $row )
		{
			if ($row->sendEmail)
			{
				$message2 = sprintf ( JText::_('SEND_MSG_ADMIN'), $row->name, $sitename, $name, $email, $username);
				$message2 = html_entity_decode($message2, ENT_QUOTES);
				JUtility::sendMail($mailfrom, $fromname, $row->email, $subject2, $message2);
			}
		}
	}
	function CambiarTamanoExacto($nombre,$max_width,$max_height,$destinoCopia,$destinoNombre,$destino){
		$InfoImage=getimagesize($destino.$nombre);               
        $width=$InfoImage[0];
        $height=$InfoImage[1];
		$type=$InfoImage[2];						 
							
		$x_ratio = $max_width / $width;
		$y_ratio = $max_height / $height;
	
		if (($width <= $max_width) && ($height <= $max_height) ) {
			$tn_width = $width;
			$tn_height = $height;
		} else if (($x_ratio * $height) < $max_height) {
			$tn_height = ceil($x_ratio * $height);
			$tn_width = $max_width;
		} else {
			$tn_width = ceil($y_ratio * $width);
			$tn_height = $max_height;
		}
		$width=$tn_width;
		$height	=$tn_height;	
			 
		switch($type)
	     {
	       case 1: //gif
	        {
             $img = imagecreatefromgif($destino.$nombre);
             $thumb = imagecreatetruecolor($width,$height);
             imagecopyresampled($thumb,$img,0,0,0,0,$width,$height,imagesx($img),imagesy($img));
             ImageGIF($thumb,$destinoCopia.$destinoNombre,100);
	
	           break;
	        }
	       case 2: //jpg,jpeg
	        {					 
	             $img = imagecreatefromjpeg($destino.$nombre);
	             $thumb = imagecreatetruecolor($width,$height);
	            imagecopyresampled($thumb,$img,0,0,0,0,$width,$height,imagesx($img),imagesy($img));
	            ImageJPEG($thumb,$destinoCopia.$destinoNombre);
	           break;
	        }
	       case 3: //png
	        {
	             $img = imagecreatefrompng($destino.$nombre);
	             $thumb = imagecreatetruecolor($width,$height);
	           imagecopyresampled($thumb,$img,0,0,0,0,$width,$height,imagesx($img),imagesy($img));
	           ImagePNG($thumb,$destinoCopia.$destinoNombre);
	           break;
	        }
	     } // switch				  
	}
	//ajax check mail
	function isEmailAddress($email)
	{

		// Split the email into a local and domain
		$atIndex	= strrpos($email, "@");
		$domain		= substr($email, $atIndex+1);
		$local		= substr($email, 0, $atIndex);

		// Check Length of domain
		$domainLen	= strlen($domain);
		if ($domainLen < 1 || $domainLen > 255) {
			return false;
		}

		// Check the local address
		// We're a bit more conservative about what constitutes a "legal" address, that is, A-Za-z0-9!#$%&\'*+/=?^_`{|}~-
		$allowed	= 'A-Za-z0-9!#&*+=?_-';
		$regex		= "/^[$allowed][\.$allowed]{0,63}$/";
		if ( ! preg_match($regex, $local) ) {
			return false;
		}

		// No problem if the domain looks like an IP address, ish
		$regex		= '/^[0-9\.]+$/';
		if ( preg_match($regex, $domain)) {
			return true;
		}

		// Check Lengths
		$localLen	= strlen($local);
		if ($localLen < 1 || $localLen > 64) {
			return false;
		}

		// Check the domain
		$domain_array	= explode(".", rtrim( $domain, '.' ));
		$regex		= '/^[A-Za-z0-9-]{0,63}$/';
		foreach ($domain_array as $domain ) {

			// Must be something
			if ( ! $domain ) {
				return false;
			}

			// Check for invalid characters
			if ( ! preg_match($regex, $domain) ) {
				return false;
			}

			// Check for a dash at the beginning of the domain
			if ( strpos($domain, '-' ) === 0 ) {
				return false;
			}

			// Check for a dash at the end of the domain
			$length = strlen($domain) -1;
			if ( strpos($domain, '-', $length ) === $length ) {
				return false;
			}

		}

		return true;
	}
	function CheckEmploy(){
		$db 	= JFactory::getDBO();
		$email 	= JRequest::getVar('email');
		if ((trim($email) == "") || !JLORDCOREControllerEmploy::isEmailAddress($email) ) {
			$msg = 1;//'Dia chi mail ko chinh xac';
		}else {
			$qry 	= "SELECT email FROM #__users WHERE email = '$email'";
			$db->setQuery($qry);
			$res 	= $db->loadObjectList();
			if (count($res))
				$msg = 2;//'Dia chi mail da dc dang ky';
			else 
				$msg = 3;//'Dia chi mail co the su dung';	
		}
		echo $msg;
		exit;
	}
	function Checkname(){
		$db 	= JFactory::getDBO();
		$name 	= JRequest::getVar('name');
		if ($name == ""){
			$msg = 0;
		}else {
			$qry 	= "SELECT username FROM #__users WHERE `username` = '$name'";
			$db->setQuery($qry);
			$res 	= $db->loadObjectList();
			if (count($res))
				$msg = 1;//'username da dc dang ky';
			else 
				$msg = 2;//'username co the su dung';	
		}
			echo $msg;exit;
	}
	
} // end class
?>