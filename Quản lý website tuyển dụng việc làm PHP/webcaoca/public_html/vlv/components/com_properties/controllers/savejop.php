<?php
// ensure a valid entry point
defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.controller');
//JTable::addIncludePath( JPATH_COMPONENT_ADMINISTRATOR.DS.'tables' );
$id 	= JRequest::getVar('id', 0, 'get', 'int');
class JLORDCOREControllerSavejop extends JController
{
	function __construct()
	{
		parent::__construct();
	}
	function display()
	{
		global $mainframe;
		$cid 	= JRequest::getVar('cid', array(0), 'post', 'array');
		$user 	= & JFactory::getUser(); 
		JArrayHelper::toInteger($cid, array(0));
		$mName 		= 'savejop';	
		$vName 		= 'savejop';	
		$vLayout 	= 'savejop';
		$option 	= JRequest::getVar('option');	
		$task = JRequest::getVar('task');
		switch ($task){
			case 'add':	
			case 'edit':
				JRequest::setVar( 'hidemainmenu', 1 );				
				$vLayout = 'alls';				
				//JRequest::setVar('edit',true);
				break;
			case 'save':
			case 'apply':
				$this->saveSavejop();
				break;
			case 'remove':
				$this->removePost();
				break;
			case 'cancel':
				$this->cancelpost();
				break;	
			case 'Checkmail':
				$this->Checkmail();
				break;
			default:
				$vName 		= 'savejop';	
				$vLayout 	= 'default';							
		}
		//exit($vLayout);
		$document 	= JFactory::getDocument();
		$vType 		= $document->getType();		
		$view = $this->getView($vName, $vType);
		if ($model 	= &$this->getModel($mName)) {
			$view->setModel($model, true);
		}
		$view->setLayout($vLayout);		
		$view->display();		
	}
	function removePost(){		
		global $mainframe;
		$cid 	= JRequest::getVar('cid', array(), '', 'array');
		$db 	= &JFactory::getDBO();
		$t_cid 	= count($cid);  
		//exit($cid[0]." = 0");
		if($t_cid){
			$cids 	= implode(',', $cid);
			$query 	= "DELETE FROM `#__properties_products` WHERE `id` IN ($cids)";
			$db->setQuery($query);
			$msg = "Deleted ($t_cid) items";
			if (!$db->query()) {
				echo "<script> alert('".$db->getErrorMsg()."');
				window.history.go(-1); </script>\n";
			}
		}
		$mainframe->redirect('index.php?option=com_properties&view=savejop',$msg);
	}
	function cancelpost(){
		global $mainframe;
		$db 	= &JFactory::getDBO();
		$option = JRequest::getVar('option');
		// Initialize variables
		$mainframe->redirect('index.php?option='.$option.'&view=savejop');
	}
	//send mail
	function char_makerand ()
	{
		$charset 	 = "abcdefghijklmnopqrstuvwxyz";
		$charset 	.= "0123456789";
		$char = $charset[(mt_rand( 0,(strlen( $charset )-1)) )];
		return $char;
	}
	
	function str_makerand()
	{
	  	$key = "";
	  	for ( $i=0; $i < 50; $i++ ) {
	   		$key .= char_makerand();
	  	}
	  	return $key;
	}
	
	function saveDetail(){
		$id = JRequest::getVar('id', NULL, 'method', 'int');		
		$model = $this->getModel('savejop');
		if (!$model->save($id)) {
			//exit('sadfasd');
			return JError::raiseWarning(500, $url_record->getError());

		}

		// Return to extensions page

		$this->setRedirect('index.php?option=com_properties&view=savejop', JTEXT::_('Saved'));
	}
	function CambiarTamanoExacto($nombre,$max_width,$max_height,$destinoCopia,$destinoNombre,$destino){
		$InfoImage=getimagesize($destino.$nombre);               
        $width=$InfoImage[0];
        $height=$InfoImage[1];
		$type=$InfoImage[2];						 
							
		$x_ratio = $max_width / $width;
		$y_ratio = $max_height / $height;
	
		if (($width <= $max_width) && ($height <= $max_height) ) {
			$tn_width = $width;
			$tn_height = $height;
		} else if (($x_ratio * $height) < $max_height) {
			$tn_height = ceil($x_ratio * $height);
			$tn_width = $max_width;
		} else {
			$tn_width = ceil($y_ratio * $width);
			$tn_height = $max_height;
		}
		$width=$tn_width;
		$height	=$tn_height;	
			 
		switch($type)
	     {
	       case 1: //gif
	        {
             $img = imagecreatefromgif($destino.$nombre);
             $thumb = imagecreatetruecolor($width,$height);
             imagecopyresampled($thumb,$img,0,0,0,0,$width,$height,imagesx($img),imagesy($img));
             ImageGIF($thumb,$destinoCopia.$destinoNombre,100);
	
	           break;
	        }
	       case 2: //jpg,jpeg
	        {					 
	             $img = imagecreatefromjpeg($destino.$nombre);
	             $thumb = imagecreatetruecolor($width,$height);
	            imagecopyresampled($thumb,$img,0,0,0,0,$width,$height,imagesx($img),imagesy($img));
	            ImageJPEG($thumb,$destinoCopia.$destinoNombre);
	           break;
	        }
	       case 3: //png
	        {
	             $img = imagecreatefrompng($destino.$nombre);
	             $thumb = imagecreatetruecolor($width,$height);
	           	imagecopyresampled($thumb,$img,0,0,0,0,$width,$height,imagesx($img),imagesy($img));
	           	ImagePNG($thumb,$destinoCopia.$destinoNombre);
	           break;
	        }
	     } // switch				  
	}
	//ajax check mail
	function Checkmail(){
		/*$db 	= JFactory::getDBO();
		$email 	= JRequest::getVar('email');
		$qry 	= "SELECT email FROM #__users WHERE email = '$email'";
		$db->setQuery($qry);
		$res 	= $db->loadResult();
		if (count($res))
			exit('Dia chi mail da dc dang ky');
		else 
			exit('Dia chi mail co the su dung');*/	
		$msg = 'thanhtd';
		echo $msg;
	}
} // end class
?>