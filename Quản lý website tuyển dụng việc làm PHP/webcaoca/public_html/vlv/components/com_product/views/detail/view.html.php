<?php

defined("_JEXEC") or die('Truy cập không hợp lệ');
jimport( 'joomla.application.component.view' );
class ProductViewDetail extends JView
{
	function display($tpl=null)
	{
		$id = JRequest::getVar("id");
		$model =&$this->getModel('detail');
		
		$query = $model->getData($id);
		
		$catid = $query[0]->categories;
		$categories=$model->getCategories($catid);
			
        $this->assignRef('query', $query);
        $this->assignRef('categories', $categories); 
        
		parent::display($tpl);
	}
}
?>