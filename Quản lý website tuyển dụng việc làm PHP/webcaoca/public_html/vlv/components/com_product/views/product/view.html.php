<?php

defined("_JEXEC") or die('Truy cập không hợp lệ');
jimport( 'joomla.application.component.view' );
class ProductViewProduct extends JView
{
	function display($tpl=null)
	{	
		$id = JRequest::getVar("id");
		$model =&$this->getModel('Product');
		$model->setState('limit',5);
		$model->setState('limitstart',JRequest::getVar('limitstart',0));		
		//$categories = $model->getCategories($id);
		$categories=$model->getCategories(); 	
		
		$cate = $model->getCate();
	
        $pagination =& $this->get('Pagination');   
        $this->assignRef('categories', $categories);  
        $this->assignRef('pagination', $pagination);
        $this->assignRef('cate',$cate); 
        
		parent::display($tpl);
	}
}
?>