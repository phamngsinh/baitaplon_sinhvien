<?php
defined('_JEXEC') or die;
jimport('joomla.application.component.model');

class ProductModelProduct extends JModel{
		
	var $_data;
	var $_total = null;
  	var $_pagination = null;
  	var $_categories =null;
  	function __construct()
  	{
        parent::__construct();
 
        global $mainframe, $option;
 		$app = JFactory::getApplication('administrator');
        // Get pagination request variables
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'int');
        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
 
        // In case limit has been changed, adjust it
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
 
        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);
  	}
  	
	
	function _buildQuery()
	{    	
        $query = "select * from #__product where published=1"." ORDER BY created asc ";        
        return $query;
	}
	
	function getCategories(){
		$categories = 'SELECT * FROM #__cate AS c WHERE c.published=1 and c.parent=0';
		$this->_categories=$this->_getList($categories);
		//echo('<pre>');print_r($this->_categories);exit;
		return $this->_categories;
	}
	
	function getCate(){
		$qry = 'SELECT c.* FROM #__cate AS c WHERE c.published=1 and c.parent=0';
		$quer=$this->_getList($qry);
//		echo('<pre>');print_r($quer[0]->id);exit('asdgasd');
		$query ='SELECT c.* FROM #__cate AS c WHERE c.published=1 and c.parent='.$quer[0]->id;
		$query1=$this->_getList($query);
		return $query1;
	}
	
	function getData($catid)
	{       
		$cate="select * from #__cate where published = 1 and parent != 0";
		$cat = $this->_getList($cate);
            $query = "select * from #__product where published=1 and categories=".$cat[0]->id." ORDER BY created asc ";
            //echo('<pre>');print_r($query);exit('dasgsda');
            $this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit')); 
            //echo('<pre>');print_r($this->_data);exit('dasgsda');
            return $this->_data;       
	}
	function getTotal()
	  {
	        // Load the content if it doesn't already exist
	        if (empty($this->_total)) {
	            $query = $this->_buildQuery();
	            $this->_total = $this->_getListCount($query);    
	        }
	        return $this->_total;
	  }
	function getPagination()
  	{
        // Load the content if it doesn't already exist
        if (empty($this->_pagination)) {
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
        }
        return $this->_pagination;
  	}
	  
	
}
	
?>