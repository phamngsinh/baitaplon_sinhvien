-- phpMyAdmin SQL Dump
-- version 2.11.11.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 09, 2011 at 04:42 PM
-- Server version: 5.0.77
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tuyendung_2701`
--

-- --------------------------------------------------------

--
-- Table structure for table `jos_banner`
--

CREATE TABLE IF NOT EXISTS `jos_banner` (
  `bid` int(11) NOT NULL auto_increment,
  `cid` int(11) NOT NULL default '0',
  `type` varchar(30) NOT NULL default 'banner',
  `name` varchar(255) NOT NULL default '',
  `alias` varchar(255) NOT NULL default '',
  `imptotal` int(11) NOT NULL default '0',
  `impmade` int(11) NOT NULL default '0',
  `clicks` int(11) NOT NULL default '0',
  `imageurl` varchar(100) NOT NULL default '',
  `clickurl` varchar(200) NOT NULL default '',
  `date` datetime default NULL,
  `showBanner` tinyint(1) NOT NULL default '0',
  `checked_out` tinyint(1) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `editor` varchar(50) default NULL,
  `custombannercode` text,
  `catid` int(10) unsigned NOT NULL default '0',
  `description` text NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL default '0',
  `ordering` int(11) NOT NULL default '0',
  `publish_up` datetime NOT NULL default '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL default '0000-00-00 00:00:00',
  `tags` text NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY  (`bid`),
  KEY `viewbanner` (`showBanner`),
  KEY `idx_banner_catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `jos_banner`
--

INSERT INTO `jos_banner` (`bid`, `cid`, `type`, `name`, `alias`, `imptotal`, `impmade`, `clicks`, `imageurl`, `clickurl`, `date`, `showBanner`, `checked_out`, `checked_out_time`, `editor`, `custombannercode`, `catid`, `description`, `sticky`, `ordering`, `publish_up`, `publish_down`, `tags`, `params`) VALUES
(1, 1, '', 'OSM 1', 'osm-1', 0, 43, 0, 'shop-ad.jpg', 'http://www.vieclamviet.com.vn', '2010-11-25 04:26:04', 1, 0, '0000-00-00 00:00:00', NULL, '', 13, '', 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'width=0\nheight=0'),
(2, 1, 'banner', 'OSM 2', 'osm-2', 0, 49, 0, 'osmbanner2.png', 'http://www.opensourcematters.org', '2004-07-07 15:31:29', 1, 0, '0000-00-00 00:00:00', NULL, '', 13, '', 0, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(3, 1, '', 'Joomla!', 'joomla', 0, 23, 0, '', 'http://www.joomla.org', '2006-05-29 14:21:28', 1, 0, '0000-00-00 00:00:00', '', '<a href="{CLICKURL}" target="_blank">{NAME}</a>\r\n<br/>\r\nJoomla! The most popular and widely used Open Source CMS Project in the world.', 14, '', 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(4, 1, '', 'JoomlaCode', 'joomlacode', 0, 23, 0, '', 'http://joomlacode.org', '2006-05-29 14:19:26', 1, 0, '0000-00-00 00:00:00', NULL, '<a href="{CLICKURL}" target="_blank">{NAME}</a>\r\n<br/>\r\nJoomlaCode, development and distribution made easy.', 14, '', 0, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(5, 1, '', 'Joomla! Extensions', 'joomla-extensions', 0, 18, 0, '', 'http://extensions.joomla.org', '2006-05-29 14:23:21', 1, 0, '0000-00-00 00:00:00', '', '<a href="{CLICKURL}" target="_blank">{NAME}</a>\r\n<br/>\r\nJoomla! Components, Modules, Plugins and Languages by the bucket load.', 14, '', 0, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(6, 1, '', 'Joomla! Shop', 'joomla-shop', 0, 13, 0, '', 'http://shop.joomla.org', '2006-05-29 14:23:21', 0, 0, '0000-00-00 00:00:00', '', '<a href="{CLICKURL}" target="_blank">{NAME}</a>\r\n<br/>\r\nFor all your Joomla! merchandise.', 14, '', 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(7, 1, '', 'Joomla! Promo Shop', 'joomla-promo-shop', 0, 8, 1, 'shop-ad.jpg', 'http://shop.joomla.org', '2007-09-19 17:26:24', 1, 62, '2011-03-09 03:32:18', NULL, '', 33, '', 0, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(8, 1, '', 'Joomla! Promo Books', 'joomla-promo-books', 0, 12, 0, 'shop-ad-books.jpg', 'http://shop.joomla.org/amazoncom-bookstores.html', '2007-09-19 17:28:01', 1, 0, '0000-00-00 00:00:00', NULL, '', 33, '', 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_bannerclient`
--

CREATE TABLE IF NOT EXISTS `jos_bannerclient` (
  `cid` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `contact` varchar(255) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  `extrainfo` text NOT NULL,
  `checked_out` tinyint(1) NOT NULL default '0',
  `checked_out_time` time default NULL,
  `editor` varchar(50) default NULL,
  PRIMARY KEY  (`cid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jos_bannerclient`
--

INSERT INTO `jos_bannerclient` (`cid`, `name`, `contact`, `email`, `extrainfo`, `checked_out`, `checked_out_time`, `editor`) VALUES
(1, 'Open Source Matters', 'Administrator', 'admin@opensourcematters.org', '', 0, '00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jos_bannertrack`
--

CREATE TABLE IF NOT EXISTS `jos_bannertrack` (
  `track_date` date NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_bannertrack`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_cate`
--

CREATE TABLE IF NOT EXISTS `jos_cate` (
  `id` int(9) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `parent` int(9) NOT NULL,
  `description` text NOT NULL,
  `img_thumb` varchar(255) NOT NULL,
  `img_original` varchar(255) NOT NULL,
  `pathway` varchar(255) NOT NULL,
  `ordering` int(9) NOT NULL,
  `metadesc` text NOT NULL,
  `metakey` text NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `parent` (`parent`),
  KEY `ordering` (`ordering`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=79 ;

--
-- Dumping data for table `jos_cate`
--

INSERT INTO `jos_cate` (`id`, `name`, `alias`, `parent`, `description`, `img_thumb`, `img_original`, `pathway`, `ordering`, `metadesc`, `metakey`, `published`) VALUES
(77, 'Việc làm tại công ty hàng đầu', 'Việc làm tại công ty hàng đầu', 78, '', '', '', '', 0, '', '', 1),
(78, 'Quảng cáo phải', 'Quảng cáo phải', 0, '', '', '', '', 0, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_categories`
--

CREATE TABLE IF NOT EXISTS `jos_categories` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) NOT NULL default '0',
  `title` varchar(255) NOT NULL default '',
  `name` varchar(255) NOT NULL default '',
  `alias` varchar(255) NOT NULL default '',
  `image` varchar(255) NOT NULL default '',
  `section` varchar(50) NOT NULL default '',
  `image_position` varchar(30) NOT NULL default '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL default '0',
  `checked_out` int(11) unsigned NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `editor` varchar(50) default NULL,
  `ordering` int(11) NOT NULL default '0',
  `access` tinyint(3) unsigned NOT NULL default '0',
  `count` int(11) NOT NULL default '0',
  `params` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `cat_idx` (`section`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `jos_categories`
--

INSERT INTO `jos_categories` (`id`, `parent_id`, `title`, `name`, `alias`, `image`, `section`, `image_position`, `description`, `published`, `checked_out`, `checked_out_time`, `editor`, `ordering`, `access`, `count`, `params`) VALUES
(1, 0, 'Latest', '', 'latest-news', 'taking_notes.jpg', '1', 'left', 'The latest news from the Joomla! Team', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 1, ''),
(2, 0, 'Joomla! Specific Links', '', 'joomla-specific-links', 'clock.jpg', 'com_weblinks', 'left', 'A selection of links that are all related to the Joomla! Project.', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(3, 0, 'Newsflash', '', 'newsflash', '', '1', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, ''),
(4, 0, 'Liên Hệ', '', 'lienhe', '', 'com_newsfeeds', 'left', '<p style="TEXT-ALIGN: justify">Nếu bạn có thắc mắc trong quá trình sử dụng trang web vieclamviet.com.vn, mời bạn vào trang <strong><a href="index.php?option=com_content&amp;view=article&amp;id=53&amp;Itemid=50"><span style="color: #0000ff;">Thoả thuận sử dụng</span></a></strong> để xem hướng dẫn và thỏa thuận sử dụng trang web của chúng tôi. </p>\r\n<p style="TEXT-ALIGN: justify">Đối với những vấn đề khác, mời bạn liên hệ với chúng tôi theo địa chỉ dưới đây. Chúng tôi sẽ trả lời bạn trong thời gian sớm nhất.</p>\r\n<p><span style="color: #0000ff;">CÔNG TY TNHH NHÂN LỰC VIỆT BẮC</span></p>\r\n<p>Office: Số 705 Tòa Nhà LICOGI 18, Đường Thăng Long - Nội Bài, Mê Linh, Hà Nội.</p>\r\n<p>Tell: 0423228999                      </p>\r\n<p>Fax: 0436462527</p>\r\n<p>Email: <a href="mailto:info@vieclamviet.com.vn"><span style="color: #0000ff;">info@vieclamviet.com.vn</span></a></p>\r\n<p> </p>\r\n<div>\r\n<p style="TEXT-ALIGN: justify"> </p>\r\n</div>', 1, 0, '0000-00-00 00:00:00', NULL, 0, 0, 0, ''),
(5, 0, 'Free and Open Source Software', '', 'free-and-open-source-software', '', 'com_newsfeeds', 'left', 'Read the latest news about free and open source software from some of its leading advocates.', 1, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, ''),
(6, 0, 'Related Projects', '', 'related-projects', '', 'com_newsfeeds', 'left', 'Joomla builds on and collaborates with many other free and open source projects. Keep up with the latest news from some of them.', 1, 62, '2010-12-27 07:40:44', NULL, 4, 0, 0, ''),
(12, 0, 'Contacts', '', 'contacts', '', 'com_contact_details', 'left', '<p>Contact</p>', 1, 62, '2010-12-26 02:32:57', NULL, 0, 0, 0, ''),
(13, 0, 'Joomla', '', 'joomla', '', 'com_banner', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(14, 0, 'Text Ads', '', 'text-ads', '', 'com_banner', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 0, 0, 0, ''),
(15, 0, 'Features', '', 'features', '', 'com_content', 'left', '', 0, 0, '0000-00-00 00:00:00', NULL, 6, 0, 0, ''),
(17, 0, 'Benefits', '', 'benefits', '', 'com_content', 'left', '', 0, 0, '0000-00-00 00:00:00', NULL, 4, 0, 0, ''),
(18, 0, 'Platforms', '', 'platforms', '', 'com_content', 'left', '', 0, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, ''),
(19, 0, 'Other Resources', '', 'other-resources', '', 'com_weblinks', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, ''),
(29, 0, 'The CMS', '', 'the-cms', '', '4', 'left', 'Information about the software behind Joomla!<br />', 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, ''),
(28, 0, 'Current Users', '', 'current-users', '', '3', 'left', 'Questions that users migrating to Joomla! 1.5 are likely to raise<br />', 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, ''),
(25, 0, 'Giới thiệu', '', 'gii-thiu', '', '4', 'left', '<p>General facts about Joomla!</p>', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(27, 0, 'New to Joomla!', '', 'new-to-joomla', '', '3', 'left', 'Questions for new users of Joomla!', 1, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, ''),
(30, 0, 'The Community', '', 'the-community', '', '4', 'left', 'About the millions of Joomla! users and Web sites<br />', 1, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, ''),
(31, 0, 'General', '', 'general', '', '3', 'left', 'General questions about the Joomla! CMS', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(32, 0, 'Languages', '', 'languages', '', '3', 'left', 'Questions related to localisation and languages', 1, 0, '0000-00-00 00:00:00', NULL, 4, 0, 0, ''),
(33, 0, 'Joomla! Promo', '', 'joomla-promo', '', 'com_banner', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 0, 0, 0, ''),
(34, 0, 'TÀI LIỆU BIỂU MẪU', '', 'tai-liu-biu-mu', '', '7', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(35, 0, 'Đăng hồ sơ', '', 'ng-h-s', '', '5', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, ''),
(36, 0, 'Thông báo việc làm', '', 'thong-bao-vic-lam', '', '5', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, ''),
(37, 0, 'Nhật ký tìm việc làm', '', 'nht-ky-tim-vic-lam', '', '4', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 4, 0, 0, ''),
(38, 0, 'Nhật ký tìm việc làm', '', 'nht-ky-tim-vic-lam', '', '5', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 4, 0, 0, ''),
(39, 0, ' Tư vấn nghề nghiệp', '', '-t-vn-ngh-nghip', '', '5', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 5, 0, 0, ''),
(40, 0, 'trắc nghiệm', '', 'trc-nghim', '', '5', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 6, 0, 0, ''),
(41, 0, 'Dịch vụ cung ứng nhân sự', '', 'dch-v-cung-ng-nhan-s', '', '5', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 7, 0, 0, ''),
(42, 0, 'Bảng giá', '', 'bng-gia', '', '4', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 5, 0, 0, ''),
(43, 0, 'Giá và dịch vụ', '', 'gia-va-dch-v', '', '6', 'left', '', 1, 62, '2010-12-26 07:36:20', NULL, 1, 0, 0, ''),
(44, 0, 'Mẫu tài liệu', '', 'mu-tai-liu', '', '7', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_components`
--

CREATE TABLE IF NOT EXISTS `jos_components` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `link` varchar(255) NOT NULL default '',
  `menuid` int(11) unsigned NOT NULL default '0',
  `parent` int(11) unsigned NOT NULL default '0',
  `admin_menu_link` varchar(255) NOT NULL default '',
  `admin_menu_alt` varchar(255) NOT NULL default '',
  `option` varchar(50) NOT NULL default '',
  `ordering` int(11) NOT NULL default '0',
  `admin_menu_img` varchar(255) NOT NULL default '',
  `iscore` tinyint(4) NOT NULL default '0',
  `params` text NOT NULL,
  `enabled` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `parent_option` (`parent`,`option`(32))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=128 ;

--
-- Dumping data for table `jos_components`
--

INSERT INTO `jos_components` (`id`, `name`, `link`, `menuid`, `parent`, `admin_menu_link`, `admin_menu_alt`, `option`, `ordering`, `admin_menu_img`, `iscore`, `params`, `enabled`) VALUES
(1, 'Banners', '', 0, 0, '', 'Banner Management', 'com_banners', 0, 'js/ThemeOffice/component.png', 0, 'track_impressions=0\ntrack_clicks=0\ntag_prefix=\n\n', 1),
(2, 'Banners', '', 0, 1, 'option=com_banners', 'Active Banners', 'com_banners', 1, 'js/ThemeOffice/edit.png', 0, '', 1),
(3, 'Clients', '', 0, 1, 'option=com_banners&c=client', 'Manage Clients', 'com_banners', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(4, 'Web Links', 'option=com_weblinks', 0, 0, '', 'Manage Weblinks', 'com_weblinks', 0, 'js/ThemeOffice/component.png', 0, 'show_comp_description=1\ncomp_description=\nshow_link_hits=1\nshow_link_description=1\nshow_other_cats=1\nshow_headings=1\nshow_page_title=1\nlink_target=0\nlink_icons=\n\n', 1),
(5, 'Links', '', 0, 4, 'option=com_weblinks', 'View existing weblinks', 'com_weblinks', 1, 'js/ThemeOffice/edit.png', 0, '', 1),
(6, 'Categories', '', 0, 4, 'option=com_categories&section=com_weblinks', 'Manage weblink categories', '', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(7, 'Contacts', 'option=com_contact', 0, 0, '', 'Edit contact details', 'com_contact', 0, 'js/ThemeOffice/component.png', 1, 'contact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_fax=\nicon_misc=\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\nsession=1\ncustomReply=0\n\n', 1),
(8, 'Contacts', '', 0, 7, 'option=com_contact', 'Edit contact details', 'com_contact', 0, 'js/ThemeOffice/edit.png', 1, '', 1),
(9, 'Categories', '', 0, 7, 'option=com_categories&section=com_contact_details', 'Manage contact categories', '', 2, 'js/ThemeOffice/categories.png', 1, 'contact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_fax=\nicon_misc=\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\nsession=1\ncustomReply=0\n\n', 1),
(10, 'Polls', 'option=com_poll', 0, 0, 'option=com_poll', 'Manage Polls', 'com_poll', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(11, 'News Feeds', 'option=com_newsfeeds', 0, 0, '', 'News Feeds Management', 'com_newsfeeds', 0, 'js/ThemeOffice/component.png', 0, 'show_headings=0\nshow_name=1\nshow_articles=1\nshow_link=1\nshow_cat_description=1\nshow_cat_items=1\nshow_feed_image=1\nshow_feed_description=1\nshow_item_description=1\nfeed_word_count=0\n\n', 1),
(12, 'Feeds', '', 0, 11, 'option=com_newsfeeds', 'Manage News Feeds', 'com_newsfeeds', 1, 'js/ThemeOffice/edit.png', 0, 'show_headings=1\nshow_name=1\nshow_articles=1\nshow_link=1\nshow_cat_description=1\nshow_cat_items=1\nshow_feed_image=1\nshow_feed_description=1\nshow_item_description=1\nfeed_word_count=0\n\n', 1),
(13, 'Categories', '', 0, 11, 'option=com_categories&section=com_newsfeeds', 'Manage Categories', '', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(14, 'User', 'option=com_user', 0, 0, '', '', 'com_user', 0, '', 1, '', 1),
(15, 'Search', 'option=com_search', 0, 0, 'option=com_search', 'Search Statistics', 'com_search', 0, 'js/ThemeOffice/component.png', 1, 'enabled=0\n\n', 1),
(16, 'Categories', '', 0, 1, 'option=com_categories&section=com_banner', 'Categories', '', 3, '', 1, '', 1),
(17, 'Wrapper', 'option=com_wrapper', 0, 0, '', 'Wrapper', 'com_wrapper', 0, '', 1, '', 1),
(18, 'Mail To', '', 0, 0, '', '', 'com_mailto', 0, '', 1, '', 1),
(19, 'Media Manager', '', 0, 0, 'option=com_media', 'Media Manager', 'com_media', 0, '', 1, 'upload_extensions=bmp,csv,doc,epg,gif,ico,jpg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,EPG,GIF,ICO,JPG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS\nupload_maxsize=10000000\nfile_path=images\nimage_path=images/stories\nrestrict_uploads=1\nallowed_media_usergroup=3\ncheck_mime=1\nimage_extensions=bmp,gif,jpg,png\nignore_extensions=\nupload_mime=image/jpeg,image/gif,image/png,image/bmp,application/x-shockwave-flash,application/msword,application/excel,application/pdf,application/powerpoint,text/plain,application/x-zip\nupload_mime_illegal=text/html\nenable_flash=0\n\n', 1),
(20, 'Articles', 'option=com_content', 0, 0, '', '', 'com_content', 0, '', 1, 'show_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=0\nshow_create_date=0\nshow_modify_date=0\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=0\nshow_pdf_icon=0\nshow_print_icon=0\nshow_email_icon=0\nshow_hits=1\nfeed_summary=0\nfilter_groups=20\nfilter_tags=\nfilter_attritbutes=\n\n', 1),
(21, 'Configuration Manager', '', 0, 0, '', 'Configuration', 'com_config', 0, '', 1, '', 1),
(22, 'Installation Manager', '', 0, 0, '', 'Installer', 'com_installer', 0, '', 1, '', 1),
(23, 'Language Manager', '', 0, 0, '', 'Languages', 'com_languages', 0, '', 1, 'site=vi-VN\nadministrator=vi-VN\n\n', 1),
(24, 'Mass mail', '', 0, 0, '', 'Mass Mail', 'com_massmail', 0, '', 1, 'mailSubjectPrefix=\nmailBodySuffix=\n\n', 1),
(25, 'Menu Editor', '', 0, 0, '', 'Menu Editor', 'com_menus', 0, '', 1, '', 1),
(27, 'Messaging', '', 0, 0, '', 'Messages', 'com_messages', 0, '', 1, '', 1),
(28, 'Modules Manager', '', 0, 0, '', 'Modules', 'com_modules', 0, '', 1, '', 1),
(29, 'Plugin Manager', '', 0, 0, '', 'Plugins', 'com_plugins', 0, '', 1, '', 1),
(30, 'Template Manager', '', 0, 0, '', 'Templates', 'com_templates', 0, '', 1, '', 1),
(31, 'User Manager', '', 0, 0, '', 'Users', 'com_users', 0, '', 1, 'allowUserRegistration=1\nnew_usertype=Registered\nuseractivation=1\nfrontend_userparams=1\n\n', 1),
(32, 'Cache Manager', '', 0, 0, '', 'Cache', 'com_cache', 0, '', 1, '', 1),
(33, 'Control Panel', '', 0, 0, '', 'Control Panel', 'com_cpanel', 0, '', 1, '', 1),
(118, 'Categories', '', 0, 116, 'option=com_product&controller=categories&task=view', 'Categories', 'com_product', 1, 'js/ThemeOffice/component.png', 0, '', 1),
(116, 'product', 'option=com_product', 0, 0, 'option=com_product', 'product', 'com_product', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(117, 'Product', '', 0, 116, 'option=com_product&controller=product&task=view', 'Product', 'com_product', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(109, 'Properties', 'option=com_properties', 0, 0, 'option=com_properties', 'Properties', 'com_properties', 0, 'components/com_properties/includes/img/menu_properties.png', 0, 'UseCountry=1\nUseCountryDefault=1\nUseState=1\nUseStateDefault=1\nUseLocality=1\nUseLocalityDefault=1\ncantidad_productos=5\nexpireDays=5\nActivarMapa=1\napikey=ABQIAAAAFHktBEXrHnX108wOdzd3aBTupK1kJuoJNBHuh0laPBvYXhjzZxR0qkeXcGC_0Dxf4UMhkR7ZNb04dQ\ndistancia=15\nDefaultLat=30.062438\nDefaultLng=31.248207\nAutoCoord=1\nuser_add_locality=0\nSaveContactForm=0\nShowVoteRating=0\nListlayout=0\nWidthThumbs=100\nHeightThumbs=75\nWidthImage=640\nHeightImage=480\nShowImagesSystem=1\nShowOrderBy=0\nShowOrderByDefault=0\nShowOrderDefault=DESC\nSimbolPrice=$\nPositionPrice=0\nFormatPrice=0\nShowLogoAgent=1\nShowReferenceInList=1\nShowCategoryInList=1\nShowTypeInList=1\nShowAddressInList=1\nShowPriceInList=1\nShowContactLink=1\nShowMapLink=1\nShowAddShortListLink=1\nShowViewPropertiesAgentLink=1\nThumbsInAccordion=5\nWidthThumbsAccordion=100\nHeightThumbsAccordion=75\nShowFeaturesInList=1\nShowAllParentCategory=0\nAmountPanel=\nAmountForRegistered=5\nRegisteredAutoPublish=1\nAmountForAuthor=5\nAmountForEditor=5\nAmountForPublisher=5\nAmountForManager=5\nAmountForAdministrator=5\nAutoPublish=1\nMailAdminPublish=1\nDetailLayout=0\nActivarTabs=0\nActivarDescripcion=1\nActivarDetails=1\nActivarVideo=1\nActivarPanoramica=1\nActivarContactar=1\nContactMailFormat=1\nActivarReservas=1\nShowImagesSystemDetail=1\nWidthThumbsDetail=120\nHeightThumbsDetail=90\nms_country=1\nms_state=1\nms_locality=1\nms_category=1\nms_type=1\nms_price=1\nms_bedrooms=1\nms_bathrooms=1\nms_parking=1\nms_area=1\nRangeArea=001_099;100_199;200_299;300_399;400_499;500_\nAreaToSearch=area\nMinPriceRentDay=\nMaxPriceRentDay=\nIdCatPriceDay=\nMinPriceRentMonth=\nMaxPriceRentMonth=\nIdCatPriceMonth=\nMinPriceSell=\nMaxPriceSell=\nIdCatPriceSell=\nmd_country=1\nmd_state=1\nmd_locality=1\nmd_category=1\nmd_type=1\nshowComments=0\nuseComment2=0\nuseComment3=0\nuseComment4=0\nuseComment5=0\nAmountMonthsCalendar=3\nStartYearCalendar=2009\nStartMonthCalendar=1\nPeriodOnlyWeeks=0\nPeriodAmount=3\nPeriodStartDay=\ntitle=46\n\n', 1),
(110, 'Control Panel', '', 0, 109, 'option=com_properties', 'Control Panel', 'com_properties', 0, 'components/com_properties/includes/img/t_properties.png', 0, '', 1),
(111, 'Configuration', '', 0, 109, 'option=com_properties&view=configuration', 'Configuration', 'com_properties', 1, 'components/com_properties/includes/img/t_configuration.png', 0, '', 1),
(112, 'Categories', '', 0, 109, 'option=com_properties&view=categories', 'Categories', 'com_properties', 2, 'components/com_properties/includes/img/t_categories.png', 0, '', 1),
(113, 'Types', '', 0, 109, 'option=com_properties&view=types', 'Types', 'com_properties', 3, 'components/com_properties/includes/img/t_types.png', 0, '', 1),
(114, 'Products', '', 0, 109, 'option=com_properties&view=products', 'Products', 'com_properties', 4, 'components/com_properties/includes/img/menu_properties.png', 0, '', 1),
(115, 'Help', '', 0, 109, 'option=com_properties&view=help', 'Help', 'com_properties', 5, 'components/com_properties/includes/img/t_help.png', 0, '', 1),
(119, 'sh404SEF', 'option=com_sh404sef', 0, 0, 'option=com_sh404sef', 'sh404SEF', 'com_sh404sef', 0, 'components/com_sh404sef/assets/images/menu-icon-sh404sef.png', 0, '', 1),
(120, 'CONTROL_PANEL', '', 0, 119, 'option=com_sh404sef&c=default', 'CONTROL_PANEL', 'com_sh404sef', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(121, 'URL_MANAGER', '', 0, 119, 'option=com_sh404sef&c=urls&layout=default&view=urls', 'URL_MANAGER', 'com_sh404sef', 1, 'js/ThemeOffice/component.png', 0, '', 1),
(122, 'ALIASES_MANAGER', '', 0, 119, 'option=com_sh404sef&c=aliases&layout=default&view=aliases', 'ALIASES_MANAGER', 'com_sh404sef', 2, 'js/ThemeOffice/component.png', 0, '', 1),
(123, 'PAGEID_MANAGER', '', 0, 119, 'option=com_sh404sef&c=pageids&layout=default&view=pageids', 'PAGEID_MANAGER', 'com_sh404sef', 3, 'js/ThemeOffice/component.png', 0, '', 1),
(124, '404_REQ_MANAGER', '', 0, 119, 'option=com_sh404sef&c=urls&layout=view404&view=urls', '404_REQ_MANAGER', 'com_sh404sef', 4, 'js/ThemeOffice/component.png', 0, '', 1),
(125, 'TITLE_METAS_MANAGER', '', 0, 119, 'option=com_sh404sef&c=metas&layout=default&view=metas', 'TITLE_METAS_MANAGER', 'com_sh404sef', 5, 'js/ThemeOffice/component.png', 0, '', 1),
(126, 'ANALYTICS_MANAGER', '', 0, 119, 'option=com_sh404sef&c=analytics&layout=default&view=analytics', 'ANALYTICS_MANAGER', 'com_sh404sef', 6, 'js/ThemeOffice/component.png', 0, '', 1),
(127, 'DOCUMENTATION', '', 0, 119, 'option=com_sh404sef&layout=info&view=default&task=info', 'DOCUMENTATION', 'com_sh404sef', 7, 'js/ThemeOffice/component.png', 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_contact_details`
--

CREATE TABLE IF NOT EXISTS `jos_contact_details` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `alias` varchar(255) NOT NULL default '',
  `con_position` varchar(255) default NULL,
  `address` text,
  `suburb` varchar(100) default NULL,
  `state` varchar(100) default NULL,
  `country` varchar(100) default NULL,
  `postcode` varchar(100) default NULL,
  `telephone` varchar(255) default NULL,
  `fax` varchar(255) default NULL,
  `misc` mediumtext,
  `image` varchar(255) default NULL,
  `imagepos` varchar(20) default NULL,
  `email_to` varchar(255) default NULL,
  `default_con` tinyint(1) unsigned NOT NULL default '0',
  `published` tinyint(1) unsigned NOT NULL default '0',
  `checked_out` int(11) unsigned NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL default '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL default '0',
  `catid` int(11) NOT NULL default '0',
  `access` tinyint(3) unsigned NOT NULL default '0',
  `mobile` varchar(255) NOT NULL default '',
  `webpage` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jos_contact_details`
--

INSERT INTO `jos_contact_details` (`id`, `name`, `alias`, `con_position`, `address`, `suburb`, `state`, `country`, `postcode`, `telephone`, `fax`, `misc`, `image`, `imagepos`, `email_to`, `default_con`, `published`, `checked_out`, `checked_out_time`, `ordering`, `params`, `user_id`, `catid`, `access`, `mobile`, `webpage`) VALUES
(1, 'Name', 'name', 'Hỗ trợ khách hàng', '705, Đường Thăng Long - Nội Bài', 'Hà Nội', 'Việt Nam', 'Việt Nam', 'Zip Code', '04-23.228.999', '04-36462527', 'Miscellanous info', '', 'top', 'contact@vieclamviet.com.vn', 0, 1, 62, '2010-12-29 04:32:08', 1, 'show_name=1\nshow_position=1\nshow_email=1\nshow_street_address=1\nshow_suburb=1\nshow_state=0\nshow_postcode=1\nshow_country=1\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nshow_webpage=1\nshow_misc=1\nshow_image=1\nallow_vcard=0\ncontact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_mobile=\nicon_fax=\nicon_misc=\nshow_email_form=1\nemail_description=1\nshow_email_copy=1\nbanned_email=\nbanned_subject=\nbanned_text=', 0, 12, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_content`
--

CREATE TABLE IF NOT EXISTS `jos_content` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `alias` varchar(255) NOT NULL default '',
  `title_alias` varchar(255) NOT NULL default '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL default '0',
  `sectionid` int(11) unsigned NOT NULL default '0',
  `mask` int(11) unsigned NOT NULL default '0',
  `catid` int(11) unsigned NOT NULL default '0',
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  `created_by` int(11) unsigned NOT NULL default '0',
  `created_by_alias` varchar(255) NOT NULL default '',
  `modified` datetime NOT NULL default '0000-00-00 00:00:00',
  `modified_by` int(11) unsigned NOT NULL default '0',
  `checked_out` int(11) unsigned NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL default '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL default '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` text NOT NULL,
  `version` int(11) unsigned NOT NULL default '1',
  `parentid` int(11) unsigned NOT NULL default '0',
  `ordering` int(11) NOT NULL default '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(11) unsigned NOT NULL default '0',
  `hits` int(11) unsigned NOT NULL default '0',
  `metadata` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_section` (`sectionid`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `jos_content`
--

INSERT INTO `jos_content` (`id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`) VALUES
(1, 'Welcome to Joomla!', 'welcome-to-joomla', '', '<div align="left"><strong>Joomla! is a free open source framework and content publishing system designed for quickly creating highly interactive multi-language Web sites, online communities, media portals, blogs and eCommerce applications. <br /></strong></div><p><strong><br /></strong><img src="images/stories/powered_by.png" border="0" alt="Joomla! Logo" title="Example Caption" hspace="6" vspace="0" width="165" height="68" align="left" />Joomla! provides an easy-to-use graphical user interface that simplifies the management and publishing of large volumes of content including HTML, documents, and rich media.  Joomla! is used by organisations of all sizes for intranets and extranets and is supported by a community of tens of thousands of users. </p>', 'With a fully documented library of developer resources, Joomla! allows the customisation of every aspect of a Web site including presentation, layout, administration, and the rapid integration with third-party applications.<p>Joomla! now provides more developer power while making the user experience all the more friendly. For those who always wanted increased extensibility, Joomla! 1.5 can make this happen.</p><p>A new framework, ground-up refactoring, and a highly-active development team brings the excitement of ''the next generation CMS'' to your fingertips.  Whether you are a systems architect or a complete ''noob'' Joomla! can take you to the next level of content delivery. ''More than a CMS'' is something we have been playing with as a catchcry because the new Joomla! API has such incredible power and flexibility, you are free to take whatever direction your creative mind takes you and Joomla! can help you get there so much more easily than ever before.</p><p>Thinking Web publishing? Think Joomla!</p>', -2, 1, 0, 1, '2008-08-12 10:00:00', 62, '', '2008-08-12 10:00:00', 62, 0, '0000-00-00 00:00:00', '2006-01-03 01:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 29, 0, 0, '', '', 0, 92, 'robots=\nauthor='),
(2, 'Newsflash 1', 'newsflash-1', '', '<p>Joomla! makes it easy to launch a Web site of any kind. Whether you want a brochure site or you are building a large online community, Joomla! allows you to deploy a new site in minutes and add extra functionality as you need it. The hundreds of available Extensions will help to expand your site and allow you to deliver new services that extend your reach into the Internet.</p>', '', -2, 1, 0, 3, '2008-08-10 06:30:34', 62, '', '2008-08-10 06:30:34', 62, 0, '0000-00-00 00:00:00', '2004-08-09 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 0, '', '', 0, 1, 'robots=\nauthor='),
(3, 'Newsflash 2', 'newsflash-2', '', '<p>The one thing about a Web site, it always changes! Joomla! makes it easy to add Articles, content, images, videos, and more. Site administrators can edit and manage content ''in-context'' by clicking the ''Edit'' link. Webmasters can also edit content through a graphical Control Panel that gives you complete control over your site.</p>', '', -2, 1, 0, 3, '2008-08-09 22:30:34', 62, '', '2008-08-09 22:30:34', 62, 0, '0000-00-00 00:00:00', '2004-08-09 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 0, '', '', 0, 0, 'robots=\nauthor='),
(4, 'Newsflash 3', 'newsflash-3', '', '<p>With a library of thousands of free <a href="http://extensions.joomla.org" target="_blank" title="The Joomla! Extensions Directory">Extensions</a>, you can add what you need as your site grows. Don''t wait, look through the <a href="http://extensions.joomla.org/" target="_blank" title="Joomla! Extensions">Joomla! Extensions</a>  library today. </p>', '', -2, 1, 0, 3, '2008-08-10 06:30:34', 62, '', '2008-08-10 06:30:34', 62, 0, '0000-00-00 00:00:00', '2004-08-09 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 0, '', '', 0, 1, 'robots=\nauthor='),
(5, 'Joomla! License Guidelines', 'joomla-license-guidelines', 'joomla-license-guidelines', '<p>This Web site is powered by <a href="http://joomla.org/" target="_blank" title="Joomla!">Joomla!</a> The software and default templates on which it runs are Copyright 2005-2008 <a href="http://www.opensourcematters.org/" target="_blank" title="Open Source Matters">Open Source Matters</a>. The sample content distributed with Joomla! is licensed under the <a href="http://docs.joomla.org/JEDL" target="_blank" title="Joomla! Electronic Document License">Joomla! Electronic Documentation License.</a> All data entered into this Web site and templates added after installation, are copyrighted by their respective copyright owners.</p> <p>If you want to distribute, copy, or modify Joomla!, you are welcome to do so under the terms of the <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.html#SEC1" target="_blank" title="GNU General Public License"> GNU General Public License</a>. If you are unfamiliar with this license, you might want to read <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.html#SEC4" target="_blank" title="How To Apply These Terms To Your Program">''How To Apply These Terms To Your Program''</a> and the <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0-faq.html" target="_blank" title="GNU General Public License FAQ">''GNU General Public License FAQ''</a>.</p> <p>The Joomla! licence has always been GPL.</p>', '', -2, 4, 0, 25, '2008-08-20 10:11:07', 62, '', '2008-08-20 10:11:07', 62, 0, '0000-00-00 00:00:00', '2004-08-19 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 0, '', '', 0, 100, 'robots=\nauthor='),
(6, 'We are Volunteers', 'we-are-volunteers', '', '<p>The Joomla Core Team and Working Group members are volunteer developers, designers, administrators and managers who have worked together to take Joomla! to new heights in its relatively short life. Joomla! has some wonderfully talented people taking Open Source concepts to the forefront of industry standards.  Joomla! 1.5 is a major leap forward and represents the most exciting Joomla! release in the history of the project. </p>', '', -2, 1, 0, 1, '2007-07-07 09:54:06', 62, '', '2007-07-07 09:54:06', 62, 0, '0000-00-00 00:00:00', '2004-07-06 22:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 10, 0, 0, '', '', 0, 54, 'robots=\nauthor='),
(9, 'Millions of Smiles', 'millions-of-smiles', '', '<p>The Joomla! team has millions of good reasons to be smiling about the Joomla! 1.5. In its current incarnation, it''s had millions of downloads, taking it to an unprecedented level of popularity.  The new code base is almost an entire re-factor of the old code base.  The user experience is still extremely slick but for developers the API is a dream.  A proper framework for real PHP architects seeking the best of the best.</p><p>If you''re a former Mambo User or a 1.0 series Joomla! User, 1.5 is the future of CMSs for a number of reasons.  It''s more powerful, more flexible, more secure, and intuitive.  Our developers and interface designers have worked countless hours to make this the most exciting release in the content management system sphere.</p><p>Go on ... get your FREE copy of Joomla! today and spread the word about this benchmark project. </p>', '', -2, 1, 0, 1, '2007-07-07 09:54:06', 62, '', '2007-07-07 09:54:06', 62, 0, '0000-00-00 00:00:00', '2004-07-06 22:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 0, '', '', 0, 23, 'robots=\nauthor='),
(10, 'How do I localise Joomla! to my language?', 'how-do-i-localise-joomla-to-my-language', '', '<h4>General<br /></h4><p>In Joomla! 1.5 all User interfaces can be localised. This includes the installation, the Back-end Control Panel and the Front-end Site.</p><p>The core release of Joomla! 1.5 is shipped with multiple language choices in the installation but, other than English (the default), languages for the Site and Administration interfaces need to be added after installation. Links to such language packs exist below.</p>', '<p>Translation Teams for Joomla! 1.5 may have also released fully localised installation packages where site, administrator and sample data are in the local language. These localised releases can be found in the specific team projects on the <a href="http://extensions.joomla.org/component/option,com_mtree/task,listcats/cat_id,1837/Itemid,35/" target="_blank" title="JED">Joomla! Extensions Directory</a>.</p><h4>How do I install language packs?</h4><ul><li>First download both the admin and the site language packs that you require.</li><li>Install each pack separately using the Extensions-&gt;Install/Uninstall Menu selection and then the package file upload facility.</li><li>Go to the Language Manager and be sure to select Site or Admin in the sub-menu. Then select the appropriate language and make it the default one using the Toolbar button.</li></ul><h4>How do I select languages?</h4><ul><li>Default languages can be independently set for Site and for Administrator</li><li>In addition, users can define their preferred language for each Site and Administrator. This takes affect after logging in.</li><li>While logging in to the Administrator Back-end, a language can also be selected for the particular session.</li></ul><h4>Where can I find Language Packs and Localised Releases?</h4><p><em>Please note that Joomla! 1.5 is new and language packs for this version may have not been released at this time.</em> </p><ul><li><a href="http://joomlacode.org/gf/project/jtranslation/" target="_blank" title="Accredited Translations">The Joomla! Accredited Translations Project</a>  - This is a joint repository for language packs that were developed by teams that are members of the Joomla! Translations Working Group.</li><li><a href="http://extensions.joomla.org/component/option,com_mtree/task,listcats/cat_id,1837/Itemid,35/" target="_blank" title="Translations">The Joomla! Extensions Site - Translations</a>  </li><li><a href="http://community.joomla.org/translations.html" target="_blank" title="Translation Work Group Teams">List of Translation Teams and Translation Partner Sites for Joomla! 1.5</a> </li></ul>', -2, 3, 0, 32, '2008-07-30 14:06:37', 62, '', '2008-07-30 14:06:37', 62, 0, '0000-00-00 00:00:00', '2006-09-29 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 9, 0, 0, '', '', 0, 10, 'robots=\nauthor='),
(11, 'How do I upgrade to Joomla! 1.5 ?', 'how-do-i-upgrade-to-joomla-15', '', '<p>Joomla! 1.5 does not provide an upgrade path from earlier versions. Converting an older site to a Joomla! 1.5 site requires creation of a new empty site using Joomla! 1.5 and then populating the new site with the content from the old site. This migration of content is not a one-to-one process and involves conversions and modifications to the content dump.</p> <p>There are two ways to perform the migration:</p>', ' <div id="post_content-107"><li>An automated method of migration has been provided which uses a migrator Component to create the migration dump out of the old site (Mambo 4.5.x up to Joomla! 1.0.x) and a smart import facility in the Joomla! 1.5 Installation that performs required conversions and modifications during the installation process.</li> <li>Migration can be performed manually. This involves exporting the required tables, manually performing required conversions and modifications and then importing the content to the new site after it is installed.</li>  <p><!--more--></p> <h2><strong> Automated migration</strong></h2>  <p>This is a two phased process using two tools. The first tool is a migration Component named <font face="courier new,courier">com_migrator</font>. This Component has been contributed by Harald Baer and is based on his <strong>eBackup </strong>Component. The migrator needs to be installed on the old site and when activated it prepares the required export dump of the old site''s data. The second tool is built into the Joomla! 1.5 installation process. The exported content dump is loaded to the new site and all conversions and modification are performed on-the-fly.</p> <h3><u> Step 1 - Using com_migrator to export data from old site:</u></h3> <li>Install the <font face="courier new,courier">com_migrator</font> Component on the <u><strong>old</strong></u> site. It can be found at the <a href="http://joomlacode.org/gf/project/pasamioprojects/frs/" target="_blank" title="JoomlaCode">JoomlaCode developers forge</a>.</li> <li>Select the Component in the Component Menu of the Control Panel.</li> <li>Click on the <strong>Dump it</strong> icon. Three exported <em>gzipped </em>export scripts will be created. The first is a complete backup of the old site. The second is the migration content of all core elements which will be imported to the new site. The third is a backup of all 3PD Component tables.</li> <li>Click on the download icon of the particular exports files needed and store locally.</li> <li>Multiple export sets can be created.</li> <li>The exported data is not modified in anyway and the original encoding is preserved. This makes the <font face="courier new,courier">com_migrator</font> tool a recommended tool to use for manual migration as well.</li> <h3><u> Step 2 - Using the migration facility to import and convert data during Joomla! 1.5 installation:</u></h3><p>Note: This function requires the use of the <em><font face="courier new,courier">iconv </font></em>function in PHP to convert encodings. If <em><font face="courier new,courier">iconv </font></em>is not found a warning will be provided.</p> <li>In step 6 - Configuration select the ''Load Migration Script'' option in the ''Load Sample Data, Restore or Migrate Backed Up Content'' section of the page.</li> <li>Enter the table prefix used in the content dump. For example: ''jos_'' or ''site2_'' are acceptable values.</li> <li>Select the encoding of the dumped content in the dropdown list. This should be the encoding used on the pages of the old site. (As defined in the _ISO variable in the language file or as seen in the browser page info/encoding/source)</li> <li>Browse the local host and select the migration export and click on <strong>Upload and Execute</strong></li> <li>A success message should appear or alternately a listing of database errors</li> <li>Complete the other required fields in the Configuration step such as Site Name and Admin details and advance to the final step of installation. (Admin details will be ignored as the imported data will take priority. Please remember admin name and password from the old site)</li> <p><u><br /></u></p></div>', -2, 3, 0, 28, '2008-07-30 20:27:52', 62, '', '2008-07-30 20:27:52', 62, 0, '0000-00-00 00:00:00', '2006-09-29 12:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 10, 0, 0, '', '', 0, 14, 'robots=\nauthor='),
(12, 'Why does Joomla! 1.5 use UTF-8 encoding?', 'why-does-joomla-15-use-utf-8-encoding', '', '<p>Well... how about never needing to mess with encoding settings again?</p><p>Ever needed to display several languages on one page or site and something always came up in Giberish?</p><p>With utf-8 (a variant of Unicode) glyphs (character forms) of basically all languages can be displayed with one single encoding setting. </p>', '', -2, 3, 0, 31, '2008-08-05 01:11:29', 62, '', '2008-08-05 01:11:29', 62, 0, '0000-00-00 00:00:00', '2006-10-03 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 8, 0, 0, '', '', 0, 29, 'robots=\nauthor='),
(13, 'What happened to the locale setting?', 'what-happened-to-the-locale-setting', '', 'This is now defined in the Language [<em>lang</em>].xml file in the Language metadata settings. If you are having locale problems such as dates do not appear in your language for example, you might want to check/edit the entries in the locale tag. Note that multiple locale strings can be set and the host will usually accept the first one recognised.', '', -2, 3, 0, 28, '2008-08-06 16:47:35', 62, '', '2008-08-06 16:47:35', 62, 0, '0000-00-00 00:00:00', '2006-10-05 14:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 0, '', '', 0, 11, 'robots=\nauthor='),
(14, 'What is the FTP layer for?', 'what-is-the-ftp-layer-for', '', '<p>The FTP Layer allows file operations (such as installing Extensions or updating the main configuration file) without having to make all the folders and files writable. This has been an issue on Linux and other Unix based platforms in respect of file permissions. This makes the site admin''s life a lot easier and increases security of the site.</p><p>You can check the write status of relevent folders by going to ''''Help-&gt;System Info" and then in the sub-menu to "Directory Permissions". With the FTP Layer enabled even if all directories are red, Joomla! will operate smoothly.</p><p>NOTE: the FTP layer is not required on a Windows host/server. </p>', '', -2, 3, 0, 31, '2008-08-06 21:27:49', 62, '', '2008-08-06 21:27:49', 62, 0, '0000-00-00 00:00:00', '2006-10-05 16:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=', 6, 0, 0, '', '', 0, 23, 'robots=\nauthor='),
(15, 'Can Joomla! 1.5 operate with PHP Safe Mode On?', 'can-joomla-15-operate-with-php-safe-mode-on', '', '<p>Yes it can! This is a significant security improvement.</p><p>The <em>safe mode</em> limits PHP to be able to perform actions only on files/folders who''s owner is the same as PHP is currently using (this is usually ''apache''). As files normally are created either by the Joomla! application or by FTP access, the combination of PHP file actions and the FTP Layer allows Joomla! to operate in PHP Safe Mode.</p>', '', -2, 3, 0, 31, '2008-08-06 19:28:35', 62, '', '2008-08-06 19:28:35', 62, 0, '0000-00-00 00:00:00', '2006-10-05 14:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 0, '', '', 0, 8, 'robots=\nauthor='),
(16, 'Only one edit window! How do I create "Read more..."?', 'only-one-edit-window-how-do-i-create-read-more', '', '<p>This is now implemented by inserting a <strong>Read more...</strong> tag (the button is located below the editor area) a dotted line appears in the edited text showing the split location for the <em>Read more....</em> A new Plugin takes care of the rest.</p><p>It is worth mentioning that this does not have a negative effect on migrated data from older sites. The new implementation is fully backward compatible.</p>', '', -2, 3, 0, 28, '2008-08-06 19:29:28', 62, '', '2008-08-06 19:29:28', 62, 0, '0000-00-00 00:00:00', '2006-10-05 14:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 0, '', '', 0, 20, 'robots=\nauthor='),
(17, 'My MySQL database does not support UTF-8. Do I have a problem?', 'my-mysql-database-does-not-support-utf-8-do-i-have-a-problem', '', 'No you don''t. Versions of MySQL lower than 4.1 do not have built in UTF-8 support. However, Joomla! 1.5 has made provisions for backward compatibility and is able to use UTF-8 on older databases. Let the installer take care of all the settings and there is no need to make any changes to the database (charset, collation, or any other).', '', -2, 3, 0, 31, '2008-08-07 09:30:37', 62, '', '2008-08-07 09:30:37', 62, 0, '0000-00-00 00:00:00', '2006-10-05 20:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 10, 0, 0, '', '', 0, 9, 'robots=\nauthor='),
(18, 'Joomla! Features', 'joomla-features', '', '<h4><font color="#ff6600">Joomla! features:</font></h4> <ul><li>Completely database driven site engines </li><li>News, products, or services sections fully editable and manageable</li><li>Topics sections can be added to by contributing Authors </li><li>Fully customisable layouts including <em>left</em>, <em>center</em>, and <em>right </em>Menu boxes </li><li>Browser upload of images to your own library for use anywhere in the site </li><li>Dynamic Forum/Poll/Voting booth for on-the-spot results </li><li>Runs on Linux, FreeBSD, MacOSX server, Solaris, and AIX', '  </li></ul> <h4>Extensive Administration:</h4> <ul><li>Change order of objects including news, FAQs, Articles etc. </li><li>Random Newsflash generator </li><li>Remote Author submission Module for News, Articles, FAQs, and Links </li><li>Object hierarchy - as many Sections, departments, divisions, and pages as you want </li><li>Image library - store all your PNGs, PDFs, DOCs, XLSs, GIFs, and JPEGs online for easy use </li><li>Automatic Path-Finder. Place a picture and let Joomla! fix the link </li><li>News Feed Manager. Easily integrate news feeds into your Web site.</li><li>E-mail a friend and Print format available for every story and Article </li><li>In-line Text editor similar to any basic word processor software </li><li>User editable look and feel </li><li>Polls/Surveys - Now put a different one on each page </li><li>Custom Page Modules. Download custom page Modules to spice up your site </li><li>Template Manager. Download Templates and implement them in seconds </li><li>Layout preview. See how it looks before going live </li><li>Banner Manager. Make money out of your site.</li></ul>', -2, 4, 0, 29, '2008-08-08 23:32:45', 62, '', '2008-08-08 23:32:45', 62, 0, '0000-00-00 00:00:00', '2006-10-07 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 11, 0, 0, '', '', 0, 59, 'robots=\nauthor='),
(19, 'Joomla! Overview', 'joomla-overview', '', '<p>If you''re new to Web publishing systems, you''ll find that Joomla! delivers sophisticated solutions to your online needs. It can deliver a robust enterprise-level Web site, empowered by endless extensibility for your bespoke publishing needs. Moreover, it is often the system of choice for small business or home users who want a professional looking site that''s simple to deploy and use. <em>We do content right</em>.<br /> </p><p>So what''s the catch? How much does this system cost?</p><p> Well, there''s good news ... and more good news! Joomla! 1.5 is free, it is released under an Open Source license - the GNU/General Public License v 2.0. Had you invested in a mainstream, commercial alternative, there''d be nothing but moths left in your wallet and to add new functionality would probably mean taking out a second mortgage each time you wanted something adding!</p><p>Joomla! changes all that ... <br />Joomla! is different from the normal models for content management software. For a start, it''s not complicated. Joomla! has been developed for everybody, and anybody can develop it further. It is designed to work (primarily) with other Open Source, free, software such as PHP, MySQL, and Apache. </p><p>It is easy to install and administer, and is reliable. </p><p>Joomla! doesn''t even require the user or administrator of the system to know HTML to operate it once it''s up and running.</p><p>To get the perfect Web site with all the functionality that you require for your particular application may take additional time and effort, but with the Joomla! Community support that is available and the many Third Party Developers actively creating and releasing new Extensions for the 1.5 platform on an almost daily basis, there is likely to be something out there to meet your needs. Or you could develop your own Extensions and make these available to the rest of the community. </p>', '', -2, 4, 0, 29, '2008-08-09 07:49:20', 62, '', '2008-08-09 07:49:20', 62, 0, '0000-00-00 00:00:00', '2006-10-07 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 13, 0, 0, '', '', 0, 150, 'robots=\nauthor='),
(20, 'Support and Documentation', 'support-and-documentation', '', '<h1>Support </h1><p>Support for the Joomla! CMS can be found on several places. The best place to start would be the <a href="http://docs.joomla.org/" target="_blank" title="Joomla! Official Documentation Wiki">Joomla! Official Documentation Wiki</a>. Here you can help yourself to the information that is regularly published and updated as Joomla! develops. There is much more to come too!</p> <p>Of course you should not forget the Help System of the CMS itself. On the <em>topmenu </em>in the Back-end Control panel you find the Help button which will provide you with lots of explanation on features.</p> <p>Another great place would of course be the <a href="http://forum.joomla.org/" target="_blank" title="Forum">Forum</a> . On the Joomla! Forum you can find help and support from Community members as well as from Joomla! Core members and Working Group members. The forum contains a lot of information, FAQ''s, just about anything you are looking for in terms of support.</p> <p>Two other resources for Support are the <a href="http://developer.joomla.org/" target="_blank" title="Joomla! Developer Site">Joomla! Developer Site</a> and the <a href="http://extensions.joomla.org/" target="_blank" title="Joomla! Extensions Directory">Joomla! Extensions Directory</a> (JED). The Joomla! Developer Site provides lots of technical information for the experienced Developer as well as those new to Joomla! and development work in general. The JED whilst not a support site in the strictest sense has many of the Extensions that you will need as you develop your own Web site.</p> <p>The Joomla! Developers and Bug Squad members are regularly posting their blog reports about several topics such as programming techniques and security issues.</p> <h1>Documentation</h1> <p>Joomla! Documentation can of course be found on the <a href="http://docs.joomla.org/" target="_blank" title="Joomla! Official Documentation Wiki">Joomla! Official Documentation Wiki</a>. You can find information for beginners, installation, upgrade, Frequently Asked Questions, developer topics, and a lot more. The Documentation Team helps oversee the wiki but you are invited to contribute content, as well.</p> <p>There are also books written about Joomla! You can find a listing of these books in the <a href="http://shop.joomla.org/" target="_blank" title="Joomla! Shop">Joomla! Shop</a>.</p>', '', -2, 4, 0, 25, '2008-08-09 08:33:57', 62, '', '2008-08-09 08:33:57', 62, 0, '0000-00-00 00:00:00', '2006-10-07 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 13, 0, 0, '', '', 0, 6, 'robots=\nauthor='),
(21, 'Joomla! Facts', 'joomla-facts', '', '<p>Here are some interesting facts about Joomla!</p><ul><li><span>Over 210,000 active registered Users on the <a href="http://forum.joomla.org" target="_blank" title="Joomla Forums">Official Joomla! community forum</a> and more on the many international community sites.</span><ul><li><span>over 1,000,000 posts in over 200,000 topics</span></li><li>over 1,200 posts per day</li><li>growing at 150 new participants each day!</li></ul></li><li><span>1168 Projects on the JoomlaCode (<a href="http://joomlacode.org/" target="_blank" title="JoomlaCode">joomlacode.org</a> ). All for open source addons by third party developers.</span><ul><li><span>Well over 6,000,000 downloads of Joomla! since the migration to JoomlaCode in March 2007.<br /></span></li></ul></li><li><span>Nearly 4,000 extensions for Joomla! have been registered on the <a href="http://extensions.joomla.org" target="_blank" title="http://extensions.joomla.org">Joomla! Extension Directory</a>  </span></li><li><span>Joomla.org exceeds 2 TB of traffic per month!</span></li></ul>', '', -2, 4, 0, 30, '2008-08-09 16:46:37', 62, '', '2008-08-09 16:46:37', 62, 0, '0000-00-00 00:00:00', '2006-10-07 14:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 13, 0, 0, '', '', 0, 50, 'robots=\nauthor='),
(22, 'What''s New in 1.5?', 'whats-new-in-15', '', '<p>As with previous releases, Joomla! provides a unified and easy-to-use framework for delivering content for Web sites of all kinds. To support the changing nature of the Internet and emerging Web technologies, Joomla! required substantial restructuring of its core functionality and we also used this effort to simplify many challenges within the current user interface. Joomla! 1.5 has many new features.</p>', '<p style="margin-bottom: 0in">In Joomla! 1.5, you''ll notice: </p>    <ul><li>     <p style="margin-bottom: 0in">       Substantially improved usability, manageability, and scalability far beyond the original Mambo foundations</p>   </li><li>     <p style="margin-bottom: 0in"> Expanded accessibility to support internationalisation, double-byte characters and right-to-left support for Arabic, Farsi, and Hebrew languages among others</p>   </li><li>     <p style="margin-bottom: 0in"> Extended integration of external applications through Web services and remote authentication such as the Lightweight Directory Access Protocol (LDAP)</p>   </li><li>     <p style="margin-bottom: 0in"> Enhanced content delivery, template and presentation capabilities to support accessibility standards and content delivery to any destination</p>   </li><li>     <p style="margin-bottom: 0in">       A more sustainable and flexible framework for Component and Extension developers</p>   </li><li>     <p style="margin-bottom: 0in">Backward compatibility with previous releases of Components, Templates, Modules, and other Extensions</p></li></ul>', -2, 4, 0, 29, '2008-08-11 22:13:58', 62, '', '2008-08-11 22:13:58', 62, 0, '0000-00-00 00:00:00', '2006-10-10 18:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 10, 0, 0, '', '', 0, 92, 'robots=\nauthor='),
(23, 'Platforms and Open Standards', 'platforms-and-open-standards', '', '<p class="MsoNormal">Joomla! runs on any platform including Windows, most flavours of Linux, several Unix versions, and the Apple OS/X platform.  Joomla! depends on PHP and the MySQL database to deliver dynamic content.  </p>            <p class="MsoNormal">The minimum requirements are:</p>      <ul><li>Apache 1.x, 2.x and higher</li><li>PHP 4.3 and higher</li><li>MySQL 3.23 and higher</li></ul>It will also run on alternative server platforms such as Windows IIS - provided they support PHP and MySQL - but these require additional configuration in order for the Joomla! core package to be successful installed and operated.', '', -2, 4, 0, 25, '2008-08-11 04:22:14', 62, '', '2008-08-11 04:22:14', 62, 0, '0000-00-00 00:00:00', '2006-10-10 08:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 0, '', '', 0, 11, 'robots=\nauthor='),
(24, 'Content Layouts', 'content-layouts', '', '<p>Joomla! provides plenty of flexibility when displaying your Web content. Whether you are using Joomla! for a blog site, news or a Web site for a company, you''ll find one or more content styles to showcase your information. You can also change the style of content dynamically depending on your preferences. Joomla! calls how a page is laid out a <strong>layout</strong>. Use the guide below to understand which layouts are available and how you might use them. </p> <h2>Content </h2> <p>Joomla! makes it extremely easy to add and display content. All content  is placed where your mainbody tag in your template is located. There are three main types of layouts available in Joomla! and all of them can be customised via parameters. The display and parameters are set in the Menu Item used to display the content your working on. You create these layouts by creating a Menu Item and choosing how you want the content to display.</p> <h3>Blog Layout<br /> </h3> <p>Blog layout will show a listing of all Articles of the selected blog type (Section or Category) in the mainbody position of your template. It will give you the standard title, and Intro of each Article in that particular Category and/or Section. You can customise this layout via the use of the Preferences and Parameters, (See Article Parameters) this is done from the Menu not the Section Manager!</p> <h3>Blog Archive Layout<br /> </h3> <p>A Blog Archive layout will give you a similar output of Articles as the normal Blog Display but will add, at the top, two drop down lists for month and year plus a search button to allow Users to search for all Archived Articles from a specific month and year.</p> <h3>List Layout<br /> </h3> <p>Table layout will simply give you a <em>tabular </em>list<em> </em>of all the titles in that particular Section or Category. No Intro text will be displayed just the titles. You can set how many titles will be displayed in this table by Parameters. The table layout will also provide a filter Section so that Users can reorder, filter, and set how many titles are listed on a single page (up to 50)</p> <h2>Wrapper</h2> <p>Wrappers allow you to place stand alone applications and Third Party Web sites inside your Joomla! site. The content within a Wrapper appears within the primary content area defined by the "mainbody" tag and allows you to display their content as a part of your own site. A Wrapper will place an IFRAME into the content Section of your Web site and wrap your standard template navigation around it so it appears in the same way an Article would.</p> <h2>Content Parameters</h2> <p>The parameters for each layout type can be found on the right hand side of the editor boxes in the Menu Item configuration screen. The parameters available depend largely on what kind of layout you are configuring.</p>', '', -2, 4, 0, 29, '2008-08-12 22:33:10', 62, '', '2008-08-12 22:33:10', 62, 0, '0000-00-00 00:00:00', '2006-10-11 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 11, 0, 0, '', '', 0, 70, 'robots=\nauthor='),
(25, 'What are the requirements to run Joomla! 1.5?', 'what-are-the-requirements-to-run-joomla-15', '', '<p>Joomla! runs on the PHP pre-processor. PHP comes in many flavours, for a lot of operating systems. Beside PHP you will need a Web server. Joomla! is optimized for the Apache Web server, but it can run on different Web servers like Microsoft IIS it just requires additional configuration of PHP and MySQL. Joomla! also depends on a database, for this currently you can only use MySQL. </p>Many people know from their own experience that it''s not easy to install an Apache Web server and it gets harder if you want to add MySQL, PHP and Perl. XAMPP, WAMP, and MAMP are easy to install distributions containing Apache, MySQL, PHP and Perl for the Windows, Mac OSX and Linux operating systems. These packages are for localhost installations on non-public servers only.<br />The minimum version requirements are:<br /><ul><li>Apache 1.x or 2.x</li><li>PHP 4.3 or up</li><li>MySQL 3.23 or up</li></ul>For the latest minimum requirements details, see <a href="http://www.joomla.org/about-joomla/technical-requirements.html" target="_blank" title="Joomla! Technical Requirements">Joomla! Technical Requirements</a>.', '', -2, 3, 0, 31, '2008-08-11 00:42:31', 62, '', '2008-08-11 00:42:31', 62, 0, '0000-00-00 00:00:00', '2006-10-10 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 0, '', '', 0, 25, 'robots=\nauthor='),
(26, 'Extensions', 'extensions', '', '<p>Out of the box, Joomla! does a great job of managing the content needed to make your Web site sing. But for many people, the true power of Joomla! lies in the application framework that makes it possible for developers all around the world to create powerful add-ons that are called <strong>Extensions</strong>. An Extension is used to add capabilities to Joomla! that do not exist in the base core code. Here are just some examples of the hundreds of available Extensions:</p> <ul>   <li>Dynamic form builders</li>   <li>Business or organisational directories</li>   <li>Document management</li>   <li>Image and multimedia galleries</li>   <li>E-commerce and shopping cart engines</li>   <li>Forums and chat software</li>   <li>Calendars</li>   <li>E-mail newsletters</li>   <li>Data collection and reporting tools</li>   <li>Banner advertising systems</li>   <li>Paid subscription services</li>   <li>and many, many, more</li> </ul> <p>You can find more examples over at our ever growing <a href="http://extensions.joomla.org" target="_blank" title="Joomla! Extensions Directory">Joomla! Extensions Directory</a>. Prepare to be amazed at the amount of exciting work produced by our active developer community!</p><p>A useful guide to the Extension site can be found at:<br /><a href="http://extensions.joomla.org/content/view/15/63/" target="_blank" title="Guide to the Joomla! Extension site">http://extensions.joomla.org/content/view/15/63/</a> </p> <h3>Types of Extensions </h3><p>There are five types of extensions:</p> <ul>   <li>Components</li>   <li>Modules</li>   <li>Templates</li>   <li>Plugins</li>   <li>Languages</li> </ul> <p>You can read more about the specifics of these using the links in the Article Index - a Table of Contents (yet another useful feature of Joomla!) - at the top right or by clicking on the <strong>Next </strong>link below.<br /> </p> <hr title="Components" class="system-pagebreak" /> <h3><img src="images/stories/ext_com.png" border="0" alt="Component - Joomla! Extension Directory" title="Component - Joomla! Extension Directory" width="17" height="17" /> Components</h3> <p>A Component is the largest and most complex of the Extension types.  Components are like mini-applications that render the main body of the  page. An analogy that might make the relationship easier to understand  would be that Joomla! is a book and all the Components are chapters in  the book. The core Article Component (<font face="courier new,courier">com_content</font>), for example, is the  mini-application that handles all core Article rendering just as the  core registration Component (<font face="courier new,courier">com_user</font>) is the mini-application  that handles User registration.</p> <p>Many of Joomla!''s core features are provided by the use of default Components such as:</p> <ul>   <li>Contacts</li>   <li>Front Page</li>   <li>News Feeds</li>   <li>Banners</li>   <li>Mass Mail</li>   <li>Polls</li></ul><p>A Component will manage data, set displays, provide functions, and in general can perform any operation that does not fall under the general functions of the core code.</p> <p>Components work hand in hand with Modules and Plugins to provide a rich variety of content display and functionality aside from the standard Article and content display. They make it possible to completely transform Joomla! and greatly expand its capabilities.</p>  <hr title="Modules" class="system-pagebreak" /> <h3><img src="images/stories/ext_mod.png" border="0" alt="Module - Joomla! Extension Directory" title="Module - Joomla! Extension Directory" width="17" height="17" /> Modules</h3> <p>A more lightweight and flexible Extension used for page rendering is a Module. Modules are used for small bits of the page that are generally  less complex and able to be seen across different Components. To  continue in our book analogy, a Module can be looked at as a footnote  or header block, or perhaps an image/caption block that can be rendered  on a particular page. Obviously you can have a footnote on any page but  not all pages will have them. Footnotes also might appear regardless of  which chapter you are reading. Simlarly Modules can be rendered  regardless of which Component you have loaded.</p> <p>Modules are like little mini-applets that can be placed anywhere on your site. They work in conjunction with Components in some cases and in others are complete stand alone snippets of code used to display some data from the database such as Articles (Newsflash) Modules are usually used to output data but they can also be interactive form items to input data for example the Login Module or Polls.</p> <p>Modules can be assigned to Module positions which are defined in your Template and in the back-end using the Module Manager and editing the Module Position settings. For example, "left" and "right" are common for a 3 column layout. </p> <h4>Displaying Modules</h4> <p>Each Module is assigned to a Module position on your site. If you wish it to display in two different locations you must copy the Module and assign the copy to display at the new location. You can also set which Menu Items (and thus pages) a Module will display on, you can select all Menu Items or you can pick and choose by holding down the control key and selecting multiple locations one by one in the Modules [Edit] screen</p> <p>Note: Your Main Menu is a Module! When you create a new Menu in the Menu Manager you are actually copying the Main Menu Module (<font face="courier new,courier">mod_mainmenu</font>) code and giving it the name of your new Menu. When you copy a Module you do not copy all of its parameters, you simply allow Joomla! to use the same code with two separate settings.</p> <h4>Newsflash Example</h4> <p>Newsflash is a Module which will display Articles from your site in an assignable Module position. It can be used and configured to display one Category, all Categories, or to randomly choose Articles to highlight to Users. It will display as much of an Article as you set, and will show a <em>Read more...</em> link to take the User to the full Article.</p> <p>The Newsflash Component is particularly useful for things like Site News or to show the latest Article added to your Web site.</p>  <hr title="Plugins" class="system-pagebreak" /> <h3><img src="images/stories/ext_plugin.png" border="0" alt="Plugin - Joomla! Extension Directory" title="Plugin - Joomla! Extension Directory" width="17" height="17" /> Plugins</h3> <p>One  of the more advanced Extensions for Joomla! is the Plugin. In previous  versions of Joomla! Plugins were known as Mambots. Aside from changing their name their  functionality has been expanded. A Plugin is a section of code that  runs when a pre-defined event happens within Joomla!. Editors are Plugins, for example, that execute when the Joomla! event <font face="courier new,courier">onGetEditorArea</font> occurs. Using a Plugin allows a developer to change  the way their code behaves depending upon which Plugins are installed  to react to an event.</p>  <hr title="Languages" class="system-pagebreak" /> <h3><img src="images/stories/ext_lang.png" border="0" alt="Language - Joomla! Extensions Directory" title="Language - Joomla! Extensions Directory" width="17" height="17" /> Languages</h3> <p>New  to Joomla! 1.5 and perhaps the most basic and critical Extension is a Language. Joomla! is released with multiple Installation Languages but the base Site and Administrator are packaged in just the one Language <strong>en-GB</strong> - being English with GB spelling for example. To include all the translations currently available would bloat the core package and make it unmanageable for uploading purposes. The Language files enable all the User interfaces both Front-end and Back-end to be presented in the local preferred language. Note these packs do not have any impact on the actual content such as Articles. </p> <p>More information on languages is available from the <br />   <a href="http://community.joomla.org/translations.html" target="_blank" title="Joomla! Translation Teams">http://community.joomla.org/translations.html</a></p>', '', -2, 4, 0, 29, '2008-08-11 06:00:00', 62, '', '2008-08-11 06:00:00', 62, 0, '0000-00-00 00:00:00', '2006-10-10 22:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 24, 0, 0, 'About Joomla!, General, Extensions', '', 0, 102, 'robots=\nauthor=');
INSERT INTO `jos_content` (`id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`) VALUES
(27, 'The Joomla! Community', 'the-joomla-community', '', '<p><strong>Got a question? </strong>With more than 210,000 members, the Joomla! Discussion Forums at <a href="http://forum.joomla.org/" target="_blank" title="Forums">forum.joomla.org</a> are a great resource for both new and experienced users. Ask your toughest questions the community is waiting to see what you''ll do with your Joomla! site.</p><p><strong>Do you want to show off your new Joomla! Web site?</strong> Visit the <a href="http://forum.joomla.org/viewforum.php?f=514" target="_blank" title="Site Showcase">Site Showcase</a> section of our forum.</p><p><strong>Do you want to contribute?</strong></p><p>If you think working with Joomla is fun, wait until you start working on it. We''re passionate about helping Joomla users become contributors. There are many ways you can help Joomla''s development:</p><ul>	<li>Submit news about Joomla. We syndicate Joomla-related news on <a href="http://news.joomla.org" target="_blank" title="JoomlaConnect">JoomlaConnect<sup>TM</sup></a>. If you have Joomla news that you would like to share with the community, find out how to get connected <a href="http://community.joomla.org/connect.html" target="_blank" title="JoomlaConnect">here</a>.</li>	<li>Report bugs and request features in our <a href="http://joomlacode.org/gf/project/joomla/tracker/" target="_blank" title="Joomla! developement trackers">trackers</a>. Please read <a href="http://docs.joomla.org/Filing_bugs_and_issues" target="_blank" title="Reporting Bugs">Reporting Bugs</a>, for details on how we like our bug reports served up</li><li>Submit patches for new and/or fixed behaviour. Please read <a href="http://docs.joomla.org/Patch_submission_guidelines" target="_blank" title="Submitting Patches">Submitting Patches</a>, for details on how to submit a patch.</li><li>Join the <a href="http://forum.joomla.org/viewforum.php?f=509" target="_blank" title="Joomla! development forums">developer forums</a> and share your ideas for how to improve Joomla. We''re always open to suggestions, although we''re likely to be sceptical of large-scale suggestions without some code to back it up.</li><li>Join any of the <a href="http://www.joomla.org/about-joomla/the-project/working-groups.html" target="_blank" title="Joomla! working groups">Joomla Working Groups</a> and bring your personal expertise to the Joomla community. </li></ul><p>These are just a few ways you can contribute. See <a href="http://www.joomla.org/about-joomla/contribute-to-joomla.html" target="_blank" title="Contribute">Contribute to Joomla</a> for many more ways.</p>', '', -2, 4, 0, 30, '2008-08-12 16:50:48', 62, '', '2008-08-12 16:50:48', 62, 0, '0000-00-00 00:00:00', '2006-10-11 02:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 12, 0, 0, '', '', 0, 53, 'robots=\nauthor='),
(28, 'How do I install Joomla! 1.5?', 'how-do-i-install-joomla-15', '', '<p>Installing of Joomla! 1.5 is pretty easy. We assume you have set-up your Web site, and it is accessible with your browser.<br /><br />Download Joomla! 1.5, unzip it and upload/copy the files into the directory you Web site points to, fire up your browser and enter your Web site address and the installation will start.  </p><p>For full details on the installation processes check out the <a href="http://help.joomla.org/content/category/48/268/302" target="_blank" title="Joomla! 1.5 Installation Manual">Installation Manual</a> on the <a href="http://help.joomla.org" target="_blank" title="Joomla! Help Site">Joomla! Help Site</a> where you will also find download instructions for a PDF version too. </p>', '', -2, 3, 0, 31, '2008-08-11 01:10:59', 62, '', '2008-08-11 01:10:59', 62, 0, '0000-00-00 00:00:00', '2006-10-10 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 0, '', '', 0, 5, 'robots=\nauthor='),
(29, 'What is the purpose of the collation selection in the installation screen?', 'what-is-the-purpose-of-the-collation-selection-in-the-installation-screen', '', 'The collation option determines the way ordering in the database is done. In languages that use special characters, for instance the German umlaut, the database collation determines the sorting order. If you don''t know which collation you need, select the "utf8_general_ci" as most languages use this. The other collations listed are exceptions in regards to the general collation. If your language is not listed in the list of collations it most likely means that "utf8_general_ci is suitable.', '', -2, 3, 0, 32, '2008-08-11 03:11:38', 62, '', '2008-08-11 03:11:38', 62, 0, '0000-00-00 00:00:00', '2006-10-10 08:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=', 4, 0, 0, '', '', 0, 6, 'robots=\nauthor='),
(30, 'What languages are supported by Joomla! 1.5?', 'what-languages-are-supported-by-joomla-15', '', 'Within the Installer you will find a wide collection of languages. The installer currently supports the following languages: Arabic, Bulgarian, Bengali, Czech, Danish, German, Greek, English, Spanish, Finnish, French, Hebrew, Devanagari(India), Croatian(Croatia), Magyar (Hungary), Italian, Malay, Norwegian bokmal, Dutch, Portuguese(Brasil), Portugues(Portugal), Romanian, Russian, Serbian, Svenska, Thai and more are being added all the time.<br />By default the English language is installed for the Back and Front-ends. You can download additional language files from the <a href="http://extensions.joomla.org" target="_blank" title="Joomla! Extensions Directory">Joomla!Extensions Directory</a>. ', '', -2, 3, 0, 32, '2008-08-11 01:12:18', 62, '', '2008-08-11 01:12:18', 62, 0, '0000-00-00 00:00:00', '2006-10-10 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 0, '', '', 0, 8, 'robots=\nauthor='),
(31, 'Is it useful to install the sample data?', 'is-it-useful-to-install-the-sample-data', '', 'Well you are reading it right now! This depends on what you want to achieve. If you are new to Joomla! and have no clue how it all fits together, just install the sample data. If you don''t like the English sample data because you - for instance - speak Chinese, then leave it out.', '', -2, 3, 0, 27, '2008-08-11 09:12:55', 62, '', '2008-08-11 09:12:55', 62, 0, '0000-00-00 00:00:00', '2006-10-10 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 0, '', '', 0, 3, 'robots=\nauthor='),
(32, 'Where is the Static Content Item?', 'where-is-the-static-content', '', '<p>In Joomla! versions prior to 1.5 there were separate processes for creating a Static Content Item and normal Content Items. The processes have been combined now and whilst both content types are still around they are renamed as Articles for Content Items and Uncategorized Articles for Static Content Items. </p><p>If you want to create a static item, create a new Article in the same way as for standard content and rather than relating this to a particular Section and Category just select <span style="font-style: italic">Uncategorized</span> as the option in the Section and Category drop down lists.</p>', '', -2, 3, 0, 28, '2008-08-10 23:13:33', 62, '', '2008-08-10 23:13:33', 62, 0, '0000-00-00 00:00:00', '2006-10-10 04:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 0, '', '', 0, 5, 'robots=\nauthor='),
(33, 'What is an Uncategorised Article?', 'what-is-uncategorised-article', '', 'Most Articles will be assigned to a Section and Category. In many cases, you might not know where you want it to appear so put the Article in the <em>Uncategorized </em>Section/Category. The Articles marked as <em>Uncategorized </em>are handled as static content.', '', -2, 3, 0, 31, '2008-08-11 15:14:11', 62, '', '2008-08-11 15:14:11', 62, 0, '0000-00-00 00:00:00', '2006-10-10 12:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 8, 0, 0, '', '', 0, 6, 'robots=\nauthor='),
(34, 'Does the PDF icon render pictures and special characters?', 'does-the-pdf-icon-render-pictures-and-special-characters', '', 'Yes! Prior to Joomla! 1.5, only the text values of an Article and only for ISO-8859-1 encoding was allowed in the PDF rendition. With the new PDF library in place, the complete Article including images is rendered and applied to the PDF. The PDF generator also handles the UTF-8 texts and can handle any character sets from any language. The appropriate fonts must be installed but this is done automatically during a language pack installation.', '', -2, 3, 0, 32, '2008-08-11 17:14:57', 62, '', '2008-08-11 17:14:57', 62, 0, '0000-00-00 00:00:00', '2006-10-10 14:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 0, '', '', 0, 6, 'robots=\nauthor='),
(35, 'Is it possible to change A Menu Item''s Type?', 'is-it-possible-to-change-the-types-of-menu-entries', '', '<p>You indeed can change the Menu Item''s Type to whatever you want, even after they have been created. </p><p>If, for instance, you want to change the Blog Section of a Menu link, go to the Control Panel-&gt;Menus Menu-&gt;[menuname]-&gt;Menu Item Manager and edit the Menu Item. Select the <strong>Change Type</strong> button and choose the new style of Menu Item Type from the available list. Thereafter, alter the Details and Parameters to reconfigure the display for the new selection  as you require it.</p>', '', -2, 3, 0, 31, '2008-08-10 23:15:36', 62, '', '2008-08-10 23:15:36', 62, 0, '0000-00-00 00:00:00', '2006-10-10 04:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 0, '', '', 0, 18, 'robots=\nauthor='),
(36, 'Where did the Installers go?', 'where-did-the-installer-go', '', 'The improved Installer can be found under the Extensions Menu. With versions prior to Joomla! 1.5 you needed to select a specific Extension type when you wanted to install it and use the Installer associated with it, with Joomla! 1.5 you just select the Extension you want to upload, and click on install. The Installer will do all the hard work for you.', '', -2, 3, 0, 28, '2008-08-10 23:16:20', 62, '', '2008-08-10 23:16:20', 62, 0, '0000-00-00 00:00:00', '2006-10-10 04:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 0, '', '', 0, 4, 'robots=\nauthor='),
(37, 'Where did the Mambots go?', 'where-did-the-mambots-go', '', '<p>Mambots have been renamed as Plugins. </p><p>Mambots were introduced in Mambo and offered possibilities to add plug-in logic to your site mainly for the purpose of manipulating content. In Joomla! 1.5, Plugins will now have much broader capabilities than Mambots. Plugins are able to extend functionality at the framework layer as well.</p>', '', -2, 3, 0, 28, '2008-08-11 09:17:00', 62, '', '2008-08-11 09:17:00', 62, 0, '0000-00-00 00:00:00', '2006-10-10 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 0, '', '', 0, 4, 'robots=\nauthor='),
(38, 'I installed with my own language, but the Back-end is still in English', 'i-installed-with-my-own-language-but-the-back-end-is-still-in-english', '', '<p>A lot of different languages are available for the Back-end, but by default this language may not be installed. If you want a translated Back-end, get your language pack and install it using the Extension Installer. After this, go to the Extensions Menu, select Language Manager and make your language the default one. Your Back-end will be translated immediately.</p><p>Users who have access rights to the Back-end may choose the language they prefer in their Personal Details parameters. This is of also true for the Front-end language.</p><p> A good place to find where to download your languages and localised versions of Joomla! is <a href="http://extensions.joomla.org/index.php?option=com_mtree&task=listcats&cat_id=1837&Itemid=35" target="_blank" title="Translations for Joomla!">Translations for Joomla!</a> on JED.</p>', '', -2, 3, 0, 32, '2008-08-11 17:18:14', 62, '', '2008-08-11 17:18:14', 62, 0, '0000-00-00 00:00:00', '2006-10-10 14:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 0, '', '', 0, 7, 'robots=\nauthor='),
(39, 'How do I remove an Article?', 'how-do-i-remove-an-article', '', '<p>To completely remove an Article, select the Articles that you want to delete and move them to the Trash. Next, open the Article Trash in the Content Menu and select the Articles you want to delete. After deleting an Article, it is no longer available as it has been deleted from the database and it is not possible to undo this operation.  </p>', '', -2, 3, 0, 27, '2008-08-11 09:19:01', 62, '', '2008-08-11 09:19:01', 62, 0, '0000-00-00 00:00:00', '2006-10-10 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 0, '', '', 0, 4, 'robots=\nauthor='),
(40, 'What is the difference between Archiving and Trashing an Article? ', 'what-is-the-difference-between-archiving-and-trashing-an-article', '', '<p>When you <em>Archive </em>an Article, the content is put into a state which removes it from your site as published content. The Article is still available from within the Control Panel and can be <em>retrieved </em>for editing or republishing purposes. Trashed Articles are just one step from being permanently deleted but are still available until you Remove them from the Trash Manager. You should use Archive if you consider an Article important, but not current. Trash should be used when you want to delete the content entirely from your site and from future search results.  </p>', '', -2, 3, 0, 27, '2008-08-11 05:19:43', 62, '', '2008-08-11 05:19:43', 62, 0, '0000-00-00 00:00:00', '2006-10-10 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 8, 0, 0, '', '', 0, 5, 'robots=\nauthor='),
(41, 'Newsflash 5', 'newsflash-5', '', 'Joomla! 1.5 - ''Experience the Freedom''!. It has never been easier to create your own dynamic Web site. Manage all your content from the best CMS admin interface and in virtually any language you speak.', '', -2, 1, 0, 3, '2008-08-12 00:17:31', 62, '', '2008-08-12 00:17:31', 62, 0, '0000-00-00 00:00:00', '2006-10-11 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 0, '', '', 0, 4, 'robots=\nauthor='),
(42, 'Newsflash 4', 'newsflash-4', '', 'Yesterday all servers in the U.S. went out on strike in a bid to get more RAM and better CPUs. A spokes person said that the need for better RAM was due to some fool increasing the front-side bus speed. In future, buses will be told to slow down in residential motherboards.', '', -2, 1, 0, 3, '2008-08-12 00:25:50', 62, '', '2008-08-12 00:25:50', 62, 0, '0000-00-00 00:00:00', '2006-10-11 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 0, '', '', 0, 5, 'robots=\nauthor='),
(43, 'Example Pages and Menu Links', 'example-pages-and-menu-links', '', '<p>This page is an example of content that is <em>Uncategorized</em>; that is, it does not belong to any Section or Category. You will see there is a new Menu in the left column. It shows links to the same content presented in 4 different page layouts.</p>\r\n<ul>\r\n<li>Section Blog</li>\r\n<li>Section Table</li>\r\n<li> Blog Category</li>\r\n<li>Category Table</li>\r\n</ul>\r\n<p>Follow the links in the <strong>Example Pages</strong> Menu to see some of the options available to you to present all the different types of content included within the default installation of Joomla!.</p>\r\n<p>This includes Components and individual Articles. These links or Menu Item Types (to give them their proper name) are all controlled from within the <strong><span style="font-family: courier new,courier;">Menu Manager-&gt;[menuname]-&gt;Menu Items Manager</span></strong>.</p>', '', 1, 0, 0, 0, '2008-08-12 09:26:52', 62, '', '2010-12-21 10:20:15', 62, 0, '0000-00-00 00:00:00', '2006-10-11 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 8, 0, 1, 'Uncategorized, Uncategorized, Example Pages and Menu Links', '', 0, 44, 'robots=\nauthor='),
(44, 'Joomla! Security Strike Team', 'joomla-security-strike-team', '', '<p>The Joomla! Project has assembled a top-notch team of experts to form the new Joomla! Security Strike Team. This new team will solely focus on investigating and resolving security issues. Instead of working in relative secrecy, the JSST will have a strong public-facing presence at the <a href="http://developer.joomla.org/security.html" target="_blank" title="Joomla! Security Center">Joomla! Security Center</a>.</p>', '<p>The new JSST will call the new <a href="http://developer.joomla.org/security.html" target="_blank" title="Joomla! Security Center">Joomla! Security Center</a> their home base. The Security Center provides a public presence for <a href="http://developer.joomla.org/security/news.html" target="_blank" title="Joomla! Security News">security issues</a> and a platform for the JSST to <a href="http://developer.joomla.org/security/articles-tutorials.html" target="_blank" title="Joomla! Security Articles">help the general public better understand security</a> and how it relates to Joomla!. The Security Center also offers users a clearer understanding of how security issues are handled. There''s also a <a href="http://feeds.joomla.org/JoomlaSecurityNews" target="_blank" title="Joomla! Security News Feed">news feed</a>, which provides subscribers an up-to-the-minute notification of security issues as they arise.</p>', -2, 1, 0, 1, '2007-07-07 09:54:06', 62, '', '2007-07-07 09:54:06', 62, 0, '0000-00-00 00:00:00', '2004-07-06 22:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 0, '', '', 0, 0, 'robots=\nauthor='),
(45, 'Joomla! Community Portal', 'joomla-community-portal', '', '<p>The <a href="http://community.joomla.org/" target="_blank" title="Joomla! Community Portal">Joomla! Community Portal</a> is now online. There, you will find a constant source of information about the activities of contributors powering the Joomla! Project. Learn about <a href="http://community.joomla.org/events.html" target="_blank" title="Joomla! Events">Joomla! Events</a> worldwide, and see if there is a <a href="http://community.joomla.org/user-groups.html" target="_blank" title="Joomla! User Groups">Joomla! User Group</a> nearby.</p><p>The <a href="http://magazine.joomla.org/" target="_blank" title="Joomla! Community Magazine">Joomla! Community Magazine</a> promises an interesting overview of feature articles, community accomplishments, learning topics, and project updates each month. Also, check out <a href="http://community.joomla.org/connect.html" target="_blank" title="JoomlaConnect">JoomlaConnect&#0153;</a>. This aggregated RSS feed brings together Joomla! news from all over the world in your language. Get the latest and greatest by clicking <a href="http://community.joomla.org/connect.html" target="_blank" title="JoomlaConnect">here</a>.</p>', '', -2, 1, 0, 1, '2007-07-07 09:54:06', 62, '', '2007-07-07 09:54:06', 62, 0, '0000-00-00 00:00:00', '2004-07-06 22:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 0, '', '', 0, 5, 'robots=\nauthor='),
(46, 'TÀI LIỆU BIỂU MẪU', 'tai-liu-biu-mu', '', '<p>Các mẫu tài liệu giúp bạn chuẩn bị tốt hơn cho CV ứng tuyển của mình</p>', '', 1, 7, 0, 34, '2010-10-18 08:50:42', 62, '', '2010-12-18 02:23:31', 62, 0, '0000-00-00 00:00:00', '2010-10-18 08:50:42', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 9, 0, 2, '', '', 0, 74, 'robots=\nauthor='),
(47, 'Đăng hồ sơ', 'ng-h-s', '', '<p>Hãy tạo hồ sơ ấn tượng với nhà tuyển dụng để tạo được nhiều cơ hội cho mình.</p>', '', 1, 5, 0, 35, '2010-10-18 08:51:05', 62, '', '2010-12-18 01:53:44', 62, 0, '0000-00-00 00:00:00', '2010-10-18 08:51:05', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 3, 0, 1, '', '', 0, 45, 'robots=\nauthor='),
(48, 'Thông báo việc làm', 'thong-bao-vic-lam', '', '<p>Bạn dễ dàng tìm thấy các công việc phù hợp với yêu cầu qua các công cụ tìm kiếm thân thiện.</p>', '', 1, 5, 0, 36, '2010-10-18 08:51:39', 62, '', '2010-12-18 01:58:51', 62, 0, '0000-00-00 00:00:00', '2010-10-18 08:51:39', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 1, '', '', 0, 43, 'robots=\nauthor='),
(49, 'Nhật ký tìm việc làm', 'nht-ky-tim-vic-lam', '', '<p>Bạn dễ dàng lưu và ứng tuyển vào công việc phù hợp với yêu cầu của mình</p>', '', 1, 5, 0, 38, '2010-10-18 08:52:27', 62, '', '2010-12-18 02:00:00', 62, 0, '0000-00-00 00:00:00', '2010-10-18 08:52:27', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 1, '', '', 0, 47, 'robots=\nauthor='),
(50, 'Nhiều “bẫy tuyển dụng” trên mạng', 'nhiu-by-tuyn-dng-tren-mng', '', '<p class="news_tvnghe"><strong><img src="images/stories/baytuyendungnew.gif" border="0" width="175" height="143" style="margin: 5px; float: left;" /></strong>Phỏng vấn xin việc ở…quán trà đá . Nếu như hiện nay, việc dán, phát tờ rơi, quảng cáo tuyển dụng việc làm bị lực lượng chức năng giám sát và xử lý nghiêm thì việc tung các tin</p>\r\n', '\r\n<p>tuyển người làm việc lại nở rộ trên các trang quảng cáo, rao vặt. Đánh đúng vào tâm lý của các bạn sinh viên trẻ là có được việc làm thêm, tăng thu nhập dịp hè, chuẩn bị cho năm học mới, các tin cung cấp việc làm thêm cũng đưa ra mức lương hấp dẫn: tuyển gấp kế toán, lương 3 triệu đồng, không môi giới… Thế nhưng, rất nhiều trong những tin quảng cáo đó là "bẫy tuyển dụng" lừa đảo sinh viên.   Nguyễn Ngọc Minh, sinh viên Học viện Báo chí và Tuyên truyền, qua đọc mẩu tin quảng cáo việc làm trên một tờ báo điện tử, Minh đã điện thoại cho người đăng tin tên Quân để xin đi làm kế toán. Thế nhưng, khác với tưởng tượng của Minh, cuộc phỏng vấn xin việc lại diễn ra ở quán trà đá vỉa hè.  Người đàn ông này tự nhận mình tên Quân, hiện là Trưởng phòng Tuyển dụng của Công ty TNHH HN. Minh như được "rót mật" vào tai về công việc trong mơ với mức lương 3 triệu đồng. Đang nói chuyện, người đàn ông này bảo điện thoại hết pin, muốn mượn điện thoại của Minh để giao dịch công việc.  Vừa bấm điện thoại xong, người đàn ông này lấy lý do ồn đi ra góc khuất cạnh quán trà đá để nói chuyện cho yên tĩnh. Chờ 20 phút vẫn không thấy người đàn ông này quay lại, chiếc điện thoại của Minh cũng "mất hút" cùng người đàn ông này.  <br /><br /><strong>"Bẫy" quảng cáo</strong><br /><br />Từ một thông tin tuyển dụng trên trang web rongbay.com, Đào Thị H., sinh viên năm 3, Đại học DL Phương Đông đến xin việc tại một công ty tuyển trực điện thoại tại đường Trần Quốc Hoàn, Cầu Giấy, Hà Nội.  H. được yêu cầu nộp số tiền 250.000 đồng tiền phí, 50.000 đồng tiền đào tạo và đi làm luôn tại công ty với công việc đơn giản: Chỉ cần ngồi ở công ty nghe điện thoại là H. sẽ được nhận số tiền lương 2 triệu đồng/tháng. Thế nhưng, đến ngày hôm sau đi làm, H. bị đưa thêm yêu cầu phải "môi giới" được 2 người lao động 1 ngày mới được nhận tiền lương.  Cho đến ngày thứ 3, H. chỉ "dụ dỗ" được 1 người đến xin việc. Chủ công ty lạnh lùng thông báo tháng này H. lao động năng xuất thấp, bị trừ 50% lương. Ấm ức nhưng không biết làm thế nào, H. xin nghỉ việc và đòi 50% lương còn lại thì bị chủ công ty áp đặt luôn: Em tự phá hợp đồng nên không nhận được lương. Cũng đọc thông báo tuyển dụng nhân viên văn phòng công ty với mức lương 4,5 triệu đồng/tháng trên mạng, chị Nguyễn Phương Lê ở Đan Phượng, Hà Nội không quản ngại trưa hè nắng gắt, đến địa điểm đăng thông tin. Chị đã nộp cho nhân viên của công ty 200.000đ tiền phí giao dịch nhưng không có hóa đơn với cái hẹn "ngày mai đến làm việc, nhớ mang theo 3 triệu đồng tiền đặt cọc để đi làm".  Chị Lê đã tặc lưỡi nộp 3 triệu đồng và được họ dẫn đến một công ty giới thiệu việc làm ở đường Trương Định. Công việc đâu chưa thấy, chị Lê cả ngày chỉ ngồi trực điện thoại của những người gọi đến để xin việc và… ngủ gật. Bức xúc vì không có việc làm như quảng cáo, chị Lê quay lại nơi tuyển dụng ban đầu xin lại tiền đặt cọc thì bị từ chối thẳng thừng "bỏ dở nửa chừng thì mất tiền đặt cọc, không đọc kỹ hợp đồng à?".  Trường hợp của anh Phạm Văn Hải phản ánh đến Đường dây nóng Báo CAND là một ví dụ. Anh Hải đã nộp 1 triệu đồng cho công ty tuyển dụng. Được họ đưa  địa chỉ nơi làm việc, anh đi lòng vòng 2 tiếng đồng hồ mà vẫn không tìm thấy công ty đó đâu vì địa chỉ đó là… cây xăng...  <br /><br />Tìm kiếm việc làm là một nguyện vọng chính đáng của nhiều sinh viên, để họ tránh bị lừa, các cơ quan chức năng cần tiến hành kiểm tra công tác "hậu" đăng ký kinh doanh trên toàn thành phố. Khi bị mắc "bẫy", người bị hại cần đến ngay cơ quan Công an nơi gần nhất để trình báo, giúp cơ quan bảo vệ pháp luật có căn cứ để điều tra, xác minh<br /><br />Theo Đình Phương ( cand.com.vn)</p>\r\n<p> </p>', 1, 5, 0, 39, '2010-10-18 09:20:57', 62, '', '2010-12-08 05:35:43', 62, 0, '0000-00-00 00:00:00', '2010-10-18 09:20:57', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 4, '', '', 0, 85, 'robots=\nauthor='),
(51, 'Bí quyết viết hồ sơ xin việc trực tuyến hiệu quả ', 'bi-quyt-vit-h-s-xin-vic-trc-tuyn-hiu-qu-', '', '<p><img src="images/stories/img_new2s.jpg" border="0" width="152" style="float: left; margin: 5px;" />Một trong những nhân tố giúp bạn trẻ thành công trong  việc chinh phục nhà tuyển dụng là cần phải chuẩn bị cho mình một bảng sơ yếu lý lịch \r\n', '\r\ntuyệt vời để “ra mắt” nhà tuyển dụng. Vì hồ sơ xin việc của bạn là yếu tố đầu tiên tạo ấn tượng mạnh mẽ nơi nhà tuyển dụng, chính vì điều đó bạn phải chuẩn bị nó thật kỹ để thể hiện những kinh nghiệm và mô tả về bản thân một cách tốt nhất, có lợi nhất cho bạn. Sau đây là những điều bạn cần lưu ý để tạo một hồ sơ trực tuyến hiệu quả.<br /><br />Tạo một ấn tượng ban đầu mạnh mẽ<br />Hầu hết nhà tuyển dụng thường dành chưa tới 30 giây để đọc CV của bạn. Vì vậy, ấn tượng ban đầu là rất quan trọng.<br />Chẳng hạn khi đọc một bài báo, bao giờ bạn cũng đọc lướt những dòng đầu tiên, sau đó mới quyết định xem có đọc tiếp hay không. Tương tự như vậy, nhà tuyển dụng chỉ lướt qua CV của bạn và nhìn vào một điểm nhấn duy nhất trong đó mà thôi. Vì thế, hãy tạo ấn tượng với nhà tuyển dụng bằng việc chú trọng đến điểm nổi bật nhất của bạn.<br />Bạn có thể nhờ một người đáng tin cậy để chỉnh sửa CV cho bạn. Đây là cách rất tốt giúp bạn vượt qua những thách thức và gây được sự chú ý với nhà tuyển dụng. Tất nhiên, cách rẻ nhất vẫn là bạn tự chỉnh sửa nó. Có rất nhiều cuốn sách và websites cung cấp các mẫu CV, bạn có thể đọc để tham khảo và hoàn chỉnh cv của bạn giúp bạn để được kỹ càng hơn trước khi gửi đến nhà tuyển dụng.<br /><br />Thu thập thông tin<br />Điều trước tiên bạn cần phải lưu ý là mục tiêu của bạn khi viết resume. Bạn nên trả lời được các câu hỏi chẳng hạn như: Bạn viết resume này cho vị trí nào? những yêu cầu của nhà tuyển dụng cho vị trí ấy là gì? khi nắm rõ những mô tả cụ thể từ phía nhà tuyển dụng và hiểu được sự thích hợp của bản thân cho vị trí ấy ở mức nào, bạn có thể bắt đầu viết một cv hiệu quả và thu hút nhà tuyển dụng.<br /><br />Nhấn mạnh điểm nổi bật của bản thân<br />Đầu tiên, hãy chỉ ra điểm mạnh của bạn bởi nhà tuyển dụng thường không có thời gian đọc hết một đoạn văn dài trong CV để tìm ra những kỹ năng quan trọng của bạn. Do vậy, hãy thử làm theo những lời khuyên dưới đây.<br /> - Viết các tiêu đề cho CV hoặc slogan ấn tượng gắn với vị trí, ước muốn mà bạn dự tuyển vào công ty.<br />Tiêu đề là cái mà nhà tuyển dụng sẽ nhìn nó đầu tiên. Những CV được chú ý nhiều nhất thường có những tiêu đề bắt mắt và hấp dẫn người đọc.<br />- Dùng những động từ có tác động mạnh để làm nổi bật các kỹ năng. Từ khoá là những tính từ và danh từ để miêu tả những kỹ năng và kinh nghiệm của bạn. Bạn viết dài, không sao đối với một bản CV trực tuyến, nhưng sẽ rất khó để nhà tuyển dụng có thể nhận ra các kỹ năng của bạn nếu không có các từ khoá.<br />- Nêu những thành quả của bạn. Nếu một ứng cử viên sử dụng những con số, hoặc một danh sách các thành quả thì sẽ giúp nhà tuyển dụng có thể dễ dàng hơn trong việc đánh giá bạn.<br /><br />Viết đơn giản, ngắn gọn, xúc tích<br />Bỏ tất cả các ký hiệu, các câu chữ trừu tượng trước khi gửi CV cho nhà tuyển dụng và nhớ đừng quên chỉnh sửa nó. Hãy kiểm tra các lỗi về chính tả và ngữ pháp. Tốt hơn, bạn nên nhờ một người khác xem giúp bạn để được kỹ càng hơn trước khi gửi đến nhà tuyển dụng.<br /><br />Lưu ý khi gửi<br />Khi viết CV trực tuyến, để có thể nổi bật trước hàng trăm đối thủ, bạn cần phải tạo được ấn tượng mạnh mẽ cho CV của mình. Với một CV ngắn gọn, sáng sủa cùng với một chút nghệ thuật marketing tới nhà tuyển dụng, chắc chắn bạn sẽ thành công.</p>', 1, 5, 0, 40, '2010-10-18 11:31:28', 62, '', '2010-12-07 04:49:28', 62, 0, '0000-00-00 00:00:00', '2010-10-18 11:31:28', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 2, '', '', 0, 180, 'robots=\nauthor='),
(52, 'Quy định bảo mật', 'quy-nh-bo-mt', '', '<p>Việc làm Việt là công ty chuyên về tư vấn nguồn nhân lực đánh giá được tầm quan trọng khi lập ra website tuyển dụng. Những thông tin về ứng viên và nhà tuyển dụng được lưu trữ trên website của chúng tôi luôn luôn được bảo mật một cách tuyệt đối .Và dưới đây là những nội dung được đặt ra nhằm quản lý thông tin ứng viên và nhà tuyển dụng một cách chuyên nghiệp nhất.<br /><br />Thông tin của ứng viên tìm việc được xem là những thông tin liên quan đến một cá nhân sử dụng bao gồm: Họ tên, ngày, tháng, năm sinh, địa chỉ Email, nơi làm việc, các yếu tố khác của ứng viên. Thông tin của từng ứng viên có những nét đặc trưng riêng cho nên không thể nhầm lẫn thông tin với bất kỳ một cá nhân nào khác.<br /><br />Đăng nhập và sử dụng thông tin ứng viên và nhà tuyển dụng<br /><br />- Thông tin cá nhân được sử dụng vào mục đích tìm việc và đáp ứng nhu cầu tìm kiếm ứng viên của nhà tuyển dụng. Cho nên không được sử dụng thông tin của ứng viên vào những mục đích mà không liên quan đến phạm vi họat động của trang web.<br /><br />- Cung cấp dịch vụ tìm kiếm việc làm đến ứng viên tìm việc hoặc tìm kiếm ứng viên sáng giá của nhà tuyển dụng trên trang web www.vieclamviet.vn. Chủ quản trực tiếp website tuyển dụng trên phải tôn trọng quyền riêng tư khi cung cấp dịch vụ cho các thành viên ( ứng viên &amp; nhà tuyển dụng ) của trang web.<br /><br />- Được sự đồng ý của ứng viên hoặc nhà tuyển dụng đăng ký thành viên trên trang web , các thông tin cá nhân của ứng viên ., hoặc của nhà tuyển dụng bắt đầu được hiển thị đến các nhà tuyển dụng hoặc ngược lại, là thành viên của  www.vieclamviet.vn<br /><br />- Khi viết về các chuyên đề, kinh nghiệm nghề nghiệp ban quản trị trang web sẽ liên lạc với ứng viên hoặc nhà tuyển dụng.<br /><br />- Các kết quả liên quan đến trường hợp ứng viên hoặc nhà tuyển dụng không cung cấp đầy đủ thông tin theo yêu cầu của trang web thì ứng viên và nhà tuyển dụng sẽ không nhận được sự hổ trợ tốt nhất từ ban quản tri trang web.</p>', '', 1, 4, 0, 25, '2010-11-26 09:25:04', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2010-11-26 09:25:04', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 2, '', '', 0, 185, 'robots=\nauthor=');
INSERT INTO `jos_content` (`id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`) VALUES
(53, 'Thỏa thuận sử dụng', 'tha-thun-s-dng', '', '<p><strong>Giới thiệu :</strong><br /> Đây là trang về quy định và điều kiện sử dụng của Website này, nó mang tính kiểm sóat các hoạt động của người dùng website. Nếu bạn không đồng ý với các điều nêu ra của bản “Thỏa thuận sử dụng” này thì bạn không nên tiếp tục sử dụng Website này. Nếu bạn là một nhà tuyển dụng thì bạn cần phải xem phần “Thoả thuận về dịch vụ” - kết hợp với tất cả các điều khoản được nêu dưới đây. <br /><br /><strong>Quy định ràng buộc:</strong><br />Các quy định và điều kiện đưa ra ở đây có thể được chỉnh sửa tuỳ ý bởi Việc làm Việt, khi bạn truy cập và sử dụng website tức là bạn đã đồng ý với những nội dung mà chúng tôi đưa ra, và do đó chúng tôi không chịu trách nhiệm với những rủi ro nếu có do sử dụng website trái với những qui định này.<br /><br /><strong>Định nghĩa:</strong><br /><br />“Nội dung” bao gồm một chuỗi ký tự, hình họa, thiết kế và mã lập trình được sử dụng trong website này.<br /><br />“Chuỗi ký tự” bao gồm tất cả các chữ ở mỗi trang của website này.<br /><br />“Hình hoạ” bao gồm logos, nút bấm, và các thành phần đồ hoạ khác trên Website, không bao gồm banner quảng cáo.<br /><br />“Thiết kế” là những sự kết hợp màu sắc và bố cục của Website.<br /><br />“Mã lập trình” bao gồm cả mã phía người dùng (HTML, JavaScript,…) lẫn mã phía server (PHP, databases…) được dùng trên trang này.<br /><br />“Tài liệu” là tất cả những bài đăng trên Website.<br /><br />“Dịch vụ” là những tiện ích được Website này hoặc các đại diện của nó cung cấp.<br /><br />“Người dùng” là những người sử dụng một đặc điểm nào đó của Website.<br /><br />“Người tìm việc” là người dùng truy cập vào website để tìm việc.<br /><br />“Nhà tuyển dụng” là người hoặc tổ chức truy cập vào Website này đăng tuyển hoặc tìm kiếm ứng viên cho tổ chức mình.<br /><br />“Bạn” hoặc “bạn” là người hoặc tổ chức đang đồng ý với các quy định này.<br /><br /><strong>Thoả thuận sử dụng Website:</strong><br /><br /><strong>Quy định chung.</strong><br /><br />Website này dành cho những cá nhân tìm kiếm công việc và cho những nhà tuyển dụng tìm kiếm ứng viên. Bạn có thể dùng Website này nếu bạn đồng ý với những quy định của Website.Việc làm Việt là bên duy nhất đặt ra các quy định sử dụng đối với người dùng.<br /><br /><strong>Cấp quyền sử dụng cho đối tượng là “Người tìm việc”:</strong><br /><br />Vieclamviet.vn cấp quyền cho bạn sử dụng website này, đó là một quyền có giới hạn, có thể bị thu hồi, không độc quyền, với quyền này bạn có thể truy cập và sử dụng Website cho riêng mình với mục đích tìm kiếm cơ hội việc làm cho bản thân. Quyền này cho phép bạn xem, tải về một bản sao cấu trúc trang của Website, không dùng với mục đích kinh doanh. Bạn phải cam kết chịu trách nhiệm về nội dung bạn đăng tải.<br /><br /><strong>Cấp quyền cho đối tượng là "Nhà tuyển dụng":</strong><br /><br />Vieclamviet.vn cấp quyền cho bạn sử dụng website này, đó là một quyền có giới hạn, có thể bị thu hồi, không độc quyền, với quyền này bạn có thể truy cập và sử dụng Website phục vụ cho mục đích tuyển dụng nhân lực cho tổ chức của mình. Quyền này cho phép bạn duyệt, tìm kiếm ứng viên, đăng tải thông tin và sử dụng form biểu của Website. Bạn không có quyền chuyển nội dung thông tin mà bạn lấy được từ Website này nếu không được sự chấp thuận từ Việc làm Việt. Bạn phải cam kết chịu trách nhiệm đối với những nội dung đăng tải lên Website này.Việc làm Việt có quyền lấy lại các quyền của bạn trên Website này nếu bạn vi phạm các điều khoản quy định tại đây.<br /><br /><strong>Các điều luật đặc biệt<br /></strong><br />Bạn được nhắc nhở là có trách nhiệm không được dùng (hoặc lên kế hoạch hoặc khuyến khích người khác) sử dụng website vào bất cứ mục đích nào bị cấm trong qui định này và với những điều luật do nhà nước ban hành.<br /><br />Luật lệ riêng về đăng tải, quản lý và an ninh.<br /><br />Bạn đồng ý chấp hành những luật lệ riêng quy định tại phần quy định về an ninh.<br /><br /><strong>Phạm vi trách nhiệm:</strong><br /><br />Việc làm Việt không chịu trách nhiệm với những nội dung mà người dùng đăng tải, cũng như không chịu trách nhiệm những hành vi của người dùng. Website này hoạt động như là một nơi hội họp trên mạng, phân phối những thông tin mà người dùng đăng tải, không chịu trách nhiệm theo dõi tính chính xác, hợp lệ của những thông tin được người dùng đưa lên, lưu trữ. Việc làm Việt có quyền xoá bỏ tất cả những nội dung hoặc tài liệu nào mà người dùng đưa lên mà không cần báo trước.<br /><br /><strong>Xác thực thông tin.<br /></strong><br />Các thông tin mà người dùng đưa lên không được xác thực bởiViệc làm Việt. Việc làm Việt không có trách nhiệm trả lời về các thông tin như dịch vụ, tuyển dụng, kinh nghiệm làm việc, …<br /><br /><strong>Không bảo đảm kết quả.</strong><br /><br />Việc làm Việt không phải là một đại diện tuyển dụng hoặc là một công ty tuyển dụng. Do đó những phàn nàn về chất lượng tuyển dụng, chất lượng ứng viên hoặc nhà tuyển dụng không thuộc trách nhiệm trả lời của Việc làm Việt.<br /><br /><strong>Giới hạn bảo đảm</strong><br /><br />WEBSITE NÀY ĐƯỢC CUNG CẤP CÁC NỘI DUNG NHƯ NÓ VỐN CÓ, KHÔNG CÓ BẤT KÌ BẢO ĐẢM NÀO VỀ TÍNH CHÍNH XÁC, ĐỘ TIN CẬY, SỰ HOÀN CHỈNH, TÍNH KỊP THỜI..<br /><br />Việc làm Việt KHÔNG BẢO ĐẢM WEBSITE VẬN HÀNH MÀ KHÔNG CÓ SỰ CỐ<br /><br />WEBSITE KHÔNG BẢO ĐẢM VỀ TÍNH TUYỆT ĐỐI AN TOÀN TRƯỚC NHỮNG SỰ CỐ VỀ MẠNG VÀ MÁY TÍNH NHƯ VIRUS HOẶC PHÁ HOẠI DẪN ĐẾN VIỆC MẤT MÁT DỮ LIỆU, HƯ HỎNG PHẦN CỨNG.<br /><br /><strong>Vấn đề chi trả dịch vụ:</strong><br /><br />Được quy định cụ thể giữa Việc làm Việt và quí khách.<br /><br /><strong>Liên kết đến trang khác.</strong><br /><br />Việc làm Việt có chứa những liên kết đến những trang màViệc làm Việt thấy cần thiết cho công việc của bạn, tuy nhiên, chúng tôi không đảm bảo nội dung của những trang đó.<br /><br /><strong>Bổ sung, thay đổi nội dung những quy định này.</strong><br /><br />Việc làm Việt có thể thay đổi nội dung, cập nhật lại nội dung của trang này. Điều đó có nghĩa là bạn phải thường xuyên xem lại những quy định này và cân nhắc việc sử dụng website nếu thấy không phù hợp.<br /><br /><strong>Thông tin cá nhân.</strong><br /><br />Bạn phải chịu trách nhiệm những thông tin mà bạn cung cấp trước những đối tác mà bạn làm việc.<br /><br />Bạn phải đảm bảo tính chính xác, giữ bí mật những nội dung không muốn phô bày.<br /><br />Chúng tôi có thể sử dụng email của bạn để gởi đến những thông tin cần thiết cho công việc của bạn đến hộp mail của bạn, không nhằm mục đích thương mại.<br /><br />Trong trường hợp xấu nhất, chúng tôi có thể sử dụng thông tin của bạn như là thông tin để điều tra. Chúng tôi có thể tiết lộ thông tin của bạn cho các tổ chức thực thi pháp luật nếu nằm trong khuôn khổ luật pháp.<br /><br /><strong>Hỏi đáp, lưu ý.</strong><br /><br />Chúng tôi hoan nghênh sự đóng góp, thông báo sự cố.. mọi sự cố ý lợi dụng sai sót của Website đều bị truy tố trước pháp luật.<br /><br /><strong>Tổng quát.</strong><br /><br />Mọi hành động của bạn trên Website phải phù hợp với quy định của luật pháp của nhà nước XHCN Việt nam, những hành vi mang tính chính trị, vi phạm các qui định và luật lệ của pháp luật hiện hành đều bị loại trừ. Chúng tôi có thể truy tố bạn nếu bạn vi phạm luật pháp, văn hoá, thuần phong mỹ tục của nước CHXHCN Việt nam.<br /><br />Nếu bạn có hành động mang tính kích động, gây chia rẽ, hận thù, bôi nhọ, hạ nhục, phá hoại đều không được chấp nhận. Chúng tôi có thể cung cấp cho cơ quan có thẩm quyền những thông tin của bạn.<br /><br /><strong>Phần qui định về an ninh</strong><br /><br />a) Người dùng bị cấm trước mọi hành vi vi phạm hoặc xâm phạm an ninh của site, bao gồm (i) truy cập dữ liệu không dùng cho mình, đăng nhập vào server mà không được cấp quyền truy nhập ; (ii) cố ý scan, thử tính năng, dò lỗ hổng của hệ thống hoặc mạng, đột nhập qua hàng rào an ninh hoặc các nguyên tắc an toàn mà không được cấp phép ; (iii) cố can thiệp vào dịch vụ hoặc người dùng, máy chủ hoặc mạng, phát tán virus, làm nghẽn mạng, dội bom mail, gây sự cố ; (iv) gởi những thông tin quảng cáo; (v) bắt gói tin TCP/IP hoặc lấy các thông tin trong mail của site. Vi phạm về an ninh hệ thống hoặc mạng có thể dẫn đến truy cứu trách nhiệm hình sự hoặc dân sự.<br /><br />b) Vi phạm các điều luật an ninh như trên có thể dẫn đến truy cứu trách nhiệm hình sự hoặc dân sự. Việc làm Việt có thể phối hợp, giúp đỡ cơ quan nhà nước, nhân viên thực thi pháp luật, cung cấp thông tin đẫn đến việc tìm ra và xử lý những người liên quan đến những vi phạm như trên.</p>', '', 1, 4, 0, 25, '2010-11-26 09:27:33', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2010-11-26 09:27:33', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 1, '', '', 0, 182, 'robots=\nauthor='),
(54, 'Làm gì để hồ sơ của bạn được chú ý? ', 'lam-gi--h-s-ca-bn-c-chu-y-', '', '<p><img src="images/stories/cv.gif" border="0" width="152" style="float: left; margin: 5px;" />Có nhiều lý do khách quan và chủ quan khiến hồ sơ của bạn không được nhà tuyển dụng chú ý và chọn lựa. Bạn cần phải viết một bản lý lịch rõ\r\n', '\r\nràng, súc tích, gây ấn tượng mạnh nơi người đọc là một trong những yếu tố quan trọng nhất giúp ứng viên lọt vào tầm ngắm của nhiều nhà tuyển dụng.<br />Không một nhà tuyển dụng nào đọc hồ sơ của bạn kỹ càng trong lần đầu tiên, vì thế bạn phải viết resume làm sao để thu hút sự chú ý của họ ngay từ lần đầu nhìn thấy hồ sơ của bạn. Để làm được điều đó, ngoài việc bạn tham khảo các tài liệu hướng dẫn viết resume, những mẫu cv hay cùng với việc nhờ người đáng tin cậy hướng dẫn, sửa lỗi chính tả. Bạn nên chú ý những điều sau:<br /><br /> 1. Hãy dùng những từ ngữ cô đọng, ấn tượng và thật chính xác để giới thiệu trước nhà tuyển dụng những thành tích xuất sắc của bạn trong quá trình làm việc.<br /> 2. Trình bày những kỹ năng theo thứ tự thời gian, bắt đầu là những công việc và kinh nghiệm, kỹ năng, thành tích trong những năm gần đây nhất rồi theo thứ tự thời gian trở về trước. Nếu cần thiết có thể sử dụng những gam màu sắc thích hợp tô đậm từng mục cụ thể trong bảng resume để tạo ấn tương.<br /> 3. Sử dụng những động từ có tác động mạnh và mang tính chất miêu tả. Đồng thời dùng những gạch đầu dòng để mô tả những thành tích và công việc của bạn<br /> 4. Bạn nên biến đổi hồ sơ xin việc theo từng vị trí mà bạn đang muốn dự tuyển. Tập trung nêu rõ những công việc và hoạt động có liên quan và lượt bớt những hoạt động không liên quan đến vị trí mà bạn cần dự tuyển. Ví dụ bạn từng là một người phụ trách nhân sự trong thời gian 2 năm, sau đó làm công việc văn thư - lưu trữ. Bây giờ bạn muốn nộp đơn cho vị trí trưởng phòng nhân sự bạn phải nêu nổi bật những kỹ năng chuyên môn, những kinh nghiệm trong công tác nhân sự và chỉ nêu lướt qua công việc ở vị trí văn thư trong hồ sơ.<br /> 5. Đừng nên nêu những lý do mà bạn muốn thay đổi công việc trong bảng resume. Điều đó khiến nhà tuyển dụng có ấn tượng không tốt về bạn vì cho rằng bạn là người không muốn gắn bó với công ty. Tâm lý nhà tuyển dụng luôn mong muốn nhân viên mới sẽ làm việc lâu dài với công ty và ổn định.</p>', 1, 5, 0, 40, '2010-10-18 08:51:05', 62, '', '2010-12-07 04:47:36', 62, 0, '0000-00-00 00:00:00', '2010-10-18 08:51:05', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 1, '', '', 0, 57, 'robots=\nauthor='),
(55, 'Làm thế nào để biết mức lương nhà tuyển dụng có thể trả cho bạn?', 'lam-th-nao-bit-mc-lng-nha-tuyn-dng-co-th-tr-cho-bn', '', '<p><img src="images/stories/untitled-1 copy.jpg" border="0" width="152" style="float: left; margin: 5px;" />Bạn trả lời như thế nào khi nhà tuyển dụng (NTD) hỏi :"Mức lương mong muốn của bạn là bao nhiêu?" Thật là khó phải không? \r\n', '\r\nĐưa ra mức lương quá cao thì có thể sẽ không được nhận vào làm, còn nếu đề nghị mức thấp thì bạn cảm thấy không xứng tầm với khả năng của mình. Sẽ có nhà tuyển dụng hiểu bạn không muốn đưa ra một câu trả lời trực tiếp. Tuy nhiên vẫn có nhà tuyển dụng cần bạn tiết lộ con số bạn mong muốn.<br /><br /> Dưới đây là ba cách hiệu quả để đưa ra câu trả lời cho câu hỏi trên.<br /><br />Hỏi thêm trước khi trả lời<br /><br />Nguyên tắc đầu tiên là bạn không nên trả lời ngay câu hỏi trên. Hãy nhớ rằng khi NTD hỏi mức lương mong muốn, nhiều khả năng bạn đã là "người-được-chọn". Hãy tìm hiểu thêm chi tiết về công việc (nếu bạn chưa có nhiều cơ hội để tìm hiểu trong quá trình trao đổi) để có thể đề nghị một mức lương phù hợp.<br /><br />-      Cơ hội để bạn học hỏi, phát triển và thăng tiến?<br /><br />-      Trách nhiệm công việc (kể cả khoản ngân sách bạn sẽ phải quản lý hoặc doanh số bạn phải chịu)?<br /><br />-      Số lượng nhân viên bạn sẽ quản lý (nếu có)?<br /><br />-      Những chương trình phúc lợi cho nhân viên?<br /><br />Những câu hỏi về chi tiết công việc thể hiện sự nghiêm túc và quan tâm của bạn đối với cơ hội được làm việc với công ty. Điều đó sẽ ghi thêm điểm cho bạn. Nhà tuyển dụng luôn đánh giá cao những ai thật sự quan tâm đến công việc, họ sẽ chia sẻ thêm những thông tin hữu ích giúp bạn có thể đưa ra một mức lương phù hợp. Lý tưởng hơn, bạn có thể khéo léo chen vào câu hỏi về mức lương dành cho vị trí này trong quá trình trao đổi.<br /><br />Hãy để nhà tuyển dụng thay bạn trả lời<br /><br />Bạn cũng có thể áp dụng thuật "đi vòng", nghĩa là chuyển buổi nói chuyện theo một hướng khác rồi khéo léo quay trở lại vấn đề NTD đang hỏi. Nếu bạn thật sự chưa thể nghĩ ra một con số cụ thể, hãy biến câu trả lời thành một cơ hội để giới thiệu thêm về định hướng của bạn cho nhà tuyển dung. Một ví dụ cho câu trả lời của bạn:"Qua trao đổi với anh/chị, tôi thật sự rất thích môi trường làm việc của công ty cũng như những thử thách của công việc này. Tôi mong muốn trở thành một thành viên và đóng góp vào sự phát triển của công ty. Đối với tôi, cơ hội học hỏi và phát triển để trở thành một kế toán trưởng trong vòng ba năm tới cũng như môi trường làm việc thân thiện, cởi mở là điều tôi quan tâm nhất hiện tại. Và thật sự tôi nghĩ đây chính là một cơ hội dành cho mình. Nếu có thể, anh/chị vui lòng cho tôi biết mức lương dành cho vị trí này?".<br /><br />Với cách trả lời này, bạn tạo được một không khí thân thiện với NTD, và nhất là tránh được câu trả lời trực tiếp.<br /><br />Hãy chuẩn bị những câu trả lời như vậy, nhưng phải dựa vào thông tin bạn có được trong suốt quá trình trao đổi với nhà tuyển dụng. Tuyệt đối tránh sử dụng một "câu trả lời mẫu" cho tất cả nhà tuyển dụng khác nhau. Họ sẽ biết ngay là bạn đã học thuộc lòng. Và bạn sẽ bị mất điểm.<br /><br />Quan trọng hơn, "biết người biết ta"<br /><br />Bạn nên tìm hiểu về thị trường lương thực tế trước khi đi phỏng vấn. Có thể tìm hiểu từ bạn bè, người thân, từ network của mình để biết thêm khoảng lương cho vị trí bạn đang ứng tuyển. Tuy nhiên, hãy thực tế! Cùng một vị trí, chẳng hạn kế toán trưởng, nhưng công ty nước ngoài sẽ trả khác công ty trong nước, ngành hàng tiêu dùng nhanh sẽ khác ngành dịch vụ, công ty tại Tp. Hồ Chí Minh hay Hà Nội sẽ có mức lương khác với các công ty tại khu công nghiệp.<br /><br />Những thông tin này sẽ giúp bạn tư tin hơn, cảm thấy thoải mái hơn trước khi bước vào buổi phỏng vấn. Tôn Tử đã từng nói "biết người biết ta, trăm trận trăm thắng."<br /><br />Khi đươc hỏi "bạn mong muốn mức lương bao nhiêu?", bạn đừng đưa ra một mức cụ thể - "Tôi nghĩ 10 triệu đồng". Bạn nên đưa ra một khoảng. Nếu bạn nghĩ mình xứng đáng với 10 triệu, bạn nên trả lời "Với những chi tiết công việc như anh/chị đã trao đổi với em, em nghĩ mức lương từ 9 triệu đến 11 triệu là hợp lý."<br /><br />Nhiều người sau khi nhận được lời mời đi làm vẫn cảm thấy không vui, họ ước gì họ đã thương lượng thêm về phần lương và phúc lợi. "Ước gì tôi đã nói thêm điều này, điều kia thì chắc chắn lương của tôi có thể cao hơn" là những lời "than vãn" của những người vội vàng thỏa thuận mức lương.<br /><br />Còn bạn thì sao? Đã bao giờ bạn rơi vào trường hợp đó chưa? Đừng bao giờ để mình rơi vào tình huống đó. Còn một khi bạn đã không chuẩn bị kỹ khi đàm phán về lương, hãy coi đó là một kinh nghiệm đáng giá, và quên điều đó đi. Thay vì than vãn, tỏ vẻ tiếc nuối, bạn nên tập trung vào công việc hiện tại, mở rộng phạm vi trách nhiệm của mình, thể hiện thái độ làm việc tích cực, và đến kỳ đánh giá hiệu quả công việc và điều chỉnh lương tiếp theo, bạn sẽ được đền đáp xứng đáng.</p>\r\n<p> </p>', 1, 5, 0, 39, '2010-12-07 04:38:37', 62, '', '2010-12-07 04:41:12', 62, 0, '0000-00-00 00:00:00', '2010-12-07 04:38:37', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 3, '', '', 0, 57, 'robots=\nauthor='),
(56, '8 bí quyết của người thương lượng lương cừ khôi', '8-bi-quyt-ca-ngi-thng-lng-lng-c-khoi', '', '<p><img src="images/stories/ed2010salary_1.thumbnail.jpg" border="0" width="152" style="float: left; margin: 5px;" />Thương lượng lương rất cần sự khéo léo và một chút “kỹ xảo”. Martin Yate, tác giả của quyển sách bán rất chạy “Knock ‘em Dead” nói:\r\n', '\r\n“Khi đến giai đoạn thương lượng lương, nhiều ứng viên lại không chuẩn bị trước”. Vì vậy để đạt được mức lương mơ ước, hãy chắc chắn là bạn đã hiểu rõ nghệ thuật thương lượng để chuẩn bị thật tốt cho giai đoạn quan trọng này.<br /><br />1. Tìm hiểu, tìm hiểu và tìm hiểu<br />Tìm hiểu mức lương. Rất nhiều ứng viên đã trúng tuyển nhưng không biết mức lương như vậy có thích đáng hay không. Theo Yate, biết được mức lương thực tế cho vị trí bạn đang ứng tuyển đóng vai trò rất quan trọng. Bạn có thể biết được điều này bằng cách tham khảo thông tin từ thị trường lao động, từ những người có công việc giống bạn, hay từ những người quen làm việc trong công ty bạn ứng tuyển. Một cách dễ hơn là bạn có thể tìm hiểu mức lương trung bình dành cho vị trí ứng tuyển thông qua các thông báo tuyển dụng của một số công ty đăng trên các website việc làm. Tuy nhiên bạn cũng cần phải trung thực và khách quan trong việc đánh giá khả năng thật sự của mình để đề nghị với NTD mức lương phù hợp với bạn nhất.<br /><br />2. Kế hoạch “3 con số”<br />Để trở thành người thương lượng lương cừ khôi, bạn hãy nhớ nguyên tắc vàng là chuẩn bị thật kỹ kế hoạch cho buổi thảo luận. Yate khuyên: “Trước bất kì buổi phỏng vấn nào, bạn cũng cần lên kế hoạch ‘3 con số’. Con số đầu tiên thể hiện mức lương thấp nhất, là con số có thể đáp ứng nhu cầu tối thiểu cho cuộc sống của bạn, ví dụ như thức ăn và nhà ở. Con số thứ hai là mức lương hợp lý bạn có thể kiếm được dựa trên kinh nghiệm và trình độ của bạn. Con số thứ ba là mức lương ‘trong mơ’, vượt xa mức lương mong đợi của bạn. Hãy ‘quên’ con số thứ nhất đi, vì điều đó rất riêng tư và không nên đem ra thảo luận. Lấy con số thứ hai và thứ ba làm cơ sở để thảo luận với NTD về mức lương mơ ước của bạn.”<br /><br />3. Đừng là người đầu tiên nói về chuyện lương bổng<br />Yate khuyên “Nếu bạn là người đầu tiên trong buổi phỏng vấn nói về lương bổng trước thì buổi phỏng vấn sẽ rất khó diễn ra theo ý bạn muốn. Nếu NTD không ‘đả động’ gì về lương bổng, bạn có thể ngầm hiểu là bạn chưa thuyết phục họ được rằng bạn là ứng viên lý tưởng. Nghệ thuật ở đây là làm sao để NTD nhận ra rằng bạn là ứng viên sáng giá nhất. Hãy cho NTD thấy bạn có thể giúp họ tiết kiệm tiền bạc hoặc tạo ra lợi nhuận như thế nào, và bạn sẽ cống hiến hết mình cho công ty ra sao...”<br /><br />4. Thà đừng nêu ra câu hỏi nào còn hơn là…<br />Đừng bao giờ kết thúc buổi phỏng vấn bằng một câu hỏi về lương bổng. Hầu hết các NTD đều kết thúc buổi phỏng vấn của họ bằng cách hỏi bạn còn câu hỏi nào nữa không. “Điều tệ nhất bạn có thể làm là đưa ra một câu hỏi về lương. Điều đó cho thấy bạn không còn gì để nói về bản thân hoặc trình độ chuyên môn của mình nữa,” Yate cảnh báo.<br /><br />5. Đơn giản là sự thật<br />Trong bài viết trước, bạn biết rằng nên trì hoãn việc thảo luận lương bổng cho đến khi bạn biết chắc 90% là mình được tuyển. Nếu NTD gặng hỏi bạn mong muốn mức lương bao nhiêu, bạn đừng nêu ra con số ngay lập tức và cũng đừng phóng đại. Yate khuyên ”Ứng viên nên nói rằng họ cần hiểu thêm về công việc trước khi thảo luận về vấn đề lương bổng. Hãy nêu ra vài câu hỏi với NTD để hiểu rõ hơn về yêu cầu công việc.”<br /><br />6. Làm NTD “toát mồ hôi”<br />Nên “quảng bá” cơ hội đầu quân của bạn với công ty khác là lời khuyên của Yate “Nếu bạn nhận được lời mời đi làm từ công ty khác, hãy biết cách khéo léo sử dụng điều này.” Đó chính là vũ khí để bạn ”cân não” NTD. Tuy nhiên, chỉ thực hiện kế hoạch này nếu bạn cảm thấy NTD thực sự cần bạn. Hãy nói với NTD là bạn thật sự muốn đầu quân cho họ, nhưng vừa mới nhận được lời mời đi làm từ công ty X. Đó là cách “thúc” NTD quyết định tuyển bạn ngay để không bỏ mất nhân tài.<br /><br />7. Biết “kiềm chế”<br />Đừng đồng ý ngay với mức lương NTD đưa ra đầu tiên. Các công ty thường định sẵn ngân sách tuyển dụng cho vị trí họ cần tuyển, đi từ mức thấp nhất đến mức cao nhất. Vì vậy, trừ phi mức lương NTD đưa ra vượt xa mức lương mơ ước của bạn, hãy tìm cách thương lượng thêm nếu có thể.<br /><br />8. Đừng vội từ chối cơ hội chỉ vì mức lương<br />Bạn làm gì nếu bạn thật sự yêu thích công việc nhưng mức lương NTD đưa ra quá thấp? Yate khuyên: “Đừng từ chối vội. Hãy yêu cầu NTD cho bạn vài ngày để suy nghĩ. Đó là cách bạn ‘xi-nhan’ để NTD biết là bạn rất quan tâm đến vị trí đó. Sau đó bạn hãy gọi điện cho NTD để xem họ có thể nâng mức lương hay không trước khi bạn đưa ra câu trả lời cuối cùng.”</p>', 1, 5, 0, 39, '2010-12-07 04:41:32', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2010-12-07 04:41:32', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 2, '', '', 0, 64, 'robots=\nauthor='),
(57, 'Dịch vụ cung ứng nhân sự', 'dch-v-cung-ng-nhan-s', '', '<p><img src="images/stories/hr-jobs-4.png" border="0" width="152" style="float: left; margin: 5px;" />Việc làm Việt chuyên cung cấp cho các công ty các giải pháp tuyển dụng để huy động  nguồn nhân lực có trình độ cho các hợp đồng ngắn và dài hạn.</p>\r\n', '\r\n<p>Dịch vụ của  chúng tôi giúp quý vị tiết kiệm thời gian tìm kiếm các nhân viên làm  việc chuyên môn khi nhân viên chính thức nghỉ thai sản, nghỉ đi học hoặc  tuyển dụng cho các dự án ngắn hạn, etc.<br /><br />Chúng tôi cung cấp cho  quý vị dịch vụ quản lý tiền lương trọn gói cùng với tuyển dụng/thay thế  nhân viên và chịu trách nhiệm pháp lý khi ký hợp đồng lao động với nhân  viên</p>\r\n<p> </p>', 1, 5, 0, 41, '2010-12-07 05:23:59', 62, '', '2010-12-11 23:52:01', 62, 0, '0000-00-00 00:00:00', '2010-12-07 05:23:59', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 3, '', '', 0, 332, 'robots=\nauthor='),
(58, 'Cung ứng và cho thuê lao động', 'cung-ng-va-cho-thue-lao-ng', '', '<p><img src="images/stories/picture4.jpg" border="0" width="185" height="166" style="margin: 5px; float: left;" />CÔNG TY TNHH NHÂN LỰC  VIỆT BẮC<br />Trụ sở: Tân Hưng - Sóc Sơn - Hà Nội<br />Tel: .......... . Email: contact@vieclamviet.vn<br /><br />THƯ MỜI HỢP TÁC<br /><br />Kính gửi: QUÝ CÔNG TY<br />Lời đầu tiên, Ban điều hành CÔNG TY TNHH NHÂN LỰC  VIỆT BẮC xin gửi tới Quý công ty lời chào trân trọng, lời chúc sức khỏe và thành công!<br /><br />● CÔNG TY TNHH NHÂN LỰC  VIỆT BẮC chúng tôi rất mong muốn được trao đổi cùng Quý công ty những thông tin cỏ bản về: Dịch vụ cung cấp, Quản lý và cho thuê người lao động phổ thông và lao động kỹ thuật các ngành nghề chuyên nghiệp, hợp đồng dài hạn ngắn hạn (thời vụ) tùy theo yêu cầu của Quý công ty.<br />* Chúng tôi có thể cung ứng tuyển chọn nhân lực giúp Quý công ty tự quản lý hoặc chúng tôi quản lý và chịu trách nghiệm 100% (hay Chúng tôi là phòng nhân sự của Quý công ty). Chúng tôi sẽ chịu trách nghiệm mọi thủ tục như: hợp đồng lao động, đóng BHYT, BHXH, trang phục, tuyển chọn và đào tạo,.... đối với công nhân, nhân viên.<br /><br />KHI QUÝ CÔNG TY HỢP TÁC VỚI CHÚNG TÔI BẠN SẼ RẤT ĐƯỢC NHIỀU LỢI ÍCH<br />Mọi chi tiết xin liên hệ:</p>', '', 1, 5, 0, 41, '2010-12-07 05:31:30', 62, '', '2010-12-18 08:23:14', 62, 0, '0000-00-00 00:00:00', '2010-12-07 05:31:30', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 2, '', '', 0, 123, 'robots=\nauthor='),
(59, 'Dịch vụ cung cấp Osin', 'dch-v-cung-cp-osin', '', '<p><img src="images/stories/osin19112009.jpg" border="0" width="250" height="244" style="margin: 5px; float: left;" />DỊCH VỤ GIA ĐÌNH, CUNG CẤP NGƯỜI GIÚP VIỆC UY TÍN<br />Website: <br />ĐT: <br />Công ty TNHH nhân lực Việt Bắc chuyên cung cấp lao động giúp việc cho các hộ gia đình với chất lượng cao và quy trình tuyển dụng đảm bảo.<br />1.Nguồn lao động được tuyển lựa kỹ lưỡng, nhân viên của chúng tôi sẽ về tận địa phương để điều tra thân nhân, lý lịch của từng lao động với sự hỗ trợ của chính quyền địa phương và phỏng vấn trực tiếp.<br /><br />2. Lao động sau khi được tuyển lựa qua vòng 1 sẽ được đào tạo bài bản đến chuyên sâu để trở thành người giúp việc chuyên nghiệp. Với các kỹ năng cơ bản: Kỹ năng giúp việc nhà, sử dụng trang thiết bị trong gia đình, đi chợ, nấu ăn, giặt giũ, kỹ năng chăm sóc trẻ như một bảo mẫu, kỹ năng chăm sóc người già, kỹ năng tâm lý gia đình, giao tiếp ứng xử, kiến thức pháp luật và các kỹ năng sơ, cấp cứu cần thiết khi xảy ra sự cố và đề phòng sự cố.<br /><br />3. Sau khoá đào tạo, lao động chúng tôi phân làm các cấp độ khác nhau tuỳ theo mức lương.<br /><br /><br />Mọi thông tin xin liên hệ:<br /><br />ĐT: <br /><br />Website: <br /><br />Email:</p>', '', 1, 5, 0, 41, '2010-12-07 05:44:56', 62, '', '2010-12-11 23:50:23', 62, 0, '0000-00-00 00:00:00', '2010-12-07 05:44:56', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 3, 0, 1, '', '', 0, 139, 'robots=\nauthor='),
(60, 'Giá và dịch vụ', 'gia-va-dch-v', '', '<!--[if gte mso 9]><xml> <w:WordDocument> <w:View>Normal</w:View> <w:Zoom>0</w:Zoom> <w:PunctuationKerning /> <w:ValidateAgainstSchemas /> <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid> <w:IgnoreMixedContent>false</w:IgnoreMixedContent> <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText> <w:Compatibility> <w:BreakWrappedTables /> <w:SnapToGridInCell /> <w:WrapTextWithPunct /> <w:UseAsianBreakRules /> <w:DontGrowAutofit /> </w:Compatibility> <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel> </w:WordDocument> </xml><![endif]--><!--[if gte mso 9]><xml> <w:LatentStyles DefLockedState="false" LatentStyleCount="156"> </w:LatentStyles> </xml><![endif]--><!--[if gte mso 10]> <mce:style><!   /* Style Definitions */  table.MsoNormalTable 	{mso-style-name:"Table Normal"; 	mso-tstyle-rowband-size:0; 	mso-tstyle-colband-size:0; 	mso-style-noshow:yes; 	mso-style-parent:""; 	mso-padding-alt:0in 5.4pt 0in 5.4pt; 	mso-para-margin:0in; 	mso-para-margin-bottom:.0001pt; 	mso-pagination:widow-orphan; 	font-size:10.0pt; 	font-family:"Times New Roman"; 	mso-ansi-language:#0400; 	mso-fareast-language:#0400; 	mso-bidi-language:#0400;} --> <!--[endif]-->\r\n<h1><span style="font-size: 10pt; font-family: Arial;">Cảm ơn Quý khách hàng đã quan tâm đến website tuyển dụng trực tuyến www.vieclamviet.vn của chúng tôi. Xin được gửi tới Quý khách hàng bảng báo giá dịch vụ như sau: </span></h1>\r\n<table id="table6" class="MsoNormalTable" style="width: 100%; border-collapse: collapse; border: 3px none;" border="3" cellspacing="1" cellpadding="3">\r\n<tbody>\r\n<tr>\r\n<td style="width: 18.36%; border: 1.5pt solid windowtext; background: none repeat scroll 0% 0% #a0d2fc; padding: 1.5pt;" width="18%">\r\n<p style="text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;">Dịch Vụ</span></strong></p>\r\n</td>\r\n<td style="width: 26.54%; background: none repeat scroll 0% 0% #a0d2fc; padding: 1.5pt;" width="26%">\r\n<p style="text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;">Mô tả dịch vụ</span></strong></p>\r\n</td>\r\n<td style="width: 34.7%; background: none repeat scroll 0% 0% #a0d2fc; padding: 1.5pt;" width="34%">\r\n<p style="text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;">Lợi ích của Khách hàng</span></strong></p>\r\n</td>\r\n<td style="width: 20.4%; background: none repeat scroll 0% 0% #a0d2fc; padding: 1.5pt;" width="20%">\r\n<p style="text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;">Giá dịch vụ</span></strong></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="width: 18.36%; padding: 1.5pt;" width="18%">\r\n<p style="text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;">Đăng tuyển dụng </span></strong></p>\r\n</td>\r\n<td style="width: 26.54%; padding: 1.5pt;" width="26%">\r\n<p><span style="font-size: 10pt; font-family: Arial;">- Tin tuyển dụng sẽ được   hiển thị  trên  website vieclamviet.vn </span></p>\r\n</td>\r\n<td style="width: 34.7%; padding: 1.5pt;" width="34%">\r\n<p><span style="font-size: 10pt; font-family: Arial;">- Tin tuyển dụng được   hiển thị theo thời gian và ngành nghề Ứng viên quan tâm. <br /> - Nhận được thông tin của ứng viên một cách nhanh chóng. <br /> - Ứng viên có thể nộp đơn trực tuyến thông qua website www. vieclamviet.vn đến   nhà tuyển dung, nhà tuyển dụng có thể liên hệ trực tiếp đến ứng viên <span style="color: red;">( miễn phí )</span></span></p>\r\n</td>\r\n<td style="width: 20.4%; padding: 1.5pt;" width="20%">\r\n<p style="text-align: center;" align="center"><span style="font-family: Verdana;"> </span></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="width: 18.36%; padding: 1.5pt;" width="18%">\r\n<p style="text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;">Đăng tin tuyển dụng trong mục "Việc làm  tốt   nhất"</span></strong></p>\r\n</td>\r\n<td style="width: 26.54%; padding: 1.5pt;" width="26%">\r\n<p><span style="font-size: 10pt; font-family: Arial;">- Tin tuyển dụng sẽ được   hiển thị  trên  website vieclamviet.vn mục<span style="color: red;"> <strong>“Việc   làm tốt nhất”</strong></span></span></p>\r\n</td>\r\n<td style="width: 34.7%; padding: 1.5pt;" width="34%">\r\n<p><span style="font-size: 10pt; font-family: Arial;">- Vị trí thu hút được   nhiều người truy cập quan tâm. </span></p>\r\n<p><span style="font-size: 10pt; font-family: Arial;">- Hiển thị lên trang chủ   của website</span></p>\r\n</td>\r\n<td style="width: 20.4%; padding: 1.5pt;" width="20%">\r\n<p style="text-align: center;" align="center"><span style="font-family: Verdana;"><br /> </span><span style="font-size: 10pt; font-family: Arial; color: red;"><span> </span></span></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="width: 18.36%; padding: 1.5pt;" width="18%">\r\n<p style="text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;">Đăng tin tuyển dụng trong mục "bôi đậm đỏ"</span></strong></p>\r\n</td>\r\n<td style="width: 26.54%; padding: 1.5pt;" width="26%">\r\n<p><span style="font-size: 10pt; font-family: Arial;">Tin tuyển dụng sẽ được   bôi đỏ đậm</span></p>\r\n</td>\r\n<td style="width: 34.7%; padding: 1.5pt;" width="34%">\r\n<p><span style="font-size: 10pt; font-family: Arial;">Nổi bật tin tuyển dụng và   thu hút các ứng viên nhiều hơn</span></p>\r\n</td>\r\n<td style="width: 20.4%; padding: 1.5pt;" width="20%">\r\n<p style="text-align: center;" align="center"><span style="font-family: Verdana;"> </span></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="width: 18.36%; padding: 1.5pt;" width="18%">\r\n<p style="text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;">Đăng tin tuyển dụng trong mục "Tốp đầu"</span></strong></p>\r\n</td>\r\n<td style="width: 26.54%; padding: 1.5pt;" width="26%">\r\n<p><span style="font-size: 10pt; font-family: Arial;">- Tin tuyển dụng sẽ được   hiển thị  trên  website vieclamviet.vn xuất hiện ngay vị trí đầu   tiên của ngành nghề </span></p>\r\n</td>\r\n<td style="width: 34.7%; padding: 1.5pt;" width="34%">\r\n<p><span style="font-size: 10pt; font-family: Arial;">- Vị trí thu hút được   nhiều người truy cập quan tâm. </span></p>\r\n<p><span style="font-size: 10pt; font-family: Arial;"> </span></p>\r\n</td>\r\n<td style="width: 20.4%; padding: 1.5pt;" width="20%">\r\n<p style="text-align: center;" align="center"><span style="font-family: Verdana;"> </span></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="width: 18.36%; padding: 1.5pt;" width="18%">\r\n<p style="text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;">Tìm hồ sơ ứng viên </span></strong></p>\r\n</td>\r\n<td style="width: 26.54%; padding: 1.5pt;" width="26%">\r\n<p><span style="font-size: 10pt; font-family: Arial;">-Truy cập hồ sơ ứng viên   chất lượng trong dữ liệu của Ungvien.com </span></p>\r\n</td>\r\n<td style="width: 34.7%; padding: 1.5pt;" width="34%">\r\n<p><span style="font-size: 10pt; font-family: Arial;">- Truy cập hồ sơ ứng viên   tốt nhất trong cơ sở dữ liệu của </span><span style="text-decoration: underline;"><span style="color: #3366ff;"><a href="http:///"><span style="color: #3366ff;">http:///</span></a></span></span><span style="text-decoration: underline;"><span style="font-size: 10pt; font-family: Arial; color: #3366ff;">www.</span><span style="color: #3366ff;">vieclamviet.vn</span></span><span style="text-decoration: underline;"><span style="font-size: 10pt; font-family: Arial;"> </span></span><span style="font-size: 10pt; font-family: Arial;"><span> </span>ở tất cả các ngành   nghề tìm kiếm <br /> - Lưu lại hồ sơ Ứng viên quan tâm. <br /> - Liên hệ trực tiếp với các Ứng viên - Tìm kiếm hồ sơ ứng viên một cách nhanh   chóng nhờ công cụ hổ trợ tìm kiếm hiệu quả</span></p>\r\n</td>\r\n<td style="width: 20.4%; padding: 1.5pt;" width="20%">\r\n<p style="text-align: center;" align="center"><span style="font-family: Verdana;"> </span></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="width: 18.36%; padding: 1.5pt;" width="18%">\r\n<p style="text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Verdana;">Dịch vụ khác</span></strong></p>\r\n</td>\r\n<td style="width: 26.54%; padding: 1.5pt;" width="26%">\r\n<p class="MsoNormal"><span style="font-family: Verdana;">- </span><span style="font-size: 10pt; font-family: Arial;">Tư vấn nhân sự</span><span style="font-family: Arial;"> </span></p>\r\n<p><span style="font-size: 10pt; font-family: Arial;">- Cung ứng và quản lý   nhân sự</span><span style="font-family: Arial;"> </span></p>\r\n<p><span style="font-size: 10pt; font-family: Arial;">- Cung cấp osin cao cấp </span></p>\r\n<p><span style="font-size: 10pt; font-family: Arial;">- Cho thuê nhân sự</span></p>\r\n</td>\r\n<td style="width: 34.7%; padding: 1.5pt;" width="34%">\r\n<p class="MsoNormal"><span style="font-family: Verdana;"> </span></p>\r\n</td>\r\n<td style="width: 20.4%; padding: 1.5pt;" width="20%">\r\n<p style="text-align: center;" align="center"><span style="font-size: 10pt; font-family: Verdana; color: red;">Call</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><strong><span style="font-size: 10pt; font-family: Arial;">Lưu ý: - Mức giá trên chưa bao gồm 10% VAT </span></strong></p>\r\n<p><strong><span style="font-size: 10pt; font-family: Arial;">- Có chế độ ưu đãi đặc biệt đối với khách hàng thường xuyên</span></strong></p>', '', 1, 6, 0, 43, '2010-12-07 06:09:08', 62, '', '2010-12-07 06:24:52', 62, 0, '0000-00-00 00:00:00', '2010-12-07 06:09:08', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 4, 0, 1, '', '', 0, 74, 'robots=\nauthor=');
INSERT INTO `jos_content` (`id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`) VALUES
(61, '6 cách để xây dựng một công sở vững mạnh', '6cacg', '', '<p><img src="images/stories/baytuyendungnew.gif" border="0" width="152" style="float: left; margin: 5px;" /> - Một môi trường làm việc vững mạnh là nền tảng cơ bản để công ty thành công trong mọi hoạt động, từ cung cấp dịch vụ tốt nhất cho khách hàng đến tận dụng tối đa năng suất của nhân viên…\r\n', '\r\n</p>\r\n<p>Là một người quản lí, bạn có thể áp dụng 6 cách cơ bản dưới đây để xây dựng một công sở vững mạnh:</p>\r\n<p><em>1. Loại bỏ những căng thẳng không cần thiết cho nhân viên</em></p>\r\n<p>Để biết những căng thẳng không đáng có của nhân viên, những sếp thông minh sẽ đặt ra câu hỏi trực tiếp: “ Công ty có điều gì khiến bạn căng thẳng, mệt mỏi và làm ảnh hưởng tới hiệu quả công việc?” Có thể năng lượng của nhân viên bị phung phí vào những lo lắng về nạn quan liêu trong công ty, về những khó khăn khi thực hiện công việc, thiếu thốn về thông tin và trang bị… Hãy tìm hiểu khó khăn của nhân viên và loại bỏ những trở ngại không cần thiết. Làm được điều đó, bạn sẽ khiến nhân viên đạt tới đỉnh cao năng suất và hiệu quả công việc.</p>\r\n<p><em>2. Cung cấp thông tin rõ ràng cho nhân viên</em></p>\r\n<p>Thông tin mập mờ, không rõ ràng cũng là một nguyên nhân khiến tinh thần làm việc của nhân viên sụt giảm. Lảng tránh vấn đề hay nói dối chỉ làm cho tình hình trở nên nghiêm trọng hơn. Do đó, hãy cho họ biết những thông tin cần thiết, về những gì đang diễn ra trong công ty, phòng ban, kể cả chuyện không vui. Nhân viên càng biết nhiều về những gì đang diễn ra, họ càng mất ít thời gian cũng như năng lượng suy đoán và lo lắng về những điều họ không biết. Môi trường làm việc sẽ được gắn chặt hơn khi mọi người cùng biết và cùng nhau hướng tới mục tiêu chung.</p>\r\n<p><em>3. Truyền động lực cho nhân viên</em></p>\r\n<p>Làm cho nhân viên có động lực và gắn kết với công ty là một nhiệm vụ quan trọng nhưng rất khó khăn, đặc biệt trong giai đoạn khủng hoảng kinh tế. Họ sẽ có nhuệ khí làm việc nếu thấy một tầm nhìn hấp dẫn về tương lai tốt đẹp của công ty, về sự khác biệt họ đem lại. Do đó, hãy truyền động lực cho nhân viên bằng những lời ca tụng, những câu chuyện về tấm gương sáng trong công ty, về những lá thư cám ơn của khách hàng… Bạn có thể thực hiện điều này trong cuộc họp định kì của phòng ban/ công ty hay trong các thông báo chung.</p>\r\n<p><em>4. Đảm bảo nhân viên cố gắng hết mình để giành thắng lợi nhưng không suy sụp khi thất bại</em></p>\r\n<p>Nếu nhân viên gặp thất bại trong công việc nhưng không nhận được sự chia sẻ từ sếp, họ sẽ mang tư tưởng và trạng thái cảm xúc đó tới mọi việc khác. Điều này tiếp diễn sẽ không chỉ ảnh hưởng tới sự nghiệp của nhân viên nói riêng mà còn cả năng suất, hiệu quả của công ty nói chung. Vì vậy, bên cạnh việc động viên nhân viên làm việc hết mình, một người quản lí cũng cần giúp đỡ khi họ gặp thất bại, giúp họ rút ra bài học và động viên họ tiếp tục phấn đấu.</p>\r\n<p><em>5. Chúc mừng thành công của nhân viên</em></p>\r\n<p>Ăn mừng thành công của công ty và của cá nhân không chỉ tạo ra tiếng vang tích cực, nó còn giúp nhân viên nhận thấy bản thân mình như một phần của chiến thắng. Hành động này chắc chắn sẽ nuôi dưỡng thái độ làm việc nhiệt huyết hơn và tinh thần sẵn sàng đương đầu và vượt qua thách thức của nhân viên.</p>\r\n<p>Hơn nữa, khi giai đoạn khó khăn mang tới những thông tin tiêu cực, bằng cách thu hút sự chú ý tới thành thành công, bạn sẽ mang tới sự tích cực cho nhân viên. Chúc mừng và trao thưởng xứng đáng cho nhân viên, bạn sẽ tăng cường niềm tin vào công ty của họ. Bạn sẽ nâng trạng thái cảm xúc của nhân viên lên trạng thái hi vọng, nghĩa là nhân viên luôn tin rằng mình có thể vượt qua bất cứ trở ngại nào.</p>\r\n<p><em>6. Thấu hiểu cảm xúc của nhân viên</em></p>\r\n<p>Khi nhân viên có nỗi niềm tâm sự nhưng không ai chú ý tới, họ sẽ mắc kẹt trong trạng thái cô độc. Họ cũng sẽ không lắng nghe hay quan tâm tới những điều bạn nói. Hãy cố gắng hiểu được những trăn trở của nhân viên, kể cả trong công việc cũng như cuộc sống riêng. Từ đó, bạn sẽ thắt chặt mối quan hệ giữa nhân viên và người quản lí cũng như xây dựng môi trường làm việc hòa hợp, vững mạnh.</p>\r\n<p align="right">Vũ Vũ</p>', 1, 5, 0, 39, '2010-12-07 10:08:58', 62, '', '2010-12-08 08:12:07', 62, 0, '0000-00-00 00:00:00', '2010-12-07 10:08:58', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 1, '', '', 0, 130, 'robots=\nauthor='),
(62, 'Thư xin việc', 'th-xin-vic', '', '<p><!--[if gte mso 9]><xml> <w:WordDocument> <w:View>Normal</w:View> <w:Zoom>0</w:Zoom> <w:PunctuationKerning /> <w:ValidateAgainstSchemas /> <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid> <w:IgnoreMixedContent>false</w:IgnoreMixedContent> <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText> <w:Compatibility> <w:BreakWrappedTables /> <w:SnapToGridInCell /> <w:WrapTextWithPunct /> <w:UseAsianBreakRules /> <w:DontGrowAutofit /> </w:Compatibility> <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel> </w:WordDocument> </xml><![endif]--><!--[if gte mso 9]><xml> <w:LatentStyles DefLockedState="false" LatentStyleCount="156"> </w:LatentStyles> </xml><![endif]--><!--[if !mso]> <span class="mceItemObject"   classid="clsid:38481807-CA0E-42D2-BF39-B33AF135CC4D" id=ieooui> </span> <mce:style><!  st1\\:*{behavior:url(#ieooui) } --> <!--[endif]--><!--[if gte mso 10]> <mce:style><!   /* Style Definitions */  table.MsoNormalTable 	{mso-style-name:"Table Normal"; 	mso-tstyle-rowband-size:0; 	mso-tstyle-colband-size:0; 	mso-style-noshow:yes; 	mso-style-parent:""; 	mso-padding-alt:0in 5.4pt 0in 5.4pt; 	mso-para-margin:0in; 	mso-para-margin-bottom:.0001pt; 	mso-pagination:widow-orphan; 	font-size:10.0pt; 	font-family:"Times New Roman"; 	mso-ansi-language:#0400; 	mso-fareast-language:#0400; 	mso-bidi-language:#0400;} --> <!--[endif]--></p>\r\n<p class="style1" style="text-align: left;">Để gây ấn tượng cho nhà tuyển dụng ngay lần xem CV đầu tiên, một phần không thể thiếu trong hồ sơ của bạn là Thư xin việc . Bạn đã chuẩn bị thư xin việc chưa? Nội dung bao gồm những gì ? Hãy tham khảo mẫu thư xin việc dưới đây để tao cho mình bộ hồ sơ đẹp.\r\n', '\r\n</p>\r\n<p class="style1" style="text-align: center;" align="center"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM <br /> Độc Lập - Tự Do - Hạnh Phúc <br /> ………………………<br /> <br /> <span class="style31"><span style="font-family: &quot;Times New Roman&quot;;">ĐƠN XIN VIỆC</span></span></span></p>\r\n<p class="style1style2"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;">Kính gởi: Ban lãnh đạo công ty XX</span></p>\r\n<p class="style1" style="text-align: justify;"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;">Tôi được biết quý công ty đang có nhu cầu tuyển dụng nhân viên ở vị trí ..... qua wetbsite : http://vieclamviet.vn/ . Đây chính là công việc rất phù hợp với khả năng và kinh nghiệm được đúc kết trong quá trình học tập và làm việc từ trước đến nay của tôi.</span></p>\r\n<p class="style1" style="text-align: justify;"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;">Thông qua tin tuyển dụng tôi được biết vị trí quý công ty đang tuyển dụng đòi hỏi kỹ năng nghe nói đọc viết tiếng Nhật, tiếng Anh tốt, sử dụng vi tính văn phòng thành thạo, cũng như khả năng tự tin trong giao tiếp. Tôi đã tốt nghiệp trường Đại học XX, khoa tiếng Nhật, trong đó các môn tiếng Nhật, tiếng Anh và vi tính văn phòng đã được chú trọng và đầu tư hàng đầu. Ngoài ra, về tiếng Nhật, tôi còn có điều kiện phát triển toàn diện mọi kỹ năng trong thời gian du học ở trường Đại học XX, Nhật Bản, và trong thời gian làm các công việc bán thời gian có sử dụng tiếng Nhật. </span></p>\r\n<p class="style1" style="text-align: justify;"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;">Tôi cũng hiểu ngoài những yêu cầu trên, vị trí quý công ty đang tuyển dụng còn đòi hỏi người có tinh thần đoàn kết, hòa đồng với tập thể, cũng như có khả năng làm việc trong môi trường có áp lực cao. Đây chính là những kỹ năng mà tôi đúc kết được qua quá trình học tập, làm việc từ trước đến nay ở Việt Nam cũng như ở Nhật Bản. </span></p>\r\n<p class="style1" style="text-align: justify;"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;">Vì những kỹ năng, kinh nghiệm nêu trên, tôi tin rằng tôi có thể đáp ứng được yêu cầu của quý công ty. Tôi cũng xin cam đoan chấp hành nghiêm chỉnh mọi qui định của quý công ty và hoàn thành tốt công việc được phân công. </span></p>\r\n<p class="style1" style="text-align: justify;"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;">Mọi liên hệ về lịch phỏng vấn hay thông tin về hồ sơ xin việc này, xin vui lòng liên lạc số điện thoại 09xxxxxxxx. </span></p>\r\n<p class="style1" style="text-align: justify;"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;">Xin chân thành cám ơn và trân trọng kính chào.<br /></span></p>\r\n<p class="MsoNormal"><span style="font-size: 11pt;"> </span></p>\r\n<p class="style1" style="text-align: center;" align="center"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;"><span> </span><span> </span>Hà Nội, ngày xxx tháng xxx năm xxxx <br /> <span> </span><span> </span>Người làm đơn </span></p>\r\n<p class="style1" style="text-align: center;" align="center"><em><span style="font-size: 11pt;"><span> </span>(Kí tên) </span></em><span style="font-family: &quot;Times New Roman&quot;;"><span> </span></span></p>\r\n<p class="MsoNormal"><span style="font-size: 11pt;"> </span></p>\r\n<p class="MsoNormal"><span style="font-size: 11pt;"> </span></p>\r\n<p class="MsoNormal"><span style="font-size: 11pt;"> </span></p>\r\n<p class="MsoNormal"><span style="font-size: 11pt;"><span> </span><span> </span></span></p>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow: hidden;"><!--[if gte mso 9]><xml> <w:WordDocument> <w:View>Normal</w:View> <w:Zoom>0</w:Zoom> <w:PunctuationKerning /> <w:ValidateAgainstSchemas /> <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid> <w:IgnoreMixedContent>false</w:IgnoreMixedContent> <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText> <w:Compatibility> <w:BreakWrappedTables /> <w:SnapToGridInCell /> <w:WrapTextWithPunct /> <w:UseAsianBreakRules /> <w:DontGrowAutofit /> </w:Compatibility> <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel> </w:WordDocument> </xml><![endif]--><!--[if gte mso 9]><xml> <w:LatentStyles DefLockedState="false" LatentStyleCount="156"> </w:LatentStyles> </xml><![endif]--><!--[if !mso]><span class="mceItemObject"   classid="clsid:38481807-CA0E-42D2-BF39-B33AF135CC4D" id=ieooui></span> <mce:style><!  st1\\:*{behavior:url(#ieooui) } --> <!--[endif]--><!--[if gte mso 10]> <mce:style><!   /* Style Definitions */  table.MsoNormalTable 	{mso-style-name:"Table Normal"; 	mso-tstyle-rowband-size:0; 	mso-tstyle-colband-size:0; 	mso-style-noshow:yes; 	mso-style-parent:""; 	mso-padding-alt:0in 5.4pt 0in 5.4pt; 	mso-para-margin:0in; 	mso-para-margin-bottom:.0001pt; 	mso-pagination:widow-orphan; 	font-size:10.0pt; 	font-family:"Times New Roman"; 	mso-ansi-language:#0400; 	mso-fareast-language:#0400; 	mso-bidi-language:#0400;} --> <!--[endif]-->\r\n<p class="style1" style="text-align: center;" align="center"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM <br /> Độc Lập - Tự Do - Hạnh Phúc <br /> ………………………<br /> <br /> <span class="style31"><span style="font-family: &quot;Times New Roman&quot;;">ĐƠN XIN VIỆC</span></span></span></p>\r\n<p class="style1style2"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;">Kính gởi: Ban lãnh đạo công ty XX</span></p>\r\n<p class="style1" style="text-align: justify;"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;">Tôi được biết quý công ty đang có nhu cầu tuyển dụng nhân viên ở vị trí thông phiên dịch tiếng Nhật kiêm trợ lý Tổng Giám Đốc qua quảng cáo trên báo ABC đăng ngày xx/x/2006. Đây chính là công việc rất phù hợp với khả năng và kinh nghiệm được đúc kết trong quá trình học tập và làm việc từ trước đến nay của tôi. </span></p>\r\n<p class="style1" style="text-align: justify;"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;">Thông qua mẫu quảng cáo, tôi được biết vị trí quý công ty đang tuyển dụng đòi hỏi kỹ năng nghe nói đọc viết tiếng Nhật, tiếng Anh tốt, sử dụng vi tính văn phòng thành thạo, cũng như khả năng tự tin trong giao tiếp. Tôi đã tốt nghiệp trường Đại học XX, khoa tiếng Nhật, trong đó các môn tiếng Nhật, tiếng Anh và vi tính văn phòng đã được chú trọng và đầu tư hàng đầu. Ngoài ra, về tiếng Nhật, tôi còn có điều kiện phát triển toàn diện mọi kỹ năng trong thời gian du học ở trường Đại học XX, Nhật Bản, và trong thời gian làm các công việc bán thời gian có sử dụng tiếng Nhật. </span></p>\r\n<p class="style1" style="text-align: justify;"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;">Tôi cũng hiểu ngoài những yêu cầu trên, vị trí quý công ty đang tuyển dụng còn đòi hỏi người có tinh thần đoàn kết, hòa đồng với tập thể, cũng như có khả năng làm việc trong môi trường có áp lực cao. Đây chính là những kỹ năng mà tôi đúc kết được qua quá trình học tập, làm việc từ trước đến nay ở Việt Nam cũng như ở Nhật Bản. </span></p>\r\n<p class="style1" style="text-align: justify;"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;">Vì những kỹ năng, kinh nghiệm nêu trên, tôi tin rằng tôi có thể đáp ứng được yêu cầu của quý công ty. Tôi cũng xin cam đoan chấp hành nghiêm chỉnh mọi qui định của quý công ty và hoàn thành tốt công việc được phân công. </span></p>\r\n<p class="style1" style="text-align: justify;"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;">Mọi liên hệ về lịch phỏng vấn hay thông tin về hồ sơ xin việc này, xin vui lòng liên lạc số điện thoại 0988xxxxxx. </span></p>\r\n<p class="MsoNormal"><span style="font-size: 11pt;">Xin chân thành cám ơn và trân trọng kính chào. </span></p>\r\n<p class="MsoNormal"><span style="font-size: 11pt;"> </span></p>\r\n<p class="style1" style="text-align: center;" align="center"><span style="font-size: 11pt; font-family: &quot;Times New Roman&quot;;"><span> </span><span> </span>Hà Nội, ngày xxx tháng xxx năm xxxx <br /> <span> </span><span> </span>Người làm đơn </span></p>\r\n<p class="style1" style="text-align: center;" align="center"><em><span style="font-size: 11pt;"><span> </span>(Kí tên) </span></em><span style="font-family: &quot;Times New Roman&quot;;"><span> </span></span></p>\r\n<p class="MsoNormal"><span style="font-size: 11pt;"> </span></p>\r\n<p class="MsoNormal"><span style="font-size: 11pt;"> </span></p>\r\n<p class="MsoNormal"><span style="font-size: 11pt;"> </span></p>\r\n<p class="MsoNormal"><span style="font-size: 11pt;"><span> </span><span> </span>Nguyễn Văn Thảo </span></p>\r\n</div>', 1, 7, 0, 44, '2010-12-18 02:10:25', 62, '', '2010-12-22 07:52:19', 62, 0, '0000-00-00 00:00:00', '2010-12-18 02:10:25', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 2, '', '', 0, 66, 'robots=\nauthor='),
(63, 'Mẫu thư cảm ơn', 'mu-th-cm-n', '', '<p>Một việc quan trọng là bạn cần phải viết thư cảm ơn sau buổi phỏng vấn, nhất là buổi phỏng vấn thứ hai. Bạn cũng cần cảm ơn những người mà bạn phỏng vấn và những người đã giúp bạn biết được công việc đó. Dưới đây là một số mẹo hay mach bạn cách viết một thư cảm ơn hiệu quả.\r\n', '\r\n</p>\r\n<p><strong>GửI nhanh – </strong>Viết thư cảm ơn và gửi càng sớm càng tốt (tốt nhất là trong vòng hai tư giờ sau buổi phỏng vấn). Nếu bạn không có nhiều thời gian, <em>hãy gửi email cho họ</em>.</p>\r\n<p><strong>Viết thư cảm ơn những người lien quan – </strong>Ngoài người phỏng vấn, bạn cần cảm ơn những người đã giúp bạn tìm được công việc đó bao gồm người giới thiệu công việc đó cho bạn và những người chứng nhận lý lịch và quá trình công tác của bạn.</p>\r\n<p><strong>Tạo ấn tượng tốt – </strong>Theo số liệu của Viện Nghiên Cứu Chuyên Môn Hoa Kỳ, không quá 4% số người đi xin việc viết thư cảm ơn. Vì vậy bạn cần phải viết một bức thư cảm ơn thật sự nổi bật so vớ những người khác để tạo được ấn tượng tốt.</p>\r\n<p><strong>Thêm thông tin mà bạn muốn nói trong buổi phỏng vấn - </strong>Nếu bạn quên một số thông tin trong buổi phỏng vấn thì đây chính là cơ hội bạn viết them trong thư cảm ơn.</p>\r\n<p><strong>Những vấn đề cơ bản – </strong>Thư cảm ơn có thể viết tay, đánh máy hoặc gửi qua email. Một bức thư cảm ơn phảI bao gồm lời cảm ơn, sự quan tâm của bạn tới vị trí công việc, bằng cấp và trình độ và kết thúc là một lời cảm ơn.</p>\r\n<p><strong>Ngắn gọn và đơn giản – </strong>Hãy viết ngăn gọn và đơn giản và không nên dung thư cảm ơn như là một cách chỉ để thể hiện sự quan tâm của bạn tới vị trí công việc hoặc công ty đó.</p>\r\n<p><strong>Kiểm tra - </strong>Kiểm tra chính tả và lỗi ngữ pháp. Sau đó, bạn hãy nhờ người khác kiểm tra và sưa lại nếu có thể thư cho bạn để có một bức thư hoàn thiện.</p>\r\n<p><strong>Dòng cuối – </strong>Luôn kết thúc thư bằng lời cảm ơn.</p>\r\n<p><strong>Mẫu thư cảm ơn sau buổi phỏng vấn</strong></p>\r\n<p>Tên <br /> Địa chỉ <br /> Tỉnh, Thành phố <br /> Điện thoạI <br /> Email</p>\r\n<p>Ngày <br /> <br /> Tên <br /> Chức danh <br /> Công ty <br /> Địa chỉ <br /> Thành phố, mã điện thoại <br /> <br /> Ông/Bà thân mến:</p>\r\n<p>Tôi xin chân thành cảm ơn ông/bà đã dành thời gian phỏng vấn tôi cho vị trí Lập Trình Viên Cao Cấp trong công ty XXX.</p>\r\n<p>Sau khi được phỏng vấn và gặp gỡ những người trong công ty, tôi thấy mình là ngườI xứng đáng cho vị trí công việc đó vì tôi có khả năng nắm bắt nhanh và thích nghi cao cho một vị trí đa ngành.</p>\r\n<p>Cùng với nhiệt huyết và tác phong làm việc tốt, chuyên môn cao về kỹ thuật và kỹ năng phân tích của tôi chắc chắn sẽ làm tôt công việc này.</p>\r\n<p>Tôi mong muốn có được cơ hội làm việc cho ông/bà và mong sớm nhận được quyết định của ông về vị trí công việc này.</p>\r\n<p>Xin vui lòng liên lạc với tôi nếu ông/bà muốn biết thêm chi tiết. Số điện thoại của tôi là: (555) 111-1111.</p>\r\n<p>Xin chân thành cảm ơn</p>\r\n<p>Chữ ký <br /> <br /> Tên đầy đủ</p>', 1, 7, 0, 44, '2010-12-18 03:07:30', 62, '', '2010-12-22 07:47:00', 62, 0, '0000-00-00 00:00:00', '2010-12-18 03:07:30', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 3, 0, 1, '', '', 0, 96, 'robots=\nauthor='),
(64, '__404__', '__404__', '__404__', '<h1>Bad karma: we can''t find that page!</h1>\r\n<p>You asked for <strong>{%sh404SEF_404_URL%}</strong>, but despite our computers looking very hard, we could not find it. What happened ?</p>\r\n<ul>\r\n<li>the link you clicked to arrive here has a typo in it</li>\r\n<li>or somehow we removed that page, or gave it another name</li>\r\n<li>or, quite unlikely for sure, maybe you typed it yourself and there was a little mistake ?</li>\r\n</ul>\r\n<h4>{sh404sefSimilarUrlsCommentStart}It''s not the end of everything though : you may be interested in the following pages on our site:{sh404sefSimilarUrlsCommentEnd}</h4>\r\n<p>{sh404sefSimilarUrls}</p>\r\n<p> </p>', '', 1, 0, 0, 0, '2009-01-09 12:00:00', 62, '', '2011-02-14 09:01:05', 62, 0, '2009-01-09 12:00:00', '2009-01-09 12:00:00', '0000-00-00 00:00:00', '', '', 'menu_image=-1\nshow_title=0\nshow_section=0\nshow_category=0show_vote=0\nshow_author=0\nshow_create_date=0\nshow_modify_date=0\nshow_pdf_icon=0\nshow_print_icon=0\nshow_email_icon=0\npageclass_sfx=', 1, 0, 0, '', '', 0, 124, '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_content_frontpage`
--

CREATE TABLE IF NOT EXISTS `jos_content_frontpage` (
  `content_id` int(11) NOT NULL default '0',
  `ordering` int(11) NOT NULL default '0',
  PRIMARY KEY  (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_content_frontpage`
--

INSERT INTO `jos_content_frontpage` (`content_id`, `ordering`) VALUES
(45, 1),
(6, 2),
(44, 3),
(5, 4),
(9, 5),
(30, 6),
(16, 7);

-- --------------------------------------------------------

--
-- Table structure for table `jos_content_rating`
--

CREATE TABLE IF NOT EXISTS `jos_content_rating` (
  `content_id` int(11) NOT NULL default '0',
  `rating_sum` int(11) unsigned NOT NULL default '0',
  `rating_count` int(11) unsigned NOT NULL default '0',
  `lastip` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_content_rating`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_aro`
--

CREATE TABLE IF NOT EXISTS `jos_core_acl_aro` (
  `id` int(11) NOT NULL auto_increment,
  `section_value` varchar(240) NOT NULL default '0',
  `value` varchar(240) NOT NULL default '',
  `order_value` int(11) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `hidden` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `jos_section_value_value_aro` (`section_value`(100),`value`(100)),
  KEY `jos_gacl_hidden_aro` (`hidden`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=219 ;

--
-- Dumping data for table `jos_core_acl_aro`
--

INSERT INTO `jos_core_acl_aro` (`id`, `section_value`, `value`, `order_value`, `name`, `hidden`) VALUES
(10, 'users', '62', 0, 'Super Administrator', 0),
(11, 'users', '63', 0, 'khuong', 0),
(17, 'users', '69', 0, 'asgsaghshsd', 0),
(18, 'users', '70', 0, 'zxbzxbzbzxbzbxzb', 0),
(15, 'users', '67', 0, 'nguyen ngoc khuong', 0),
(19, 'users', '71', 0, 'khuong', 0),
(20, 'users', '72', 0, 'asgashshdsd123', 0),
(21, 'users', '73', 0, 'nnkhuong', 0),
(22, 'users', '74', 0, 'GIỚI THIỆU321', 0),
(23, 'users', '75', 0, 'asgsgsgsd', 0),
(172, 'users', '227', 0, 'Vu Van Nam', 0),
(25, 'users', '77', 0, 'khuong', 0),
(171, 'users', '226', 0, 'thanhtd211986', 0),
(27, 'users', '79', 0, 'phuong', 0),
(170, 'users', '225', 0, 'hanh', 0),
(31, 'users', '83', 0, 'thanhtd1', 0),
(32, 'users', '84', 0, 'thanhtd2', 0),
(179, 'users', '234', 0, 'trần trọng tân', 0),
(35, 'users', '87', 0, 'admin321', 0),
(37, 'users', '89', 0, 'mrthanhtd2', 0),
(38, 'users', '90', 0, 'thuhang1', 0),
(39, 'users', '91', 0, 'thuhang2', 0),
(42, 'users', '94', 0, 'Lan Anh', 0),
(43, 'users', '95', 0, 'Đỗ Bích Thắm', 0),
(44, 'users', '96', 0, 'ESECO', 0),
(45, 'users', '97', 0, 'Thuc pham TH', 0),
(46, 'users', '98', 0, 'dulich', 0),
(169, 'users', '224', 0, 'hangnt05', 0),
(141, 'users', '193', 0, 'thaison', 0),
(68, 'users', '120', 0, 'Trần Thành Trung', 0),
(54, 'users', '106', 0, 'Teleperformance', 0),
(55, 'users', '107', 0, 'gialong', 0),
(58, 'users', '110', 0, 'thuhang4', 0),
(59, 'users', '111', 0, 'joliesiam', 0),
(60, 'users', '112', 0, 'Nguyễn Tiến Minh', 0),
(61, 'users', '113', 0, 'avo', 0),
(62, 'users', '114', 0, 'Nguyễn Lê Sơn', 0),
(63, 'users', '115', 0, 'Nguyễn Quốc Dũng', 0),
(67, 'users', '119', 0, 'Mis Bông', 0),
(66, 'users', '118', 0, 'Đặng Tuấn Hưng', 0),
(69, 'users', '121', 0, 'cao văn trung', 0),
(70, 'users', '122', 0, 'Thanh Nga', 0),
(71, 'users', '123', 0, 'Phạm Hồng Thu', 0),
(72, 'users', '124', 0, 'Đỗ Thị Trung', 0),
(73, 'users', '125', 0, 'Nguyễn Giang', 0),
(177, 'users', '232', 0, 'Tống Văn Dũng', 0),
(75, 'users', '127', 0, 'Võ Thị Thanh Đàn', 0),
(76, 'users', '128', 0, 'Đỗ Thị Bằng', 0),
(77, 'users', '129', 0, 'Hùng', 0),
(78, 'users', '130', 0, 'Nguyễn Thị Việt Anh', 0),
(79, 'users', '131', 0, 'Bích Thuỷ', 0),
(80, 'users', '132', 0, 'Anh Bình hoặc Hùng', 0),
(81, 'users', '133', 0, 'Ho Thi Hai Anh', 0),
(82, 'users', '134', 0, 'Mai Thị Yến', 0),
(83, 'users', '135', 0, 'Nguyễn Thành Lập', 0),
(84, 'users', '136', 0, 'Cương', 0),
(86, 'users', '138', 0, 'Lê Thị Hồng', 0),
(87, 'users', '139', 0, 'Phòng nhân sự', 0),
(88, 'users', '140', 0, 'Phòng lễ tân', 0),
(89, 'users', '141', 0, 'Anh Bình', 0),
(90, 'users', '142', 0, 'Phòng quản lý nhân sự', 0),
(91, 'users', '143', 0, 'CÔNG TY CỔ PHẦN CÔNG NGHỆ SAO PHƯƠNG NAM', 0),
(92, 'users', '144', 0, 'hành chính nhân sự', 0),
(93, 'users', '145', 0, 'Yến', 0),
(94, 'users', '146', 0, 'Thanh Hoa', 0),
(95, 'users', '147', 0, 'Nguyễn Thị Ngọc Hà', 0),
(96, 'users', '148', 0, 'Vũ Huyền', 0),
(97, 'users', '149', 0, 'Nguyễn Thùy Duyên', 0),
(98, 'users', '150', 0, 'Kỳ Duyên', 0),
(99, 'users', '151', 0, 'Vo minh Tuan', 0),
(100, 'users', '152', 0, 'Nguyễn Phương', 0),
(101, 'users', '153', 0, 'Vân', 0),
(102, 'users', '154', 0, 'Minh', 0),
(103, 'users', '155', 0, 'Nguyễn Hồng Loan', 0),
(104, 'users', '156', 0, 'Thanh  Trúc', 0),
(105, 'users', '157', 0, 'Phúc', 0),
(106, 'users', '158', 0, 'Lê Trà My', 0),
(107, 'users', '159', 0, 'Hữu Lập', 0),
(108, 'users', '160', 0, 'Hoàng Anh', 0),
(109, 'users', '161', 0, 'Thu Phương', 0),
(110, 'users', '162', 0, 'tran thi hoai anh', 0),
(111, 'users', '163', 0, 'Lê Thị Nê', 0),
(112, 'users', '164', 0, 'Nhung', 0),
(113, 'users', '165', 0, 'Lê Huỳnh Triều', 0),
(114, 'users', '166', 0, 'Quyen', 0),
(115, 'users', '167', 0, 'Hùng', 0),
(116, 'users', '168', 0, 'Trịnh Lợi', 0),
(118, 'users', '170', 0, 'Hảo', 0),
(119, 'users', '171', 0, 'Nguyễn Thị Thu', 0),
(120, 'users', '172', 0, 'Nguyễn Thị Dung', 0),
(121, 'users', '173', 0, 'Hoàng Phú', 0),
(122, 'users', '174', 0, 'Phòng HC- NS', 0),
(123, 'users', '175', 0, 'Nhật Duy', 0),
(124, 'users', '176', 0, 'Hồng Nhung', 0),
(125, 'users', '177', 0, 'Chị Khánh', 0),
(126, 'users', '178', 0, 'newstartour', 0),
(127, 'users', '179', 0, 'Quỳnh Trang', 0),
(128, 'users', '180', 0, 'Tạ Giang', 0),
(129, 'users', '181', 0, 'Thắng', 0),
(130, 'users', '182', 0, 'Diễm', 0),
(131, 'users', '183', 0, 'Tùng', 0),
(132, 'users', '184', 0, 'Hồng Anh', 0),
(133, 'users', '185', 0, 'Hanowindow', 0),
(134, 'users', '186', 0, 'Phạm Hoài Vũ', 0),
(135, 'users', '187', 0, 'Hà Văn Lưu', 0),
(136, 'users', '188', 0, 'Đặng Thành Tung', 0),
(137, 'users', '189', 0, 'Mr Hải', 0),
(144, 'users', '196', 0, 'Trịnh Trần Ngọc Trang', 0),
(167, 'users', '222', 0, 'Đặng Ngọc Hà', 0),
(145, 'users', '197', 0, 'Quyết', 0),
(151, 'users', '203', 0, 'Nguyễn Thị Kim Nhung', 0),
(152, 'users', '204', 0, 'Nguyễn Thị Thương', 0),
(153, 'users', '205', 0, 'Trần Loan', 0),
(154, 'users', '206', 0, 'Phạm Thanh Huyền', 0),
(155, 'users', '207', 0, 'Thanh Lâm', 0),
(156, 'users', '208', 0, 'Nguyễn Hồng Anh Khá', 0),
(157, 'users', '212', 0, 'thanhtd-creativevietnam', 0),
(158, 'users', '213', 0, 'Nguyễn Thu Hà', 0),
(159, 'users', '214', 0, 'Lê Dưỡng', 0),
(160, 'users', '215', 0, 'thuhang22', 0),
(161, 'users', '216', 0, 'nhathongco', 0),
(162, 'users', '217', 0, 'mediapro', 0),
(166, 'users', '221', 0, 'hanchip', 0),
(173, 'users', '228', 0, 'Phạm Trọng Nghĩa', 0),
(176, 'users', '231', 0, 'Phạm Trọng Nghĩa', 0),
(175, 'users', '230', 0, 'Mss Quỳnh', 0),
(178, 'users', '233', 0, 'Quản trị Viên', 0),
(180, 'users', '235', 0, 'hangnt04', 0),
(181, 'users', '236', 0, 'Trung', 0),
(182, 'users', '237', 0, 'Thanh', 0),
(183, 'users', '238', 0, 'Trueman', 0),
(184, 'users', '239', 0, 'thanh nhàn', 0),
(185, 'users', '240', 0, 'nhàn', 0),
(186, 'users', '241', 0, 'ngan hà', 0),
(187, 'users', '242', 0, 'thuhang.mnc', 0),
(188, 'users', '243', 0, 'Nguyễn Thanh Tân', 0),
(189, 'users', '244', 0, 'Phi', 0),
(190, 'users', '245', 0, 'Anh Ngọc', 0),
(191, 'users', '246', 0, 'Anh Đào', 0),
(192, 'users', '247', 0, 'Quy', 0),
(193, 'users', '248', 0, 'Hieu', 0),
(194, 'users', '249', 0, 'Phạm Thị Bích Vân', 0),
(195, 'users', '250', 0, 'Nguyễn Duy Khánh', 0),
(196, 'users', '251', 0, 'Ms Hà', 0),
(197, 'users', '252', 0, 'mr Cong', 0),
(198, 'users', '253', 0, 'Hoàn', 0),
(199, 'users', '254', 0, 'Nguyễn Hà Long', 0),
(200, 'users', '255', 0, 'Hoàn', 0),
(201, 'users', '256', 0, 'Bùi Văn Tân', 0),
(202, 'users', '257', 0, 'Công ty cổ phần tư vấn toàn câu Vina - Globavina., JSC', 0),
(203, 'users', '258', 0, 'Nguyễn MInh Tú', 0),
(204, 'users', '259', 0, 'Nguyễn Thị Thu Hà', 0),
(205, 'users', '260', 0, 'Bộ Phận Tuyển Dụng', 0),
(206, 'users', '261', 0, 'Nguyễn Hữu Đông', 0),
(207, 'users', '262', 0, 'Anh Thành', 0),
(208, 'users', '263', 0, 'CHỊ THU', 0),
(209, 'users', '264', 0, 'Tham', 0),
(210, 'users', '265', 0, 'Đỗ Hồng Phước', 0),
(211, 'users', '266', 0, 'Lê Quang Dũng', 0),
(212, 'users', '267', 0, 'Nguyễn Thành Long', 0),
(213, 'users', '268', 0, 'Giang', 0),
(214, 'users', '269', 0, 'Thu Trang', 0),
(215, 'users', '270', 0, 'thanhtd3', 0),
(216, 'users', '271', 0, '1111', 0),
(218, 'users', '273', 0, 'son test', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_aro_groups`
--

CREATE TABLE IF NOT EXISTS `jos_core_acl_aro_groups` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `lft` int(11) NOT NULL default '0',
  `rgt` int(11) NOT NULL default '0',
  `value` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `jos_gacl_parent_id_aro_groups` (`parent_id`),
  KEY `jos_gacl_lft_rgt_aro_groups` (`lft`,`rgt`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `jos_core_acl_aro_groups`
--

INSERT INTO `jos_core_acl_aro_groups` (`id`, `parent_id`, `name`, `lft`, `rgt`, `value`) VALUES
(17, 0, 'ROOT', 1, 22, 'ROOT'),
(28, 17, 'USERS', 2, 21, 'USERS'),
(29, 28, 'Public Frontend', 3, 12, 'Public Frontend'),
(18, 29, 'Registered', 4, 11, 'Registered'),
(19, 18, 'Author', 5, 10, 'Author'),
(20, 19, 'Editor', 6, 9, 'Editor'),
(21, 20, 'Publisher', 7, 8, 'Publisher'),
(30, 28, 'Public Backend', 13, 20, 'Public Backend'),
(23, 30, 'Manager', 14, 19, 'Manager'),
(24, 23, 'Administrator', 15, 18, 'Administrator'),
(25, 24, 'Super Administrator', 16, 17, 'Super Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_aro_map`
--

CREATE TABLE IF NOT EXISTS `jos_core_acl_aro_map` (
  `acl_id` int(11) NOT NULL default '0',
  `section_value` varchar(230) NOT NULL default '0',
  `value` varchar(100) NOT NULL,
  PRIMARY KEY  (`acl_id`,`section_value`,`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_core_acl_aro_map`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_aro_sections`
--

CREATE TABLE IF NOT EXISTS `jos_core_acl_aro_sections` (
  `id` int(11) NOT NULL auto_increment,
  `value` varchar(230) NOT NULL default '',
  `order_value` int(11) NOT NULL default '0',
  `name` varchar(230) NOT NULL default '',
  `hidden` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `jos_gacl_value_aro_sections` (`value`),
  KEY `jos_gacl_hidden_aro_sections` (`hidden`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `jos_core_acl_aro_sections`
--

INSERT INTO `jos_core_acl_aro_sections` (`id`, `value`, `order_value`, `name`, `hidden`) VALUES
(10, 'users', 1, 'Users', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_groups_aro_map`
--

CREATE TABLE IF NOT EXISTS `jos_core_acl_groups_aro_map` (
  `group_id` int(11) NOT NULL default '0',
  `section_value` varchar(240) NOT NULL default '',
  `aro_id` int(11) NOT NULL default '0',
  UNIQUE KEY `group_id_aro_id_groups_aro_map` (`group_id`,`section_value`,`aro_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_core_acl_groups_aro_map`
--

INSERT INTO `jos_core_acl_groups_aro_map` (`group_id`, `section_value`, `aro_id`) VALUES
(18, '', 12),
(18, '', 13),
(18, '', 14),
(18, '', 15),
(18, '', 16),
(18, '', 17),
(18, '', 18),
(18, '', 19),
(18, '', 20),
(18, '', 21),
(18, '', 22),
(18, '', 23),
(18, '', 25),
(18, '', 27),
(18, '', 31),
(18, '', 32),
(18, '', 35),
(18, '', 37),
(18, '', 38),
(18, '', 39),
(18, '', 42),
(18, '', 43),
(18, '', 44),
(18, '', 45),
(18, '', 46),
(18, '', 54),
(18, '', 55),
(18, '', 58),
(18, '', 59),
(18, '', 60),
(18, '', 61),
(18, '', 62),
(18, '', 63),
(18, '', 66),
(18, '', 67),
(18, '', 68),
(18, '', 69),
(18, '', 70),
(18, '', 71),
(18, '', 72),
(18, '', 73),
(18, '', 75),
(18, '', 76),
(18, '', 77),
(18, '', 78),
(18, '', 79),
(18, '', 80),
(18, '', 81),
(18, '', 82),
(18, '', 83),
(18, '', 84),
(18, '', 86),
(18, '', 87),
(18, '', 88),
(18, '', 89),
(18, '', 90),
(18, '', 91),
(18, '', 92),
(18, '', 93),
(18, '', 94),
(18, '', 95),
(18, '', 96),
(18, '', 97),
(18, '', 98),
(18, '', 99),
(18, '', 100),
(18, '', 101),
(18, '', 102),
(18, '', 103),
(18, '', 104),
(18, '', 105),
(18, '', 106),
(18, '', 107),
(18, '', 108),
(18, '', 109),
(18, '', 110),
(18, '', 111),
(18, '', 112),
(18, '', 113),
(18, '', 114),
(18, '', 115),
(18, '', 116),
(18, '', 118),
(18, '', 119),
(18, '', 120),
(18, '', 121),
(18, '', 122),
(18, '', 123),
(18, '', 124),
(18, '', 125),
(18, '', 126),
(18, '', 127),
(18, '', 128),
(18, '', 129),
(18, '', 130),
(18, '', 132),
(18, '', 133),
(18, '', 134),
(18, '', 135),
(18, '', 136),
(18, '', 137),
(18, '', 141),
(18, '', 144),
(18, '', 145),
(18, '', 151),
(18, '', 152),
(18, '', 153),
(18, '', 154),
(18, '', 155),
(18, '', 156),
(18, '', 158),
(18, '', 159),
(18, '', 160),
(18, '', 161),
(18, '', 162),
(18, '', 166),
(18, '', 167),
(18, '', 169),
(18, '', 171),
(18, '', 172),
(18, '', 173),
(18, '', 175),
(18, '', 176),
(18, '', 177),
(18, '', 179),
(18, '', 180),
(18, '', 181),
(18, '', 182),
(18, '', 183),
(18, '', 184),
(18, '', 185),
(18, '', 186),
(18, '', 187),
(18, '', 188),
(18, '', 189),
(18, '', 190),
(18, '', 191),
(18, '', 192),
(18, '', 193),
(18, '', 194),
(18, '', 195),
(18, '', 196),
(18, '', 197),
(18, '', 198),
(18, '', 199),
(18, '', 200),
(18, '', 201),
(18, '', 202),
(18, '', 203),
(18, '', 204),
(18, '', 205),
(18, '', 206),
(18, '', 207),
(18, '', 208),
(18, '', 209),
(18, '', 210),
(18, '', 211),
(18, '', 212),
(18, '', 213),
(18, '', 214),
(18, '', 215),
(18, '', 216),
(18, '', 218),
(24, '', 11),
(24, '', 170),
(25, '', 10),
(25, '', 131),
(25, '', 157),
(25, '', 178);

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_log_items`
--

CREATE TABLE IF NOT EXISTS `jos_core_log_items` (
  `time_stamp` date NOT NULL default '0000-00-00',
  `item_table` varchar(50) NOT NULL default '',
  `item_id` int(11) unsigned NOT NULL default '0',
  `hits` int(11) unsigned NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_core_log_items`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_core_log_searches`
--

CREATE TABLE IF NOT EXISTS `jos_core_log_searches` (
  `search_term` varchar(128) NOT NULL default '',
  `hits` int(11) unsigned NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_core_log_searches`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_groups`
--

CREATE TABLE IF NOT EXISTS `jos_groups` (
  `id` tinyint(3) unsigned NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_groups`
--

INSERT INTO `jos_groups` (`id`, `name`) VALUES
(0, 'Public'),
(1, 'Registered'),
(2, 'Special');

-- --------------------------------------------------------

--
-- Table structure for table `jos_menu`
--

CREATE TABLE IF NOT EXISTS `jos_menu` (
  `id` int(11) NOT NULL auto_increment,
  `menutype` varchar(75) default NULL,
  `name` varchar(255) default NULL,
  `alias` varchar(255) NOT NULL default '',
  `link` text,
  `type` varchar(50) NOT NULL default '',
  `published` tinyint(1) NOT NULL default '0',
  `parent` int(11) unsigned NOT NULL default '0',
  `componentid` int(11) unsigned NOT NULL default '0',
  `sublevel` int(11) default '0',
  `ordering` int(11) default '0',
  `checked_out` int(11) unsigned NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `pollid` int(11) NOT NULL default '0',
  `browserNav` tinyint(4) default '0',
  `access` tinyint(3) unsigned NOT NULL default '0',
  `utaccess` tinyint(3) unsigned NOT NULL default '0',
  `params` text NOT NULL,
  `lft` int(11) unsigned NOT NULL default '0',
  `rgt` int(11) unsigned NOT NULL default '0',
  `home` int(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `componentid` (`componentid`,`menutype`,`published`,`access`),
  KEY `menutype` (`menutype`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `jos_menu`
--

INSERT INTO `jos_menu` (`id`, `menutype`, `name`, `alias`, `link`, `type`, `published`, `parent`, `componentid`, `sublevel`, `ordering`, `checked_out`, `checked_out_time`, `pollid`, `browserNav`, `access`, `utaccess`, `params`, `lft`, `rgt`, `home`) VALUES
(1, 'mainmenu', 'Trang chủ', 'trang-ch', 'index.php?option=com_content&view=frontpage', 'component', 1, 0, 20, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'num_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\norderby_pri=\norderby_sec=front\nmulti_column_order=1\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=0\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 1),
(55, 'topmenu', 'Trang chủ', 'trang-ch', '', 'url', 1, 0, 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(2, 'mainmenu', 'Đăng hồ sơ', 'ng-h-s', 'index.php?option=com_properties&view=postcv', 'url', 1, 0, 0, 0, 5, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(41, 'mainmenu', 'Hướng dẫn', 'hng-dn', 'index.php?option=com_content&view=section&id=3', 'component', 0, 0, 20, 0, 8, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_description=0\nshow_description_image=0\nshow_categories=1\nshow_empty_categories=0\nshow_cat_num_articles=1\nshow_category_description=1\norderby=\norderby_sec=\nshow_feed_link=1\nshow_noauth=1\nshow_title=1\nlink_titles=1\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(11, 'othermenu', 'Joomla! Home', 'joomla-home', 'index.php?option=com_content&view=article&id=59', 'component', 1, 0, 20, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(12, 'othermenu', 'Joomla! Forums', 'joomla-forums', 'http://forum.joomla.org', 'url', 1, 0, 0, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(13, 'othermenu', 'Joomla! Documentation', 'joomla-documentation', 'http://docs.joomla.org', 'url', 1, 0, 0, 0, 3, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(14, 'othermenu', 'Joomla! Community', 'joomla-community', 'http://community.joomla.org', 'url', 1, 0, 0, 0, 4, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(15, 'othermenu', 'Joomla! Magazine', 'joomla-community-magazine', 'http://magazine.joomla.org/', 'url', 1, 0, 0, 0, 5, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(16, 'othermenu', 'OSM Home', 'osm-home', 'http://www.opensourcematters.org', 'url', 1, 0, 0, 0, 6, 0, '0000-00-00 00:00:00', 0, 0, 0, 6, 'menu_image=-1\n\n', 0, 0, 0),
(17, 'othermenu', 'Administrator', 'administrator', 'administrator/', 'url', 1, 0, 0, 0, 7, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(18, 'topmenu', 'Nhà tuyển dụng', 'nha-tuyn-dng', 'index.php?option=com_user&view=login', 'component', 1, 0, 14, 0, 4, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'show_login_title=1\nheader_login=\nlogin=\nlogin_message=1\ndescription_login=1\ndescription_login_text=\nimage_login=\nimage_login_align=right\nshow_logout_title=1\nheader_logout=\nlogout=\nlogout_message=1\ndescription_logout=1\ndescription_logout_text=\nimage_logout=\npage_title=News\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(20, 'usermenu', 'Your Details', 'your-details', 'index.php?option=com_user&view=user&task=edit', 'component', 1, 0, 14, 0, 1, 62, '2010-12-26 02:23:51', 0, 0, 1, 3, '', 0, 0, 0),
(24, 'usermenu', 'Logout', 'logout', 'index.php?option=com_user&view=login', 'component', 1, 0, 14, 0, 5, 0, '0000-00-00 00:00:00', 0, 0, 1, 3, '', 0, 0, 0),
(38, 'keyconcepts', 'Content Layouts', 'content-layouts', 'index.php?option=com_content&view=article&id=24', 'component', 1, 0, 20, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(27, 'mainmenu', 'Tìm việc làm', 'tiim-vic-lam', 'index.php?option=com_properties&view=all', 'component', 1, 0, 109, 0, 3, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'page_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(28, 'topmenu', 'Trang Chủ', 'trang-ch', 'index.php?option=com_content&view=frontpage', 'component', -2, 0, 20, 0, 6, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'num_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\norderby_pri=\norderby_sec=front\nmulti_column_order=1\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=0\nshow_title=\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(29, 'topmenu', 'Người tìm việc', 'ngi-tim-vic', 'index.php?option=com_user&view=login', 'component', 1, 0, 14, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_login_title=1\nheader_login=\nlogin=\nlogin_message=0\ndescription_login=0\ndescription_login_text=\nimage_login=\nimage_login_align=right\nshow_logout_title=1\nheader_logout=\nlogout=\nlogout_message=1\ndescription_logout=1\ndescription_logout_text=\nimage_logout=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(30, 'topmenu', 'Cung ứng nhân sự', 'cung-ng-nhan-s', 'index.php?option=com_content&view=article&id=57', 'component', 1, 0, 20, 0, 7, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=0\nshow_title=\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(34, 'mainmenu', 'What''s New in 1.5?', 'what-is-new-in-1-5', 'index.php?option=com_content&view=article&id=22', 'component', -2, 0, 20, 0, 4, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(40, 'keyconcepts', 'Extensions', 'extensions', 'index.php?option=com_content&view=article&id=26', 'component', 1, 0, 20, 0, 1, 62, '2010-12-26 02:23:04', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(37, 'mainmenu', 'Nhật ký tìm việc', 'nht-ky-tim-vic', 'index.php?option=com_properties&view=seecker', 'url', 1, 0, 0, 0, 7, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(43, 'keyconcepts', 'Example Pages', 'example-pages', 'index.php?option=com_content&view=article&id=43', 'component', 1, 0, 20, 0, 3, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(44, 'mainmenu', 'Tài liệu biểu mẫu', 'section-blog', 'index.php?option=com_content&view=category&layout=blog&id=44', 'component', 1, 0, 20, 0, 6, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_description=1\nshow_description_image=1\nnum_leading_articles=0\nnum_intro_articles=4\nnum_columns=1\nnum_links=4\norderby_pri=\norderby_sec=\nmulti_column_order=0\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=0\nshow_create_date=0\nshow_modify_date=0\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=0\nshow_pdf_icon=0\nshow_print_icon=0\nshow_email_icon=0\nshow_hits=1\nfeed_summary=\npage_title=Example of Section Blog layout (FAQ section)\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(45, 'mainmenu', 'home', 'section-table', 'index.php?option=com_content&view=frontpage', 'component', -2, 0, 20, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'num_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\norderby_pri=\norderby_sec=front\nmulti_column_order=1\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(46, 'ExamplePages', 'Category Blog', 'categoryblog', 'index.php?option=com_content&view=category&layout=blog&id=31', 'component', 1, 0, 20, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_page_title=1\npage_title=Example of Category Blog layout (FAQs/General category)\nshow_description=0\nshow_description_image=0\nnum_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\nshow_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\norderby_pri=\norderby_sec=\nshow_pagination=2\nshow_pagination_results=1\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(47, 'ExamplePages', 'Category Table', 'category-table', 'index.php?option=com_content&view=category&id=32', 'component', 1, 0, 20, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_page_title=1\npage_title=Example of Category Table layout (FAQs/Languages category)\nshow_headings=1\nshow_date=0\ndate_format=\nfilter=1\nfilter_type=title\npageclass_sfx=\nmenu_image=-1\nsecure=0\norderby_sec=\nshow_pagination=1\nshow_pagination_limit=1\nshow_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(48, 'mainmenu', 'Quy định bảo mật', 'quy-nh-bo-mt', 'index.php?option=com_content&view=article&id=52', 'component', 1, 0, 20, 0, 10, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(49, 'mainmenu', 'Liên hệ', 'lien-h', 'index.php?option=com_contact&view=contact&id=1', 'component', 1, 0, 7, 0, 11, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_contact_list=0\nshow_category_crumb=0\ncontact_icons=\nicon_address=\nicon_email=\nicon_telephone=\nicon_mobile=\nicon_fax=\nicon_misc=\nshow_headings=1\nshow_position=\nshow_email=\nshow_telephone=\nshow_mobile=\nshow_fax=\nallow_vcard=\nbanned_email=\nbanned_subject=\nbanned_text=\nvalidate_session=\ncustom_reply=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(50, 'mainmenu', 'Thỏa thuận sử dụng', 'tha-thun-s-dng', 'index.php?option=com_content&view=article&id=53', 'component', 1, 0, 20, 0, 9, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(51, 'usermenu', 'Submit an Article', 'submit-an-article', 'index.php?option=com_content&view=article&layout=form', 'component', 1, 0, 20, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 2, 0, '', 0, 0, 0),
(52, 'usermenu', 'Submit a Web Link', 'submit-a-web-link', 'index.php?option=com_weblinks&view=weblink&layout=form', 'component', 1, 0, 4, 0, 4, 0, '0000-00-00 00:00:00', 0, 0, 2, 0, '', 0, 0, 0),
(53, 'topmenu', 'Cung ứng và cho thuê lao động', 'cung-ng-va-cho-thue-lao-ng', 'index.php?option=com_content&view=article&id=58', 'component', -2, 0, 20, 0, 3, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(54, 'topmenu', 'Dịch vụ cung cấp Osin cao cấp', 'dch-v-cung-cp-osin-cao-cp', 'index.php?option=com_content&view=article&id=59', 'component', -2, 0, 20, 0, 5, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_menu_types`
--

CREATE TABLE IF NOT EXISTS `jos_menu_types` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `menutype` varchar(75) NOT NULL default '',
  `title` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `menutype` (`menutype`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `jos_menu_types`
--

INSERT INTO `jos_menu_types` (`id`, `menutype`, `title`, `description`) VALUES
(1, 'mainmenu', 'Main Menu', 'The main menu for the site'),
(2, 'usermenu', 'User Menu', 'A Menu for logged in Users'),
(3, 'topmenu', 'Top Menu', 'Top level navigation'),
(4, 'othermenu', 'Resources', 'Additional links'),
(5, 'ExamplePages', 'Example Pages', 'Example Pages'),
(6, 'keyconcepts', 'Key Concepts', 'This describes some critical information for new Users.');

-- --------------------------------------------------------

--
-- Table structure for table `jos_messages`
--

CREATE TABLE IF NOT EXISTS `jos_messages` (
  `message_id` int(10) unsigned NOT NULL auto_increment,
  `user_id_from` int(10) unsigned NOT NULL default '0',
  `user_id_to` int(10) unsigned NOT NULL default '0',
  `folder_id` int(10) unsigned NOT NULL default '0',
  `date_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `state` int(11) NOT NULL default '0',
  `priority` int(1) unsigned NOT NULL default '0',
  `subject` text NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY  (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_messages`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_messages_cfg`
--

CREATE TABLE IF NOT EXISTS `jos_messages_cfg` (
  `user_id` int(10) unsigned NOT NULL default '0',
  `cfg_name` varchar(100) NOT NULL default '',
  `cfg_value` varchar(255) NOT NULL default '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_messages_cfg`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_migration_backlinks`
--

CREATE TABLE IF NOT EXISTS `jos_migration_backlinks` (
  `itemid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `url` text NOT NULL,
  `sefurl` text NOT NULL,
  `newurl` text NOT NULL,
  PRIMARY KEY  (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_migration_backlinks`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_modules`
--

CREATE TABLE IF NOT EXISTS `jos_modules` (
  `id` int(11) NOT NULL auto_increment,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  `position` varchar(50) default NULL,
  `checked_out` int(11) unsigned NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL default '0',
  `module` varchar(50) default NULL,
  `numnews` int(11) NOT NULL default '0',
  `access` tinyint(3) unsigned NOT NULL default '0',
  `showtitle` tinyint(3) unsigned NOT NULL default '1',
  `params` text NOT NULL,
  `iscore` tinyint(4) NOT NULL default '0',
  `client_id` tinyint(4) NOT NULL default '0',
  `control` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

--
-- Dumping data for table `jos_modules`
--

INSERT INTO `jos_modules` (`id`, `title`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `published`, `module`, `numnews`, `access`, `showtitle`, `params`, `iscore`, `client_id`, `control`) VALUES
(1, 'Main Menu', '', 1, 'bottom_menu', 0, '0000-00-00 00:00:00', 1, 'mod_mainmenu', 0, 0, 1, 'menutype=mainmenu\nmenu_style=list\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=\nmoduleclass_sfx=_menu\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nmenu_images_link=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\n\n', 1, 0, ''),
(2, 'Login', '', 1, 'login', 0, '0000-00-00 00:00:00', 1, 'mod_login', 0, 0, 1, '', 1, 1, ''),
(3, 'Popular', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_popular', 0, 2, 1, '', 0, 1, ''),
(4, 'Recent added Articles', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_latest', 0, 2, 1, 'ordering=c_dsc\nuser_id=0\ncache=0\n\n', 0, 1, ''),
(5, 'Menu Stats', '', 5, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_stats', 0, 2, 1, '', 0, 1, ''),
(6, 'Unread Messages', '', 1, 'header', 0, '0000-00-00 00:00:00', 1, 'mod_unread', 0, 2, 1, '', 1, 1, ''),
(7, 'Online Users', '', 2, 'header', 0, '0000-00-00 00:00:00', 1, 'mod_online', 0, 2, 1, '', 1, 1, ''),
(8, 'Toolbar', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', 1, 'mod_toolbar', 0, 2, 1, '', 1, 1, ''),
(9, 'Quick Icons', '', 1, 'icon', 0, '0000-00-00 00:00:00', 1, 'mod_quickicon', 0, 2, 1, '', 1, 1, ''),
(10, 'Logged in Users', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_logged', 0, 2, 1, '', 0, 1, ''),
(11, 'Footer', '', 0, 'footer', 0, '0000-00-00 00:00:00', 1, 'mod_footer', 0, 0, 1, '', 1, 1, ''),
(12, 'Admin Menu', '', 1, 'menu', 0, '0000-00-00 00:00:00', 1, 'mod_menu', 0, 2, 1, '', 0, 1, ''),
(13, 'Admin SubMenu', '', 1, 'submenu', 0, '0000-00-00 00:00:00', 1, 'mod_submenu', 0, 2, 1, '', 0, 1, ''),
(14, 'User Status', '', 1, 'status', 0, '0000-00-00 00:00:00', 1, 'mod_status', 0, 2, 1, '', 0, 1, ''),
(15, 'Title', '', 1, 'title', 0, '0000-00-00 00:00:00', 1, 'mod_title', 0, 2, 1, '', 0, 1, ''),
(16, 'Polls', '', 3, 'right', 0, '0000-00-00 00:00:00', 1, 'mod_poll', 0, 0, 1, 'id=14\ncache=1', 0, 0, ''),
(17, 'User Menu', '', 5, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_mainmenu', 0, 0, 1, 'menutype=usermenu\nmenu_style=list\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=\nmoduleclass_sfx=_menu\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nmenu_images_link=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\n\n', 1, 0, ''),
(18, 'Login Form', '', 8, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_login', 0, 0, 1, 'cache=0\nmoduleclass_sfx=\npretext=\nposttext=\nlogin=\nlogout=\ngreeting=1\nname=0\nusesecure=0\n\n', 1, 0, ''),
(19, 'Latest News', '', 1, 'user1', 0, '0000-00-00 00:00:00', 1, 'mod_latestnews', 0, 0, 1, 'cache=1', 1, 0, ''),
(20, 'Statistics', '', 7, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_stats', 0, 0, 1, 'serverinfo=1\nsiteinfo=1\ncounter=1\nincrease=0\nmoduleclass_sfx=\ncache=0\ncache_time=900\n\n', 0, 0, ''),
(21, 'Who''s Online', '', 1, 'right', 0, '0000-00-00 00:00:00', 1, 'mod_whosonline', 0, 0, 1, 'cache=0\nshowmode=0\nmoduleclass_sfx=\n\n', 0, 0, ''),
(22, 'Popular', '', 1, 'user2', 0, '0000-00-00 00:00:00', 1, 'mod_mostread', 0, 0, 1, 'cache=1', 0, 0, ''),
(23, 'Archive', '', 9, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_archive', 0, 0, 1, 'cache=1', 1, 0, ''),
(24, 'Sections', '', 10, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_sections', 0, 0, 1, 'cache=1', 1, 0, ''),
(25, 'Newsflash', '', 1, 'top', 0, '0000-00-00 00:00:00', 1, 'mod_newsflash', 0, 0, 1, 'catid=3\r\nstyle=random\r\nitems=\r\nmoduleclass_sfx=', 0, 0, ''),
(26, 'Related Items', '', 11, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_related_items', 0, 0, 1, '', 0, 0, ''),
(27, 'Tìm kiếm', '', 1, 'user4', 0, '0000-00-00 00:00:00', 0, 'mod_search', 0, 0, 1, 'moduleclass_sfx=\nwidth=20\ntext=\nbutton=\nbutton_pos=right\nimagebutton=\nbutton_text=\nset_itemid=\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(28, 'Random Image', '', 2, 'right', 0, '0000-00-00 00:00:00', 1, 'mod_random_image', 0, 0, 1, 'type=jpg\nfolder=\nlink=\nwidth=\nheight=\nmoduleclass_sfx=\ncache=0\ncache_time=900\n\n', 0, 0, ''),
(29, 'Top Menu', '', 1, 'menu', 0, '0000-00-00 00:00:00', 0, 'mod_mainmenu', 0, 0, 0, 'menutype=topmenu\nmenu_style=list_flat\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=-nav\nmoduleclass_sfx=\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nmenu_images_link=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=-1\nindent_image2=-1\nindent_image3=-1\nindent_image4=-1\nindent_image5=-1\nindent_image6=-1\nspacer=\nend_spacer=\n\n', 1, 0, ''),
(30, 'Banners', '', 1, 'footer', 0, '0000-00-00 00:00:00', 1, 'mod_banners', 0, 0, 1, 'target=1\ncount=1\ncid=1\ncatid=33\ntag_search=0\nordering=random\nheader_text=\nfooter_text=\nmoduleclass_sfx=\ncache=1\ncache_time=15\n\n', 1, 0, ''),
(31, 'Resources', '', 3, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_mainmenu', 0, 0, 1, 'menutype=othermenu\nmenu_style=list\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=\nmoduleclass_sfx=_menu\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nmenu_images_link=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\n\n', 0, 0, ''),
(32, 'Wrapper', '', 2, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_wrapper', 0, 0, 1, 'moduleclass_sfx=\nurl=\nscrolling=auto\nwidth=100%\nheight=200\nheight_auto=1\nadd=1\ntarget=\ncache=0\ncache_time=900\n\n', 0, 0, ''),
(33, 'Footer', '', 2, 'footer', 0, '0000-00-00 00:00:00', 1, 'mod_footer', 0, 0, 1, 'cache=1\n\n', 1, 0, ''),
(34, 'Feed Display', '', 1, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_feed', 0, 0, 1, 'moduleclass_sfx=\nrssurl=\nrssrtl=0\nrsstitle=1\nrssdesc=1\nrssimage=1\nrssitems=3\nrssitemdesc=1\nword_count=0\ncache=0\ncache_time=15\n\n', 1, 0, ''),
(35, 'Breadcrumbs', '', 0, 'breadcrumb', 0, '0000-00-00 00:00:00', 0, 'mod_breadcrumbs', 0, 0, 1, 'showHome=1\nhomeText=Home\nshowLast=1\nseparator=\nmoduleclass_sfx=\ncache=0\n\n', 1, 0, ''),
(36, 'Syndication', '', 1, 'syndicate', 0, '0000-00-00 00:00:00', 1, 'mod_syndicate', 0, 0, 0, 'cache=0\ntext=Feed Entries\nformat=rss\nmoduleclass_sfx=\n\n', 1, 0, ''),
(38, 'Advertisement', '', 4, 'right', 0, '0000-00-00 00:00:00', 1, 'mod_banners', 0, 0, 1, 'count=4\r\nrandomise=0\r\ncid=0\r\ncatid=14\r\nheader_text=Featured Links:\r\nfooter_text=<a href="http://www.joomla.org">Ads by Joomla!</a>\r\nmoduleclass_sfx=_text\r\ncache=0\r\n\r\n', 0, 0, ''),
(39, 'Example Pages', '', 6, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_mainmenu', 0, 0, 1, 'cache=1\nclass_sfx=\nmoduleclass_sfx=_menu\nmenutype=ExamplePages\nmenu_style=list_flat\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nfull_active_id=0\nmenu_images=0\nmenu_images_align=0\nexpand_menu=0\nactivate_parent=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\nwindow_open=\n\n', 0, 0, ''),
(40, 'Key Concepts', '', 4, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_mainmenu', 0, 0, 1, 'cache=1\nclass_sfx=\nmoduleclass_sfx=_menu\nmenutype=keyconcepts\nmenu_style=list\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nfull_active_id=0\nmenu_images=0\nmenu_images_align=0\nexpand_menu=0\nactivate_parent=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\nwindow_open=\n\n', 0, 0, ''),
(41, 'Welcome to Joomla!', '<div style="padding: 5px">  <p>   Congratulations on choosing Joomla! as your content management system. To   help you get started, check out these excellent resources for securing your   server and pointers to documentation and other helpful resources. </p> <p>   <strong>Security</strong><br /> </p> <p>   On the Internet, security is always a concern. For that reason, you are   encouraged to subscribe to the   <a href="http://feedburner.google.com/fb/a/mailverify?uri=JoomlaSecurityNews" target="_blank">Joomla!   Security Announcements</a> for the latest information on new Joomla! releases,   emailed to you automatically. </p> <p>   If this is one of your first Web sites, security considerations may   seem complicated and intimidating. There are three simple steps that go a long   way towards securing a Web site: (1) regular backups; (2) prompt updates to the   <a href="http://www.joomla.org/download.html" target="_blank">latest Joomla! release;</a> and (3) a <a href="http://docs.joomla.org/Security_Checklist_2_-_Hosting_and_Server_Setup" target="_blank" title="good Web host">good Web host</a>. There are many other important security considerations that you can learn about by reading the <a href="http://docs.joomla.org/Category:Security_Checklist" target="_blank" title="Joomla! Security Checklist">Joomla! Security Checklist</a>. </p> <p>If you believe your Web site was attacked, or you think you have discovered a security issue in Joomla!, please do not post it in the Joomla! forums. Publishing this information could put other Web sites at risk. Instead, report possible security vulnerabilities to the <a href="http://developer.joomla.org/security/contact-the-team.html" target="_blank" title="Joomla! Security Task Force">Joomla! Security Task Force</a>.</p><p><strong>Learning Joomla!</strong> </p> <p>   A good place to start learning Joomla! is the   "<a href="http://docs.joomla.org/beginners" target="_blank">Absolute Beginner''s   Guide to Joomla!.</a>" There, you will find a Quick Start to Joomla!   <a href="http://help.joomla.org/ghop/feb2008/task048/joomla_15_quickstart.pdf" target="_blank">guide</a>   and <a href="http://help.joomla.org/ghop/feb2008/task167/index.html" target="_blank">video</a>,   amongst many other tutorials. The   <a href="http://community.joomla.org/magazine/view-all-issues.html" target="_blank">Joomla!   Community Magazine</a> also has   <a href="http://community.joomla.org/magazine/article/522-introductory-learning-joomla-using-sample-data.html" target="_blank">articles   for new learners</a> and experienced users, alike. A great place to look for   answers is the   <a href="http://docs.joomla.org/Category:FAQ" target="_blank">Frequently Asked   Questions (FAQ)</a>. If you are stuck on a particular screen in the   Administrator (which is where you are now), try clicking the Help toolbar   button to get assistance specific to that page. </p> <p>   If you still have questions, please feel free to use the   <a href="http://forum.joomla.org/" target="_blank">Joomla! Forums.</a> The forums   are an incredibly valuable resource for all levels of Joomla! users. Before   you post a question, though, use the forum search (located at the top of each   forum page) to see if the question has been asked and answered. </p> <p>   <strong>Getting Involved</strong> </p> <p>   <a name="twjs" title="twjs"></a> If you want to help make Joomla! better, consider getting   involved. There are   <a href="http://www.joomla.org/about-joomla/contribute-to-joomla.html" target="_blank">many ways   you can make a positive difference.</a> Have fun using Joomla!.</p></div>', 0, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 2, 1, 'moduleclass_sfx=\n\n', 1, 1, ''),
(42, 'Joomla! Security Newsfeed', '', 6, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_feed', 0, 0, 1, 'cache=1\ncache_time=15\nmoduleclass_sfx=\nrssurl=http://feeds.joomla.org/JoomlaSecurityNews\nrssrtl=0\nrsstitle=1\nrssdesc=0\nrssimage=1\nrssitems=1\nrssitemdesc=1\nword_count=0\n\n', 0, 1, ''),
(43, 'TÀI LIỆU BIỂU MẪU', '', 0, 'tin', 0, '0000-00-00 00:00:00', 1, 'mod_jabulletin', 0, 0, 1, 'type=latest\nmax=500\ndiv=tlbm_css\nmoduleclass_sfx=index.php?option=com_content&view=category&layout=blog&id=44&Itemid=44\ncategory=34\nwidth=0\nheight=0\nshow_front=1\nshow_image=1\nshow_date=1\ncountother=3\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(44, 'Đăng hồ sơ', '', 2, 'tin', 0, '0000-00-00 00:00:00', 1, 'mod_jabulletin', 0, 0, 1, 'type=latest\nmax=500\ndiv=dhs_css\nmoduleclass_sfx=index.php?option=com_user&view=login\ncategory=35\nwidth=0\nheight=0\nshow_front=1\nshow_image=1\nshow_date=1\ncountother=3\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(45, 'Thông báo việc làm', '', 3, 'tin', 0, '0000-00-00 00:00:00', 1, 'mod_jabulletin', 0, 0, 1, 'type=latest\nmax=500\ndiv=tbvl_css\nmoduleclass_sfx=index.php?option=com_user&view=login\ncategory=36\nwidth=0\nheight=0\nshow_front=1\nshow_image=1\nshow_date=1\ncountother=3\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(46, 'Nhật ký tìm việc làm', '', 4, 'tin', 0, '0000-00-00 00:00:00', 1, 'mod_jabulletin', 0, 0, 1, 'type=latest\nmax=500\ndiv=nktv_css\nmoduleclass_sfx=index.php?option=com_user&view=login\ncategory=38\nwidth=0\nheight=0\nshow_front=1\nshow_image=1\nshow_date=1\ncountother=3\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(47, ' Tư vấn nghề nghiệp', '', 0, 'bottom_tin', 0, '0000-00-00 00:00:00', 1, 'mod_jabulletin2', 0, 0, 1, 'type=latest\ntitle= Tư vấn nghề nghiệp\nmax=250\nmoduleclass_sfx=\ncategory=39\nwidth=162\nheight=98\nshow_front=1\nshow_image=1\nshow_date=1\ncountother=3\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(48, 'Trắc nghiệm tình huống phỏng vấn và chia sẻ trải nghiệm', '', 2, 'bottom_tin', 0, '0000-00-00 00:00:00', 1, 'mod_jabulletin4', 0, 0, 1, 'type=latest\ntitle=Trắc nghiệm tình huống phỏng vấn và chia sẻ trải nghiệm\nmax=200\nmoduleclass_sfx=\ncategory=40\ncount=5\nwidth=0\nheight=0\nshow_front=1\nshow_image=1\nshow_date=1\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(49, 'Việc Làm tại công ty', '', 3, 'leftbar', 0, '0000-00-00 00:00:00', 1, 'mod_product', 0, 0, 1, 'title=Việc Làm tại công ty\ndiv=\ndiv1=box-left\ndiv2=vieclam-company\ncount=20\nwhotline=50\nhhotline=15\nshow=1\ncategory=77\n\n', 0, 0, ''),
(50, 'Quảng Cáo Phải', '', 1, 'rightbar', 0, '0000-00-00 00:00:00', 1, 'mod_product', 0, 0, 1, 'title=\ndiv=\ndiv1=box-right\ndiv2=quangcao-company\ncount=10\nwhotline=178\nhhotline=115\nshow=1\ncategory=78\n\n', 0, 0, ''),
(54, 'mod_demo', '', 2, 'leftbar', 0, '0000-00-00 00:00:00', 0, 'mod_demo', 0, 0, 1, 'nametitle=\ncount=5\nwhotline=25\nhhotline=15\nshow=1\ncategory=0\n\n', 0, 0, ''),
(51, 'Việc Làm Tốt Nhất', '', 0, 'center', 0, '0000-00-00 00:00:00', 1, 'mod_crjob_best', 0, 0, 1, 'title=Việc Làm Tốt Nhất\ncount=100\n\n', 0, 0, ''),
(52, ' Các ngành nghề hot nhất', '', 2, 'hot', 0, '0000-00-00 00:00:00', 1, 'mod_crjob_hot', 0, 0, 1, 'nametitle= Các ngành nghề hot nhất\ncount=12\n\n', 0, 0, ''),
(53, 'Custom', '<p>Bản quyền © 2010 . <a href="http://www.vieclamviet.vn" title="Tuyển dụng">Tuyển dụng</a>, <a href="http://www.vieclamviet.vn" title="tìm việc làm">tìm việc làm</a> - Công ty TNHH Nhân Lực Việt Bắc Tel: (84-4) 23 228 999. Fax: (84-4) 3646 2527. Email: <a href="mailto:contact@vieclamviet.com.vn">contact@vieclamviet.com.vn</a></p>', 0, 'custom', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(55, 'ZVzxbzxbzxbz', '', 1, 'hot', 0, '0000-00-00 00:00:00', 0, 'mod_login', 0, 0, 1, 'cache=0\nmoduleclass_sfx=\npretext=\nposttext=\nlogin=\nlogout=\ngreeting=1\nname=0\nusesecure=0\n\n', 0, 0, ''),
(56, 'mod_search_left', '', 0, 'leftbar', 0, '0000-00-00 00:00:00', 1, 'mod_search_left', 0, 0, 1, 'items=\nmoduleclass_sfx=\ncache=0\ncache_time=900\n\n', 0, 0, ''),
(57, 'mod_cr_footer', '', 2, 'menu', 0, '0000-00-00 00:00:00', 1, 'mod_cr_footer', 0, 0, 1, 'menutype=topmenu\nseecker=29\nemploy=18\n\n', 0, 0, ''),
(58, 'flash_banner', '<p><img src="images/stories/banner.jpg" border="0" /></p>', 0, 'banner_flash', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(59, 'mod count job', '', 12, 'job_count', 0, '0000-00-00 00:00:00', 1, 'mod_count_job', 0, 0, 1, 'name_count=Việc làm đăng tuyển\ncache=0\ncache_time=900\n\n', 0, 0, ''),
(60, 'mod_search_ntd', '', 0, 'leftbar', 0, '0000-00-00 00:00:00', 1, 'mod_search_ntd', 0, 0, 1, 'items=\nmoduleclass_sfx=\ncache=0\ncache_time=900\n\n', 0, 0, ''),
(61, 'sh404sef control panel icon', '', 9, 'icon', 0, '0000-00-00 00:00:00', 1, 'mod_sh404sef_cpicon', 0, 2, 1, '', 0, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_modules_menu`
--

CREATE TABLE IF NOT EXISTS `jos_modules_menu` (
  `moduleid` int(11) NOT NULL default '0',
  `menuid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`moduleid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_modules_menu`
--

INSERT INTO `jos_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 0),
(16, 1),
(17, 0),
(18, 1),
(19, 1),
(19, 2),
(19, 4),
(19, 27),
(19, 36),
(21, 1),
(22, 1),
(22, 2),
(22, 4),
(22, 27),
(22, 36),
(25, 0),
(27, 0),
(28, 1),
(28, 11),
(29, 0),
(30, 0),
(31, 1),
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(38, 1),
(39, 43),
(39, 44),
(39, 45),
(39, 46),
(39, 47),
(40, 0),
(43, 0),
(44, 0),
(45, 0),
(46, 0),
(47, 1),
(47, 11),
(48, 1),
(48, 11),
(49, 0),
(50, 1),
(50, 11),
(50, 28),
(51, 1),
(51, 11),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0),
(59, 0),
(60, 0),
(61, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_newsfeeds`
--

CREATE TABLE IF NOT EXISTS `jos_newsfeeds` (
  `catid` int(11) NOT NULL default '0',
  `id` int(11) NOT NULL auto_increment,
  `name` text NOT NULL,
  `alias` varchar(255) NOT NULL default '',
  `link` text NOT NULL,
  `filename` varchar(200) default NULL,
  `published` tinyint(1) NOT NULL default '0',
  `numarticles` int(11) unsigned NOT NULL default '1',
  `cache_time` int(11) unsigned NOT NULL default '3600',
  `checked_out` tinyint(3) unsigned NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL default '0',
  `rtl` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `published` (`published`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `jos_newsfeeds`
--

INSERT INTO `jos_newsfeeds` (`catid`, `id`, `name`, `alias`, `link`, `filename`, `published`, `numarticles`, `cache_time`, `checked_out`, `checked_out_time`, `ordering`, `rtl`) VALUES
(4, 1, 'Joomla! Announcements', 'joomla-official-news', 'http://feeds.joomla.org/JoomlaAnnouncements', '', 1, 1, 3600, 0, '0000-00-00 00:00:00', 0, 0),
(4, 2, 'Joomla! Core Team Blog', 'joomla-core-team-blog', 'http://feeds.joomla.org/JoomlaCommunityCoreTeamBlog', '', 1, 5, 3600, 0, '0000-00-00 00:00:00', 2, 0),
(4, 3, 'Joomla! Community Magazine', 'joomla-community-magazine', 'http://feeds.joomla.org/JoomlaMagazine', '', 1, 20, 3600, 0, '0000-00-00 00:00:00', 3, 0),
(4, 4, 'Joomla! Developer News', 'joomla-developer-news', 'http://feeds.joomla.org/JoomlaDeveloper', '', 1, 5, 3600, 0, '0000-00-00 00:00:00', 4, 0),
(4, 5, 'Joomla! Security News', 'joomla-security-news', 'http://feeds.joomla.org/JoomlaSecurityNews', '', 1, 5, 3600, 0, '0000-00-00 00:00:00', 5, 0),
(5, 6, 'Free Software Foundation Blogs', 'free-software-foundation-blogs', 'http://www.fsf.org/blogs/RSS', NULL, 0, 5, 3600, 0, '0000-00-00 00:00:00', 4, 0),
(5, 7, 'Free Software Foundation', 'free-software-foundation', 'http://www.fsf.org/news/RSS', NULL, 0, 5, 3600, 0, '0000-00-00 00:00:00', 3, 0),
(5, 8, 'Software Freedom Law Center Blog', 'software-freedom-law-center-blog', 'http://www.softwarefreedom.org/feeds/blog/', NULL, 0, 5, 3600, 0, '0000-00-00 00:00:00', 2, 0),
(5, 9, 'Software Freedom Law Center News', 'software-freedom-law-center', 'http://www.softwarefreedom.org/feeds/news/', NULL, 0, 5, 3600, 0, '0000-00-00 00:00:00', 1, 0),
(5, 10, 'Open Source Initiative Blog', 'open-source-initiative-blog', 'http://www.opensource.org/blog/feed', NULL, 0, 5, 3600, 0, '0000-00-00 00:00:00', 5, 0),
(6, 11, 'PHP News and Announcements', 'php-news-and-announcements', 'http://www.php.net/feed.atom', NULL, 0, 5, 3600, 0, '0000-00-00 00:00:00', 1, 0),
(6, 12, 'Planet MySQL', 'planet-mysql', 'http://www.planetmysql.org/rss20.xml', NULL, 0, 5, 3600, 0, '0000-00-00 00:00:00', 2, 0),
(6, 13, 'Linux Foundation Announcements', 'linux-foundation-announcements', 'http://www.linuxfoundation.org/press/rss20.xml', NULL, 0, 5, 3600, 0, '0000-00-00 00:00:00', 3, 0),
(6, 14, 'Mootools Blog', 'mootools-blog', 'http://feeds.feedburner.com/mootools-blog', NULL, 0, 5, 3600, 0, '0000-00-00 00:00:00', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_plugins`
--

CREATE TABLE IF NOT EXISTS `jos_plugins` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL default '',
  `element` varchar(100) NOT NULL default '',
  `folder` varchar(100) NOT NULL default '',
  `access` tinyint(3) unsigned NOT NULL default '0',
  `ordering` int(11) NOT NULL default '0',
  `published` tinyint(3) NOT NULL default '0',
  `iscore` tinyint(3) NOT NULL default '0',
  `client_id` tinyint(3) NOT NULL default '0',
  `checked_out` int(11) unsigned NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `params` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_folder` (`published`,`client_id`,`access`,`folder`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `jos_plugins`
--

INSERT INTO `jos_plugins` (`id`, `name`, `element`, `folder`, `access`, `ordering`, `published`, `iscore`, `client_id`, `checked_out`, `checked_out_time`, `params`) VALUES
(1, 'Authentication - Joomla', 'joomla', 'authentication', 0, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(2, 'Authentication - LDAP', 'ldap', 'authentication', 0, 2, 0, 1, 0, 0, '0000-00-00 00:00:00', 'host=\nport=389\nuse_ldapV3=0\nnegotiate_tls=0\nno_referrals=0\nauth_method=bind\nbase_dn=\nsearch_string=\nusers_dn=\nusername=\npassword=\nldap_fullname=fullName\nldap_email=mail\nldap_uid=uid\n\n'),
(3, 'Authentication - GMail', 'gmail', 'authentication', 0, 4, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(4, 'Authentication - OpenID', 'openid', 'authentication', 0, 3, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(5, 'User - Joomla!', 'joomla', 'user', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'autoregister=1\n\n'),
(6, 'Search - Content', 'content', 'search', 0, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\nsearch_content=1\nsearch_uncategorised=1\nsearch_archived=1\n\n'),
(7, 'Search - Contacts', 'contacts', 'search', 0, 3, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(8, 'Search - Categories', 'categories', 'search', 0, 4, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(9, 'Search - Sections', 'sections', 'search', 0, 5, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(10, 'Search - Newsfeeds', 'newsfeeds', 'search', 0, 6, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(11, 'Search - Weblinks', 'weblinks', 'search', 0, 2, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(12, 'Content - Pagebreak', 'pagebreak', 'content', 0, 10000, 1, 1, 0, 0, '0000-00-00 00:00:00', 'enabled=1\ntitle=1\nmultipage_toc=1\nshowall=1\n\n'),
(13, 'Content - Rating', 'vote', 'content', 0, 4, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(14, 'Content - Email Cloaking', 'emailcloak', 'content', 0, 5, 1, 0, 0, 0, '0000-00-00 00:00:00', 'mode=1\n\n'),
(15, 'Content - Code Hightlighter (GeSHi)', 'geshi', 'content', 0, 5, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(16, 'Content - Load Module', 'loadmodule', 'content', 0, 6, 1, 0, 0, 0, '0000-00-00 00:00:00', 'enabled=1\nstyle=0\n\n'),
(17, 'Content - Page Navigation', 'pagenavigation', 'content', 0, 2, 1, 1, 0, 0, '0000-00-00 00:00:00', 'position=1\n\n'),
(18, 'Editor - No Editor', 'none', 'editors', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(19, 'Editor - TinyMCE', 'tinymce', 'editors', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 'mode=extended\nskin=0\ncompressed=0\ncleanup_startup=0\ncleanup_save=2\nentity_encoding=raw\nlang_mode=0\nlang_code=en\ntext_direction=ltr\ncontent_css=1\ncontent_css_custom=\nrelative_urls=1\nnewlines=0\ninvalid_elements=applet\nextended_elements=\ntoolbar=top\ntoolbar_align=left\nhtml_height=550\nhtml_width=750\nelement_path=1\nfonts=1\npaste=1\nsearchreplace=1\ninsertdate=1\nformat_date=%Y-%m-%d\ninserttime=1\nformat_time=%H:%M:%S\ncolors=1\ntable=1\nsmilies=1\nmedia=1\nhr=1\ndirectionality=1\nfullscreen=1\nstyle=1\nlayer=1\nxhtmlxtras=1\nvisualchars=1\nnonbreaking=1\nblockquote=1\ntemplate=0\nadvimage=1\nadvlink=1\nautosave=1\ncontextmenu=1\ninlinepopups=1\nsafari=1\ncustom_plugin=\ncustom_button=\n\n'),
(20, 'Editor - XStandard Lite 2.0', 'xstandard', 'editors', 0, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(21, 'Editor Button - Image', 'image', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(22, 'Editor Button - Pagebreak', 'pagebreak', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(23, 'Editor Button - Readmore', 'readmore', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(24, 'XML-RPC - Joomla', 'joomla', 'xmlrpc', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(25, 'XML-RPC - Blogger API', 'blogger', 'xmlrpc', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', 'catid=1\nsectionid=0\n\n'),
(27, 'System - SEF', 'sef', 'system', 0, 1, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(28, 'System - Debug', 'debug', 'system', 0, 2, 1, 0, 0, 0, '0000-00-00 00:00:00', 'queries=1\nmemory=1\nlangauge=1\n\n'),
(29, 'System - Legacy', 'legacy', 'system', 0, 3, 0, 1, 0, 0, '0000-00-00 00:00:00', 'route=0\n\n'),
(30, 'System - Cache', 'cache', 'system', 0, 4, 0, 1, 0, 0, '0000-00-00 00:00:00', 'browsercache=0\ncachetime=15\n\n'),
(31, 'System - Log', 'log', 'system', 0, 5, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(32, 'System - Remember Me', 'remember', 'system', 0, 6, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(33, 'System - Backlink', 'backlink', 'system', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(34, 'System - Mootools Upgrade', 'mtupgrade', 'system', 0, 8, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(35, 'Content - Relate Article', 'relatearticle', 'content', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'act=onPrepareContent\ntitle_text=Các bài viết khác\nshowdate=1\ndateposition=0\ndateformat=%d-%m-%Y\ndatetype=created\narticle_num=10\ncss_path=\n\n'),
(36, 'J16 Language backport - system - plugin', 'shjlang16', 'system', 0, 10, 1, 0, 0, 0, '0000-00-00 00:00:00', 'plugin_element=shjlang16\nplugin_folder=system\n'),
(37, 'sh404sef - System mobile template switcher', 'shmobile', 'system', 0, 10, 1, 0, 0, 0, '0000-00-00 00:00:00', 'plugin_element=shmobile\nplugin_folder=system\n'),
(38, 'sh404sef - System plugin', 'shsef', 'system', 0, 10, 1, 0, 0, 0, '0000-00-00 00:00:00', 'plugin_element=shsef\nplugin_folder=system\n'),
(39, 'sh404sef - Analytics plugin', 'sh404sefanalytics', 'sh404sefcore', 0, 10, 1, 0, 0, 0, '0000-00-00 00:00:00', 'plugin_element=sh404sefanalytics\nplugin_folder=sh404sefcore\nplugin_event=onShInsertAnalyticsSnippet\nplugin_functions=plgSh404sefAnalyticsCustomVars\n'),
(40, 'sh404sef - Offline code plugin', 'sh404sefofflinecode', 'sh404sefcore', 0, 10, 1, 0, 0, 0, '0000-00-00 00:00:00', 'retry_after_delay=7400\nplugin_element=sh404sefofflinecode\nplugin_folder=sh404sefcore\nplugin_event=onAfterDispatch\nplugin_functions=plgSh404sefofflinecode\n'),
(41, 'sh404sef - Similar urls plugin', 'sh404sefsimilarurls', 'sh404sefcore', 0, 10, 1, 0, 0, 0, '0000-00-00 00:00:00', 'max_number_of_urls=5\nmin_segment_length=3\nexcluded_words_sef=__404__\nplugin_element=sh404sefsimilarurls\nplugin_folder=sh404sefcore\nplugin_event=onPrepareContent\nplugin_functions=plgSh404sefsimilarurls\n');

-- --------------------------------------------------------

--
-- Table structure for table `jos_polls`
--

CREATE TABLE IF NOT EXISTS `jos_polls` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `alias` varchar(255) NOT NULL default '',
  `voters` int(9) NOT NULL default '0',
  `checked_out` int(11) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL default '0',
  `access` int(11) NOT NULL default '0',
  `lag` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `jos_polls`
--

INSERT INTO `jos_polls` (`id`, `title`, `alias`, `voters`, `checked_out`, `checked_out_time`, `published`, `access`, `lag`) VALUES
(14, 'Joomla! is used for?', 'joomla-is-used-for', 11, 62, '2010-12-26 02:33:50', 1, 0, 86400);

-- --------------------------------------------------------

--
-- Table structure for table `jos_poll_data`
--

CREATE TABLE IF NOT EXISTS `jos_poll_data` (
  `id` int(11) NOT NULL auto_increment,
  `pollid` int(11) NOT NULL default '0',
  `text` text NOT NULL,
  `hits` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `pollid` (`pollid`,`text`(1))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `jos_poll_data`
--

INSERT INTO `jos_poll_data` (`id`, `pollid`, `text`, `hits`) VALUES
(1, 14, 'Community Sites', 2),
(2, 14, 'Public Brand Sites', 3),
(3, 14, 'eCommerce', 1),
(4, 14, 'Blogs', 0),
(5, 14, 'Intranets', 0),
(6, 14, 'Photo and Media Sites', 2),
(7, 14, 'All of the Above!', 3),
(8, 14, '', 0),
(9, 14, '', 0),
(10, 14, '', 0),
(11, 14, '', 0),
(12, 14, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_poll_date`
--

CREATE TABLE IF NOT EXISTS `jos_poll_date` (
  `id` bigint(20) NOT NULL auto_increment,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `vote_id` int(11) NOT NULL default '0',
  `poll_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `poll_id` (`poll_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `jos_poll_date`
--

INSERT INTO `jos_poll_date` (`id`, `date`, `vote_id`, `poll_id`) VALUES
(1, '2006-10-09 13:01:58', 1, 14),
(2, '2006-10-10 15:19:43', 7, 14),
(3, '2006-10-11 11:08:16', 7, 14),
(4, '2006-10-11 15:02:26', 2, 14),
(5, '2006-10-11 15:43:03', 7, 14),
(6, '2006-10-11 15:43:38', 7, 14),
(7, '2006-10-12 00:51:13', 2, 14),
(8, '2007-05-10 19:12:29', 3, 14),
(9, '2007-05-14 14:18:00', 6, 14),
(10, '2007-06-10 15:20:29', 6, 14),
(11, '2007-07-03 12:37:53', 2, 14);

-- --------------------------------------------------------

--
-- Table structure for table `jos_poll_menu`
--

CREATE TABLE IF NOT EXISTS `jos_poll_menu` (
  `pollid` int(11) NOT NULL default '0',
  `menuid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`pollid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_poll_menu`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_product`
--

CREATE TABLE IF NOT EXISTS `jos_product` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) collate utf8_unicode_ci NOT NULL,
  `alias` varchar(255) collate utf8_unicode_ci NOT NULL,
  `email` varchar(255) collate utf8_unicode_ci NOT NULL,
  `website` varchar(255) collate utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `title` text collate utf8_unicode_ci NOT NULL,
  `description` text collate utf8_unicode_ci NOT NULL,
  `content` text collate utf8_unicode_ci NOT NULL,
  `categories` varchar(500) collate utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `img_thump` varchar(500) collate utf8_unicode_ci NOT NULL,
  `img_original` varchar(500) collate utf8_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL,
  `metadesc` text collate utf8_unicode_ci NOT NULL,
  `metakey` text collate utf8_unicode_ci NOT NULL,
  `published` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

--
-- Dumping data for table `jos_product`
--

INSERT INTO `jos_product` (`id`, `name`, `alias`, `email`, `website`, `phone`, `title`, `description`, `content`, `categories`, `created`, `img_thump`, `img_original`, `ordering`, `metadesc`, `metakey`, `published`, `state`) VALUES
(11, '', 'Vibama', '', 'http://http://http://vieclamviet.vn', 0, 'VIBAMA', '', '', '77', '0000-00-00 00:00:00', 'http://vieclamviet.com.vn/images/resized/images/stories/danh cho quang cao_70_90.jpg', 'images/stories/danh cho quang cao.jpg', 0, '', '', 1, 0),
(12, '', 'Vinaland', '', 'http://vieclamviet.com.vn/index.php?option=com_newsfeeds&view=categories&Itemid=49', 0, 'Quang cao', '', '', '77', '0000-00-00 00:00:00', 'http://vieclamviet.com.vn/images/resized/images/stories/danh cho quang cao_70_90.jpg', 'images/stories/danh cho quang cao.jpg', 0, '', '', 1, 0),
(13, '', 'Pv', '', 'http://vieclamviet.com.vn', 0, 'Pv', '', '', '77', '0000-00-00 00:00:00', 'http://vieclamviet.com.vn/images/resized/images/stories/danh cho quang cao_70_90.jpg', 'images/stories/danh cho quang cao.jpg', 0, '', '', 1, 0),
(14, '', 'Cát tường', '', 'http://vieclamviet.com.vn', 0, 'Cát tường', '', '', '77', '0000-00-00 00:00:00', 'http://vieclamviet.com.vn/images/resized/images/stories/danh cho quang cao_70_90.jpg', 'images/stories/danh cho quang cao.jpg', 0, '', '', 1, 0),
(15, '', 'TT thông tin Di động', '', '', 0, 'TT thông tin Di động', '', '', '77', '0000-00-00 00:00:00', 'http://vieclamviet.com.vn/images/resized/images/stories/danh cho quang cao_70_90.jpg', 'images/stories/danh cho quang cao.jpg', 0, '', '', 1, 0),
(16, '', 'Đường Minh', '', 'http://creativevietnam.vn', 0, 'Đường Minh', '', '', '77', '0000-00-00 00:00:00', 'http://localhost/jobs_plazza_quickstart/images/resized/images/stories/company6_97_52.jpg', 'images/stories/company6.jpg', 0, '', '', 1, 0),
(17, '', 'Pnat', '', 'http://creativevietnam.vn', 0, 'Pnat', '', '', '77', '0000-00-00 00:00:00', 'http://localhost/jobs_plazza_quickstart/images/resized/images/stories/company7_97_52.jpg', 'images/stories/company7.jpg', 0, '', '', 1, 0),
(18, '', 'vinastar', '', 'http://creativevietnam.vn', 0, 'vinastar', '', '', '77', '0000-00-00 00:00:00', 'http://localhost/jobs_plazza_quickstart/images/resized/images/stories/company8_97_52.jpg', 'images/stories/company8.jpg', 0, '', '', 1, 0),
(19, '', 'Ctc', '', 'http://creativevietnam.vn', 0, 'Ctc', '', '', '77', '0000-00-00 00:00:00', 'http://localhost/jobs_plazza_quickstart/images/resized/images/stories/company9_97_52.jpg', 'images/stories/company9.jpg', 0, '', '', 1, 0),
(20, '', 'Đông dương', '', 'http://creativevietnam.vn', 0, 'Đông dương', '', '', '77', '0000-00-00 00:00:00', 'http://localhost/jobs_plazza_quickstart/images/resized/images/stories/company10_97_52.jpg', 'images/stories/company10.jpg', 0, '', '', 1, 0),
(21, '', 'Quảng cáo 1', '', '', 0, 'Quảng cáo 1', '', '', '78', '0000-00-00 00:00:00', 'http://vieclamviet.com.vn/images/resized/images/stories/danh cho quang cao_70_90.jpg', 'images/stories/danh cho quang cao.jpg', 0, '', '', 1, 0),
(22, '', 'Quảng cáo 2', '', 'http://', 0, 'Quảng cáo 2', '', '', '78', '0000-00-00 00:00:00', 'http://vieclamviet.com.vn/images/resized/images/stories/danh cho quang cao_70_90.jpg', 'images/stories/danh cho quang cao.jpg', 0, '', '', 1, 0),
(23, '', 'Quảng Cáo 3', '', 'http://', 0, 'Quảng Cáo 3', '', '', '78', '0000-00-00 00:00:00', 'http://vieclamviet.com.vn/images/resized/images/stories/danh cho quang cao_70_90.jpg', 'images/stories/danh cho quang cao.jpg', 0, '', '', 1, 0),
(24, '', 'Quảng cáo 4', '', '', 0, 'Quảng cáo 4', '', '', '78', '0000-00-00 00:00:00', 'http://vieclamviet.com.vn/images/resized/images/stories/danh cho quang cao_70_90.jpg', 'images/stories/danh cho quang cao.jpg', 0, '', '', 1, 0),
(25, '', 'Quảng cáo 5', '', '', 0, 'Quảng cáo 5', '', '', '78', '0000-00-00 00:00:00', 'http://localhost/jobs_plazza_quickstart/images/resized/images/stories/qc_left5_160_79.jpg', 'images/stories/qc_left5.jpg', 0, '', '', 0, 0),
(26, '', 'nhanluc', '', 'http://nhanlucvietbac.com.vn', 0, 'Nhân lực Việt Bắc', '', '', '78', '0000-00-00 00:00:00', '', '', 0, '', '', 0, 0),
(27, '', 'quang cao anh dong', '', 'http://vieclamviet.vn/index.php?option=com_content&view=article&id=59', 123654789, 'QC dich vu osin', '', '', '78', '0000-00-00 00:00:00', 'http://vieclamviet.com.vn/images/resized/images/stories/ccosin_107_90.gif', 'images/stories/ccosin.gif', 0, '', '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_available_product`
--

CREATE TABLE IF NOT EXISTS `jos_properties_available_product` (
  `id` int(11) NOT NULL auto_increment,
  `id_product` int(11) NOT NULL,
  `date` date NOT NULL,
  `available` tinyint(4) NOT NULL,
  `published` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_properties_available_product`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_buypacket`
--

CREATE TABLE IF NOT EXISTS `jos_properties_buypacket` (
  `id` int(11) NOT NULL auto_increment,
  `agent_id` int(11) NOT NULL,
  `name_packet` varchar(255) collate utf8_unicode_ci NOT NULL,
  `type_packet` varchar(50) collate utf8_unicode_ci NOT NULL,
  `start_datebuy` date NOT NULL,
  `end_datebuy` date NOT NULL,
  `published` tinyint(2) NOT NULL,
  `checkbuy` tinyint(3) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Dumping data for table `jos_properties_buypacket`
--

INSERT INTO `jos_properties_buypacket` (`id`, `agent_id`, `name_packet`, `type_packet`, `start_datebuy`, `end_datebuy`, `published`, `checkbuy`) VALUES
(18, 98, 'goicv', '', '2010-12-09', '0000-00-00', 0, 1),
(17, 112, 'goicv', '40', '2010-11-26', '2010-11-30', 1, 1),
(16, 112, 'goicv', '90', '2010-11-26', '2010-11-30', 1, 2),
(19, 111, 'goicv', '40', '2010-11-30', '2010-11-25', 1, 1),
(20, 165, 'goicv', '40', '2010-12-01', '0000-00-00', 0, 3),
(21, 0, 'goicv', '60', '2010-12-08', '0000-00-00', 0, 3),
(22, 216, 'goicv', '40', '2010-12-09', '0000-00-00', 1, 1),
(23, 106, 'goicv', '40', '2010-12-16', '0000-00-00', 1, 1),
(24, 228, 'goicv', '', '2010-12-28', '0000-00-00', 0, 3),
(25, 242, 'goicv', '40', '2011-01-19', '2011-01-20', 1, 1),
(26, 244, 'goicv', '40', '2011-01-23', '0000-00-00', 0, 3),
(27, 83, 'goicv', '90', '2011-01-30', '0000-00-00', 0, 3),
(28, 270, 'goicv', '60', '2011-02-15', '2011-02-25', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_category`
--

CREATE TABLE IF NOT EXISTS `jos_properties_category` (
  `id` int(2) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `alias` varchar(100) default NULL,
  `parent` int(2) default NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=84 ;

--
-- Dumping data for table `jos_properties_category`
--

INSERT INTO `jos_properties_category` (`id`, `name`, `alias`, `parent`, `published`, `ordering`) VALUES
(4, 'Truyền thông', 'truyn-thong', 6, 1, 2),
(3, 'Xây Dựng', 'xay-dng', 6, 1, 1),
(5, 'Sản xuất', 'sn-xut', 6, 1, 3),
(6, 'Theo Ngành Nghề', 'theo-nganh-ngh', 0, 1, 0),
(7, 'Theo Chức Năng', 'theo-chc-nng', 0, 1, 1),
(8, 'Theo Đối Tượng', 'theo-i-tng', 0, 1, 3),
(9, 'Giao Dịch Khách Hàng', 'giao-dch-khach-hang', 7, 1, 1),
(20, 'Dịch vụ tài chính', 'dch-v-tai-chinh', 6, 1, 0),
(33, 'Vận tải', 'vn-ti', 6, 1, 0),
(36, 'Hàng tiêu dùng', 'hang-tieu-dung', 6, 1, 0),
(39, 'Kỹ Thuật', 'k-thut', 6, 1, 0),
(44, 'Khách sạn & Du lịch', 'khach-sn-a-du-lch', 6, 1, 0),
(47, 'Dịch vụ', 'dch-v', 6, 1, 0),
(52, 'Bán lẻ', 'ban-l', 6, 1, 0),
(56, 'Hỗ trợ sản xuất', 'h-tr-sn-xut', 7, 1, 0),
(78, 'Kỹ thuật - Công nghệ', 'k-thut-cong-ngh', 7, 1, 0),
(79, 'Bộ Phận Hỗ trợ', 'b-phn-h-tr', 7, 1, 0),
(72, 'Khác', 'khac', 7, 1, 0),
(73, 'Overseas Jobs ', 'overseas-jobs-', 8, 1, 0),
(74, 'Người nước ngoài/Việt Kiều', 'ngi-nc-ngoaivit-kiu', 8, 1, 0),
(75, 'Cấp quản lý điều hành', 'cp-qun-ly-iu-hanh', 8, 1, 0),
(76, 'Mới tốt nghiệp', 'mi-tt-nghip', 8, 1, 0),
(77, 'Thời vụ/Hợp đồng ngắn hạn ', 'thi-vhp-ng-ngn-hn-', 8, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_country`
--

CREATE TABLE IF NOT EXISTS `jos_properties_country` (
  `id` int(3) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  `checked_out` tinyint(1) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `jos_properties_country`
--

INSERT INTO `jos_properties_country` (`id`, `name`, `alias`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(1, 'Viet Nam', 'viet-nam', 1, 0, 0, '0000-00-00 00:00:00'),
(2, 'France', 'france', 1, 0, 0, '0000-00-00 00:00:00'),
(3, 'United States ', 'united-states-', 1, 0, 0, '0000-00-00 00:00:00'),
(4, 'Australia', 'australia', 1, 0, 0, '0000-00-00 00:00:00'),
(6, 'Canada', 'canada', 1, 0, 0, '0000-00-00 00:00:00'),
(7, 'China ', 'china-', 1, 0, 0, '0000-00-00 00:00:00'),
(13, 'Thailand', 'thailand', 1, 0, 0, '0000-00-00 00:00:00'),
(14, 'Malaysia', 'malaysia', 1, 0, 0, '0000-00-00 00:00:00'),
(15, 'Indonesia', 'indonesia', 1, 0, 0, '0000-00-00 00:00:00'),
(16, 'Japan', 'japan', 1, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_education`
--

CREATE TABLE IF NOT EXISTS `jos_properties_education` (
  `id` int(3) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  `checked_out` tinyint(1) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `jos_properties_education`
--

INSERT INTO `jos_properties_education` (`id`, `name`, `alias`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(11, 'Trung cấp', 'trung-cp', 1, 4, 0, '0000-00-00 00:00:00'),
(12, 'Cao đẳng', 'bng-cao-ng', 1, 3, 0, '0000-00-00 00:00:00'),
(13, 'Đại học', 'bng-i-hc', 1, 2, 0, '0000-00-00 00:00:00'),
(14, 'Trên đại học', 'bng-tren-i-hc', 1, 1, 0, '0000-00-00 00:00:00'),
(15, 'Trung học', 'trung-hc', 1, 5, 0, '0000-00-00 00:00:00'),
(16, 'Lao động phổ thông', 'lao-ng-ph-thong', 1, 6, 0, '0000-00-00 00:00:00'),
(17, 'Không yêu cầu', 'khong-yeu-cu', 1, 7, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_images`
--

CREATE TABLE IF NOT EXISTS `jos_properties_images` (
  `id` int(5) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `parent` int(4) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(4) NOT NULL,
  `type` varchar(100) NOT NULL,
  `path` varchar(255) NOT NULL,
  `rout` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `uid` int(5) NOT NULL,
  `product_id` int(5) NOT NULL,
  `sector` varchar(255) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_properties_images`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_jobtime`
--

CREATE TABLE IF NOT EXISTS `jos_properties_jobtime` (
  `id` int(3) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  `checked_out` tinyint(1) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `jos_properties_jobtime`
--

INSERT INTO `jos_properties_jobtime` (`id`, `name`, `alias`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(11, 'Toàn thời gian cố định', 'toan-thi-gian-c-nh', 1, 0, 0, '0000-00-00 00:00:00'),
(12, 'Làm bán thời gian', 'lam-ban-thi-gian', 1, 0, 0, '0000-00-00 00:00:00'),
(13, 'Làm thêm giờ', 'lam-them-gi', 1, 0, 0, '0000-00-00 00:00:00'),
(14, 'Làm theo ca ', 'lamtheoca', 1, 0, 0, '0000-00-00 00:00:00'),
(15, 'Thực tập', 'thc-tp', 1, 0, 0, '0000-00-00 00:00:00'),
(16, 'Khác', 'khac', 1, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_lightbox`
--

CREATE TABLE IF NOT EXISTS `jos_properties_lightbox` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL default '0',
  `propid` int(11) NOT NULL default '0',
  `date` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_properties_lightbox`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_packet`
--

CREATE TABLE IF NOT EXISTS `jos_properties_packet` (
  `id` int(3) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `price_packet` float NOT NULL,
  `list_type` varchar(20) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  `checked_out` tinyint(1) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `jos_properties_packet`
--

INSERT INTO `jos_properties_packet` (`id`, `name`, `alias`, `price_packet`, `list_type`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(13, 'Gói việc làm', 'goi-vic-lam', 200, '11,12,13', 1, 0, 0, '0000-00-00 00:00:00'),
(14, 'Gói CV', 'goi-cv', 250, ',14,15,16', 1, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_pdfs`
--

CREATE TABLE IF NOT EXISTS `jos_properties_pdfs` (
  `id` int(5) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `parent` int(3) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(5) NOT NULL,
  `text` text NOT NULL,
  `date` date NOT NULL,
  `archivo` varchar(255) NOT NULL,
  `archivo_path` varchar(255) NOT NULL,
  `archivo_rout` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_properties_pdfs`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_persions`
--

CREATE TABLE IF NOT EXISTS `jos_properties_persions` (
  `id` int(3) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  `checked_out` tinyint(1) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `jos_properties_persions`
--

INSERT INTO `jos_properties_persions` (`id`, `name`, `alias`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(14, ' 10 người', '-10-ngi', 1, 1, 0, '0000-00-00 00:00:00'),
(15, 'Từ 10 - 20 Người', 't-10-20-ngi', 1, 2, 0, '0000-00-00 00:00:00'),
(16, 'Từ 21 - 50 Người', 't-21-50-ngi', 1, 3, 0, '0000-00-00 00:00:00'),
(17, '100 -200 người', 'tp-oan-g-1000-ngi', 1, 5, 0, '0000-00-00 00:00:00'),
(18, '51-100 người', '50', 1, 4, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_positions`
--

CREATE TABLE IF NOT EXISTS `jos_properties_positions` (
  `id` int(3) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  `checked_out` tinyint(1) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `jos_properties_positions`
--

INSERT INTO `jos_properties_positions` (`id`, `name`, `alias`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(13, 'Quản trị cao cấp', 'qun-tr-cao-cp', 1, 0, 0, '0000-00-00 00:00:00'),
(14, 'Trưởng/phó phòng', 'trngpho-phong', 1, 0, 0, '0000-00-00 00:00:00'),
(15, 'Trưởng nhóm/Giám sát', 'trng-nhomgiam-sat', 1, 0, 0, '0000-00-00 00:00:00'),
(16, 'Tư vấn/ Trợ lý', 't-vn-tr-ly', 1, 0, 0, '0000-00-00 00:00:00'),
(17, 'Chuyên gia', 'chuyen-gia', 1, 0, 0, '0000-00-00 00:00:00'),
(18, 'Nhân viên', 'nhan-vien', 1, 0, 0, '0000-00-00 00:00:00'),
(19, 'Khác', 'khac', 1, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_products`
--

CREATE TABLE IF NOT EXISTS `jos_properties_products` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `agent_id` int(6) NOT NULL,
  `agent` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `ref` varchar(50) NOT NULL,
  `cid` int(6) NOT NULL,
  `eid` int(6) NOT NULL,
  `sid` varchar(11) NOT NULL,
  `jid` int(6) NOT NULL,
  `pid` int(11) NOT NULL,
  `cyid` int(6) NOT NULL,
  `jobyear` tinyint(2) NOT NULL,
  `salary` varchar(50) NOT NULL,
  `m_salary` varchar(50) NOT NULL,
  `i_curren` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `text` text NOT NULL,
  `quyenloi` text NOT NULL,
  `panoramic` varchar(100) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  `jpacket` tinyint(2) NOT NULL,
  `jp` varchar(10) NOT NULL,
  `p1` tinyint(1) NOT NULL,
  `p2` tinyint(1) NOT NULL,
  `p3` tinyint(1) NOT NULL,
  `date1` date NOT NULL,
  `date2` date NOT NULL,
  `date3` date NOT NULL,
  `hits` int(6) NOT NULL,
  `startdate` date NOT NULL default '0000-00-00',
  `enddate` date NOT NULL,
  `refresh_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `checked_out` tinyint(1) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=166 ;

--
-- Dumping data for table `jos_properties_products`
--

INSERT INTO `jos_properties_products` (`id`, `name`, `alias`, `agent_id`, `agent`, `type`, `ref`, `cid`, `eid`, `sid`, `jid`, `pid`, `cyid`, `jobyear`, `salary`, `m_salary`, `i_curren`, `description`, `text`, `quyenloi`, `panoramic`, `published`, `ordering`, `jpacket`, `jp`, `p1`, `p2`, `p3`, `date1`, `date2`, `date3`, `hits`, `startdate`, `enddate`, `refresh_time`, `checked_out`, `checked_out_time`) VALUES
(69, 'Sale tour có kinh nghiệm', 'sale-tour-co-kinh-nghim', 98, '', '30', '', 44, 12, '1', 11, 14, 1, 3, '14', '0', '0', '<p>Môi trường làm việc năng động và chuyên nghiệp · Có khả năng phát triển và thăng tiến · Mức lương thỏa thuận,hưởng hoa hồng theo năng lực làm việc</p>', '<p><span style="background-color: #ffffff; width: 544px; float: left;">Tìm kiếm khách hàng,sale tour</span></p>', '', '', 0, 0, 0, '', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-11-30', '2010-12-06 23:33:05', 0, '0000-00-00 00:00:00'),
(70, 'Nhân viên pha chế (Bartender)', 'nhan-vien-pha-ch-bartender', 98, '', '31', '', 44, 13, '1', 11, 14, 1, 4, '14', '0', '0', '<p>Chính sách nhân sự chính: a. Ký hợp đồng dài hạn; b. Thu nhập khởi điểm: từ 1,8tr-2,5tr. d. Thưởng tháng lương 13, thưởng A,B,C hàng tháng, d. Cơm trưa miễn phí do Công ty cung cấp. e. Và nhiều chính sách khác cho nhân viên theo luật lao động và chế độ phục lợi có trong quy chế công ty (tham khảo thêm quy chế công ty khi được mời phỏng vấn, như: - Bảo hiểm xã hội, bảo hiểm y tế. - Chế độ phép năm. - Chế độ thưởng các ngày lễ. - Chế độ đám cưới, ốm đau, ma chay. - Chế độ nghỉ mát. - Chế độ sinh nhật, 8/3, thai sản, trung thu, 1/6 - Thưởng và quà tết, chế độ thâm niên dịp tết nguyên đán)</p>', '<p><span style="background-color: #ffffff; width: 544px; float: left;">1. Chuẩn bị các công việc vào ca <br />2. Pha chế và cung cấp đồ uống theo yêu cầu của khách hàng thông qua order từ bộ phận phục vụ <br />3. Bảo quản và làm sạch các dụng cụ làm việc <br />4. Quản lý hàng hoá của bộ phận Bar <br />5. Chuẩn bị các công việc đóng ca <br />6. Thực hiện các công việc khác do Tổ trưởng tổ bar hoặc Quản lý Nhà hàng phân công </span></p>', '', '', 0, 0, 0, '1,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-11-24', '2010-12-06 23:33:36', 0, '0000-00-00 00:00:00'),
(67, 'Hỗ Trợ và Chăm Sóc Khách Hàng', 'h-tr-va-chm-soc-khach-hang', 106, '', '31', '', 44, 12, '1', 11, 14, 1, 1, '14', '0', '0', '<p>Sự siêng năng chăm chỉ trong công việc của mỗi nhân viên sẽ được bù đắp với mức thu nhập cạnh tranh, bên cạnh các khoản phúc lợi ( BHYT, BHXH…) hàng năm được bảo đảm. Có nhiều cơ hội thăng tiến và phát triển nghề nghiệp</p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Chi tiết công việc sẽ được trao đổi tại buổi phỏng vấn.  	 <br /> </span></p>', '', '', 0, 0, 0, '2,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-11-30', '2010-12-06 23:32:42', 0, '0000-00-00 00:00:00'),
(68, 'Nhân Viên Kinh Doanh Du Lịch ( Nữ)', 'nhan-vien-kinh-doanh-du-lch-n', 98, '', '30', '', 44, 13, '1', 11, 14, 1, 2, '14', '0', '0', '<p>Được hưởng mọi chế độ do pháp luật quy định. Hưởng hoa hồng khi bán được tour.</p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Bán  các chương trình, các dịch vụ, tour du lịch của công ty. Ngoài việc  hưởng lương chính, nhân viên còn nhận được hoa hồng khi bán  được các  dịch vụ, chương trình của công ty...</span></p>\r\n<p><strong><span style="float: left; width: 544px; background-color: #ffffff;">Yêu cầu khác</span></strong></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Có  ngoại hình ưa nhìn , có khả năng giao tiếp tốt,có khả năng thuyết phục,  nhanh nhẹn, học các khối chuyên ngành du lịch.có kinh nghiệm trong nghề  01 năm. Thành thạo tin học văn phòng, word, excel... <br />Biết ngoại ngữ là một lợi thế.</span></p>', '', '', 0, 0, 0, '', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-11-30', '2010-12-06 23:43:26', 0, '0000-00-00 00:00:00'),
(66, 'NV chăm sóc khách hàng qua điện thoại ', 'nhan-vien-ban-hang-kiem-chm-soc-khach-hang-qua-in-thoi-', 106, '', '12', '', 9, 12, '1', 11, 14, 1, 1, '14', '0', '0', '<p>Ứng viên có tiềm năng sẽ được nhận mức lương, thưởng hấp dẫn và những cơ hội thăng tiến trong công việc.</p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Hướng dẫn, tư vấn cho khách hàng về các đặc tính và giá của sản phẩm; <br />- Trả lời thắc mắc của khách hàng; <br />- Cập nhập thông tin vào hệ thống <br />- Thực hiện các báo cáo theo ngày, tuần và tháng.</span></p>', '', '', 0, 0, 0, '', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-11-30', '2010-12-06 23:32:31', 0, '0000-00-00 00:00:00'),
(55, 'TRƯỞNG PHÒNG KỸ THUẬT', 'trng-phong-k-thut', 91, '', '2', '', 3, 11, '5,3,', 12, 0, 1, 3, '14', '0', '0', '<p>Tốt nghiệp ĐH, chuyên ngành xây dựng dân dụng và công nghiệp, kiến trúc;<br />- Có khả năng tổ chức, quản lý, điều hành hoạt động của Phòng. Khả năng quản lý kỹ thuật chất lượng, an toàn vệ sinh lao động trong tổ chức thi công xây dựng;<br />- Tối thiểu 05 năm kinh nghiệm quản lý, điều hành, ưu tiên ứng viên đã từng giữ chức vụ phó các phòng Kỹ thuật chất lượng và Kinh tế kế hoạch trở lên.</p>', '<p>Tốt nghiệp ĐH, chuyên ngành xây dựng dân dụng và công nghiệp, kiến trúc;<br />- Có khả năng tổ chức, quản lý, điều hành hoạt động của Phòng. Khả năng quản lý kỹ thuật chất lượng, an toàn vệ sinh lao động trong tổ chức thi công xây dựng;<br />- Tối thiểu 05 năm kinh nghiệm quản lý, điều hành, ưu tiên ứng viên đã từng giữ chức vụ phó các phòng Kỹ thuật chất lượng và Kinh tế kế hoạch trở lên.</p>', '', '', 0, 0, 14, '11,2,', 1, 2, 3, '2011-12-24', '2011-12-27', '2011-12-30', 0, '2010-11-18', '2010-12-31', '2011-03-08 15:01:46', 0, '0000-00-00 00:00:00'),
(61, 'Phó Giám Đốc Kinh Doanh', 'pho-giam-c-kinh-doanh', 96, '', '12', '', 9, 14, '1', 11, 15, 1, 7, '14', '0', '0', '<p>- Mức lương thỏa thuận, phù hợp với năng lực, trình độ và kinh nghiệm làm việc;<br /> - Làm việc trong môi trường hiện đại, năng động và chuyên nghiệp;<br /> - Được hưởng đầy đủ các quyền lợi và các chế độ đối với người lao động theo Bộ luật lao động và theo quy định của Công ty.</p>', '<p> </p>\r\n<div class="box_right" style="padding-bottom: 5px;">- Quản lý bán lẻ hệ thống showroom. <br /> - Quản lý đội ngũ NVBH, NV Kỹ thuật.<br /> - Giải quyết các đơn hàng lớn.<br /> - Báo cáo các hoạt động, hiệu quả kinh doanh cho BGĐ Công ty</div>\r\n<div class="box_right" style="padding-bottom: 5px;"><strong>Yêu cầu </strong></div>\r\n<div class="box_right" style="padding-bottom: 5px;">- Tốt nghiệp ĐH chuyên ngành Thương mại, Kinh tế, QTKD trở lên<br /> - 02 năm kinh nghiệm với vị trí tương đương trong lĩnh vực nội thất, kinh nghiệm lãnh đạo nhóm tập thể.<br /> - Giao tiếp tốt, sử dụng thành thạo máy tính, Tiếng Anh giao tiếp; <br /> - Hiểu biết về kinh doanh.<br /> - Nhiệt tình, chăm chỉ, bao quát công việc, lập kế hoạch và tổ chức thực hiện kế hoạch. <br /> - Quản trị nhân sự nhóm<br /> <br /> <strong>• Quyền lợi:</strong><br /> - Mức lương thỏa thuận, phù hợp với năng lực, trình độ và kinh nghiệm làm việc;<br /> - Làm việc trong môi trường hiện đại, năng động và chuyên nghiệp;<br /> - Được hưởng đầy đủ các quyền lợi và các chế độ đối với người lao động theo Bộ luật lao động và theo quy định của Công ty.</div>', '', '', 0, 0, 0, '', 0, 2, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-11-30', '2011-03-08 15:00:17', 0, '0000-00-00 00:00:00'),
(71, 'Nhân viên giám sát bán hàng', 'nhan-vien-giam-sat-ban-hang', 111, '', '30', '', 0, 13, '2', 11, 15, 1, 1, '14', '0', '0', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Nam tuổi từ 30 đến 35 tuổi. <br />- Trình độ tối thiểu: Trung cấp trở lên, biết sử dụng vi tính văn phòng <br />- Tự tin; nhiệt tình, kiên nhẫn và yêu thích công việc.  <br />- Ưu tiên cho các ứng viên có kinh nghiệm về giám sát, quản lý. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Giám sát, quản lý , đào tạo, động viên các nhân viên bán hàng tại siêu thị. <br />- Phân bố triển khai mục tiêu hàng tuần xuống cho nhân viên. <br />- Đánh giá chấm điểm tác phong làm việc của nhân viên. <br />-Thiết lập và chăm sóc các mối quan hệ với các siêu thị. <br />-Quản lý tình hình hàng hóa:số lượng, chất lượng tại các điểm bán hàng. <br />-Theo dõi doanh thu và các mã hàng bán được. <br />-Nhập báo cáo trên hệ thống ERP mỗi ngày và giám sát việc nhân viên báo cáo trên hệ thống. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Lương hấp dẫn (lương cơ bản + phụ cấp ) <br />- Thường xuyên được đào tạo nâng cao năng lực chuyên môn và các kỹ năng làm việc.  <br />- Môi trường làm việc  năng động , cơ hội thăng tiến và chuyên nghiệp.  <br />- Công việc ổn định, lâu dài.  <br />- Được đảm bảo đẩy đủ các BHXH, BHYT, BHTN và các chế độ phúc lợi xã hội…  <br />- Các chế độ khác theo luật định và theo quy định của Công ty </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"><br /></span></p>\r\n<p> </p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">đăng tuyển : http://jolieisiam.com &gt;&gt; click vào : tuyển dụng &gt;&gt; sau đó bạn vào trang http://vita-share.com  &gt;&gt; click vào DISC . điền đầy đủ nội dung. Hộp mai bên dưới ghi : hr@joliesiam.com,</span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">và nhớ kèm theo anhe nha. nhớ là hình đẹp, ăn bận nghiêm chỉnh.</span></p>\r\n<p> </p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Phỏng vấn trực tiếp : thứ 5 hàng tuần<br /></span></p>', '', 0, 0, 0, '', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-11-30', '2010-12-06 23:33:59', 0, '0000-00-00 00:00:00'),
(72, 'Nhân viên bán hàng tại Showroom ', 'nhan-vien-ban-hang-ti-showroom-', 111, '', '40', '', 0, 11, '1', 11, 14, 1, 1, '14', '0', '2tr VND', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Ngoại hình ưa nhìn, Giao tiếp tốt, nhanh nhẹn, năng động, nhiệt tình, yêu thích đồ công nghệ. <br />- Biet sử dụng Internet, biết tìm kiếm thông tin sản phẩm. <br />- Ham học hỏi, muốn gắn bó lâu dài với hệ thống <br />- Ưu tiên có kinh nghiệm <br />- Full time </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Tư vấn và bán các sản phẩm công ty đang kinh doanh tại cửa hàng ( Điện thoại, máy nghe nhạc, máy ghi âm, phụ kiện..) <br />- Gọi điện chăm sóc khách hàng, giải đáp thắc mắc, hỗ trợ khách hàng. <br />- Chăm sóc các tủ hàng, cập nhật thông tin sản phẩm. <br />-  Giám sát, quản lý và kiểm kê hàng hóa. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">+Mức Lương: 2.500.000 đ – 4.000.000đ ( có thể thỏa thuận theo năng lực) <br /> Có thưởng hiệu quả theo tháng, năm. <br /> Ngày lễ, Chủ nhật làm việc được tính ngày công làm thêm <br />+Thời gian thử việc:  <br />Thử việc 01 – 03 tháng.  <br />Lương thử việc: 1.800.000 đ  <br /> <br /> <br />Quyền lợi: <br /> <br />- Được làm việc trong môi trường chuyên nghiệp và năng động  <br />- Được tham gia BHXH, BHYT và các chế độ phúc lợi khác của Công ty  <br />- Được thưởng tháng/quý/năm tùy theo năng lực làm việc  <br />- Tăng lương định kỳ hàng năm tùy theo năng lực làm việc  <br />- Được thưởng các ngày lễ, tết theo quy định của Công ty  <br />- Các chế độ chính sách khác được thực hiện đầy đủ theo quy định của LLĐ và quy định của Công ty  <br />- Có cơ hội thăng tiến </span></p>', '', 0, 0, 0, '2,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-11-26', '2010-12-06 23:42:25', 0, '0000-00-00 00:00:00'),
(75, 'Kỹ sư điện lạnh', 'k-s-in-lnh', 111, '', '33', '', 0, 16, '1', 11, 14, 1, 1, '14', '', '3trvnđ', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Nam, tốt nghiệp Đại Học chuyên ngành  <br />- Có kinh nghiệm làm việc tối thiểu 2 năm. <br />- Sẵn sàng đi công trình. <br />- Có kinh nghiệm làm việc trong lĩnh vực lắp đặt điều hòa không khí công nghiệp và văn phòng. <br /> - Trung thưc, nhanh nhẹn, chủ động cao trong công việc. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Thiết kế hệ thống lắp đặt. <br />- Giám sát công trình. <br />- Biết thiết kế và lập dự toán công trình. <br />- Bóc tách dữ liệu, bản vẽ thiết kế để lập dự toán đấu vào, đầu ra cho công trình <br />- Tổ chức, giám sát, thực hiện thi công công trình, lập hồ sơ hoàn công. <br />- Hoàn thiện hồ sơ, quyết toán công trình . <br />- Thực hiện các công việc cụ thể khác trong quá trình làm việc </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Lương khởi điểm: 5 - 10 triệu <br />- Được hưởng các chế độ theo luật định và theo quy định của công ty. <br />- Ưu tiên người muốn gắn bó lâu dài với công ty. </span></p>', '', 0, 0, 0, '', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-11-25', '2010-12-06 23:34:10', 0, '0000-00-00 00:00:00'),
(80, '	\r\nKỹ sư nghiên cứu Set-Top-Box (STB) ', '-k-s-nghien-cu-set-top-box-stb-', 114, '', '5', '', 0, 13, '1', 11, 14, 1, 2, '14', '', '5 triệu - 10 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Nam, tốt nghiệp chuyên ngành Điện tử - Viễn  thông hoặc Công nghệ thông tin. <br />- Có ít nhất 2 năm kinh nghiệm trong lĩnh vực điện tử, viễn thông, truyền thông, truyền hình. <br />- Hiểu biết về kỹ thuật truyền hình, dã làm việc cho các công ty truyền hình là một lợi thế. <br />- Hiểu biết về tiêu chuần truyền dẫn phát sóng  DVB trong truyền hình số mặt đất, vệ tinh, cáp. <br />- Ưu tiên ứng viên có hiểu biết và kinh nghiệm trong lĩnh vực đầu thu kỹ thuật số STB, hiểu biết về STB Middleware. <br />- Có kiến thức về lập trình và lập trình nhúng (Embedded). <br />- Hiểu biết và có kiến thức về hệ điều hành  Unix và Linux.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Nghiên cứu các giải pháp công nghệ mới tương tác với Đầu thu kỹ thuật số (STB). <br />- Nghiên cứu các giải pháp tương tự STB; <br />- Nghiên cứu, chuẩn bị giải pháp Middleware cho STB.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Mức lương cạnh tranh. <br />- Môi trường làm việc cạnh tranh. Cơ hội phát triển nghề nghiệp, chế độ đãi ngộ nhân sự thỏa đáng.</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-10', '2010-11-24 10:21:59', 0, '0000-00-00 00:00:00'),
(78, 'Nhân viên kỹ thật', 'nhan-vien-k-tht', 112, '', '7', '', 0, 12, '2', 11, 14, 1, 0, '14', '', 'Thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Nam, Tốt nghiệp cao đẳng trở lên, chuyên ngành điện, điện tử. Tuổi từ 23 - 35. <br />- Ưu tiên am hiểu điện, điện tử.</span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">- Am hiểu về điện, điện tử <br />- Sẽ miêu tả cụ thể qua phỏng vấn</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Am hiểu về điện, điện tử <br />- Sẽ miêu tả cụ thể qua phỏng vấn</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Theo luật lao động của nhà nước và các chính sách, ưu đãi của công ty.</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-11-30', '2010-11-24 00:40:45', 0, '0000-00-00 00:00:00'),
(99, 'Nhân viên lái xe riêng cho giám đốc', 'nhan-vien-lai-xe-rieng-cho-giam-c', 132, '', '9', '', 0, 15, '1', 11, 14, 1, 2, '14', '', '3 triệu - 5 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Thông  thạo nhiều đường, cẩn thận, trung thực, có thể làm việc ngoài giờ và đi  công tác xa bất cứ lúc nào cần. Ưu tiên người có kinh nghiệm, nộp hồ sơ  trực tiếp không liên hệ điện thoại</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Lái xe riêng cho giám đốc, có thể đi công tác xa làm việc ngoài giờ bất cứ lúc nào</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Lương khởi điểm 3tr + phụ cấp</span></p>', '', 0, 0, 0, '3,2,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-30', '2010-11-28 22:30:01', 0, '0000-00-00 00:00:00'),
(100, 'Giám sát kinh doanh làm việc tại Hải Phòng, Hà Nội, Thái Nguyên', 'giam-sat-kinh-doanh-lam-vic-ti-hi-phong-ha-ni-thai-nguyen', 133, '', '12', '', 0, 13, '1', 11, 15, 1, 2, '14', '', 'Thỏa Thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- 	Tốt nghiệp ĐH, CĐ các khối kinh tế, QTKD, 02 năm kinh nghiệm quản lý  bán hàng, 01 năm kinh nghiệm trong lĩnh vực điện tử, điện gia dụng.  Tiếng Anh giao tiếp, vi tính văn phòng, kỹ năng quản lý, giao tiếp,  thuyết phục và bán hàng chuyên nghiệp. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Lập kế hoạch, tổ chức bán hàng và quản lý nhân viên bán hàng thuộc khu vực phụ trách. <br />Hoàn thành chỉ tiêu doanh số kinh doanh theo mục tiêu được giao. <br />Phát triển hệ thống phân phối / Hệ thống bán hàng kênh.  <br />Giám sát các hoạt động của nhà phân phối trong địa bàn. <br />Báo cáo thông tin thị trường. Thiết lập và duy trì quan hệ tốt với khách hàng. <br />-	Quản lý từ 2-3 tỉnh. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Theo quy định pháp luật và theo chính sách của Công ty.</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-10', '2010-11-29 22:27:46', 0, '0000-00-00 00:00:00'),
(98, 'Trưởng Phòng thiết kế', 'trng-phong-thit-k', 131, '', '7', '', 0, 13, '1', 11, 15, 1, 2, '14', '', 'Thoả thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Nam, Nữ. Hình thức tốt	 --- Tốt nghiệp Đại học các chuyên ngành có liên quan. <br />- Có ít nhất 2 năm kinh nghiệm trong lĩnh vực Quản lý thiết kế thời  trang, ưu tiên đã có Kinh nghiệm trong lĩnh vực Thời trang công sở. <br />- Tin học: Word, exel, Power point, Email, Internet … <br />- Ngoại ngữ: Anh văn trung cấp <br />- Kỹ năng: Giao tiếp, thương lượng, giải quyết vấn đề tốt, làm việc độc lập và phân tích dự báo tốt. <br />- Phẩm chất: Trung thực, trách nhiệm, sáng tạo, có tinh thần đồng đội và chịu được áp lực công việc cao. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Quản lý phòng Thiết kế  <br />- Dựa trên số liệu từ phòng kinh doanh để lên kế hoạch từng chủng loại mẫu sx. <br />- Lên kế hoạch các mẫu vải cần. <br />- Giao file hình ảnh cho nhân viên theo dõi đơn hàng <br />- Yêu cầu thời điểm xuất bán của file mẫu đối với nhân viên theo dõi đơn hàng. <br />- Báo cáo số lượng file đã thực hiện vào các ngày đầu tuần. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Được trang bị các vận dụng cần thiết theo yêu cầu để thực hiện công việc <br />- Thời gian làm việc theo quy định của Công ty: 8h/ngày, nghỉ CN</span></p>', '', 0, 0, 0, '1,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-30', '2010-12-06 23:42:50', 0, '0000-00-00 00:00:00'),
(88, '10 Nhân viên kinh doanh Nam  ', '10-nhan-vien-kinh-doanh-nam-', 119, '', '12', '', 0, 15, '2', 11, 14, 1, 1, '14', '', 'Thoả thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">• Tốt nghiệp PTTH trở lên. <br />• 1 năm kinh nghiệm. <br />• Ưu tiên có kinh nghiệm bán hàng. <br />• Có khả năng thuyết phục và chăm sóc khách hàng tốt. <br />• Có kiến thức về internet, biết cài đặt phần mềm windows, chương trình ứng dụng, hiểu biết về điện thoại di động. <br /> <br />• Sử dụng thành thạo tin học văn phòng, sử dụng thành thạo internet. <br />• Khả năng giao dịch và quan hệ với khách hàng tốt <br />• Khả năng thích nghi với công việc tốt. <br />• Cần cù, chịu khó, trung thực. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">•  Giới thiệu sản phẩm thẻ gọi quốc tế Evoizvn đến các đại lý (Bưu điện,  cửa hàng điện thoại, đại lý internet, shop bán software, cửa hàng máy  tính v..v ) <br />• Phát triển đại lý theo khu vực công ty giao, chịu trách nhiệm về  doanh số bán hàng, hình ảnh của công ty trong khu vực. Xem sản phẩm  www.evoizvn.com.vn </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">• Lương căn bản: 2.500.000 vnđ/tháng <br />• Khi bán hàng tốt lương trên 10.000.000 đ/tháng. <br />• Thưởng tháng, quý, năm khi đạt được chỉ tiêu công việc <br />• Theo quy định của pháp luật và theo chính sách của Công ty <br />• Có nhiều cơ hội thăng tiến và phát triển nghề nghiệp. </span></p>', '', 0, 0, 0, '1,2,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-30', '2010-12-06 23:43:32', 0, '0000-00-00 00:00:00'),
(89, 'Nhân viên bán hàng(2_3h/ngày) 27 người', 'nhan-vien-ban-hang23hngay-27-ngi', 120, '', '12', '', 0, 16, '1', 11, 14, 1, 0, '14', '', '1 triệu - 3 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Không cần nhiều kinh nghiệm làm việc và khi trúng tuyển sẽ được hướng dẫn. <br />Biết kết quả phỏng vấn và đi làm ngay nếu trúng tuyển. <br />Có khả năng làm việc độc lập và làm việc nhóm tốt.  <br />Nhanh nhẹn, giao tiếp giỏi, kiên trì, biết đàm phán. <br />Ngoại hình khá, không nói ngọng. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">+ Bán hàng và khai thác khách hàng tiềm năng.  <br />+ Giới thiệu, tư vấn khách hàng về các sản phẩm của công ty. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">+ Hưởng các mức lương theo quy định chung và các chế độ khen thưởng khác. <br />+ Phát triển mình trong một công ty lớn, chuyên nghiệp và thân thiện. </span></p>', '', 0, 0, 0, '2,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-14', '2010-12-06 23:34:42', 0, '0000-00-00 00:00:00'),
(87, 'Nhân viên kinh doanh ', '-nhan-vien-kinh-doanh-', 118, '', '12', '', 0, 15, '1', 11, 14, 1, 0, '14', '', 'Thoả thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Tốt nghiệp Trung cấp trở lên chuyên ngành kinh tế, thương mại... <br />- Nhanh nhẹn, nhiệt tình, chăm chỉ và linh hoạt trong công viêc kinh doanh. <br />- Không ngại đi công tác xa, có phương tiện đi lại. <br />- Thành thạo tin học văn phòng (Microsoft Exel, Word...) <br />- Ưu tiên có kinh nghiệm </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Tìm kiếm, khai thác khách hàng. <br />- Kinh doanh , phát triển thị trường. <br /> </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Mức lương theo thoả thuận <br />- Được hưởng đầy đủ các chế độ BHXH, BHYT, BHTN <br />- Được hưởng các chế độ ưu đãi khác theo quy định của Công ty. <br />- Môi trường làm việc chuyên nghiệp, năng động, thân thiện.  <br /> </span></p>', '', 0, 0, 0, '1,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-15', '2010-12-06 23:43:15', 0, '0000-00-00 00:00:00'),
(90, ' 5 NHÂN VIÊN CƠ ĐỘNG - AN NINH', '-5-nhan-vien-c-ng-an-ninh', 121, '', '43', '', 0, 11, '2', 11, 14, 1, 1, '14', '', '3 triệu - 5 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">+ tính tình trung thực, hòa đồng với mọi người, năng động trong công việc, <br />+ Nam cao: 165 trở lên  <br />+ Cân nặng : 55kg trở lên </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;"># Bảo vệ - an ninh yếu nhân- VIP <br /># Bảo vệ an ninh - sự kiện , hội chợ <br /># Bảo vệ an ninh - trường học , bệnh viện <br /># Bảo vệ - an ninh công trình xây dựng , nhà xưởng <br /># Bảo vệ - an ninh cao ốc , văn phòng</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">+ được hưởng đầy đủ các quyền lợi theo quy định của pháp luật</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-25', '2010-11-25 08:09:17', 0, '0000-00-00 00:00:00'),
(91, 'Tuyển bảo vệ ', 'tuyn-bo-v-', 122, '', '43', '', 0, 16, '1', 11, 14, 1, 0, '14', '', '1 triệu - 3 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Trung thực, chăm chỉ <br />- Chấp nhận làm ca </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- trông giữ xe nhân viên công ty <br />- Bảo vệ tài sản công ty <br />- Hỗ trợ các công việc khác <br />(Làm 8 tiếng/ ngày)</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Công việc ổn định <br />- Lương và phụ cấp : 1.800.000đ <br />- Thử việc 1 tháng.</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-15', '2010-11-25 19:23:05', 0, '0000-00-00 00:00:00'),
(92, 'Tuyển gấp: 1 Nhân viên in ấn', 'tuyn-gp-1-nhan-vien-in-n', 123, '', '46', '', 0, 15, '1', 11, 14, 1, 0, '14', '', 'Thoả thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Tốt nghiệp trung cấp trở lên  <br />-Làm việc toàn thời gian  <br />-Đam mê kinh doanh  <br />-Nhanh nhẹn, có trách nhiệm, cẩn thận với công việc  <br />-ưu tiên ứng viên có kinh nghiệm quảng cáo. k biết được đào tạo</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">-Làm công việc in ấn các ấn phẩm quảng cáo của công ty <br /> - Sẽ trao đổi cụ thể hơn khi phỏng vấn.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Được hưởng đầy đủ các quyền lợi theo luật Lao động (BHYT &amp; BHXH) và các quyền lợi khác theo nội quy, quy chế Công ty.  <br />- Môi trường làm việc sáng tạo, chuyên nghiệp &amp; thân thiện.  <br />- Thu nhập xứng đáng với kết quả công việc.  <br />- Có nhiều cơ hội thăng tiến và phát triển nghề nghiệp . </span></p>', '', 0, 0, 0, '1,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-30', '2010-12-06 23:34:52', 0, '0000-00-00 00:00:00'),
(93, 'Nhân viên kế toán', 'nhan-vien-k-toan', 124, '', '23', '', 0, 13, '1', 11, 14, 1, 1, '14', '', 'Thoả thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Tốt nghiệp Đại học chuyên ngành tài chính kế toán <br />- Cẩn thận , tỉ mỉ , chăm chỉ</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">-Có kinh nghiệm và kỹ năng về các đầu việc sau: <br />- Thực hiện các nghiệp vụ kế toán tổng hợp, công nợ. <br />- Kiểm tra, theo dõi các khoản thu, trả đảm bảo đúng yêu cầu và tiến độ công việc. <br />- Thực hiện các công tác nghiệp vụ kế toán theo quy định của Công ty và pháp luật hiện hành. <br />- Tính toán, quyết toán kết quả hoạt động kinh doanh các mảng phụ trách. <br />- Đảm bảo các công việc hoàn thành đúng thời hạn quy định. <br />- Các công việc phát sinh khác của Công ty và Bộ phận theo yêu cầu </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Các quyền lợi được hưởng theo chế độ <br />- Trao đổi trực tiếp khi phỏng vấn</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-30', '2010-11-25 20:22:12', 0, '0000-00-00 00:00:00'),
(94, 'Biên tập Viên SMS', 'bien-tp-vien-sms', 125, '', '21', '', 0, 12, '1', 11, 14, 1, 0, '14', '', 'Thoả thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">. Nam <br />. Sáng tạo, năng động, nhiệt tình, thích tìm hiểu và tạo ra những dịch vụ mới.  <br />• Ưu tiên những ứng viên đã có kinh nghiệm làm việc tại các công ty cung cấp nội dung(SMS, Web...).  <br />. Có khả năng chịu áp lực cao trong công việc .</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Khai  thác các nội dung về SMS trên internet, xây dựng ý tưởng quảng cáo, ý  tưởng về các dịch vụ sms liên quan Trên điện thoại di động và tổ chức  triển khai  <br />• Thời gian làm việc: Hành chính và theo lịch phân công của công ty. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">-  Tạo điều kiện để mỗi người phát huy tối đa sở trường của mình, cả trong  công việc trực tiếp cũng nhưng các hoạt động phong trào. <br />- Được hưởng một mức đãi ngộ xứng đáng thông qua các về BHXH, BHYT  chế độ lương, thưởng, quyền mua cổ phiếu...; của nhà nước và của công ty </span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-11-30', '2010-11-25 20:35:22', 0, '0000-00-00 00:00:00'),
(95, 'Thủ quỹ', 'th-qu', 127, '', '23', '', 0, 12, '1', 11, 14, 1, 1, '14', '', 'Thoả thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Tính cách: nhanh nhẹn, trung thực, cẩn thận.  <br />- Hộ khẩu Hà Nội.  <br />- Sức khỏe tốt.  <br />- Thành thạo tin học văn phòng (word, exel).  <br />- Ưu tiên các ứng viên có kinh nghiệm làm việc tương đương từ 1 năm  trở lên và tốt nghiệp các trường liên quan đến ngành kế toán. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Thực hiện việc thu chi tiền mặt theo mệnh lệnh của Lãnh đạo công ty thông qua các phiếu thu, chi do Kế toán lập.  <br />- Kiểm tra nội dung của hợp đồng hỗ trợ vốn.  <br />- Thu tiền, chi tiền, ghi chép phản ánh chính xác, kịp thời vào sổ quỹ.  <br />- Tổng hợp doanh số, doanh thu của bộ phận bán hàng theo tháng, quý, năm.  <br />- Gửi tiền, rút tiền ngân hàng.  <br />- Báo cáo tình hình tài chính nội bộ theo quý. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">-Hưởng lương và phụ cấp theo quy định của công ty  <br />-Được hỗ trợ ăn trưa, tham gia BHXH <br /> </span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-10', '2010-11-25 23:47:46', 0, '0000-00-00 00:00:00'),
(96, 'Nhân viên kỹ thuật sửa chữa bảo dưỡng xe máy', 'nhan-vien-k-thut-sa-cha-bo-dng-xe-may', 128, '', '9', '', 0, 15, '1', 11, 14, 1, 2, '14', '', 'Thoả thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Giới tính: Nam  <br />+ Tốt nghiệp cao đẳng, ĐH chuyên ngành kỹ thuật ô tô, xe máy , động cơ, điện..  <br />+ Kinh nghiệm: &gt;1 năm </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">+ Bảo dưỡng các sản phẩm của Công ty đang trong thời gian bảo hành, những sản phẩm khác khi khách hàng yêu cầu.  <br />+ Phát hiện và tìm cách giải quyết những sự cố về sản phẩm trong  thời gian bảo hành hoặc những hư hỏng của hàng hóa lưu kho. Thông báo  kịp thời những sự cố của sản phẩm cho người phụ trách để tìm ra hướng  giải quyết.  <br /> <br />+ Tư vấn, đóng góp ý kiến cho lãnh đạo Công ty về chất lượng sản  phẩm, hệ thống sản phẩm / dịch vụ cung cấp trên thị trường xe máy.  <br />+ Duy trì các mối quan hệ với khách hàng của Công ty thông qua việc  bảo dưỡng sản phẩm, đem lại sự hài lòng đối với sản phẩm của Công ty đặc  biệt trong mối quan hệ với khách hàng truyền thống.  <br />+ Lập báo cáo công việc theo thời điểm hàng tuần / hàng tháng / hàng quý / hàng năm. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">+ Hưởng lương cơ bản  <br />- Được hưởng đầy đủ mọi chế độ đãi ngỗ, phúc lợi nhân viên như BHXH, BHYT... theo đúng luật lao động hiện hành </span></p>', '', 0, 0, 0, '2,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-12', '2010-12-06 23:39:59', 0, '0000-00-00 00:00:00'),
(97, 'CNKT SX cửa nhựa, nhôm kính', 'cnkt-sx-ca-nha-nhom-kinh', 130, '', '33', '', 0, 17, '1', 11, 14, 1, 0, '14', '', 'Thoả thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Nhiệt tình và gắn bó lâu dài với Công ty. Ưu tiên những trường hợp ở gần khu vực Đông Anh.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">CNKT  làm cửa nhựa cho dây chuyền sản xuất cửa nhựa lõi thép, nhôm kính:  thành thạo tất cả các công việc từ công đoạn đọc bản vẽ kích thước tới  công đoạn sản xuất và lắp đặt.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Về thu nhập: <br />- Đối với những trường hợp đã thành thạo làm khung nhôm cửa kính thu nhập thỏa thuận từ 2.500.000đ/tháng trở lên. <br />- Đối với những trường hợp chưa thành thạo sẽ được đào tạo làm thợ  nhôm kính và thợ làm cửa nhựa thu nhập khởi điểm từ  1.800.000đ/tháng.(thử việc 2 tháng trong thời gian thử việc nếu làm tốt  sẽ ngừng thử việc và thỏa thuận lại thu nhập). <br />Về các chế độ chính sách: <br />- Các ngày lễ tết hưởng theo quy định của Bộ luật lao động. <br />- Đóng bảo hiểm theo quy định của Nhà nước. </span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-30', '2010-11-26 00:42:50', 0, '0000-00-00 00:00:00'),
(102, '	\r\nNhân viên trắc địa ', '-nhan-vien-trc-a-', 134, '', '3', '', 0, 12, '1', 11, 14, 1, 2, '14', '', 'Thỏa Thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Nam. Khỏe mạnh và đi công tác trên công trường Lạng Sơn</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Cần  tuyển 01 nhân viên trắc địa tốt nghiệp cao đẳng trở lên làm cho dự án  xây dựng tại Lạng Sơn. Công việc cụ thể sẽ trao đổi khi phỏng vấn</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Được hưởng mức lương hấp dẫn. <br />Được đóng BHXH, BHYT.</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-31', '2010-11-30 18:54:03', 0, '0000-00-00 00:00:00'),
(103, '\r\nNhân viên truyền thông và tổ chức sự kiện ', '-nhan-vien-truyn-thong-va-t-chc-s-kin-', 135, '', '12', '', 0, 13, '1', 11, 14, 1, 1, '14', '', '3 triệu - 5 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Nam, Nữ trình độ Đại học hoặc cao đẳng thuộc lĩnh vực Marketing, PR, event <br />- Kỹ năng tự học hỏi <br />- Kỹ năng làm việc độc lập <br />- Kỹ năng giao tiếp <br />- Kỹ năng diễn đạt/thuyết trình <br />- Kỹ năng tổ chức công việc <br />- Kỹ năng đàm phán thương lượng thuyết phục </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">•  Có năng lực thiết kế lên kế hoạch tổng thể 1 chương trình Marketing,  PR, Quảng cáo… Phân tích chi tiết công việc có logic theo thứ tự thời  gian, sự phối hợp nhịp nhàng giữa các đơn vị thực hiện. <br />• Có kinh nghiệm tổ chức nhiều chương trình  quy mô rộng, có phối  hợp nhiều chương trình con, hiểu biết và có mối quan hệ trên nhiêu tỉnh  thành. Có mối quan hệ và hiểu biết sâu về các dịch vụ cung cấp phục vụ  cho sự kiện như : Lân sư rồng – Thuê nhà bạt, Âm thanh ánh sáng – bàn  ghế - PG – MC – Trang phục – Đài truyền hình – Giấy phép… <br />• Có khả năng thuyết trình, diễn thuyết thu hút, biết tại và sử dụng  thành thạo các soft trình chiếu trên máy tính. Tự tin trong giao tiếp.  Làm việc chủ động trong công việc, thời gian… Nhất là khả năng kiểm soát  chương trình và phối hợp với các bộ phận liên quan. <br />• Tính cách quyết đoán, chuyển công việc và giải quyết sự cố nhanh  gọn, dứt điểm. Luôn dự đoán nhiều tình huống khi triển khai 1 chương  trình ( Trời mưa, bị trễ giờ khai mạc, khách tới chậm, sự cố trong di  chuyển, thiếu hụt nhân sự….) <br />• Tổ chức và triển khai chương trình sự kiện theo nhu cầu của các  doanh nghiệp. Định được giá thành thực hiện. luôn tìm kiếm những đơn vị  cung cấp tốt nhất. <br />• Có ý tưởng sáng tạo đột phá. Khả năng mỹ thuật cao. Lên chương  trình, khớp thời gian, xuyên suốt toàn bộ chương trình sự kiện trước,  trong và sau khi diễn ra sự kiện. <br />• Xây dựng chương trình sự kiện, lo thủ tục (liên kết và kiểm soát  chất lượng, tiến độ với các nhà cung cấp chất lượng và uy tín …) quy  tình thực hiện sự kiện. <br />• Có khả năng biên soạn, biên tập bài giảng, có kỹ năng giảng dạy tích cực, am hiểu về kế toán tài chính, công nghệ thông tin. <br />• Ngoại hình và sức khỏe tốt. Sẵn sàng đi công tác xa. Kỹ năng mềm tốt, có khả năng làm việc độc lập hoặc theo nhóm. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">• Lương chính: 4.500.000 VND/ tháng + Thưởng.. <br />• BHXH, BHYT, công tác phí theo quy định của công ty. <br />• Môi trường làm việc và cơ hội phát triển nghề nghiệp tốt <br />• Thử việc và đào tạo kỹ năng 60 ngày. <br />• Môi trường làm việc và cơ hội phát triển nghề nghiệp tốt. </span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-11-30', '2010-11-29 22:54:12', 0, '0000-00-00 00:00:00'),
(104, 'Nhân viên Phòng Công nghệ', 'nhan-vien-phong-cong-ngh', 140, '', '8', '', 0, 13, '1', 11, 14, 1, 1, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Tốt nghiệp ĐH các trường kỹ thuật trở lên, có nền tảng về kỹ thuật, công nghệ, sản phẩm một trong các lĩnh vực: Y tế, Tự động hóa, CNTT, vât lý, sinh học... <br />- Có kỹ năng làm việc theo nhóm cũng như làm việc độc lập. <br />- Có kỹ năng thuyết phục khách hàng, khả năng thuyết trình giải pháp, sản phẩm công nghệ. <br /> - Hiểu biết về kinh doanh dự án, nhạy bén trong việc xây dựng các tiêu chí về công nghệ của dự án. <br /> - Có khả năng nghiên cứu, cập nhật các công nghệ mới. <br />- Ưu tiên các ứng viên có các chứng chỉ về công nghệ, có quan hệ tốt với các hãng cung cấp thiết bị. <br />- Tiếng Anh thành thạo. <br />- Thành thạo các công cụ: Visio, Word, Excel <br />(Ứng viên có thể gửi bản hồ sơ scan đầy đủ và đã công chứng qua email đến nhà tuyển dụng) </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Tư vấn, viết giải pháp cho các dự án kỹ thuật công ty Cổ phần công nghệ Vĩnh Hưng. <br /> - Chủ trì thiết kế hệ thống cho các Dự án kỹ thuật, công nghệ. <br /> - Nghiên cứu, triển khai các công nghệ mới <br /> - Tham gia triển khai trong các dự án phức tạp, công nghệ cao </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">-Môi trường làm việc chuyên nghiệp. <br />-Được đào tạo và hỗ trợ các điều kiện phát huy khả năng ở mức cao nhất. <br />-Đãi ngộ thoả đáng <br />-Được đóng bảo hiểm theo quy định của nhà nước. </span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2010-12-01 00:17:18', 0, '0000-00-00 00:00:00'),
(105, 'Công nghệ thông tin', 'cong-ngh-thong-tin', 141, '', '8', '', 0, 15, '2', 11, 14, 1, 0, '14', '', 'thỏa thuận', '', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Quản trị mạng và các Hệ thống quản lý nội bộ. Hỗ trợ nhân viên công ty các vấn đề liên quan đến việc sử dụng, khai thác các tài nguyên nội bộ và các Hệ thống quản lý. - Thực hiện các công việc khác liên quan (theo yêu cầu).</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Có chế độ lương thưởng hợp lý cũng như những chế độ về BHXH và BHYT </span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2010-12-01 00:26:01', 0, '0000-00-00 00:00:00'),
(106, 'Nhân viên cấp cứu dữ liệu', 'nhan-vien-cp-cu-d-liu', 142, '', '8', '', 0, 15, '1', 11, 15, 1, 1, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;"><br /></span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Kiểm tra nội dung file dữ liệu, ghi nhận và phân tích yêu cầu của khách hàng, open và lọc dữ liệu theo yêu cầu, viết phiếu thanh toán, bàn giao file dữ liệu đã hoàn chĩnh. <br />- Phân tích yêu cầu và thực hiện lệnh tìm kiếm, linh hoạt trong trong việc tùy biến các ứng dụng hoặc dự liệu. <br />- Sửa chữa file bị lỗi, biết các lệnh copy, cut, paste, kết nối tất cả các dạng thiết bị lưu trử như: USB, BOX data, Camera, The SD, SATA, ATA.. vào máy vi tính. <br />- Bảo mật dữ liệu và kiểm soát data. <br />- Chuyển giao dữ liệu và hướng dẫn sử dụng các loại dữ liệu khác nhau.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Lương cao nhiều chế độ phúc lợi: thỏa thuận, nhiều cơ hội thăng tiến <br /> <br />- Được huấn luyện – đào tạo đặc biệt, chuyên ngành Data Recovery và giải mã nội dung số. <br /> <br />- Làm việc theo sức sáng tạo của mình, hưởng lương như một kỹ sư cao cấp, không có ranh giới</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2010-12-01 00:34:37', 0, '0000-00-00 00:00:00'),
(107, 'Kỹ sư lập trình và phát triển Website', 'k-s-lp-trinh-va-phat-trin-website', 143, '', '8', '', 0, 13, '2', 11, 14, 1, 2, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">1.Tốt nghiệp Cao đẳng, Đại học chuyên ngành Công nghệ thông tin hoặc chuyên ngành tương đương. <br />2.Thành thạo lập trình bằng ngôn ngữ ASP, PHP, PHP frame work, XML, SQL server, MySQL và đã có kinh nghiệm lập trình Web với các kỹ năng trên ít nhất 1 năm (Thực thiện tối thiểu 03 dự án web với vai trò senior developber). <br />3.Có khả năng làm việc theo nhóm. <br />4.Có khả năng đi triển khai dự án. <br />5.Có trách nhiệm cao và chủ động trong công việc. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">1.Thực hiện phát triển các chức năng của dự án website, duy trì, nâng cấp-phát triển các sản phẩm, dự án phần mềm. <br />2.Thực hiện chính công việc triển khai dự án, hỗ trợ dự án đến khách hàng. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Nam/Nữ: tuổi từ 22 đến 35 <br />-Có sức khỏe tốt. <br />-Lương : Lương cứng + dự án. <br />-Được hưởng đầy đủ chế độ BHXH, BHYT theo qui định của luật lao động VN và các chính sách phát triển nhân viên của công ty. </span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-31', '2010-12-01 00:43:27', 0, '0000-00-00 00:00:00');
INSERT INTO `jos_properties_products` (`id`, `name`, `alias`, `agent_id`, `agent`, `type`, `ref`, `cid`, `eid`, `sid`, `jid`, `pid`, `cyid`, `jobyear`, `salary`, `m_salary`, `i_curren`, `description`, `text`, `quyenloi`, `panoramic`, `published`, `ordering`, `jpacket`, `jp`, `p1`, `p2`, `p3`, `date1`, `date2`, `date3`, `hits`, `startdate`, `enddate`, `refresh_time`, `checked_out`, `checked_out_time`) VALUES
(108, 'Nhân viên Tuyển dụng', 'nhan-vien-tuyn-dng', 144, '', '13', '', 0, 12, '1', 11, 14, 1, 1, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">.   Yêu cầu:  <br /> <br />a. Trình độ: Tốt nghiệp trình độ Cao đẳng trở lên chuyên ngành Luật, Hành chính quốc gia, Quản trị nhân lực và các trường khác thuộc ngành kinh tế. <br />b.	Vi tính: Thạo tin học văn phòng  <br />c.	Kinh nghiệm: Có ít nhất 2 năm kinh nghiệm quản trị nhân sự (ưu tiên trong ngành nhà hàng, khách sạn...hoặc sản xuất).  <br />d.	Từ 25 – 35 tuổi.  <br />e.	Khả năng giải quyết các tình huống nhân sự. Khả năng làm việc độc lập tốt </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">1.	Thực hiện hoạt động tuyển dụng của Công ty <br />1.1.	Tiếp nhận yêu cầu nhân sự của tất cả các bộ phận; <br />1.2.	Quản lý danh mục các kênh tuyển dụng thường sử dụng phù hợp với từng nhóm đối tượng cần tuyển dụng; <br />1.3. Trên cơ sở Kế hoạch tuyển dụng của Công ty, tìm kiếm, liên hệ và đánh giá các nguồn tuyển dụng cũ, khai thác các nguồn tuyển dụng mới phù hợp; <br />1.4. Tìm kiếm, liên hệ các trường dạy nghề, trung tâm hướng nghiệp, trung tâm đào tạo, trung tâm giới thiệu việc làm ... và tổ chức các hoạt động để thu hút nguồn ứng viên phù hợp; <br />1.5.	Chủ động tìm kiếm ứng viên trên mạng, trên báo và mọi nguồn khác;  <br />1.6.	Gửi thông tin tuyển dụng cho các nguồn liên quan bao gồm nguồn miễn phí và tính phí; <br />1.7.	Đề xuất, đánh giá, ký hợp đồng và tổ chức thực hiện hợp đồng với các đơn vị cung cấp dịch vụ tuyển dụng; <br />1.8.	Theo dõi và đánh giá hiệu quả các kênh tuyển dụng hàng tháng; <br />1.9.	Liên lạc với ứng viên mời nộp hồ sơ dự tuyển; <br />1.10.	Nhận hồ sơ của các ứng viên và kiểm tra đánh giá hồ sơ; <br />1.11.	Tổ chức việc đánh giá sơ tuyển ứng viên; <br />1.12.	Lập Danh sách ứng viên đạt yêu cầu sơ tuyển. Sắp xếp lịch chuyển Trưởng bộ phận hoặc Giám đốc phỏng vấn; <br />1.13.	Thông báo ứng viên trúng tuyển; <br />1.14.	Chuyển hồ sơ cho Nhân viên nhân sự phụ trách tổ chức ký Hợp đồng với ứng viên; <br />2.	Chuẩn bị các nguồn nhân sự dự trữ cho nhu cầu của Công ty <br />2.1.	Xây dựng danh mục các chức danh cần có nguồn dự trữ; <br />2.2.	Tìm kiếm danh sách hồ sơ dự trữ đạt yêu cầu theo Bản mô tả công việc của Công ty; <br />3.	Thực hiện các công việc khác do Giám đốc hoặc Trưởng phòng HCNS giao </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Chính sách nhân sự chính: <br /> <br />a.	Ký hợp đồng dài hạn. <br /> <br />b.	Thu nhập khởi điểm:  1,5 – 2 triệu <br /> <br />c.	Tháng lương 13. Thưởng ABC <br /> <br />d.	Cơm trưa miễn phí do công ty cung cấp. <br /> <br />e. Và nhiều chính sách khác cho nhân viên theo luật lao động và chế độ phục lợi có trong quy chế công ty (tham khảo thêm quy chế công ty khi được mời phỏng vấn) như: <br /> <br />-	Bảo hiểm xã hội, bảo hiểm y tế. <br />-	Chế độ phép năm. <br />-	Chế độ thưởng các ngày lễ. <br />-	Chế độ đám cưới, ốm đau, ma chay. <br />-	Chế độ nghỉ mát. <br />-	Chế độ sinh nhật, 8/3, thai sản, trung thu, 1/6… <br />-	Thưởng và quà tết, chế độ thâm niên dịp tết nguyên đán</span></p>', '', 0, 0, 0, '', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-15', '2010-12-06 23:45:51', 0, '0000-00-00 00:00:00'),
(109, 'Tuyển trưởng phòng nhân sự', 'tuyn-trng-phong-nhan-s', 145, '', '4', '', 0, 13, '1', 11, 15, 1, 2, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Tốt nghiệp Đại học <br />- Có ít nhất 02 năm kinh nghiệm trong ngành Nhân sự. <br />- Có khả năng nhìn nhận, đánh giá con người <br />- Tiếng Anh giao tiếp và vi tính văn phòng tốt. <br />- Kỹ năng giao tiếp, trình bày và thiết lập mối quan hệ tốt. <br />- Khả năng lập kế hoạch và báo cáo tốt <br />- Khả năng làm việc độc lập và dưới áp lực cao.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Phỏng vấn, tuyển dụng và chọn lọc ứng viên cho các vị trí mà GĐ yêu cầu. <br />- Quản lý mảng tuyển dụng và chính sách nhân viên để phát triển các chiến lược của công ty. <br />- Phát triển và thực hiện các chính sách, quy định về nhân sự trong cho công ty. <br />- Quản lý và kiểm soát các chính sách về phụ cấp, lợi tức và các khóa đào tạo phát triển các kỹ năng cho nhân viên. <br />- Đảm bảo các chính sách và quy trình nhân sự phù hợp với tình hình công ty và quy định của pháp luật <br />- Lập chương trình đánh giá hiệu quả làm việc của nhân viên.</span></p>\r\n<p> </p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Lương cao theo thỏa thuận: <br />- Thưởng theo hiệu quả công việc <br />- Làm việc trong một môi trường chuyên nghiệp, năng động và thân thiện. <br />- Cơ hội phát triển sự nghiệp bền vững. Đề cao các giá trị văn hoá doanh nghiệp. <br />- Được hưởng đầy đủ các chế độ theo Luật Lao động Việt Nam: Bảo hiểm Xã hội, Bảo hiểm Y tế...</span></p>\r\n<p> </p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-15', '2010-12-01 01:49:32', 0, '0000-00-00 00:00:00'),
(110, ' 	\r\nQuản lý nhân sự ', '-qun-ly-nhan-s-', 146, '', '15', '', 0, 12, '1', 11, 14, 1, 1, '14', '', '3 - 5 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Nhanh nhẹn, trung thực, có tố chất lãnh đạo.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;"> Quản lý hệ thống nhân viên <br />- Lập kế hoạch cho phòng kinh doanh <br />- Chịu trách nhiệm chính trước cán bộ cấp trên về kết quả kinh doanh của công ty.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Lương + chế độ tưởng thưởng theo kết quả kinh doanh. <br />- BHXH, BHYT theo quy định riêng của công ty. <br />- Cơ hội nghề nghiệp lâu dài, ổn định.</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-26', '2010-12-01 01:55:24', 0, '0000-00-00 00:00:00'),
(111, 'Nhân viên nhân sự', 'nhan-vien-nhan-s', 147, '', '15', '', 0, 13, '2', 11, 14, 1, 1, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Nam, bắt buộc phải biết tiếng hoa <br />có kinh nghiện trong lĩnh vực nhân sự ít nhất 01 năm.không biết tiếng hoa vui lòng không nộp hồ sơ.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Thành thạo trong việc quản lý nhân sự. Tính lương cho công nhân.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Hưởng đầy đủ các chế độ BHXH,BHYT,BHTN và các chế độ phúc lợi của nhà nước Việt Nam.</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2011-01-01', '2010-12-01 02:01:05', 0, '0000-00-00 00:00:00'),
(112, 'Tuyển chuyên viên nhân sự', 'tuyn-chuyen-vien-nhan-s', 148, '', '15', '', 0, 13, '1', 11, 14, 1, 3, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;"> Ưu tiên ứng viên đã có kinh nghiệm làm về tuyển dụng nhân sự cho các công ty, tập đoàn lớn. <br />- Khả năng đàm phán tốt, ngoại hình ưa nhìn.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Đăng tuyển, sàng lọc ứng viên; <br />- Phụ Trách mảng tuyển dụng cho công ty; <br />- Các việc HCNS khác.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Lương thỏa thuận tùy theo năng lực ( Sẵn sàng trả lương cao cho các ứng viên đáp ứng được yêu cầu tuyển dụng)</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-10', '2010-12-01 02:07:31', 0, '0000-00-00 00:00:00'),
(113, ' 	\r\nQuản lý nhân sự ', '-qun-ly-nhan-s-', 149, '', '15', '', 0, 15, '1', 11, 14, 1, 1, '14', '', '3 - 5 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Nhanh nhẹn, trung thực, có trách nhiệm trong công việc, khả năng giao tiếp tốt, không ngại áp lực công việc.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Quản lý hồ sơ, sổ sách, thúc đẩy bộ phận nhân lực hoàn thành kế hoạch (tháng, quý, năm)</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Lương, thưởng, các khoản phụ phí, phụ cấp khác</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-25', '2010-12-01 02:19:29', 0, '0000-00-00 00:00:00'),
(114, ' 	 Nhân viên tuyển dụng ', '-nhan-vien-tuyn-dng-', 150, '', '15', '', 0, 15, '1', 11, 14, 1, 1, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Nam/nữ có độ tuổi từ 20- 30  <br />- Khả năng giao tiếp tốt  <br />-Yêu thích kinh doanh  <br />- Ưu tiên ứng viên tốt nghiệp cao đẳng trở lên chuyên ngành; Quản trị kinh doanh sư phạm, kế toán,marketing, nhân sự, bảo hiểm, kinh tế, tài chính, ngân hàng. <br />-Có khả năng sử dụng vi tính văn phòng và internet  <br />- Tinh thần cầu tiến.  <br />- Ưu tiên ứng viên có kimh nghiệm trong lĩnh vực tài chính ,bảo hiểm,các nghành kinh doanh khác. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Tuyển dụng nhân lực theo yêu cầu của công ty  <br />- Theo dõi, hướng dẫn, dẫn dắt nhân sự mới tuyển đảm bảo yêu cầu công việc</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Bạn muốn làm việc  <br />* Trong một môi trường chuyên nghiệp  <br />* Được huấn luyện theo chương trình hiện đại nhất  <br />* Có mức thu nhập cao nhất  <br />* Cơ hội thăng tiến luôn mở ra trước mắt bạn  <br />* Dễ dàng mở rộng tầm nhìn ra thế giới với các chuyến tham quan và tập huấn tại nước ngoài hàng năm…  <br />- Bạn sẽ được tham gia lớp học và được cấp bằng của Bộ Tài Chính- Khóa học kéo dài 1 tuần .  <br />- Chế độ thăng tiến lên các cấp cao nhanh và chế độ thu nhập cực kỳ hấp dẫn .  <br />- Nếu bạn đang làm việc tại các công ty Bảo Hiểm khác thì bạn sẽ thấy rẳng chế độ của Cathay cực kỳ hấp dẫn .  <br />- Mức lương cứng +trợ cấp trách nhiệm+Hoa hồng Cao + Thưởng  <br />- Thu nhập hấp dẫn, có thể làm bán thời gian.  <br />- Cơ hội thăng tiến nghề nghiệp nhanh theo năng lực.  <br />- Được đào tạo nhiều khóa học (miến phí) nâng cao kỹ năng &amp; được cấp chứng chỉ chuyên ngành.  <br />- Được trang bị các công cụ làm việc  <br />- Có cơ hội đi du lịch miễn phí trong &amp; ngoài nước và tham dự các hội nghị quốc tế.</span></p>', '', 0, 0, 0, '', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-25', '2010-12-06 23:46:00', 0, '0000-00-00 00:00:00'),
(115, 'Trưởng phòng nhân sự', 'trng-phong-nhan-s', 151, '', '15', '', 0, 14, '2', 11, 15, 1, 1, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Từng làm ở vị trí giám đốc nhân sự hoặc trưởng phòng nhân sự.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Quản lý nhân sự trong công ty. <br />- quản lý nhân viên, bảo hiểm, tiền lương, tài sản....</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Lương và thưởng.</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-20', '2010-12-01 02:31:45', 0, '0000-00-00 00:00:00'),
(116, 'Tuyển 01 Lái xe bằng E', 'tuyn-01-lai-xe-bng-e', 152, '', '52', '', 0, 11, '1', 11, 14, 1, 5, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Tốt nghiệp từ THPT trở lên <br />- Đã có kinh nghiệm chay xe chở container. <br />- Có kinh nghiệm chạy xe tuyến đường dài. <br />(Ưu tiên PV ứng viên nộp hồ sơ sớm)</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Lái xe 15 tấn <br />(Trao đổi công việc rõ hơn khi tham gia phỏng vấn)</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Mức lương: thỏa thuận <br />- Các ưu đãi khác: theo quy định của công ty. <br />- Liên hệ: <br />Ms Phương - Công ty Cổ phần Cửa sổ Nhựa Châu Âu <br />Nhà máy 1 - lô 15 - khu CN Quang Minh - Mê Linh - Hà Nội </span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-15', '2010-12-01 02:39:48', 0, '0000-00-00 00:00:00'),
(117, 'Nhân viên kinh doanh bất động sản ', 'nhan-vien-kinh-doanh-bt-ng-sn-', 153, '', '4', '', 0, 13, '1', 11, 14, 1, 1, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Giới tính :Nam <br />Tuổi: 23-32 <br />Trình độ: cử nhân <br />Trình độ tiếng Anh: C <br />Thạo Vi tính: văn phòng, internet <br />Kinh nghiệm bất động sản : 6 tháng trở lên.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Chăm sóc khách hàng có nhu cầu về bất động sản. <br />- Đàm phán giao dịch với khách hàng và ký kết hợp đồng giao dịch Bất động sản. <br />- Chi tiết công việc sẽ được trao đổi cụ thể trong quá trình phỏng vấn.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Theo bộ luật nhà nước. <br />- Hưởng hoa hồng và làm việc theo giờ hành chính ,nghỉ chiều thứ 7 và ngày chủ nhật.</span></p>', '', 0, 0, 0, '1,2,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-10', '2010-12-06 23:43:39', 0, '0000-00-00 00:00:00'),
(118, 'Nhân viên kinh doanh tư vấn', 'nhan-vien-kinh-doanh-t-vn', 154, '', '4', '', 0, 15, '1', 11, 14, 1, 1, '14', '', 'trên 10 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Có kinh nghiệm trong lĩnh vực tư vấn, ưu tiên có kinh nghiệm trong lĩnh vực tư vấn Bất động sản. <br />- Có kỹ năng giao tiếp đàm phán với khách hàng, <br />- Năng động, nhiệt tình,cầu tiến </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Môi giới, kinh doanh BĐS, phát triển mạng lưới thị trường,... sẽ trao đổi cụ thể hơn khi phỏng vấn</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Lương cứng + %doanh thu cao + quỹ khen thưởng của công ty và được hưởng đầy đủ các chế độ theo quy định của luật lao động</span></p>', '', 0, 0, 0, '2,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-30', '2010-12-06 23:38:19', 0, '0000-00-00 00:00:00'),
(119, 'Chuyên viên pháp lý', 'chuyen-vien-phap-ly', 155, '', '4', '', 0, 14, '1', 12, 15, 1, 1, '14', '', 'trên 10 triệu', '<p style="text-align: left;"><span style="float: left; width: 544px; background-color: #ffffff;">- Trình độ: Cử nhân Luật (Chính quy).  <br />- Ưu tiên: Ứng viên có thẻ hành nghề Luật sư hoặc là Người đang tập sự hành nghề luật sư;  <br />- Kinh nghiệm tối thiểu 01 năm hoạt động trong các công ty, văn phòng luật, văn phòng công chứng (điều kiện kinh nghiệm tối thiểu 1 năm được áp dụng linh hoạt cho trường hợp Ứng viên được nhà tuyển dụng đánh giá cao trong buổi phỏng vấn) ; <br />- Sử dụng tốt phần mềm văn phòng thông dụng;  <br />- Thành thạo kỹ năng soạn thảo văn bản, hợp đồng, có khả năng giao tiếp và đàm phán;  <br />- Có khả năng làm việc độc lập và làm việc nhóm, chịu được áp lực công việc, khéo giao tiếp </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Soạn thảo các văn bản, hợp đồng, biên bản liên quan đến giao dịch bất động sản <br />- Nghiên cứu, cập nhập các văn bản pháp luật, thông tin pháp lý, chế độ, chính sách của nhà nước về lĩnh vực đầu tư, kinh doanh bất động sản. <br />- Thực hiện các công việc khác được phân công.  <br />- Chi tiết công việc sẽ trao đổi cụ thể khi phỏng vấn. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Thù lao, thưởng và chế độ liên quan theo thoả thuận dựa trên năng lực làm việc, các điều kiện và yếu tố ưu tiên mà Ứng viên đáp ứng được. <br />- Làm việc trong môi trường hiện đại, năng động và chuyên nghiệp.  <br />- Được tạo điều kiện để phát huy tối đa năng lực và sự sáng tạo của bản thân.  <br />- Được hưởng đầy đủ các quyền lợi và các chế độ BHXH, BHYT đối với người lao động theo Luật lao động và theo quy định của Công ty. </span></p>', '', 0, 0, 0, '1,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-19', '2010-12-06 23:44:37', 0, '0000-00-00 00:00:00'),
(120, ' 	\r\nNhân viên Kinh doanh Bất động sản ', '-nhan-vien-kinh-doanh-bt-ng-sn-', 156, '', '4', '', 0, 15, '2', 11, 14, 1, 1, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Tốt nghiệp trung cấp trở lên <br />- Am hiểu về thị trường BĐS <br />- Có kinh nghiệm về kinh doanh, ưu tiên các ngành: bảo hiểm, tư vấn tài chính, BĐS, chứng khoán <br />- Có khả năng thuyết phục, đàm phán <br />- Thành thạo vi tính và sử dụng tốt các phần mềm liên quan đến công việc  <br />- Chịu được áp lực công việc  <br />- Sẵn sàng đi công tác </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">-Tư vấn, bán sản phẩm dự án do công ty phân phối hoặc các sản phẩm môi giới lẻ. <br />- Tư vấn thuê, cho thuê nhà lẻ, căn hộ <br />- Tìm kiếm mặt bằng, địa điểm cho thuê, mua và bán hoặc các dự án.. <br />- Gặp gỡ, tiếp xúc để tìm hiểu mục đích, nhu cầu của chủ nhà, thỏa thuận và ký kết hợp đồng mua bán, môi giới, ... với chủ nhà <br />- Tìm kiếm khách hàng để cho thuê các địa điểm do chính mình hoặc do các nhân viên khác tìm được hoặc các địa điểm trống do Công ty thầu lại hoặc sở hữu. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Lương, thưởng, chế độ đãi ngộ hấp dẫn, cạnh tranh  <br />- Các chế độ phúc lợi theo quy định Nhà nước và theo quy định Công ty  <br />- Được cấp đầy đủ các trang thiết bị hiện đại phục vụ cho công việc  <br />- Môi trường làm việc năng động, chuyên nghiệp </span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-30', '2010-12-01 04:06:10', 0, '0000-00-00 00:00:00'),
(121, 'Nhân viên Quản lý chất lượng ISO ', 'nhan-vien-qun-ly-cht-lng-iso-', 157, '', '31', '', 0, 13, '2', 11, 14, 1, 1, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Tốt nghiệp Đại học chính quy chuyên ngành Quản trị kinh doanh, Quản trị chất lượng, Công nghệ thực phẩm. <br />- Có ít nhất 2 năm kinh nghiệm quản lý hệ thống chất lượng theo tiêu chuẩn ISO.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Quản lý hệ thống chất lượng theo tiêu chuẩn ISO. <br />- Công việc cụ thể trao đổi khi phỏng vấn.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Lương thỏa thuận tùy theo năng lực, có nhiều cơ hội thăng tiến. <br />- Được ký HĐLĐ, đóng BHXH, BHYT, BHTN. <br />- Được hưởng đầy đủ chế độ theo luật lao động và quy chế Công ty. <br />- Được thưởng các ngày Lễ, tết, lương tháng 13. <br />- Hằng năm được đi nghỉ mát với Công ty.</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-10', '2010-12-01 04:20:35', 0, '0000-00-00 00:00:00'),
(122, 'Nhà hàng Legend Beer tuyển đầu bếp', 'nha-hang-legend-beer-tuyn-u-bp', 158, '', '31', '', 0, 15, '1', 11, 14, 1, 2, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">-Có bằng cấp đầu bếp tại các trường dạy nghề và đạo tạo đầu bếp chính  <br />-Kinh nghiệm làm việc ít nhất 2 năm tại các nhà hàng cao cấp, đặc biệt là các nhà hàng hải sản  <br />- Nấu thành thạo các món ăn hải sản Âu, Á </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Phụ trách nấu các món ăn hải sản <br />- Chịu trách nhiệm trực tiếp trước Bếp trưởng về nấu các món ăn theo phân công, chịu trách nhiệm về chất lượng, thẩm mỹ món ăn, vệ sinh an toàn thực phẩm <br />- Nhận và quản lý hàng hóa, thực thẩm nguyên liệu được giao trong ca, đảm bảo đủ số lượng và chất lượng sản phẩm trong ca làm việc của mình <br />- Tiếp thu những phản hồi từ phía khách hàng về món ăn do mình nấu, từ đó có những thay đổi phù hợp để có món ăn chất lượng tốt hơn.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Mức lương hấp dẫn, tùy thuộc vào tay nghề và kinh nghiệm làm việc  <br />- Các chế độ khác theo Luật lao động (BHYT, BHXH,...)</span></p>\r\n<p> </p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-30', '2010-12-01 04:47:46', 0, '0000-00-00 00:00:00'),
(123, 'Nhân viên kinh doanh', 'nhan-vien-kinh-doanh', 159, '', '31', '', 0, 15, '1', 11, 14, 1, 1, '14', '', '3 - 5 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Trung thực, chịu áp lực công việc, ưu tiên ứng viên thông thạo địa bàn huyện Đông Anh - Hà Nội.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Bán và giới thiệu sản phẩm của công ty Kinh Đô tại các điểm bán trên địa bàn được phân công. <br />Thực hiện các chế độ báo cáo theo quy định công ty.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Được làm việc trong môi trường làm việc chuyên nghiệp. <br />Được ký HDLD trực tiếp với công ty Kinh Đô. <br />Được hưởng lương, phụ cấp xăng xe và điện thoại, được hưởng các chế độ thưởng theo chế độ của công ty. <br />Được hưởng các chế độ bảo hiểm theo quy định của pháp luật Việt Nam.</span></p>', '', 0, 0, 0, '1,2,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-31', '2010-12-06 23:43:48', 0, '0000-00-00 00:00:00'),
(124, 'Phục vụ, Bartender, đầu bếp', 'phc-v-bartender-u-bp', 160, '', '31', '', 0, 15, '2', 11, 14, 1, 1, '14', '', '1 - 3 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">• Có khả năng làm việc độc lập <br />• Có khả năng phù hợp nhanh <br />• Có khả năng thuyết phục người khác <br />• Có mắt thẩm mỹ cao <br />• Cẩn thận, tỉ mỉ <br />• Chịu được áp lực công việc cao <br />• Có khả năng giao tiếp tốt <br />• Nhanh nhẹn, hoạt bát <br />• Trung thực, thật thà <br />• Ưa nhìn </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Chào đón khách, sắp xếp và hướng dẫn cho khách vào vị trí ngồi <br />Mời và tư vấn cho khách cách chọn đồ ăn và nhận order từ khách <br />Chuyển order từ khách đến bộ phận đồ uống và bếp một cách chính xác và nhanh nhất <br />Phục vụ đồ ăn uống cho khách <br />Làm thủ tục thanh toán cho khách <br />Giải đáp mọi thắc mắc của khách hàng <br />Những công việc khác được giao bởi Quản lý   <br />Có kỹ năng giao tiếp tốt <br />Nhanh nhẹn, hoạt bát, xử lý tình huống tốt <br />Trung thực, nhiệt tình, có tinh thần trách nhiệm </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Công ty đóng toàn bộ BHXH, BHYT <br />- Hỗ trợ tiền gởi xe</span></p>', '', 0, 0, 0, '1,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-15', '2010-12-06 23:38:51', 0, '0000-00-00 00:00:00'),
(125, 'Phụ trách Marketing', 'ph-trach-marketing', 161, '', '11', '', 0, 13, '1', 11, 14, 1, 3, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Tối thiểu 3 năm kinh nghiệm ở vị trí dự tuyển. <br />- Am hiểu chiến lược Marketing thương hiệu và sản phẩm. <br />- Có khả năng làm việc độc lập; Sáng tạo, chủ động trong công việc; <br />- Có khả năng viết tốt, có kỹ năng trình bày thuyết phục</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Xây dựng kế hoạch và chiến lược Marketing thương hiệu SOHACO và sản phẩm. <br />- Đề xuất xây dựng và triển khai các chương trình Marketing, PR. <br />- Tổng hợp và phân tíchd đánh giá hiệu quả các chương trình Marketing sản phẩm và phát triển thương hiệu... </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Thu nhập thoả đáng theo năng lực; <br />- Chế độ BHXH, BHYT, BHTN theo quy định của Nhà nước. <br />- Môi trường làm việc chuyên nghiệp - naăg động - thân thiện. <br />- Có điều kiện để phát triển và thăng tiến trong nghề nghiệp</span></p>', '', 0, 0, 0, '', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-15', '2010-12-06 23:46:23', 0, '0000-00-00 00:00:00'),
(126, 'Nhân viên marketing(KELLY BÙI)', 'nhan-vien-marketingkelly-bui', 163, '', '11', '', 0, 12, '1', 11, 14, 1, 2, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Nam, Nữ <br />- Đã từng làm cho công ty thời trang <br />- Ngoại hình ưa nhìn <br />- Giọng nói dễ nghe, không nói ngọng, không nói lắp, không nói giọng địa phương <br />- Kỹ năng giao tiếp và xử lý tình huống tốt. <br />- Khả năng chịu áp lực công việc cao. <br />- khả năng đàm phán với khách hàng tốt,tạo mối quan hệ lâu dài với khách hàng. <br />- Tốt nghiệp CD trở lên. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;"> Lên kế hoạch triển khai,thực hiện chiến lược marketing của Công ty. <br />- Tư vấn và khai thác  mở rộng thị trường. <br />-Sẽ trao đổi thêm khi phỏng vấn. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Được làm việc trong môi trường chuyên nghiệp năng động. <br />- Có nhiều cơ hội thăng tiến. <br />- Ứng viên trúng tuyển sẽ được hưởng đầy đủ các quyền lợi theo quy định của Công ty <br />- Lương cơ bản + phụ cấp ăn trưa,công việc ổn định </span></p>', '', 0, 0, 0, '', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-15', '2010-12-06 23:47:15', 0, '0000-00-00 00:00:00'),
(127, 'Tuyển GẤP GẤP Nhân viên Sale Marketing cho nhà hàng 5 sao 1911!!Cơ hội việc làm lớn!!!Hot Hot Hot', 'tuyn-gp-gp-nhan-vien-sale-marketing-cho-nha-hang-5-sao-1911c-hi-vic-lam-lnhot-hot-hot', 164, '', '11', '', 0, 12, '1', 11, 14, 1, 1, '14', '', '3 - 5 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Có ít nhất 1 năm kinh nghiệm về Sale Marketing tại các nhà hàng, khách sạn lớn <br />Nam, nữ ngoại hình ưa nhìn <br />Tiếng Anh giao tiếp tốt <br />TN Cao đẳng trở lên <br />Sử dụng vi tính thành thạo <br />Làm giờ hành chính </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Quảng bá thương hiệu của nhà hàng <br />Liên hệ, tìm kiếm đối tác, khách hàng (Trung tâm du lịch, ngân hàng, cơ quan, tổ chức...) cho nhà hàng <br />Chịu trách nhiệm đặt tiệc, suất ăn lớn cho nhà hàng <br />Hỗ trợ công việc marketing của nhà hàng <br />Phối hợp với các bộ phận khác khi cần thiết để đảm bảo sự hài lòng khách hàng đối với dịch vụ của nhà hàng</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Lương cứng + service charge <br />Hưởng đầy đủ các chế độ theo quy định của Luật Lao động( BHYT, BHXH, nghỉ lễ, phép…) <br />Hưởng lương tháng 13 </span></p>', '', 0, 0, 0, '2,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-15', '2010-12-06 23:38:38', 0, '0000-00-00 00:00:00'),
(129, 'Trưởng phòng Marketing', 'trng-phong-marketing', 166, '', '11', '', 0, 13, '1', 11, 15, 1, 5, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- 	Nam tốt nghiệp Đại học trở lên chuyên ngành Marketing, Kinh tế, đối ngoại. <br />-	Thành thạo tin học văn phòng thông dụng (Word, Excel, Power point, Internet, Outlook…) <br />-	Ngoại ngữ: Tiếng Anh giao tiếp thông thường. <br />-	Hiểu biết về Pháp luật liên quan lĩnh vực phụ trách </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">-	Kỹ năng lắng nghe <br />-	Có kiến thức về tâm lý học và hành vi con người <br />-	Khả năng giao tiếp, thuyết trình, thuyết phục tốt. Kỹ năng đàm phán, giải quyết xung đột </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">-	Có kiến thức , kỹ năng về quản lý, lãnh đạo <br />-	Kỹ năng làm việc độc lập, sáng tạo, giải quyết vấn đề và ra quyết định <br />-	Khả năng làm việc dưới áp lực cao. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Tham mưu cho Ban lãnh đạo các vấn đề về Marketing. <br />- Quản lý Phòng Marketing. <br />- Mô tả chi tiết trong quá trình phỏng vấn.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Được làm việc trong môi trường năng động, chuyên nghiệp. Mức thu nhập hấp dẫn. <br />- Cơ hội thăng tiến không hạn chế. <br />- Được hưởng mức lương hấp dẫn, phù hợp với năng lực và các chế độ phúc lợi mở rộng khác như: Ăn trưa, sinh nhật, thăm quan, du lịch ... <br />- Hưởng các chế độ đãi ngộ theo luật lao động như: BHXH, BHYT, BHTN ... </span></p>', '', 0, 0, 0, '2,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-30', '2010-12-06 23:38:58', 0, '0000-00-00 00:00:00'),
(130, 'Tuyển Nhân viên Bán Quảng Cáo', 'tuyn-nhan-vien-ban-qung-cao', 167, '', '11', '', 0, 15, '1', 11, 14, 1, 0, '14', '', '5 - 10 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Nam, Nữ. Tuổi từ 20- 35 tuổi. - Tốt nghiệp trung cấp. - Yêu thích công việc kinh doanh - Ngoại hình ưa nhìn. - Kỹ năng giao tiếp và thuyết trình tốt. - Trung thực, nhiệt tình và trách nhiệm cao trong công việc.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Thực hiện các công tác kinh doanh theo sự phân công của cấp trên.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">-Lương + thưởng + % hoa hồng. <br />- Được bồi dưỡng các nghiệp vụ chuyên môn và nâng cao khả năng ngoại ngữ.  <br />- Các bạn sẽ cảm nhận được thực tế một môi trường làm việc thân thiện, năng động, sáng tạo.  <br />- Được hưởng mức lương, thưởng tương xứng với năng lực, trình độ chuyên môn, kinh nghiệm làm việc.  <br />- Các chế độ theo luật lao động và theo quy chế của Công ty: BHXH, BHYT,….  <br />- Nghỉ chiều T7 và CN</span></p>', '', 0, 0, 0, '', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-05', '2010-12-06 23:46:48', 0, '0000-00-00 00:00:00'),
(131, ' 	 Nhân viên marketing ', '-nhan-vien-marketing-', 168, '', '11', '', 0, 13, '1', 11, 14, 1, 1, '14', '', '3 - 5 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">thuyết trình, giao tiếp, sử dụng máy tính, quản lý công việc, quản lý thời gian</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Tốt nghiệp ĐH chuyên ngành Marketing. <br />- Ưu tiên người có kinh nghiệm tổ chức sự kiện hoặc đã bán hàng đa cấp <br /></span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Làm việc ngay khi trúng tuyển <br />- Thu nhập hấp dẫn, phù hợp năng lực. <br />- Chế độ khen thưởng xứng đáng <br />- Cơ hội thăng tiến cao.</span></p>', '', 0, 0, 0, '', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-15', '2010-12-06 23:47:26', 0, '0000-00-00 00:00:00'),
(132, ' 	 Nhân viên Marketing ', '-nhan-vien-marketing-', 169, '', '11', '', 0, 12, '2', 11, 14, 1, 2, '14', '', '3 - 5 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Trình độ đại học chuyên ngành <br />Có tính sáng tạo, nhạy bén, yêu thích lĩnh vực quảng cáo, truyền thông; linh hoạt, năng động <br />Kỹ năng viết tốt, giao tiếp tốt, kỹ năng đàm phán, hoạch định tổ chức, giám sát. <br />Có hiểu biết về thiết kế, in ấn; <br />Có gu thẩm mỹ tốt, cẩn thận, chăm chút cho công việc.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Tham gia xây dựng thương hiệu, quảng bá cho sản phẩm dịch vụ của Công ty. Xây dựng các kế hoạch kinh doanh <br />Lập kế hoạch phát triển thị phần, xây dựng các chương trình khuyến mại, phân tích thị trường, xây dựng giá bán. <br />Thực hiện quảng bá thương hiệu sản phẩm, PR, quảng cáo trên báo và tạp chí. Theo dõi làm bảng hiệu quảng cáo, tổ chức sự kiện, biên soạn tài liệu phục vụ Marketing <br />Quan hệ tốt với các nhà cung cấp dịch vụ quảng cáo, truyền thông, các cơ quan báo đài.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Được hưởng các chế độ về BHXH, BHYT,... <br />Các ngày nghỉ phép năm, nghỉ Lễ Tết theo quy định. <br />Chế độ điều chỉnh lương 2 lần trong năm</span></p>', '', 0, 0, 0, '2,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-15', '2010-12-06 23:47:35', 0, '0000-00-00 00:00:00'),
(133, ' 	\r\nMarketing salers,Leader Team. ', '-marketing-salersleader-team-', 170, '', '11', '', 0, 12, '1', 11, 14, 1, 0, '14', '', '5 - 10 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Yêu cầu: <br />Làm bán thời gian- hành chính. <br />Trung thực, nhiệt tình, nghiêm túc, cộng tác lâu dài.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Yêu cầu: <br />Làm bán thời gian- hành chính. <br />Trung thực, nhiệt tình, nghiêm túc, cộng tác lâu dài. <br />Hồ sơ bao gồm: <br />+ 03 ảnh 3x4. <br />+ 01 photo CMND. <br />Đối Tượng: Công dân Việt Nam <br />Liên hệ: Mrs HẢO trưởng phòng nhân sự <br />Đ/C: Toà nhà ALICOR - 273 BẠCH ĐẰNG - quận HOÀN KIẾM – Hà Nội <br />ĐT: 01656096866 (Goi dien truoc de sap lich phong van)</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Là tập đoàn Đa Quốc Gia của Hoa Kỳ, đứng đầu về lĩnh vực sản xuất và kinh doanh các sản phẩm cao cấp: Sản phẩm chăm sóc cá nhân, chăm sóc đồ gia dụng, mỹ phẩm cao cấp... Triết lý cơ bản của chúng tôi là mang lại cơ hội kinh doanh giúp mọi người có cuộc sống tốt đẹp hơn. Đây là điều mà tất cả chúng ta đều ước mơ tới. <br /> <br />Hiện nay do nhu cầu phát triển và mở rộng kinh doanh tại Hà Nội và các tỉnh lân cận. Chúng tôi liên tục tìm kiếm và tuyển dụng: <br /> <br />+ Nhân viên bán hàng. <br /> <br />+ Nhân viên chăm sóc khách hàng. <br /> <br />+Quản lý SUPER VISORs <br /> <br />(Các bạn có thể tổ chức bán hàng tại nhà với những sản phẩm ưu việt của công ty chúng tôi đang cung ứng). </span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-31', '2010-12-01 07:28:09', 0, '0000-00-00 00:00:00'),
(134, 'Nhân viên kinh doanh ,Marketing', 'nhan-vien-kinh-doanh-marketing', 171, '', '11', '', 0, 12, '1', 11, 14, 1, 2, '14', '', '5 - 10 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">• Tối thiểu 3 năm kinh nghiệm trong lĩnh vực Sales/Marketing, ưu tiên có kinh nghiệm Sales/Marketing  <br />• Có hiểu biết tốt trong ngành  <br />• Có khả năng giao tiếp tiếng Anh tốt;  <br />• Nhanh nhẹn, hiệu quả trong xử lí tình huống;  <br />• Có khả năng làm việc dưới áp lực công việc cao. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">• Phát triển, giám sát, phối hợp và lên kế hoạch cho hoạt động marketing nhằm đáp ứng nhu cầu của thị trường và hoạt động kinh doanh của công ty; <br />• Đưa ra đánh giá về thị trường sau khi tiến hành nghiên cứu thị trường; <br />• Tìm hiểu những nhu cầu khác nhau của khách hàng và đẩy mạnh công việc chăm sóc khách hàng;  <br />• Thực hiện việc quảng bá đưa hình ảnh Austfeed Việt Nam phổ biến trên thị trường. <br />• Mọi chi tiết công việc sẽ trao đổi cụ thể khi phỏng vấn. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">* Mức lương đảm bảo hấp dẫn, phù hợp với năng lực. <br />* Môi trường làm việc năng động, chuyên nghiệp. <br />* Cơ hội phát triển sự nghiệp bền vững tại công ty. <br />* Hưởng các chế độ đãi ngộ khác từ công ty. </span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-31', '2010-12-01 07:38:29', 0, '0000-00-00 00:00:00'),
(135, 'Nhân viên Marketing trong lĩnh vực y tế', 'nhan-vien-marketing-trong-lnh-vc-y-t', 172, '', '11', '', 0, 15, '1', 11, 14, 1, 1, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Nam/Nữ, dưới 30 tuổi <br />- Tốt nghiệp trung cấp trở lên. <br />- Có ít nhất 1 năm kinh nghiệm, ưu tiên ứng viên có kinh nghiệm marketing trong lĩnh vực y dược. <br />- Năng động, nhiệt tình, cẩn thận, trung thực và chủ động trong công việc. <br />- Có tinh thần trách nhiệm cao trong công việc, ý thức kỷ luật tốt, thái độ hòa nhã với mọi người. <br />- Thành thạo vi tính văn phòng. <br />- Làm việc gắn bó lâu dài cho công ty. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Tiếp xúc các bác sỹ, bệnh viện để giới thiệu về dự án của công ty. Công việc cụ thể trao đổi khi phỏng vấn</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Được đảm bảo đầy đủ các chế độ: BHXH, BHYT, BHTN.. các chế độ đãi ngộ như tham quan, nghỉ mát, lễ, tết, hiếu, hỉ …  <br />- Được làm việc trong môi trường chuyên nghiệp, năng động, cơ hội phát triển tốt. </span></p>', '', 0, 0, 0, '2,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-15', '2010-12-06 23:39:47', 0, '0000-00-00 00:00:00'),
(136, 'Nhân viên Marketing', 'nhan-vien-marketing', 173, '', '11', '', 0, 13, '1', 11, 14, 1, 0, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Nữ, Sinh viên năm thứ ba, năm cuối chuyên ngành Marketing, quản trị kinh doanh của các trường đại học, cao đẳng khối ngành kinh tế. <br />Yêu thích công việc markerting, có khả năng giao tiếp tốt. Có khả năng làm việc độc lập và theo nhóm. <br /> Mô tả công việc: <br />Thực hiện kế hoạch marketing của công ty. Tham gia xây dựng chương trình và kế hoạch nhằm phát triển thương hiệu của công ty và của khách hàng của công ty. Công việc cụ thể sẽ được trao đổi khi phỏng vấn </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Thực hiện Dự án marketing mở rộng khách hàng của công ty. Tham gia thực hiện chương trình và kế hoạch marketing nhằm phát triển khách hàng. Công việc cụ thể sẽ được trao đổi khi phỏng vấn</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Được đào tạo bài bản trong vòng 02 tuần. <br />Lương cứng + Hoa hồng hợp đồng <br />Lương thử việc là 70% lương chính, Thời gian thử việc là 1 tháng</span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Có cơ hội khẳng định bản thân <br />Và nhiều quyền lợi khác </span></p>\r\n<p> </p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-15', '2010-12-01 07:55:44', 0, '0000-00-00 00:00:00'),
(137, 'Nhân viên Marketing', 'nhan-vien-marketing', 174, '', '11', '', 0, 12, '1', 11, 14, 1, 3, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Thành thạo photoshop, corel. <br />- Quản tri website <br />- ...</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Trực tiếp thực hiện hoạt động bán hàng <br />- Tìm kiếm và đánh giá thông tin đối thủ cạnh tranh <br />- Tìm kiếm và quản lý nhà cung cấp dịch vụ quảng cáo <br />- Xây dựng kế hoạch marketing <br />- Quản lý và cập nhật nội dung Website <br />-...</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Lương cơ bản <br />- BHYT + BHXH <br />- Thưởng theo doanh thu</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-30', '2010-12-01 08:00:34', 0, '0000-00-00 00:00:00'),
(138, 'Nhân Viên Marketing', 'nhan-vien-marketing', 175, '', '11', '', 0, 13, '2', 11, 14, 1, 2, '14', '', '5 - 10 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Tối thiểu 2 năm kinh nghiệm trong công việc ứng tuyển <br />- Chịu được áp lực công việc và có khả năng làm việc độc lập. <br />- Giao tiếp tốt bằng tiếng Anh. <br />- Nhanh nhẹn, siêng năng. <br />- Tuổi từ 24 đến 30 </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Triển khai kế hoạch Marketing, các chương trình khuyến mãi, sự kiện.. <br />- Sẽ trao đổi công việc cụ thể hơn khi phỏng vấn</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Theo quy định của Luật lao động và các chế độ khác của Công ty.</span></p>', '', 0, 0, 0, '1,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-20', '2010-12-06 23:47:44', 0, '0000-00-00 00:00:00'),
(139, ' 	\r\nNhân viên marketing ', '-nhan-vien-marketing-', 176, '', '11', '', 0, 12, '1', 11, 14, 1, 1, '14', '', '1 - 3 triệu', '', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty Cổ phần Fiona Việt Nam tuyển nhân viên marketing, có kinh nghiệm từ 1 năm trở lên trong lĩnh vực marketing, quảng bá thương hiệu thời trang, mỹ phẩm hoặc tương tự, sử dụng thành thạo máy tính, tốt nghiệp các trường đại học, cao đẳng chuyên ngành ngoại ngữ, kinh tế, quản trị kinh doanh, marketing…. <br />Thời gian làm việc: 8:00 sáng – 5:00 chiều, nghỉ chủ nhật <br />Thu nhập: 4 – 6 triệu  <br />Hồ sơ gửi tới: 107 Thái Hà, Đống Đa, Hà Nội. Hoặc liên hệ chị Nhung 0912028668; nhung8668@hotmail.com để hẹn phỏng vấn. Hồ sơ gồm: Đơn xin việc, CV miêu tả rõ quá trình làm việc trước đây, sơ yếu lý lịch dán ảnh 3x4, photo bằng tốt nghiệp ĐH/CĐ; photo chứng minh nhân dân, photo hộ khẩu </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Trao đổi trực tiếp</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-30', '2010-12-01 08:13:40', 0, '0000-00-00 00:00:00');
INSERT INTO `jos_properties_products` (`id`, `name`, `alias`, `agent_id`, `agent`, `type`, `ref`, `cid`, `eid`, `sid`, `jid`, `pid`, `cyid`, `jobyear`, `salary`, `m_salary`, `i_curren`, `description`, `text`, `quyenloi`, `panoramic`, `published`, `ordering`, `jpacket`, `jp`, `p1`, `p2`, `p3`, `date1`, `date2`, `date3`, `hits`, `startdate`, `enddate`, `refresh_time`, `checked_out`, `checked_out_time`) VALUES
(140, 'Nhân viên kinh doanh', 'nhan-vien-kinh-doanh', 177, '', '11', '', 0, 13, '1', 11, 14, 1, 1, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">-	Tốt nghiệp đại học chuyên nghành kinh tế hoặc CNTT  <br />-	Biết sử dụng tốt tiếng Anh là một lợi thế. <br />-	Có khả năng giao tiếp tốt. <br />-	Nhanh nhẹn hoạt bát, có khả năng làm việc độc lập hoặc theo nhóm </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Tìm kiếm khách hàng mới, chủ động làm việc trực tiếp với khách hàng. Kinh doanh dịch vụ cho thuê máy chủ, dịch vụ co-location và các giải pháp lưu trữ cao cấp . Phát triển kinh doanh, tìm kiếm dự án, phát triển thị trường kinh doanh sản phẩm. . Chào giá, chăm sóc khách hàng, theo dõi hợp đồng/dự án và làm hồ sơ thầu . Tư vấn giải pháp, lựa chọn thương hiệu, đàm phán ký kết hợp đồng . Hoàn thành các chỉ tiêu kinh doanh đề ra.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Tìm kiếm khách hàng mới, chủ động làm việc trực tiếp với khách hàng. Kinh doanh dịch vụ cho thuê máy chủ, dịch vụ co-location và các giải pháp lưu trữ cao cấp . Phát triển kinh doanh, tìm kiếm dự án, phát triển thị trường kinh doanh sản phẩm. . Chào giá, chăm sóc khách hàng, theo dõi hợp đồng/dự án và làm hồ sơ thầu . Tư vấn giải pháp, lựa chọn thương hiệu, đàm phán ký kết hợp đồng . Hoàn thành các chỉ tiêu kinh doanh đề ra.</span></p>', '', 0, 0, 0, '1,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-31', '2010-12-06 23:43:58', 0, '0000-00-00 00:00:00'),
(141, ' 	\r\nNhân viên thị trường (Marketing) ', '-nhan-vien-th-trng-marketing-', 178, '', '11', '', 0, 12, '1', 11, 14, 1, 1, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- TNĐH, CĐ chuyên ngành kinh tế, Quản trị kinh doanh, Marketing , Hướng dẫn, Ngoại ngữ,  <br />- Có tối thiểu 1 năm kinh nghiệm ở vị trí tương đương  <br />- Có kỹ năng thuyết trình, đàm phán và thuyết phục khách hàng, tổng hợp và phân tích tốt các vấn đề có liên quan đến công việc <br />- Có hiểu biết kiến thức về Marketing trong lĩnh vực kinh doanh du lịch và các loại hình kinh doanh dịch vụ của Công ty, <br />- Sử dụng thành thạo tin học văn phòng và giao tiếp tốt bằng tiếng Anh hoặc tiếng Trung <br />- Sẵn sàng đi công tác ngoại tỉnh trong nước và nước ngoài, hình thức ưa nhìn, tự tin, nhanh nhẹn, trung thực, chịu được áp lực cao trong công việc, có phương tiện đi lại (ưu tiên những ứng viên đã kinh nghiệm ở các vị trí dự tuyển) </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Xây dựng kế hoạch tiếp thị khách hàng tiềm năng, tiếp xúc khách hàng trực tiếp và gián tiếp, triển khai và thực hiện các chiến lược tiếp thị, giới thiệu sản phẩm và mở rộng thị trường kinh doanh của Công ty <br />- Củng cố và phát triển mối quan hệ khách hàng doanh nghiệp, cơ quan quản lý nhà nước, các bộ, ngành, các tổ chức nhằm khai thác tối đa nhu cầu của khách hàng <br />- Triển khai các nghiệp vụ Marketing, tổ chức  thực hiện và hoàn thành các chỉ tiêu kinh doanh  <br />- Duy trì quan hệ khách hàng, các đại lý, các công ty du lịch trong cả nước. Sử dụng các nghiệp vụ Marketing, chăm sóc khách hàng cũ và phát triển khách hàng, đối tác mới, phối hợp với các bộ phận chức năng của phòng và của Công ty để chào bán các sản phẩm dịch vụ du lịch Outbound, Nội địa, Inbound cho khách hàng trong và ngoài nước </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Lương trả theo thỏa thuận + Phụ cấp cơm trưa tại VP + thưởng hoa hồng kinh doanh theo doanh số và lợi nhuận, được hưởng đầy đủ các chế độ bảo hiểm xã hội, bảo hiểm y tế theo qui định của nhà nước và các chế độ đãi ngộ theo qui định của Công ty.</span></p>', '', 0, 0, 0, '1,3,2,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-20', '2010-12-01 08:20:25', 0, '0000-00-00 00:00:00'),
(142, ' 	 Nhân viên PR/ marketing ', '-nhan-vien-pr-marketing-', 179, '', '11', '', 0, 12, '1', 11, 14, 1, 1, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Bắt buộc có phương tiện đi lại <br />- Có kinh nghiệm làm việc trong lĩnh vực khai thác,tổ chức sự kiện. <br />- Không tuyển nhân viên mới ra trường</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">-Tư vấn, chăm sóc, tìm kiếm khách hàng tiềm năng trong lĩnh vực quảng cáo, truyền thông sự kiện, in ấn... cho công ty. <br />-Công việc qua điện thoại và giặp gỡ trực tiếp khách hàng. <br />-Lên kịch bản, và làm các công việc có liên quan. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">-Thu nhập cao, hưởng theo năng lực. <br />-Chế độ bảo hiểm+thưởng+du lịch+nghỉ phép và các chế độ đãi ngộ khác tuân theo luật lao động. <br />-Môi trường làm việc chuyên nghiệp,năng động. </span></p>', '', 0, 0, 0, '1,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-29', '2010-12-06 23:37:04', 0, '0000-00-00 00:00:00'),
(143, 'PR Truyền Thông', 'pr-truyn-thong', 180, '', '11', '', 0, 17, '1', 11, 14, 1, 5, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Không yêu cầu  trình độ</span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"><br /></span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">PR các dự án truyền thông, chăm sóc khách hàng, tìm kiếm khách hàng tiềm năng( sẽ trao đổi kỹ tron gkhi phỏng vấn)</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Làm việc trong môi trường năg động,chuyên nghiệp, có cơ hội phát triển tốt</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-20', '2010-12-01 12:06:50', 0, '0000-00-00 00:00:00'),
(144, ' 	\r\nTrưởng phòng marketing ', '-trng-phong-marketing-', 181, '', '11', '', 0, 13, '1', 11, 15, 1, 5, '14', '', '5 - 10 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Phỏng vấn: 9h30’ hàng ngày, có thể liên hệ để hẹn lịch phỏng vấn trực tiếp với TGĐ.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Tuyển trưởng phòng marketing, yêu cấu tốt nghiệp đại học khối kinh tế, từ 30 tuổi trở lên, có kinh nghiệm phát triển thị trường ô tô cũ và ô tô mới nhập khẩu. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Lương cao từ 5 triệu – 10 triệu + thưởng nếu đạt yêu cầu tuyển dụng. <br />Phỏng vấn: 9h30’ hàng ngày, có thể liên hệ để hẹn lịch phỏng vấn trực tiếp với TGĐ. </span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-15', '2010-12-01 12:11:21', 0, '0000-00-00 00:00:00'),
(145, 'Nhân viên Marketing', 'nhan-vien-marketing', 182, '', '11', '', 0, 13, '1', 11, 14, 1, 1, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Tốt nghiệp Đại học trở lên.  <br />- Có kinh nghiệm ở vị trí tương đương.  <br />- Có kiến thức chuyên về marketing ứng dụng, quan hệ công chúng, quản lý và phát triển thương hiệu. <br />- Kỹ năng giao tiếp, đàm phán, thuyết trình tốt  <br />- Có khả năng lập, hoạch định các chương trình marketing, các chương trình quảng bá hình ảnh của Công ty  <br />- Thành thạo tin học văn phòng và các ứng dụng  <br />- Có khả năng làm việc độc lập, theo nhóm  <br />- Có tinh thần trách nhiệm trong công việc và phong cách làm việc chuyên nghiệp  <br />- Chịu được áp lực công việc. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Nghiên cứu, triển khai thực hiện các chương trình quảng cáo, khuyến mãi, nhằm đảm bảo kế hoạch kinh doanh và chiến lược phát triển thương hiệu của Công ty <br />- Phân tích tình hình và xu thế phát triển của thị trường để tham mưu cho Trưởng phòng chiến lược phát triển marketing của Công ty. <br />- Phát triển và quản lý dữ liệu marketing bao gồm nghiên cứu thị trường, khai thác dữ liệu khách hàng để có thông tin về khách hàng hiện tại và khách hàng tiềm năng nhằm phục vụ cho việc triển khai các kế hoạch marketing của Công ty <br />- Quản lý thương hiệu, chuẩn hóa hình ảnh thương hiệu, đảm bảo tính chất của thương hiệu được quản lý đúng đắn và nhất quán (trong nội bộ và trước công chúng) bao gồm quảng bá, thiết kế tất cả các vật phẩm khác <br />- Thiết lập và giám sát tiêu chuẩn hệ thống nhận diện bao gồm: Logo, bảng hiệu, pano quảng cáo, ấn phẩm, vật phẩm, hoạt động hoạt náo và cổ động, sự kiện ... nhằm tiếp cận, thông tin cho khách hàng và kích thích mua hàng <br />- Thiết kế, tổ chức thực hiện các chương trình hoặc hoạt động liên quan đến việc phát triển thương hiệu, xây dựng hình ảnh thương hiệu <br />- Thực hiện các công việc liên quan khác. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Theo quy định của Công ty và Luật Lao động</span></p>', '', 0, 0, 0, '1,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-20', '2010-12-06 23:37:42', 0, '0000-00-00 00:00:00'),
(146, 'Công nhân kỹ thuật ', 'cong-nhan-k-thut-', 184, '', '33', '', 0, 15, '1', 11, 14, 1, 0, '14', '', 'thỏa thuận', '', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Thực hiện các công việc về lắp đặt và sửa chữa, bảo dưỡng thay thế các phụ tùng cơ giới, thiết bị công nghiệp. <br /> <br />Yêu cầu chung: <br />-	Nam, tuổi không quá 35. <br />-	Tốt nghiệp trung cấp hoặc các trường dạy nghề ngành cơ khí, máy xây dựng. <br />-	Có hiểu biết về hệ thống truyền động thủy lực công nghiệp hoặc kinh nghiệm sửa chữa, chế tạo các sản phẩm cơ khí. <br />-	Sẵn sàng đi công tác ngoại tỉnh theo yêu cầu.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Các ứng viên được tuyển dụng sẽ được đảm bảo một mức lương cạnh tranh, hoạt động trong môi trường chuyên nghiệp với cơ hội thăng tiến cùng sự phát triển của công ty. <br />Sau thời gian thử việc, ứng viên được ký Hợp đồng lao động có kỳ hạn ít nhất 1 năm, được tham gia BHXH, BHYT, hưởng các quyền lợi của người lao động theo đúng Luật Lao động Việt Nam.</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-15', '2010-12-02 01:19:06', 0, '0000-00-00 00:00:00'),
(147, 'Công nhân lắp đặt, Tập đoàn sản xuất cửa sổ nhựa Hà Nội - Hanowindow uPVC', 'cong-nhan-lp-t-tp-oan-sn-xut-ca-s-nha-ha-ni-hanowindow-upvc', 185, '', '44', '', 0, 15, '1', 11, 14, 1, 0, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Tốt nghiệp PTTH trở lên, không có tiền án, tiền sự, không nghiện các loại chất kích thích bị Nhà nước cấm (Nếu vi phạm hoặc cố tình khai không đúng sự thật có thể bị điều tra hoặc truy tố khi gây hậu quả nghiêm trong). <br />- Sức khỏe tốt, có thể đi công tác ngoại tỉnh.  <br />- Ưu tiên nam giới, cao từ 1,60m trở lên, không nói lắp.  <br />- Khả năng giao tiếp khách hàng, truyền đạt thông tin và giải quyết tốt những vấn đề thuộc trách nhiệm khi càn thiệt.  <br />- Trung thực, sáng tạo, cẩn thận và kiên trì</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">* Chịu sự điều hành, phân công công việc của Đội trưởng Đội lắp đặt &amp; Phó Giám đốc Nhà máy phụ trách Bộ phận lắp đặt, Giám đốc Nhà máy trong trường hợp cần thiết. <br />* Hoàn thành công việc lắp đặt sản phẩm tại công trình của Khách hàng theo đúng quy trình, quy phạm kỹ thuật đã được đào tạo trong khóa học (tại VN hoặc nước ngoài). <br />* Chi tiết công việc sẽ được trao đổi cụ thể khi phỏng vấn.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Mức thu nhập phù hợp với năng lực.  <br />- BHXH, BHYT theo quy định và chế độ thưởng của công ty.  <br />- Môi trường làm việc năng động, có nhiều cơ hội học hỏi, thăng tiến và phát triển nghề gnhiệp.  <br />- Công ty có xe đưa đón 100% khi đi công tác ngoại tỉnh.  <br />- Có thể được cử đi học tại nước ngoài với chi phí do Doanh nghiệp đài thọ 100%.  <br /> <br />* Chú ý: Tuyệt đối không liên hệ điện thoại!</span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-28', '2010-12-02 01:28:49', 0, '0000-00-00 00:00:00'),
(148, 'Thiết kế kỹ thuật', 'thit-k-k-thut', 186, '', '33', '', 0, 13, '1', 11, 14, 1, 3, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Giới tính: Nam.  <br />- Kinh nghiệm: Tối thiểu 01 năm trong lĩnh vực xây dựng hoặc thiết kế.  <br />- Tốt nghiệp: Trường Bách Khoa, Xây Dựng, kỹ thuật...  <br />- Kỹ năng: Giao tiếp,   <br />- Yêu cầu chung : Trung thực, ham học hỏi, nhiệt tình.- Phần mềm - AutoCAD  <br />- Phần mềm - MS Office </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Thiết kế các bản vẽ kỹ thuật và các thông số kỹ thuật.  <br />- Tập dự toán nguyên vật liệu <br />- Phối hợp với các bộ phận khác triển khai dự án theo thiết kế <br />- Tư vấn thay đổi thiết kế nếu có. <br />- Đào tạo cán bộ công nhân các kỹ năng cơ khí </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Thu nhập: Lương cơ bản + thưởng doanh thu.  <br />- Bảo hiểm: Bảo hiểm xã hội, bảo hiểm y tế.  <br />- Cơ hội: Trưởng thành trong cuộc sống và công việc.  <br />- Chế độ khác: Tham quan và học tập phục vụ công việc ở trong và ngoài nước. </span></p>\r\n<p> </p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-30', '2010-12-02 01:35:26', 0, '0000-00-00 00:00:00'),
(149, 'Thiết kế nội thất', 'thit-k-ni-tht', 188, '', '2', '', 0, 13, '1', 11, 14, 1, 1, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Thành thạo các phần mềm ứng dụng:2D,3D,Photoshop,Max, Sketup... <br />- Nhanh nhẹn nhiệt tình trong công việc...</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Thực hiện các công việc thiết kế phương án kiến trúc cho các dự án <br />- Diễn hoạ phương án thiết kế  <br />- Triển khai Hồ sơ thiết kế bản vẽ thi công công trình cho toàn bộ hoặc một phần công việc thuộc dự án....  <br />Công việc sẽ được trao đổi cụ thể hơn khi phỏng vấn.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Mức lương thoả thuận phù hợp với khả năng <br />- Có các chế độ BHXH, BHYT,  <br />- Thưởng theo quy chế công ty </span></p>', '', 0, 0, 0, '1,2,3,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-15', '2010-12-02 01:56:32', 0, '0000-00-00 00:00:00'),
(150, 'Chuyên Viên Thiết Kế Đồ Họa', 'chuyen-vien-thit-k-ha', 189, '', '2', '', 0, 12, '1', 11, 14, 1, 1, '14', '', '4 triệu - 5 triệu', '<p>Yêu cầu công việc:<br /> <br /> - Tốt nghiệp cao đẳng trở lên, có ít nhất 01 năm kinh nghiệm trong lĩnh vực thiết kế, đồ hoạ.<br /> - Sử dụng thành thạo các phần mềm đồ họa: Adobe Photoshop, Adobe Illustrator, Adobe Flash.<br /> - Có khả năng tư duy sáng tạo, phân tích nội dung thiết kế.<br /> - Có khả năng cảm nhận và thể hiện thẩm mỹ tốt<br /> - Biết làm media, xử lý phim, nhạc hoặc dựng mô hình 3D  là một lợi thế.<br /> - Tác phong làm việc chuyên nghiệp, siêng năng, nhiệt tình, cầu thị trong công việc...</p>', '<p>Thiết kế các ấn phẩm phục vụ cho các chương trình, sự  kiện, hội thảo  và các hoạt động thường niên của Tập đoàn như: Profile,  Poster,  bangroll, backdrop, brochure, tờ rơi, thiệp mời, sổ tay ….<br /> - Thiết kế các marquette PR/quảng cáo về thương hiệu Tập đoàn trên các phương tiện truyền thông  đại chúng<br /> - Thiết kế các mẫu quảng cáo trực tuyến: banner, images, flash, các đoạn  video clip … để phục vụ vụ cho công tác truyền thông về website và  thương hiệu Tập đoàn trên Internet.<br /> - Thực hiện các công việc khác khi có yêu cầu</p>', '<p>- Mức lương thưởng cao, cạnh tranh theo trình độ, kinh nghiệm và năng lực của ứng viên. <br /> - Môi trường làm việc trẻ trung, thân thiện, linh hoạt, phát huy sự chủ động và sáng tạo.<br /> - Được hưởng các chế độ BHXH, BHYT, và các chế độ phúc lợi của Tập đoàn.<br /> - Ứng viên xuất sắc có nhiều cơ hội thăng tiến trong Tập đoàn.</p>', '', 0, 0, 0, '2,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-31', '2010-12-06 23:45:30', 0, '0000-00-00 00:00:00'),
(151, 'Chăm sóc khách hàng - Nhân viên kinh doanh', 'chm-soc-khach-hang-nhan-vien-kinh-doanh', 196, '', '22', '', 0, 17, '2', 11, 18, 1, 0, '14', '', 'thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Làm bên ngành quảng cáo và có mối quan hệ với các công ty là một lợi thế </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">-Tìm kiếm khách hàng <br />-Chăm sóc và mở rộng mối quan hệ khách hàng </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">-Có cơ hội thăng tiến -Mức lương xứng đáng với năng lực -Môi trường làm việc tốt</span></p>', '', 0, 0, 0, '1,2,', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-10', '2010-12-06 23:41:56', 0, '0000-00-00 00:00:00'),
(152, 'NHÂN VIÊN BÁN HÀNG', 'nhan-vien-ban-hang', 197, '', '22', '', 0, 15, '14,5,', 11, 18, 1, 1, '14', '', '3 - 5 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Có kinh nghiệm 1năm bán hàng(chưa có kinh nghiệm sẽ được đào tạo),đã bán hàng thực phẩm là một lợi thế.Ưu tiên các ứng viên liên hệ và nộp hồ sơ sớm.</span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Lấy đơn hàng và bán hàng trực tiếp các sản phẩm bánh mềm kẹp kem,bánh mềm phủ sôcôla...tại Quận Đống Đa,Thanh Xuân.Chăm sóc các cửa hàng đã có sản phẩm của Công ty và tăng cường độ phủ. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Được hưởng lương cứng + phụ cấp 2,5triệu;thưởng từ 1-2,5triệu.Được cấp áo đồng phục,mũ bảo hiểm,thùng đựng hàng và cặp file.Được nghỉ chủ nhật và các ngày lễ tết theo quy định của nhà nước.Được đóng BH. </span></p>', '', 0, 0, 0, '1,', 1, 2, 0, '2011-01-31', '2011-01-28', '0000-00-00', 0, '0000-00-00', '2010-12-26', '2011-01-30 09:46:22', 0, '0000-00-00 00:00:00'),
(153, 'Nhân viên tiếp thị - quảng cáo', 'nhan-vien-tip-th-qung-cao', 207, '', '22', '', 0, 12, '8,1,3,', 11, 18, 1, 1, '14', '', 'Lương,Hoa Hồng', '', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Xin các bạn ứng tuyển vui lòng trả lời các nội dung sau cho nhà tuyển dụng: <br />Nguồn tin nào bạn biết được nhu cầu tuyển dụng <br />Bạn nói rỏ, nói đúng về chúng tôi nơi đang tuyển dụng <br />Bạn sẽ làm công việc gì ở nới đó <br />Bạn làm nó như thế nào <br />Bạn là ai, năng lực, ưu điểm, cá tính <br />Bạn mong muốn gì ở nơi làm việc  này.  <br />Bạn sẽ có được cái gì sau 1 năm, 2 năm, 5 năm <br />Viết thành bài luận và gửi về địa chỉ: Haidangtcthv@yahoo.com; hxdtphcm@yahoo.com.vn </span></p>', '', '', 0, 0, 0, '', 1, 2, 3, '2011-01-25', '2011-01-26', '2011-01-31', 0, '0000-00-00', '2011-01-31', '2011-01-30 13:59:55', 0, '0000-00-00 00:00:00'),
(154, ' 	 Tuyển nhân viên chăm sóc khách hàng VIETTEL ', '-tuyn-nhan-vien-chm-soc-khach-hang-viettel-', 208, '', '13', '', 0, 11, '25,', 11, 18, 1, 1, '14', '', '3 - 5 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;"> Đơn Xin việc viết tay trên giấy A4, CMND, Hộ khẩu, SYLL(dán hình và đóng dấu giáp lai), Khám Sức Khỏe(mộc tròn), Khai sinh, Bằng cấp liên quan, 4 ảnh 3x4, 4 ảnh 4x6 ( tất cả hồ sơ đều phải công chứng trong 6 tháng). </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Nộp Hồ Sơ và Phỏng Vấn trực tiếp tại:  <br /> <br />- Tại TP.HCM: Số 33 Lê Trung Nghĩa – Phường 12 – Q. Tân Bình – TPHCM. <br /> Điện thoại: 08.6255.9999 Line- 805, 813  Hotline: 0908.112656 Mr.Duy <br /> 0987797972 Mr.Cầu </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">-    Hỗ trợ, tư vấn khách hàng của mạng điện thoại di động Viettel. <br />-	Làm ca luân chuyển, nghỉ 1 ngày/ tuần. <br />-	Nơi làm việc: Q. Tân Bình – TPHCM. <br /> <br />Yêu cầu: <br /> + Nam/ nữ từ 18 đến 29 tuổi. <br /> + Tốt nghiệp Trung Cấp trở lên, không phân biệt chuyên ngành. <br /> + Giọng nói chuẩn, giao tiếp tốt. <br /> + Tin học căn bản. </span></p>', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Lương khởi điểm : Trên 2.500.000 VNĐ <br /> -  Các khoản phụ cấp hàng tháng : <br /> -  Thưởng năng suất công việc  <br /> -  Tăng cường 200% lương (nếu nhân viên có nhu cầu ).  <br /> Thưởng hàng năm : các ngày lễ lớn trong năm.  <br />- Tăng lương cơ bản theo thâm niên 6 tháng/lần. <br /> <br />Quyền lợi : <br />-    Làm việc trong môi trường chuyên nghiệp, thường xuyên được đào tạo. <br />-    Hưởng đầy đủ BHXH, BHYT, Bảo Hiểm thất nghiệp theo chế độ của nhà nước. <br />-    Cơ hội vào biên chế của VIETTEL . </span></p>', '', 0, 0, 0, '', 0, 2, 3, '2010-12-16', '2010-12-18', '2010-12-20', 0, '0000-00-00', '2010-12-31', '2011-01-30 09:47:27', 0, '0000-00-00 00:00:00'),
(157, 'Kế toán tổng hợp', 'k-toan-tng-hp', 222, '', '23', '', 0, 11, '5,6,', 11, 18, 1, 1, '14', '', 'Thoả thuận', '<p>- Kỷ năng nhạy bén, Chăm chỉ , nhiệt tình, thật thà, tỷ mỉ, cẩn thận</p>\r\n<p>- Biết sử dụng thành thạo máy tính văn phòng.</p>\r\n<p>- Giao tiếp tốt.</p>', '<p>Quản lý việc thu chi,  hoàn thành hồ sơ kế toán công ty</p>', '<p>Lương, thưởng, các chế độ theo quy định của nhà nước và của Công ty.</p>', '', 0, 0, 0, '', 1, 2, 3, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2010-12-31', '2011-02-18 08:26:59', 0, '0000-00-00 00:00:00'),
(158, 'Kế toán tổng hợp', 'k-toan-tng-hp', 230, '', '23', '', 0, 12, '5,', 11, 18, 1, 1, '14', '', 'Thoả thuận', '<p>Văn phòng dịch vụ kế toán Hải Linh đang tìm người có 3 chữ T: Tâm huyết, Tài năng, Trình độ để chung tay xây dựng một Trung tâm dịch vụ kế toán chất lượng và hiệu quả. <br />Tâm huyết: Chia sẻ quan điểm với Hải Linh. <br />Tài năng: Thành tích trong quá khứ. <br />Trình độ: Bằng cấp. Trên thực tế hải Linh không quan trọng việc bạn bằng cấp như thế nào. Quan trọng là khả năng nắm bắt công việc của bạn. <br />Hãy liên hệ với chúng tôi để cùng thể hiện khả năng bản thân mình.</p>', '<p>Có kinh nghiệm và kỹ năng về các đầu việc sau: <br />- Thực hiện các nghiệp vụ kế toán tổng hợp, công nợ. <br />- Kiểm tra, theo dõi các khoản thu, trả đảm bảo đúng yêu cầu và tiến độ công việc. <br />- Thực hiện các công tác nghiệp vụ kế toán theo quy định của Công ty và pháp luật hiện hành. <br />- Tính toán, quyết toán kết quả hoạt động kinh doanh các mảng phụ trách. <br />- Đảm bảo các công việc hoàn thành đúng thời hạn quy định. <br />- Các công việc phát sinh khác của Công ty và Bộ phận theo yêu cầu.</p>', '<p>* Môi trường làm việc năng động chuyên nghiệp. <br />* Có mức thu nhập cạnh tranh (trả theo năng lực) <br />* Có cơ hội tham gia các hoạt động đào tạo nâng cao trình độ. <br />* Các quyền lợi khác: du lịch nghỉ mát hàng năm, tham gia các hoạt động vui chơi giải trí… <br />* Được đóng BHXH, BHYT, BHTN…</p>', '', 0, 0, 0, '', 1, 0, 3, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '2011-01-30', '2011-02-18 08:27:05', 0, '0000-00-00 00:00:00'),
(159, 'Quản trị Mạng', 'qun-tr-mng', 228, '', '66', '', 0, 12, '12,5,', 11, 18, 1, 2, '14', '', 'Thỏa Thuận', '<p>Kỹ năng làm việc độc lập.</p>', '', '<p>Được hưởng BHXH, BHYT, và thưởng theo quy định của Công ty</p>', '', 0, 0, 0, '', 1, 2, 3, '2011-01-10', '2011-01-10', '2011-01-10', 0, '0000-00-00', '2011-01-10', '2011-01-30 09:46:03', 0, '0000-00-00 00:00:00'),
(160, 'Nhân viên văn phòng', 'nhan-vien-vn-phong', 231, '', '59', '', 0, 11, '1', 11, 18, 1, 1, '14', '', 'Thỏa Thuận', '', '', '', '', 0, 0, 0, '', 1, 0, 3, '2010-11-04', '2011-02-25', '1900-01-11', 0, '0000-00-00', '2011-01-04', '2011-02-19 08:57:02', 0, '0000-00-00 00:00:00'),
(161, 'lap trinh php 1', 'lap-trinh-php-1', 83, '', '45', '', 0, 14, '10,2,1,', 11, 17, 1, 3, '14', '', '400$', '<p>Kinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiết</p>', '<p>Mô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việc</p>', '<p>Quyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởng</p>', '', 0, 0, 0, '', 1, 2, 3, '2011-01-31', '2011-01-31', '2011-01-31', 0, '0000-00-00', '2011-01-31', '2011-02-19 09:37:11', 0, '0000-00-00 00:00:00'),
(162, 'Vị trí tuyển dụng', 'v-tri-tuyn-dng', 83, '', '55', '', 0, 14, '3,4,2,', 13, 13, 1, 0, '14', '', '', '<p>Kinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiết</p>', '<p>Mô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việcMô tả chi tiết công việc</p>', '<p>Quyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởngQuyền lợi được hưởng</p>', '', 1, 0, 0, '', 0, 0, 0, '2011-01-28', '2011-01-29', '2011-01-30', 0, '0000-00-00', '2012-01-28', '2011-02-19 10:03:53', 0, '0000-00-00 00:00:00'),
(163, 'test 123', 'test-123', 270, '', '12', '', 0, 14, '4,3,2,', 12, 13, 1, 3, '14', '', '', '<p>Kinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiếtKinh nghiệm/Kỹ năng chi tiết</p>', '', '', '', 0, 0, 0, '', 1, 2, 3, '2011-02-24', '2011-02-23', '2011-02-28', 0, '0000-00-00', '2011-02-28', '2011-02-19 09:37:31', 0, '0000-00-00 00:00:00'),
(164, 'dsa', 'dsa', 83, '', '12', '', 0, 14, '1,2,3,', 12, 13, 1, 2, '14', '', '3432', '<p>sadsa</p>', '<p>ưtrui88oi</p>', '<p>dsadsa</p>', '', 1, 0, 0, '', 1, 2, 3, '2011-03-08', '2011-03-08', '2011-03-08', 0, '0000-00-00', '2011-03-18', '2011-03-08 15:17:46', 0, '0000-00-00 00:00:00'),
(165, 'sdsdsd', 'sdsdsd', 83, '', '12', '', 0, 0, '1,2,3,', 11, 14, 1, 0, '14', '', '', '<p>sdsdsds</p>', '<p>sdsdsds</p>', '<p style="text-align: left;">sdsdssdsd</p>', '', 1, 0, 0, '', 1, 2, 0, '2011-03-10', '2011-03-15', '0000-00-00', 0, '0000-00-00', '2011-03-23', '2011-03-09 09:35:22', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_product_category`
--

CREATE TABLE IF NOT EXISTS `jos_properties_product_category` (
  `productid` int(11) NOT NULL default '0',
  `categoryid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`productid`,`categoryid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_properties_product_category`
--

INSERT INTO `jos_properties_product_category` (`productid`, `categoryid`) VALUES
(2, 3),
(3, 11),
(4, 11),
(9, 11),
(23, 11),
(25, 11),
(55, 3),
(58, 9),
(60, 9),
(61, 9),
(63, 9),
(64, 9),
(65, 44),
(66, 9),
(67, 44),
(68, 44),
(69, 44),
(70, 44);

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_profiles`
--

CREATE TABLE IF NOT EXISTS `jos_properties_profiles` (
  `id` int(6) NOT NULL auto_increment,
  `mid` int(6) NOT NULL default '0',
  `salutation` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL default '',
  `alias` varchar(255) NOT NULL,
  `c_name` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL default '',
  `con_name` varchar(100) NOT NULL,
  `c_summary` text NOT NULL,
  `address1` varchar(50) NOT NULL default '',
  `cyid` smallint(6) NOT NULL,
  `phone` varchar(20) NOT NULL default '',
  `fax` varchar(20) NOT NULL default '',
  `mobile` varchar(20) NOT NULL default '',
  `web` varchar(255) NOT NULL default '',
  `credit` int(11) NOT NULL,
  `type_credit` varchar(10) NOT NULL,
  `desc_credit` varchar(255) NOT NULL,
  `gcviec` varchar(50) NOT NULL,
  `goicv` varchar(50) NOT NULL,
  `idgoi_cv` int(11) NOT NULL,
  `goi_cv` tinyint(2) NOT NULL,
  `xem_cv` tinyint(4) NOT NULL,
  `startdate` date NOT NULL,
  `enddate` date NOT NULL,
  `show_name` varchar(2) NOT NULL,
  `show_phone` varchar(2) NOT NULL,
  `show_fax` varchar(2) NOT NULL,
  `show_email` varchar(2) NOT NULL,
  `show_addr` varchar(2) NOT NULL,
  `show_packet` tinyint(4) NOT NULL,
  `end_datebuy` date NOT NULL,
  `image` varchar(70) NOT NULL default '',
  `logo_image` varchar(70) NOT NULL default '',
  `logo_image_large` varchar(70) NOT NULL default '',
  `published` tinyint(1) NOT NULL default '0',
  `ordering` int(3) NOT NULL default '0',
  `checked_out` tinyint(1) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `mid` (`mid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=150 ;

--
-- Dumping data for table `jos_properties_profiles`
--

INSERT INTO `jos_properties_profiles` (`id`, `mid`, `salutation`, `name`, `alias`, `c_name`, `company`, `con_name`, `c_summary`, `address1`, `cyid`, `phone`, `fax`, `mobile`, `web`, `credit`, `type_credit`, `desc_credit`, `gcviec`, `goicv`, `idgoi_cv`, `goi_cv`, `xem_cv`, `startdate`, `enddate`, `show_name`, `show_phone`, `show_fax`, `show_email`, `show_addr`, `show_packet`, `end_datebuy`, `image`, `logo_image`, `logo_image_large`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(45, 133, 'Mr.', '	Ho Thi Hai Anh ', '-ho-thi-hai-anh-', '16', 'Chi Nhánh Công ty Cổ phần Tara', '	Ho Thi Hai Anh ', '<p><span style="float: left; width: 544px; background-color: #ffffff;">CÔNG  TY CỔ PHẦN TARA, một trong những công ty hàng đầu về lĩnh vực kinh  doanh, phân phối các mặt hàng điện tử, điện lạnh, điện gia dụng của các  nhãn hiệu nổi tiếng như: Panasonic, Sharp, Pioneer, Philips, Bluestone…  với hệ thống phân phối phủ rộng toàn quốc. </span></p>', 'Tầng 3 – Tòa nhà TT Phục vụ Bách Khoa – Số 10 Tạ Q', 1, '	0436230550', '', '	0436230550', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(44, 132, 'Mr.', 'Anh Bình hoặc Hùng', 'anh-binh-hoc-hung', '15', 'Công ty cổ phần công nghệ và đồ uống Việt Nam VINATB', 'Anh Bình hoặc Hùng', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty cổ phần công nghệ và đồ uống Việt Nam VINATB<br /></span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">chuyên Sản  xuất, Chế biến, Kinh doanh, Tư vấn chuyển giao công nghệ, Môi giới,  buôn bán thiết bị-day chuyền công nghệ, Cung cấp các nguyên liệu đầu vào  cho ngành công nghiệp đồ uống và thực phẩm.</span></p>', 'P302 - 105 Hoàng Văn Thái - Thanh Xuân - HN', 1, '04', '', '04', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(12, 62, 'Mr.', 'Đỗ Bích Thắm', '-bich-thm', '16', 'Công ty TNHH Ngọc (OPAL)', 'Đỗ Bích Thắm', '<p>tyu7i</p>', '132 Lò Đúc - Hai Bà Trưng - Hà Nội', 1, '04 3 972 3533', '04 3 972 3533', '04 3 972 3533', 'opal.com.vn', 0, 'p', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '_logo.png', '', '', 1, 1, 0, '0000-00-00 00:00:00'),
(13, 96, 'Mr.', 'ESECO', 'eseco', '17', 'Công ty TNHH Thiết bị và DVKT (ESECO)', 'Ngọc Dung', '', '915 Đường Hồng Hà- P. Chương Dương - Q. Hoàn Kiếm ', 1, '043897569', '', '0987634566', '', 0, 'p', '', 'gcviec', 'goicv', 0, 40, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 1, 0, '0000-00-00 00:00:00'),
(14, 97, 'Mr.', 'Thuc pham TH', 'thuc-pham-th', '16', 'Cty CP Chuỗi Thực Phẩm TH', 'Chị Hoa', '', 'Số1 Đinh Lễ, Hoàn Kiếm, Hà Nội ', 1, '04 38978773', '04 38978773', '04 38978773', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(15, 98, 'Mr.', 'dulich', 'dulich', '14', 'Cty CP Khách sạn Du lịch Tháng Mười', 'chị Nhung', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công Ty Thương mại và Du lịch Quốc Tế Hải Sơn</span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Ra  đời trong sự chuyển mình hội nhập cùng thế giới của đất nước, Giấy phép  kinh doanh lữ hành số: 0102028513, với 100% đội ngũ cán bộ nhân viên  tốt nghiệp đại học chuyên ngành Du lịch, Kinh tế và Quản trị du lịch  nhiệt tình năng động, có tinh thần trách nhiệm cao với công việc,  có  kinh nghiệm hoạt động trong ngành Du lịch, có tác phong du lịch hiện  đại, trong nhiều năm qua Haisontravel luôn là địa chỉ tin cậy của Du  khách trong và ngoài nước mỗi khi có nhu cầu: Đi du lịch; Đặt vé máy  bay; Đặt vé tàu; Đặt phòng khách sạn; Làm visa - hộ chiếu hoặc Tư vấn du  lịch.</span></p>', '151 Thùy Vân, TP Vũng Tàu ', 1, '04 38978773', '04 38978773', '04 38978773', '', 0, 'p', '', '', 'goicv', 18, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 1, 0, '0000-00-00 00:00:00'),
(43, 131, 'Mrs.', 'Bích Thuỷ', 'bich-thu', '16', 'Công ty Thời trang Eva de eva', 'Bích Thuỷ', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty Thời trang Eva de eva</span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Năm  2007 thời trang Eva de eva có mặt tại Hà Nội và ghi nhận dấu ấn bởi sự  đột phá, mới lạ trong từng mẫu thiết kế. Với sự xuất hiện liên tục của  các bộ sưu tập đẳng cấp cùng mẫu thiết kế đầy đam mê cảm xúc, Eva de eva  đã chinh phục được rất nhiều khách hàng kể cả những khách hàng khó tính  nhất. Eva de eva đã được người tiêu dùng biết đến trong suốt thời gian  qua, đã và đang khẳng định được vị thế của mình trên thị trường Việt  Nam. Do nhu cầu mở rộng sản xuất, kinh doanh trong những năm tiếp theo,  chúng tôi mong muốn tìm kiếm ứng viên phù hợp.</span></p>', '173 Tôn Đức Thắng, Đống Đa, Hà Nội', 4, '0983532956', '', '0983532956', 'www.evafashion.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(21, 106, 'Mr.', 'Teleperformance', 'teleperformance', '16', 'Công Ty Teleperformance Vietnam', 'Chị Hiền', '', 'số 142 Đội Cấn, Hà Nội', 1, '+84 4 35730022', '+84 4 35730022', '+84 4 35730022', 'www.teleperformance.com ', 0, 'p', '', '', 'goicv', 23, 40, 2, '0000-00-00', '0000-00-00', '', '', '', '', '', 1, '2010-12-15', '', '', '', 1, 1, 0, '0000-00-00 00:00:00'),
(22, 107, 'Mr.', 'gialong', 'gialong', '16', 'Gia Long Computer', 'Nguyễn Hoài Nam', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Để  đáp ứng nhu cầu phát triển và mở rộng thêm 2 showroom bán lẻ cho năm  2011, Công ty Gia Long thực hiện chính sách thu hút nhân tài với môi  trường làm việc chuyên nghiệp, khác biệt và chế độ đãi ngộ hấp dẫn.  Chúng tôi đang cần tuyển ứng viên vào các vị trí sau:</span></p>', '168 Đường Láng', 1, '0904269050', '0904269050', '0904269050', 'www.gialong.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(23, 109, 'Mr.', 'nguyenminh', 'nguyenminh', '14', 'cong ty TNHH Nhan Luc Viet Bac', 'Nguyen Anh Minh', '<p>lmakfpần k[ hiàh0dkfd</p>', 'ha noi', 1, '01229292929', '0436462527', '01229292929', 'nhanlucvietbac.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '_logo.jpg', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(24, 111, 'Mr.', 'joliesiam', 'joliesiam', '16', 'CÔNG TY THHH XI AM', 'Ms Hiền', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Lấy  nụ cười và tinh thần phục vụ làm sản phẩm kinh doanh, Jolie Siam là một  trong những công ty đầu tiên chuyên cung cấp dịch vụ lễ tân đặt cho  mình sứ mạng “tiếp thị nụ cười và lòng hiếu khách vốn có của con người  Việt Nam”. </span></p>', '9 A Hồ Biểu Chánh Q. Phú Nhuận', 1, '08 62926858 hoặc 09 ', '', '08 62926858', 'http://www.joliesiam.com/', 0, 'p', '', '', 'goicv', 19, 40, 1, '0000-00-00', '0000-00-00', '', '', '', '', '', 1, '2010-11-25', '', '', '', 1, 1, 0, '0000-00-00 00:00:00'),
(25, 112, 'Mr.', 'Nguyễn Tiến Minh', 'nguyn-tin-minh', '16', 'Công Ty TNHH An Khánh', 'Ms Hieu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  ty TNHH AN KHÁNH ( ANKHANH CO., LTD) được thành lập vào năm 2006, tự  hào là nhà phân phối chính sản phẩm đồng hồ CASIO- NHẬT BẢN của công ty  CASIO COMPUTER CO., LTD Tokyo, Japan tại thị trường Việt Nam. <br /> <br /> Nhãn hiệu CASIO có mặt trên thị trường thế giới đã hơn 50 năm  qua với nhiều sản phẩm điện tử nổi tiếng và điều đó đã là một sự bảo  chứng về chất lượng sản phẩm mà chúng tôi đang cung cấp. <br /> <br /> Với vai trò nhà phân phối chinh một sản phẩm danh tiếng tại  thị trường Việt Nam, chúng tôi luôn cố gắng đưa sản phầm đến với người  tiêu dùng với giá cả cạnh tranh và hợp lý nhất, ngoài những chương trình  khuyến mãi hấp dẫn mang đến nhiều quyền lợi cho khách hàng. chúng tôi  con chú trọng là chế độ hậu mãi chu đáo, để hỗ trợ quý khách trong quá  trình sử dụng và giữ gìn sản phẩm. <br /> <br /> Với hơn 100 đại lý trải dài khắp các tỉnh thành cả nước, Công  ty An Khánh chúng tôi mong muốn được phục vụ rộng rãi cho tất cả khách  hàng tại Việt Nam. Chúng tôi luôn đảm bảo tất cả những sản phẩm chúng  tôi kinh doanh là sản phẩm chính hãng và được bảo hành chính hãng <br /> <br /> Bên cạnh hoạt động bán lẻ, chúng tôi còn bán số lượng lớn cho  các công ty để làm chương trình khuyến mãi hoặc làm quà tặng với chính  sách chiết khấu đặc biệt. Chúng tôi sẵn sàng hỗ trợ in logo công ty quý  khách lên mặt đồng hồ và đây là một phương thức quảng cáo hữu hiệu nhất  cho thương hiệu của quý khách trên đồng hồ Casio. <br /> <br /> Với những tiêu chí trên, Cty chúng tôi tin rằng sản phẩm mà  chúng tôi cung cấp sẽ luôn mang đến vẻ đẹp, sự tiện ích và làm hài lòng  quý khách hàng thân thiết trên thị trường Việt Nam. <br /> </span></p>', 'TPHCM', 1, '08 927 0317', '08 0271657 ', '08 927 0317', '', 0, 'p', '', '', 'goicv', 17, 40, 3, '0000-00-00', '0000-00-00', '', '', '', '', '', 1, '2010-11-30', '', '', '', 1, 1, 0, '0000-00-00 00:00:00'),
(99, 193, 'Mr.', 'thaison', 'thaison', '14', 'thaison', 'thaison', '<p>thaison thaison thaison thaison</p>', 'trung hoaf', 1, '0982766333', '', 'thaison', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(27, 114, 'Mr.', 'Nguyễn Lê Sơn', 'nguyn-le-sn', '16', 'Công ty AVG', 'Nguyễn Lê Sơn', '<p><span style="background-color: #ffffff; width: 544px; float: left;">Công ty AVG</span></p>\r\n<p><span style="background-color: #ffffff; width: 544px; float: left;">Kinh doanh đa lĩnh vực, đa ngành nghề: Bất động sản, Khai thác khoáng sản, Kinh doanh truyền hình</span></p>\r\n<p><span style="background-color: #ffffff; width: 544px; float: left;">Đ/c:</span><span style="background-color: #ffffff; width: 544px; float: left;">15AV Hồ Xuân Hương, Hai Bà Trưng, Hà Nội</span></p>', 'ha noi', 1, '0912.498456', '', '0912.498456', '', 0, 'p', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 1, 0, '0000-00-00 00:00:00'),
(28, 115, 'Mr.', 'Nguyễn Quốc Dũng', 'nguyn-quc-dng', '16', 'ANZ BANK', 'Nguyễn Quốc Dũng', '<p><span style="float: left; width: 544px; background-color: #ffffff;">ANZ  là ngân hàng Úc hàng đầu tại Châu Á. Tại Việt Nam, ANZ đã hoạt động hơn  15 năm. Chìa khóa thành công của chúng tôi chính là cam kết đào tạo  nhân viên bản địa về dịch vụ khách hàng thân thiện và chuyên nghiệp, và  ANZ nổi tiếng trong khu vực về sự hài lòng của khách hàng. </span></p>', '14 Lê Thái Tổ - Quận Hoàn Kiếm - Hà Nội', 1, '094 303 8558', '', '094 303 8558', 'www.anz.com', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(34, 121, 'Mr.', 'cao văn trung', 'cao-vn-trung', '16', 'CÔNG TY TNHH MTV BẢO VỆ VÀ DỊCH VỤ THĂNG LONG', 'cao văn trung', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty dịch vụ Bảo vệ THĂNG LONG muốn gửi đến quý khách hàng những lời chúc tốt đẹp của sự thịnh vượng, sức khỏe và hạnh phúc. <br /> <br />Kính thưa quý khách hàng, với hy vọng cung cấp cho bạn một dịch vụ  bảo vệ chuyên nghiệp, sự an toàn cao nhất trong quản lý tài sản, bảo vệ  con người, trật tự trong quá trình sản xuất, kinh doanh. <br /> <br /> Trong quá trình sử dụng dịch vụ của Thăng Long Dịch vụ an ninh.  Chúng tôi cam kết đem lại cho khách hàng một dịch vụ với nhiều lợi ích  bao gồm: <br />- Luôn luôn lắng nghe khách hàng và đáp ứng các nhu cầu tối đa của khách hàng. <br />- Duy trì tính bảo mật của công ty 24/24 của bạn. <br />- Đảm bảo tuyệt đối thông tin nội bộ cho bạn. <br />- Thăng Long sẽ được phục vụ an ninh luôn luôn là một đối tác tin  cậy trong việc bảo đảm an ninh và để cho khách hàng của chúng tôi trong  quá trình sản xuất, kinh doanh. <br /> <br /> Các loại hình dịch vụ chúng tôi đang cung cấp: <br /> <br /> - Bảo vệ  tòa nhà, văn phòng, ngân hàng, khách sạn, siêu thị,  cầu cảng, kho hàng, công ty xây dựng, chương trình ca nhạc, cửa hàng ...  .... <br /> - Bảo vệ yếu nhân (VIP), bảo vệ nhà riêng, biệt thự, khi bạn đi du lịch. <br /> -Công ty trách nhiệm hữu hạn một thành viên dịch vụ bảo vệ tại  Thăng Long đã được cấp giấy chứng nhận của Công an Hà Nội đủ điều kiện  về an ninh để kinh doanh dịch vụ bảo vệ chuyên nghiệp, giấy phép đăng ký  kinh doanh do Sở Kế hoạch và Đầu tư Hà Nội. <br /> <br /> Các nhân viên CHUYÊN NGHIỆP, TỰ TIN, CHÍNH XÁC, AN NINH, HIỆU  QUẢ, TIẾT KIỆM, TRUNG THÀNH - KỶ LUẬT của đội ngũ nhân viên THĂNG LONG  (đã được chọn để xác minh lịch sử tốt hơn) là phương châm chính trong  hoạt động của công ty. <br /> Với đội ngũ lãnh đạo và chỉ huy có kinh nghiệm lâu năm trong  lĩnh vực bảo vệ chuyên nghiệp sẽ luôn luôn làm cho các cam kết cho  khách hàng. </span></p>', '540/9 CMT8, phường 11, Quận 3, TPHCM', 1, '0979109179', '', '0979109179', 'www.baovethanglong.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(33, 120, 'Mr.', 'Trần Thành Trung', 'trn-thanh-trung', '15', 'Công ty CP và TM Vietlightstar', 'Trần Thành Trung', '<p><span style="float: left; width: 544px; background-color: #ffffff;"> Công ty Cp và Tm Vietlightstar chuyên  nhập khẩu và phân phối các mặt hàng tiêu dùng nhanh từ tập đoàn TMDV  đến từ Thụy Điển, sản phẩm của chúng tôi đã được biết đến rộng khắp và  được khách hàng sử dụng đánh giá cao về chất lượng sản phẩm. Là đại diện  số 1 của Thụy Điển tại Việt Nam. Hiện nay công ty đang có chi nhánh ở  HN,Hải Phòng, HCM, Đà Nẵng, Cần Thơ và muốn tiếp tục mở rộng thị phần ra  các tỉnh miền Bắc, do vậy chúng tôi muốn tuyển thêm 1 số lượng sinh  viên làm thêm bán hàng và đào tạo quản lý tại khu vực nội thành Hà Nội.</span></p>', '21 Pháo Đài Láng', 1, '0973 629 028', '', '0973 629 028', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(32, 119, 'Mrs.', 'Mis Bông', 'mis-bong', '16', 'CÔNG TY TNHH MTV – TM – DV – XNK HẢI SƠN', 'Mis Bông', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  Ty Hải Sơn là nhà phân phối độc quyền thẻ gọi quốc tế bằng máy tính ,  điện thoại di động với thương hiệu Evoizvn ( www.evoizvn.com.vn ) sản  phẩm được nhiều khách hàng ưa chuộng tại TP Hồ Chí Minh <br /> <br />- Với  Evoizvn quý khách có thể liên lạc với bất kỳ người thân, bạn  bè, đối tác trên thế giới một cách thật nhanh chóng và thuận tiện. Hãy  liên hệ với Đại lý của Hải Sơn để có thể mua thẻ Evoizvn một cách dễ  dàng. </span></p>', '025 Lô H chung cư Nguyễn Thiện Thuật, P1, Q3', 1, '(08). 62.650.574', '', '(08). 62.650.574', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(31, 118, 'Mr.', 'Đặng Tuấn Hưng', 'ng-tun-hng', '15', 'Long Vân Tisco', 'Đặng Tuấn Hưng', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Long  Van Tisco –chuyên cung cấp các trang thiết bị, đồ dùng, dụng cụ trong  lĩnh vực công nghiệp xây dựng, văn phòng, khách sạn, nhà hàng, quán bar,  café…</span></p>', 'Tầng 3 nhà số 61 phố Yên phụ Tây hồ Hà Nội', 1, '0913303270', '', '0913303270', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(35, 122, 'Mr.', 'Thanh Nga', 'thanh-nga', '14', 'Công ty TNHH Minh Phúc (MP Telecom)', 'Thanh Nga', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  ty TNHH Minh Phúc (MP Telecom) thành lập ngày 29 tháng 07 năm 2002.  Hiện tại chúng tôi có hơn 1.000 nhân viên, hoạt động tại ba miền, trụ sở  chính đặt tại Hà Nội và hai chi nhánh đặt tại thành phố Hồ Chí Minh và  thành phố Đà Nẵng.  <br />Lĩnh vực hoạt động chính của chúng tôi là Contact Center (dịch vụ  Chăm sóc khách hàng) từ xa, hàng đầu tại Việt Nam, bao gồm: cung cấp  công nghệ Chăm sóc khách hàng thuê ngoài, cung cấp dịch vụ Chăm sóc  khách hàng trọn gói, cung cấp nguồn nhân lực.  <br />Cùng công nghệ hiện đại, kinh nghiệm và giá trị gia tăng mà chúng  tôi dành cho khách hàng, hoạt động kinh doanh của chúng tôi mở rộng  không ngừng và tăng trưởng hàng năm. Đối tác của chúng tôi là những tập  đoàn lớn mạnh như Interactive Intelligence và khách hàng hiện nay của  chúng tôi là Công ty Thông tin Di động (VMS-MobiFone), Tổng Công ty Viễn  thông quân đội (Viettel), Công ty Viễn thông Điện lực (EVN), Công ty  Dịch vụ Viễn Thông Vinaphone</span></p>', '	Số 18 Đường Giải phóng- Đống Đa- Hà Nội', 1, '04.5771608', '', '04.5771609', 'mptelecom.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(36, 123, 'Mr.', 'Phạm Hồng Thu', 'phm-hng-thu', '16', 'CTy CP phát triển công nghệ và Quảng cáo Quang Vinh', 'Phạm Hồng Thu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Cty  CP Phát triển Công Nghệ và Quảng cáo quang Vinh là nhà quảng cáo chuyên  nghiệp trong lĩnh vực thiết kế,thi công và dàn dựng các công trình  quảng cáo trong nhà và ngoài trời; Thiết kế mỹ thuật, in ấn và hoàn  thiện các ấn phẩm quảng cáo; In phun quảng cáo tấm lớn; Xây dựng ý tưởng  quảng cáo; Nhận diện thương hiệu; Thiết kế, thi công nội ngoại thất:  Showroom, triển lãm, gian hàng, hội chợ, kinh doanh các loại máy in  phun, thiết bị và vật tư ngành in quảng cáo.</span></p>', 'Số 462A Đường Bưởi - HN', 1, '0983 025 792', '', '3.7618247', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(37, 124, 'Mr.', 'Đỗ Thị Trung', '-th-trung', '15', 'CÔNG TY CỔ PHẦN QUỐC TẾ IDC', 'Đỗ Thị Trung', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  ty CP Quốc tế IDC là một Công ty đa ngành nghề, được thành lập từ năm  2004. Trong nhiều năm phát triển của mình Công ty IDC luôn khẳng định  được vị trí và thương hiệu của mình trước đối tác trong và ngoài nước.  Đến với IDC là đến với một môi trường làm việc thân thiện, chuyên nghiệp  mang tầm quốc tế. Vơi quy mô ngày càng phát triển, Công ty IDC đang cần  tuyển gấp nhiều vị trí, ứng cử viên tài năng với những chính sách đặc  biệt chỉ có được ở IDC</span></p>', 'P601B, tòa nhà LICOGI 13, Khuất Duy Tiến, Nhân Chí', 1, '04 3 6404099', '', '04 3 6404099', 'www.idctravel.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(38, 125, 'Mr.', 'Nguyễn Giang', 'nguyn-giang', '17', 'FTC Group', 'Nguyễn Giang', '<p><span style="float: left; width: 544px; background-color: #ffffff;">FTC Group hoạt động đa ngành nghề bao gồm các Công ty: <br /> <br />1. Công ty Cổ phần Đầu tư Công nghệ Truyền Thông Đa phương tiện  FTC Việt Nam (VINAFTC ) là doanh nghiệp hoạt động trong lĩnh vực đầu  tư, công nghệ, truyền thông tại Việt Nam  <br /> <br />2. Công ty GFOODS Việt Nam hoạt động trong lĩnh vực kinh doanh đặc sản thực phẩm. <br /> <br />3. Công ty Cổ phần Thương mại FTC Việt Nam hoạt động trong lĩnh vực  Thương mại diện tử, kinh doanh hàng tiêu dùng, lương thực, thực phẩm. <br /> <br />Với đội ngũ nhân viên trẻ trung, tài năng và đầy nhiệt huyết , FTC Group đã và đang gặt hái được nhiều thành Công. <br /> <br />Hiện nay FTC Group đang cần bổ sung nhân sự cho các Công ty thành  viên. Nếu bạn quan tâm, vui lòng gửi hồ sơ về địa chỉ Phòng Hành chính  Nhân sự FTC Group, địa chỉ 9/2, ngõ 850 đường Láng, Đống Đa, Hà Nội </span></p>', 'Số 9/2 ngõ 850 Đường Láng, Đống Đa', 1, '0462928558', '', '0462928558', 'www.vietbrandsport.com/tuyendung', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(39, 127, 'Mr.', 'Võ Thị Thanh Đàn', 'vo-th-thanh-an', '14', 'Công ty TNHH Quảng cáo và Nội thất Hùng Sơn', 'Võ Thị Thanh Đàn', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty TNHH Quảng cáo và Nội thất Hùng Sơn là một trong những công ty uy tín hàng đầu tại Hà Nội, hoạt động trong lĩnh vực: <br />- Sản xuất, thiết kế và kinh doanh đồ nội thất như nội thất khách sạn, nội thất gia đình, nội thất văn phòng... <br />- Hoạt động trong lĩnh vực quảng cáo : Thiết kế, thi công các biển  hiệu, pano quảng cáo, quảng cáo trên các phương tiện giao thông, bảng  điện tử, đèn neon sight... <br />- Kinh doanh các sản phẩm chăn - ga - gối đệm thương hiệu nổi tiếng KYMDAN, HAVICO, EVERON... <br />- Trụ sở chính tại số 1/401 Nguyễn Khang - Cầu Giấy - Hà Nội <br />- Văn phòng giao dịch 1 : 413 Nguyễn Khang - Cầu Giấy - Hà Nội <br />- Văn phòng giao dịch 2 : 155 Cầu Giấy - Hà Nội <br />- Xưởng sản xuất Nội thất: Khu Công nghiệp Hòai Đức- Hà Nội <br />- Xưởng sản xuất Quảng cáo: K3- Phú Diễn- Từ Liêm- Hà Nội</span></p>', '413 Nguyễn Khang - Cầu Giấy - Hà Nội', 4, '	0973287751', '', '	0973287751', 'hungson.net', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(40, 128, 'Mrs.', 'Đỗ Thị Bằng', '-th-bng', '15', 'Cty cổ phần ĐT&PT Thương mại Đại Hưng', 'Đỗ Thị Bằng', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty CP ĐT&amp;PT Thương mại Đại Hưng chuyên kinh doanh mặt hàng các loại xe găn máy thương hiệu Hon Đa và đồ nội thất nhập ngoại</span></p>', '	Số 25 chợ bún - Đa Tốn - Gia Lâm - Hà Nội', 1, '0983.347.204', '', '0983.347.204', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(41, 129, 'Mr.', 'Hùng', 'hung', '16', 'Công ty Cổ Phần Nước Khoáng Vĩnh Hảo', 'Mr Hùng ', '<p><!--[if gte mso 9]><xml> <w:WordDocument> <w:View>Normal</w:View> <w:Zoom>0</w:Zoom> <w:PunctuationKerning /> <w:ValidateAgainstSchemas /> <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid> <w:IgnoreMixedContent>false</w:IgnoreMixedContent> <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText> <w:Compatibility> <w:BreakWrappedTables /> <w:SnapToGridInCell /> <w:WrapTextWithPunct /> <w:UseAsianBreakRules /> <w:DontGrowAutofit /> <w:UseFELayout /> </w:Compatibility> <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel> </w:WordDocument> </xml><![endif]--><!--[if gte mso 9]><xml> <w:LatentStyles DefLockedState="false" LatentStyleCount="156"> </w:LatentStyles> </xml><![endif]--><!--[if gte mso 10]>\r\n<style>\r\n /* Style Definitions */\r\n table.MsoNormalTable\r\n	{mso-style-name:"Table Normal";\r\n	mso-tstyle-rowband-size:0;\r\n	mso-tstyle-colband-size:0;\r\n	mso-style-noshow:yes;\r\n	mso-style-parent:"";\r\n	mso-padding-alt:0in 5.4pt 0in 5.4pt;\r\n	mso-para-margin:0in;\r\n	mso-para-margin-bottom:.0001pt;\r\n	mso-pagination:widow-orphan;\r\n	font-size:10.0pt;\r\n	font-family:"Times New Roman";\r\n	mso-fareast-font-family:"Times New Roman";\r\n	mso-ansi-language:#0400;\r\n	mso-fareast-language:#0400;\r\n	mso-bidi-language:#0400;}\r\n</style>\r\n<![endif]--></p>\r\n<p class="MsoNormal" style="text-indent: 0.5in;">Công ty Cổ Phần Nước Khoáng Vĩnh Hảo thành lập năm 1928. Qua suốt quá trình hình thành và phát triển đến nay, với quy mô mở rộng ra khắp các tỉnh thành trên mọi miền đất nước và hệ thống dây chuyền SX hiện đại có công xuất 30 triệu lít/ năm. Công ty Vĩnh Hảo luôn tự hào là một trong những nhà sản xuất và cung cấp hàng đầu Việt Nam các sản phẩm nước khoáng, nước tinh khiết đóng chai, bùn khoáng và các dịch vụ tắm bùn – khoáng chăm sóc sức khỏe…</p>', 'Thành phố Hồ Chí Minh ', 1, '38233462', '', '38233462', 'www.vinhao.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '_logo.jpg', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(42, 130, 'Mr.', 'Nguyễn Thị Việt Anh', 'nguyn-th-vit-anh', '16', 'CÔNG TY CP CÔNG NGHỆ THT-WINDOWS', 'Nguyễn Thị Việt Anh', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  ty CP công nghệ THT - Windows là công ty chuyên sản xuất và cung cấp  các sản phẩm cửa từ nhựa uPVC, nhôm Profile, trên dây chuyền máy móc  hiện đại, tân tiến theo tiêu chuẩn quốc tế. Đặc biệt với đội ngũ cán bộ  nhân viên giàu kinh nghiệm, tay nghề cao, năng động và sáng tạo mang đến  cho Quý khách những sản phẩm đa dạng và chất lượng. <br />Để mở rộng sản xuất và đạo tạo nhân lực Công ty chúng tôi cần tuyển các vị trí sau:</span></p>', '	www.tht-windows.com', 1, '01657989989', '', '01657989989', 'www.tht-windows.com', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(46, 134, 'Mrs.', 'Mai Thị Yến', 'mai-th-yn', '16', 'Công ty cổ phần đầu tư xây dựng và phát triển thủy điện Tuấn Anh', '	Mai Thị Yến', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  ty CP ĐTXD và phát triển thủy điện Tuấn Anh là chủ đầu tư xây dựng nhà  máy thủy điện Bản Nhùng - tỉnh Lạng Sơn. Công ty chúng tôi đang cần  tuyển bổ sung nhân sư cho các vị trí sau.</span></p>', 'Số 3A ngõ 430 Bạch Đằng - Chương Dương - Hoàn Kiếm', 1, '04.39844777', '', '04.39844777', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(47, 135, 'Mr.', 'Nguyễn Thành Lập', 'nguyen-thanh-lap', '16', 'Công ty Cổ Phần ICEL Tư Vấn Và Đào Tạo', 'Nguyễn Thành Lập', '<p><span style="float: left; width: 544px; background-color: #ffffff;">CEL  – nhà cung cấp tổ hợp dịch vụ nhằm nâng cao năng lực quản lý doanh  nghiệp. Tổ hợp dịch vụ nói đến ở đây mang tính liên hoàn bao gồm: tư vấn  quản lý kinh doanh, đào tạo kế toán và quản trị, các giải pháp phần mềm  và công cụ phục vụ công tác quản trị tài chính doanh nghiệp. <br />Toàn bộ hệ thống công cụ đó được cung cấp tới doanh nghiệp đều mang  tính hỗ trợ và xây dựng cộng đồng cùng quyền lợi tại http://smartkey.vn  và http://icel.vn <br />Dịch vụ do ICEL cung cấp <br />* Tư vấn quản lý kinh doanh, đầu tư <br /> <br />* Đào tạo kế toán và quản trị <br /> <br />* Giải pháp phần mềm doanh nghiệp <br /> <br />Chính sách nhân sự của ICEL: "Luôn tôn trọng và phát huy năng lực cá nhân cho mỗi thành viên" </span></p>', 'Số 5B Ngõ 551 Phố Kim Mã, P. Ngọc Khánh, Q. ', 1, '0437618355', '', '0437618355', 'www.smartkey.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(48, 136, 'Mr.', 'Cương', 'cng', '16', 'Công ty CP Đầu tư Thương mại - HK', 'Cương', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Thành  lập hơn mười năm, Công ty Cổ phần Đầu tư và Thương mại - HK là đơn vị  hàng đầu trong cung cấp thiết bị điện cho đường dây và thiết bị trạm đến  cấp điện áp 500kV. </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Hiện  nay, công ty cung cấp vật tư thiết bị điện trung - cao thế cho các dự  án và xây lắp công trình đường dây-trạm, hệ thống phân phối, truyền tải  điện cho các ngành điện công nghiệp, năng lượng. </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Sản  phẩm được Công ty cung cấp với chất lượng cao của các công ty va hang  co uy tín trên thế giới mà công ty đã lựa chọn để cung cấp và bảo hành  như: Sứ cách điện thuỷ tinh, sứ cách điện polymer;  Cáp quang OPGW (12  hoặc 24 sợi), phụ kiện đường dây và thiết bị đấu nối; Tủ trung thế, máy  cắt Recloser; Cầu chì tự rơi; Aptomat; Chống sét van; Hộp đầu cáp - hộp  nối cáp; v.v… </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Đặc  biệt hơn Công ty còn có đội ngũ cán bộ chuyên gia, công nhân lành nghề,  sản phẩm do công ty nghiên cứu và sản xuất cho lưới điện hạ thế như:  ghíp nhựa dùng cho cáp vặn xoắn, kẹp xiết, kẹp treo, kẹp cực …cầu chì  rơi .. đạt chất lượng cao tương đương hàng các nước phát triển, giá  thành hợp lý, được các cơ quan có thẩm quyền cấp chứng chỉ, chứng nhận  tiêu chuẩn chất lượng. </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Không  chỉ cung cấp vật tư thiết bị Công ty còn có máy móc và dụng cụ thi công  chuyên dụng để tham gia thi công xây lắp công trình đường dây và trạm  biến áp tới 110kV. Với cam kết luôn đảm bảo an toàn, chất lượng và đúng  tiến độ. </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Ngoài  ra, chúng tôi còn là đại lý cung cấp một số mặt hàng có chất lượng cao  trong nước sản xuất như: Chống sét van, sứ cách điện; phụ kiện đường  dây; các thiết bị đóng cắt .... </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"><br /></span></p>\r\n<p> </p>', 'Số 101B Âu Cơ - Tây Hồ - Hà Nội', 1, '0987998871', '', '0987998871', '	www.hkec.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(49, 137, 'Mr.', 'Dũng', 'dng', '16', 'Công ty CPTM Madona', 'Mr Dũng', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  ty tổ chức sự kiện chuyên nghiệp hàng đầu Việt Nam Công ty Tổ chức sự  kiện Madona là công ty tổ chức sự kiện chuyên nghiệp hàng đầu Việt Nam </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Tiêu chí hoạt động: <br />- Với đội ngũ chuyên viên sự kiện chuyên nghiệp, kinh nghiệm.  <br />- Công ty có đầy đủ các trang thiết bị để phục vụ sự kiện như: </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">+  Giàn không gian sân khâu, nhà bạt kiểu dáng Singapo, nhà không gian,  bàn ghế có áo, váy quây đẹp và trang trọng, cổng hơi, ô dù to nhỏ, âm  thanh ánh sáng vv....  <br />+ Cho thuê máy chiếu, các trang thiết bị phục vụ hội thảo, hội nghị.  <br />+ Đáp ứng đầy đủ trang thiết bị phục vụ các lễ KHAI TRƯƠNG, KHÁNH THÀNH, ĐỘNG THỔ, KỶ NIỆM, HỘI CHỢ ,vv.... </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Chúng  tôi đã từng tổ chức thành công rất nhiều chương trình khánh thành, khai  trương, động thổ, hội nghị , hội thảo, mừng công, kỷ niệm vv.... cho  các doanh nghiệp, tổ chức, cơ quan và cá nhân tại Hải Dương, Quảng Ninh  cũng như các tỉnh lân cận. Nếu Khách hàng quan tâm và đang lo lắng chuẩn  bị cho sự kiện của cơ quan, doanh nghiệp mình thì hãy liên hệ với chúng  tôi, chúng tôi sẵn lòng phục vụ quý khách. </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"><br />Trân trọng! </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"><br /></span></p>', '27/84 Trần Quang Diệu, Đống Đa, Hà Nội', 1, '0975272627', '', '043537 8775 ', 'www.madonamedia.com', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(50, 139, 'Mr.', 'Phòng nhân sự', 'phong-nhan-s', '16', 'Công ty cổ phần Liên Hiệp Nhất Nguyên', 'Phòng nhân sự', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty chúng tôi là đại lý phát triển thuê bao mạng di động Vinaphone. Do nhu cầu mở rộng thị trường, chúng tôi cần tuyển gấp nhiều nhân viên và cộng tác viên kinh doanh trong tháng 12/2010.</span></p>', '14/34 Đào Duy Anh - Phường 9 - Q. Phú Nhuận', 1, '0944444905', '', '0944444816', 'www.nhatnguyencorp.com', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(51, 140, 'Mr.', 'Phòng lễ tân', 'phong-l-tan', '16', 'Tập đoàn Vĩnh Hưng-Vinh Hung Group', 'Tầng 1-Phòng lễ tân', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Tập đoàn Vĩnh Hưng: <br />-Tư vấn <br />-Đầu tư <br />-Công nghệ <br />-Xây dựng <br />-Bất động sản <br />-Thẩm định giá tài sản <br />-...</span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Nhân viên Vinhhung Group dù ở đâu, làm việc gì cũng hướng về Công ty với tình cảm cao đẹp. Nói đến Vinhhung Group trước tiên phải nói về tính văn hóa, con người và những giá trị tinh thần bao năm xây đắp. Văn hóa đó, những con người đó đóng vai trò quyết định trong sự nghiệp của Vinhhung Group hôm nay và cả mai sau. </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Tinh thần, giá trị nhân học là nền móng cho văn hóa Vinhhung Group phát triển. Biểu hiện cao nhất của con người là sự lao động quên mình, sáng tạo tột bực tạo nên thành công cho Vinhhung Group trên các mặt trận</span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"><br /></span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"><br /></span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"><br /></span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"><br /></span></p>', 'Số 69 - Tổ 50 - TT Tổng Cục Chính Trị - Phố Trung ', 1, '0987.711.227', '', '0987.711.227', 'http://adsoft.com.vn, http://gialoc.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(52, 141, 'Mr.', 'Anh Bình', 'anh-binh', '16', 'TNHH TM- DV- XD ĐỊA ỐC DUY ĐIỀN', 'Anh Bình', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Chuyên khảo sát, đo đạc công trình. <br />cần tuyển công nhân phụ việc, và nhân viên chắc địa.</span></p>', ' 	42/70 Hồ Đắc Di - Phường Tây Thạnh - Quận Tân Ph', 1, '0908116213', '', '0908116213', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(53, 142, 'Mr.', 'Phòng quản lý nhân sự', 'phong-qun-ly-nhan-s', '15', 'VIỆN MÁY TÍNH', 'Phòng QLNS', '<p><span style="float: left; width: 544px; background-color: #ffffff;">+ Chuyên phục hồi dữ liệu mọi trường hợp: hư đầu đọc, kêu lạch cạch, bị cháy, bị va đập, bị ẩm ướt, cháy chipset hoặc cháy board, chết motor, format, fdisk, ghi đè... </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">+ Phục hồi files và folders bị mất trong hầu hết các thiết bị lưu trữ như: Hard disk, Server Raid, HDD SAS, Card SD, Máy ảnh, iPhone, CD, DVD, Camera, USB,... </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">+ Chuyên sửa chữa Mainboard - Pin laptop, LCD bằng kính hiển vi điện tử 100.000 lần </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">+ Máy hàn chipset công suất lớn, hiện đại nhất hiện nay, nhập trực tiếp từ Singapore </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">+ Công nghệ hàn chipset BGA và cab LCD hoàn toàn tự động bằng lập trình</span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"><br /></span></p>', ' 178 - 180 Hoàng Văn Thụ, Q. Phú Nhuận', 1, '0838442008', '', '0838442008', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(54, 143, 'Mr.', ' 	CÔNG TY CỔ PHẦN CÔNG NGHỆ SAO PHƯƠNG NAM', '-cong-ty-c-phn-cong-ngh-sao-phng-nam', '16', ' 	CÔNG TY CỔ PHẦN CÔNG NGHỆ SAO PHƯƠNG NAM', ' 	CÔNG TY CỔ PHẦN CÔNG NGHỆ SAO PHƯƠNG NAM', '<p><span style="float: left; width: 544px; background-color: #ffffff;">SOUTHERN STARS luôn cam kết mang đến các giải pháp, sản phẩm phần mềm và dịch vụ CNTT phù hợp nhất cho khách hàng. Vì thế Đầu tư, phát triển và trọng dụng nhân tài là một trong những mục tiêu hàng đầu của chúng tôi, bên cạnh đó là môi trường làm việc chuyên nghiệp, năng động, sáng tạo cùng với chế độ đãi ngộ hấp dẫn, có nhiều cơ hội thăng tiến là chính sách giúp Southern Stars cùng bạn phát triển.</span></p>', 'OO11 Bạch Mã - Phường 15 - Quận 10', 1, '08 6264 6980 (105)', '', '08 6264 6980 (105)', 'www.southernstars.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(55, 144, 'Mr.', 'hành chính nhân sự', 'hanh-chinh-nhan-s', '15', 'Công ty TNHH Triều Nhật ', 'Hành chính nhân sự', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty TNHH Triều Nhật là một đơn vị hoạt động chủ yếu trong lĩnh vực kinh doanh Nhà hàng cao cấp và các khu nghỉ dưỡng (resort). Hiện nay Công ty đang sở hữu các Thương hiệu nổi tiếng với các Nhà hàng cao cấp như Triều Nhật Asahi Sushi, Long Đình Dragon Palace, Triều Nhật Asahi Hot Pot và Thương hiệu Phở Vuông nổi tiếng đất Hà Nội. <br />Với sứ mệnh tầm nhìn:"Trở thành một tập đoàn hàng đầu Việt Nam chuyên cung cấp các dịch vụ nhà hàng, khách sạn, khu nghỉ dưỡng với tiêu chuẩn tốt nhất; <br />Đáp ứng nguyện vọng của khách hàng một cách sáng tạo, hiệu quả thông qua các sản phẩm, dịch vụ theo tiêu chuẩn cao nhất; Góp phần nâng cao chất lượng cuộc sống, cho sự tiến bộ và phát triển xã hội, cho tất cả mọi người một cuộc sống tốt đẹp hơn". Công ty chúng tôi luôn sẵn sàng mở cửa chào đón các ứng viên gia nhập vào đội ngũ nhân viên của Công ty: <br />Quy mô công ty: 300 nhân viên</span></p>', ' 	288 Bà Triệu Hà Nội', 1, '043.9745945, 0984696', '', '043.9745945, 0984696', 'www.trieunhat.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(56, 145, 'Mrs.', 'Yến', 'yn', '15', 'Công ty cổ phần bất động sản B.D.S', 'Ms Yến', '', 'Tầng 16 - Phòng 1604 - Toà nhà 101 Láng Hạ - Đống ', 1, '04.39410916', '', '04.39410916', 'batdongsan.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(57, 146, 'Mrs.', 'Thanh Hoa', 'thanh-hoa', '15', 'Công ty TNHH TM & DV An Phát ', 'Thanh Hoa', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty TNHH TM &amp; DV An Phát là một công ty hoạt động trong lĩnh vực thương mại, đặc biệt trong lĩnh vực tài chính. Công ty được thành lập và đi vào hoạt động đến nay đã có uy tín trên thị trường. Và để mở rộng hoạt động kinh doanh nhu cầu tuyển dụng của công ty là một trong những điều cần thiết nhất. <br />- Tạo công ăn việc làm cho người lao động, bên cạnh đó góp phần vào sự phát triển của công ty.</span></p>', '126B - Nguyễn Khánh Toàn - Cầu Giấy - Hà Nội', 1, '01666165566', '', '01666165566', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(58, 147, 'Mrs.', 'Nguyễn Thị Ngọc Hà', 'nguyn-th-ngc-ha', '15', ' 	Vision International', 'Nguyễn Thị Ngọc Hà', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Với sự phát triển không ngừng và lớn mạnh cả về nhân lực lẫn cơ sở vật chất và công nghệ máy móc, chúng tôi là một trong những công ty sản xuất đầu gậy đánh GOLD hàng đầu trên thế giới.</span></p>', ' 	19 Đại Lộ Hưu Nghị, KCN VSIP I Thuận An', 1, '0650 - 3782968 nội b', '', '0650 - 3782968 nội b', ' 	www.fusheng.com', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(59, 148, 'Mrs.', 'Vũ Huyền', 'v-huyn', '16', ' 	Báo lao động online', 'Ms Vũ Huyền', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Là cơ quan của Tổng liên đoàn lao động Việt Nam</span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"><br /></span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"><br /></span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"><br /></span></p>', 'B52, Nguyễn Thị Định, Trung hòa Nhân chính, Hà Nội', 1, ' (01689)186886', '', ' (01689)186886', ' http://laodong.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(60, 149, 'Mrs.', 'Nguyễn Thùy Duyên', 'nguyn-thuy-duyen', '14', 'Công ty TNHH Tuấn Duyên Việt Nam', 'Nguyễn Thùy Duyên', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty TNHH Tuấn Duyên Việt Nam thuộc tập đoàn tài chính Hoa Viên (Financial Holding company) Là tập đoàn bảo hiểm tài chính lớn nhất tại Đài Loan, xếp hạng thứ 389 trong danh sách 500 các doanh nghiệp lớn nhất trên thế giới. Phạm vi kinh doanh gồm bảo hiểm nhân thọ, ngân hàng, bảo hiểm phi nhân thọ, đầu tư tín dụng, cổ phiếu. Năm 2006 tổng tài sản lên đến 106 tỉ đô la Mỹ. Mục tiêu của chúng tôi là trở thành tập đoàn tài chính hàng đầu Châu Á . </span></p>', '26 Phùng Khắc Khoan - Hai Bà Trưng - Hà nội', 1, '0974.722.633', '', '0974.722.633', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(61, 150, 'Mrs.', 'Kỳ Duyên', 'k-duyen', '16', 'Công ty TNHH Hiếu Duyên Việt Nam ', 'Kỳ Duyên', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty TNHH Hiếu Duyên Việt Nam thuộc tập đoàn tài chính Hoa Viên.  <br />1. Là tập đoàn tài chính lớn nhất tại Đài Loan, xếp hạng thứ 291/500 trong danh sách các doanh nghiệp lớn nhất trên thế giới. Phạm vi kinh doanh gồm bảo hiểm nhân thọ, ngân hàng, bảo hiểm phi nhân thọ, đầu tư tín dụng, cổ phiếu. Năm 2006 tổng tài sản lên đến 106 tỉ đô la Mỹ. <br />2. Ở Đài Loan cứ tính hai người thì có một người là khách hàng của tập đoàn Hoa Lâm.  <br />3. Đã từng nhận giải thưởng nhà tuyển dụng xuất sắc nhất Châu Á.  <br />4. Ngày 21/11/2007 bảo hiểm nhân thọ Hoa Lâm chính thức được trao giấy phép kinh doanh, dự kiến sẽ đi vào hoạt động vào năm 2008. Mục tiêu của chúng tôi là trở thành tập đoàn tài chính hàng đầu Châu Á và sẽ cung cấp dịch vụ bảo hiểm tài chính hoàn hảo nhất cho khách hàng. Hiện nay tập đoàn tài chính Hoa Lâm ở Việt Nam có các công ty thành viên như ngân hàng Thế Hoa đã hợp tác đầu tư với ngân hàng Công thương thành lập nên ngân hàng Thế Việt (Indovina Bank). Bảo hiểm nhân thọ Hoa Lâm là công ty bảo hiểm lớn nhất ở Đài Loan, chúng tôi có nhu cầu tuyển dụng những ứng viên có nghiệp vụ xuất sắc về bảo hiểm, để đào tạo Bạn trở thành những ứng viên chuyên nghiệp với mức thu nhập hấp dẫn </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Cùng gieo mầm hôm nay cho tương lai phồn thịnh.</span></p>', '26 Phùng Khắc Khoan - HBT - Hà Nội', 1, ' 	0974.722.633', '', ' 	0974.722.633', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00');
INSERT INTO `jos_properties_profiles` (`id`, `mid`, `salutation`, `name`, `alias`, `c_name`, `company`, `con_name`, `c_summary`, `address1`, `cyid`, `phone`, `fax`, `mobile`, `web`, `credit`, `type_credit`, `desc_credit`, `gcviec`, `goicv`, `idgoi_cv`, `goi_cv`, `xem_cv`, `startdate`, `enddate`, `show_name`, `show_phone`, `show_fax`, `show_email`, `show_addr`, `show_packet`, `end_datebuy`, `image`, `logo_image`, `logo_image_large`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(62, 151, 'Mr.', 'Vo minh Tuan', 'vo-minh-tuan', '16', ' 	Cty TNHH MỘT THÀNH VIÊN AN SINH LỢI ', 'Vo Minh Tuan', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Cty TNHH MỘT THÀNH VIÊN AN SINH LỢI </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">- Chuyên cung cấp các mặt hàng: máy móc  và thiết bị phục vụ cho ngành y tế. <br />- Công ty chúng tôi là nhà phân phối độc quyền cho một số hãng nổi tiếng trên thới giới.</span></p>', '449/6/24 Lê Quang Định, phường 5, quận Bình Thạnh', 1, '0914500898', '', '0914500898', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(63, 152, 'Mrs.', 'Nguyễn Phương', 'nguyn-phng', '16', 'Công ty Cổ phần Cửa sổ nhựa Châu Âu Eurowindow', 'Nguyễn Phương', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty Eurowindow là công ty hàng đầu chuyên sản xuất các loại cửa sổ, cửa đi, vách ngăn bằng vật liệu u-PVC cao cấp, có lõi thép gia cường và hộp kính tiêu chuẩn chất lượng Châu Âu.</span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"> Hiện nay để mở rộng quy mô sản xuất chúng tôi có nhu cầu tuyển dụng các ứng viên có năng lực</span><span style="float: left; width: 544px; background-color: #ffffff;">kinh nghiệm tại các vị trí sau.</span></p>', ' 	Nhà máy 1 - lô 15 - khu CN Quang Minh - Mê Linh ', 1, '043.586.0727 - máy l', '', '043.586.0727 - máy l', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(64, 153, 'Mrs.', 'Vân', 'van', '16', 'Cong ty CP BDS Kmass', 'Ms Vân', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty CP Tư vấn BĐS Kmass Việt Nam được thành lập tại Hà Nội, Việt Nam vào tháng 1 năm 2009, cung cấp những giải pháp và dịch vụ bất động sản hiệu quả cho các chủ bất động sản và khách hàng tập trung chủ yếu vào thị trường Hà Nội, Việt Nam. </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Chiến lược thực hiện. </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Kmass thực hiện chiến lược phát triển theo từng thời điểm trong năm. Kmass với mục tiêu trở thành một mạng dịch vụ bất động sản chuyên nghiệp tại Hà nội, Việt nam. Sự phát triển của Kmass trong hai năm tới sẽ là một thương hiệu có uy tín trên thị trường Hà Nội và mở rộng theo suốt thị trường Việt nam. Kmass sử dụng các phương pháp kỹ thuật thực nghiệm và áp dụng vào các thị trường Hà Nội. Bằng việc phát triển các chuối sản phẩm BĐS vô giá và một số dịch vụ sáng tạo do Kmass tạo ra nhằm giúp cho các đối tác thành viên ngày càng chuyên nghiệp hơn và thành công hơn trong Đầu tư Thương mại, Đầu tư Nhà ở, mạng dịch vụ bất động sản Việt Nam, các khóa đào tạo nâng cao, các công cụ công nghệ tiên tiến, và hệ thống Web.</span></p>', '98 ĐỐC Ngữ - Ba Đình - Hà Nội', 1, ' 	0462733838', '', ' 	0462733838', ' http://kmassproperty.com', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(65, 154, 'Mr.', 'Minh', 'minh', '16', 'Công ty cổ phần Đạt Phát', 'Anh Minh', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty Cổ Phần Đạt Phát là đơn vị hoạt động kinh doanh nhiều lĩnh vực nhưng chủ yếu là lĩnh vực tư vấn đầu tư, môi giới kinh doanh và tiếp thị Bất động sản. Chúng tôi có một đội ngũ chuyên viên tư vấn bất động sản có trình độ chuyên môn cao, giàu kinh nghiệm, nhiệt tình với công việc và luôn đáp ứng mọi yêu cầu của quý khách hàng. Chúng tôi tự hào là công ty tư vấn hàng đầu trong lĩnh vực bất động sản. Mọi khách hàng khi đến với công ty chúng tôi đều hài lòng về sự phục vụ nhiệt tình, chu đáo và hiệu quả trong công việc. Chúng tôi muốn tạo môi trường thân thiện và là nơi làm việc thoải mái, tạo điều kiện để các nhân viên có thể phát huy hết khả năng và lợi thế của mình</span></p>', ' 	P502 tòa nhà 18T1 Đường Lê Văn Lương, Trung Hòa ', 1, '0977346888', '', '0977346888', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(66, 155, 'Mrs.', 'Nguyễn Hồng Loan', 'nguyn-hng-loan', '16', 'Sàn Bất động sản.info', 'Nguyễn Hồng Loan', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty Cổ phần Truyền Thông Bất động sản Việt Nam là đơn vị hoạt động trong lĩnh vực kinh doanh, tư vấn, quản lý và tiếp thị bất động sản. Chúng tôi tự hào là đơn vị hàng đầu cung cấp các dịch vụ bất động sản tại Việt Nam. Chúng tôi có một đội ngũ chuyên viên tư vấn bất động sản có trình độ chuyên môn cao, giàu kinh nghiệm và luôn lấy chữ tín làm đầu. Từ khách những khách hang tiêu dung đến những nhà đầu tư chuyên nghiệp khi đến với chúng Tôi là đến với sự thành công, và thịnh vượng nhất do vậy phương châm của chúng Tôi là “ Thông tin nhanh cho thành công lớn” <br />Các dịch vụ của chúng tôi bao gồm: <br />- Dịch vụ môi giới mua - bán, thuê - cho thuê bất động sản <br />- Dịch vụ quản lý, tiếp thị các dự án bất động sản <br />- Dịch vụ quảng cáo, rao bán, cho thuê bất động sản <br />- Dịch vụ tư vấn đầu tư, kinh doanh bất động sản <br />- Dịch vụ tư vấn pháp lý liên quan đến bất động sản <br />- Làm các thủ tục trọn gói về giao dịch bất động sản </span></p>', ' 	Phòng GD: 1005 Tòa Nhà 15T Nguyễn Thị Định - Tru', 1, '04.35558649', '', '04.35558649', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(67, 156, 'Mrs.', 'Thanh  Trúc', 'thanh-truc', '15', 'Công ty cổ phần Hiệp Hưng Phát', 'Thanh Trúc', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty Cổ phần Hiệp Hưng Phát là một công ty bất động sản chuyên nghiệp với ban quản lý nhiều kinh nghiệm, đội ngũ nhân viên năng động, chúng tôi cung cấp các dịch vụ toàn diện về bất động sản bao gồm: kinh doanh, quản lý, xây dựng, phân phối và định giá. Tham gia vào thị trường bất động sản, công ty cổ phần Hiệp Hưng Phát đã xây dựng được một thương hiệu riêng, hiện chúng tôi đang tìm kiếm thêm nguồn nhân lực năng động, có kinh nghiệm về lĩnh vực bất động sản để mở rộng hệ thống công ty.</span></p>', '06 đường 24,KDC Him Lam 6A (Đường Nguyễn Văn Cừ ND', 1, '54 317 317', '', '54 317 317', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(68, 157, 'Mr.', 'Phúc', 'phuc', '15', ' 	Công ty TNHH MTV Thực phẩm và Đầu tư FOCOCEV', 'A Phúc', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty TNHH MTV Thực phẩm &amp; Đầu tư FOCOCEV, là 1 trong 10 Công ty Nhà nước có doanh thu lớn nhất Bộ Công thương(Doanh thu năm 2009 đạt trên 1300tỷ đồng). Kinh doanh nhiều ngành nghề khác nhau như lương thực, thực phẩm, kỹ thuật công nghệ, in ấn, xe máy, thương mại, đầu tư, bất động sản... với nhiều chi nhánh, Công ty con trải dài từ Bắc tới Nam. Hiện nay, Công ty đang có nhu cầu tuyển dụng Nhân sự ở các vị trí sau:</span></p>', '21 Bùi Thị Xuân, P.Bến Thành, Q.1, Tp.HCM', 1, '08-62911024', '', '08-62911024', ' 	www.fococev.com', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(69, 158, 'Mrs.', 'Lê Trà My', 'le-tra-my', '15', 'Công ty CP Đại Việt Trí Tuệ', 'Lê Trà My', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Cty Cp Đại Việt Trí Tuệ được thành lập từ năm 1993. Qua hơn 15 năm hoạt động với hai lĩnh vực chính là kinh doanh Thiết bị ngành nước và hệ thống nhà hàng Legend Beer nay là Công ty Cổ phần Huyền Thoại Bia, một thương hiệu bia tươi cao cấp rất thân thuộc với giới sành bia ở Hà Nội. Năm 2008 Cty đã đầu tư tại TTTM Big C Đồng Nai với diện tích gần 2000 m2 bao gồm Nhà hàng Legend Beer là nhà hàng nấu bia tươi Đức tại chỗ với dây chuyền công nghệ hoàn toàn nhập từ Đức mong muốn mang lại những ly bia tươi độc đáo đến với quý khách hàng ,bên cạnh đó là nhà hàng Legend Seafood, nhà hàng hải sản tươi sống với các lọai hải sản ngon và lạ.</span></p>', ' 	109 Nguyễn Tuân, Thanh Xuân, Hà Nội', 1, ' 	04.37876655', '', ' 	04.37876655', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(70, 159, 'Mr.', 'Hữu Lập', 'hu-lp', '16', 'KINH DO FOOD', 'Hữu Lập', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Là tập đoàn thực phẩm hàng đầu Việt Nam. <br />Chuyên sản xuất và phân phối các sản phẩm bánh kẹo các loại. <br />Chúng tôi có hệ thống phân phối rộng khắp trên cả nước.</span></p>', '141 Nguyễn Du, Bến Thành, Quận 1, thành phố Hồ Chí', 1, '0983267827', '', '0983267827', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(71, 160, 'Mrs.', 'Hoàng Anh', 'hoang-anh', '16', 'N KID Corp (Phong cách sống mới)', 'Ms Hoàng Anh', '<p><span style="float: left; width: 544px; background-color: #ffffff;">tiNi World (Một trong những trung tâm của NKID)là chuỗi thương hiệu trung tâm giáo trí đầu tiên tại Việt Nam (www.tiniworld.com), mô hình giải trí mang tính giáo dục dành cho thiếu nhi từ 2 đến 12 tuổi. Hiện Công ty đang thực hiện việc mở rộng hệ thống các trung tâm mới tại Hà Nội, HCMC</span></p>', ' 	Tầng 7, tòa nhà AB, 76 Lê Lai, P. Bến Thành, Quậ', 1, '+84 8 3824 4661', '', '+84 8 3824 4661', 'www.tiniworld.com', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(72, 161, 'Mrs.', 'Thu Phương', 'thu-phng', '17', 'CTCP Tập đoàn Dược phẩm và Thương mại SOHACO', 'Nguyễn Thị Thu Phương', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty cổ phần Tập đoàn Dược phẩm và Thương mại SOHACO là một doanh nghiệp đã có 15 năm kinh nghiệm trong lĩnh vực sản xuất kinh doanh Dược phẩm và Máy tính thương hiệu. <br />Các đơn vị thành viên: Công ty cổ phần Dược phẩm SOHACO Miền Bắc, Công ty TNHH Sông Nhuệ, Xí nghiệp Dược phẩm Á Châu, Công ty TNHH KTTH Nam Thành, Công ty cổ phần Dược phẩm SOHACO Miền Nam và đơn vị liên doanh Nhà máy Dược phẩm Medisun </span></p>', 'P702 - Toà nhà CFM, số 23 Láng Hạ, Ba Đình, Hà Nội', 4, '04.35149986', '', ' 091.2231468', 'www.sohacogroup.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(73, 163, 'Mr.', 'Lê Thị Nê', 'le-th-ne', '16', 'Công ty Cổ phần Bách Dương', 'Lê Thị Nê', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty Cổ phần Bách Dương là công ty chuyên thiết kế và sản xuất các sản phẩm thời trang trình diễn và thời trang ứng dụng cao cấp với các mẫu thiết kế sang trọng, độc đáo đầy ấn tượng mang Thương hiệu Thời trang Kelly Bui. <br />Với hệ thống các Showroom lớn tại Hà Nội và TP. HCM, chúng tôi luôn chào đón các ứng viên xuất sắc, có đủ phẩm chất và năng lực</span></p>', ' 	89 Trung Liệt, Đống Đa, HN', 4, '0948994789', '', '0948994789', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(74, 164, 'Mrs.', 'Nhung', 'nhung', '16', ' Công ty Cổ phần Việt Thái Quốc Tế', 'Ms Nhung', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty cổ phần Việt Thái Quốc Tế (VTI) được thành lập năm 1998, hoạt động chính trong ngành dịch vụ và sản xuất cà phê. Với sự phát triển nhanh chóng, VTI hiện đã sở hữu hơn 60 quán cà phê mang thương nổi tiếng Highlands Coffee trên toàn quốc và nhà hàng đẳng cấp 5 sao 1911 tại Hà Nội. </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Bên cạnh đó, VTI còn phát triển mạng lưới phân phối và bán lẻ sản phẩm nổi tiếng mang thương hiệu Nike và tham gia vào ngành Quảng cáo thông qua việc liên doanh cùng với Tập đoàn Grey Global, tập đoàn lớn thứ 3 toàn cầu trong ngành quảng cáo phát triển thương hiệu. </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Ứng viên trúng tuyển sẽ có cơ hội được đào tạo tại Công ty theo tiêu chuẩn của Việt Thái Quốc Tế, làm việc trong môi trường chuyên nghiệp, có nhiều cơ hội giao tiếp với người nước ngoài, được hưởng chính sách lương thoả đáng và có cơ hội thăng tiến.</span></p>', 'Tầng 3 - Toà nhà Star Bolw - 2b Phạm Ngọc Thạch - ', 1, ' 04.35745391', '', ' 04.35745391', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(75, 165, 'Mr.', 'Lê Huỳnh Triều', 'le-hunh-triu', '17', ' Công ty Cổ phần Tập Đoàn Thái Tuấn', 'Lê Huỳnh Triều', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty CP tập đoàn Thái Tuấn là một Công ty chuyên về sản xuất và kinh doanh vải, quần áo thời trang có: </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">- Thương hiệu nổi tiếng được nhiều người tiêu dùng biết đến: 5 năm liền đứng TOPTEN 100 thương hiệu mạnh Việt Nam (2004 – 2008); 9 năm liền được người tiêu dùng bình chọn danh hiệu Hàng Việt Nam chất lượng cao (1999 - 2008); Danh hiệu Sao vàng Đất Việt 2003. </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"><br /></span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">-	Quy mô doanh thu: 30 triệu USD/năm. <br />-	Thị trường: Xuất khẩu qua 10 quốc gia (Asean, Trung Đông, châu Mỹ) và phủ khắp 64 tỉnh thành trong cả nước. <br />Hòa trong xu thế hội nhập quốc tế, cùng với sự trợ giúp của một công ty tư vấn nước ngoài tại Việt Nam - chúng tôi đã xây dựng cho mình một chiến lược phát triển để trở thành Công ty hàng đầu về thời trang tại khu vực Châu Á. </span></p>', ' 	1/148 Nguyễn Văn Quá, Phường Đông Hưng Thuận, Qu', 1, '0837194612', '', '0837194612', 'www.thaituanfashion.com', 0, '', '', '', 'goicv', 20, 40, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(76, 166, 'Mrs.', 'Quyen', 'quyen', '17', 'TẬP ĐOÀN SUNHOUSE', 'Ms Quyên', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Tập đoàn Sunhouse là một doanh nghiệp hàng đầu tại Việt Nam trong lĩnh vực sản xuất và phân phối các mặt hàng gia dụng cao cấp theo công nghệ tiên tiến của Hàn Quốc. Tham gia vào đội ngũ của Sunhouse, bạn sẽ có cơ hội làm việc trong môi trường năng động, chuyên nghiệp, thu nhập hấp dẫn, được hưởng đầy đủ các chế độ theo qui định của Luật lao động, được tham gia các khoá đào tạo chuyên ngành và có cơ hội thăng tiến</span></p>', '24 - 26 Phan Văn Trị, Quận Đống Đa, TP Hà Nội', 1, '04 37 36 66 76', '', '04 37 36 66 76', 'www.sunhouse.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(77, 167, 'Mr.', 'Hùng', 'hung', '16', 'Công ty Cổ phần Truyền thông ĐÔNG TÂY - DONGTAY Media Corp', 'Mr Hùng', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty cổ phần truyền thông Đông Tây là đơn vị chuyên tổ chức các sự kiện, sách, báo chí, truyền thông, truyền hình,hội thảo, hội họp... <br />In ấn, ấn phẩm quảng cáo, <br />Nội thất showroom <br />Quảng cáo ngoài trời <br />Tư vấn quảng cáo miễn phí...</span></p>', 'Ngõ 166 Kim Mã, Ngách 82/122 số nhà 14, Ba Đình, H', 1, '0942222129', '', '0942222129', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(78, 168, 'Mr.', 'Trịnh Lợi', 'trnh-li', '16', 'Công ty dược phẩm ANZ', 'Trịnh Thị Lợi', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty dược phẩm ANZ có trụ sỡ tại: số 12 ngõ 33 Đường Tạ Quang Bửu, phường Bách Khoa,Quận Hai Bà Trưng, HN. CTy ANZ chuyên cung cấp dòng sp chức năng và sp dinh dưỡng.</span></p>', ' 	số 12/33 Đường Tạ Quang Bửu, phường Bách Khoa qu', 1, '1900966986', '', '04.36231278', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(79, 169, 'Mr.', ' P.Nhân Sự', '-pnhan-s', '16', 'Công ty TNHH IVI', 'Phòng nhân sự', '<p><span style="background-color: #ffffff; width: 544px; float: left;">Công ty TNHH I VI chuyên sản xuất và kinh doanh nữ trang cao cấp CHARME’S với đội ngũ quản lý chuyên nghiệp từ Úc, môi trường làm việc năng động, thân thiện. </span></p>\r\n<p><span style="background-color: #ffffff; width: 544px; float: left;">Trãi qua những năm tháng hình thành và phát triển thương hiệu Công ty TNHH IVI đang trên đà phát triển và vươn tầm ra quốc tế. Để làm được điều đó chúng tôi luôn hiểu rằng con người là yếu tố quyết định sự thành công và phát triển. </span></p>\r\n<p><span style="background-color: #ffffff; width: 544px; float: left;">Với đội ngũ chuyên nghiệp chúng tôi tiếp tục phát triển lớn mạnh và tạo cơ hội cho tất cả những ai có khát vọng vươn lên. Tại IVI bạn sẽ có cơ hội phát triển cùng với sự lớn mạnh không ngừng của chúng tôi. </span></p>', '5-7-9 Nguyễn Trung Trực, Quận 1', 1, ' 	08 3822 3738', '', ' 	08 3822 3738', '', 0, 'p', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', 'y', 'y', 'y', 'y', 'y', 0, '0000-00-00', '', '', '', 1, 1, 0, '0000-00-00 00:00:00'),
(80, 170, 'Mrs.', 'Hảo', 'ho', '16', ' 	Công Ty EWAY INC', 'Ms Hảo', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Để thành công trong một môi trường kinh doanh hiện đại và liên tục thay đổi như ngày nay. Chúng tôi luôn ý thức được rằng chúng tôi là ai ? Chúng tôi là một nhóm các giám đốc điều hành, giám đốc tài chính…với hơn 10 năm kinh nghiệm trong việc phát triển và quản lý kinh doanh cho các tập đoàn nổi tiếng trên thế giới như : Pháp, Anh, Mỹ và Việt Nam. <br />Chúng tôi luôn tâm niệm chữ tín , niềm tin mới có thể tạo ra các giá trị đặc biệt cũng như cách thức làm việc phù hợp với nhu cầu của thị trường mới giúp chúng tôi đi đúng hướng .Điều này sẽ góp phần làm nâng cao sức mạnh của nhóm của chúng tôi. </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Năm 2005,Eway Inc được chuyển nhượng từ tập đoàn tư vấn và quản lý KING HORSE. Eway Inc chủ yếu đầu tư về lĩnh vực : bất động sản, thiết kế kiến trúc , quản lý khách sạn, khu nghỉ mát và các ngành công nghiệp du lịch, thương mại quốc tế, xuất nhập khẩu, thương mại điện tử… Bộ máy cơ cấu tổ chức của công ty EWAY INC bao gồm các phòng ban : Phòng Đầu tư công nghiệp, phòng thương mại điện tử và thương mại quốc tế, phòng nghiên cứu và phát triển,phòng thiết kế kiến trúc, phòng hành chính, phòng tài chính và phòng đào tạo các chương trình chuyên nghiệp </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"> Tiêu chí hoạt động của đội ngũ EWAY là “ trung thực, tin cậy, hợp tác và phát triển “. EWAY chúng tôi làm việc hết mình,hợp tác với tất cả các khách hàng trong và ngoài nước để cùng nhau tạo ra một tương lai tươi sáng hơn. Chúng tôi luôn không ngừng cố gắng tạo ra hàng loạt các sản phẩm mới cũng như ngày càng nâng cao chất lượng dịch vụ. </span></p>', '384/1C - Nam Kỳ Khởi Nghĩa St - Dist. 3 - Ho Chi M', 1, ' 01656096866', '', '01656096866', 'http://eway.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(81, 171, 'Mrs.', 'Nguyễn Thị Thu', 'nguyn-th-thu', '16', 'Công ty cổ phần đầu tư kinh doanh tài chính Việt Nam', 'Nguyễn Thị Thu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty Cổ Phần Đầu Tư kinh doanh tài chính Việt Nam là công ty có 100% vốn nước ngoài. Với nỗ lực làm việc của đội ngũ nhân viên nhiệt tình nhiều kinh nghiệm và trình độ cao. Đến nay công ty đã phát triển lớn mạnh với hơn 30 chi nhánh trên toàn quốc. Công ty đang không ngừng phấn đấu để trở thàng một trong những tập đoàn kinh doanh về tài chính lớn nhất Việt Nam. </span></p>', ' 	1144 Đê La Thành - Hà Nội', 1, '0978299586 ', '', '0978299586 ', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(82, 172, 'Mrs.', 'Nguyễn Thị Dung', 'nguyn-th-dung', '16', ' 	Công ty Cổ phần Đầu tư Forincons', 'Nguyễn  Thị Dung', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Được thành lập đầu năm 2008, với vốn điều lệ là: 60.000.000.000 đồng (Sáu mươi tỷ đồng), các cổ đông sáng lập của công ty bao gồm các cá nhân, doanh nhân và tổ chức đang hoạt động thành đạt tại Việt nam. Công ty Cổ phần Đầu tư Forincons là nơi hội tụ của các cổ đông có tiềm lực về tài chính cũng như có bề dày kinh nghiệm đầu tư trong lĩnh vực y tế, đặc biệt là trong lĩnh vực chẩn đoán và xạ trị ung bướu. <br />Chiến lược của Forincons Invest là trở thành một trong những đơn vị hàng đầu trong lĩnh vực chẩn đoán và xạ trị ung bướu ở Việt Nam. Mục tiêu đến năm 2012 của công ty là đầu tư và quản lý một mạng lưới bao gồm bốn trung tâm chẩn đoán và xạ trị kỹ thuật cao trên phạm vi toàn quốc. Để đạt được những mục tiêu này, công ty đã và đang xúc tiến chương trình hợp tác với các bệnh viện có tiềm năng ở các khu vực kinh tế phát triển, đồng thời mở rộng quan hệ với các nhà cung cấp trang thiết bị có uy tín trong và ngoài nước.</span></p>', ' 	6A, Sơn Tây, Ba Đình', 1, '04.37346764', '', '04.37346764', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(83, 173, 'Mr.', 'Hoàng Phú', 'hoang-phu', '14', 'Công ty CP Đầu tư Thương mại Hoàng Phú', 'Công ty CP Đầu tư Thương mại Hoàng Phú', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Chuyên cung cấp các giải pháp Công nghệ thông tin, phần mềm, thiết kế website, giải pháp thương mại điện tử, tư vấn giải pháp ứng dụng công nghệ thông tin cho doanh nghiệp, trường học,... <br />Nhằm nâng cao hơn nữa chất lượng của việc chăm sóc khách hàng của trung tâm truyền thông. Chúng tôi cần tuyển 02 nhân sự cho lĩnh vực sản phẩm in ấn, quảng cáo.</span></p>', 'Phòng 1008 tòa nhà N4A, Khu Đô thị Trung Hòa Nhân ', 1, '04.22109537', '', '04.22109537', ' 	www.hoangphu.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(84, 174, 'Mr.', 'Phòng HC- NS', 'phong-hc-ns', '15', 'Nhà hàng Phố Nướng Hawaii', 'Phòng HC- NS', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Nhà hàng cao cấp Phố Nướng Hawaii mang phong cách kiến trúc Châu Âu hiện đại, chuyên phục vụ các món ăn Âu Á và hải sản. Chúng tôi đang chào đón các ứng viên tự tin, tràn đầy năng lực và nhiệt huyết cho những vị trí tuyển dụng nhằm đáp ứng sự phát triển của nhà hàng.</span></p>', '127 Nguyễn Khoái – Hai Bà Trưng - Hà Nội', 1, '04. 398 449 86; 0904', '', '04. 398 449 86; 0904', 'www.phonuonghawaii.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(85, 175, 'Mr.', 'Nhật Duy', 'nht-duy', '16', ' 	Công Ty TNHH Thương Mại - Dịch Vụ - Nhà Hàng Hộp V', 'Nhật Duy', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Công ty chúng tôi đang trên đà phát triển mạnh mẽ trong lĩnh vực thương mại, dịch vụ, nhà hàng và giải trí tại TPHCM.</span></p>', '552 - 554 Trần Hưng Đạo, Q.5, Tp. HCM', 1, '3950 6789 - 09096776', '', '3950 6789 - 09096776', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(86, 176, 'Mrs.', 'Hồng Nhung', 'hng-nhung', '16', 'Công ty cổ phần Fiona VIệt Nam', 'Nguyễn Hồng Nhung', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty Cổ phần Fiona Việt Nam là thương hiệu thời trang hiện đại có đội ngũ thiết kế và kỹ thuật lành nghề, cùng chuỗi cửa hàng tại Hà Nội. Sản phẩm thương hiệu của chúng tôi tập trung vào thời trang công sở cho nữ, và một số dòng thời trang cho nam. Chúng tôi luôn quan tâm tới chất lượng, mẫu mã, giá thành sản phẩm phù hợp và dịch vụ tốt nhất giành cho khách hàng. </span></p>', '107 Thái Hà, Đống Đa, Hà Nội', 1, '0912028668', '', '0912028668', 'http://fiona.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(87, 177, 'Mrs.', 'Chị Khánh', 'ch-khanh', '16', 'Công ty TNHH HANEL-CSF', 'Chị Khánh', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty TNHH HANEL-CSF là công ty liên doanh, hoạt động trong lĩnh vực xây dựng và cung cấp dịch vụ Trung tâm dữ liệu (Data Center). Hiện nay công ty đang khai thác Data Center đạt chuẩn Tier 3+ tại khu công nghiệp Sài Đồng B <br /></span></p>', ' 	Khu công nghiệp Sài Đồng B- Long Biên Hà Nội.', 1, '3675 7019', '', '3675 7019', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(88, 178, 'Mr.', 'newstartour', 'newstartour', '16', 'Trung Tâm Du Lịch Quốc Tế - Ngôi Sao Mới', 'newstartour', '<p><span style="float: left; width: 544px; background-color: #ffffff;">NEWSTAR TOUR là Công ty du lịch hàng đầu tại Viêt Nam chuyên tổ chức các Tour du lịch trong nước và quốc tế, Du lịch chuyên đề, tổ chức sự kiện, hội nghị, hội thảo, tour khen thưởng và các Tour du lịch đặc biệt theo yêu cầu của khách hàng. Sau 12 năm hoạt động và kinh doanh trong nghành dịch vụ. </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Du lịch NEWSTAR TOUR đã ngày càng khẳng định được vị thế của mình trên thị trường trong và ngoài nước. Thương hiệu NEWSTAR TOUR đã và đang là một địa chỉ đáng tin cậy với hàng triệu khách hàng trong và ngoài nước. Bên cạnh đó, hiện nay Du lịch NEWSTAR TOUR đã và đang sở hữu một đội ngũ cán bộ và nhân viên phục vụ có năng lực và chuyên môn nghiệp vụ cao, tận tình, chu đáo chắc chắn sẽ đem lại sự hài lòng cho khách hàng mỗi khi sử dụng các dịch vụ của Du lịch NEWSTAR TOUR. Đây cũng sẽ là một môi trường làm việc năng động và tốt nhất cho các ứng viên thực sự có năng lực, đồng thời mong muốn khẳng định vị trí của mình trong lĩnh vực kinh doanh dịch vụ và phát triển nghề nghiệp. </span></p>', '44 - Nghi Tàm - Yên Phụ - Tây Hồ', 1, '0437170573', '', '0437170573', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(89, 179, 'Mrs.', 'Quỳnh Trang', 'qunh-trang', '16', 'CÔNG TY CP TRUYỀN THÔNG VÀ DV THƯƠNG MẠI THÁI BÌNH DƯƠNG', 'Quỳnh Trang', '<p><span style="float: left; width: 544px; background-color: #ffffff;">THAIBINHDUONGMEDIA là công ty hoạt động chuyên nghiệp trong lĩnh vực tổ chức sự kiện, quảng cáo và thiết kế in ấn. </span></p>', 'Số 1/4 Đường Trần Quý Kiên - Cầu Giấy - Hà Nội', 1, ' 	0945 268 686 ', '', ' 	0945 268 686 ', ' 	http://thaibinhduongmedia.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(90, 180, 'Mrs.', 'Tạ Giang', 't-giang', '16', ' 	Trường đào tạo doanh nhân PTI', ' 	Mrs Tạ Giang', '<p><span style="float: left; width: 544px; background-color: #ffffff;">TRƯỜNG ĐÀO TẠO DOANH NHÂN PTI là một "học viện" dành cho Doanh nhân và Giám đốc, một ngôi trường dành cho Doanh Nhân. <br />Hoài bão của PTI là "Việt Nam xuất khẩu Giám Đốc", sứ mệnh của PTI là "Đồng Hành Cùng Doanh Nhân Việt”  <br /> Nhằm hiện thực hóa sứ mệnh của mình, PTI đã và đang nỗ lực không ngừng trong việc nghiên cứu, thiết kế và biên soạn nhiều chương trình đào tạo đẳng cấp quốc tế và phù hợp với môi trường Việt Nam để cung cấp cho cộng đồng doanh nghiệp đang hoạt động tại đây. </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"><br />Mục tiêu của PTI là trở thành một “học viện” quản trị kinh doanh thực hành hàng đầu tại Việt Nam (“HỌC VIỆN” DOANH NHÂN PTI)</span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Cũng trong sứ mệnh nói trên, PTI đã tổ chức và phối hợp tổ chức thành công hàng loạt hội nghị và hội thảo (quốc gia và quốc tế) về các lĩnh vực kinh tế, quản trị kinh doanh,, nhân lực, văn hóa và giáo dục.Chẳng hạn như: Hội thảo khoa học về “văn hóa kinh doanh” với chủ đề "Văn Hóa &amp; Văn Hoá Doanh Nghiệp”; Hội thảo Quốc tế về kinh doanh với chủ đề "Marketing mới cho thời đại mới"; <br /> <br />Cùng với hoạt động đào tạo và việc tổ chức nhiều hội thảo, hội nghị quốc gia, quốc tế, PTI cũng đã và đang triển khai hàng loạt đề tài nghiên cứu khoa học nhằm góp phần xác lập và phổ biến những tư tưởng kinh doanh tiến bộ của thời đại cho người Việt, đồng thời đưa ra những giải pháp thiết thực về quản trị và kinh doanh cho cộng đồng doanh nghiệp Việt nam. <br /> <br />Đặc biệt, hiện PTI đang phối hợp với các tổ chức và cá nhân tâm huyết để cùng sáng lập và triển khai một số dự án giáo dục có ý nghĩa, nhưng không vì mục tiêu lợi nhuận (Not-for-profit Educational Projects). </span></p>', ' 	P405 34T Trung Hòa Nhân Chính Hà Nội', 1, '0904 800 636/ 046675', '', '0904 800 636/ 046675', 'http://www.pti.edu.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(91, 181, 'Mr.', 'Thắng', 'thng', '16', 'Công ty cổ phần thương mại xuất nhập khẩu Quyết Thắng', 'Mr Thắng', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Chuyên kinh doanh các dòng xe đã qua sử dụng và xe mới nhập khẩu 100%</span></p>\r\n<p> </p>', ' 	Số 1, Lê Văn Thiêm, Nhân Chính, Thanh Xuân, Hà N', 1, ' 	093 627 8888', '', ' 	093 627 8888', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(92, 182, 'Mrs.', 'Diễm', 'dim', '16', 'MANGO.VN Export Import and Investment Company Limited', 'Mrs Diễm', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty MANGO.VN Export Import and Investment Company Limited chuyên xuất khẩu nông sản, Trái cây tươi, phục vụ cho các chuỗi siêu thị lớn nhất tại thị trường LB Nga. <br />Hiện nay Công ty MANGO.VN đang tăng sản lượng xuất khẩu và mở rộng thị trường trong nước nên cần tuyển dụng nhiều nhân sự có trình độ và tinh thần trách nhiệm cao, nhằm giữ vững và phát triển thị trường xuất khẩu hàng nông sản Việt nam trên quy mô rộng. </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Đề nghị trước tiên gửi RESUME (kèm Ảnh) về hoạt động của bản thân trong thời gian đã trải qua kinh nghiệm tới e-mail: info@mango.vn <br /> Lãnh đạo Công ty sẽ xem xét, nếu được sẽ thông báo lịch mời phỏng vấn. <br /> </span></p>', ' 	23/158 Nguyen Khanh Toan', 1, ' 	0972.726.302', '', ' 	0972.726.302', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(93, 184, 'Mrs.', 'Hồng Anh', 'hng-anh', '16', 'Công ty TNHH Kỹ thuật Nam Hải', 'Hồng Anh', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty TNHH kỹ thuật Nam Hải là đơn vị uy tín hoạt động trong phạm vi cả nước với khả năng thiết kế - chế tạo – sửa chữa các hệ thống truyền động dầu thủy lực, bôi trơn sử dụng trong các hệ thống công nghiệp và các thiết bị, xe máy thi công công trình. </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Chúng tôi đang tìm kiếm các ứng viên năng lực tham gia phát triển hoạt động kinh doanh</span></p>', 'Số 88 Phạm Ngọc Thạch, Đống Đa', 1, ' 	04-35737122', '', ' 	04-35737122', 'www.hydraulics.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(94, 185, 'Mr.', 'Hanowindow', 'hanowindow', '16', 'CÔNG TY CỔ PHẦN TẬP ĐOÀN SẢN XUẤT CỬA NHỰA HÀ NỘI ', 'Hanowindow', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty Cổ phần Tập đoàn Sản xuất Cửa sổ nhựa Hà Nội - Hanowindow® uPVC là Doanh nghiệp chuyên sản xuất, thi công và cung cấp các sản phẩm có chất lượng cao, như Hệ cửa sổ, cửa đi, vách kính uPVC, Hệ cửa tự động (Automatic door), cửa chống cháy, cửa cuốn tự động và Hệ vách kính mặt dựng hiện đại.. mang nhãn hiệu Hanowindow® - Cùng phát triển cộng đồng! Nhà Máy của chúng tôi với công suất hơn 182.000 m2 sản phẩm các loại một năm, có dây chuyền sản xuất hiện đại, đồng bộ, có tính tự động hoá cao do các hãng của CHLB Đức và Italy chế tạo năm 2003 - 2004. Sản phẩm của Hanowindow® đã được xuất khẩu sang EU và một số nước khác trên thế giới </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Để mở rộng hoạt động kinh doanh theo chiến lược và định hướng của công ty trong thời gian tới tại thị trường Việt Nam, hiện chúng tôi đang có nhu cầu tuyển dụng nhân sự cho nhiều vị trí! </span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;"><br /></span></p>', 'Số 127 Nguyễn Tuân, Thanh Xuân, Hà Nội, Việt Nam', 1, '84 4 6285 4214/ 2216', '', '84 4 6285 4214/ 2216', ' 	http://www.hanowindow.com', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(95, 186, 'Mr.', 'Phạm Hoài Vũ ', 'phm-hoai-v-', '14', 'Công ty TNHH Minh Việt', 'Phạm Hoài Vũ ', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công ty TNHH Minh Việt , chuyên sản xuất lắp đặt sản phẩm nhôm kính cao cấp bao gồm cửa, vách kính v.v cho các công trình lớn với thương hiệu MINHVIETDOOR. <br /> <br />Phương châm hoạt động của công ty là NGƯỜI THẬT VIỆC THẬT <br /> <br />Công ty Minh Việt hiện là nhà phân phối sản phẩm nhôm kính cao cấp của 01 hãng nhôm kính có uy tín của châu Âu.  <br /> <br />Do nhu cầu phát triển, công ty Minh Việt đang tìm kiếm các ứng viên có năng lực và nhiệt huyết, tham gia các vị trí quan trọng của công ty. <br /> <br />Công ty Minh Việt có cơ chế lương xứng đáng đối với những ứng viên đáp ứng được sự phát triển lâu dài của Công ty. Công ty tin chắc, bất cứ ứng viên nào, được tuyển dụng vào làm tại Công ty đều thấy: Đây là môi trường tốt để cho các cá nhân phát triển tài năng và được đãi ngộ xứng đáng.</span></p>', ' 	C1701, Tòa nhà Lilama 21 tầng, 124 Minh Khai, Hà', 1, ' 	04.38683883', '', ' 	04.38683883', 'minhvietdoor.com', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(96, 187, 'Mr.', 'Hà Văn Lưu ', 'ha-vn-lu-', '17', 'TRƯỜNG CAO ĐẲNG CÔNG NGHIỆP & XÂY DỰNG', 'Hà Văn Lưu ', '<p>TRƯỜNG CAO ĐẲNG CÔNG NGHIỆP &amp; XÂY DỰNG Là một trường đào tạo đa ngành, đa bậc học trực thuộc Bộ Công Thương có uy tín ở khu vực Đông Bắc Bộ</p>', 'Phương Đông - Uông Bí - Quảng Ninh ', 1, '0904 445 215 ', '', '0904 445 215 ', 'www.cic.edu.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(97, 188, 'Mr.', ' Đặng Thành Tung', '-ng-thanh-tung', '16', 'Rainbow Architect - Construction & Trading Join Stok Company', ' Đặng Thành Tung', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Cty chuyên tư vấn - thiết kế và thi công công trình. lắp đặt thiết bị cho các công trình xây dựng. Tư vấn, quản lý lập dự án, lập hồ sơ mời thầu. Cung cấp và trang trí nội thất công trình... <br />Cty có môi trường làm việc chuyên nghiệp, luôn tạo điều kiện cho những cá nhân ham học hỏi và có trí hướng phấn đấu.</span></p>', ' 	Số 18 - Ngõ 62 Nguyễn Viết Xuân - Thanh Xuân - H', 1, '04.5189056', '', '04.5189056', ' www.raiarc.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(98, 189, 'Mr.', 'Mr Hải ', 'mr-hi-', '17', 'TẬP ĐOÀN HOA SAO', 'Mr Hải ', '<p>Tập đoàn Hoa Sao tự hào là một trong những doanh nghiệp dẫn đầu trong  lĩnh vực cung cấp dịch vụ chăm sóc khách hàng tại Việt Nam. Với hệ thống  hạ tầng công nghệ hiện đại, dịch vụ tiên tiến cùng hệ thống quản lý  chuyên nghiệp và bộ máy nhân sự lên đến gần 2000 nhân viên, Hoa Sao ngày  nay đã trở thành đối tác chiến lược của nhiều doanh nghiệp viễn thông,  ngân hàng, tài chính, bảo hiểm, du lịch ... hàng đầu tại Việt Nam. Bên  cạnh đó, với môi trường làm việc chuyên nghiệp, nuôi dưỡng sự sáng tạo,  tự chủ và niềm vui, Hoa Sao rất mong muốn được đón nhận các ứng cử viên  thực sự tâm huyết, gắn bó và có niềm đam mê công việc</p>', 'Số 70 Vương Thừa Vũ, Thanh Xuân, Hà Nội ', 1, '04 3565 9596 ', '', '0986333396', 'http://hoasao.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(100, 196, 'Mrs.', 'Trịnh Trần Ngọc Trang', 'trnh-trn-ngc-trang', '16', ' 	CN Công ty TNHH Truyền Thông Quảng Cáo Sức Sống', 'Trịnh Trần Ngọc Trang', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công Ty chúng tôi hoạt động trong lĩnh vực Quảng Cáo - Truyền Thông - Chuyên cung cấp các dịch vụ toàn diện về quảng bá thương hiệu,thực hiện các chương trình truyền thông quảng bá sản phẩm -Với thế mạnh khả năng sáng tạo không ngừng đáp ứng nhu cầu chuyên về lĩnh vực: thiết kế,in ấn,tổ chức sự kiện,hội chợ,trang trí nội thất,quà tặng... -Là công ty quảng cáo chuyên nghiệp với mục tiêu "sự hài lòng của khách hàng".Công ty chúng tôi đang cần những ứng viên năng động - có kinh nghiệm</span></p>', 'Số 29 Cao Bá Nhạ.F.Nguyễn Cư Trinh.Q1', 1, '08.39208658', '', '08.39208658', ' 	www.quangcaosucsong.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(101, 197, 'Mr.', 'Quyết', 'quyt', '14', 'CÔNG TY CỔ PHẦN THỰC PHẨM ĐÔNG Á', 'Mr.Quyết', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Là Công Ty sản xuất và kinh doanh bánh kẹo nhãn hiệu DAF có nhà máy đặt tại KCN Trảng Bàng Tỉnh tây Ninh.Các sản phẩm của Công Ty gồm bánh mềm kẹp kem,bánh mềm phủ sôcôla...</span></p>', '116 - Xóm Lẻ - Triều Khúc - Thanh Xuân - Hà Nội', 1, '0977153388', '', '0977153388', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(102, 207, 'Mr.', 'Thanh Lâm', 'thanh-lam', '16', ' TẠP CHÍ THƯƠNG HIỆU VIỆT', 'Thanh Lâm', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Là đơn vị trực thuộc Liên hiệp các Hội Khoa học và kỹ thuật Việt Nam, hoạt động trên lĩnh vực khoa học - công nghệ và sở hữu trí tuệ Tạp chí Thương hiệu Việt có chức năng, nhiệm vụ: Nghiên cứu triển khai, thực hiện các đề tài, dự án trong lĩnh vực công nghệ thông tin, truyền thông Internet; Xây dựng và phát triển, quảng bá thương hiệu; đào tạo ứng dụng các thành tựu khoa học - công nghệ để phát triển các chuyên ngành thuộc lĩnh vực trên; Chuyển giao kết quả và những tiến bộ khoa học - công nghệ về thông tin, truyền thông thương hiệu, tư vấn, thẩm định, đào tạo, tổ chức hội nghị, hội thảo về công nghệ thông tin, truyền thông Internet và thương mại điện tử; Hợp tác với các tổ chức, đoàn thể, cá nhân trong và ngoài nước trong quy định pháp luật cho phép để thực hiện nhiệm vụ.</span></p>', '45. Hoa Mai, p2. Phú Nhuận', 1, '0933156157', '', ' 0933156157', 'www.thv.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(103, 208, 'Mr.', ' Nguyễn Hồng Anh Khá', '-nguyn-hng-anh-kha', '16', 'Công ty cổ phần truyền thông Kim Cương', ' Nguyễn Hồng Anh Khá', '<p><span style="float: left; width: 544px; background-color: #ffffff;">CÔng ty cổ phần truyền thông Kim Cương là một trong những đối tác hàng đầu của các tập đoàn lớn như:VIettel,Prudencial,Vàng VIna...</span></p>', '33 Lê Trung Nghĩa, Phường 12, Quận Tân Bình, TPHCM', 1, '0907653534', '', '0907653534', 'www.callcenter.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(104, 83, 'Mr.', 'thanhtd1', 'thanhtd1', '18', 'abc', 'mrthanhtd86', '', 'trung yen', 1, '', '', '', '', 0, 'p', '', '', 'goicv', 27, 90, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 1, 0, '0000-00-00 00:00:00'),
(105, 215, 'Mrs.', 'thuhang22', 'thuhang22', '16', 'thuhang22', 'thuhang22', '<p>thuhang22thuhang22thuhang22</p>', 'trung yen', 1, '123456', '08 0271657', '111122223333', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(106, 216, 'Mr.', 'nhathongco', 'nhathongco', '16', 'công ty công nghệ thực phẩm nhật hồng', 'nhật Hồng', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  ty công nghệ thực phẩm nhật hồng được thành lập vào năm 1998. Vào những   ngày đầu mới thành lập công ty  tự hào là nhà sản xuất đầu tiên trên  thị trường việt nam  đưa ra dòng sản phẩm nước giải khát lô hội (nha  đam) và các sản phẩm khác được làm từ cây lô hôi? </span></p>', '07 Phan Đình Phùng - HCM', 1, '08.38107610', '', '08.38107610', 'footnhathong.com.vn', 0, 'p', '', '', 'goicv', 22, 40, 2, '0000-00-00', '0000-00-00', '', '', '', '', '', 1, '0000-00-00', '', '', '', 1, 1, 0, '0000-00-00 00:00:00');
INSERT INTO `jos_properties_profiles` (`id`, `mid`, `salutation`, `name`, `alias`, `c_name`, `company`, `con_name`, `c_summary`, `address1`, `cyid`, `phone`, `fax`, `mobile`, `web`, `credit`, `type_credit`, `desc_credit`, `gcviec`, `goicv`, `idgoi_cv`, `goi_cv`, `xem_cv`, `startdate`, `enddate`, `show_name`, `show_phone`, `show_fax`, `show_email`, `show_addr`, `show_packet`, `end_datebuy`, `image`, `logo_image`, `logo_image_large`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(107, 217, 'Mr.', 'mediapro', 'mediapro', '16', 'Công ty TNHH Kỹ Thuật - Thương Mại - Dịch Vụ Thăng Long', 'Hùng', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  ty TNHH Kỹ Thuật - Thương Mại - Dịch Vụ Thăng Long - là nhà cung cấp  giải pháp mang tính tổng thể trong lĩnh vực Quảng Cáo và Truyền thông  điện tử. Chúng tôi chuyên cung cấp các dịch vụ: </span></p>', '79 Đường B2, P.Tây Thạnh, Q.Tân Phú, HCM', 1, '0908179636', '', '0908179636', 'www.mediapro.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(108, 218, 'Mrs.', 'Hang', 'hang', '15', 'Cty Hang', 'hang', '<p>jdgfgg sjgfgg hdgkghg dfgfr  dfdfgf</p>', 'ha noi', 1, '0904640930', '0437890009', '0437890009', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(110, 222, 'Mr.', 'Đặng Ngọc Hà', 'ng-ngc-ha', '16', 'Công ty TNHH Nhân Lực Việt Bắc', 'Đặng Ngọc Hà', '<p>Công ty TNHH Nhân Lực Việt Bắc là một trong những doanh nghiệp hàng đầu trong lĩnh vực cung cấp nhân sự cho các công ty đối tác 100% vốn nước ngoài.</p>', '705 Tòa nhà LICOGI 18 Đường Thăng Long- Nội Bài ( ', 1, '0423228999', '', '0989727799', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(111, 224, 'Mr.', 'hangnt05', 'hangnt05', '15', 'Cty thực phẩm Hang', 'Chị Hằng', '<p>hangnt05 hangnt05 hangnt05</p>', 'ha noi', 1, '0989087000', '', '0989087000', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(112, 228, 'Mr.', 'Phạm Trọng Nghĩa', 'phm-trng-ngha', '14', 'Cty TNHH Nhân Lực Việt Bắc', 'Phòng nhân sự', '', 'Sóc Sơn', 1, '04-23228999', '046278529', '0945272828', '', 0, '', '', '', 'goicv', 24, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(113, 229, 'Mr.', 'admin123', 'admin123', '15', 'admin123', 'ssssssadmin123', '', '22111', 4, '', '', '', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(114, 230, 'Mr.', 'Mss Quỳnh', 'mss-qunh', '15', 'CÔNG TY DỊCH VỤ KẾ TOÁN HẢI LINH', 'Mss Quỳnh', '<p>Văn phòng dịch vụ kế toán Hải Linh xin gửi tới Quý doanh nghiệp lời chào trân trọng nhất! <br /><br />Văn phòng dịch vụ kế toán Hải Linh thực chất là do một nhóm người đã và đang làm kế toán trưởng cho các DN, có ít nhất 08 năm kinh nghiệm làm việc thực tế kết hợp với nhau cùng đồng tâm – đồng trí - đồng lòng với phương châm ''Đo lọ nước mắm, đếm củ dưa hành – Tiết kiệm tới từng đồng cho DN</p>', 'Phòng 202 - chung cư N06 B1 KĐT Dịch Vọng - đường ', 1, '0977.092.58', '', '', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(115, 231, 'Mr.', 'Nguyễn Anh Minh', 'nguyn-anh-minh', '14', 'Khách hàng của Cty TNHH Nhân Lực Việt Bắc', 'Phòng nhân sự', '', '705 Tòa Nhà LICOGI18, Mê Linh, Hà Nội', 1, '04-23228999', '0462876529', '', 'http://vieclamviet.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '115_logo.jpg', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(116, 232, 'Mr.', 'Tống Văn Dũng', 'tng-vn-dng', '16', 'Công ty CP Thế Giới Di Động', 'Tống Văn Dũng', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Thegioididong.com  - Hệ thống siêu thị ĐTDĐ - Laptop toàn quốc. Với hơn 3000 nhân viên,  gần 70 siêu thị ĐTDĐ, Laptop và 7 trung tâm sửa chữa kỹ thuật và bảo  hành trên toàn quốc, Thế Giới Di Động vinh dự được khách hàng bình chọn  là hệ thống bán lẻ điện thoại di động tốt nhất trong những năm qua và là  đối tác chiến lược tin cậy của LG và DELL.Với sự phát triển không  ngừng, Thế Giới Di Động được vinh dự nhận được 2 giải thưởng danh giá  “Top 5 công ty tăng trưởng nhanh nhất Châu Á”, “Top 10 nhà bán lẻ hàng  đầu Việt Nam”</span></p>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">Phương châm "Lắng nghe và chia sẻ" luôn mong muốn đem đến sự hài lòng cho khách hàng</span></p>', '34 Thành Công, Ngõ 12 Láng Hạ, Q. Ba Đình, Hà Nội', 1, '0976801443', '', '', 'thegioididong.com', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(117, 237, 'Mr.', 'Thanh', 'thanh', '14', 'Thanh trung', 'Trung', '', ' asdf asdf asdf asdf asdf', 4, '', '', '', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '117_logo.jpg', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(118, 238, 'Mr.', 'Trueman', 'trueman', '14', 'ABCDE', 'hrhsthh', '', 'dsgarg', 5, '', '', '', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(119, 240, 'Mr.', 'nhàn', 'nhan', '14', 'philong', 'thanh nhàn', '<p>thiết kế website</p>', 'gò vấp', 1, '', '', '', 'philong.info', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(120, 241, 'Mr.', 'ngan hà', 'ngan-ha', '14', 'philong', 'Ngân hà', '<p>chuyên thiết kế website</p>', 'gò vấp', 1, ' ', '', '', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(121, 242, 'Mr.', 'thuhang.mnc', 'thuhangmnc', '15', 'công ty tnhh Vạn An', 'hằng', '<p>zfdghghg</p>', '07 Phan Đình Phùng - HCM', 1, '', '', '', '', 0, 'p', '', '', 'goicv', 25, 40, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 1, '2011-01-20', '', '', '', 0, 1, 0, '0000-00-00 00:00:00'),
(122, 243, 'Mr.', 'Nguyễn Thanh Tân', 'nguyn-thanh-tan', '16', 'Đại Thành Đạt', 'Nguyễn Thanh Tân', '<p>Công ty TNHH MTV TM XNK Đại Thành Đạt được thành lập từ năm 2007 là một công ty tiên phong hàng đầu về nhập khẩu trực tiếp và phân phối sỉ, lẻ Giấy Dán tường HQ hiệu Cosmos và Geanari, được cấp giấy chứng nhận đảm bảo quyền đại lý từ nhà máy sản xuất, Cty nhập hàng số lượng trên 95% có sẵn của mỗi cuốn mẫu về VN. Đáp ứng nhu cầu cung ứng tại chỗ của khách hàng.</p>', '221 Lê Đại Hành, P13, Q11, HCM', 1, '39621916', '39621915', '0908251085', 'www.daithanhdat.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '_logo.JPG', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(123, 244, 'Mr.', 'Phi', 'phi', '14', 'Đô Thị Địa ốc', 'Mr Địa', '<p>http://dothidiaoc.com/ chuyên về địa ốc</p>', 'gv', 1, '', '', '', '', 0, '', '', '', 'goicv', 26, 40, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '_logo.png', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(124, 245, 'Mr.', 'Anh Ngọc', 'anh-ngc', '16', 'Công ty CP XNK & PP Facom Việt Nam', 'Anh Ngọc', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  ty CP XNK &amp; PP Facom Việt Nam chuyên Nhập khẩu và Phân phối chính  thức các Thiết bị Nhà bếp, hàng Điện tử, điện lạnh, điện gia dụng như :  Bếp gas, máy hút khử mùi, Ti vi, Tủ lạnh,.... tại Hà Nội và các tỉnh  phía Bắc.</span></p>', 'Phòng 1211, Tầng 12, Tòa nhà CT6, ( Giữa tòa nhà M', 1, '0989999456', '', '', 'wwww.facom.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(125, 246, 'Mr.', 'Anh Đào', 'anh-ao', '16', 'Anh Đào', 'Anh Đào', '', '	71/16 Cong Hoa, Ward 4, Tan Binh Dist. Ho Chi Min', 1, '22450761', '22450761', '22450761', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(126, 247, 'Mr.', 'Quy', 'quy', '16', 'CÔNG TY CỔ PHẦN TƯ VẤN VÀ ĐẦU TƯ THIÊN PHÚC', 'Lý Đình Quý', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  ty cổ phần tư vấn và đầu tư Thiên Phúc có văn phòng giao dịch tại 164  Triệu Việt Vương. Công ty chúng tôi là 1 công ty chuyên về tư vấn giao  dịch ngoại tệ, vàng, dầu mỏ.</span></p>', '101 Láng Hạ', 1, '0983533986', '', '0983533986', 'http://cafeforex.net', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(127, 248, 'Mr.', 'Hieu', 'hieu', '16', 'TNHH MTV Mốt Quốc Tế', 'Anh HIếu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  ty TNHH MTV Mốt Quốc Tế với nhãn hiệu thời trang Blook và Agxy. Công ty  chúng tôi chuyên sản xuất các mặt hàng thời trang cotton nam, nữ với  kiểu dáng trẻ trung, năng động dành cho giới trẻ Việt Nam. Với công nghệ  tiên tiến, đội ngũ nhân viên giàu kinh nghiệm, các sản phẩm thời trang  của công ty Mốt Quốc Tế đã khẳng định được vị thế của mình trên thị  trường thời trang Việt Nam và được giới trẻ đón nhận nồng nhiệt.</span></p>', '4A - 6 Ngô Quyền, Phường Tân Thành, Quận Tân Phú', 1, '6269 3847-6269 3848 ', '', '6269 3847-6269 3848 ', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(128, 249, 'Mrs.', 'Phạm Thị Bích Vân', 'phm-th-bich-van', '16', '	DNTN 7 SAO', 'Phạm Thị Bích Vân', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  ty chúng tôi chuyên về sản xuất gia công mặt hàng nón xuất  khẩu sang thị trường các nước như Nhật, Hàn Quốc,...</span></p>', '	60/6 Phạm Văn Chiêu, P.12, Q.Gò Vấp', 4, '	08.54360972', '', '	08.54360972', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(129, 250, 'Mr.', '	Nguyễn Duy Khánh', '-nguyn-duy-khanh', '16', 'CÔNG TY PUNGKOOK SAIGON II', '	Nguyễn Duy Khánh', '<table style="width: 457px; height: 46px;" border="0" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td valign="top"></td>\r\n<td>\r\n<p><span style="float: left; width: 544px; background-color: #ffffff;">CÔNG TY PUNGKOOK SAIGON II</span></p>\r\n<p>Là Công ty 100% vốn nước ngoài thuộc tập đoàn Pungkook, với hơn 14000 công nhân, chuyên may balo, túi xách, túi hành lý, ..</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>', '	2A, Đường số 8, KCN Sóng Thần I, Dĩ An , Bình Dươ', 1, '0650.3791166 (ext: 2', '', '0650.3791166 (ext: 2', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(130, 251, 'Mrs.', 'Ms Hà', 'ms-ha', '16', 'Tập Đoàn Hoa Sao', 'Ms Hà', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Tập  đoàn Hoa Sao tự hào tập đoàn chăm sóc khách hàng đầu tiên và số 1 tại  Việt Nam. Với hệ thống hạ tầng công nghệ hiện đại, dịch vụ tiên tiến  cùng hệ thống quản lý chuyên nghiệp và bộ máy nhân sự lên đến gần 2000  nhân viên, Hoa Sao ngày nay đã trở thành đối tác chiến lược của nhiều  doanh nghiệp hàng đầu tại Việt Nam. Bên cạnh đó, với môi trường làm việc  chuyên nghiệp, nuôi dưỡng sự sáng tạo, tự chủ và niềm vui, Hoa Sao rất  mong muốn được đón nhận các ứng cử viên thực sự tâm huyết, gắn bó và có  niềm đam mê công việc.</span></p>', 'Số 70 Vương Thừa Vũ- Thanh Xuân- Hà Nội', 1, '04 .3565 9596', '', '04 .3565 9596', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(131, 252, 'Mrs.', 'mr Cong', 'mr-cong', '16', 'OLECO', 'Mr cong', '<p><span style="float: left; width: 544px; background-color: #ffffff;">OLECO  là công ty chuyên về xây dựng thuộc Bộ Nông Nghiệp và Phát Triển Nông  Thôn. Có trên 20 năm hoạt động trong lĩnh vực xây dựng và hợp tác lao  động quốc tế. Giấy phép kinh doanh số: 05/LDTBXH-GP</span></p>', 'Km 10 Thanh Tri Ha Noi', 1, '0978934468', '', '0978934468', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(132, 253, 'Mr.', 'Hoàn', 'hoan', '16', 'Tập Đoàn Hoa Sao', 'Hoàn', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Tập  đoàn Hoa Sao tự hào tập đoàn chăm sóc khách hàng đầu tiên và số 1 tại  Việt Nam. Với hệ thống hạ tầng công nghệ hiện đại, dịch vụ tiên tiến  cùng hệ thống quản lý chuyên nghiệp và bộ máy nhân sự lên đến gần 2000  nhân viên, Hoa Sao ngày nay đã trở thành đối tác chiến lược của nhiều  doanh nghiệp hàng đầu tại Việt Nam. Bên cạnh đó, với môi trường làm việc  chuyên nghiệp, nuôi dưỡng sự sáng tạo, tự chủ và niềm vui, Hoa Sao rất  mong muốn được đón nhận các ứng cử viên thực sự tâm huyết, gắn bó và có  niềm đam mê công việc.</span></p>', 'Số 70 Vương Thừa Vũ- Thanh Xuân- Hà Nội', 1, '0986 333 97', '', '0986 333 97', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(133, 254, 'Mr.', 'Nguyễn Hà Long', 'nguyn-ha-long', '16', 'Công ty cổ phần viễn thông Giá trị việt', 'Nguyễn Hà Long', '<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td valign="top"><br /></td>\r\n<td>Là công ty cổ phần của mobifone  . với đội ngũ BHTT king doank phát triển thị trường thuê bao trả sau  của mobifone nhận đặt số sim mobifone dễ nhớ , số đẹp , số lặp , số ý  nghĩa ....</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'Phòng 405 tầng 4 - khu CT2A - khu đô thị Xa La - H', 1, '	090.220.0246', '', '	090.220.0246', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(134, 255, 'Mr.', 'Hoàn', 'hoan', '16', '	Số 70 Vương Thừa Vũ- Thanh Xuân- Hà Nội', 'Mr Hoàn', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Tập  đoàn Hoa Sao tự hào tập đoàn chăm sóc khách hàng đầu tiên và số 1 tại  Việt Nam. Với hệ thống hạ tầng công nghệ hiện đại, dịch vụ tiên tiến  cùng hệ thống quản lý chuyên nghiệp và bộ máy nhân sự lên đến gần 2000  nhân viên, Hoa Sao ngày nay đã trở thành đối tác chiến lược của nhiều  doanh nghiệp hàng đầu tại Việt Nam. Bên cạnh đó, với môi trường làm việc  chuyên nghiệp, nuôi dưỡng sự sáng tạo, tự chủ và niềm vui, Hoa Sao rất  mong muốn được đón nhận các ứng cử viên thực sự tâm huyết, gắn bó và có  niềm đam mê công việc.</span></p>', 'Số 70 Vương Thừa Vũ- Thanh Xuân- Hà Nội', 1, '	0986.3333.97', '', '	0986.3333.97', 'www.hoasao.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(135, 256, 'Mr.', 'Bùi Văn Tân', 'bui-vn-tan', '16', '	Công Ty TNHH BKL', '	BÙI VĂN TÂN', '<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td valign="top"><br /></td>\r\n<td>Công ty TNHH BKL là công ty chuyên nhập khẩu dầu gốc và phụ gia cung cấp cho thị trường Việt Nam.  <br /> Đầu năm 2009 được sự hợp tác đầu tư vốn và hỗ trợ kỹ thuật từ  phía đối tác Banyard Singapore  Nhà Máy Pha Chế Dầu Nhờn BKL bắt đầu  khởi công và chính thức đi vào sản xuất từ quý 3 năm 2010.  <br /> <br /> Nhà Máy Pha Chế Dầu Nhờn BKL được đặt tại KCN Thịnh Phát, Huyện  Bến Lức, Tỉnh Long An (gần đường cao tốc Trung Lương ),với diện tích  hơn 4.000 m2, được trang bị máy móc hiện đại nhằm đáp ứng các yêu cầu về  tính năng kỹ thuật của các nhà chế tạo máy Hoa Kỳ, Châu Âu, và Nhật  Bản. Với công suất thiết kế của nhà máy hơn 3.000 sp/giờ BKL không chỉ  đáp ứng nhu cầu trong nước mà còn đáp ứng các đơn hàng từ các nước trong  khối ASEAN</td>\r\n</tr>\r\n</tbody>\r\n</table>', '	Số 02 - Nguyễn Thế Lộc - P..2 - Tân Bình - TP.HCM', 1, '0938.500.010', '', '0938.500.010', 'www.yukilube.com', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(136, 257, 'Mr.', 'Công ty cổ phần tư vấn toàn câu Vina - Globavina.,', 'cong-ty-c-phn-t-vn-toan-cau-vina-globavina-jsc', '16', 'Công ty cổ phần tư vấn toàn câu Vina - Globavina., JSC', 'Công ty cổ phần tư vấn toàn câu Vina - Globavina., JSC', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  ty Tư vấn toàn cầu Globavina là công ty hoạt động trong lĩnh vực Tư vấn  giám sát và quản lý dự án. Hiện tại công ty chúng tôi đang triển khai   nhiều dự án cấp đặc biệt với các đối tác chiến lược là các tập đoàn hàng  đầu của Hàn Quốc. <br />Các dự án trọng điểm như : Habico Tower Project (36 flr), Boo Young International Apartment, Bujeon Factory...</span></p>', 'P1209 17T9, Trung Hòa Nhân Chính, Hà Nội', 1, '+84 4 6281 1377', '', '+84 4 6281 1377', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(137, 258, 'Mr.', 'Nguyễn MInh Tú ', 'nguyn-minh-tu-', '16', 'Công ty cổ phần xây dựng và tư vấn thiết kế MILIGIÂY ', 'Nguyễn MInh Tú ', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  ty cổ phần xây dựng và tư vấn thiết kế MILIGIÂY  là một trong những  công ty tiên phong trong lĩnh vực Thiết kế kết cấu,điện,nước của nhà cao  tầng. Nhận thấy thế mạnh mà mình đang có được trên thị trường và định  hướng phát triển của ban lãnh đạo công ty, nên trong giai đoạn này chúng  tôi đang mong muốn tìm kiếm được những ứng viên thích hợp và tâm huyết  để thăng tiến cùng công ty. <br />Khi đến với công ty chúng tôi các bạn được làm việc trong một môi  trường chuyên nghiệp và có điều kiện để phát huy sự sáng tạo, phát triển  bản thân cũng như có cơ hội trở thành những lãnh đạo chủ chốt của công  ty.Bạn là con người năng động, mong muốn khẳng định bản thân, thành đạt  và có thu nhập cao, hãy đến với chúng tôi để hiện thực hoá điều đó. </span></p>', 'Phòng 1002, Nơ9A, Bán Đảo Linh Đàm -Quận Hoàng Mai', 1, '	0422 390 188 ;01683', '', '	0422 390 188 ;01683', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(138, 259, 'Mr.', 'Nguyễn Thị Thu Hà', 'nguyn-th-thu-ha', '16', '	Evitco Travel', 'Nguyễn Thị Thu Hà', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Evitco  Travel là một trong những công ty du lịch uy tín hàng đầu tại Việt Nam.  Với phương châm “Chất lượng dịch vụ luôn là ưu tiên số 1”, ngay từ khi  thành lập Evitco đã không ngừng lớn mạnh và phát triển. Để mở rộng hoạt  động kinh doanh, hiện nay Evitco đang có nhu cầu tuyển dụng nhân sự cho  vị trí sau</span></p>', 'Số 08 Đặng Văn Ngữ, Đống Đa, Hà Nội', 1, '3 5376152, 35376154', '', '3 5376152, 35376154', 'www.evitco.com', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(139, 260, 'Mr.', 'Bộ Phận Tuyển Dụng', 'b-phn-tuyn-dng', '16', 'Công ty Cổ phần Việt Thái Quốc Tế', 'Bộ phận tuyển dụng', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  ty cổ phần Việt Thái Quốc Tế (VTI) được thành lập năm 1998, hoạt động  chính trong ngành dịch vụ và sản xuất cà phê. Với sự phát triển nhanh  chóng, VTI hiện đã sở hữu hơn 60 quán cà phê mang thương nổi tiếng  Highlands Coffee trên toàn quốc và nhà hàng đẳng cấp 5 sao 1911 tại Hà  Nội.  <br />Bên cạnh đó, VTI còn phát triển mạng lưới phân phối và bán lẻ sản  phẩm nổi tiếng mang thương hiệu Nike và tham gia vào ngành Quảng cáo  thông qua việc liên doanh cùng với Tập đoàn Grey Global, tập đoàn lớn  thứ 3 toàn cầu trong ngành quảng cáo  phát triển thương hiệu. <br />Ứng viên trúng tuyển sẽ có cơ hội được đào tạo tại Công ty theo tiêu  chuẩn của Việt Thái Quốc Tế, làm việc trong môi trường chuyên nghiệp,  có nhiều cơ hội giao tiếp với người nước ngoài, được hưởng chính sách  lương thoả đáng và có cơ hội thăng tiến.</span></p>', 'Tầng 3 - Toà nhà Star Bolw - 2b Phạm Ngọc Thạch - ', 1, '	04.35746046 ext 20', '', '	04.35746046 ext 20', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(140, 261, 'Mr.', 'Nguyễn Hữu Đông', 'nguyn-hu-ong', '16', 'Công ty cổ phần quốc tế vietlinks Hà Nội', 'Nguyễn Hữu Đông', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Chuyên kinh doanh du lịch lữ hành nội địa và quốc tế</span></p>', 'phố Lai Xá -Kim Chung -Hoài Đức - Hà Nội', 1, '0976955081', '', '0976955081', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(141, 262, 'Mr.', 'Anh Thành', 'anh-thanh', '16', 'Cổ Phần Khang Vương', 'Anh Thành', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Chuyên  đào tạo, hội thảo, chuyên nghiên cứu thị trường, môi giới thương  mại,thương mại điện tử, phân phối các sản phẩm nhập khẩu, đặc biệt là  các sản phẩm đồ uống, nước uống sinh học, mỹ phẩm, sản phẩm giảm cân... </span></p>', '384/32 Quang rung, Quận Gò Vấp', 1, '	0975053559', '', '	0975053559', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(142, 263, 'Mrs.', '	CHỊ THU', '-ch-thu', '16', 'Công ty TNHH Anh Huy Travel', 'CHỊ THU', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Công  ty là một công ty thẩm định hàng đầu trong lĩnh vực thương mại và dịch  vụ. Hiện nay do nhu cầu phát triển kinh doanh không ngừng để phục vụ  khách hàng rộng khắp trên địa bàn thành phố Hà Nội và một số tỉnh miền  bắc. Công ty liên tục tìm kiếm và tuyển dụng ứng viên có đủ khả năng hợp  tác làm việc bán thời gian hoặc toàn thời gian, đây là một công việc ổn  định và lâu dài.</span></p>', '	521 Kim Mã, Ba Đình, Hà Nội', 1, '0974088325', '', '0974088325', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(143, 264, 'Mr.', 'Tham', 'tham', '16', 'Công ty CP bệnh viện máy tính Quốc tế iCARE', '	Truong Cao dang nghe iSPACE', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Năm  2005, sách kỷ lục Việt Nam đã công nhận ý tưởng: "iCARE-Bệnh Viện Máy  Tính đầu tiên tại Việt Nam". iCARE là một Bệnh Viện Máy Tính thực thụ  với các khoa chức năng: Khoa Cấp cứu &amp; Điều trị ngoại trú, Khoa Điều  trị Nội trú, Khoa Giải phẫu &amp; Phục hồi chức năng..v..v.. cung cấp  cho khách hàng các gói dịch vụ sữa chữa máy tính đa dạng và chuyên  nghiệp. Nối tiếp ý tưởng trên. Trung tâm Đào tạo Bác Sỹ Máy Tính Thực  Hành iSPACE (hiện nay đã được nâng cấp thành Trường Cao Đẳng Nghê  iSPACE)- là đơn vị trực thuộc Công ty Cổ phần Bệnh Viện Máy Tính Quốc Tế  iSPACE - được thành lập và là trường đầu tiên tại Việt Nam nghiên cứu  ứng dụng thành công chương trình đào tạo Bác Sỹ Máy Tính Thực Hành. Mục  tiêu đào tạo của iSPACE là lấy hiệu quả thực tiễn làm thước đo chất  lượng đào tạo, các Bác Sỹ Máy Tính tương lai được thực hành ngay tại  Bệnh Viện Máy Tính Quốc Tế iCARE theo từng cấp độ của chương trình. Đến  nay đã có hơn 20.000 học viên theo học tại cơ sở của iSPACE trên toàn  quốc. Tháng 03/2009, iCARE-iSPACE chính thức đi vào hoạt động tại Hà  Nội, mang đến cho các bạn trẻ có năng lực và hoài bão một cơ hội làm  việc trong môi trường năng động và chuyên nghiệp.</span></p>', '12 Trần Đại Nghĩa - Hai Bà Trưng - HN', 1, '	0462626666', '', '	0462626666', '	icare.com.vn - ispace.edu.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(144, 265, 'Mrs.', 'Đỗ Hồng Phước', '-hng-phc', '17', '	BEST BUY CO, LTD - CÔNG TY TNHH LỰA CHỌN HOÀN HẢO ', 'Ms. Đỗ Hồng Phước', '<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td valign="top"><br /></td>\r\n<td>Best Buy là công ty tiên phong trong lĩnh vực bán hàng qua truyền hình tại Việt Nam, bắt đầu từ năm 2001. Sản phẩm được cung cấp từ Bestbuy rất đa dạng, từ các dụng cụ tập luyện thể dục thể thao, điện gia dụng và các sản phẩm chăm sóc sắc đẹp cùng với nhiều sản phẩm phục vụ tốt nhất cho cuộc sống và tiện nghi của con người. <br />Best Buy có mạng lưới giao hàng trên toàn quốc và cam kết phục vụ khách hàng với dịch vụ chuyên nghiệp nhất.</td>\r\n</tr>\r\n</tbody>\r\n</table>', '	17 Phạm Hùng - Mỹ Đình - Từ Liêm - Hà Nội ', 1, '043.39386477 ', '', '043.39386477 ', '	www.bestbuy.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(145, 266, 'Mr.', '	Lê Quang Dũng', '-le-quang-dng', '17', 'Công ty TNHH SX-TM DV Đức Nhân', '	Lê Quang Dũng', '<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td valign="top"><br /></td>\r\n<td>Là công ty chuyên sản xuất đồ gỗ nội, ngoại thất <br />Hiện tại công ty có hơn 500 CB-CNV</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'Ấp 1, Thạnh phước, Tân Uyên, Bình Dương', 1, '	0903.147.792', '', '	0903.147.792', 'www.ducnhan.com', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(146, 267, 'Mr.', '	Nguyễn Thành Long', '-nguyn-thanh-long', '16', '	CN Công ty CP đại lý vận tải SAFI tại Hải Phòng', '	Nguyễn Thành Long', '<p><span style="float: left; width: 544px; background-color: #ffffff;">SAFI hoạt động trong lĩnh vực Môi giới hàng hải và Đại lý tàu biển, khai thác kho bãi, dịch vụ Logistics... </span></p>', 'Tầng 6 số 22 Lý Tự Trọng', 1, '	CN Công ty CP đại l', '', '	CN Công ty CP đại l', 'www.safi.com.vn', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(147, 268, 'Mrs.', 'Giang', 'giang', '16', 'Xí nghiệp Thương mại Mặt đất Nội Bài', '	Ms Giang', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Xí  nghiệp Thương mại mặt đất Nội Bài (Noi Bai International Airport Ground  Services – NIAGS) là đơn vị trực thuộc Vietnam Airlines, chuyên cung  cấp các dịch vụ phục vụ thương mại mặt đất theo tiêu chuẩn quốc tế cho  các chuyến bay đi và đến Sân bay Quốc tế Nội Bài.</span></p>', 'Sân bay Quốc tế Nội Bài – Sóc Sơn – Hà Nội', 1, '	0973 880 806', '', '	0973 880 806', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(148, 269, 'Mrs.', '	Thu Trang', '-thu-trang', '16', '	SITC-DINH VU Logistics Company Limited', '	Thu Trang', '<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td valign="top"><br /></td>\r\n<td>SITC-DINH VU Logistics Company  Limited (Công ty TNHH Tiếp vận SITC - Đình Vũ)là công ty Liên doanh giữa  Việt Nam và Trung Quốc, hoạt động trong lĩnh vực kinh doanh siêu thị  thực phẩm,dịch vụ giao nhận, đại lý tàu, giao thông vận tải đa phương  thức, bãi Container và kho, Container hàng hóa đóng gói, khai thác hàng  CFS, khai thác bãi và lưu trữ hàng hóa, giao nhận vận tải bằng đường  biển, đường bộ, đường sắt và đường hàng không, dịch vụ hải quan... <br />Công ty TNHH Tiếp vận SITC - Đình Vũ là công ty mới thành lập, có  diện tích kho bãi Container là 30.000 m2. Công ty áp dụng hệ thống quản  lý kho bãi Container hiện đại được tiếp thu kinh nghiệm và thành tựu từ  Trung Quốc.</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'Công ty TNHH SITC- ĐÌNH VŨ, Phương Đông Hải II, qu', 4, '031 .326.0000/0974.9', '', '031 .326.0000/0974.9', '', 0, '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, '0000-00-00', '', '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(149, 270, 'Mr.', 'thanhtd3', 'thanhtd3', '14', 'thanhtd3', 'thanhtd3', '<p>thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3thanhtd3</p>', 'thanhtd3', 1, '', '', '', '', 0, 'p', '', '', 'goicv', 28, 60, 3, '0000-00-00', '0000-00-00', '', '', '', '', '', 1, '2011-02-25', '', '', '', 1, 1, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_profiles_1`
--

CREATE TABLE IF NOT EXISTS `jos_properties_profiles_1` (
  `id` int(6) NOT NULL auto_increment,
  `salutation` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL default '',
  `alias` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL default '',
  `country` int(3) NOT NULL,
  `con_name` varchar(255) NOT NULL,
  `address1` varchar(50) NOT NULL default '',
  `email` varchar(50) NOT NULL default '',
  `phone` varchar(20) NOT NULL default '',
  `fax` varchar(20) NOT NULL default '',
  `mobile` varchar(20) NOT NULL default '',
  `image` varchar(70) NOT NULL default '',
  `published` tinyint(1) NOT NULL default '0',
  `credit` int(11) NOT NULL,
  `type_credit` varchar(5) NOT NULL,
  `startdate` date NOT NULL,
  `enddate` date NOT NULL,
  `show_name` varchar(5) NOT NULL,
  `show_phone` varchar(5) NOT NULL,
  `show_email` varchar(5) NOT NULL,
  `show_addr` varchar(5) NOT NULL,
  `ordering` tinyint(4) NOT NULL,
  `mid` smallint(6) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jos_properties_profiles_1`
--

INSERT INTO `jos_properties_profiles_1` (`id`, `salutation`, `name`, `alias`, `pass`, `company`, `country`, `con_name`, `address1`, `email`, `phone`, `fax`, `mobile`, `image`, `published`, `credit`, `type_credit`, `startdate`, `enddate`, `show_name`, `show_phone`, `show_email`, `show_addr`, `ordering`, `mid`) VALUES
(1, '', 'ffrreee', '', '', 'sdfasf', 0, 'adsf', 'dfdfdf', 'thanhtdfd1986@gmail.com', '2323232', '32323', '232323232', '1_p.jpg', 1, 0, '', '0000-00-00', '0000-00-00', '', '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_rates`
--

CREATE TABLE IF NOT EXISTS `jos_properties_rates` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `description` text,
  `week` int(11) NOT NULL,
  `validfrom` date default NULL,
  `validto` date default NULL,
  `rateperday` double default '0',
  `rateperweek` double NOT NULL,
  `mindays` int(11) default NULL,
  `maxdays` int(11) default NULL,
  `minpeople` int(11) default NULL,
  `maxpeople` int(11) default NULL,
  `typeid` varchar(10) default NULL,
  `weekonly` tinyint(2) NOT NULL default '0',
  `validfrom_ts` date default NULL,
  `validto_ts` date default NULL,
  `dayofweek` int(1) NOT NULL default '7',
  `productid` int(11) default NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(6) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `jos_properties_rates`
--

INSERT INTO `jos_properties_rates` (`id`, `title`, `description`, `week`, `validfrom`, `validto`, `rateperday`, `rateperweek`, `mindays`, `maxdays`, `minpeople`, `maxpeople`, `typeid`, `weekonly`, `validfrom_ts`, `validto_ts`, `dayofweek`, `productid`, `published`, `ordering`) VALUES
(1, '24/04 - 01/05', NULL, 0, '2010-04-24', '2010-04-30', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 1),
(2, '01/05 - 08/05', NULL, 0, '2010-05-01', '2010-05-07', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 2),
(3, '08/05 - 15/05', NULL, 0, '2010-05-08', '2010-05-14', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 3),
(4, '15/05 - 22/05', NULL, 0, '2010-05-15', '2010-05-21', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 4),
(5, '22/05 - 29/05', NULL, 0, '2010-05-22', '2010-05-28', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 5),
(6, '29/05 - 05/06', NULL, 0, '2010-05-29', '2010-06-04', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 6),
(7, '05/06 - 12/06', NULL, 0, '2010-06-05', '2010-06-11', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 7),
(8, '12/06 - 19/06', NULL, 0, '2010-06-12', '2010-06-18', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 8),
(9, '19/06 - 26/06', NULL, 0, '2010-06-19', '2010-06-25', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 9),
(10, '26/06 - 03/07', NULL, 0, '2010-06-26', '2010-07-02', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 10),
(11, '03/07 - 10/07', NULL, 0, '2010-07-03', '2010-07-09', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 11),
(12, '10/07 - 17/07', NULL, 0, '2010-07-10', '2010-07-16', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 12),
(13, '17/07 - 24/07', NULL, 0, '2010-07-17', '2010-07-23', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 13),
(14, '24/07 - 31/07', NULL, 0, '2010-07-24', '2010-07-30', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 14),
(15, '31/07 - 07/08', NULL, 0, '2010-07-31', '2010-08-06', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 15),
(16, '07/08 - 14/08', NULL, 0, '2010-08-07', '2010-08-13', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 16),
(17, '14/08 - 21/08', NULL, 0, '2010-08-14', '2010-08-20', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 17),
(18, '21/08 - 28/08', NULL, 0, '2010-08-21', '2010-08-27', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 18),
(19, '28/08 - 04/09', NULL, 0, '2010-08-28', '2010-09-03', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 19),
(20, '04/09 - 11/09', NULL, 0, '2010-09-04', '2010-09-10', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 20),
(21, '11/09 - 18/09', NULL, 0, '2010-09-11', '2010-09-17', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 21),
(22, '18/09 - 25/09', NULL, 0, '2010-09-18', '2010-09-24', 0, 111, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7, 0, 1, 22);

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_rating`
--

CREATE TABLE IF NOT EXISTS `jos_properties_rating` (
  `product_id` int(11) NOT NULL,
  `rating_sum` int(11) NOT NULL,
  `rating_count` int(11) NOT NULL,
  `lastip` varchar(50) NOT NULL,
  PRIMARY KEY  (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_properties_rating`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_rating_user`
--

CREATE TABLE IF NOT EXISTS `jos_properties_rating_user` (
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` tinyint(1) NOT NULL,
  `lastip` varchar(50) NOT NULL,
  PRIMARY KEY  (`product_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_properties_rating_user`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_salary`
--

CREATE TABLE IF NOT EXISTS `jos_properties_salary` (
  `id` int(3) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  `checked_out` tinyint(1) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `jos_properties_salary`
--

INSERT INTO `jos_properties_salary` (`id`, `name`, `alias`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(14, 'Trả lương theo giờ', 'tr-lng-theo-gi', 1, 0, 0, '0000-00-00 00:00:00'),
(15, 'Trả lương theo tuần', 'tr-lng-theo-tun', 1, 0, 0, '0000-00-00 00:00:00'),
(16, 'Trả lương theo tháng', 'tr-lng-theo-thang', 1, 0, 0, '0000-00-00 00:00:00'),
(17, 'Trả lương theo năm', 'per-year', 1, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_savecv`
--

CREATE TABLE IF NOT EXISTS `jos_properties_savecv` (
  `id` int(11) NOT NULL auto_increment,
  `id_employ` int(11) NOT NULL,
  `idgoi_cv` int(11) NOT NULL,
  `goicv` int(4) NOT NULL,
  `id_seecker` int(11) NOT NULL,
  `id_cv` int(11) NOT NULL,
  `checkcv` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Dumping data for table `jos_properties_savecv`
--

INSERT INTO `jos_properties_savecv` (`id`, `id_employ`, `idgoi_cv`, `goicv`, `id_seecker`, `id_cv`, `checkcv`) VALUES
(20, 112, 17, 40, 94, 18, 1),
(19, 112, 17, 40, 110, 20, 1),
(18, 112, 17, 40, 110, 24, 1),
(17, 0, 0, 0, 0, 0, 1),
(16, 112, 16, 90, 110, 19, 1),
(15, 112, 16, 90, 84, 23, 1),
(21, 111, 19, 40, 138, 26, 1),
(22, 216, 22, 40, 214, 32, 1),
(23, 216, 22, 40, 205, 29, 1),
(24, 106, 23, 40, 221, 35, 1),
(25, 106, 23, 40, 90, 34, 1),
(26, 270, 28, 60, 235, 40, 1),
(27, 270, 28, 60, 227, 37, 1),
(28, 270, 28, 60, 221, 36, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_savejob`
--

CREATE TABLE IF NOT EXISTS `jos_properties_savejob` (
  `id` tinyint(9) NOT NULL auto_increment,
  `mid` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_employ` int(11) NOT NULL,
  `id_cv` int(4) NOT NULL,
  `save` int(11) NOT NULL,
  `apply` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=114 ;

--
-- Dumping data for table `jos_properties_savejob`
--

INSERT INTO `jos_properties_savejob` (`id`, `mid`, `id_product`, `id_employ`, `id_cv`, `save`, `apply`) VALUES
(86, 138, 71, 111, 26, 1, 1),
(85, 110, 78, 112, 19, 1, 1),
(93, 126, 156, 215, 0, 1, 1),
(88, 90, 150, 189, 34, 1, 1),
(89, 90, 55, 133, 34, 1, 1),
(90, 90, 100, 124, 33, 1, 1),
(98, 227, 157, 120, 23, 1, 1),
(94, 221, 67, 106, 35, 1, 1),
(95, 221, 104, 140, 35, 1, 1),
(97, 223, 148, 186, 0, 1, 1),
(99, 235, 72, 111, 0, 1, 1),
(100, 235, 118, 154, 40, 1, 1),
(101, 239, 104, 140, 0, 1, 1),
(102, 239, 106, 142, 0, 1, 1),
(103, 239, 89, 120, 0, 1, 1),
(113, 273, 152, 207, 0, 1, 0),
(112, 273, 153, 207, 0, 1, 0),
(109, 273, 161, 83, 41, 1, 1),
(110, 273, 159, 228, 41, 1, 1),
(111, 273, 160, 231, 41, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_seekekjop`
--

CREATE TABLE IF NOT EXISTS `jos_properties_seekekjop` (
  `id` int(9) NOT NULL auto_increment,
  `mid` int(9) NOT NULL,
  `name_cv` varchar(255) collate utf8_unicode_ci NOT NULL,
  `pid` int(11) NOT NULL,
  `jid` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `nganhhoc` varchar(255) collate utf8_unicode_ci NOT NULL,
  `hocluc` varchar(255) collate utf8_unicode_ci NOT NULL,
  `ngoaingu` int(11) NOT NULL,
  `tinhoc` int(11) NOT NULL,
  `bangcapkhac` text collate utf8_unicode_ci NOT NULL,
  `cid` int(4) NOT NULL,
  `type` int(4) NOT NULL,
  `vitri` varchar(255) collate utf8_unicode_ci NOT NULL,
  `capbac` varchar(255) collate utf8_unicode_ci NOT NULL,
  `loaicv` varchar(255) collate utf8_unicode_ci NOT NULL,
  `nganhnghe` varchar(255) collate utf8_unicode_ci NOT NULL,
  `cyid` int(11) NOT NULL,
  `sid` varchar(11) collate utf8_unicode_ci NOT NULL,
  `experience_year` int(6) NOT NULL,
  `culture` varchar(255) collate utf8_unicode_ci NOT NULL,
  `muctieu` varchar(255) collate utf8_unicode_ci NOT NULL,
  `salary` varchar(255) collate utf8_unicode_ci NOT NULL,
  `cv` varchar(255) collate utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `name_file` varchar(255) collate utf8_unicode_ci NOT NULL,
  `published` int(3) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=43 ;

--
-- Dumping data for table `jos_properties_seekekjop`
--

INSERT INTO `jos_properties_seekekjop` (`id`, `mid`, `name_cv`, `pid`, `jid`, `eid`, `nganhhoc`, `hocluc`, `ngoaingu`, `tinhoc`, `bangcapkhac`, `cid`, `type`, `vitri`, `capbac`, `loaicv`, `nganhnghe`, `cyid`, `sid`, `experience_year`, `culture`, `muctieu`, `salary`, `cv`, `start_date`, `name_file`, `published`) VALUES
(8, 0, '', 0, 0, 0, '', '', 0, 0, '', 0, 0, 'fffffffffffff', '4', '', '', 4, '4', 4, '4', '', '4', '<p>gffffffffffff</p>', '0000-00-00', '', 1),
(23, 84, '', 15, 11, 13, '', 'k', 3, 3, 'Bằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khác', 0, 12, 'lap trinh php nang cao', '', '', '', 1, '1,2,3,', 5, '', '', '5 triệu - 10 triệu', '<p>Mô tả kinh nghiệm Mô tả kinh nghiệm Mô tả kinh nghiệm Mô tả kinh nghiệm Mô tả kinh nghiệm Mô tả kinh nghiệm Mô tả kinh nghiệm Mô tả kinh nghiệm Mô tả kinh nghiệm Mô tả kinh nghiệm</p>\r\n<p>Mô tả kinh nghiệm Mô tả kinh nghiệm Mô tả kinh nghiệm Mô tả kinh', '2010-11-26', '23_cv.doc', 1),
(11, 93, '', 0, 0, 0, '', '', 0, 0, '', 0, 0, 'Bán hàng hoặc Thu ngân', 'Nhân viên', '', '', 1, '1', 2, 'Cao đẳng', '', '3.000.000', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Đã từng có kinh nghiệm bán hàng, thu ngân, làm kế toán ở các công ty.</span></p>', '0000-00-00', '', 1),
(12, 93, '', 0, 0, 0, '', '', 0, 0, '', 0, 0, 'Nhân viên kinh doanh thiết bị điện', 'trưởng phòng', '', '', 1, '1', 4, 'Đại học', '', '5.000.000', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Làm  nhân viên kỹ thuật tại công ty đan hạ.làm công nhân điện tại công ty  kim hải lôc.làm nhân viên kinh doanh tại công ty ý nhiên chuyên về kinh  doanh thiết bị điện</span></p>', '0000-00-00', '', 1),
(14, 79, '', 0, 0, 0, '', '', 0, 0, '', 0, 0, 'ãgg', 'zxgzxgzxg', '', '', 4, '0', 0, 'zgzxzxgz', '', 'zxgzzgxgz', '', '2010-11-08', '', 1),
(15, 89, '', 14, 11, 12, 'kinh doanh', 'kha', 2, 2, 'chung nhan giao tiep ', 20, 24, 'rgtu', '', '', '', 1, '1', 3, '', '', 'Thỏa thuận', '<p>sfgrg dfj frg grhytr</p>', '2010-11-09', '', 1),
(16, 94, '', 14, 11, 13, 'Ngoại ngữ', 'khá', 1, 2, 'kỹ năng giao tiếp', 44, 31, 'Bán hàng, dịch vụ, văn phòng, trợ lý', '', '', '', 1, '1', 2, '', '', '1 triệu - 3 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">Hình thức cao ráo, ưa nhìn. Hai năm làm văn phòng.</span></p>', '2010-11-09', '', 1),
(17, 94, '', 14, 11, 13, 'Kinh doanh', 'khá', 2, 2, '', 44, 30, 'Bán hàng, dịch vụ, văn phòng, trợ lý', '', '', '', 1, '1', 2, '', '', '1 triệu - 3 triệu', '<p>frtydlyu7 grytu7</p>', '2010-11-09', '', 1),
(18, 94, '', 15, 11, 13, 'quản trị kinh doanh', 'TB', 0, 0, '', 52, 41, 'Nhân viên bán hàng', '', '', '', 1, '1', 3, '', '', '3 triệu - 5 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">6 tháng bán hàng thiết bị vệ sinh qua mạng.6 tháng giao hàng trong nội thành bằng xe máy.</span></p>', '2010-11-09', '', 1),
(19, 110, '', 14, 11, 13, 'Công Nghệ Thông Tin', 'khá', 2, 3, '', 0, 16, 'Nhân viên IT ', '', '', '', 1, '1', 3, '', '', 'Thỏa thuận', '<p><span style="float: left; width: 544px; background-color: #ffffff;">- Thành thạo tin hoc VP. <br />- Có kinh nghiệm trong việc quản lý sổ sách,hồ sơ,các con dấu và các văn bản. <br />- Kế toán nội bộ và bán hàng, công nợ và thủ quỹ. <br />- Biết báo cá', '2010-11-22', '19_cv.doc', 1),
(20, 110, '', 14, 11, 13, 'Công nghệ thông tin', 'TB', 2, 3, '', 0, 32, 'Tester,Nhân viên kinh doanh phần mềm', '', '', '', 1, '1', 1, '', '', '3 triệu - 5 triệu', '<p><span style="float: left; width: 544px; background-color: #ffffff;">_Tốt nghiệp ngành Công nghệ thông tin trường Infoworld chuyên ngành phần mềm.. <br />_Tham gia nhiều khóa đào tạo Tester chuyên nghiệp tại trường và tại công ty Logigear. <br />_Đã có<', '2010-11-22', '20_cv.doc', 1),
(22, 84, '', 14, 11, 13, 'cntt', 'k', 1, 2, 'Bằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khác', 0, 20, 'lap trinh php basic', '', '', '', 1, '13,1,', 2, '', '', '3 triệu - 5 triệu', '<p>Mô tả kinh nghiệm  Mô tả kinh nghiệm  Mô tả kinh nghiệm  Mô tả kinh nghiệm  Mô tả kinh nghiệm  Mô tả kinh nghiệm  Mô tả kinh nghiệm  Mô tả kinh nghiệm  Mô tả kinh nghiệm  Mô tả kinh nghiệm  Mô tả kinh nghiệm</p>\r\n<p>Mô tả kinh nghiệm  Mô tả kinh nghiệm', '2010-11-26', '22_cv.doc', 1),
(24, 110, '', 16, 11, 14, 'cntt', 'k', 1, 1, '', 0, 41, 'giam doc dieu hanh', '', '', '', 1, '2', 10, '', '', 'Hơn 10 triệu', '<p>Mô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệm</p>', '2010-11-26', '24_cv.doc', 1),
(25, 138, '', 14, 11, 12, 'tiếng anh', 'khá', 2, 2, '', 0, 50, 'Nhân viên biên phiên dịch', '', '', '', 1, '4', 3, '', '', '3 triệu - 5 triệu', '<p>- Từ 12/2009 - 5/2010 làm việc tại SinhNam metal Company. Vị trí Co- ordinator.<br />- Từ 6/2010 - 9/2010 làm tại KUMON Việt Nam. Vị trí Center Assistant.</p>', '2010-11-30', '25_cv.doc', 1),
(26, 138, '', 14, 11, 12, 'Tiếng anh', 'khá', 3, 3, '', 0, 50, 'Nhân viên văn phòng', '', '', '', 1, '1', 3, '', '', 'Thỏa thuận', '<p>- Mong muốn được làm trong môi trường tiếng anh chuyên nghiệp.<br />- Sử dụng được tiếng anh nhuần nhuyễn vào công việc.<br />- Có thêm kinh nghiệm thông qua quá trình làm việc.<br />- Có cơ hội thăng tiến trong nghề nghiệp.<br />- Có mức thu nhập ổn đ', '2010-11-30', '26_cv.doc', 1),
(27, 203, '', 18, 11, 13, 'Quản trị kinh doanh', 'tb', 2, 2, '', 0, 11, 'Nhân viên kinh doanh, nhân vien marketing ', '', '', '', 1, '1', 2, '', '', 'Thỏa thuận', '<p><span>\r\n<p><strong><em>hân viên bán hàng</em></strong> (T6 - T9/2010): công ty TNHH Vật liệu Hoàng Gia</p>\r\n<ul>\r\n<li>1 Giới thiệu và quảng cáo sản phẩm của công ty</li>\r\n</ul>\r\n<p>2  Chăm sóc hệ thống đại lý sẵn có và mở rộng hệ thống phân phối mới</p', '2010-12-03', '', 1),
(28, 204, '', 14, 11, 13, 'Kinh tế du lịch', 'Khá', 2, 2, '', 0, 13, '  Trưởng phòng kinh doanh ', '', '', '', 1, '1', 2, '', '', '3 triệu - 5 triệu', '<p>07/2007 – 03/2008:<br />Nhân viên hợp tác (làm bán thời gian) tại Công ty TNHH ACECOOK Việt Nam.<br /><br />- Tiếp thị sản phẩm và tìm kiếm đại lý.       <br /><br />- Nghiên cứu thị trường, lập báo cáo.<br /><br /><br />04/2008 – 09/2008:<br /><br />N', '2010-12-03', '', 1),
(29, 205, '', 16, 11, 12, 'tư vấn', 'TB', 3, 3, '', 0, 13, '  Nhân viên chăm sóc khách hàng ', '', '', '', 1, '1', 2, '', '', '1 triệu - 3 triệu', '<p>Nhân viên trung tâm giải pháp ngữ âm và văn hóa giao tiếp PSC<br /><br /> * Tiếp cận với tiếng anh<br /> * Có cơ hội tìm hiểu về văn hóa các nước cũng như các kĩ năng giao tiếp cơ bản<br /> * Làm việc trong môi trường trẻ trung năng động<br /> * Rèn lu', '2010-12-03', '', 1),
(30, 206, '', 18, 11, 13, 'Cữ Nhân Tin học', 'Khá', 3, 1, '', 0, 16, 'Lập trình viên (C#, . NET)', '', '', '', 1, '12', 0, '', '', 'Thỏa thuận', '<p>3/2010 – 7/2010 : Thực tập tại DNTN dịch vụ xây dựng Hòa Phương để hoàn thành tốt đồ án tốt nghiệp với phần mềm “Cung cấp vật liệu xây dựng công trình” với điểm đạt được là 9 điểm.</p>', '2010-12-03', '', 1),
(31, 213, '', 18, 11, 13, 'Đại học Kinh tế quốc dân', 'Khá', 3, 3, '', 0, 11, 'Nhân viên kinh doanh', '', '', '', 1, '1', 2, '', '', '3 triệu - 5 triệu', '<p>03/2010 - 05/2010:  Nhân viên nghiên cứu thị trường, Tập đoàn viễn thông quân đội Viettel, chi nhánh tỉnh Điện Biên<br /><br />- Thu thập thông tin đối thủ cạnh tranh, báo cáo hàng tuần, hàng tháng về thị phần và biến động doanh thu, báo cáo hoạt động ', '2010-12-07', '', 1),
(32, 214, '', 18, 11, 13, 'Quản trị kinh doanh', 'Khá', 3, 3, '', 0, 58, 'Nhân viên kinh doanh', '', '', '', 1, '4', 2, '', '', 'Thỏa thuận', '<p>Nhân Viên <br /> - Công Ty TNHH Young In Vina - Sản phẩm công nghiệp <br /> - Trưởng nhóm/Giám sát - tháng 10 năm 2008 đến tháng 08 năm 2009<br /> - Hà Nội - Việt Nam<br /> Thông tin liên quan:<br /> - Triển khai thành lập các đại lý cấp I tại khu vực ', '2010-12-07', '', 1),
(33, 90, '', 17, 13, 14, 'cntt', 'k', 1, 1, 'Bằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khác', 0, 12, 'job thu hang 1', '', '', '', 1, '2', 3, '', '', '3 triệu - 5 triệu', '<p>Bằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khácBằng cấp khác</p>', '2010-12-07', '', 1),
(34, 90, '', 13, 12, 14, 'cntt', 'k', 0, 0, '', 0, 40, 'job thu hang 2', '', '', '', 1, '3', 5, '', '', '5 triệu - 10 triệu', 'Mô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệmMô tả kinh nghiệm', '2010-12-07', '34_cv.doc', 1),
(35, 221, '', 18, 13, 14, 'dgfdg', 'sfgd', 2, 1, 'svfdgf', 0, 59, 'fdfdgfd', '', '', '', 1, '1', 1, '', '', '1 triệu - 3 triệu', '<p>sfdfgfh</p>', '2010-12-16', '', 1),
(36, 221, '', 18, 11, 12, 'Quản trị kinh doanh', 'Khá', 2, 1, 'dsfdf', 0, 13, 'Nhân viên kinh doanh', '', '', '', 1, '1', 2, '', '', 'Thỏa thuận', '<p>fđg</p>', '2010-12-22', '36_cv.doc', 1),
(37, 227, '', 18, 11, 16, '', '', 0, 0, '', 0, 15, 'Quản lý', '', '', '', 1, '1', 1, '', '', 'Thỏa thuận', '', '2010-12-28', '', 1),
(39, 239, '', 18, 11, 13, 'công nghệ thông tin', 'khá ', 2, 3, '', 0, 45, 'Thiết ké website', '', '', '', 1, '2', 3, '', '', '5 triệu - 10 triệu', '<p>có 2 năm kinh nghiệm làm viẹc ở vị trí thiết ké website</p>', '2011-01-18', '', 1),
(40, 235, '', 15, 11, 13, 'Quản trị kinh doanh', 'Khá', 1, 2, '', 0, 26, 'Nhân viên kinh doanh', '', '', '', 1, '14', 3, '', '', '1 triệu - 3 triệu', '<p>ádfđsfd</p>', '2011-01-18', '', 1),
(41, 273, '', 14, 13, 13, 'dsa', 'dsa', 0, 0, 'da dsa', 0, 40, 'theit ke', '', '', '', 1, '1,3,4,', 23, '', '', '3 triệu - 5 triệu', '<p>dsa d sa das</p>', '2011-02-19', '41_cv.doc', 1),
(42, 273, '', 14, 13, 0, '', '', 0, 0, '', 0, 40, 'dsa dsadsasa', '', '', '', 1, '1,3,4,', 0, '', '', 'Thỏa thuận', '<p>dasda</p>', '2011-02-19', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_seeker`
--

CREATE TABLE IF NOT EXISTS `jos_properties_seeker` (
  `id` int(6) NOT NULL auto_increment,
  `mid` int(6) NOT NULL default '0',
  `salsek` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL default '',
  `alias` varchar(50) NOT NULL,
  `company` varchar(255) NOT NULL default '',
  `type` int(1) NOT NULL default '0',
  `address` varchar(50) NOT NULL,
  `location` varchar(255) NOT NULL,
  `m_salary` float NOT NULL,
  `salary` int(3) NOT NULL,
  `pcode` varchar(10) NOT NULL default '',
  `state` varchar(50) NOT NULL default '',
  `cyid` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL default '',
  `phone` varchar(20) NOT NULL default '',
  `birth_day` varchar(20) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `youknow` varchar(255) NOT NULL,
  `c_post` varchar(255) NOT NULL,
  `eid` smallint(6) NOT NULL,
  `cid` smallint(6) NOT NULL,
  `jid` smallint(6) NOT NULL,
  `i_curren` varchar(50) NOT NULL,
  `image` varchar(70) NOT NULL default '',
  `published` tinyint(1) NOT NULL default '0',
  `ordering` int(3) NOT NULL default '0',
  `checked_out` tinyint(1) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `mid` (`mid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `jos_properties_seeker`
--

INSERT INTO `jos_properties_seeker` (`id`, `mid`, `salsek`, `name`, `alias`, `company`, `type`, `address`, `location`, `m_salary`, `salary`, `pcode`, `state`, `cyid`, `email`, `phone`, `birth_day`, `gender`, `youknow`, `c_post`, `eid`, `cid`, `jid`, `i_curren`, `image`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(1, 64, 'Mrs.', 'thanhtd', 'thanhtd', '', 5, 'Ngo 4k5 ao sen', 'Bac Ninh', 120, 15, '', '', '7', '', '0987898723', '2010-10-21', '1', '15', 'Lap trinh vien', 12, 4, 12, '200$', '1_p.jpg', 1, 1, 0, '0000-00-00 00:00:00'),
(3, 76, '1', 'khuong', '', '', 0, 'Hà Đông Hà Nội', '', 0, 0, '', '', '', 'emyeu@yahoo.com', '0976480323', '0000-00-00', '0', '0', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(4, 77, 'Mr', 'khuong', '', '', 0, 'ha noi', '', 0, 0, '', '', '', 'koyeu@yahoo.com', '0976480323', '0000-00-00', '0', '0', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(5, 78, 'Mr', 'khuong', '', '', 0, 'ágagsagas', '', 0, 0, '', '', '', 'toiyeuem@yahoo.com', '0976480323', '0000-00-00', '', 'Quảng cáo trên truyền hình', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(6, 79, 'Mr', 'phuong123', '', '', 0, 'Van dien - ha Noi', '', 0, 0, '', '', '', 'khuong@yahoo.com', '0983862767', '1985-08-19', '', '', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(7, 80, 'Mr', 'thanhtd1`', '', '', 0, '', 'Viet nam', 0, 0, '', '', '', 'alo@gmail.com', '', '0000-00-00', 'Nam', '', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(8, 81, 'Mr', 'khuong', '', '', 0, '', 'Viet nam', 0, 0, '', '', '', 'ppppppp@gmail.com', '', '0000-00-00', 'Nam', '', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(9, 84, 'Mr.', 'thanhtd2', 'thanhtd2', '', 0, 'ewewewew', '', 0, 0, '', '', '1', 'thanhtd2@gmail.com', '34232323', '2-2-1941', '0', '15', '', 0, 0, 11, '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(10, 86, 'Mrs', 'Hằng', 'hng', '', 0, 'phòng 314 nhà G4 KĐT Trung Yên - Trung Hòa - cầu G', 'Viet nam', 0, 0, '', '', '', 'hangnt@creativevietnam.vn', '0915436574', '0000-00-00', 'Nữ', 'Website khác', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(11, 87, 'Mrs', 'admin321', 'admin321', '', 0, 'admin321admin321', 'Lao', 0, 0, '', '', '', 'admin321@gmail.com', '121212121', '2010-11-10', 'Nữ', 'Website khác', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(12, 89, 'Mr', 'Lương Đức Thủy', 'mrthanhtd2', '', 0, 'Cầu Giấy', 'Viet nam', 0, 0, '', '', '', 'mrthanhtd2@gmail.com', '32323232', '1987-07-15', 'Nữ', 'Bạn bè', '', 0, 0, 0, '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(13, 90, 'Mrs', 'thuhang1', 'thuhang1', '', 0, 'trung yen', 'Viet nam', 0, 0, '', '', '4', 'thuhang1@gmail.com', '23232323', '2-11-1940', 'Nu', '14', '', 0, 0, 0, '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(14, 93, 'Mrs', 'Hằng', 'hng', '', 0, 'Hà Nội', 'Viet nam', 0, 0, '', '', '', 'hangnt03@gmail.com', '0915436574', '0000-00-00', 'Nữ', 'Bạn bè', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(15, 94, 'Miss', 'Lan Anh', 'lan-anh', '', 0, 'Cầu Giấy- Hà Nội', 'Viet nam', 0, 0, '', '', '', 'lananh@yahoo.com', '012 4243 5997', '1984-07-19', 'Nữ', 'Bạn bè', '', 0, 0, 0, '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(16, 100, 'Mr', 'thanh_http', 'thanhhttp', '', 0, 'trung yen', 'Viet nam', 0, 0, '', '', '', 'thanh_http@yahoo.com', '23232323', '2010-11-24', 'Nam', 'Quảng cáo trên truyền hình', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(17, 108, 'Mr', 'Nguyen Anh Minh', 'nguyen-anh-minh', '', 0, 'Tan Hung - Soc Son - Ha Noi', 'Viet nam', 0, 0, '', '', '', 'minhsshn@gmail.com', '0928868688', '1983-10-17', 'Nam', 'Website khác', '', 0, 0, 0, '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(18, 110, 'Mr.', 'thuhang4', 'thuhang4', '', 0, 'Trung Hòa - HN', 'Viet nam', 0, 0, '', '', '4', 'thuongtrangcong@gmail.com', '0904269050', '1983-11-16', '0', '14', '', 0, 0, 11, '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(19, 138, 'Miss', 'Lê Thị Hồng', 'le-th-hng', '', 0, 'Trung Hòa - HN', 'Viet nam', 0, 0, '', '', '', 'hangchipchop@yahoo.co.uk', '012 4243 5997', '1983-12-15', 'Nữ', 'Bạn bè', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(20, 183, 'Mr', 'Tùng', 'tung', '', 0, 'Sóc Sơn,Hà Nội', 'Viet nam', 0, 0, '', '', '', 'thanhtungsshn@gmail.com', '0988629953', '1990-10-27', 'Nam', 'Bạn bè', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(21, 190, 'Miss', 'Nguyễn Thu Hà', 'nguyn-thu-ha', '', 0, 'B-203, chung cư M3-M4 Nguyễn Chí Thanh- Hà Nội', 'Viet nam', 0, 0, '', '', '', 'hangnt03@gmail.com', '0989087000 ', '1987-08-21', 'Nữ', 'Website khác', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(22, 191, 'Mr', 'thanh_http', 'thanhhttp', '', 0, 'vn', 'Viet nam', 0, 0, '', '', '', 'thanh_http@yahoo.com', '123456', '2010-12-23', 'Nam', 'Bạn bè', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(23, 192, 'Mr', 'thaison01', 'thaison01', '', 0, 'vn', 'Viet nam', 0, 0, '', '', '', 'thaison01@gmail.com', '123456', '2010-12-31', 'Nam', 'Bạn bè', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(24, 194, 'Mr', 'sonnt1', 'sonnt1', '', 0, 'Số 10A, Tổ 6, Phường Trung Hoà, Cầu Giấy, Hà Nội', 'Viet nam', 0, 0, '', '', '', 'sonnt@creativevietnam.vn', '087637090987', '2010-12-23', 'Nam', 'Bạn bè', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(25, 195, 'Mr', 'hangnt', 'hangnt', '', 0, 'vn', 'Viet nam', 0, 0, '', '', '', 'hangnt@creativevietnam.vn', '123456', '2010-12-28', 'Nam', 'Bạn bè', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(26, 198, 'Mr', 'hangnt03', 'hangnt03', '', 0, 'vn', 'Lao', 0, 0, '', '', '', 'hangnt03@gmail.com', '123456', '2010-12-28', 'Nam', 'Website khác', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(27, 199, 'Mr', 'hangnt03', 'hangnt03', '', 0, 'sfdskf', 'Viet nam', 0, 0, '', '', '', 'hangnt03@gmail.com', '9498843', '2010-12-27', 'Nam', 'Website khác', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(28, 200, 'Mr', 'hangnt03', 'hangnt03', '', 0, 'hsajlkfkkjdsf', 'Viet nam', 0, 0, '', '', '', 'hangnt03@gmail.com', '0982766333', '2010-12-22', 'Nam', 'Bạn bè', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(29, 201, 'Mr', 'thuhang.mnc', 'thuhangmnc', '', 0, 'dsfdgfh', 'Viet nam', 0, 0, '', '', '', 'thuhang.mnc@gmail.com', '0982766333', '2010-12-29', 'Nam', 'Website khác', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(30, 202, 'Mr', 'hoanghoa', 'hoanghoa', '', 0, 'hoanghoa', 'Viet nam', 0, 0, '', '', '', 'hoanghoa@gmail.com', '0982766333', '2010-12-29', 'Nữ', 'Bạn bè', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(31, 203, 'Miss', 'Nguyễn Thị Kim Nhung', 'nguyn-th-kim-nhung', '', 0, 'Cầu Giấy - Hà Nội', 'Viet nam', 0, 0, '', '', '', 'kimnhung@gmail.com', '0982766333', '1987-08-18', 'Nữ', 'Website khác', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(32, 204, 'Miss', 'Nguyễn Thị Thương', 'nguyn-th-thng', '', 0, '189 Đội Cấn - Ba Đình - HN', 'Viet nam', 0, 0, '', '', '', 'nguyenthithuong@gmail.com', '0983650106', '1986-12-30', 'Nữ', 'Website khác', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(33, 205, 'Miss', 'Trần Loan', 'trn-loan', '', 0, 'Xuân Đỉnh -Từ Liêm _ Hà Nội ', 'Viet nam', 0, 0, '', '', '', 'tranloan@gmail.com', '098 434 7389 ', '1988-07-13', 'Nữ', 'Website khác', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(34, 206, 'Miss', 'Phạm Thanh Huyền', 'phm-thanh-huyn', '', 0, 'P. Hiệp Thành, TX. Thủ Dầu Một, Bình Dương', 'Viet nam', 0, 0, '', '', '', 'thanhhuyen@yahoo.com', '01662335323', '1986-12-16', 'Nữ', 'Website khác', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(35, 213, 'Miss', 'Nguyễn Thu Hà', 'nguyn-thu-ha', '', 0, 'Nguyễn Chí Thanh - Hà Nội', 'Viet nam', 0, 0, '', '', '', 'ngha87@gmail.com', '0989087000', '1986-12-17', 'Nữ', 'Bạn bè', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(36, 214, 'Mr', 'Lê Dưỡng', 'le-dng', '', 0, 'Trung Hòa- Cầu Giấy - Hà Nội', 'Viet nam', 0, 0, '', '', '', 'leduong@yahoo.com', '0982766333 ', '1983-08-23', 'Nam', 'Website khác', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(37, 220, 'Mr', 'aasss', 'aasss', '', 0, '360 Kim Ma Str., Ba Dinh District, Hanoi, Vietnam', '', 0, 0, '', '', '4', 'thanhtd1986@gmail.com1', '123654789', '1-2-1941', 'Nam', '16', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(38, 221, 'Mrs', 'hanchip', 'hanchip', '', 0, 'ffgkhjlk', '', 0, 0, '', '', '1', 'hanchip@yahoo.com', '08.38107610', '4-6-1947', 'Nu', '14', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(39, 226, 'Mr.', 'thanhtd211986', 'thanhtd211986', '', 0, 'n456', '', 0, 0, '', '', '1', 'thanhtd211986@gmail.com', '2323232323', '2-2-1942', '0', '15', '', 0, 0, 11, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(40, 227, 'Mr', 'Vu Van Nam', 'vu-van-nam', '', 0, 'Soc son', '', 0, 0, '', '', '1', 'nam@nhanlucvietbac.com.vn', '0985773169', '1-1-1940', 'Nam', '16', '', 0, 0, 0, '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(41, 234, 'Mr', 'trần trọng tân', 'trn-trng-tan', '', 0, 'ha noi', '', 0, 0, '', '', '1', 'trantrongtan1983@yahoo.com', '0916566616', '3-8-1983', 'Nam', '18', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(42, 235, 'Mr', 'hangnt04', 'hangnt04', '', 0, 'Hà Nội', '', 0, 0, '', '', '1', 'hangnt03@gmail.com', '091434354545', '17-7-1982', 'Nu', '15', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(43, 236, 'Mr', 'Trung', 'trung', '', 0, 'ert yerty ', '', 0, 0, '', '', '1', 'longltaptech@yahoo.com', '4564564565', '18-2-1975', 'Nam', '16', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(44, 239, 'Miss', 'thanh nhàn', 'thanh-nhan', '', 0, 'quận 9', '', 0, 0, '', '', '1', 'nhanthanh00@gmail.com', '0933768755', '9-5-1987', 'Nu', '16', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(45, 271, 'Mr', '1111', '1111', '', 0, '11111', '', 0, 0, '', '', '1', 'thanhtd198346@gmail.com', '1111', '1-3-1941', 'Nam', '14', '', 0, 0, 0, '', '', 0, 0, 0, '0000-00-00 00:00:00'),
(46, 272, 'Mr.', 'Son', 'son', '', 0, 'Phòng 314 Tòa nhà G4 Trung Yên Trung Hòa Cầu Giấy', '', 0, 0, '', '', '1', 'sonnt@creativevietnam.vn', '0437834005', '3-3-1942', '0', '14', '', 0, 0, 11, '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(47, 273, 'Mr', 'son test', 'son-test', '', 0, 'Số 297 xóm 3 Thôn Ngọc Đại – Xã Đại Mỗ - Từ Liêm', '', 0, 0, '', '', '1', 'sonnt@creativevietnam.vn', '0948468558', '1-1-1941', 'Nu', '15', '', 0, 0, 0, '', '', 1, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_state`
--

CREATE TABLE IF NOT EXISTS `jos_properties_state` (
  `id` int(3) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `parent` int(5) NOT NULL,
  `mid` int(5) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  `checked_out` tinyint(1) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `jos_properties_state`
--

INSERT INTO `jos_properties_state` (`id`, `name`, `alias`, `parent`, `mid`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(1, 'Hà Nội', 'ha-noi', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(2, 'TP Ho Chi Minh', 'tp-ho-chi-minh', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(3, 'Bắc Kạn', 'bc-kn', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(4, 'Đà Nẵng', 'a-nng', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(5, 'Hải Phòng', 'hi-phong', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(6, 'Thái Nguyên', 'thai-nguyen', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(7, 'Hải Dương', 'hai-duong', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(8, 'Hưng Yên', 'hung-yen', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(9, 'Bắc Giang', 'bc-giang', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(10, 'Bắc Ninh', 'bc-ninh', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(11, 'Bến Tre', 'bn-tre', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(12, 'Bình Dương', 'binh-dng', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(13, 'Bình Phước', 'binh-phc', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(14, 'Bình Thuận', 'binh-thun', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(15, 'Cà Mau', 'ca-mau', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(16, 'Cao Bằng', 'cao-bng', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(17, 'Cần Thơ', 'cn-th', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(18, 'Cửu Long', 'cu-long', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(19, 'Đắc Lắc', 'c-lc', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(20, 'Đắc Nông', 'c-nong', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(21, 'Điện Biên', 'in-bien', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(22, 'Đồng Nai', 'ng-nai', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(23, 'Đồng Tháp', 'ng-thap', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(24, 'Gia Lai', 'gia-lai', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(25, 'Hà Nam', 'ha-nam', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(26, 'Hà Giang', 'ha-giang', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(27, 'Hà Tĩnh', 'ha-tnh', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(28, 'Hậu Giang', 'hu-giang', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(29, 'Hòa Bình', 'hoa-binh', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(30, 'Khánh Hòa', 'khanh-hoa', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(31, 'Kiên Giang', 'kien-giang', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(32, 'Kon Tum', 'kon-tum', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(33, 'Lai Châu', 'lai-chau', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(34, 'Lạng Sơn', 'lng-sn', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(35, 'Lào Cai', 'lao-cai', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(36, 'Lâm Đồng', 'lam-ng', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(37, 'Long An', 'long-an', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(38, 'Nam Định', 'nam-nh', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(39, 'Nghệ An', 'ngh-an', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(40, 'Ninh Bình', 'ninh-binh', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(41, 'Ninh Thuận', 'ninh-thun', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(42, 'Phú Thọ', 'phu-th', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(43, 'Phú Yên', 'phu-yen', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(44, 'Quảng Bình', 'qung-binh', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(45, 'Quảng Nam', 'qung-nam', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(46, 'Quảng Ngãi', 'qung-ngai', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(47, 'Quảng Ninh', 'qung-ninh', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(48, 'Quảng Trị', 'qung-tr', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(49, 'Sóc Trăng', 'soc-trng', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(50, 'Sơn La', 'sn-la', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(51, 'Tây Ninh', 'tay-ninh', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(52, 'Thái Bình', 'thai-binh', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(53, 'Thái Nguyên', 'thai-nguyen', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(54, 'Thanh Hóa', 'thanh-hoa', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(55, 'Tiền Giang', 'tin-giang', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(56, 'Trà Vinh', 'tra-vinh', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(57, 'Tuyên Quang', 'tuyen-quang', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(58, 'Vĩnh Long', 'vnh-long', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(59, 'Vĩnh Phúc', 'vnh-phuc', 1, 0, 1, 0, 0, '0000-00-00 00:00:00'),
(60, 'Yên Bái', 'yen-bai', 1, 0, 1, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_type`
--

CREATE TABLE IF NOT EXISTS `jos_properties_type` (
  `id` int(2) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `alias` varchar(100) default NULL,
  `parent` int(2) default NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(1) NOT NULL,
  `checked_out` tinyint(1) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=71 ;

--
-- Dumping data for table `jos_properties_type`
--

INSERT INTO `jos_properties_type` (`id`, `name`, `alias`, `parent`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(2, 'Kiến trúc/Thiết kế nội thất', 'kin-tructhit-k-ni-tht', 3, 1, 0, 0, '0000-00-00 00:00:00'),
(3, 'Xây dựng ', 'xay-dng-', 3, 1, 0, 0, '0000-00-00 00:00:00'),
(4, 'Bất động sản', 'bt-ng-sn', 3, 1, 0, 0, '0000-00-00 00:00:00'),
(5, 'Viễn Thông', 'vin-thong', 4, 1, 0, 0, '0000-00-00 00:00:00'),
(6, 'Internet/Online Media', 'internetonline-media', 4, 1, 0, 0, '0000-00-00 00:00:00'),
(7, 'Mỹ thuật/Thiết kế', 'm-thutthit-k', 4, 1, 0, 0, '0000-00-00 00:00:00'),
(8, 'Công nghệ cao', 'cong-ngh-cao', 5, 1, 0, 0, '0000-00-00 00:00:00'),
(9, 'Ô tô -Xe Máy', 'o-to', 5, 1, 0, 0, '0000-00-00 00:00:00'),
(10, 'Sản phẩm công nghiệp', 'sn-phm-cong-nghip', 5, 1, 0, 0, '0000-00-00 00:00:00'),
(11, 'Marketing', 'marketing', 9, 1, 0, 0, '0000-00-00 00:00:00'),
(12, 'Bán hàng', 'ban-hang', 9, 1, 0, 0, '0000-00-00 00:00:00'),
(13, 'Dịch vụ khách hàng ', 'dch-v-khach-hang-', 9, 1, 0, 0, '0000-00-00 00:00:00'),
(15, 'Nhân sự', 'nhan-s', 79, 1, 0, 0, '0000-00-00 00:00:00'),
(18, 'Overseas Jobs', 'overseas-jobs', 8, 1, 0, 0, '0000-00-00 00:00:00'),
(19, 'Người nước ngoài/Việt Kiều', 'ngi-nc-ngoaivit-kiu', 8, 1, 0, 0, '0000-00-00 00:00:00'),
(20, 'Cấp quản lý điều hành', 'cp-qun-ly-iu-hanh', 8, 1, 0, 0, '0000-00-00 00:00:00'),
(67, 'Tổ chức sự kiện', 't-chc-s-kin', 47, 1, 0, 0, '0000-00-00 00:00:00'),
(22, 'Quảng cáo/Khuyến mãi', 'qung-caokhuyn-maii-ngoi', 4, 1, 0, 0, '0000-00-00 00:00:00'),
(23, 'Kế toán/Kiểm toán ', 'k-toankim-toan-', 20, 1, 0, 0, '0000-00-00 00:00:00'),
(24, 'Ngân hàng', 'ngan-hang', 20, 1, 0, 0, '0000-00-00 00:00:00'),
(25, 'Tài chính/Đầu tư', 'tai-chinhu-t', 20, 1, 0, 0, '0000-00-00 00:00:00'),
(26, 'Bảo hiểm', 'bo-him', 20, 1, 0, 0, '0000-00-00 00:00:00'),
(27, 'Chứng khoán', 'chng-khoan', 20, 1, 0, 0, '0000-00-00 00:00:00'),
(29, 'Hàng gia dụng/Chăm sóc cá nhân', 'hang-gia-dngchm-soc-ca-nhan', 36, 1, 0, 0, '0000-00-00 00:00:00'),
(30, 'Khách sạn/ Nhà hàng', 'hang-khongdu-lchkhach-sn', 44, 1, 0, 0, '0000-00-00 00:00:00'),
(31, 'Thực phẩm/Đồ uống', 'thc-phm-ung', 44, 1, 0, 0, '0000-00-00 00:00:00'),
(32, 'Điện/Điện tử', 'inin-t', 39, 1, 0, 0, '0000-00-00 00:00:00'),
(33, 'Cơ khí - Chế tạo', 'c-khi-ch-to', 39, 1, 0, 0, '0000-00-00 00:00:00'),
(35, 'Môi trường/Xử lý chất thải', 'moi-trngx-ly-cht-thi', 39, 1, 0, 0, '0000-00-00 00:00:00'),
(36, 'Dược Phẩm/Công nghệ sinh học', 'dc-phmcong-ngh-sinh-hc', 5, 1, 0, 0, '0000-00-00 00:00:00'),
(37, 'Dầu khí- Hóa chất', 'du-khi', 5, 1, 0, 0, '0000-00-00 00:00:00'),
(38, 'Dệt may/Da giày', 'dt-mayda-giay', 5, 1, 0, 0, '0000-00-00 00:00:00'),
(39, 'Nông nghiệp/Lâm nghiệp', 'nong-nghiplam-nghip', 5, 1, 0, 0, '0000-00-00 00:00:00'),
(40, 'Bán lẻ/Bán sỉ ', 'ban-lban-s-', 52, 1, 0, 0, '0000-00-00 00:00:00'),
(41, 'Hàng cao cấp', 'hang-cao-cp', 52, 1, 0, 0, '0000-00-00 00:00:00'),
(42, 'Thời trang/Lifestyle', 'thi-tranglifestyle', 52, 1, 0, 0, '0000-00-00 00:00:00'),
(43, 'Bảo vệ', 'bo-v', 0, 1, 0, 0, '0000-00-00 00:00:00'),
(44, 'Kỹ sư', 'k-s', 0, 1, 0, 0, '0000-00-00 00:00:00'),
(45, 'IT phần mềm', 'cong-ngh-thong-tin', 39, 1, 0, 0, '0000-00-00 00:00:00'),
(46, 'Khác', 'khac', 0, 1, 0, 0, '0000-00-00 00:00:00'),
(49, 'In ấn-Xuất bản', 'in-n-xut-bn', 80, 1, 0, 0, '0000-00-00 00:00:00'),
(51, 'Người giúp việc', 'ngi-giup-vic', 72, 1, 0, 0, '0000-00-00 00:00:00'),
(52, 'Lao động phổ thông', 'lao-ng-ph-thong', 6, 1, 0, 0, '0000-00-00 00:00:00'),
(68, 'Y tế- Dược phẩm', 'y-t-dc-phm', 5, 1, 0, 0, '0000-00-00 00:00:00'),
(54, 'Phiên dịch', 'phien-dch', 47, 1, 0, 0, '0000-00-00 00:00:00'),
(55, 'Bưu chính', 'bu-chinh', 4, 1, 0, 0, '0000-00-00 00:00:00'),
(56, 'Dệt may - Da giày', 'dt-may-da-giay', 5, 1, 0, 0, '0000-00-00 00:00:00'),
(57, 'Điện tử viễn thông', 'in-t-vin-thong', 39, 1, 0, 0, '0000-00-00 00:00:00'),
(58, 'Du lịch', 'du-lch', 47, 1, 0, 0, '0000-00-00 00:00:00'),
(59, 'Giáo dục-Đào tạo', 'giao-dc-ao-to', 47, 1, 0, 0, '0000-00-00 00:00:00'),
(60, 'Hàng hải', 'hang-hi', 33, 1, 0, 0, '0000-00-00 00:00:00'),
(61, 'Hàng không', 'hang-khong', 33, 1, 0, 0, '0000-00-00 00:00:00'),
(62, 'Hành chính - Văn phòng', 'hanh-chinh-vn-phong', 79, 1, 0, 0, '0000-00-00 00:00:00'),
(63, 'Hóa học - Sinh học', 'hoa-hc-sinh-hc', 5, 1, 0, 0, '0000-00-00 00:00:00'),
(64, 'Hoạch định - Dự án', 'hoch-nh-d-an', 3, 1, 0, 0, '0000-00-00 00:00:00'),
(65, 'In ấn - Xuất bản', 'in-n-xut-bn', 5, 1, 0, 0, '0000-00-00 00:00:00'),
(66, 'IT phần cứng/mạng', 'it-phn-cngmng', 39, 1, 0, 0, '0000-00-00 00:00:00'),
(69, 'Pháp lý', 'phap-ly', 47, 1, 0, 0, '0000-00-00 00:00:00'),
(70, 'Thư ký/Trợ lý', 'th-kytr-ly', 79, 1, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_typepacket`
--

CREATE TABLE IF NOT EXISTS `jos_properties_typepacket` (
  `id` int(3) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `price_typepacket` float NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  `checked_out` tinyint(1) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `jos_properties_typepacket`
--

INSERT INTO `jos_properties_typepacket` (`id`, `name`, `alias`, `price_typepacket`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(11, 'top dau', 'top-dau', 350, 1, 0, 0, '0000-00-00 00:00:00'),
(12, 'Boi dam do', 'boi-dam-do', 300, 1, 0, 0, '0000-00-00 00:00:00'),
(13, 'viec lam tot nhat', 'viec-lam-tot-nhat', 400, 1, 0, 0, '0000-00-00 00:00:00'),
(14, 'Gói 40 CV', 'goi-40-cv', 40, 1, 0, 0, '0000-00-00 00:00:00'),
(15, 'Gói 60 CV', 'goi-60-cv', 60, 1, 0, 0, '0000-00-00 00:00:00'),
(16, 'Gói 90 CV', 'goi-90-cv', 90, 1, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_properties_youknow`
--

CREATE TABLE IF NOT EXISTS `jos_properties_youknow` (
  `id` int(3) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  `checked_out` tinyint(1) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `jos_properties_youknow`
--

INSERT INTO `jos_properties_youknow` (`id`, `name`, `alias`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(14, 'Website khác', 'website-khac', 1, 0, 0, '0000-00-00 00:00:00'),
(15, 'Báo/Tạp chí', 'baotp-chi', 1, 0, 0, '0000-00-00 00:00:00'),
(16, 'Công cụ tìm kiếm', 'cong-c-tim-kim', 1, 0, 0, '0000-00-00 00:00:00'),
(17, 'Quảng cáo trên truyền hình', 'qung-cao-tren-truyn-hinh', 1, 0, 0, '0000-00-00 00:00:00'),
(18, 'Khác', 'khac', 1, 0, 0, '0000-00-00 00:00:00'),
(19, 'Biển quảng cáo', 'bienquangcao', 1, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_redirection`
--

CREATE TABLE IF NOT EXISTS `jos_redirection` (
  `id` int(11) NOT NULL auto_increment,
  `cpt` int(11) NOT NULL default '0',
  `rank` int(11) NOT NULL default '0',
  `oldurl` varchar(255) NOT NULL default '',
  `newurl` varchar(255) NOT NULL default '',
  `dateadd` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id`),
  KEY `newurl` (`newurl`),
  KEY `rank` (`rank`),
  KEY `oldurl` (`oldurl`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1802 ;

--
-- Dumping data for table `jos_redirection`
--

INSERT INTO `jos_redirection` (`id`, `cpt`, `rank`, `oldurl`, `newurl`, `dateadd`) VALUES
(1087, 1, 0, 'nha-tuyển-dụng/danh-sach-hồ-sơ-ứng-vien-đa-ch�_n/28/60-goicv.html', '', '2011-02-15'),
(222, 1, 0, '84.html', '', '2011-02-14'),
(480, 4, 0, 'nha-tuyển-dụng/index.php', '', '2011-02-15'),
(865, 1, 0, 'lien-hệ/', '', '2011-02-15'),
(875, 1, 0, 'lien-hệ/index.php', '', '2011-02-15'),
(1615, 1, 0, 'Nha-tuyển-dụng/Sửa-việc-lam/Bưu-chinh/162/modules/mod_search_left/tmpl/ajaxloading.gif', '', '2011-03-08'),
(1612, 2, 0, 'Nha-tuyển-dụng/Sửa-việc-lam/IT-phần-mềm/161/index.php', '', '2011-03-08'),
(1611, 2, 0, 'Nha-tuyển-dụng/Sửa-việc-lam/IT-phần-mềm/161/modules/mod_search_left/tmpl/ajaxloading.gif', '', '2011-03-08'),
(1475, 9, 0, 'Quảng-cao/modules/mod_search_left/tmpl/ajaxloading.gif', '', '2011-02-19'),
(1595, 1, 0, 'Quảng-cao/Khuyến-mai/151/modules/mod_search_left/tmpl/ajaxloading.gif', '', '2011-03-08'),
(1484, 66, 0, 'modules/mod_search_left/tmpl/ajaxloading.gif', '', '2011-02-19'),
(1485, 5, 0, 'IT-phần-mềm/161/modules/mod_search_left/tmpl/ajaxloading.gif', '', '2011-02-19'),
(1486, 8, 0, 'Kiến-truc/modules/mod_search_left/tmpl/ajaxloading.gif', '', '2011-02-19'),
(1487, 2, 0, 'Giao-dục-Đao-tạo/160/modules/mod_search_left/tmpl/ajaxloading.gif', '', '2011-02-19'),
(1509, 1, 0, 'Kế-toan/modules/mod_search_left/tmpl/ajaxloading.gif', '', '2011-02-19'),
(1741, 15, 0, 'Ban-hang/164/index.php', '', '2011-03-09'),
(1545, 2, 0, 'Quảng-cao/index.php', '', '2011-02-19'),
(1801, 0, 0, 'chitiet.html', 'index.php?option=com_properties&agent_id=83&lang=vi&task=select_cv&view=chitiet', '0000-00-00'),
(1786, 0, 0, 'dịch-vụ-cung-ứng-nhan-sự/dch-v-cung-ng-nhan-s.html', 'index.php?option=com_content&id=57&lang=vi&view=article', '0000-00-00'),
(1800, 0, 0, 'logout.html', 'index.php?option=com_user&lang=vi&task=logout', '0000-00-00'),
(1799, 0, 0, 'lost-password.html', 'index.php?option=com_user&lang=vi&view=reset', '0000-00-00'),
(1798, 2, 0, 'ajax.html', 'index.php?option=com_properties&lang=vi&task=resultsearch&view=ajax', '0000-00-00'),
(1791, 0, 0, 'đang-ky-nha-tuyển-dụng.html', 'index.php?option=com_properties&lang=vi&task=add&view=employ', '0000-00-00'),
(1792, 3, 0, 'ban-hang/164/dsa.html', 'index.php?option=com_properties&agent_id=83&id=164&lang=vi&view=chitiet', '0000-00-00'),
(1793, 0, 0, '__404__.html', 'index.php?option=com_content&Itemid=1&id=64&lang=vi&view=article', '0000-00-00'),
(1794, 0, 1, '__404__.html', 'index.php?option=com_content&id=64&lang=vi&view=article', '0000-00-00'),
(1795, 0, 0, 'example-pages-and-menu-links.html', 'index.php?option=com_content&id=43&lang=vi&view=article', '0000-00-00'),
(1796, 0, 1, 'search-viec-lam.html', 'index.php?option=com_properties&lang=vi&view=search', '0000-00-00'),
(1797, 0, 0, 'ban-hang/165/sdsdsd.html', 'index.php?option=com_properties&agent_id=83&id=165&lang=vi&view=chitiet', '0000-00-00'),
(1788, 0, 0, 'bưu-chinh.html', 'index.php?option=com_properties&lang=vi&type=55&view=detail', '0000-00-00'),
(1789, 0, 1, 'danh-sach-việc-lam.html', 'index.php?option=com_properties&lang=vi&view=all', '0000-00-00'),
(1790, 0, 0, 'đang-ky-thanh-vien.html', 'index.php?option=com_properties&lang=vi&task=add&view=seecker', '0000-00-00'),
(1787, 1, 0, 'ban-hang.html', 'index.php?option=com_properties&lang=vi&type=12&view=detail', '0000-00-00'),
(1785, 1, 0, 'log-in.html', 'index.php?option=com_user&lang=vi&view=login', '0000-00-00'),
(1784, 0, 0, 'nhật-ky-tim-việc-lam/nht-ky-tim-vic-lam.html', 'index.php?option=com_content&catid=38&id=49&lang=vi&view=article', '0000-00-00'),
(1783, 0, 0, 'thong-bao-việc-lam/thong-bao-vic-lam.html', 'index.php?option=com_content&catid=36&id=48&lang=vi&view=article', '0000-00-00'),
(1782, 0, 0, 'đang-hồ-sơ/ng-h-s.html', 'index.php?option=com_content&catid=35&id=47&lang=vi&view=article', '0000-00-00'),
(1668, 2, 0, 'Nha-tuyển-dụng/Sửa-việc-lam/Ban-hang/164/modules/mod_search_left/tmpl/ajaxloading.gif', '', '2011-03-08'),
(1669, 1, 0, 'Nha-tuyển-dụng/Sửa-việc-lam/Ban-hang/164/index.php', '', '2011-03-08'),
(1779, 0, 0, 'trắc-nghiệm/lam-gi-h-s-ca-bn-c-chu-y.html', 'index.php?option=com_content&catid=40&id=54&lang=vi&view=article', '0000-00-00'),
(1671, 9, 0, 'Ban-hang/164/modules/mod_search_left/tmpl/ajaxloading.gif', '', '2011-03-08'),
(1672, 1, 0, 'Bưu-chinh/162/modules/mod_search_left/tmpl/ajaxloading.gif', '', '2011-03-09'),
(1673, 6, 0, 'Nha-tuyển-dụng/modules/mod_search_left/tmpl/ajaxloading.gif', '', '2011-03-09'),
(1781, 0, 0, 'tai-liệu-biểu-mẫu/tai-liu-biu-mu.html', 'index.php?option=com_content&catid=34&id=46&lang=vi&view=article', '0000-00-00'),
(1780, 1, 0, 'search-viec-lam.html', 'index.php?option=com_properties&Itemid=123&lang=vi&view=search', '0000-00-00'),
(1777, 0, 0, 'tư-vấn-nghề-nghiệp/6cacg.html', 'index.php?option=com_content&catid=39&id=61&lang=vi&view=article', '0000-00-00'),
(1778, 0, 0, 'trắc-nghiệm/bi-quyt-vit-h-s-xin-vic-trc-tuyn-hiu-qu.html', 'index.php?option=com_content&catid=40&id=51&lang=vi&view=article', '0000-00-00'),
(1775, 0, 0, 'giới-thiệu/quy-nh-bo-mt.html', 'index.php?option=com_content&Itemid=48&id=52&lang=vi&view=article', '0000-00-00'),
(1776, 0, 0, 'lien-hệ/name.html', 'index.php?option=com_contact&Itemid=49&id=1&lang=vi&view=contact', '0000-00-00'),
(1681, 6, 0, 'IT-phần-mềm/161/index.php', '', '2011-03-09'),
(1773, 0, 0, 'cv-ứng-vien.html', 'index.php?option=com_properties&Itemid=37&lang=vi&view=seecker', '0000-00-00'),
(1774, 0, 0, 'giới-thiệu/tha-thun-s-dng.html', 'index.php?option=com_content&Itemid=50&id=53&lang=vi&view=article', '0000-00-00'),
(1769, 0, 0, 'feed/atom.html', 'index.php?option=com_content&Itemid=1&format=feed&lang=vi&type=atom&view=frontpage', '0000-00-00'),
(1770, 0, 0, 'danh-sach-việc-lam.html', 'index.php?option=com_properties&Itemid=27&lang=vi&view=all', '0000-00-00'),
(1771, 0, 0, 'đang-hồ-sơ.html', 'index.php?option=com_properties&Itemid=2&lang=vi&view=postcv', '0000-00-00'),
(1772, 0, 0, 'mẫu-tai-liệu/', 'index.php?option=com_content&Itemid=44&id=44&lang=vi&layout=blog&view=category', '0000-00-00'),
(1768, 0, 0, 'feed/rss.html', 'index.php?option=com_content&Itemid=1&format=feed&lang=vi&type=rss&view=frontpage', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_sections`
--

CREATE TABLE IF NOT EXISTS `jos_sections` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `name` varchar(255) NOT NULL default '',
  `alias` varchar(255) NOT NULL default '',
  `image` text NOT NULL,
  `scope` varchar(50) NOT NULL default '',
  `image_position` varchar(30) NOT NULL default '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL default '0',
  `checked_out` int(11) unsigned NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL default '0',
  `access` tinyint(3) unsigned NOT NULL default '0',
  `count` int(11) NOT NULL default '0',
  `params` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_scope` (`scope`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `jos_sections`
--

INSERT INTO `jos_sections` (`id`, `title`, `name`, `alias`, `image`, `scope`, `image_position`, `description`, `published`, `checked_out`, `checked_out_time`, `ordering`, `access`, `count`, `params`) VALUES
(1, 'News', '', 'news', 'articles.jpg', 'content', 'right', 'Select a news topic from the list below, then select a news article to read.', 1, 0, '0000-00-00 00:00:00', 3, 0, 2, ''),
(3, 'FAQs', '', 'faqs', 'key.jpg', 'content', 'left', 'From the list below choose one of our FAQs topics, then select an FAQ to read. If you have a question which is not in this section, please contact us.', 1, 0, '0000-00-00 00:00:00', 5, 0, 23, ''),
(4, 'Giới thiệu', '', 'gii-thiu', '', 'content', 'left', '', 1, 0, '0000-00-00 00:00:00', 2, 0, 19, ''),
(5, 'tin', '', 'tin', '', 'content', 'left', '', 1, 0, '0000-00-00 00:00:00', 6, 0, 9, ''),
(6, 'giá', '', 'gia', '', 'content', 'left', '', 1, 0, '0000-00-00 00:00:00', 7, 0, 1, ''),
(7, 'Tài liệu biểu  mẫu', '', 'tai-liu-biu-mu', '', 'content', 'left', '', 1, 0, '0000-00-00 00:00:00', 8, 0, 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_session`
--

CREATE TABLE IF NOT EXISTS `jos_session` (
  `username` varchar(150) default '',
  `time` varchar(14) default '',
  `session_id` varchar(200) NOT NULL default '0',
  `guest` tinyint(4) default '1',
  `userid` int(11) default '0',
  `usertype` varchar(50) default '',
  `gid` tinyint(3) unsigned NOT NULL default '0',
  `client_id` tinyint(3) unsigned NOT NULL default '0',
  `data` longtext,
  PRIMARY KEY  (`session_id`(64)),
  KEY `whosonline` (`guest`,`usertype`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_session`
--

INSERT INTO `jos_session` (`username`, `time`, `session_id`, `guest`, `userid`, `usertype`, `gid`, `client_id`, `data`) VALUES
('admin', '1299663689', 'd46e5750f159c9ea9766d64b6a0da3cb', 0, 62, 'Super Administrator', 25, 1, '__default|a:8:{s:15:"session.counter";i:80;s:19:"session.timer.start";i:1299657141;s:18:"session.timer.last";i:1299663688;s:17:"session.timer.now";i:1299663689;s:22:"session.client.browser";s:90:"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.15) Gecko/20110303 Firefox/3.6.15";s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:7:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":0:{}}s:11:"application";a:1:{s:4:"data";O:8:"stdClass":1:{s:4:"lang";s:0:"";}}s:10:"com_cpanel";a:1:{s:4:"data";O:8:"stdClass":1:{s:9:"mtupgrade";O:8:"stdClass":1:{s:7:"checked";b:1;}}}s:4:"urls";a:1:{s:4:"data";O:8:"stdClass":1:{s:7:"default";O:8:"stdClass":1:{s:10:"limitstart";d:0;}}}s:12:"com_sh404sef";a:1:{s:4:"data";O:8:"stdClass":1:{s:4:"urls";O:8:"stdClass":1:{s:4:"urls";O:8:"stdClass":1:{s:7:"default";O:8:"stdClass":1:{s:10:"limitstart";d:0;}}}}}s:9:"com_users";a:1:{s:4:"data";O:8:"stdClass":6:{s:12:"filter_order";s:4:"a.id";s:16:"filter_order_Dir";s:4:"desc";s:11:"filter_type";s:1:"0";s:13:"filter_logged";i:0;s:6:"search";s:0:"";s:10:"limitstart";i:0;}}s:6:"global";a:1:{s:4:"data";O:8:"stdClass":1:{s:4:"list";O:8:"stdClass":1:{s:5:"limit";i:20;}}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";s:2:"62";s:4:"name";s:19:"Super Administrator";s:8:"username";s:5:"admin";s:5:"email";s:21:"thanhtd1986@gmail.com";s:8:"password";s:65:"a240050b27da2db786b923c75fb84e28:MiiDPmfrau1lCnvB1gXe1hvDv124vY5B";s:14:"password_clear";s:0:"";s:8:"usertype";s:19:"Super Administrator";s:5:"block";s:1:"0";s:9:"sendEmail";s:1:"1";s:3:"gid";s:2:"25";s:12:"registerDate";s:19:"2010-12-06 17:12:56";s:13:"lastvisitDate";s:19:"2011-03-09 03:32:56";s:10:"activation";s:32:"a9ea31fdd739c1dc34a9e8b211722fcf";s:6:"params";s:56:"admin_language=\nlanguage=\neditor=\nhelpsite=\ntimezone=7\n\n";s:3:"aid";i:2;s:5:"guest";i:0;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:63:"/var/www/html/tuyendung/libraries/joomla/html/parameter/element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":5:{s:14:"admin_language";s:0:"";s:8:"language";s:0:"";s:6:"editor";s:0:"";s:8:"helpsite";s:0:"";s:8:"timezone";s:1:"7";}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}s:13:"session.token";s:32:"17988fc93373140283214d229b77971f";}'),
('', '1299659455', 'rhh5tbckkk1l4330eqcua53f15', 1, 0, '', 0, 0, '__default|a:7:{s:15:"session.counter";i:1;s:19:"session.timer.start";i:1299659455;s:18:"session.timer.last";i:1299659455;s:17:"session.timer.now";i:1299659455;s:22:"session.client.browser";s:37:"Serf/0.3.1 mod_pagespeed/0.9.15.3-404";s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:1:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":1:{s:13:"security_code";s:6:"3913f4";}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:8:"usertype";N;s:5:"block";N;s:9:"sendEmail";i:0;s:3:"gid";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:3:"aid";i:0;s:5:"guest";i:1;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:63:"/var/www/html/tuyendung/libraries/joomla/html/parameter/element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}}'),
('', '1299663684', 'miqlofrjneov9poa8rtubep1c0', 1, 0, '', 0, 0, '__default|a:8:{s:15:"session.counter";i:151;s:19:"session.timer.start";i:1299659998;s:18:"session.timer.last";i:1299663683;s:17:"session.timer.now";i:1299663684;s:22:"session.client.browser";s:90:"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.15) Gecko/20110303 Firefox/3.6.15";s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:1:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":1:{s:13:"security_code";s:6:"7fc38f";}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:8:"usertype";N;s:5:"block";N;s:9:"sendEmail";i:0;s:3:"gid";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:3:"aid";i:0;s:5:"guest";i:1;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:63:"/var/www/html/tuyendung/libraries/joomla/html/parameter/element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}s:13:"session.token";s:32:"589ee3cdc53a12d9d61f0b6e1fbb79a4";}'),
('', '1299660546', '7suqga28c1gn1ivo29gfdd5fb3', 1, 0, '', 0, 0, '__default|a:7:{s:15:"session.counter";i:1;s:19:"session.timer.start";i:1299660546;s:18:"session.timer.last";i:1299660546;s:17:"session.timer.now";i:1299660546;s:22:"session.client.browser";s:37:"Serf/0.3.1 mod_pagespeed/0.9.15.3-404";s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:1:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":1:{s:13:"security_code";s:6:"b4f67e";}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:8:"usertype";N;s:5:"block";N;s:9:"sendEmail";i:0;s:3:"gid";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:3:"aid";i:0;s:5:"guest";i:1;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:63:"/var/www/html/tuyendung/libraries/joomla/html/parameter/element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}}'),
('', '1299661274', 'tbuskik7hmi5h13oglnst1vil5', 1, 0, '', 0, 0, '__default|a:7:{s:15:"session.counter";i:1;s:19:"session.timer.start";i:1299661274;s:18:"session.timer.last";i:1299661274;s:17:"session.timer.now";i:1299661274;s:22:"session.client.browser";s:37:"Serf/0.3.1 mod_pagespeed/0.9.15.3-404";s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:1:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":1:{s:13:"security_code";s:6:"c31474";}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:8:"usertype";N;s:5:"block";N;s:9:"sendEmail";i:0;s:3:"gid";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:3:"aid";i:0;s:5:"guest";i:1;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:63:"/var/www/html/tuyendung/libraries/joomla/html/parameter/element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}}'),
('', '1299661677', 'ljrbmp5tc52fh0f2tbl4ocmh12', 1, 0, '', 0, 0, '__default|a:7:{s:15:"session.counter";i:1;s:19:"session.timer.start";i:1299661677;s:18:"session.timer.last";i:1299661677;s:17:"session.timer.now";i:1299661677;s:22:"session.client.browser";s:37:"Serf/0.3.1 mod_pagespeed/0.9.15.3-404";s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:1:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":1:{s:13:"security_code";s:6:"c5f4e7";}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:8:"usertype";N;s:5:"block";N;s:9:"sendEmail";i:0;s:3:"gid";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:3:"aid";i:0;s:5:"guest";i:1;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:63:"/var/www/html/tuyendung/libraries/joomla/html/parameter/element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}}'),
('thanhtd', '1299663056', '37a98641ca2564763c11f63bf02bfe6a', 0, 212, 'Super Administrator', 25, 0, '__default|a:8:{s:15:"session.counter";i:30;s:19:"session.timer.start";i:1299662617;s:18:"session.timer.last";i:1299663030;s:17:"session.timer.now";i:1299663056;s:22:"session.client.browser";s:87:"Mozilla/5.0 (Windows; U; Windows NT 6.1; vi; rv:1.9.2.15) Gecko/20110303 Firefox/3.6.15";s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:1:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";s:3:"212";s:4:"name";s:23:"thanhtd-creativevietnam";s:8:"username";s:7:"thanhtd";s:5:"email";s:16:"thanhtd@info.com";s:8:"password";s:65:"a5a9e9a237118ac3401244dc19a8a578:fxQwIba148Ihuhf4kPuPVSt7VlsdUa99";s:14:"password_clear";s:0:"";s:8:"usertype";s:19:"Super Administrator";s:5:"block";s:1:"0";s:9:"sendEmail";s:1:"0";s:3:"gid";s:2:"25";s:12:"registerDate";s:19:"2010-12-07 00:33:59";s:13:"lastvisitDate";s:19:"2011-03-05 02:07:59";s:10:"activation";s:0:"";s:6:"params";s:56:"admin_language=\nlanguage=\neditor=\nhelpsite=\ntimezone=0\n\n";s:3:"aid";i:2;s:5:"guest";i:0;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:63:"/var/www/html/tuyendung/libraries/joomla/html/parameter/element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":5:{s:14:"admin_language";s:0:"";s:8:"language";s:0:"";s:6:"editor";s:0:"";s:8:"helpsite";s:0:"";s:8:"timezone";s:1:"0";}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}s:13:"session.token";s:32:"1e296e4dc19db262c38235a13a37d3d7";}'),
('', '1299662703', 'dqk4p2mqfd95199eppovtq8o95', 1, 0, '', 0, 0, '__default|a:7:{s:15:"session.counter";i:1;s:19:"session.timer.start";i:1299662703;s:18:"session.timer.last";i:1299662703;s:17:"session.timer.now";i:1299662703;s:22:"session.client.browser";s:37:"Serf/0.3.1 mod_pagespeed/0.9.15.3-404";s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:1:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":1:{s:13:"security_code";s:6:"252c19";}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:8:"usertype";N;s:5:"block";N;s:9:"sendEmail";i:0;s:3:"gid";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:3:"aid";i:0;s:5:"guest";i:1;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:63:"/var/www/html/tuyendung/libraries/joomla/html/parameter/element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}}'),
('admin', '1299663054', '365d59eec86ab30d8e2f977e6104b64c', 0, 62, 'Super Administrator', 25, 1, '__default|a:8:{s:15:"session.counter";i:7;s:19:"session.timer.start";i:1299663041;s:18:"session.timer.last";i:1299663052;s:17:"session.timer.now";i:1299663054;s:22:"session.client.browser";s:87:"Mozilla/5.0 (Windows; U; Windows NT 6.1; vi; rv:1.9.2.15) Gecko/20110303 Firefox/3.6.15";s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:3:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":0:{}}s:11:"application";a:1:{s:4:"data";O:8:"stdClass":1:{s:4:"lang";s:0:"";}}s:10:"com_cpanel";a:1:{s:4:"data";O:8:"stdClass":1:{s:9:"mtupgrade";O:8:"stdClass":1:{s:7:"checked";b:1;}}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";s:2:"62";s:4:"name";s:19:"Super Administrator";s:8:"username";s:5:"admin";s:5:"email";s:21:"thanhtd1986@gmail.com";s:8:"password";s:65:"a240050b27da2db786b923c75fb84e28:MiiDPmfrau1lCnvB1gXe1hvDv124vY5B";s:14:"password_clear";s:0:"";s:8:"usertype";s:19:"Super Administrator";s:5:"block";s:1:"0";s:9:"sendEmail";s:1:"1";s:3:"gid";s:2:"25";s:12:"registerDate";s:19:"2010-12-06 17:12:56";s:13:"lastvisitDate";s:19:"2011-03-09 08:39:04";s:10:"activation";s:32:"a9ea31fdd739c1dc34a9e8b211722fcf";s:6:"params";s:56:"admin_language=\nlanguage=\neditor=\nhelpsite=\ntimezone=7\n\n";s:3:"aid";i:2;s:5:"guest";i:0;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:63:"/var/www/html/tuyendung/libraries/joomla/html/parameter/element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":5:{s:14:"admin_language";s:0:"";s:8:"language";s:0:"";s:6:"editor";s:0:"";s:8:"helpsite";s:0:"";s:8:"timezone";s:1:"7";}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}s:13:"session.token";s:32:"1a8bb2e0190223c0d5f34cda8cc9d95e";}');

-- --------------------------------------------------------

--
-- Table structure for table `jos_sh404sef_aliases`
--

CREATE TABLE IF NOT EXISTS `jos_sh404sef_aliases` (
  `id` int(11) NOT NULL auto_increment,
  `newurl` varchar(255) NOT NULL default '',
  `alias` varchar(255) NOT NULL default '',
  `type` tinyint(3) NOT NULL default '0',
  `hits` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `newurl` (`newurl`),
  KEY `alias` (`alias`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_sh404sef_aliases`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_sh404sef_meta`
--

CREATE TABLE IF NOT EXISTS `jos_sh404sef_meta` (
  `id` int(11) NOT NULL auto_increment,
  `newurl` varchar(255) NOT NULL default '',
  `metadesc` varchar(255) default '',
  `metakey` varchar(255) default '',
  `metatitle` varchar(255) default '',
  `metalang` varchar(30) default '',
  `metarobots` varchar(30) default '',
  PRIMARY KEY  (`id`),
  KEY `newurl` (`newurl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_sh404sef_meta`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_sh404sef_pageids`
--

CREATE TABLE IF NOT EXISTS `jos_sh404sef_pageids` (
  `id` int(11) NOT NULL auto_increment,
  `newurl` varchar(255) NOT NULL default '',
  `pageid` varchar(255) NOT NULL default '',
  `type` tinyint(3) NOT NULL default '0',
  `hits` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `newurl` (`newurl`),
  KEY `alias` (`pageid`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_sh404sef_pageids`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_stats_agents`
--

CREATE TABLE IF NOT EXISTS `jos_stats_agents` (
  `agent` varchar(255) NOT NULL default '',
  `type` tinyint(1) unsigned NOT NULL default '0',
  `hits` int(11) unsigned NOT NULL default '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_stats_agents`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_templates_menu`
--

CREATE TABLE IF NOT EXISTS `jos_templates_menu` (
  `template` varchar(255) NOT NULL default '',
  `menuid` int(11) NOT NULL default '0',
  `client_id` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`menuid`,`client_id`,`template`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_templates_menu`
--

INSERT INTO `jos_templates_menu` (`template`, `menuid`, `client_id`) VALUES
('jobs_plazza_quickstart', 0, 0),
('khepri', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_users`
--

CREATE TABLE IF NOT EXISTS `jos_users` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `username` varchar(150) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `password` varchar(100) NOT NULL default '',
  `usertype` varchar(25) NOT NULL default '',
  `block` tinyint(4) NOT NULL default '0',
  `sendEmail` tinyint(4) default '0',
  `gid` tinyint(3) unsigned NOT NULL default '1',
  `registerDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL default '',
  `params` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `usertype` (`usertype`),
  KEY `idx_name` (`name`),
  KEY `gid_block` (`gid`,`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=274 ;

--
-- Dumping data for table `jos_users`
--

INSERT INTO `jos_users` (`id`, `name`, `username`, `email`, `password`, `usertype`, `block`, `sendEmail`, `gid`, `registerDate`, `lastvisitDate`, `activation`, `params`) VALUES
(64, 'khuong', 'khuong', 'nnk_bk472002@yahoo.com', '6fe5cc111b771a3e9f7acea1cbf7b7cb:VNBxTZUEX3QLNd5Xvzkoyj94fK4y4ZHw', 'Administrator', 0, 0, 24, '2010-10-19 04:19:14', '2010-10-28 07:20:24', '', 'admin_language=en-GB\nlanguage=en-GB\neditor=\nhelpsite=\ntimezone=0\n\n'),
(231, 'Nguyễn Anh Minh', 'tuyendung', 'tuyendung@nhanlucvietbac.com.vn', '99820219951f5d0057e40ef79bf26a72:3W1wpqTe8mGBajM19ndHvAOMdAYvTV9g', '', 0, 0, 18, '2010-12-31 01:44:09', '2011-01-11 00:55:53', 'e2de258490c2cb34e1f7807d264575d6', '\n'),
(230, 'Mss Quỳnh', 'ketoanhailinh', 'ketoanhailinh@gmail.com', '3f271e4c4fa9ee0f89a06acfdb87374d:tyvPzwUnF1v4F6ZOXColRbxbavTH1PGD', '', 0, 0, 18, '2010-12-30 05:41:44', '2010-12-30 05:46:45', 'b318ffd3a13d3152cc3d3d0c65ba13cf', '\n'),
(226, 'thanhtd211986', 'thanhtd211986', 'thanhtd211986@gmail.com', 'a655059ed148a29964b1438e9fb2a8b7:G2ekqlsLMIB2y0m4ea5lEOtN7ggqKJZh', '', 0, 0, 18, '2010-12-28 01:31:02', '0000-00-00 00:00:00', '4defb215cbd7f25ed3e66e8359349368', '\n'),
(79, 'phuong', 'phuong', 'khuong@yahoo.com', '0134d1c6982079aaa5058506c59579f1:JlU4oKMdOzkMDVtl0KbPREwYTnOxRZ8o', 'Registered', 0, 1, 18, '2010-10-28 07:26:50', '2010-11-22 08:24:34', '4c9795ba72dab1935f2b1bc0bc55d138', 'page_title=Thay Đổi Thông Số Cơ Bản\nshow_page_title=1\n\n'),
(77, 'khuong', 'yeuem', 'koyeu@yahoo.com', '8eb7f1fbed6fcce353c4a0063bbb072b:tTmsihOrKOt0b0Sc8vUMA60DpDH0RvPV', '', 0, 0, 18, '2010-10-28 02:21:15', '0000-00-00 00:00:00', '2a323f79ce3bbb8e40b87f16f631df68', '\n'),
(227, 'Vu Van Nam', 'namvietbac', 'nam@nhanlucvietbac.com.vn', '0b8abf2c11343a9349dda3610944a28f:cWNL2jjCABYU7fE6DWEHxEioBkGW0ogp', '', 0, 0, 18, '2010-12-29 03:34:16', '2010-12-30 05:44:41', '', '\n'),
(228, 'Phạm Trọng Nghĩa', 'nghia', 'nghia@nhanlucvietbac.com.vn', 'd81934c0c7296e936aee4f1d859e108d:UM3Vm4RmIn559SKSyVOccpSpC7g2tpxu', '', 0, 0, 18, '2010-12-29 03:47:11', '2010-12-31 01:29:35', '', '\n'),
(83, 'thanhtd1', 'thanhtd1', 'thanhtd@gmail.com', '814a641e93d0ba6719dac2c7eb7b98cd:4uzGviq2trNMn9tFVZAzmtogB8FDsCG5', 'Administrator', 0, 0, 18, '2010-11-02 09:12:13', '2011-03-09 08:39:58', '6c9a8e0a880a63e05c80264dda8dd918', 'admin_language=\nlanguage=\neditor=\nhelpsite=\ntimezone=0\n\n'),
(84, 'thanhtd2', 'thanhtd2', 'thanhtd2@gmail.com', 'c6e4ea25205fbd3dfb7b22fe87cf4869:IumIvSnfWFwyxShnmJk9xkwIoXi5J9fQ', '', 0, 0, 18, '2010-11-02 09:42:33', '2011-02-19 04:28:36', '0c3bc60d7f931a90e127721997d267ac', '\n'),
(234, 'trần trọng tân', 'tantan1983', 'trantrongtan1983@yahoo.com', 'a962d199c3de1bb2759699da4eb55264:qGghVrEjF2cK0WyFuHmTYRd79A86DvIa', '', 0, 0, 18, '2011-01-11 09:55:05', '0000-00-00 00:00:00', '', '\n'),
(87, 'admin321', 'admin321', 'admin321@gmail.com', '5019ac8b4fc97ee4d7dfaaa669d8d7c8:NsTebMC2glLkwiGyd9ZfQ8xXXKaSrVSo', 'Registered', 0, 1, 18, '2010-11-04 23:57:23', '2010-11-05 00:04:01', 'fe1cce7b5f97c8ab2c3e1bfa1ab2f5e3', 'admin_language=\nlanguage=\neditor=\nhelpsite=\ntimezone=0\n\n'),
(224, 'hangnt05', 'hangnt05', 'hangnt@yahoo.com', 'f877f575151b372720622fc2f5963343:B5y4lASmJjMUPe512bftCio9Ub7EsBuE', '', 0, 0, 18, '2010-12-22 09:37:31', '2010-12-22 09:39:08', '794cd673cdcf7b5e466c688861f8cd92', '\n'),
(89, 'Lương Đức Thủy', 'mrthanhtd2', 'mrthanhtd2@gmail.com', 'e65473c8bc80ff7b327e76d9bd9d5e9d:oa873ayQA8bNGfW4RTIO7uuRcbZnItgR', '', 0, 0, 18, '2010-11-05 05:24:54', '2010-11-09 23:12:37', '74f6b9dbc7a45eb603b10cbfdc7455ed', '\n'),
(90, 'thuhang1', 'thuhang1', 'thuhang1@gmail.com', '4a908644eae2ea4c8c5c06c471f4e97b:dmtsCPglp6SH9kdao60A1JOau1PHPNuL', '', 0, 0, 18, '2010-11-05 07:40:47', '2011-02-15 03:30:27', 'd70349f0396a9cddc654f094e5c3a5bf', '\n'),
(91, 'thuhang2', 'thuhang2', 'thuhang2@gmail.com', '7e80be226eafdb30c48b315ece3c1ac3:4Ytcx5Yry4BPOgLk1RFluArm04xUkE8C', '', 0, 0, 18, '2010-11-05 07:48:09', '2010-11-10 06:49:04', '4fe9e29d55baf6fdae2cf30cbadc25a4', '\n'),
(120, 'Trần Thành Trung', 'thanhtrung', 'tuyendung.vietlightstar@gmail.com', 'fe774baf59f0f11ca167c2d002767e1b:uOuUPGpZwzI9zoPVyiQCHvLHTmKrlzgW', '', 0, 0, 18, '2010-11-25 14:26:25', '2010-11-25 14:31:47', '2b1e36184399bf6628a226830f4921d4', '\n'),
(94, 'Lan Anh', 'lananh', 'lananh@yahoo.com', '088a33379cabd85d4f77f2b00cd60ded:W8MzNCXUEVAfOXw0WxikRvpzW3rCAjXl', '', 0, 0, 18, '2010-11-09 23:37:05', '2010-11-10 00:37:01', 'c15b0aed0388b73183c6d7c43c6de985', '\n'),
(95, 'Đỗ Bích Thắm', 'opal', 'do.bich.tham@gmail.com', 'f3c803b131db365f0df1f341dd695ee5:V6JKqiaPOwQ7SblzYPvl2OkvYsRH89mi', '', 0, 0, 18, '2010-11-10 00:44:23', '2010-11-10 06:14:17', '96e3367cc4a4ce37e6e31533563bfd7f', '\n'),
(96, 'ESECO', 'eseco', 'eseco@gmail.com', '0d7986b0da2335db07774bf1548a4709:kWsZo3pMZTJjytoCvpWj2rPaDUybJjpR', '', 0, 0, 18, '2010-11-11 00:05:42', '2010-11-11 09:09:30', 'ca47f6e4dd283eb38daa924101fa18b0', '\n'),
(97, 'Thuc pham TH', 'thucphamth', 'hoa.vietnamnet@gmail.com', '12e7b50f25c65d408d1557a5052b2947:rD8fsbF1tbtrnjP6j2ltKGJpbnXSrXV4', '', 0, 0, 18, '2010-11-11 09:08:26', '2010-11-11 02:43:53', '278d8c136bc74765b97773c22d20f521', '\n'),
(98, 'dulich', 'dulich', 'fstar.jsc@gmail.com', '7943586c3966c6781ce25478765316ea:GnFclfG4ts5sQsz2Z8eNCMV73JbGr1YV', '', 0, 0, 18, '2010-11-11 09:14:09', '2010-12-27 23:57:46', 'f55f625ad72156d24e2460c1bb60daea', '\n'),
(225, 'hanh', 'minhhanh', 'minhhanh_1993@yahoo.com', '38d5ffebe2ac632233b8adf37139fb23:l1tEfUMX6uiCmmTU7qkRsJgCu1qvzEbG', 'Administrator', 0, 0, 24, '2010-12-23 15:00:04', '2010-12-23 15:01:17', '', 'admin_language=\nlanguage=\neditor=\nhelpsite=\ntimezone=0\n\n'),
(106, 'Teleperformance', 'teleperformance', 'teleperformance@gmail.com', '1155d9289c40ede8c0e67ef726f048a0:3lku8haVcruS1Pz3N6dMQot6rcyBF5Tk', '', 0, 0, 18, '2010-11-15 01:32:28', '2010-12-16 09:03:40', '7f1e405866308f4b6d7dc27fe894d84f', '\n'),
(107, 'gialong', 'gialong', 'gialong@gmail.com', 'eed0038b5c5458d9a2844afbfdcbb811:BE8RmkDWxr4ZSRGKmDtj48EBJNE0Frta', '', 0, 0, 18, '2010-11-15 06:26:26', '2010-11-15 06:29:11', '45097d7312ee7e36073ad4b83663119c', '\n'),
(110, 'thuhang4', 'thuhang4', 'thuongtrangcong@gmail.com', 'b1102cd61f70cd4cf17e2fc4e04a801b:oraKvlT9Cp3IkpJVXFQRtx8IEl0eqSyr', '', 0, 0, 18, '2010-11-23 05:36:39', '2011-02-15 03:42:04', 'a439268f9e2588e298cb3f7a2b198273', '\n'),
(111, 'joliesiam', 'joliesiam', 'hr@joliesiam.com', '88e71dd8c747375289b7d5a77e0f3ae5:DALZ21afTAVyUWgJha00JtCxPZV61RrQ', '', 0, 0, 18, '2010-11-23 05:50:04', '2010-12-01 00:52:31', '9f42996677bef4aaedda750d1bdc9333', '\n'),
(112, 'Nguyễn Tiến Minh', 'ankhanh', 'ankhanh@donghohieu.com', '49bdc75018ca3b239984cfe2c27b2aa1:jjTnp3gXgQF4E77zAyAuEMKK9oX8X9AD', '', 0, 0, 18, '2010-11-24 06:54:00', '2010-12-01 00:30:26', '9d67c16b2dac26fe9ac2f0803a58abef', '\n'),
(113, 'avo', 'avo', 'avo@gmail.com', 'ffd6d54858af716634f9868de3ce36d5:MlQH7xmsKAtt3kVdeby0Btnddv9U0NfA', '', 0, 0, 18, '2010-11-24 08:55:05', '2010-11-24 09:00:51', '0245fc4ceb1d0c4a0f83a29d5e9ec81f', '\n'),
(114, 'Nguyễn Lê Sơn', 'leson', 'son.nguyenle.avg@gmail.com', '5e2f642b1f4bccd7a0bd623431ff88ca:Wv76ozWOeRAPj1bPNPYBolxeaEa0vP3H', 'Registered', 0, 0, 18, '2010-11-24 17:19:30', '2010-11-24 23:27:42', '0deabd01cafed254a70e250cd62a0589', 'admin_language=\nlanguage=\neditor=\nhelpsite=\ntimezone=0\n\n'),
(115, 'Nguyễn Quốc Dũng', 'quocdung', 'quocdung.nguyen@anz.com', 'a0647c357d8a16bfbebb270e728bb5cd:Z7YSDLS3Fmr6prKuiMJihnXYU1dvtpLQ', '', 0, 0, 18, '2010-11-25 01:10:15', '0000-00-00 00:00:00', '27f1e07bfaa41a3611f3e490bd6999b4', '\n'),
(119, 'Mis Bông', 'MisBong', 'evoizvn@yahoo.com', '2fd1d689eb9207b0f4d10d9cafabb17a:baf3Yo1DBcRrnGblChWVzKsWI17FLNgk', '', 0, 0, 18, '2010-11-25 14:16:38', '2010-11-25 14:23:15', '968baae24232abb3a7cb8f4a3d2d04be', '\n'),
(118, 'Đặng Tuấn Hưng', 'tuanhung', 'hungdt@lv-bentonite.com', '09715e4add3ef6f4ec32a364547c24b3:FzbXhqj0y3OliIXxoxWDyDoRFsX8ghUj', '', 0, 0, 18, '2010-11-25 07:05:14', '2010-11-25 09:32:06', 'a4161ddfcc7376a86efd35dc9dfe1362', '\n'),
(121, 'cao văn trung', 'caotrung', 'caotrung2003@gmail.com', '7316a4842b1f1c464442628a685f2f0b:7BhQ36cZUSN9kyY4WnS9CxjayINEd7Pc', '', 0, 0, 18, '2010-11-25 15:03:30', '2010-11-25 15:28:46', 'eb2cec3ee21eaf6fff67782f7b72be5e', '\n'),
(122, 'Thanh Nga', 'thanhnga', 'ngactt@mptelecom.com.vn', '2e2eef3c7d74ba8e9afcb6a79150c597:I1NbYdikWYy3gk7oRGmNU5BEP486kZjl', '', 0, 0, 18, '2010-11-26 02:21:47', '2010-11-26 04:20:40', '33cf547fc38910cbc3b82d9d04b1e605', '\n'),
(123, 'Phạm Hồng Thu', 'quangvinh', 'quangvinhkd@gmail.com', 'b766ffb43734160540f4621c8f53b821:jkOdZWaz315J3rn4WMsb7RtbeMWhKX5G', '', 0, 0, 18, '2010-11-26 03:00:45', '2010-11-26 03:15:55', 'e3295d0fe865e8439be6233dcbe63d2c', '\n'),
(124, 'Đỗ Thị Trung', 'trungvlx', 'trungvlx@gmail.com', '7f2962175ddce6c645f91734777c3013:k3abYuCFWUaQw5EHpxDPvTeN2X6MOXsN', '', 0, 0, 18, '2010-11-26 03:21:01', '2010-11-26 03:27:23', 'c1188e151b99bafeca905e71ead49bba', '\n'),
(125, 'Nguyễn Giang', 'nguyengiang', 'gianghd107@gmail.com', 'fee6e97fae6639a16dda07b555aed90a:4K084njtnu8wR8FZkgTDFNpUveA43Ujs', '', 0, 0, 18, '2010-11-26 03:33:11', '2010-11-26 03:58:06', '3e7b6f4365f02463c132056df92bacd8', '\n'),
(127, 'Võ Thị Thanh Đàn', 'thanhdan', 'noithathungson@gmail.com', '96e1c0c8394e04e09d6d4f5ad1fe6298:2LLZZgfwVpFexyr2LPGPRekZcHUH7Adu', '', 0, 0, 18, '2010-11-26 06:46:19', '2010-11-26 06:52:56', '879660e8833def2a9e5732baa4ddafb9', '\n'),
(128, 'Đỗ Thị Bằng', 'dobang', 'bangdaihung.9@gmail.com', '0fffe2cf2bc707cb37e665ef0f33a9ae:ObDopCbU9E2g6FTHRAnUEBwnwB5ZBfhF', '', 0, 0, 18, '2010-11-26 07:02:33', '2010-11-26 07:24:19', '45fb7aab37bd2b4c0585d8344631b874', '\n'),
(129, 'Hùng', 'pthung', 'pthung@vinhhao.com.vn', 'b8c679056a9b037aafed7d21cdd1de30:bQ6thBXQma8k73nwgXihfKB3jWQcE1Zm', '', 0, 0, 18, '2010-11-26 07:04:23', '2010-11-26 07:06:32', '', '\n'),
(130, 'Nguyễn Thị Việt Anh', 'ducanh', 'ducanhthtwindows@gmail.com', '2f7b737f64cb7626f8c04d8a62f63014:xxW9ALDKwOgWltPDL24v0qYDcIpTd8iK', '', 0, 0, 18, '2010-11-26 07:33:08', '2010-11-26 07:34:30', '6ab2848d19833cadaf7b0c03b48eef47', '\n'),
(131, 'Bích Thuỷ', 'bichthuy', 'thuy_hb@evafashion.com.vn', '0d483801b1de2db5f38adb5f04807b6f:sY5jSpXqkWJdMbHs1Iql9Hb6UTS9QmH8', '', 0, 0, 18, '2010-11-29 02:38:34', '2010-11-29 02:48:23', 'f22b0545cec55c7620439643f171d1dc', '\n'),
(132, 'Anh Bình hoặc Hùng', 'binhhung', 'vinatb8888@gmail.com', '3d4b354af398b107ddd076d379210244:3YQJ2sCXZNsYT9Lqdr1s6DwVl2nfrJnj', '', 0, 0, 18, '2010-11-29 05:28:20', '2010-11-29 05:29:53', 'd5fc408ad4b855ae6c9513e9af6189de', '\n'),
(133, '	Ho Thi Hai Anh ', 'haianh', 'anhhohai@tara.com.vn', '16e286a3ecb2d71422b9453c40b39265:wftjRhf5oZBkfV49eQB8BCCbL0b7SI0P', '', 0, 0, 18, '2010-11-30 05:16:37', '2010-11-30 05:29:52', '6e467fafb1e7362f5bcbb0c3cb730c20', '\n'),
(134, 'Mai Thị Yến', 'maithiyen', 'congtytuananh@gmail.com', 'f28fc92c866287be6dde01a148152cc8:nOe6PbhtCTGJhnCbaenwMlH0ofXSItei', '', 0, 0, 18, '2010-11-30 05:43:04', '2010-12-01 01:55:56', 'ac9cd911aaff382745a9223437213754', '\n'),
(135, 'Nguyễn Thành Lập', 'thanhlap', 'icelvn@gmail.com', 'f0a1813bb19abf249a9c1f68c3762e18:UpYg4bZUSLlczXlrjDdMIM04JZw1e5oZ', '', 0, 0, 18, '2010-11-30 05:52:51', '2010-11-30 05:54:03', '094422d5405416eb8f9c17d5c573484c', '\n'),
(136, 'Cương', 'cuong', 'cuongkimnguu@gmail.com', '9cc6cf6578633c1f5290fc94b4937e5e:TmHr58h6xXrP7BLmzNg8QyDmJVNNqf2d', '', 0, 0, 18, '2010-11-30 10:05:06', '2010-12-01 07:14:33', 'e30eb749475a6c5e429523ccd6111d62', '\n'),
(235, 'hangnt04', 'hangnt04', 'hangnt03@gmail.com', 'e97a444e8df142f8c3c8bb985ba53300:7UAyIFsD6C6wEQDH5yG6SgVg7qaFFUSH', '', 0, 0, 18, '2011-01-14 02:59:18', '2011-01-21 01:55:03', '', '\n'),
(138, 'Lê Thị Hồng', 'lehong', 'hangchipchop@yahoo.co.uk', 'ddb62bdbe2bb1dbaf399a8e47bc2458f:1goJXRQE5JYNYNTBmRQD5ovBLUPyaLjq', '', 0, 0, 18, '2010-12-01 00:34:34', '2010-12-01 00:50:32', '', '\n'),
(139, 'Phòng nhân sự', 'phongnhansu', 'tuyendung.nhatnguyen@gmail.com', '76c96074cbfba5257d5c72e30adafc12:twu4QMLjYWq6TNcnKjFez16ptsoqoAUB', '', 0, 0, 18, '2010-12-01 06:55:37', '0000-00-00 00:00:00', '2ce410b57875c90f23561e9189f0370c', '\n'),
(140, 'Phòng lễ tân', 'letan', 'hoatd2010@gmail.com', '05a0b4d27659127f5da206efd93bf5c7:GddZPhKOBUEW0YOz7t4gxzDj3jgpTowH', '', 0, 0, 18, '2010-12-01 07:07:47', '2010-12-01 07:22:24', '9af1a183854b182213f32bb5b82d3906', '\n'),
(141, 'Anh Bình', 'anhbinh', 'khiet_funny@yahoo.com', '82df6283463a3e714d7af948df555b21:2jC8jd4QZ3TELDsT1rDsMegVHM1kFxcJ', '', 0, 0, 18, '2010-12-01 07:25:06', '2010-12-01 07:30:20', '69d7f4d16e77e45d8cce9191a0b8ddc3', '\n'),
(142, 'Phòng quản lý nhân sự', 'phongqlns', 'tuyendung@vienmaytinh.org', '3bedfba3a2af78163b5f27c20adfcf11:OAljuEc3moBiOUUgKHyuQlUeH5uPKWQa', '', 0, 0, 18, '2010-12-01 07:33:21', '2010-12-01 07:39:48', '0e74281203aaee19a40edf6a887f011a', '\n'),
(143, ' 	CÔNG TY CỔ PHẦN CÔNG NGHỆ SAO PHƯƠNG NAM', 'saophuongnam', 'admin_hrm@southernstars.com.vn', '0c7bdd905803989777dd180c99c55b89:zMyhIsJRZjfEmXtHvrAehG4cf8TZwUlQ', '', 0, 0, 18, '2010-12-01 07:42:07', '2010-12-01 07:53:29', '96f862585a9f61dd8c9f483a92a91430', '\n'),
(144, 'hành chính nhân sự', 'hcnhansu', 'nhansutrieunhat@gmail.com', 'dc3d52c8451381955ec9549506341309:wXGcRsemoaPXXbgt8pDhjhRGzPfijhB0', '', 0, 0, 18, '2010-12-01 08:38:33', '2010-12-01 08:46:18', 'e99e14932ee77e9fc493afb17b94a20b', '\n'),
(145, 'Yến', 'msyen', 'tuyendung1@bds.vn', '91a648e68e9c15d0d6e8fa8e9b447e92:c0YQJ82OUJJC0RhFNYTIr2B1qUyHbSBq', '', 0, 0, 18, '2010-12-01 08:48:37', '2010-12-01 08:52:46', '2de921e49aab36133cd626ebd108901c', '\n'),
(146, 'Thanh Hoa', 'thanhhoa', 'thanhhoa.thanhhoa71@gmail.com', '5056797c36a65101f159de64d723d2ec:jmTNoxTFmU1TKxq6705j5O0Qnxy11My8', '', 0, 0, 18, '2010-12-01 08:54:17', '2010-12-01 08:58:34', '69dc207e323de81712aea350e08b8e47', '\n'),
(147, 'Nguyễn Thị Ngọc Hà', 'ngocha', 'ha_thanh1986@yahoo.com.vn', 'fb0c5e79e23f52867080b010f8b469c1:YE3rGg6x94Kd4C5BWL6sKbcGX8ek39BP', '', 0, 0, 18, '2010-12-01 09:00:30', '2010-12-02 00:42:47', 'fa7fdbf22bb16e1312079bf6663c3cbe', '\n'),
(148, 'Vũ Huyền', 'vuhuyen', 'huyenvtt@hrc.vn', 'b671d2c44e1a7f0183d7f63903efac67:io4srLT6GmRzGXDAYREMwMJ8qxlJJpfp', '', 0, 0, 18, '2010-12-01 09:06:37', '2010-12-01 09:10:57', '14d00ff7b91b7c0fdf6c5224f6683ef0', '\n'),
(149, 'Nguyễn Thùy Duyên', 'thuyduyen', 'kezin09@gmail.com', '6051d6bf6bf69728e7b2df51f7f1c20f:LKS120GOGqMpzhz9uu3IZzVby946cifl', '', 0, 0, 18, '2010-12-01 09:17:59', '2010-12-01 09:22:45', '247a578ae381a335b314ff1c468a95f3', '\n'),
(150, 'Kỳ Duyên', 'kyduyen', 'duyenkyns@gmail.com', 'f6e005e5dcba110b55743370eebfcec4:0bodU6CWT1YVn59xw0OWBPRwSXF3eUw9', '', 0, 0, 18, '2010-12-01 09:24:51', '2010-12-01 09:27:44', '0372548a4bbf1b9b5cde1adcaebef3c2', '\n'),
(151, 'Vo minh Tuan', 'vominhtuan', 'vmtuanhaviet@gmail.com', '353998daefdc2693c79f8d0146f0015d:aJ49yRtK1KoVPbcxQRAJ73basG5H2cS4', '', 0, 0, 18, '2010-12-01 09:30:37', '2010-12-01 09:33:52', 'b2b3a4aaa4027e948d4d3e6919b3f41e', '\n'),
(152, 'Nguyễn Phương', 'nguyenphuong', 'phuongntm@eurowindow.biz', '438c04865588ef74e8a14f44fe091bab:73Q11bcreTpNzVZN8baEfQ6Ws1HbrfzS', '', 0, 0, 18, '2010-12-01 09:38:18', '2010-12-01 09:43:47', '5d9a807066e61c8cc8f9a287ed7f13c1', '\n'),
(153, 'Vân', 'msvan', 'hr@batdongsankmass.com', '02ffc892c93858dd48147bfedca4aa8b:H8mF9AyGxECF3fDKmDklnatoBK3e0oBE', '', 0, 0, 18, '2010-12-01 09:49:51', '2010-12-01 09:53:06', 'd8ab7f41486b310a86db716d669fa1fc', '\n'),
(154, 'Minh', 'anhminh', 'minhanh0936127586@gmail.com', '38f463dd44d7dad8e8f0cd48be6d8538:Ur853qzlCPdbh2vC5dPlAS4iCRzNrnfn', '', 0, 0, 18, '2010-12-01 09:55:14', '2010-12-01 10:00:35', '2c510f6d3053e08af83da152acbff64c', '\n'),
(155, 'Nguyễn Hồng Loan', 'hongloan', 'hongloangmi@gmail.com', 'afcf84dd3c1044e084ddfbbfae4cef9e:ZyopQObiCaLY0c6RjkZNm983uJCGxcE0', '', 0, 0, 18, '2010-12-01 11:00:40', '2010-12-01 11:03:51', '49408969a97c3557933e70def1472ca9', '\n'),
(156, 'Thanh  Trúc', 'thanhtruc', 'hr@hiephungphat.com.vn', '315a39504f89954580cfcf5168093b37:lFILMTK69w7wkLL202J2uCDd8e7d60PT', '', 0, 0, 18, '2010-12-01 11:05:33', '2010-12-01 11:11:29', '49160a13ba3d2d6d7bc24bb8de50b038', '\n'),
(157, 'Phúc', 'aphuc', 'tuyendung@fococev.com', '40b6af9194b872977d0c598c5e4fcd82:GpJuFu6cVWyM42W1N2AwsIUyl3hyOB0d', '', 0, 0, 18, '2010-12-01 11:16:55', '2010-12-01 11:23:15', '592d800d3029887040e2beb17bdc0a80', '\n'),
(158, 'Lê Trà My', 'tramy', 'tramyle80@yahoo.com', '8c223523211b145c9390043c94ff8445:fIVbOKTSZ7n29YCMkTD3wIHSu5fVCcum', '', 0, 0, 18, '2010-12-01 11:46:28', '2010-12-01 11:51:28', '4cc711a74ba3bda71c477cfd54c3694e', '\n'),
(159, 'Hữu Lập', 'huulap', 'tuyendung.th@gmail.com', '126418a7f1588f46852d119f029e165f:XQXkRrc5WDpj0ohxK6ZK1a6jJNqgwP9J', '', 0, 0, 18, '2010-12-01 11:53:29', '2010-12-01 11:57:25', '3ca75651418339506cea7fe71429c83c', '\n'),
(160, 'Hoàng Anh', 'hoanganh', 'anh.hoang@nkidcorp.com', 'be72c0df394d67327cf4d11d9eaf5e6d:NGIpaIHVDyJFnzv34NJnrfXj9Vu1cpSb', '', 0, 0, 18, '2010-12-01 11:58:47', '2010-12-02 11:00:00', 'eaf52281dbdf702f9321c5ef87150613', '\n'),
(161, 'Thu Phương', 'thuphuong', 'tuyendung@sohacogroup.com.vn', 'd891d14d66ca2732069546396c978d08:8yHXMsZwfj2ZSIS1V7wShRdUVLPpOPuo', '', 0, 0, 18, '2010-12-01 12:31:45', '2010-12-01 12:35:04', 'a3aa5642f9f1591c52ddb361513a2e9e', '\n'),
(162, 'tran thi hoai anh', 'hoaianhtran', 'hoaitranhoaianh@gmail.com', '89ea1f0b8394c4f2b059d3b34b169a3c:OJVRxssvSC2tCPY9qjn14AkS4T8hfHQR', 'Registered', 0, 0, 18, '2010-12-01 12:40:08', '0000-00-00 00:00:00', '7fe0b48b8dcc02e69e3b23f5ff5ecc0c', '\n'),
(163, 'Lê Thị Nê', 'chine', 'lethine87@gmail.com', '0148fc6bfc650e7c5b73d3fbc84104a3:OmMraVP0DorG0gTKUfCN8ILymQZ5lGBm', '', 0, 0, 18, '2010-12-01 12:40:35', '2010-12-01 13:34:22', '651f0c577fc0e510f2c42bde0cdd93ad', '\n'),
(164, 'Nhung', 'msnhung', 'nhung.nguyen@vtijs.com', '9f7d36b33110901775b40942fe9cf05a:iPfZ7R4p5Y4Yi3S57LF4CwlM4rni0DqO', '', 0, 0, 18, '2010-12-01 13:47:30', '2010-12-01 13:51:48', 'd94e97b8ad054cf2a9b80fc04b5d70b2', '\n'),
(165, 'Lê Huỳnh Triều', 'huynhtrieu', 'tuyendungnhansu@thaituan.com.vn', '998646a19029c6e2bf228bfdbe52ac9d:PHnpOImiFzvBdn5wbv5vAe3UM5moQVQx', '', 0, 0, 18, '2010-12-01 13:55:15', '2010-12-01 13:59:34', '9e140b27c2b9160e9258ecfa15bf6f68', '\n'),
(166, 'Quyen', 'chiquyen', 'tuyendung@sunhouse.com.vn', '0288ad79c4a432c8615b137268c52033:6oz8v3Hni9XJVgvPc8nfqKAgogb6OlNb', '', 0, 0, 18, '2010-12-01 14:02:02', '2010-12-01 14:07:36', 'b0631721102b94bae1bd9fc03ec81efd', '\n'),
(167, 'Hùng', 'hung', 'dongtaymedia@gmail.com', '0dff640dca8afa932c8c3cbd9e1a02ee:HlmP7mbNn48IoaMsKY7M9G9jnrJura2y', '', 0, 0, 18, '2010-12-01 14:09:04', '2010-12-01 14:13:08', 'c439c8dd4fa7302cb636158875ac6f88', '\n'),
(168, 'Trịnh Lợi', 'trinhloi', 'trinhthiloi@anzpharma.asia', '9faec5f56e921629da93035e6e5937c4:xwBF9eyES9OX1uCz6RtdA6Nh5YPILrO6', '', 0, 0, 18, '2010-12-01 14:16:43', '2010-12-01 23:48:53', 'd140a26cbe5cbe7fccf1e270b85ace3d', '\n'),
(232, 'Tống Văn Dũng', 'dung.tongvan', 'dung.tongvan@thegioididong.com', 'eb26c4af4220fdfd019cddcae3c85552:JfqxkbzAmmqtQoLmdMYhEmibYly7XLLd', '', 0, 0, 18, '2011-01-03 05:02:35', '2011-01-03 05:04:03', '07c8a5e254ae49bc1f909b6f54c4d2c1', '\n'),
(170, 'Hảo', 'mshao', 'trieuphutre@yahoo.com.vn', '63feaeaf409917835419c5c63b206b09:ai3Pi7TI8m6axnzp9R9FPFJyxIGDvF9I', '', 0, 0, 18, '2010-12-01 14:27:25', '2010-12-01 14:31:37', 'ca0ade917ddf126b32b73a9b255d9b76', '\n'),
(171, 'Nguyễn Thị Thu', 'nguyenthithu', 'phongnhansuhn1@yahoo.com', '33145856cbf2627ae467ab81e066d019:QmbG1iQ19u3VZ7zavHOIkN29YdNpwssj', '', 0, 0, 18, '2010-12-01 14:34:19', '2010-12-01 14:40:19', '7090f9863c221f039d0cb615fe398225', '\n'),
(172, 'Nguyễn Thị Dung', 'nguyenthidung', 'nguyendung@forinconsinvest.com.vn', '364bb28eb74b2a32307a50ee375cc429:P0KkzeoPvTXYlgnV5mZ4MIHDD28wBnkz', '', 0, 0, 18, '2010-12-01 14:42:11', '2010-12-01 14:50:12', '09a0e62c6c2f209b8d51cb0e78de918f', '\n'),
(173, 'Hoàng Phú', 'hoangphu', 'contact@hoangphu.com.vn', 'dc09c09b2411eef2f15e22c4ed863c87:6CcOZa20IgrXpkGdtolrpD0qCJcwj1Th', '', 0, 0, 18, '2010-12-01 14:55:02', '2010-12-01 14:58:09', '380dc6fee545c64b7ccdeccd41f083e5', '\n'),
(174, 'Phòng HC- NS', 'PHCNS', 'nhansu@pnk.com.vn', 'c88d3a0acfef5ba57fd548fc09e1669d:qPJDCHqJkA96JV3kDMD8Fli4dTJ7QOks', '', 0, 0, 18, '2010-12-01 14:59:57', '2010-12-01 15:02:11', 'ec15cc8a8beaaad80de3327045c1387a', '\n'),
(175, 'Nhật Duy', 'nhatduy', 'duy.pham@kimhoanh.com', 'e0881c39319905d401c86870d093f13f:GBd6LXLYRKU42uTxRqTZklcA1JfFuB9i', '', 0, 0, 18, '2010-12-01 15:03:20', '2010-12-01 15:06:02', 'd104a6ea55cfd1d836922eae5e8e4918', '\n'),
(176, 'Hồng Nhung', 'hongnhung', 'nhung8668@hotmail.com', '5d874a070927a8f5117dfdf840ac4f32:F3LIcYA7Z6GMX87LsnH1DbSaD9Ns5qeH', '', 0, 0, 18, '2010-12-01 15:11:47', '2010-12-01 15:15:07', '5d60f7f26c29e69c4127521dcf49cb1a', '\n'),
(177, 'Chị Khánh', 'chikhanh', 'khanh.dang@hanel-csf.com', '38e6a347856c0c909bfd68fd198cf798:wHsVPNlLp7fMIj3OgvmwJO6G3jPTlvkV', '', 0, 0, 18, '2010-12-01 15:16:28', '2010-12-01 15:18:09', '539a6c3d6d754f6651def77be51d6ef9', '\n'),
(178, 'newstartour', 'newstartour', 'tuyendung@newstartour.com', '411d91b3ad1e5b0b34898bdf3feeb116:JgsU3zLbvZoqCItVuDMfxZDSQijaWWQD', '', 0, 0, 18, '2010-12-01 15:19:42', '2010-12-01 15:21:57', '59c6d0149970f4c3dba91b33b953e49a', '\n'),
(179, 'Quỳnh Trang', 'quynhtrang', 'thaibinhduongmedia.jsc@gmail.com', '6b33dc13b798a6dffbd1ed08bde23dc1:Bpi75NJS4VwxM7cF0mQs2u7uGCVedvCp', '', 0, 0, 18, '2010-12-01 15:23:53', '2010-12-01 15:26:36', '82a64e5988de4e37790eac1ac4185ebf', '\n'),
(180, 'Tạ Giang', 'tagiang', 'tuyendung@pti.edu.vn', '11be1a092175a5913f6c7a9b40fc8091:LOYjVlvc3bU7EpSk7PXQWENYO1yS887T', '', 0, 0, 18, '2010-12-01 15:31:36', '2010-12-08 00:18:44', '8f2a1fa12c0916d3e18da767298db584', '\n'),
(181, 'Thắng', 'thang', 'thanhnga140288@gmail.com', 'da1ce99dd4dbca71a34d2ac8bd93294b:FBCZcchKpqhBVUYZY5c0PtrZW4MNcSkW', '', 0, 0, 18, '2010-12-01 19:10:17', '2010-12-01 19:13:18', 'e3a768111418a02e1f1f35bf36039187', '\n'),
(182, 'Diễm', 'diem', 'info@mango.vn', '9f2354bd90210b146aca155b6762d9f5:Hx0EUkboX9GYVH3l9jGc1RNGlbmOrBfk', '', 0, 0, 18, '2010-12-01 19:14:48', '2010-12-01 19:17:31', '7c3316b5b9d5e15f4e1e9c07c6139122', '\n'),
(183, 'Tùng', 'director', 'thanhtungsshn@gmail.com', '3f0253c224997ee2514674ed94de20f3:xaZ5TGXx7CGpQrbgBJigUUSfdpMnommZ', 'Super Administrator', 0, 1, 25, '2010-12-01 19:18:51', '2010-12-13 01:00:35', 'a9ea31fdd739c1dc34a9e8b211722fcf', 'admin_language=\nlanguage=\neditor=\nhelpsite=\ntimezone=7\n\n'),
(184, 'Hồng Anh', 'honganh', 'info@hydraulics.vn', 'f2b255c272423128a910dc011fbfcc23:PCqgN5nAda307UM8dY2Xn34rXOLjQc7q', '', 0, 0, 18, '2010-12-02 08:18:11', '2010-12-02 08:22:27', '70cd72eff6c07dd0dc463cd09cb9ef19', '\n'),
(185, 'Hanowindow', 'Hanowindow', 'human@hanowindow.com', '63ef0fc8e9a3a7df629292d0f9e38146:jSHZzlgxcLntJVI4dMdybXxWFoylVh7X', '', 0, 0, 18, '2010-12-02 08:27:59', '2010-12-02 08:32:19', '65eb590aa72cfb1fd9f9d064a847302e', '\n'),
(186, 'Phạm Hoài Vũ ', 'hoaivu', 'sales@minhvietdoor.com', '48fd1f81bb05de0e5d074f9a9a239931:SCevANkVNzGTbB0kmAVoKTkVpWGvmJr7', '', 0, 0, 18, '2010-12-02 08:34:09', '2010-12-02 08:40:31', 'bde65f47e18029fbd46c4d9c62aeba0b', '\n'),
(187, 'Hà Văn Lưu ', 'dominhchien', 'dominhchien@cic.edu.vn', '826ee00a1dbf9caa93d3849fcbfb5c72:8CCwHPumQOKMn3HZ63sJRb0K0H06ogQx', '', 0, 0, 18, '2010-12-02 08:49:52', '2010-12-02 08:53:01', '90227ec479f1098c76e809e29606d6d1', '\n'),
(188, ' Đặng Thành Tung', 'raiarc', 'tuyendung@raiarc.vn', '0d49f26f4f9dc6d78a3d069aa30925e9:mHphfpfzqZY5szHuSWLXmTP4vRLwJdot', '', 0, 0, 18, '2010-12-02 08:55:13', '2010-12-02 09:00:02', '7bd0a9f535c4c941646adfed9835afcf', '\n'),
(189, 'Mr Hải ', 'mrhai', 'hr@hoasao.vn', '7321adbc827de54646c74f1e969115f8:7qLcbd9rtyJpv604vWyPmvtpa0vZie7q', '', 0, 0, 18, '2010-12-02 08:58:19', '2010-12-02 22:40:24', '8d534682084eceb042c6400946593348', '\n'),
(196, 'Trịnh Trần Ngọc Trang', 'ngoctrang', 'info@quangcaosucsong.com.vn', 'e3e9d1f440c89959ac0be4d0784ea7f1:lvIjsB3oaNBrVOvVBrUVa8BIfia3nBbE', '', 0, 0, 18, '2010-12-04 03:10:42', '2010-12-04 03:15:44', '79a93d7e78e67887dea1e47e782280e1', '\n'),
(193, 'thaison', 'thaison', 'thaison01@gmail.com', 'c26e3b5c7c222f0de220308b765efaea:ZLm822wv6SnexxlpvS8Z6rhy44btbMUr', '', 0, 0, 18, '2010-12-04 02:38:25', '0000-00-00 00:00:00', '', '\n'),
(222, 'Đặng Ngọc Hà', 'ngocha.vietbac', 'ngocha.vietbac@gmail.com', 'f3cfb070d716e42d4eb3c15a9706396a:nZZ59r99rVUN10cJvBP1wT3O3HEvZQ1x', '', 0, 0, 18, '2010-12-21 23:47:52', '2010-12-22 00:07:17', '777677bc661d744bc2aa4249ace80529', '\n'),
(197, 'Quyết', 'quyet', 'daongocquyet3388@gmail.com', '380d008ac34794f65790eb62e36abfe7:CDybXnUGEVNnmb9jGzeNB2VoWJ5EPZ96', '', 0, 0, 18, '2010-12-04 03:17:43', '2010-12-04 03:21:21', '91c2c4d32608ae8df922e3776c2d4f6b', '\n'),
(203, 'Nguyễn Thị Kim Nhung', 'kimnhung', 'kimnhung@gmail.com', '64e9e10ec546b393a85b78ffe2d69a28:u2IQZ5HM3hFQbo2Gda6njpmkQFP0brhN', '', 0, 0, 18, '2010-12-04 06:04:04', '2010-12-04 06:07:08', 'da98f3eda27bd65cdaa6ece7203a38ec', '\n'),
(204, 'Nguyễn Thị Thương', 'nguyenthithuong', 'nguyenthithuong@gmail.com', 'e88679b538d2b307bc128de4e6dbacb5:BOWPzcZKFM2y8LiY2dLAEwTjxDg64Ddb', '', 0, 0, 18, '2010-12-04 06:13:56', '2010-12-04 06:17:58', '8fb5e57c289fa02c04aa980ac85d8366', '\n'),
(205, 'Trần Loan', 'tranloan', 'tranloan@gmail.com', '5e7fb929e8da2804ca8bcffb8d552ce1:LrtBCUtH4wC0QIdxi9Lb00cysfMw7Bh6', '', 0, 0, 18, '2010-12-04 06:19:08', '2010-12-04 06:35:49', '089ff8859acd1ce08d46a4b17aacfe39', '\n'),
(206, 'Phạm Thanh Huyền', 'thanhhuyen', 'thanhhuyen@yahoo.com', 'b05207cca345bbf172a1f37fc1982dde:q4BXwb2y9Cknai8tipcJBFJp9MVz6zJ7', '', 0, 0, 18, '2010-12-04 06:44:37', '2010-12-04 06:45:27', '40bc57b06286ca17705fe95a301e1d53', '\n'),
(207, 'Thanh Lâm', 'thanhlam', 'haidang@thv.com.vn', '8e0bf78bce4ad3b7290d6887e50e5949:77Q5ULkD3MmcnhqAqsxgQGWXXt7KH3xM', '', 0, 0, 18, '2010-12-06 13:35:47', '2010-12-06 13:47:23', 'e38b2165358c3f67851ac888e7ee3d49', '\n'),
(208, ' Nguyễn Hồng Anh Khá', 'anhkha', 'khanha@callcenter.vn', '83bb20a78f2dde6b812b7185d393fb31:kNFTydiHgfRmiFG9r1bPgkQTXM2Tps1C', '', 0, 0, 18, '2010-12-06 15:04:26', '2010-12-06 15:09:17', '9bfc0ba91bac028d2b1627e16bfc8896', '\n'),
(212, 'thanhtd-creativevietnam', 'thanhtd', 'thanhtd@info.com', 'a5a9e9a237118ac3401244dc19a8a578:fxQwIba148Ihuhf4kPuPVSt7VlsdUa99', 'Super Administrator', 0, 0, 25, '2010-12-07 00:33:59', '2011-03-09 09:24:03', '', 'admin_language=\nlanguage=\neditor=\nhelpsite=\ntimezone=0\n\n'),
(62, 'Super Administrator', 'admin', 'thanhtd1986@gmail.com', 'a240050b27da2db786b923c75fb84e28:MiiDPmfrau1lCnvB1gXe1hvDv124vY5B', 'Super Administrator', 0, 1, 25, '2010-12-06 17:12:56', '2011-03-09 09:30:45', 'a9ea31fdd739c1dc34a9e8b211722fcf', 'admin_language=\nlanguage=\neditor=\nhelpsite=\ntimezone=7\n\n'),
(213, 'Nguyễn Thu Hà', 'nguyenthuha', 'ngha87@gmail.com', '39c07027fc46814f0b2ca406a20e929c:Jq9S02rKCBaiIc8KBN1DejpyPRVRSPN1', '', 0, 0, 18, '2010-12-07 07:17:33', '2010-12-07 07:27:25', 'e8922854b0b7647ab1cf279c9b08bcfa', '\n'),
(214, 'Lê Dưỡng', 'leduong', 'leduong@yahoo.com', 'd8e7f26b222ea2d22b7b69ffbcba6e7f:G0CyLwPMZMsXDRFSCjOCOLo3z3bL0RGv', '', 0, 0, 18, '2010-12-07 07:30:28', '2010-12-07 07:30:59', 'aeab3118e4cc256155d20384644e76b8', '\n'),
(215, 'thuhang22', 'thuhang22', 'thuhang2@fdfd.com', 'dc9d7044856e03cd9d9c4c24e2deec2b:0D8QGlw1iKpftj5DydwaiB2oS0SAsDgG', '', 0, 0, 18, '2010-12-08 06:51:55', '2010-12-09 02:16:24', '7a069ad26c1a0d47927482b3f333942e', '\n'),
(216, 'nhathongco', 'nhathongco', 'nhathongco@yahoo.com.vn', '42a0917d2b3c6c660de5be62b0879861:RBH4rBDzs6YnUCXtDRFqUXi5fU3vALZg', '', 0, 0, 18, '2010-12-09 07:51:21', '2010-12-09 08:07:50', '2a78a3add4d31d515152ba5b4bbee28d', '\n'),
(217, 'mediapro', 'mediapro', 'mediapro@yahoo.com', '704c02246f57bf9c9837f5033ab5aa6e:Wsoj7SA9htlFp2I8ndjPrPrXBH1hUaCf', '', 0, 0, 18, '2010-12-09 08:10:15', '2010-12-09 08:11:05', 'f2809df66a9dc660c87db60534e4819d', '\n'),
(221, 'hanchip', 'hanchip', 'hanchip@yahoo.com', '3fc170acc9cbf566c991796566bb6565:iBNOiPbgFTAAAvo9p01lwIhYCb4TXzXs', '', 0, 0, 18, '2010-12-16 07:25:09', '2010-12-22 09:26:41', '2f8ced0d6e03e1e7bdefdaf27e7fcd26', '\n'),
(233, 'Quản trị Viên', 'quantrivien', 'admin@vieclamviec.com.vn', 'a6f6b4365ec2dba600ed4c4b4548be77:HuUIewdJmcaFjVg5Mo7GmcOsmPe4cjyq', 'Super Administrator', 0, 0, 25, '2011-01-06 02:45:05', '2011-03-08 08:03:50', '', 'admin_language=\nlanguage=\neditor=\nhelpsite=\ntimezone=7\n\n'),
(236, 'Trung', 'nguyenthanhtrung', 'longltaptech@yahoo.com', '6b7b4310cce07e0635e8f77ecc89c3f2:M97wLj2mZetXtcR9FPxr6HFw1iC4xMxL', '', 0, 0, 18, '2011-01-14 13:48:32', '2011-01-14 13:57:23', '', '\n'),
(237, 'Thanh', 'thanhtrung1', 'huulong2003@yahoo.com', '1383ec6dbcbed9a5a674569c2901d511:BmfGopBn9kBtxY0WYcai8o3SMDD8U92Q', '', 0, 0, 18, '2011-01-14 13:54:47', '2011-01-14 13:57:15', '', '\n'),
(238, 'Trueman', '123456', 'phile@philong.info', '3780e55b125bb04c141e768ebdb3e735:32kuGJkcAehZD4du960pICilzSgkApSY', '', 0, 0, 18, '2011-01-18 03:52:03', '2011-01-18 03:53:40', '', '\n'),
(239, 'thanh nhàn', 'nhanthanh', 'nhanthanh00@gmail.com', '4b6217ef02b2c585ea4cbc0d06bd11f8:WGRUnSqDvKoStgM6flCqTOGUtCYXT9nr', '', 0, 0, 18, '2011-01-18 04:28:42', '2011-01-20 02:44:27', '', '\n'),
(240, 'nhàn', 'nhannguyen', 'hoamacco9587@yahoo.com.vn', 'fb6f05b31686cf12774fcc965ea4eaf7:EYIdRNEFyS94vauprwTZZvmWbjdmgTe6', '', 0, 0, 18, '2011-01-19 05:13:21', '0000-00-00 00:00:00', '7b69fce8ed7ac5337b6d8ce1dd50bfe2', '\n'),
(241, 'ngan hà', 'nhannguyen87', 'hoamacco9587@yahoo.com', '067b9ad2db71351d0886732f66d799c5:ruhEy8wuoBjSAaK2ea3TJcLGiz0uqGaM', '', 0, 0, 18, '2011-01-19 05:21:25', '0000-00-00 00:00:00', 'fe253e339364ec52e3d7f09b2ee56412', '\n'),
(242, 'thuhang.mnc', 'thuhang.mnc', 'thuhang.mnc@gmail.com', 'ad662daf1fa2a60e9c7d6a0a995bc295:drBnj9IJYcDdVmhiS5BewMZW9qVF3CjY', '', 0, 0, 18, '2011-01-19 06:52:08', '2011-01-20 03:57:40', '', '\n'),
(243, 'Nguyễn Thanh Tân', 'nguyenthanhtankt', 'nguyenthanhtankt@yahoo.com', '2471c4f810f018a0cee2e160bbd64f31:hC8zWvnaFWiMCB0iI7WAdMj71pILZaX8', '', 0, 0, 18, '2011-01-20 04:57:48', '2011-01-20 05:01:31', '', '\n'),
(244, 'Phi', 'kiemviec', 'philongcomputing@gmail.com', 'cb222c18b43ed4b96961249229818153:xqeOfbIwMWmsuOzP22dUoDDqWZaVEmTG', 'Registered', 0, 0, 18, '2011-01-20 05:50:40', '2011-01-24 01:32:22', '', 'admin_language=vi-VN\nlanguage=vi-VN\neditor=\nhelpsite=\ntimezone=0\n\n'),
(245, 'Anh Ngọc', 'ngocla', 'ngocla@facom.com.vn', '6fb6e0fbe0a423ee5a4309500982dd43:dfPCjVfjoZtTQ0sTzDaiQPN6AnkMqZAH', '', 0, 0, 18, '2011-01-24 06:40:30', '2011-01-24 07:02:30', 'ea6978c680933a7bf112c2be7dfd9a50', '\n'),
(246, 'Anh Đào', 'AnhDao', 'dao.hrvietnam@gmail.com', 'b683ed5a94bf9a0b8b694077c8b7da96:jfYL0m2NXken0Eqh5Ll88P2kDmTifttG', '', 0, 0, 18, '2011-01-24 07:44:55', '2011-01-25 01:50:12', 'a39a6c7878cbb631f28a584f1844cf66', '\n'),
(247, 'Quy', 'LyDinhQuy', 'sanvang.ngoaihoi@gmail.com', '145a69d16a521559f2c2b577089b44ef:IZynVXMvgSh4lPFjL1E98cKJ60JweUDk', '', 0, 0, 18, '2011-01-24 07:49:25', '0000-00-00 00:00:00', '75785b7f1e0a84c22921c6a6513533e5', '\n'),
(248, 'Hieu', 'Hieu', 'dohoang@blook.com.vn', '4fae370e282bfa16fc099446e8e610b1:YwnLV6L4uomYzG8LXZahlcUnJAMwqr9R', '', 0, 0, 18, '2011-01-24 07:53:10', '0000-00-00 00:00:00', '3debdd72c9ce5a74cf4db7f361c3b1b6', '\n'),
(249, 'Phạm Thị Bích Vân', 'phamthibichvan', 'vanbaysao@gmail.com', 'c5042f5a50a32c3b5d20d7f4ecfd973c:V2XHzWyBWrPopxiHX2FJyANvVeVEpMpC', '', 0, 0, 18, '2011-01-24 07:56:49', '0000-00-00 00:00:00', '3e9fb33270a0bda0dae17cc4cba9b671', '\n'),
(250, '	Nguyễn Duy Khánh', 'nguyenduykhanh', 'khanh_jo@pungkookvn.com', '141ed6f0c040cc700e99d552bbf68e7e:eAFdGuJXyjwuAcptlb72VluzTDEWYLrW', '', 0, 0, 18, '2011-01-24 08:02:25', '2011-01-24 22:49:22', 'b5181ab4b5dd9e407f98c56268417803', '\n'),
(251, 'Ms Hà', 'MsHa', 'tuyendungdanang@hoasao.vn', '83129a3ac53fc9ce6f88af47c65d4127:OdcDnSSyUDioVrjKIVURZr6RUCIwntGz', '', 0, 0, 18, '2011-01-24 08:45:24', '0000-00-00 00:00:00', '78a3bbcb782f2255c7ac28042ed382db', '\n'),
(252, 'mr Cong', 'ngatrinh', 'ngatrinh2107@yahoo.com', '16250e12ebf16578e2b902b4b0b3d554:Rf9P1zQdltnsNyaQ8C9qhGPNZ1AAjw5g', '', 0, 0, 18, '2011-01-24 23:40:17', '0000-00-00 00:00:00', 'b7a2af55bc20c97cfa1d4d3123e9d08d', '\n'),
(253, 'Hoàn', 'tuyendung4', 'tuyendung4@hoasao.vn', 'f58d359591f4e448fa01c9f2c354318e:6NwtKLOZb2CkuvPyrS9b2u08wNG86Tl0', '', 0, 0, 18, '2011-01-24 23:47:56', '2011-01-25 00:15:02', '671c6520b8795bfe09794ea36d3e6c9e', '\n'),
(254, 'Nguyễn Hà Long', 'september.mobi', 'september.mobi@gmail.com', 'f673c40ef5e614dfaeb631352f131605:CF6vTpvXWDXjylDPJ6vrOsziLPwZAxsV', '', 1, 0, 18, '2011-01-25 00:22:35', '0000-00-00 00:00:00', 'b4d35583d02de77dc881e5f4fce4c840', '\n'),
(255, 'Hoàn', 'Hoan', 'tuyendung2@hoasao.vn', '19784312f071b60dc3945994e1c90d21:pJfoaAsFrQJsyN48tzE9JC8am3g67rKZ', '', 1, 0, 18, '2011-01-25 00:32:23', '0000-00-00 00:00:00', '40126cffefad201bba026c03ff8ef6b7', '\n'),
(256, 'Bùi Văn Tân', 'buivantan', 'Tan.bui@bkloils.com', '1bc4b5702602d9dd2d23a5083e0e518d:KYhLGawgr3oUXYwBtZsYfc6fzYIyxn8E', '', 1, 0, 18, '2011-01-25 01:47:31', '0000-00-00 00:00:00', '9a1639f1811b7225755e6bc48bc08682', '\n'),
(257, 'Công ty cổ phần tư vấn toàn câu Vina - Globavina., JSC', 'globavina.hrd', 'globavina.hrd@gmail.com', '0358f0159c910f1a9c1f84655fda9be3:iwjm6xMLuRYDfi0WB8DCwK8LV2lu5pjB', '', 1, 0, 18, '2011-01-25 02:03:44', '0000-00-00 00:00:00', 'a6235aad2244a14091c4bfad8279bbd1', '\n'),
(258, 'Nguyễn MInh Tú ', 'nguyenminhtu', 'chiendung09@gmail.com', 'ab56f1f3740debe4a502ec9600b48fe7:dQGtE0HaB3nRIA1jr8viSFTkZvm7KR8a', '', 0, 0, 18, '2011-01-25 02:16:55', '0000-00-00 00:00:00', '', '\n'),
(259, 'Nguyễn Thị Thu Hà', 'nguyenthithuha', 'nguyenthuha@evitco.com', '3cada0231ea321e91fd847b22b213877:GlKIdP127ASOraJr43wha7GYarqPnSm6', '', 1, 0, 18, '2011-01-25 02:20:42', '0000-00-00 00:00:00', '1be3defec233caf6054a1e99d8a966da', '\n'),
(260, 'Bộ Phận Tuyển Dụng', 'tuyendungvietthai', 'tuyendungvietthai@gmail.com', '953597f3ee0a2de2319aff732fc206c2:y3AXmiUsc9QDjRHC5NkjsMkr4udxh3xF', '', 1, 0, 18, '2011-01-25 02:24:18', '0000-00-00 00:00:00', '041ec96981da482fa51ea896fb3f90ba', '\n'),
(261, 'Nguyễn Hữu Đông', 'nguyenhuudong', 'dongdl.vietlinks.jsc@gmail.com', '6d25358b9e716276a51f6001d00d8d86:SmkzAUriaN05QCrEv15NYD7Xcyjqzzj3', '', 1, 0, 18, '2011-01-25 02:28:50', '0000-00-00 00:00:00', '8816a646b0476d3c95506c145efe09fe', '\n'),
(262, 'Anh Thành', 'AnhThanh', 'giaucovavehuusom12@yahoo.com', '51b7a0e9cf7b907ec21ef01e4c0b2c63:xQ6Hlcfzea0ZjaO0gjiyCOoW6uRS908w', '', 1, 0, 18, '2011-01-25 03:01:33', '0000-00-00 00:00:00', '524d5293f1ab6223f5f7196ab3df1661', '\n'),
(263, '	CHỊ THU', 'ChiThu', 'thuytien.tuyendung@gmail.com', '1e86ac8ff642776e823c9189314bc2be:vmheLpIf2Q3W9Y0qdDtFawuFRNhkjbcC', '', 1, 0, 18, '2011-01-25 03:04:25', '0000-00-00 00:00:00', 'fe39aa7de90e9dc9240eb82673a61fd1', '\n'),
(264, 'Tham', 'thamtth', 'thamtth@vietnamit.vn', '7e5a4acbfccec941f1a63d54be879494:UK62mam0nshMa0JZsIEoYRMXjCSALAUG', '', 0, 0, 18, '2011-01-25 03:55:03', '2011-01-25 04:55:32', '', '\n'),
(265, 'Đỗ Hồng Phước', 'dohongphuoc', 'phuocdh@bestbuy.com.vn', '16a2e970a457becf722229409bc287a6:wy4PeLV3KwHDCssLrv9G5mZiXF8XvNZO', '', 0, 0, 18, '2011-01-25 03:58:12', '2011-01-25 07:26:34', '', '\n'),
(266, '	Lê Quang Dũng', 'Lequangdung', 'dung.lequang@ducnhan.com', '12ced36a595ce5f18880babe16b9239c:rgR8e658to2jGLHcz3oxaznUMm3AJaHh', '', 1, 0, 18, '2011-01-25 04:02:26', '0000-00-00 00:00:00', 'f72757ff6159f26259eb3007584b2705', '\n'),
(267, '	Nguyễn Thành Long', 'Nguyenthanhlong', 'admin.hpg@safi.com.vn', '8004125c70eacce8c70a4dd727c960aa:rHgxwaUdw3jb6ptGm5o3yfJFy06TYclH', '', 0, 0, 18, '2011-01-25 04:21:42', '0000-00-00 00:00:00', '', '\n'),
(268, 'Giang', 'Giang', 'tuyendung@niags.com.vn', 'f042e39db913ffbb566bf65514d07b9c:Jw2r0KCTSUnwkfbkUjT5YBLx1Y4BuooJ', '', 1, 0, 18, '2011-01-25 04:31:22', '0000-00-00 00:00:00', '4e8c6a66afba9a70f2c89a46a0918a15', '\n'),
(269, '	Thu Trang', 'thutrang', 'trangdt@sitc-dinhvu.com', '580506dd6056ae7a6eb6a74cbbf53b5e:5bo6WVU9tc2f11plO9Mlcr6iflG4UbyH', '', 1, 0, 18, '2011-01-25 04:42:30', '0000-00-00 00:00:00', '8ba52ed6944fb04da881a143ee1d5482', '\n'),
(270, 'thanhtd3', 'thanhtd3', 'thanhtd3@gmail.com', 'ba4fe36eeafd77fff593deb75d0d6e62:3wFLl9tKKqwZIEEFJ5WaGpAWXNBf6Dcn', 'Registered', 0, 0, 18, '2011-02-15 03:51:43', '2011-02-18 03:50:23', 'd9fa57a9412be796c27b49f26e137ce0', 'page_title=Thay Đổi Thông Số Cơ Bản\nshow_page_title=1\n\n'),
(271, '1111', 'dfdfdfd', 'thanhtd198346@gmail.com', '9e5a081e8b34b49d538d2b90a5b5d64f:3qDqyNVEwnkBSF8ciyp7WF5zg5cVkbhD', '', 1, 0, 18, '2011-02-18 07:46:01', '0000-00-00 00:00:00', '0fbdba4ef88f0024759caeb924ab7ae4', '\n'),
(273, 'son test', 'sonnt', 'sonnt@creativevietnam.vn', '402adeb603e54feff46d28f5b3c75226:oWo1TLnBmcGWJjr9rUVV0ljDxSnUD0xl', '', 0, 0, 18, '2011-02-19 01:29:16', '2011-03-01 02:42:48', '67d5b24134b8a72617f1cff895eeffb1', '\n');

-- --------------------------------------------------------

--
-- Table structure for table `jos_users_test`
--

CREATE TABLE IF NOT EXISTS `jos_users_test` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `username` varchar(150) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `password` varchar(100) NOT NULL default '',
  `usertype` varchar(25) NOT NULL default '',
  `block` tinyint(4) NOT NULL default '0',
  `sendEmail` tinyint(4) default '0',
  `gid` tinyint(3) unsigned NOT NULL default '1',
  `registerDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL default '',
  `params` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `usertype` (`usertype`),
  KEY `idx_name` (`name`),
  KEY `gid_block` (`gid`,`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `jos_users_test`
--

INSERT INTO `jos_users_test` (`id`, `name`, `username`, `email`, `password`, `usertype`, `block`, `sendEmail`, `gid`, `registerDate`, `lastvisitDate`, `activation`, `params`) VALUES
(62, 'Administrator', 'admin', 'thanhtd1986@gmail.com', 'bffc277fe2a98d2da4631c5c82d4db78:M9lTOXrn3PkC6bFB13t2Aq0oQiIpQa4y', 'Super Administrator', 0, 1, 25, '2010-12-06 17:12:56', '2010-12-07 00:14:11', '', ''),
(63, 'Administrator', 'admin', 'thanhtd1986@gmail.com', 'bffc277fe2a98d2da4631c5c82d4db78:M9lTOXrn3PkC6bFB13t2Aq0oQiIpQa4y', 'Super Administrator', 0, 1, 25, '2010-12-06 17:12:56', '2010-12-07 00:14:11', '', ''),
(64, 'Administrator', 'admin', 'thanhtd1986@gmail.com', 'bffc277fe2a98d2da4631c5c82d4db78:M9lTOXrn3PkC6bFB13t2Aq0oQiIpQa4y', 'Super Administrator', 0, 1, 25, '2010-12-06 17:12:56', '2010-12-07 00:14:11', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_weblinks`
--

CREATE TABLE IF NOT EXISTS `jos_weblinks` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `catid` int(11) NOT NULL default '0',
  `sid` int(11) NOT NULL default '0',
  `title` varchar(250) NOT NULL default '',
  `alias` varchar(255) NOT NULL default '',
  `url` varchar(250) NOT NULL default '',
  `description` text NOT NULL,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `hits` int(11) NOT NULL default '0',
  `published` tinyint(1) NOT NULL default '0',
  `checked_out` int(11) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL default '0',
  `archived` tinyint(1) NOT NULL default '0',
  `approved` tinyint(1) NOT NULL default '1',
  `params` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `catid` (`catid`,`published`,`archived`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `jos_weblinks`
--

INSERT INTO `jos_weblinks` (`id`, `catid`, `sid`, `title`, `alias`, `url`, `description`, `date`, `hits`, `published`, `checked_out`, `checked_out_time`, `ordering`, `archived`, `approved`, `params`) VALUES
(1, 2, 0, 'Trang chủ', 'trang-chu', 'http://www.joomla.org', 'Home of Joomla!', '2010-12-18 02:21:55', 6, 1, 0, '0000-00-00 00:00:00', 1, 0, 1, 'target=0\n\n'),
(2, 2, 0, 'php.net', 'php', 'http://www.php.net', 'The language that Joomla! is developed in', '2004-07-07 11:33:24', 8, 1, 0, '0000-00-00 00:00:00', 3, 0, 1, ''),
(3, 2, 0, 'MySQL', 'mysql', 'http://www.mysql.com', 'The database that Joomla! uses', '2004-07-07 10:18:31', 3, 1, 0, '0000-00-00 00:00:00', 5, 0, 1, ''),
(4, 2, 0, 'OpenSourceMatters', 'opensourcematters', 'http://www.opensourcematters.org', 'Home of OSM', '2005-02-14 15:19:02', 13, 1, 0, '0000-00-00 00:00:00', 2, 0, 1, 'target=0'),
(5, 2, 0, 'Joomla! - Forums', 'joomla-forums', 'http://forum.joomla.org', 'Joomla! Forums', '2005-02-14 15:19:02', 6, 1, 0, '0000-00-00 00:00:00', 4, 0, 1, 'target=0'),
(6, 2, 0, 'Ohloh Tracking of Joomla!', 'ohloh-tracking-of-joomla', 'http://www.ohloh.net/projects/20', 'Objective reports from Ohloh about Joomla''s development activity. Joomla! has some star developers with serious kudos.', '2007-07-19 09:28:31', 3, 1, 0, '0000-00-00 00:00:00', 6, 0, 1, 'target=0\n\n');
