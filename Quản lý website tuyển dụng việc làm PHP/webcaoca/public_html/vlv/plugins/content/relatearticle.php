<?php

/**

 * @version		$Id: relatearticle.php 10497 2008-07-03 16:36:12Z ircmaxell $

 * @package		Joomla

 * @subpackage	Content

 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.

 * @license		GNU/GPL, see LICENSE.php

 * Joomla! is free software. This version may have been modified pursuant

 * to the GNU General Public License, and as distributed it includes or

 * is derivative of works licensed under the GNU General Public License or

 * other free or open source software licenses.

 * See COPYRIGHT.php for copyright notices and details.

**/



// Check to ensure this file is included in Joomla!

defined( '_JEXEC' ) or die( 'Restricted access' );

$plugin =& JPluginHelper::getPlugin('content', 'relatearticle');

$pluginParams = new JParameter($plugin->params);

$onAction = $pluginParams->get('act', 'onPrepareContent');

$mainframe->registerEvent( $onAction, 'pluginContentRelateArticle' );



function pluginContentRelateArticle( &$article, &$params ){

	$plugin =& JPluginHelper::getPlugin('content', 'relatearticle');

	$pluginParams = new JParameter($plugin->params);

	$title_text		= $pluginParams->get('title_text', '');

	$showdate		= $pluginParams->get('showdate', 1);

	$dateposition	= $pluginParams->get('dateposition', 1);

	$article_num	= $pluginParams->get('article_num', 10);

	$dateformat		= $pluginParams->get('dateformat', '%d-%m-%Y');

	$datetype		= $pluginParams->get('datetype', 'created');

	

	$other_articles = contentRelateArticle_getTitle($article, $article_num, $dateformat);

	if($other_articles) {

		$text = '<div class="pluginContenRelateArticle"><h3>'.$title_text.'</h3><ul>';

		foreach( $other_articles as $other_article ){		

			$link = JRoute::_('index.php?option=com_content&amp;view=article&amp;id='.$other_article->id.'&amp;catid='.$other_article->catid);

			$date = ($showdate==1) ? ($other_article->$datetype) : '';

			if($dateposition==1) {

				// hien thi ngay thang o phia truoc tieu de

				$text.= '<li><span class="pluginContenRelateArticleDate">('.$date.')</span>&nbsp;'. JHTML::link($link, $other_article->title).'</li>';

			} else {

				// hien thi ngay thang o phia sau tieu de

				$text.= '<li>'. JHTML::link($link, $other_article->title).'&nbsp;<span class="pluginContenRelateArticleDate">('.$date.')</span></li>';

			}

			

		}

		$text.= '</ul></div>';

		$article->text.=  $text;

	}

}

function contentRelateArticle_getTitle($article, $article_num, $dateformat)

{

	$other_articles = array();



	$db =& JFactory::getDBO();

	$query = 'SELECT id, title, DATE_FORMAT(created, "'.$dateformat.'") created, DATE_FORMAT(modified, "'.$dateformat.'") modified FROM #__content WHERE catid='.$article->catid.' AND sectionid='.$article->sectionid.' AND id!='.$article->id.' AND state = 1 ORDER BY ordering LIMIT '.$article_num;

	$db->setQuery($query);

	$other_articles = $db->loadObjectList();

	if($other_articles){

		return $other_articles;

	} else {

		return false;

	}

}

?>