<?php
// ensure a valid entry point
defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.controller');
jimport('joomla.application.helper');
//require_once(JApplicationHelper::getPath('html'));
JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_properties'.DS.'tables');

//JTable::addIncludePath( JPATH_COMPONENT_ADMINISTRATOR.DS.'tables' );
$id 	= JRequest::getVar('id', 0, 'get', 'int');
class JLORDCOREControllerEmploypost extends JController
{
	function __construct()
	{
		parent::__construct();
	}
	function display()
	{
		global $mainframe;
		$cid 	= JRequest::getVar('cid', array(0), 'post', 'array');
		$user 	= & JFactory::getUser(); 
		JArrayHelper::toInteger($cid, array(0));
		$mName 		= 'employpost';	
		$vName 		= 'employpost';	
		$vLayout 	= 'default';
		$option 	= JRequest::getVar('option');	
		$task = JRequest::getVar('task');
		switch ($task){
			case 'add':	
			case 'edit':
				JRequest::setVar( 'hidemainmenu', 1 );				
				$vLayout = 'form';				
				break;
			case 'save':
			case 'apply':
				$this->saveemploypost();
				break;
			case 'unpublish':
			case 'publish':
				$this->publish();
				break;			
			case 'remove':
				$this->removePost();
				break;
			case 'cancel':
				$this->cancelpost();
				break;	
			default:
				$vName 		= 'employpost';	
				$vLayout 	= 'default';							
		}
		
		$document 	= JFactory::getDocument();
		$vType 		= $document->getType();		
		$view = $this->getView($vName, $vType);
		if ($model 	= &$this->getModel($mName)) {
			$view->setModel($model, true);
		}
		$view->setLayout($vLayout);		
		$view->display();		
	}
	function publish(){	
		$cid		= JRequest::getVar( 'cid', array(), '', 'array' );
		$this->publish	= ( $this->getTask() == 'publish' ? 1 : 0 );		
	
		JArrayHelper::toInteger($cid);
		if (count( $cid ) < 1)		{
			$action = $publish ? 'publish' : 'unpublish';		
			JError::raiseError(500, JText::_( 'Select an item to' .$action, true ) );
		}
		$this->cids = implode( ',', $cid );
		
		$query = 'UPDATE #__properties_products'
		. ' SET published = ' . (int) $this->publish
		. ' WHERE id IN ( '. $this->cids .' )'		
		;    
		
		$db 	=& JFactory::getDBO();
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}
		$link = 'index.php?option=com_properties&view=employpost';
		$this->setRedirect($link, $msg);		
	}
	function removePost(){
		global $mainframe;
		$cid 	= JRequest::getVar('cid', array(), '', 'array');
		$db 	= &JFactory::getDBO();
		$t_cid 	= count($cid);  
		//exit($cid[0]." = 0");
		if($t_cid){
			$cids 	= implode(',', $cid);
			$query 	= "DELETE FROM `#__properties_products` WHERE `id` IN ($cids)";
			$db->setQuery($query);
			$msg = "Deleted ($t_cid) items";
			if (!$db->query()) {
				echo "<script> alert('".$db->getErrorMsg()."');
				window.history.go(-1); </script>\n";
			}
		}
		$mainframe->redirect('index.php?option=com_properties&view=employpost',$msg);
	}
	function cancelpost(){
		global $mainframe;
		$db 	= &JFactory::getDBO();
		$option = JRequest::getVar('option');
		// Initialize variables
		$mainframe->redirect('index.php?option='.$option.'&view=employpost');
	}
	//send mail
	function char_makerand ()
	{
		$charset 	 = "abcdefghijklmnopqrstuvwxyz";
		$charset 	.= "0123456789";
		$char = $charset[(mt_rand( 0,(strlen( $charset )-1)) )];
		return $char;
	}
	
	function str_makerand()
	{
	  	$key = "";
	  	for ( $i=0; $i < 50; $i++ ) {
	   		$key .= char_makerand();
	  	}
	  	return $key;
	}
	function saveemploypost(){
		global $mainframe;
		JRequest::checkToken() or jexit('Invalid Token');
		$db		=& JFactory::getDBO();
		$row	=& JTable::getInstance('products', 'Table');
		$post   = JRequest::get('post');
		
		$db 	=& JFactory::getDBO();
		$text = JRequest::getVar( 'text', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$text		= str_replace( '<br>', '<br />', $text );		
		$post['text'] = $text;

		$description = JRequest::getVar( 'description', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$description		= str_replace( '<br>', '<br />', $description );		
		$post['description'] = $description;

		$userid = JFactory::getUser();
		//echo $userid->get('gid');
		$post['agent_id'] = $userid->get('id');
		if (!$row->bind( $post )) {
		JError::raiseError(500, $row->getError() );
		}
		if (!$row->check()) {
			JError::raiseError(500, $row->getError() );
		}
		
		// save the changes
		if (!$row->store()) {
			JError::raiseError(500, $row->getError() );
			
		}
		$row->checkin();
		switch (JRequest::getCmd( 'task' ))
		{
			case 'save':
				$this->setRedirect( 'index.php?option=com_properties&view=employpost');	
			break;		
		}
		$this->setMessage( JText::_( $msg ) );
	}
} // end class
?>