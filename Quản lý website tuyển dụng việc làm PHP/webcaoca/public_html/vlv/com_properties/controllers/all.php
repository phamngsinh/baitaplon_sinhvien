<?php
// ensure a valid entry point
defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.controller');
//JTable::addIncludePath( JPATH_COMPONENT_ADMINISTRATOR.DS.'tables' );
$id 	= JRequest::getVar('id', 0, 'get', 'int');
class JLORDCOREControllerAll extends JController
{
	function __construct()
	{
		parent::__construct();
	}
	function display()
	{
		global $mainframe;
		$cid 	= JRequest::getVar('cid', array(0), 'post', 'array');
		$user 	= & JFactory::getUser(); 
		JArrayHelper::toInteger($cid, array(0));
		$mName 		= 'all';	
		$vName 		= 'all';	
		$vLayout 	= 'default';
		$option 	= JRequest::getVar('option');	
		$task = JRequest::getVar('task');
		switch ($task){
			case 'add':	
			case 'edit':
				JRequest::setVar( 'hidemainmenu', 1 );				
				$vLayout = 'alls';				
				//JRequest::setVar('edit',true);
				break;
			case 'save':
			case 'apply':
				$this->saveEmploy();
				break;
			case 'remove':
				$this->removePost();
				break;
			case 'cancel':
				$this->cancelpost();
				break;	
			case 'Checkmail':
				$this->Checkmail();
				break;
			default:
				$vName 		= 'all';	
				$vLayout 	= 'default';							
		}
		//exit($vLayout);
		$document 	= JFactory::getDocument();
		$vType 		= $document->getType();		
		$view = $this->getView($vName, $vType);
		if ($model 	= &$this->getModel($mName)) {
			$view->setModel($model, true);
		}
		$view->setLayout($vLayout);		
		$view->display();		
	}
	function removePost(){		
		global $mainframe;
		$cid 	= JRequest::getVar('cid', array(), '', 'array');
		$db 	= &JFactory::getDBO();
		$t_cid 	= count($cid);  
		//exit($cid[0]." = 0");
		if($t_cid){
			$cids 	= implode(',', $cid);
			$query 	= "DELETE FROM `#__properties_products` WHERE `id` IN ($cids)";
			$db->setQuery($query);
			$msg = "Deleted ($t_cid) items";
			if (!$db->query()) {
				echo "<script> alert('".$db->getErrorMsg()."');
				window.history.go(-1); </script>\n";
			}
		}
		$mainframe->redirect('index.php?option=com_properties&view=all',$msg);
	}
	function cancelpost(){
		global $mainframe;
		$db 	= &JFactory::getDBO();
		$option = JRequest::getVar('option');
		// Initialize variables
		$mainframe->redirect('index.php?option='.$option.'&view=all');
	}
	//send mail
	function char_makerand ()
	{
		$charset 	 = "abcdefghijklmnopqrstuvwxyz";
		$charset 	.= "0123456789";
		$char = $charset[(mt_rand( 0,(strlen( $charset )-1)) )];
		return $char;
	}
	
	function str_makerand()
	{
	  	$key = "";
	  	for ( $i=0; $i < 50; $i++ ) {
	   		$key .= char_makerand();
	  	}
	  	return $key;
	}
	
	function saveAll(){
		global $mainframe;
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.file');
		$component_name = 'properties';
		$option = JRequest::getVar('option');	
		
		$model = $this->getModel('all');
		$this->TableName='profiles';
					
		$post = JRequest::get( 'post' );
		//echo "<pre>";print_r($post);exit;		
		//$component = JComponentHelper::getComponent( 'com_properties' );
		//$params = new JParameter( $component->params );
		//$AutoCoord=$params->get('AutoCoord',0);
		$db 	=& JFactory::getDBO();
		
		require_once(JPATH_SITE.DS.'configuration.php');
		$datos = new JConfig();	
		$basedatos = $datos->db;
		$dbprefix = $datos->dbprefix;
		
		$query = "SHOW TABLE STATUS FROM `".$basedatos."` LIKE '".$dbprefix.$component_name."_".$this->TableName."';";
		$db->setQuery( $query );		
		$nextAutoIndex = $db->loadObject();
		$destino_imagen = JPATH_SITE.DS.'images'.DS.'properties'.DS.'profiles'.DS;		
		if(JRequest::getVar('id')){
			$id_agent=JRequest::getVar('id');
		}else{
			$id_agent=$nextAutoIndex->Auto_increment;
		}
		//$id_agent = 0;
		if (isset($_FILES['image'])){
			$name = $_FILES['image']['name'];	
			if (!empty($name)) {
				$ext = '.'.JFile::getExt($name);
			$personal_image = JPATH_SITE.DS.'images'.DS.'properties'.DS.'profiles'.DS;
			move_uploaded_file($_FILES['image']['tmp_name'],	$personal_image.$id_agent.'_p'.$ext); 
			//$post['image'] = $id_agent.'_p'.$ext;
			$img_logo = $id_agent.'_p'.$ext;
			$AnchoLogo=140;
			$AltoLogo=200;
			$destinoCopia=$personal_image;
			$destinoNombre=$post['image'];
			$destino = $personal_image;
			$this->CambiarTamanoExacto($post['image'],$AnchoLogo,$AltoLogo,$destinoCopia,$destinoNombre,$destino);
			}
		}
		//get var agu
		$text 		= JRequest::getVar( 'c_summary', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$text		= str_replace( '<br>', '<br />', $text );	
		$salutation = JRequest::getVar('salutation');
		$name 		= JRequest::getVar('name');
		$company 	= JRequest::getVar('company');
		$c_name 	= JRequest::getVar('c_name');
		$web 		= JRequest::getVar('web');
		$con_name 	= JRequest::getVar('con_name');
		$phone 		= JRequest::getVar('phone');
		$fax 		= JRequest::getVar('fax');
		$mobile 	= JRequest::getVar('mobile');
		$cyid 		= JRequest::getVar('cyid');
		$address1  	= JRequest::getVar('address1');
		$c_summary	= $text;
		//$msg='Saved  profile '.$this->TableName.' : '.$LastModif.'!  ';
		$msg = "save Alls";
		switch (JRequest::getVar('task')){
			case 'save':
				//email username password password2
				//salutation name company c_name web con_name phone fax mobile cyid address1  c_summary 				
				$qry = "insert into #__properties_profiles 
						 (`id`,`salutation`,`name`,`company`,`c_name`,`web`,`con_name`,`phone`,`fax`,`mobile`,`cyid`,`address1`,`c_summary`,`image`) 
						  values('','$salutation','$name','$company','$c_name','$web','$con_name','$phone','$fax','$mobile','$cyid','$address1','$c_summary','$img_logo')";
			    $db->setQuery($qry);
				if (!$db->query()) {
					JError::raiseError(500, $db->getErrorMsg() );
				}
				$mainframe->redirect('index.php?option=com_properties&view=all',$msg);
				break;
			default:
				break;	
		}
	}
	function CambiarTamanoExacto($nombre,$max_width,$max_height,$destinoCopia,$destinoNombre,$destino){
		$InfoImage=getimagesize($destino.$nombre);               
        $width=$InfoImage[0];
        $height=$InfoImage[1];
		$type=$InfoImage[2];						 
							
		$x_ratio = $max_width / $width;
		$y_ratio = $max_height / $height;
	
		if (($width <= $max_width) && ($height <= $max_height) ) {
			$tn_width = $width;
			$tn_height = $height;
		} else if (($x_ratio * $height) < $max_height) {
			$tn_height = ceil($x_ratio * $height);
			$tn_width = $max_width;
		} else {
			$tn_width = ceil($y_ratio * $width);
			$tn_height = $max_height;
		}
		$width=$tn_width;
		$height	=$tn_height;	
			 
		switch($type)
	     {
	       case 1: //gif
	        {
             $img = imagecreatefromgif($destino.$nombre);
             $thumb = imagecreatetruecolor($width,$height);
             imagecopyresampled($thumb,$img,0,0,0,0,$width,$height,imagesx($img),imagesy($img));
             ImageGIF($thumb,$destinoCopia.$destinoNombre,100);
	
	           break;
	        }
	       case 2: //jpg,jpeg
	        {					 
	             $img = imagecreatefromjpeg($destino.$nombre);
	             $thumb = imagecreatetruecolor($width,$height);
	            imagecopyresampled($thumb,$img,0,0,0,0,$width,$height,imagesx($img),imagesy($img));
	            ImageJPEG($thumb,$destinoCopia.$destinoNombre);
	           break;
	        }
	       case 3: //png
	        {
	             $img = imagecreatefrompng($destino.$nombre);
	             $thumb = imagecreatetruecolor($width,$height);
	           	imagecopyresampled($thumb,$img,0,0,0,0,$width,$height,imagesx($img),imagesy($img));
	           	ImagePNG($thumb,$destinoCopia.$destinoNombre);
	           break;
	        }
	     } // switch				  
	}
	//ajax check mail
	function Checkmail(){
		/*$db 	= JFactory::getDBO();
		$email 	= JRequest::getVar('email');
		$qry 	= "SELECT email FROM #__users WHERE email = '$email'";
		$db->setQuery($qry);
		$res 	= $db->loadResult();
		if (count($res))
			exit('Dia chi mail da dc dang ky');
		else 
			exit('Dia chi mail co the su dung');*/	
		$msg = 'thanhtd';
		echo $msg;
	}
} // end class
?>