<?php
defined('_JEXEC') or die('Restricted Access');
global $mainframe,$option;
$view = JRequest::getVar('view');
$rows 			= $this->res->cvs_seecks_apply;
//echo "<pre>";print_r($rows);exit;
$pageNav		= $this->res->pageNav;
$tmp			= '';
$task = JRequest::getVar('task');
?>
<link rel="stylesheet" type="text/css" href="<?php echo JURI::base()?>components/com_properties/assets/css/style.css"></link>
<script type="text/javascript">
</script>
<?php 
?>
<div class="box-center">
	<?php 
		$user = &JFactory::getUser();
		//link menu employ
		$link_new_job 			= "index.php?option=com_properties&view=employpost&task=add";
		$link_job_listing 		= 'index.php?option=com_properties&view=employpost';
		$link_job_buy_packet 	= 'index.php?option=com_properties&view=employpost&task=buy_packet';
		$link_job_list_buy_packet 	= 'index.php?option=com_properties&view=employpost&task=list_packet';
		$link_job_apply 		= "index.php?option=com_properties&view=employpost&task=jobapply";
		$link_job_seecker 		= "index.php?option=com_properties&view=employpost&task=cv_seecker";
		$link_edit_profile_employ = "index.php?option=com_properties&view=employ&task=edit&mid=".$user->get('id');
		$link_doc = "#";
		$paramss = &JComponentHelper::getParams( 'com_properties' );
		$id_content = $paramss->get('title');  
		$link_doc = "index.php?option=com_content&view=article&id=$id_content";
	?>
	<h3>
		<?php echo JText::_('Xem hồ sơ ứng viên')?>	
	</h3>
	<ul id="tpj_topmenu">
		<li><a href="<?php echo $link_new_job; ?>"><?php echo JText::_('Đăng  tuyển dụng'); ?></a></li>
		<li><?php echo JText::_('Quản lý hồ sơ'); ?>
			<ul>
				<li><a href="<?php echo $link_job_listing; ?>"><?php echo JText::_('Công việc đã đăng'); ?></a>
				<li><a href="<?php echo $link_job_buy_packet; ?>"><?php echo JText::_('Mua gói hồ sơ'); ?></a>
				<li><a href="<?php echo $link_job_list_buy_packet; ?>"><?php echo JText::_('DS hồ sơ đã mua'); ?></a>
			</ul>
		</li>
		<li><?php echo JText::_('Xem hồ sơ'); ?>
			<ul>
				<li><a href="<?php echo $link_job_apply; ?>"><?php echo JText::_('Hồ sơ apply'); ?></a>
				<li><a href="<?php echo $link_job_seecker; ?>"><?php echo JText::_('Hồ sơ ứng viên'); ?></a>
			</ul>
		</li>
		<li><a href="<?php echo $link_edit_profile_employ; ?>"><?php echo JText::_('Thông tin nhà tuyển dụng'); ?></a></li>
		<li><a href="<?php echo $link_doc; ?>"><?php echo JText::_('Giá và dịch vụ'); ?></a></li>
		<li style="display:none;"><a href="index.php?option=com_properties&view=all"><?php echo JText::_('Xem tất cả công việc'); ?></a>
          	</li>
	</ul><br />
	<div class="css_dangky">
<form action= "index.php?option=<?php echo $option;?>&view=employpost" method="POST" name="adminForm" id="adminForm">
<?php echo JHTML::_( 'form.token' ); ?>
<div class="tctt_left2">
	<div class="clear"></div>
	<div class="css_nganhnghe">
		<h1><?php echo JText::_('Danh sách hồ sơ ứng viên')?></h1>
		<table id="candidates_table" class="rc_tableList">
			<tr class="title_candid"> 
				<td class="rc_" width="45%" >
					Tên ứng viên
				</td> 
				<td class="rc_" width="15%">
			    Kinh nghiệm 
				<td class="rc_" width="15%">
					Mức lương 
					 </td> 
				<td class="rc_" width="15%">
						Nơi làm việc 
				</td> 
				 <td class="rc_" width="10%">
				 Cập nhật 
				  </td>  
	          </tr>
	          <?php 
					$k = 0;
					for ($i=0, $n=count( $rows ); $i < $n; $i++)
					{
						$row = &$rows[$i];
						$link 		= JRoute::_( 'index.php?option='.$option.'&view=chitiet&id='. $row->id_product.'&agent_id='.$row->id_employ);
						//index.php?option=com_properties&view=chitiet&id=&agent_id=83
						$checked 	= JHTML::_('grid.id',  $i, $row->id );
						$published 	= JHTML::_('grid.published', $row, $i );
						$dd_cls = ($i%2)?'Even':'Odd';
				?>
					<tr class="rc_row<?php echo $dd_cls;?>"> 
                     	<td class="rc_c tenuv" width="45%" >
							<?php JHTML::_('behavior.modal'); ?>
							<div class="button2-left">
								<div class="blank">
									<a rel="{handler: 'iframe', size: {x: 650, y: 375}}" href="index.php?option=<?php echo $option?>&view=employpost&task=show_seecker&id_seecker=<?php echo $row->mid;?>&id_cv=<?php echo $row->id_cv;?>" title="Select an Article" class="modal"><?php echo $row->psk_name;?></a>
								</div>
							</div>
							<!--<div class="button2-left">
								<div class="blank">
									<a rel="{handler: 'iframe', size: {x: 650, y: 375}}" href="index.php?option=<?php echo $option?>&view=employpost&task=show_seecker&id_seecker=<?php echo $row->mid;?>&click=1" title="Select an Article" class="modal"><?php echo $row->psk_name;?></a>
								</div>
							</div>
                     	 	-->
                     		<strong class="subTitle"><?php echo $row->vitri;?></strong>
					 	</td> 
                         <td class="rc_c" width="15%"><?php echo $row->experience_year;?>&nbsp;(<?php echo JText::_('Năm')?>)</td> 
                         <td class="rc_c" width="15%"><?php echo $row->salary?></td> 
                         <td class="rc_c" width="15%"><?php echo $row->pst_name;?></td> 
                         <td class="rc_c" width="10%"><?php echo $row->start_date;?></td>                             
                     </tr>
                     <tr><td class="rc_c vdd" colspan="5"><?php echo substr($row->cv,0,250);?></td></tr>
					
					<?php 
					$k = 1-$k;
						}
					?>
		</table> 
		<div class="clear"></div>
		<div align="right" class="page ges">
			<?php echo $pageNav->getPagesLinks(); ?><!--
			Page: <a href="#">1</a><a href="#">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a><a href="#">6</a>
			<a href="#">7</a><a href="#">8</a><a href="#">9</a><a href="#">10</a><a href="#">11</a><a href="#">12</a>
		--></div>
			
	</div>
</div>
	<input type="hidden" name="task" value="<?php echo $task?>" />
	<input type="hidden" name="option" value="<?php echo $option;?>" />
	<input type="hidden" name="view" value="<?php echo $view;?>">
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="tmp" value="<?php echo $tmp; ?>">
	<input type="hidden" name="filter_order" value="<?php echo $lists['order_post']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $lists['order_Dir_post']; ?>" />
</form>
	<div class="clear"></div>
	</div>
</div>