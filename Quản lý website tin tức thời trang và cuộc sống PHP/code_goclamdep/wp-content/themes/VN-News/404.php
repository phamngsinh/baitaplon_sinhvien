﻿<?php get_header(); ?>

    
<div id="contentleft">
	<div id="main-content">
		<div class="content-block">
			<div class="box-title">
				<div class="blue">
					<h3><?php _e('404 Page Not Found','vn-news'); ?></h3>
				</div>
			</div>
			<div class="post-content clearfix">
				<p><?php _e("Sorry, but you are looking for something that isn't here.","vn-news"); ?></p>
				<?php _e('You could try going <a href="javascript:history.back()">&laquo; Back</a> or maybe you can find what your looking for below:','vn-news'); ?>
				<?php include (TEMPLATEPATH . "/searchform.php"); ?>
			
<p><img src="http://news.image.soixam.com/thum/t23436.jpg"></p>
    <p>- 404 là một lỗi không tìm thấy trang nội dung liên kết.</p>

    <p>- Có thể nội dung đã bị xóa hoặc bị giới hạn quyền truy cập.</p>

    <p>- Xin vui lòng trở lại trang chủ của website bằng cách nhấp vào nút “Back” của trình duyệt !</a></p>



</div>
		</div>
	</div>

	<div id="sidebar2">
		<ul>
		<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(2) ) : else : ?>
			<?php include (TEMPLATEPATH . '/sidebar-2.php'); ?>
		<?php endif; ?>
		</ul>
		<?php include (TEMPLATEPATH . '/ads/banner160.php'); ?>
	</div>	
</div>

<div id="contentright">
	<div id="sidebar1">
		<?php include (TEMPLATEPATH . '/ads/banner300.php'); ?>
		<?php if (get_settings("show_subscribe")) include (TEMPLATEPATH . '/subscribe-form.php'); ?>
		<ul>
		<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(1) ) : else : ?>
			<?php include (TEMPLATEPATH . '/sidebar-1.php'); ?>
		<?php endif; ?>
		</ul>
	</div>
</div>

<?php get_footer(); ?>
