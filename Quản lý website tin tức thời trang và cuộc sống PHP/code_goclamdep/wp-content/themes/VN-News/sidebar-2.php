<li class="widget widget_categories">
	<h2><?php _e('Categories','vn-news'); ?></h2>
	<ul>
		<?php wp_list_cats('sort_column=name&hierarchical=1&optioncount=0'); ?>
	</ul>
</li>




<li class="widget widget_links">
	<h2><?php _e('Links','vn-news'); ?></h2>
	<ul>
		<?php get_links('-1', '<li>', '</li>', '<br />', FALSE, 'name', FALSE, FALSE, -1, FALSE); ?>
	</ul>
</li>