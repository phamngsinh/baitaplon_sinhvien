<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title>
<?php if ( is_single() ) {
	  wp_title('');
	  echo (' | ');
	  bloginfo('name');
 
	} else if ( is_page() || is_paged() ) {
	  bloginfo('name');
	  wp_title('|');
 
	} else if ( is_author() ) {
	  bloginfo('name');
	  wp_title(' | Archive for ');	  
	  
	} else if ( is_archive() ) {
	  bloginfo('name');
	  echo (' | Archive for ');
	  wp_title('');
 
	} else if ( is_search() ) {
	  bloginfo('name');
	  echo (' | Search Results');
 
	} else if ( is_404() ) {
	  bloginfo('name');
	  echo (' | 404 Error (Page Not Found)');
	  
	} else if ( is_home() ) {
	  bloginfo('name');
 
	} else {
	  bloginfo('name');
	  echo (' | ');
	  echo (''.$blog_longd.'');
	} ?>
</title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />

<?php if (is_home()) { ?>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/scripts/prototype.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/anchoi.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/scripts/effects.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/scripts/glider.js"></script>
<?php } ?>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/scripts/tabber.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/scripts/dropdown-menu.js"></script>

<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php if (get_settings("sidebar_1_pos") == "right") { ?>
<style type="text/css">
#main-content {float:left;}
#sidebar2 {float:right;}
</style>
<?php } ?>

<?php wp_head(); ?>

<?php if(get_settings("header_insert")) echo stripslashes(get_settings("header_insert")); ?>
<!-- mrga -->
<script type="text/javascript">

function bookmark_us(url, title){

if (window.sidebar) // firefox
    window.sidebar.addPanel(title, url, "");
else if(window.opera && window.print){ // opera
    var elem = document.createElement('a');
    elem.setAttribute('href',url);
    elem.setAttribute('title',title);
    elem.setAttribute('rel','sidebar');
    elem.click();
}
else if(document.all)// ie
    window.external.AddFavorite(url, title);
}
</script>
<!--/mrga-->

</head>
<body>

<div id="wrapper" class="clearfix"><div id="top"><div id="date"><script type='text/javascript'>function startclock(){var curTime=new Date();var nhours=curTime.getHours();var nmins=curTime.getMinutes();var nsecn=curTime.getSeconds();var nday=curTime.getDay();var nmonth=curTime.getMonth();var ntoday=curTime.getDate();var nyear=curTime.getYear();var AMorPM=" ";if (nhours>=12)AMorPM="";elseAMorPM="";if (nhours>=13)nhours-=12;if (nhours==0)nhours=12;if (nsecn<10)nsecn="0"+nsecn;if (nmins<10)nmins="0"+nmins;if (nday==0)nday="Chủ nhật";if (nday==1)nday="Thứ hai";if (nday==2)nday="Thứ ba";if (nday==3)nday="Thứ tư";if (nday==4)nday="Thứ năm";if (nday==5)nday="Thứ sáu";if (nday==6)nday="Thứ bảy";nmonth+=1;if (nyear<=99)nyear= "19"+nyear;if ((nyear>99) && (nyear<2000))nyear+=1900;var d;var Str0="";if (nhours>9) Str0 = "";else Str0 = "0";d= document.getElementById("theClock");d.innerHTML=" <span>" + Str0 + nhours+" giờ "+nmins+" phút "+nsecn + " giây - " + AMorPM + "</span>" + nday+" - Ngày " + ntoday +" tháng " + nmonth +" năm "+nyear ;setTimeout('startclock()',1000);}</script>Bây giờ là<span id="theClock" align="left"><script language="JavaScript">startclock();</script></span></div><ul id="pagenav">		<li class="tintucmoinhat"><a title="" href="http://24hketnoi.info">Trang chủ</a></li>		<li class="vuongquocgame"><a title="" href="http://forum.24hketnoi.info/">Diễn đàn teen</a></li>		<li class="xemphimnhanh"><a title="" href="http://raovat.24hketnoi.info/">Rao vặt</a></li>	 <li class="nhac"><a title="" href="http://music.24hketnoi.info/">Music</a></li>	<li class="nhacxuan2011"><a href="javascript:setHomePage();">Đặt làm trang chủ</a></li>		<li class="page_item page-item-548" style="border: medium none; padding: 0pt 0pt 0pt 4px;"><a href="javascript:bookmark_us('http://24hketnoi.info','24hketnoi.info - Kênh tin tức giải trí tổng hợp')">Ghi nhớ bookmark</a></li>		</ul>	</div>
	<div id="header" class="clearfix">
		<div class="sitehead-left">
		<?php $logo = get_settings("site_logo"); ?>
		<?php if($logo) { ?>
			<a href="<?php echo get_option('home'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo/<?php echo $logo; ?>" alt="" /></a>
		<?php } else { ?>
			<h1><a href="<?php bloginfo('home'); ?>"><?php bloginfo('name'); ?></a></h1>
			<h2 id="description"><?php bloginfo('description'); ?></h2>
		<?php } ?>
		</div>
		
		<div class="sitehead-right"><div style="float:right;padding-right:5px;">
						<br>
<script language="JavaScript"><!--

images = new Array(2);

images[0] = "<a href = 'http://ads.24hketnoi.info' target='_blank' title='Liên hệ quảng cáo'><img src='http://goclamdep.net/wp-content/themes/VN-News/ads/banner450.gif' alt='' border='0' height='90' width='450'></a>";

images[1] = "<a href = 'http://ads.24hketnoi.info' target='_blank' title='Liên hệ quảng cáo'><img src='http://goclamdep.net/wp-content/themes/VN-News/ads/banner450.gif' alt='' border='0' height='90' width='450'></a>";

index = Math.floor(Math.random() * images.length);

document.write(images[index]);

//done

// --></script></div>
			
		</div>
		<div id="page-nav" class="clearfix">
			<ul>
				
				
			</ul>
		</div>
		
	</div>
	
	<div id="cate-nav" class="clearfix">
		<ul class="clearfix">
			<?php wp_list_categories('title_li='); ?>
		</ul>
	</div>
<div id="page" class="clearfix">
	
	
<script language="JavaScript"><!--

images = new Array(2);

images[0] = "<a href = '#' target='_blank'><img src='http://24hketnoi.info/wp-content/themes/VN-News/ads/banner960.gif' alt='' border='0' height='90' width='960'></a>";

images[1] = "<a href = 'http://blog.vnnclub.com/' target='_blank'><img src='http://img.tamtay.vn/files/photo2/2011/3/4/23/198169/4d7111ee_69d8518f_sexy728x90.gif' alt='' border='0' height='90' width='960'></a>";

index = Math.floor(Math.random() * images.length);

document.write(images[index]);

//done

// --></script>	
	<div id="page" class="clearfix">