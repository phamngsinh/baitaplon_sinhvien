<?php

/*
Plugin Name: Auto Tags
Plugin URI: http://wordpress.org/extend/plugins/auto-tag/
Description: Tag posts on save and update from tagthe.net and yahoo services.
Version: 0.4.6
Author: Jonathan Foucher
Author URI: http://jfoucher.com

Copyright 2009 Jonathan Foucher

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


require_once('auto-tag.class.php');
//ini_set('display_errors',1);
$auto_tag=new auto_tag();

$auto_tag->init();
//$auto_tag->testing($text);
/*

* */
