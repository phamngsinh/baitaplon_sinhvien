<?php

/*
Part of the auto-tag plugin for wordpress

Copyright 2009 Jonathan Foucher

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
class auto_tag {
	
	
	
	function PostRequest($url, $referer, $_data) {
		// convert variables array to string:
		$data = array();    
		while(list($n,$v) = each($_data)){
			$data[] = "$n=$v";
		}    
		$data = implode('&', $data);
		// format --> test1=a&test2=b etc.
	 
		// parse the given URL
		$url = parse_url($url);
		if ($url['scheme'] != 'http') { 
			die('Only HTTP request are supported !');
		}
	 
		// extract host and path:
		$host = $url['host'];
		$path = $url['path'];
	 
		// open a socket connection on port 80
		if ($fp = @fsockopen($host, 80)){
		 
			// send the request headers:
			fputs($fp, "POST $path HTTP/1.1\r\n");
			fputs($fp, "Host: $host\r\n");
			fputs($fp, "Referer: $referer\r\n");
			fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
			fputs($fp, "Content-length: ". strlen($data) ."\r\n");
			fputs($fp, "Connection: close\r\n\r\n");
			fputs($fp, $data);
		 
			$result = ''; 
	
			while(!feof($fp)) {
				// receive the results of the request
				$result .= fgets($fp, 128);
			}
			// close the socket connection:
			fclose($fp);
		 }
		// split the result header from the content
		$result = explode("\r\n\r\n", $result, 2);

		$header = isset($result[0]) ? $result[0] : '';
		$content = isset($result[1]) ? $result[1] : '';
		$content=explode("\r\n",$content);
		$content=array_slice($content,1);
		$content=array_slice($content,0,-3);
		$content=implode("\r\n",$content);
		return $content;
		// return as array:
		//return array($header, $content);
	}
	
	public static function forbidden_tag($forbidden,$tag){
		if (is_array($forbidden) && !empty($forbidden)){
			foreach($forbidden as $forbid){
				if ($forbid !='')
					if(strpos(strtolower($tag), strtolower($forbid))!==false)
						return true;
			}
		}
		return false;
	}
	function auto_tag_yahoo($content,$num,$remove_tags){
	
		$senddata=array('context'=>urlencode(utf8_decode(strip_tags($content))),
					'output'=>'json',
					'appid'=>'BR_m.GrV34HyixkLbaEHmgSInktZjX1AohGCN6F6ywe5ojN01XGwDw4eRrV3rFdY8zdrhNWH'
					);
		//$content="appid=BR_m.GrV34HyixkLbaEHmgSInktZjX1AohGCN6F6ywe5ojN01XGwDw4eRrV3rFdY8zdrhNWH&context=".urlencode(utf8_decode(strip_tags($content)));
		$data=auto_tag::PostRequest('http://api.search.yahoo.com/ContentAnalysisService/V1/termExtraction','http://jfoucher.com',$senddata);
		//return $this->auto_get_yahoo_tags($data,$num);$
		if($json=json_decode($data)){
			$i=0;
			foreach($json->ResultSet->Result as $kw){
				if ($i>=$num) break;
				if (!auto_tag::forbidden_tag($remove_tags,$kw)) {
					$ret.=$kw.', ';
					$i++;
				}
			}
		}
		//echo '<pre>'.print_r($json,true).'</pre>';
		//die();
		//echo '<pre>'.print_r($data,true).'</pre>';
		
		return substr($ret,0,-1);
	}

	function auto_tag_the_net($content,$num,$remove_tags){
		
		//$content="text=".urlencode(utf8_decode(strip_tags($content)))."&count=".$num."&view=json";
		$senddata=array('text'=>urlencode(utf8_decode(strip_tags($content))),
					'count'=>$num*3,
					'view'=>'json'
					);
		//$data=$this->sendToHost('tagthe.net','POST','/api/',$content,$useragent=0);
		$data=auto_tag::PostRequest('http://tagthe.net/api/', 'http://jfoucher.com', $senddata);
		//return $decode;
		//echo '<pre>'.print_r($data,true).'</pre>';
		$json=json_decode($data);
		$ret='';
		if (count($json->memes[0]->dimensions->topic) >0){
			$i=0;
			foreach($json->memes[0]->dimensions->topic as $topic){
				if ($i>=$num) break;
				if (!auto_tag::forbidden_tag($remove_tags,$topic)){
					$ret.=$topic.', ';
					$i++;
				}
			}
		}
		//echo '<pre>'.print_r($ret,true).'</pre>';
		return substr($ret,0,-2);
	}

	function testing($content){
		//$kw=$this->auto_tag_the_net($content,5).",".$this->auto_yahoo_tag($content,5);
		$kw=$this->auto_tag_yahoo($content,5);
		return $kw;
	}
	
	public static function initAutotagPlugin()
	{

		$yahoo_num = get_option( 'yahoo_num');
		$tagthenet_num = get_option( 'tagthenet_num');
		$append = get_option( 'append');
		$remove_tags = get_option( 'remove_tags');
		if( $_POST['sent'] == 'Y' ) {
			$tagthenet_num = $_POST[ 'tagthenet_num' ];
			$yahoo_num = $_POST[ 'yahoo_num' ];
			$append = $_POST[ 'append' ];
			$remove_tags = $_POST[ 'remove_tags' ];
			update_option( 'yahoo_num', $yahoo_num );
			update_option( 'tagthenet_num', $tagthenet_num );
			update_option( 'remove_tags', $remove_tags );
			update_option( 'append', $append );
	?>
	<div class="updated"><p><strong><?php _e('Options saved.', 'mt_trans_domain' ); ?></strong></p></div>
	<?php

		}
		echo '<div class="wrap">';
		echo "<h2>" . __( 'Autotags Options' ) . "</h2>";
		?>
	<form name="form1" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
	<input type="hidden" name="sent" value="Y" />

	<p>
	<label>
	<?php _e("Number of tags from Yahoo:", 'mt_trans_domain' ); ?>
	<input type="text" name="yahoo_num" value="<?php echo $yahoo_num; ?>" size="20" />
	</label>
	</p>
	<p>
	<label>
	<?php _e("Number of tags from tagthe.net:", 'mt_trans_domain' ); ?>
	<input type="text" name="tagthenet_num" value="<?php echo $tagthenet_num; ?>" size="20" />
	</label>
	</p>
	<p>
	<label>
	<?php _e("Remove those tags (comma separated)", 'mt_trans_domain' ); ?>
	<input type="text" name="remove_tags" value="<?php echo $remove_tags; ?>" size="40" /> 
	<?php _e("(if these tags appear, you will get less tags than asked for)", 'mt_trans_domain' ); ?>
	</label>
	</p>
	<p>
	<label>
	<input type="checkbox" name="append" value="1"<?php echo ($append) ? ' checked="checked"' : ''?> />
	<?php _e("Append tags to the ones that already exist ?:", 'mt_trans_domain' ); ?>
	</label>
	</p>
	<hr />

	<p class="submit">
	<input type="submit" name="Submit" value="<?php _e('Update Options', 'mt_trans_domain' ) ?>" />
	</p>

	</form>
	<p>Hey! Do you like this plugin? Give it a <a href="http://wordpress.org/extend/plugins/auto-tag">good rating</a> on wordpress, or blog about it!</p>
	</div>
		<?php
	}
	public static function addPluginToSubmenu()
	{
		add_submenu_page('options-general.php', 'Auto tags plugin', 'Auto tags plugin', 10, __FILE__, 'auto_tag::initAutotagPlugin');
	}
	public function init(){
		add_action('admin_menu', 'auto_tag::addPluginToSubmenu');  
		add_action('wp_insert_post','auto_tag::tag_posts',1);
	}
	public static function trim_tags(&$item,$k){
		$item=trim($item);
	}
	public static function tag_posts($postid){
		global $wp_filter;
		$yahoo_num=get_option('yahoo_num');
		$tagthenet_num=get_option('tagthenet_num');
		$append=get_option('append');
		$remove_tags=explode(',',strtolower(get_option('remove_tags')));
		array_walk($remove_tags,'auto_tag::trim_tags');
		//echo '<pre>'.print_r($remove_tags,true).'</pre>';
		//die();
		$post=get_post($postid, ARRAY_A);
		//echo '<pre>'.print_r($post,true).'</pre>';
		//die();
		$content=$post['post_title']." ".$post['post_content'];
//auto_tag::auto_tag_the_net($content,$tagthenet_num).
		$keywords=auto_tag::auto_tag_the_net($content,$tagthenet_num,$remove_tags).', '.auto_tag::auto_tag_yahoo($content,$yahoo_num,$remove_tags);

		remove_action('wp_insert_post','auto_tag::tag_posts');
		wp_set_post_tags( $postid,$keywords, $append );
	}
	
}


?>
