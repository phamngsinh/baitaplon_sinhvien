<?php

function rbvn_set_schedule($cr_interval, $cr_period) {
		if($cr_period == 'hours') {
			$interval = $cr_interval * 3600;
		} elseif($cr_period == 'days') {
			$interval = $cr_interval * 86400;		
		}
		$recurrance = "RBVN_" . $cr_interval . "_" . $cr_period;
		$schedule = array(
			$recurrance => array(
				'interval' => $interval,
				'display' => sprintf("%c%c%c %s", 0x44, 0x42, 0x42, str_replace("_", " ", $recurrance)),
				)
			);

		if (is_array($opt_schedules = get_option('rbvn_schedules'))) {
			if (!array_key_exists($recurrance, $opt_schedules)) {
				update_option('rbvn_schedules', array_merge($schedule, $opt_schedules));
			}
			else {
					return $recurrance;
			}
		}
		else {
			add_option('rbvn_schedules', $schedule);
		}
		
		return $recurrance;			
}

function rbvn_delete_schedule($cr_interval, $cr_period) {
   global $wpdb, $rbvn_dbtable;
   
	$recurrance = "RBVN_" . $cr_interval . "_" . $cr_period;	
		if (is_array($opt_schedules = get_option('rbvn_schedules'))) {
			$sql = "SELECT id FROM " . $rbvn_dbtable . " WHERE `postspan` ='$recurrance'";
			$test = $wpdb->query($sql);
			if (array_key_exists($recurrance, $opt_schedules) && 0 === $test) {
				unset($opt_schedules[$recurrance]);				
				update_option('rbvn_schedules', $opt_schedules);
			}
		}
}

function rbvn_get_schedules($arr) {
		$schedules = get_option('rbvn_schedules');
		$schedules = (is_array($schedules)) ? $schedules : array();		
		return array_merge($schedules, $arr);
}
add_filter('cron_schedules', 'rbvn_get_schedules');

function rbvn_preventduplicates($tocheck) {
	global $wpdb, $rbvnlink_dbtable;
	$check = $wpdb->get_var("SELECT fromlink FROM ".$rbvnlink_dbtable." WHERE fromlink = '$tocheck'");
	
	return $check;
	
}

function rbvn_insertpost($content,$title,$cat,$fromlink = "") {
	global $wpdb, $rbvnlink_dbtable;
	
		
		remove_filter('content_save_pre', 'wp_filter_post_kses');
		if ($title != '') {
			$post_date= current_time('mysql');
			$post_date_gmt= current_time('mysql', 1);
			$post_author=1;
			if (get_option('rbvn_poststatus')=='published') {
				$post_status = 'publish';
			} elseif (get_option('rbvn_poststatus')=='draft') {
				$post_status = 'draft';
			}
			$post_category = array($cat);
			$post_content=$content;

			$badwords = explode(";",get_option('rbvn_badwords'));
			$badchars = array(",", ":", "(", ")", "]", "[", "?", "!", ";", "-", '"');
			
			
			$title2 = str_replace($badchars, "", $title);
	        $items = explode(' ', $title2);
			$tags_input = array();				
	        for($k = 0, $l = count($items); $k < $l; ++$k){		
				$long = strlen($items[$k]);
				if ($long > 3) {
					if (!in_array(strtolower($items[$k]), $badwords)) {
						$tags_input[] = $items[$k];
					}
				}
			}		
			if(get_option('rbvn_autotag') == 'No') {$tags_input = "";}
			
			$post_title = trim($title);
			
			$post_data = compact('post_content','post_title','post_date','post_date_gmt','post_author','post_category', 'post_status', 'tags_input');		
	
			$post_data = add_magic_quotes($post_data);
			
			$post_ID = wp_insert_post($post_data);
			if ( is_wp_error( $post_ID ) )
			echo "\n" . $post_ID->get_error_message();
			
			
			$insert = "INSERT INTO " . $rbvnlink_dbtable . " SET fromlink = '$fromlink'";
			
			$wpdb->query($insert);
			
			
			return $post_ID;
		} else {
			return false;
		}
}

function rbvn_get_string_between($string, $start, $end){
        $string = " ".$string;
        $ini = strpos($string,$start);
        if ($ini == 0) return "";
        $ini += strlen($start);   
        $len = strpos($string,$end,$ini) - $ini;
        return substr($string,$ini,$len);
}

function debug_log($message) {
	global $rbvn_debug;       
	if($rbvn_debug == 1) {
		$text = $message. "\n"; 
		// Write to log
		$fp=fopen(".././wp-content/plugins/RobotVN/debug.txt",'a');
		fwrite($fp, $text ); 
		fclose($fp);
	}
}    																																															


function rbvn_footlink() {
	global $rbvn_core_version;
		echo "<br clear=\"all\" />Hệ thống tự động lọc tin <a href=\"http://vnwebmaster.com/\" title=\"RobotVN\">Website sử dụng {$rbvn_core_version} bởi Webmaster Việt Nam Forums</a>";
}
add_action('wp_footer', 'rbvn_footlink'); 

function rbvn_add_keyword() {
   global $wpdb, $rbvn_dbtable;

		$postevery = $_POST['rbvn_postevery'];
		$cr_period = $_POST['rbvn_period'];
		
		$vnex_cat= $wpdb->escape($_POST['rbvn_vnc_vnccat']);
		$tuoitre_cat= $wpdb->escape($_POST['rbvn_tuoitrecat']);
		//Added by Nguyen Duy Nhan
		$postspan = "RBVN_" . $postevery . "_" . $cr_period;
		
		rbvn_set_schedule($postevery, $cr_period);

		$keyword = $wpdb->escape($_POST['rbvn_keyword']);
		$category = $wpdb->escape($_POST['rbvn_category']);
		
		$check =0;
		if($check == 5) {echo '<div class="updated"><p>Keyword này đã tồn tại!</p></div>';} else {
		
			if($keyword == "") {
				echo '<div class="updated"><p>Vui lòng nhập 1 keyword!</p></div>';		
			} elseif($postevery == "") {
				echo '<div class="updated"><p>Hãy chọn thời gian đăng bài!</p></div>';			
			} else {
			
				$insert = "INSERT INTO " . $rbvn_dbtable . " SET postspan = '$postspan', keyword = '$keyword', vnexcat = '$vnex_cat', tuoitrecat = '$tuoitre_cat', category = '$category'";
			 
				
				//Added by Nguyen Duy Nhan
				if ($_POST['rbvn_post_vne'] != 'yes' || !function_exists('rbvn_vnexpresspost')) {$insert .= ", num_vnexpress = '-1'";}
				if ($_POST['rbvn_post_tuoitre'] != 'yes' || !function_exists('rbvn_tuoitrepost')) {$insert .= ", num_tuoitre = '-1'";} 
				
				$results = $wpdb->query($insert);
				if ($results) {				
					$sql = "SELECT LAST_INSERT_ID() FROM " . $rbvn_dbtable;
					$id = $wpdb->get_var($sql);	

					if(get_option('rbvn_randomstart2')!= 0) {
						$lagfront = (int)get_option('rbvn_randomstart1');
						$lagback = (int)get_option('rbvn_randomstart2');
						$lagfront = $lagfront * 3600;
						$lagback = $lagback * 3600;
						$lag = rand($lagfront,$lagback);						
					} else {$lag = 0;}	
					
					wp_schedule_event( time()+$lag, $postspan, "rbvnposthook" , array($id) );

					$next = wp_next_scheduled( "rbvnposthook", array($id) );
					if($next == 0 || $next == "0" || $next == null || $next == "") {
						wp_schedule_event( time()+$lag, $postspan, "rbvnposthook" , array($id) );
					}
					
					echo '<div class="updated"><p>Keyword '.$keyword.' đã được thêm!</p></div>';		
				}
			}
		}
}
//End Endcode
function rbvn_check_schedules() {
   global $wpdb, $rbvn_dbtable;
    $records = $wpdb->get_results("SELECT * FROM " . $rbvn_dbtable);
    foreach ($records as $record) {
	
		$next = wp_next_scheduled("rbvnposthook",array($record->id));
		 
		if($next == 0 || $next == "0" || $next == null || $next == "") {
			wp_clear_scheduled_hook("rbvnposthook", $record->id);
		
			$postspanorig = $result->postspan;	
			$time = rbvn_get_string_between($postspanorig, "RBVN_", "_");
	
					$findMich   = 'days';
					$pos = strpos($postspanorig, $findMich);
					if ($pos === false) {
						$span = "hours";
					} else {
					    $span = "days";
					}			
		
			rbvn_set_schedule($time, $span);
			
			wp_schedule_event( time(), $record->postspan, "rbvnposthook" , array($record->id) );
		}
	}
}
function rbvn_delete_keyword() {
   global $wpdb, $rbvn_dbtable;

	$delete = $_POST["delete"];
	$array = implode(",", $delete);

	foreach ($_POST['delete']  as $key => $value) {
		$i = $value;
		$sql = "SELECT * FROM " . $rbvn_dbtable . " WHERE id = '$i'";
		$result = $wpdb->get_row($sql);	
		$postspan = $result->postspan;	
		$cr_interval = rbvn_get_string_between($postspan, "RBVN_", "_");
	
		$findMich   = 'days';
		$pos = strpos($postspan, $findMich);
		if ($pos === false) {
			$cr_period = "hours";
		} else {
		    $cr_period = "days";
		}			
			$delete = "DELETE FROM " . $rbvn_dbtable . " WHERE id = $i";
			$results = $wpdb->query($delete);
			if ($results) {
				rbvn_delete_schedule($cr_interval, $cr_period);				
				wp_clear_scheduled_hook("rbvnposthook", $i);
			}	
	}	
	if ($results) {
		echo '<div class="updated"><p>Keyword đã được xóa.</p></div>';
	}
}

function rbvn_post($which) {
   global $wpdb, $rbvn_dbtable;
	
	// Debug
   	debug_log(' ');		
   	debug_log(' ---- '.date('m/d/Y g:i A').' ----');	

	$sql = "SELECT * FROM " . $rbvn_dbtable . " WHERE id = '$which'";
	$result = $wpdb->get_row($sql);	
	
	$vnnum = $result->num_vnexpress;
	$ttnum = $result->num_tuoitre;
	
	$keyword = $result->keyword;
	$cat = $result->category;
	$vnexcat = $result->vnexcat;
	$tuoitrecat = $result->tuoitrecat;

	if($keyword != '' && $keyword != null) {
	
		debug_log('CREATING post for keyword '.$which.': '.$keyword);
		
		$resultall = false;
		$abort = 0;
		$totalpost = 0;

			
		//Added $vnc.' (vn)'
		debug_log('Chances: '.$vnnum.' (vnexpress)'.$ttnum.' (tuoitre)');		
		debug_log('Module selected: '.$post);	
		
		//Insert post from Vnexpress
		if($vnnum!=-1){
			
			$result = rbvn_vnexpresspost($keyword,$cat,$vnnum,$which, $vnexcat);
			if($result != false){
				$resultall = true;
				$vnnum++;
				$totalpost++;
			}
		}
		
		//Insert post from TuoiTre
		if($ttnum!=-1){
			
			$result = rbvn_tuoitrepost($keyword,$cat,$ttnum,$which, $tuoitrecat);
			if($result != false){
				$resultall = true;
				$ttnum++;
				$totalpost++;
			}
		}
		
		if ($resultall != false) {
			$inserted = true;
		}else{
			$inserted = false;
		}
			
		$sql = "SELECT * FROM " . $rbvn_dbtable . " WHERE id = '$which'";	
		$update = $wpdb->get_row($sql);	
			
		if($inserted != false) {
			$newtotal = $update->num_total + $totalpost;
		} else {
			$newtotal = $update->num_total;
		}
		
		$sql = "UPDATE " . $rbvn_dbtable . " SET `num_vnexpress` = '$vnnum',`num_tuoitre` = '$ttnum',  `num_total` = '$newtotal' WHERE id = '$which'";
		$update = $wpdb->query($sql);	
		// Debug  	  
		debug_log('Numbers: '.$newnum.' (newnum) '.$newtotal.' (newtotal) ');
		if ($update) {debug_log('Numbers inserted'); } else {debug_log('Numbers NOT inserted!');}
					
		if($inserted != true) {
			echo '<div class="updated"><p>Không có bài viết để cập nhật.</p></div>';
		} else {		
			echo '<div class="updated"><p>Bài viết đã được tạo thành công với từ khóa "'.$keyword.'"!</p></div>';
		}
	}
}
?>