<?php
//set_time_limit(0);
include ('function.php');
$phpself=$_SERVER['PHP_SELF'];
//echo $phpself;
$error=0;
$err_msg=array();
$notice=0;
$notice_msg=array();
//Set language
if (!isset($_SESSION['language']))
	{
	//Kiem tra Cookie
	if (isset($_COOKIE['language']))
		{
		$_SESSION['language']=$_COOKIE['language'];
		}
	else
		{
		set_language($default_language);
		}
	}
else
	{
	if (isset($_REQUEST['setlanguage']))
		{
		//echo $_REQUEST['language'];
		if (!set_language($_REQUEST['setlanguage']))
			{
			set_language($default_language);
			}
		}
	}
//Load language file
require($lang_dir.$_SESSION['language'].'.php');

//Theme
if (!isset($_SESSION['theme']))
	{
	$_SESSION['theme']=$default_theme;
	}
//Load theme
if (isset($_SESSION['theme']))
	{
	$page_layout=load_layout($_SESSION['theme'],'layout');
	}

//---------------------------------- MAIN ACTION -----------------------------------------
if (isset($_REQUEST['module']))
	{
	$module=$_REQUEST['module'];
	switch ($module)
		{
		//------------- Log in -------------
		case 'login':
		$phpinclude='login.php';
		$caption=$strLoginTitle;
		if ($fo=fopen($jscript_dir.'check_login.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			$script=str_replace('<!-- strNoUserName -->',$strNoUserName,$script);
			$script=str_replace('<!-- strNoPassWord -->',$strNoPassWord,$script);
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
		
		//--------- Control Panel -----------
		case 'cpanel':
		check_login();
		$phpinclude='cpanel.php';
		$caption=$strControlPanelTitle;
		break;
		
		//---------- Categories ------------
		case 'categories':
		check_login();
		$phpinclude='categories.php';
		$caption=$strCategoriesTitle;
		if ($fo=fopen($jscript_dir.'mouseovermenu.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
		
		//---------- View table ------------
		case 'viewtbl':
		check_login();
		$phpinclude='viewtbl.php';
		$caption=$strViewtableTitle;
		$jsinclude=array('checkbox.js','tablefilter.js');
		if ($fo=fopen($jscript_dir.'mouseovermenu.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
		
		//---------- Addnew record ------------
		case 'addnew':
		check_login();
		$phpinclude='addnew.php';
		$caption=$strAddnewTitle;
		$jsinclude=array('checkbox.js','tablefilter.js','xc2_default.js','xc2_inpage.js');
		if ($fo=fopen($jscript_dir.'mouseovermenu.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
		
		//---------- Insert record ------------
		case 'insert':
		check_login();
		$phpinclude='doaddnew.php';
		$caption=$strAddnewTitle;
		$jsinclude=array('checkbox.js','tablefilter.js','xc2_default.js','xc2_inpage.js');
		if ($fo=fopen($jscript_dir.'mouseovermenu.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
		
		//---------- View record ------------
		case 'viewrec':
		check_login();
		$phpinclude='viewrec.php';
		$caption=$strViewrecTitle;
		$jsinclude=array('checkbox.js','tablefilter.js','xc2_default.js','xc2_inpage.js');
		if ($fo=fopen($jscript_dir.'mouseovermenu.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
		
		//---------- Update record ------------
		case 'update':
		check_login();
		$phpinclude='doedit.php';
		$caption=$strUpdaterecTitle;
		$jsinclude=array('checkbox.js','tablefilter.js','xc2_default.js','xc2_inpage.js');
		if ($fo=fopen($jscript_dir.'mouseovermenu.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
		
		//---------- Delete record ------------
		case 'delete':
		check_login();
		$phpinclude='dodelete.php';
		$caption=$strUpdaterecTitle;
		$jsinclude=array('checkbox.js','tablefilter.js','xc2_default.js','xc2_inpage.js');
		if ($fo=fopen($jscript_dir.'mouseovermenu.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
		
		//-------- Creat new User ----------
		case 'creat_user':
		check_login();
		$phpinclude='creat_user.php';
		$caption=$strCreatUserTitle;
		$jsinclude=array('textcount.js','viettype.js',
			'toggle.js','gen_validatorv2.js');
		if ($fo=fopen($jscript_dir.'mouseovermenu.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
		
		//-------- User Group ----------
		case 'user_group':
		check_login();
		$phpinclude='user_group.php';
		$caption=$strUserGroupTitle;
		$jsinclude=array('checkbox.js','viettype.js',
			'tablefilter.js','gen_validatorv2.js');
		if ($fo=fopen($jscript_dir.'mouseovermenu.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
		
		//--------- User Profile -----------
		case 'user_profile':
		check_login();
		$phpinclude='user_profile.php';
		$caption=$strViewUserTitle;
		$jsinclude=array('textcount.js','viettype.js','gen_validatorv2.js','toggle.js');
		if ($fo=fopen($jscript_dir.'setdisplay.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
		
		//---------- Mailing List ------------
		case 'mailing':
		check_login();
		$phpinclude='mailing_list.php';
		$caption=$strViewMailList;
		$jsinclude=array('checkbox.js','tablefilter.js','xc2_default.js','xc2_inpage.js');
		if ($fo=fopen($jscript_dir.'mouseovermenu.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
				
		//-------- File explorer ----------
		case 'explorer':
		check_login();
		$phpinclude='explorer.php';
		$caption=$strExplorerTitle;
		$jsinclude=array('textcount.js','viettype.js',
			'toggle.js','gen_validatorv2.js');
		if ($fo=fopen($jscript_dir.'mouseovermenu.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
		
		//-------- Backup database ----------
		case 'explorerdb':
		check_login();
		$phpinclude='explorerdb.php';
		$caption=$strExplorerTitle;
		$jsinclude=array('textcount.js','viettype.js',
			'toggle.js','gen_validatorv2.js');
		if ($fo=fopen($jscript_dir.'mouseovermenu.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
		
		//--------- About vSpider ----------
		case 'about':
		$caption=$strvSpiderTitle;
		$phpinclude='about.php';
		break;
		
		//-------- Language Sys ----------
		case 'langsys':
		check_login();
		$phpinclude='langsys.php';
		$caption=$strLangsys;
		$jsinclude=array('textcount.js','viettype.js',
			'toggle.js','gen_validatorv2.js');
		if ($fo=fopen($jscript_dir.'mouseovermenu.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
		
		//-------- Edit Language Layout  ---------
		case 'editlanglayout':
		check_login();
		$phpinclude='editlanglayout.php';
		$caption=$streditlanglayout;
		$jsinclude=array('textcount.js','viettype.js',
			'toggle.js','gen_validatorv2.js');
		if ($fo=fopen($jscript_dir.'mouseovermenu.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
		
		//-------- Edit Language vSpider  ---------
		case 'editlang':
		check_login();
		$phpinclude='editlang.php';
		$caption=$streditlangvspider;
		$jsinclude=array('textcount.js','viettype.js',
			'toggle.js','gen_validatorv2.js');
		if ($fo=fopen($jscript_dir.'mouseovermenu.js','r'))
			{
			$script="<script>\n";
			while ($cur_line=fgets($fo))
				{
				$script.=$cur_line;
				}
			}
		$script.="</script>\n";
		$head_info=$script;
		break;
		
		//--------- Status Articles ----------
		case 'st_articles':
		$caption=$strvSpiderstatusAr;
		$phpinclude='ac_articel.php';
		break;
		
		default:
		check_login();
		$phpinclude='cpanel.php';
		$caption=$strControlPanelTitle;
		break;
		}
	}
else
	{
	$phpinclude='login.php';
	$caption=$strLoginTitle;
	if ($fo=fopen($jscript_dir.'check_login.js','r'))
		{
		$script="<script>\n";
		while ($cur_line=fgets($fo))
			{
			$script.=$cur_line;
			}
		$script=str_replace('<!-- strNoUserName -->',$strNoUserName,$script);
		$script=str_replace('<!-- strNoPassWord -->',$strNoPassWord,$script);
		}
	$script.="</script>\n";
	$head_info=$script;
	}
//-------------------------------- END OF MAIN ACTION ------------------------------------


//----------------------------------- PAGE OUT PUT ---------------------------------------
?>
<html>
<head>
<title>:: <?php echo $siteName; ?> ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $charset; ?>">
<link rel="stylesheet" type="text/css" href="style.css">
<style type="text/css">
fieldset {
	padding: 5px;
    -moz-border-radius: 5px;
	border: 1px solid #CCCCCC;
	background-color:#fff;
}

fieldset legend {
	padding-bottom: 3px;
	font-weight:bold;
	font-size:12px;
	color:#000000;
	line-height: 16px;
	text-align: justify;
	font-variant: small-caps;
}
.tabele {
	background-color: #F6F6F6;
	border-top: 1px solid  #DADADA;
	border-right: 1px solid  #DADADA;
	border-bottom: 2px solid  #DADADA;
	border-left: 2px solid  #DADADA;
	padding-left: 2px;
}
.Cat {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #990000;
	font-weight: bold;
}
.Cat a {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #990000;
	font-weight: bold;
}
.container {
	PADDING-RIGHT: 0px; DISPLAY: block; PADDING-LEFT: 0px; FONT-SIZE: smaller; BACKGROUND: none transparent scroll repeat 0% 0%; PADDING-BOTTOM: 0px; MARGIN: 0px auto; WIDTH: 60px; PADDING-TOP: 0px; TEXT-ALIGN: center;
}
</style>
<script type="text/javascript" src="javascripts/tabber.js"></script>
<script type="text/javascript">

/* Optional: Temporarily hide the "tabber" class so it does not "flash"
   on the page as plain HTML. After tabber runs, the class is changed
   to "tabberlive" and it will appear. */

document.write('<style type="text/css">.tabber{display:none;}<\/style>');
</script>
<?php
if (isset($jsinclude))
	{
	for ($i=0; $i<count($jsinclude); $i++)
		{
		echo "<script language=\"JavaScript\" type=\"text/javascript\" ";
		echo "src=\"".$jscript_dir.$jsinclude[$i]."\"></script>\n";
		}
	}
if (isset($head_info))
	echo $head_info;
?>
<script>
function updateAttributes()
	{
	document.myform.bgcolor.value=MyBgColor;
	document.myform.background.value=MyBgImg;
	}

//Ham hien thi menu JS
var curbgColor;
var curbdColor;
//Doi mau nen
function change(obj,color)
	{
	curbgColor=obj.bgColor;
	obj.bgColor=color;
	}
function undo(obj)
	{
	obj.bgColor=curbgColor;
	}
//Doi mau vien
function changebd(obj,color)
	{
	curbdColor=obj.style.borderColor;
	obj.style.borderColor=color;
	}
function undobd(obj)
	{
	obj.style.borderColor=curbdColor;
	}
//Gan gia tri bien
function get(formname,element)
	{
	var varname=eval ("document." + formname + "." + element + ".value;");
	return varname;
	}
</script>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#999999">
<center>
<table width="1000" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" style="border-left: solid 0px #336699; border-right: solid 0px #336699;">
	<tr>
	  <td align="center" valign="top"><?php include ('top.php'); ?></td>
	</tr>
	
	<tr>
		<td align="center" valign="middle" style="padding: 6 0 6 0">
		<table width="90%" cellpadding="0" cellspacing="0" border="0" class="form_table">
		<tr><td style="padding: 0 0 0 0"><p>
		<strong>&raquo; <?php echo $strLanguage.' </strong>[ '.$languages_name.' ]'; ?>
		<strong>&raquo; <?php echo $strTheme.' </strong>[ Original ]'; ?>
		</table>
		</td>
	</tr>
		
  	<tr>
	    <td valign="top" align="center">
			<table width="90%" border="0" cellpadding="0" cellspacing="0" background="images/tablebg.gif">
		    <tr>
		    <td width="32" align="left" valign="top">
			<img src="images/red_lefttitle.jpg" width="32" height="26"></td>
		    <td background="images/titlebg.gif" width="100%">
		    <p class="bigtitle">:: <?php if (isset($caption)) echo $caption; ?> ::</p></td>
		    <td width="32" align="right" valign="top">
			<img src="images/red_righttitle.jpg" width="32" height="26"></td>
		    </tr>
		    
			<tr><td height="20" colspan="3" align="center" valign="middle">&nbsp;
			</td>
			</tr>
			
			<tr align="center" valign="top">
		    <td colspan="3" align="center" valign="top">
			<?php
			if (isset($phpinclude))
				{
				eval('require ("'.$module_dir.$phpinclude.'");');
				}
			?>
			</td>
		    </tr>
		    
			<tr><td colspan="3" style="border-bottom: solid 2px #FF3300;" height="10">&nbsp;</td></tr>
			
			</table>
		</td>
  	</tr>
  
	<tr><td height="20"></td></tr>
	
	<tr>
		<td>
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
    	<tr><td><?php include ('bottom.php'); ?></td></tr>
    	</table>
    	</td>
    </tr>
    
</table>
</center>
</body>
</html>
