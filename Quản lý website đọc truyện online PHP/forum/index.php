<?php
/**
* @package     JohnCMS
* @link        http://johncms.com
* @copyright   Copyright (C) 2008-2011 JohnCMS Community
* @license     LICENSE.txt (see attached file)
* @version     VERSION.txt (see attached file)
* @author      http://johncms.com/about
*/

define('_IN_JOHNCMS', 1);

require_once('../incfiles/core.php');
 if(!$website)
require(''.$_SERVER['DOCUMENT_ROOT'].'/incfiles/websiteb.php');
$lng_forum = core::load_lng('forum');
if (isset($_SESSION['ref']))
unset($_SESSION['ref']);

/*
-----------------------------------------------------------------
Настройки форума
-----------------------------------------------------------------
*/
$set_forum = $user_id && !empty($datauser['set_forum']) ? unserialize($datauser['set_forum']) : array(
'farea' => 0,
'upfp' => 0,
'preview' => 1,
'postclip' => 1,
'postcut' => 2
);

/*
-----------------------------------------------------------------
Список расширений файлов, разрешенных к выгрузке
-----------------------------------------------------------------
*/
// Файлы архивов
$ext_arch = array(
'zip',
'rar',
'7z',
'tar',
'gz'
);
// Звуковые файлы
$ext_audio = array(
'mp3',
'amr'
);
// Файлы документов и тексты
$ext_doc = array(
'txt',
'pdf',
'doc',
'rtf',
'djvu',
'xls'
);
// Файлы Java
$ext_java = array(
'jar',
'jad'
);
// Файлы картинок
$ext_pic = array(
'jpg',
'jpeg',
'gif',
'png',
'bmp'
);
// Файлы SIS
$ext_sis = array(
'sis',
'sisx'
);
// Файлы видео
$ext_video = array(
'3gp',
'avi',
'flv',
'mpeg',
'mp4'
);
// Файлы Windows
$ext_win = array(
'exe',
'msi'
);
// Другие типы файлов (что не перечислены выше)
$ext_other = array('wmf');

/*
-----------------------------------------------------------------
Ограничиваем доступ к Форуму
-----------------------------------------------------------------
*/
$error = '';
if (!$set['mod_forum'] && $rights < 7)
$error = $lng_forum['forum_closed'];
elseif ($set['mod_forum'] == 1 && !$user_id)
$error = $lng['access_guest_forbidden'];
if ($error) {
require('../incfiles/head.php');
echo '<div class="rmenu"><p>' . $error . '</p></div>';
require('../incfiles/end.php');
exit;
}

$headmod = $id ? 'forum,' . $id : 'forum';

/*
-----------------------------------------------------------------
Заголовки страниц форума
-----------------------------------------------------------------
*/
if (empty($id)) {
$textl = '' . $lng['forum'] . '';
} else {
$req = mysql_query("SELECT `text` FROM `forum` WHERE `id`= '" . $id . "' AND `website` = '$website'");
$res = mysql_fetch_assoc($req);
	mySQL_free_result($req);

$hdr = strtr($res['text'], array(
'&quot;' => '',
'&amp;' => '',
'&lt;' => '',
'&gt;' => '',
'&#039;' => '',
'&acirc;' => 'a', '&aacute;' => 'á', '&agrave;' => 'à', '&acirc;̉' => 'ả', '&atilde;' => 'ã', '&acirc;̣' => 'ạ',
'&Acirc;' => 'A', '&Aacute;' => 'Á', '&Agrave;' => 'À', '&Acirc;̉' => 'Ả', '&Atilde;' => 'Ã', '&Acirc;̣' => 'Ạ',
'&acirc;' => 'ă', '&acirc;́' => 'ắ', '&acirc;̀' => 'ằ', '&acirc;̉' => 'ẳ', '&acirc;̃' => 'ẵ', '&acirc;̣' => 'ặ',
'&Acirc;' => 'Ă', '&Acirc;́' => 'Ắ', '&Acirc;̀' => 'Ằ', '&Acirc;̉' => 'Ẳ', '&Acirc;̃' => 'Ẵ', '&Acirc;̣' => 'Ặ',
'&acirc;' => 'â', '&acirc;́' => 'ấ', '&acirc;̀' => 'ầ', '&acirc;̉' => 'ẩ', '&acirc;̃' => 'ẫ', '&acirc;̣' => 'ậ',
'&Acirc;' => 'Â', '&Acirc;́' => 'Ấ', '&Acirc;̀' => 'Ầ', '&Acirc;̉' => 'Ẩ', '&Acirc;̃' => 'Ẫ', '&Acirc;̣' => 'Ậ',
'&ecirc;' => 'e', '&eacute;' => 'é', '&eagrave;' => 'è', '&ecirc;̉' => 'ẻ', '&etilde;' => 'ẽ', '&ecirc;̣' => 'ẹ',
'&Ecirc;' => 'E', '&Eacute;' => 'É', '&Egrave;' => 'È', '&Ecirc;̉' => 'Ẻ', '&Etilde;' => 'Ẽ', '&Ecirc;̣' => 'Ẹ',
'&ecirc;' => 'ê', '&ecirc;́' => 'ế', '&ecirc;̀' => 'ề', '&ecirc;̉' => 'ể', '&ecirc;̃' => 'ễ', '&ecirc;̣' => 'ệ',
'&Ecirc;' => 'Ê', '&Ecirc;́' => 'Ế', '&Ecirc;̀' => 'Ề', '&Ecirc;̉' => 'Ể', '&Ecirc;̃' => 'Ễ', '&Ecirc;̣' => 'Ệ',
'&icirc;' => 'i', '&iacute;' => 'í', '&igrave;' => 'ì', '&icirc;̉' => 'ỉ', '&itilde;' => 'ĩ', '&icirc;̣' => 'ị',
'&Icirc;' => 'I', '&Iacute;' => 'Í', '&Igrave;' => 'Ì', '&Icirc;̉' => 'Ỉ', '&Itilde;' => 'Ĩ', '&Icirc;̣' => 'Ị',
'&ucirc;' => 'u', '&uacute;' => 'ú', '&ugrave;' => 'ù', '&ucirc;̉' => 'ủ', '&utilde;' => 'ũ', '&ucirc;̣' => 'ụ',
'&Ucirc;' => 'U', '&Uacute;' => 'Ú', '&Ugrave;' => 'Ù', '&Ucirc;̉' => 'Ủ', '&Utilde;' => 'Ũ', '&Ucirc;̣' => 'Ụ',
'&ucirc;' => 'ư', '&ucirc;́' => 'ứ', '&ucirc;̀' => 'ừ', '&ucirc;̉' => 'ử', '&ucirc;̃' => 'ữ', '&ucirc;̣' => 'ự',
'&Ucirc;' => 'Ư', '&Ucirc;́' => 'Ứ', '&Ucirc;̀' => 'Ừ', '&Ucirc;̉' => 'Ử', '&Ucirc;̃' => 'Ữ', '&Ucirc;̣' => 'Ự',
'&ocirc;' => 'o', '&oacute;' => 'ó', '&ograve;' => 'ò', '&ocirc;̉' => 'ỏ', '&otilde;' => 'õ', '&ocirc;̣' => 'ọ',
'&Ocirc;' => 'O', '&Oacute;' => 'Ó', '&Ograve;' => 'Ó', '&Ocirc;̉' => 'Ỏ', '&Otilde;' => 'Õ', '&Ocirc;̣' => 'Ọ',
'&ocirc;' => 'ơ', '&ocirc;́' => 'ớ', '&ocirc;̀' => 'ờ', '&ocirc;̉' => 'ở', '&ocirc;̃' => 'ỡ', '&ocirc;̣' => 'ợ',
'&Ocirc;' => 'Ơ', '&Ocirc;́' => 'Ớ', '&Ocirc;̀' => 'Ờ', '&Ocirc;̉' => 'Ở', '&Ocirc;̃' => 'Ỡ', '&Ocirc;̣' => 'Ợ',
'&ocirc;' => 'ô', '&ocirc;́' => 'ố', '&ocirc;̀' => 'ồ', '&ocirc;̉' => 'ổ', '&ocirc;̃' => 'ỗ', '&ocirc;̣' => 'ộ',
'&Ocirc;' => 'Ô', '&Ocirc;́' => 'Ố', '&Ocirc;̀' => 'Ồ', '&Ocirc;̉' => 'Ổ', '&Ocirc;̃' => 'Ỗ', '&Ocirc;̣' => 'Ộ',
'&ycirc;' => 'y', '&yacute;' => 'ý', '&ygrave;' => 'ỳ', '&ycirc;̉' => 'ỷ', '&ytilde;' => 'Ỹ', '&ycirc;̣' => 'ỵ',
'&Ycirc;' => 'Y', '&Yacute;' => 'Ý', '&Ygrave;' => 'Ỳ', '&Ycirc;̉' => 'Ỷ', '&Ytilde;' => 'Ỹ', '&Ycirc;̣' => 'Ỵ'
));
$hdr = mb_substr($hdr, 0, 50);
$hdr = functions::checkout($hdr);
$textl = mb_strlen($res['text']) > 50 ? $hdr . '...' : $hdr;
if($res['type']=='t') { $id_post = mysql_result(mysql_query("SELECT MIN(`id`) FROM `forum` WHERE `type`= 'm' AND `refid`='$id';"), 0); $q = mysql_fetch_assoc(mysql_query("SELECT `text` FROM `forum` WHERE `id`= '" . $id_post . "' LIMIT 1;")); functions::create_keywords($q['text'], ', ', 20); }
}

/*
-----------------------------------------------------------------
Переключаем режимы работы
-----------------------------------------------------------------
*/
$mods = array(
'addfile',
'addvote',
'close',
'deltema',
'delvote',
'editpost',
'editvote',
'file',
'files',
'filter',
'loadtem',
'massdel',
'moders',
'new',
'nt',
'per',
'post',
'ren',
'restore',
'say',
'tema',
'users',
'vip',
'vote',
'who',
'curators',
'portal',
'kiemduyet',
'danhdau',
);
if ($act && ($key = array_search($act, $mods)) !== false && file_exists('includes/' . $mods[$key] . '.php')) {
require('includes/' . $mods[$key] . '.php');
} else {
require('../incfiles/head.php');

/*
-----------------------------------------------------------------
Если форум закрыт, то для Админов выводим напоминание
-----------------------------------------------------------------
*/
if (!$set['mod_forum']) echo '<div class="alarm">' . $lng_forum['forum_closed'] . '</div>';
elseif ($set['mod_forum'] == 3) echo '<div class="rmenu">' . $lng['read_only'] . '</div>';
if (!$user_id) {
if (isset($_GET['newup']))
$_SESSION['uppost'] = 1;
if (isset($_GET['newdown']))
$_SESSION['uppost'] = 0;
}
if ($id) {
$type = mysql_query("SELECT * FROM `forum` WHERE `id`= '$id' AND `website` = '$website'");
if (!mysql_num_rows($type)) {
// Если темы не существует, показываем ошибку
echo functions::display_error($lng_forum['error_topic_deleted'], '<a href="index.php">' . $lng['to_forum'] . '</a>');
require('../incfiles/end.php');
exit;
}
$type1 = mysql_fetch_assoc($type);
	mySQL_free_result($type);
/*
-----------------------------------------------------------------
Фиксация факта прочтения Топика
-----------------------------------------------------------------
*/
if ($user_id && $type1['type'] == 't') {
$req_r = mysql_query("SELECT * FROM `cms_forum_rdm` WHERE `topic_id` = '$id' AND `user_id` = '$user_id' AND `website` = '$website' LIMIT 1");
if (mysql_num_rows($req_r)) {
$res_r = mysql_fetch_assoc($req_r);
mySQL_free_result($req_r);
if ($type1['time'] > $res_r['time'])
mysql_query("UPDATE `cms_forum_rdm` SET `time` = '" . time() . "' WHERE `topic_id` = '$id' AND `user_id` = '$user_id' AND `website` = '$website' LIMIT 1");
} else {
mysql_query("INSERT INTO `cms_forum_rdm` SET `topic_id` = '$id',
					`website` = '$website', `user_id` = '$user_id', `time` = '" . time() . "' AND `website` = '$website'");
}
}

/*
-----------------------------------------------------------------
Получаем структуру форума
-----------------------------------------------------------------
*/
$res = true;
$parent = $type1['refid'];
while ($parent != '0' && $res != false) {
$req = mysql_query("SELECT * FROM `forum` WHERE `id` = '$parent' AND `website` = '$website' LIMIT 1");
$res = mysql_fetch_assoc($req);
mySQL_free_result($req );
if ($res['type'] == 'f' || $res['type'] == 'r')
$tree[] = '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$home.'/forum/' . functions::mikdaik($res['text']) . '_' . $parent . '.html"><span itemprop="title">' . $res['text'] . '</span></a></span>';
$parent = $res['refid'];
}
$tree[] = '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="index.php" itemprop="url"><span itemprop="title">' . $lng['forum'] . '</span></a></span>';
krsort($tree);
if ($type1['type'] != 't' && $type1['type'] != 'm')
$tree[] = '<b>' . bbcode::tags($type1['text']) . '</b>';

/*
-----------------------------------------------------------------
Счетчик файлов и ссылка на них
-----------------------------------------------------------------
*/
$sql = ($rights == 9) ? "" : " AND `del` != '1'";
if ($type1['type'] == 'f') {
$count = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_forum_files` WHERE `website` = '$website' AND `cat` = '$id'" . $sql), 0);
if ($count > 0)
$filelink = '<a href="index.php?act=files&amp;c=' . $id . '">' . $lng_forum['files_category'] . '</a>';
} elseif ($type1['type'] == 'r') {
$count = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_forum_files` WHERE `website` = '$website' AND `subcat` = '$id'" . $sql), 0);
if ($count > 0)
$filelink = '<a href="index.php?act=files&amp;s=' . $id . '">' . $lng_forum['files_section'] . '</a>';
} elseif ($type1['type'] == 't') {
$count = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_forum_files` WHERE `website` = '$website' AND `topic` = '$id'" . $sql), 0);
if ($count > 0)
$filelink = '<a href="index.php?act=files&amp;t=' . $id . '">' . $lng_forum['files_topic'] . '</a>';
}
$filelink = isset($filelink) ? $filelink . '&#160;<span class="red">(' . $count . ')</span>' : false;

/*
-----------------------------------------------------------------
Счетчик "Кто в теме?"
-----------------------------------------------------------------
*/
$wholink = false;
if ($user_id && $type1['type'] == 't') {
$online_u = mysql_result(mysql_query("SELECT COUNT(*) FROM `users` WHERE `website` = '$website' AND `lastdate` > " . (time() - 300) . " AND `place` = 'forum,$id'"), 0);
$online_g = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `website` = '$website' AND `lastdate` > " . (time() - 300) . " AND `place` = 'forum,$id'"), 0);
$wholink = '<a href="index.php?act=who&amp;id=' . $id . '">' . $lng_forum['who_here'] . '?</a>&#160;<span class="red">(' . $online_u . '&#160;/&#160;' . $online_g . ')</span><br/>';
}

/*
-----------------------------------------------------------------
Выводим верхнюю панель навигации
-----------------------------------------------------------------
*/
//Đếm lượt
mysql_query("UPDATE forum SET view = view + 1 WHERE `website` = '$website' AND id
= $id");
echo '<div class="mainblok"><div class="phdr">' . functions::display_menu($tree) . '</div>' .
'<div class="topmenu"><a href="search.php?id=' . $id . '">' . $lng['search'] . '</a>' . ($filelink ? ' | ' . $filelink : '') . ($wholink ? ' | ' . $wholink : '') . '</div></div>';

/*
-----------------------------------------------------------------
Отрбражаем содержимое форума
-----------------------------------------------------------------
*/
switch ($type1['type']) {
case 'f':
/*
-----------------------------------------------------------------
Список разделов форума
-----------------------------------------------------------------
*/
$req = mysql_query("SELECT `id`, `text`, `soft` FROM `forum` WHERE `website` = '$website' AND `type`='r' AND `refid`='$id' ORDER BY `realid`");
$total = mysql_num_rows($req);
if ($total) {
$i = 0;
while (($res = mysql_fetch_assoc($req)) !== false) {
echo $i % 2 ? '<div class="list2">' : '<div class="list1">';
$coltem = mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` WHERE `type` = 't' AND `website` = '$website' AND  `refid` = '" . $res['id'] . "'"), 0);
echo '<a href="'.$home.'/forum/' . functions::mikdaik($res['text']) . '_'.$res['id'].'.html">' . $res['text'] . '</a>';
if ($coltem)
echo " [$coltem]";
if (!empty($res['soft']))
echo '<div class="sub"><span class="gray">' . $res['soft'] . '</span></div>';
echo '</div>';
++$i;
}
mySQL_free_result($req );
unset($_SESSION['fsort_id']);
unset($_SESSION['fsort_users']);
} else {
echo '<div class="menu"><p>' . $lng_forum['section_list_empty'] . '</p></div>';
}
echo '<div class="phdr">' . $lng['total'] . ': ' . $total . '</div>';
break;

case 'r':
/*
-----------------------------------------------------------------
Список топиков
-----------------------------------------------------------------
*/
$total = mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website` = '$website' AND `type`='t' AND `refid`='$id'" . ($rights >= 7 ? '' : " AND `close`!='1'")), 0);
if (($user_id && !isset($ban['1']) && !isset($ban['11']) && $set['mod_forum'] != 3) || core::$user_rights) {
// Кнопка создания новой темы
echo '<div class="gmenu"><form action="index.php?act=nt&amp;id=' . $id . '" method="post"><input type="submit" value="' . $lng_forum['new_topic'] . '" /></form></div>';
}
if ($total) {
if($set['kiemduyet'] !=0)
$req = mysql_query("SELECT * FROM `forum` WHERE `website` = '$website' AND `kiemduyet`='1' AND `type`='t'" . ($rights >= 7 ? '' : " AND `close`!='1'") . " AND `refid`='$id' ORDER BY `vip` DESC, `time` DESC LIMIT $start, $kmess");
else 
$req = mysql_query("SELECT * FROM `forum` WHERE `website` = '$website' AND `type`='t'" . ($rights >= 7 ? '' : " AND `close`!='1'") . " AND `refid`='$id' ORDER BY `vip` DESC, `time` DESC LIMIT $start, $kmess");

$i = 0;
while (($res = mysql_fetch_assoc($req)) !== false) {
if ($res['close'])
echo '<div class="rmenu">';
else
echo $i % 2 ? '<div class="list2">' : '<div class="list1">';
$nikuser = mysql_query("SELECT `from` FROM `forum` WHERE `type` = 'm' AND `website` = '$website' AND  `close` != '1' AND `refid` = '" . $res['id'] . "' ORDER BY `time` DESC LIMIT 1");
$nam = mysql_fetch_assoc($nikuser);
$colmes = mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website` = '$website' AND `type`='m' AND `refid`='" . $res['id'] . "'" . ($rights >= 7 ? '' : " AND `close` != '1'"));
$colmes1 = mysql_result($colmes, 0);
$cpg = ceil($colmes1 / $kmess);
$np = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_forum_rdm` WHERE `website` = '$website' AND `time` >= '" . $res['time'] . "' AND `topic_id` = '" . $res['id'] . "' AND `user_id`='$user_id'"), 0);
// Значки
$icons = array(
($np ? (!$res['vip'] ? '<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/op.gif" alt=""/>' : '') : '<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/np.gif" alt=""/>'),
($res['vip'] ? '<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/pt.gif" alt=""/>' : ''),
($res['realid'] ? '<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/rate.gif" alt=""/>' : ''),
($res['edit'] ? '<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/tz.gif" alt=""/>' : '')
);
echo functions::display_menu($icons, '&#160;', '&#160;');
echo '<a href="'.$home.'/forum/' . functions::mikdaik($res['text']) . '_' . $res['id'] . '.html">' . bbcode::tags($res['text']) . '</a> [' . $colmes1 . ']';
if ($cpg > 1) {
echo '<a href="'.$home.'/forum/' . functions::mikdaik($res['text']) . '_' . $res['id'] . '_p' . $cpg . '.html">*&gt;&gt;</a>';
}
echo '<div class="sub">';
echo $res['from'];
if (!empty($nam['from'])) {
echo '&#160;/&#160;' . $nam['from'];
}
echo ' <span class="gray">(' . functions::display_date($res['time']) . ')</span></div></div>';
++$i;
}
mySQL_free_result($req );
unset($_SESSION['fsort_id']);
unset($_SESSION['fsort_users']);
} else {
echo '<div class="menu"><p>' . $lng_forum['topic_list_empty'] . '</p></div>';
}
echo '<div class="phdr">' . $lng['total'] . ': ' . $total . '</div>';
if ($total > $kmess) {
echo '<div class="topmenu">' . functions::display_pagination('index.php?id=' . $id . '&amp;', $start, $total, $kmess) . '</div>' .
'<p><form action="index.php?id=' . $id . '" method="post">' .
'<input type="text" name="page" size="2"/>' .
'<input type="submit" value="' . $lng['to_page'] . ' &gt;&gt;"/>' .
'</form></p>';
}
break;

case 't':
/*
-----------------------------------------------------------------
Читаем топик
-----------------------------------------------------------------
*/
$filter = isset($_SESSION['fsort_id']) && $_SESSION['fsort_id'] == $id ? 1 : 0;
$sql = '';
if ($filter && !empty($_SESSION['fsort_users'])) {
// Подготавливаем запрос на фильтрацию юзеров
$sw = 0;
$sql = ' AND (';
$fsort_users = unserialize($_SESSION['fsort_users']);
foreach ($fsort_users as $val) {
if ($sw)
$sql .= ' OR ';
$sortid = intval($val);
$sql .= "`forum`.`user_id` = '$sortid'";
$sw = 1;
}
$sql .= ')';
}
if ($user_id && !$filter) {
// Фиксация факта прочтения топика
}
if ($rights < 7 && $type1['close'] == 1) {
echo '<div class="rmenu"><p>' . $lng_forum['topic_deleted'] . '<br/><a href="?id=' . $type1['refid'] . '">' . $lng_forum['to_section'] . '</a></p></div>';
require('../incfiles/end.php');
exit;
}

$checkthankdau = mysql_query('SELECT COUNT(*) FROM `forum_thank` WHERE `website` = "'.$website.'" AND `userthank` = "' . $user_id . '" and `topic` = "' . $_GET['thanks'] . '" and `user` = "' . $_GET['user'] . '"');
if ($user_id && $user_id != $_GET['user'] && (mysql_result($checkthankdau, 0) < 1)) {
if ((isset ($_GET['thank']))&&(isset ($_GET['user']))&&(isset ($_GET['thanks']))) {
echo '<div class="rmenu" id="thanksyou"><b>Bạn đã cảm ơn bài viết thành công.</b></div>';
mysql_query("INSERT INTO `forum_thank` SET
`user` = '".trim(addslashes($_GET['user']))."',
`topic` = '".trim(addslashes($_GET['thanks']))."' ,
`time` = '$realtime',
					`website` = '$website',
`userthank` = '$user_id',
`chude` = '".addslashes($_GET["id"])."'
");

mysql_query("INSERT INTO `thongbao` SET
`user` = '".addslashes($login)."',
`send` = '".addslashes($_GET['id'])."' ,
`website` = '$website',
`user_id` = '".trim(addslashes($_GET['user']))."',
`loai`='0'
");
$congcamon=mysql_fetch_assoc(mysql_query('SELECT * FROM `users` WHERE `website` = "'.$website.'" AND `id` = "' . trim(addslashes($_GET['user'])) . '"'));
mysql_query("UPDATE `users` SET `thank_duoc`='" . ($congcamon['thank_duoc'] + 1) . "' WHERE `website` = '$website' AND `id` = '" . trim(addslashes($_GET['user'])) . "'");
mysql_query("UPDATE `users` SET `thank_di`='" . ($datauser['thank_di'] + 1) . "' WHERE `website` = '$website' AND `id` = '" . $user_id . "'");
mysql_query("UPDATE `users` SET `balans`='" . ($congcamon['balans'] + 20) . "' WHERE `website` = '$website' AND `id` = '" . trim(addslashes($_GET['user'])) . "'");
}
 if ((isset ($_GET['unthank']))&&(isset ($_GET['user']))&&(isset ($_GET['unthanks']))) {
 echo '<div class="rmenu" id="thanksyou"><b>Bạn đã bỏ cảm ơn bài viết thành công.</b></div>';

                  $checkthank = mysql_query('SELECT COUNT(*) FROM `forum_thank` WHERE `website` = "'.$website.'" AND`userthank` = "' . $user_id . '" and `topic` = "' . trim($_GET['unthanks']) . '" and `user` = "' . trim($_GET['user']) . '"');
                  $thankcheck = mysql_result($checkthank, 0);
                  if ($thankcheck > 0 && $user_id != $_GET['user']) {
                      mysql_query("DELETE FROM `forum_thank` WHERE `user` = '".trim(addslashes($_GET['user']))."' AND `website` = '$website' AND`userthank` = '$user_id' AND `topic` = '".trim(addslashes($_GET['unthanks']))."'");
                      mysql_query("OPTIMIZE TABLE `forum_thank`");
                      $datathankuser = functions::get_user(trim($_GET['user']));
                      mysql_query("UPDATE `users` SET `thank_duoc`='" . ($datathankuser['thank_duoc'] - 1) . "' WHERE `website` = '$website' AND`id` = '" . trim(addslashes($_GET['user'])) . "'");
                      mysql_query("UPDATE `users` SET `thank_di`='" . ($datauser['thank_di'] - 1) . "' WHERE `website` = '$website' AND`id` = '" . $user_id . "'");
                      mysql_query("UPDATE `users` SET `postforum`='" . ($datathankuser['postforum'] - 3) . "' WHERE `website` = '$website' AND`id` = '" . trim(addslashes($_GET['user'])) . "'");
                  }
                }
}
// Счетчик постов темы
$colmes = mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website` = '$website' AND `type`='m'$sql AND `refid`='$id'" . ($rights >= 7 ? '' : " AND `close` != '1'")), 0);
if ($start > $colmes) $start = $colmes - $kmess;
// Выводим название топика
echo '<div class="mainblok"><div class="phdr"><a name="up" id="up"></a><a href="#down"><img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/down.png" alt="Вниз" width="20" height="10" border="0"/></a>&#160;&#160;<b>' . bbcode::tags($type1['text']) . '</b></div>';
if ($colmes > $kmess)
echo '<div class="topmenu">' . functions::display_pagination2(''.$home.'/forum/'.functions::mikdaik($type1["text"]).'_' . $id, $start, $colmes, $kmess) . '</div>';
echo '</div>';
// Метки удаления темы
if ($type1['close'])
echo '<div class="rmenu">' . $lng_forum['topic_delete_who'] . ': <b>' . $type1['close_who'] . '</b></div>';
elseif (!empty($type1['close_who']) && $rights >= 7)
echo '<div class="gmenu"><small>' . $lng_forum['topic_delete_whocancel'] . ': <b>' . $type1['close_who'] . '</b></small></div>';
if (!empty($type1['kiemduyet_who'])) {
echo '<div class="gmenu">Chủ đề được kiểm duyệt bởi: <b>' . $type1['kiemduyet_who'] . '</b></div>';
}
// Метки закрытия темы
if ($type1['edit'])
echo '<div class="rmenu">' . $lng_forum['topic_closed'] . '</div>';
/*
-----------------------------------------------------------------
Блок голосований
-----------------------------------------------------------------
*/
if ($type1['realid']) {
$clip_forum = isset($_GET['clip']) ? '&amp;clip' : '';
$vote_user = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_forum_vote_users` WHERE `website` = '$website' AND `user`='$user_id' AND `topic`='$id'"), 0);
$topic_vote = mysql_fetch_assoc(mysql_query("SELECT `name`, `time`, `count` FROM `cms_forum_vote` WHERE `website` = '$website' AND `type`='1' AND `topic`='$id' LIMIT 1"));
echo '<div  class="gmenu"><b>' . functions::checkout($topic_vote['name']) . '</b><br />';
$vote_result = mysql_query("SELECT `id`, `name`, `count` FROM `cms_forum_vote` WHERE `website` = '$website' AND `type`='2' AND `topic`='" . $id . "' ORDER BY `id` ASC");
if (!$type1['edit'] && !isset($_GET['vote_result']) && $user_id && $vote_user == 0) {
// Выводим форму с опросами
echo '<form action="index.php?act=vote&amp;id=' . $id . '" method="post">';
while (($vote = mysql_fetch_assoc($vote_result)) !== false) {
echo '<input type="radio" value="' . $vote['id'] . '" name="vote"/> ' . functions::checkout($vote['name'], 0, 1) . '<br />';
}
echo '<p><input type="submit" name="submit" value="' . $lng['vote'] . '"/><br /><a href="index.php?id=' . $id . '&amp;start=' . $start . '&amp;vote_result' . $clip_forum .
'">' . $lng_forum['results'] . '</a></p></form></div>';
} else {
// Выводим результаты голосования
echo '<small>';
while (($vote = mysql_fetch_assoc($vote_result)) !== false) {
$count_vote = $topic_vote['count'] ? round(100 / $topic_vote['count'] * $vote['count']) : 0;
echo functions::checkout($vote['name'], 0, 1) . ' [' . $vote['count'] . ']<br />';
echo '<img src="vote_img.php?img=' . $count_vote . '" alt="' . $lng_forum['rating'] . ': ' . $count_vote . '%" /><br />';
}
echo '</small></div><div class="bmenu">' . $lng_forum['total_votes'] . ': ';
if ($datauser['rights'] > 6)
echo '<a href="index.php?act=users&amp;id=' . $id . '">' . $topic_vote['count'] . '</a>';
else
echo $topic_vote['count'];
echo '</div>';
if ($user_id && $vote_user == 0)
echo '<div class="bmenu"><a href="index.php?id=' . $id . '&amp;start=' . $start . $clip_forum . '">' . $lng['vote'] . '</a></div>';
}
}
$curators = !empty($type1['curators']) ? unserialize($type1['curators']) : array();
$curator = false;
if ($rights < 6 && $rights != 3 && $user_id) {
if (array_key_exists($user_id, $curators)) $curator = true;
}
/*
-----------------------------------------------------------------
Фиксация первого поста в теме
-----------------------------------------------------------------
*/
if (($set_forum['postclip'] == 2 && ($set_forum['upfp'] ? $start < (ceil($colmes - $kmess)) : $start > 0)) || isset($_GET['clip'])) {
$postreq = mysql_query("SELECT `forum`.*, `users`.`sex`, `users`.`rights`, `users`.`lastdate`, `users`.`status`, `users`.`datereg`
FROM `forum` LEFT JOIN `users` ON `forum`.`user_id` = `users`.`id`
WHERE `forum`.`type` = 'm' AND `forum`.`refid` = '$id'" . ($rights >= 7 ? "" : " AND `forum`.`website` = '$website' AND  `forum`.`close` != '1'") . "
ORDER BY `forum`.`id` LIMIT 1");

$postres = mysql_fetch_assoc($postreq);
mySQL_free_result($postreq);
echo '<div class="topmenu"><p>';
if ($postres['sex'])
echo '<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/' . ($postres['sex'] == 'm' ? 'm' : 'w') . ($postres['datereg'] > time() - 86400 ? '_new.png" width="14"' : '.png" width="10"') . ' height="10"/>&#160;';
else
echo '<img src="http://infoviet.net/images/del.png" width="10" height="10" alt=""/>&#160;';
if ($user_id && $user_id != $postres['user_id']) {
echo '<a href="../users/profile.php?user=' . $postres['user_id'] . '&amp;fid=' . $postres['id'] . '"><b>' . $postres['from'] . '</b></a> ' .
'<a href="index.php?act=say&amp;id=' . $postres['id'] . '&amp;start=' . $start . '"> ' . $lng_forum['reply_btn'] . '</a> ' .
'<a href="index.php?act=say&amp;id=' . $postres['id'] . '&amp;start=' . $start . '&amp;cyt"> ' . $lng_forum['cytate_btn'] . '</a> ';
} else {
echo '<b>' . $postres['from'] . '</b> ';
}
$user_rights = array(
1 => 'Kil',
3 => 'Mod',
6 => 'Smd',
7 => 'Adm',
8 => 'SV'
);
echo @$user_rights[$postres['rights']];
echo (time() > $postres['lastdate'] + 300 ? '<span class="red"> [Off]</span>' : '<span class="green"> [ON]</span>');
echo ' <span class="gray">(' . functions::display_date($postres['time']) . ')</span><br/>';
if ($postres['close']) {
echo '<span class="red">' . $lng_forum['post_deleted'] . '</span><br/>';
}
echo functions::checkout(mb_substr($postres['text'], 0, 500), 0, 2);
if (mb_strlen($postres['text']) > 500)
echo '...<a href="index.php?act=post&amp;id=' . $postres['id'] . '">' . $lng_forum['read_all'] . '</a>';
echo '</p></div>';
}
if ($filter)
echo '<div class="rmenu">' . $lng_forum['filter_on'] . '</div>';
// Задаем правила сортировки (новые внизу / вверху)
if ($user_id)
$order = $set_forum['upfp'] ? 'DESC' : 'ASC';
else
$order = ((empty($_SESSION['uppost'])) || ($_SESSION['uppost'] == 0)) ? 'ASC' : 'DESC';
// Запрос в базу
                $req = mysql_query("SELECT `forum`.*, `users`.`sex`, `users`.`rights`, `users`.`chuki`, `users`.`lastdate`, `users`.`status`, `users`.`datereg`
                FROM `forum` LEFT JOIN `users` ON `forum`.`user_id` = `users`.`id`
                WHERE `forum`.`type` = 'm' AND `forum`.`website` = '$website' AND `forum`.`refid` = '$id'"
                                   . ($rights >= 7 ? "" : " AND `forum`.`close` != '1'") . "$sql ORDER BY `forum`.`id` $order LIMIT $start, $kmess");
// Верхнее поле "Написать"
if (($user_id && !$type1['edit'] && $set_forum['upfp'] && $set['mod_forum'] != 3) || ($rights >= 7 && $set_forum['upfp'])) {
echo '<div class="gmenu"><form name="form1" action="index.php?act=say&amp;id=' . $id . '" method="post">';
if ($set_forum['farea']) {
echo '<p>' .
(!$is_mobile ? bbcode::auto_bb('form1', 'msg') : '') .
'<textarea rows="' . $set_user['field_h'] . '" name="msg"></textarea></p>' .
'<p><input type="checkbox" name="addfiles" value="1" /> ' . $lng_forum['add_file'] .
($set_user['translit'] ? '<br /><input type="checkbox" name="msgtrans" value="1" /> ' . $lng['translit'] : '') .
'</p><p><input type="submit" name="submit" value="' . $lng['write'] . '" style="width: 107px; cursor: pointer;"/> ' .
($set_forum['preview'] ? '<input type="submit" value="' . $lng['preview'] . '" style="width: 107px; cursor: pointer;"/>' : '') .
'</p></form></div>';
} else {
echo '<p><input type="submit" name="submit" value="' . $lng['write'] . '"/></p></form></div>';
}
}
if ($rights == 3 || $rights >= 6)
echo '<form action="index.php?act=massdel" method="post">';
$i = 1;
while (($res = mysql_fetch_assoc($req)) !== false) {
if ($res['close'])
echo '<div class="rmenu">';
else
echo $i % 2 ? '<div class="mainblok">' : '<div class="mainblok">';
/*////////////*
FORUM
*/////////////////////
if($he_a['forum']==null){

/////////////AVATAR
if ($set_user['avatar']) {

echo '<div class="phdr"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td width="auto"><img src="http://infoviet.net/images/op.gif"/> ' . functions::display_date($res['time']) . '</td><td align="right" width="auto">#<a href="index.php?act=post&amp;id=' . $res['id']. '?#' .( $start + $i ). '"><b>' .( $start + $i ). '</b></a></td></tr></tbody></table></div>';
echo '<div class="newsx"><table cellpadding="0" cellspacing="0" width="100%" style="margin-bottom:6px;"><tbody><tr><td width="32" style="text-align:center;">';
echo functions::avatar();
echo '</td><td style="text-align:left;">';
}
///////////// ONOFF
echo functions::onoff();
// Ник юзера и ссылка на его
if($set['caidat']==null) {
if ($res['rights'] == 0 ) {
$colornick['colornick'] = '4e387e';
$colornickk['colornick'] = '4e387e';
}
if ($res['rights'] == 1 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($res['rights'] == 2 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($res['rights'] == 3 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($res['rights'] == 4 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($res['rights'] == 5 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($res['rights'] == 6 ) {
$colornick['colornick'] = '43d6d8';
$colornickk['colornick'] = '43d6d8';
}
if ($res['rights'] == 7 ) {
$colornick['colornick'] = 'ff0000';
$colornickk['colornick'] = 'ff0000';
}
if ($res['rights'] == 9 ) {
$colornick['colornick'] = 'ff00ff';
$colornickk['colornick'] = 'ff00ff';
}
if ($res['rights'] == 10 ) {
$colornick['colornick'] = 'ff0000';
$colornickk['colornick'] = 'ff0000';
}
$user_rights = array(
0 => '<font color="gray">(</font><font
color="black">Member</font><font color="gray">)</font>',
1 => '<font color="gray">(</font><font
color="green">DMod</font><font color="gray">)</font>',
3 => '<font color="gray">(</font><font
color="green">Mod</font><font color="gray">)</font>',
4 => '<font color="gray">(</font><font
color="green>DMod</font><font color="gray">)</font>',
6 => '<font color="gray">(</font><font
color="blue">Smod</font><font color="gray">)</font>',
7 => '<font color="gray">(</font><font
color="red">Admin</font><font color="gray">)</font>',
9 => '<font color="gray">(</font><font color="red">Chủ Tịch Nước</font><font color="gray">)</font>',
10 => '<font color="gray">(</font><font color="red">Máy Chém
Tự Động</font><font color="gray">)</font>'
);
/////////////
} else {
//////////
if ($res['rights'] == 0 ) {
$colornick['colornick'] = ''.$chuoimaunick[4].'';
$colornickk['colornick'] = ''.$chuoimaunick[4].'';
}
if ($res['rights'] == 1 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($res['rights'] == 2 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($res['rights'] == 3 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($res['rights'] == 4 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($res['rights'] == 5 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($res['rights'] == 6 ) {
$colornick['colornick'] = ''.$chuoimaunick[2].'';
$colornickk['colornick'] = ''.$chuoimaunick[2].'';
}
if ($res['rights'] == 7 ) {
$colornick['colornick'] = ''.$chuoimaunick[1].'';
$colornickk['colornick'] = ''.$chuoimaunick[1].'';
}
if ($res['rights'] == 9 ) {
$colornick['colornick'] = ''.$chuoimaunick[0].'';
$colornickk['colornick'] = ''.$chuoimaunick[0].'';
}
if ($res['rights'] == 10 ) {
$colornick['colornick'] = ''.$chuoimaunick[1].'';
$colornickk['colornick'] = ''.$chuoimaunick[1].'';
}
$user_rights = array(
0 => '<font color="gray">(</font><font
color="black">'.$chuoichucvu[4].'</font><font color="gray">)</font>',
1 => '<font color="gray">(</font><font
color="green">'.$chuoichucvu[3].'</font><font color="gray">)</font>',
3 => '<font color="gray">(</font><font
color="green">'.$chuoichucvu[3].'</font><font color="gray">)</font>',
4 => '<font color="gray">(</font><font
color="green>'.$chuoichucvu[3].'</font><font color="gray">)</font>',
6 => '<font color="gray">(</font><font
color="blue">'.$chuoichucvu[2].'</font><font color="gray">)</font>',
7 => '<font color="gray">(</font><font
color="red">'.$chuoichucvu[1].'</font><font color="gray">)</font>',
9 => '<font color="gray">(</font><font color="red">'.$chuoichucvu[0].'</font><font color="gray">)</font>',
10 => '<font color="gray">(</font><font color="red">Máy Chém
Tự Động</font><font color="gray">)</font>'
);

}
// Метка должности








$user_u = $res['user_id'];
$req_u = mysql_query("SELECT * FROM `users` WHERE `website` = '$website' AND `id` = '$user_u' LIMIT 1");
$res_u = mysql_fetch_assoc($req_u);
                   // Ten Member
                    if ($user_id && $user_id != $res['user_id']) {
                        echo '<a href="../users/profile.php?user=' . $res['user_id'] . '"><font color="#' . $colornick['colornick'] . '"><b>' . $res['from'] . '</b>' .$vip1['vip1']. '</a> ';


} else {


echo '<b><span style="color:#' . $colornick['colornick'] . '">' .$vip['vip']. '' . $res['from'] . '' .$vip1['vip1']. '</b></span> ';


}				

                    echo '<span style="color:blue">['.$res_u['postforum'].']</span> | <span style="color:red">'.$res_u['balans'].''.$set['tiente'].'</span><div class="" style="float:right;"><b>Like: '.$res_u['thank_duoc'].'</b>
					</div> ';





// Статус юзера
if (!empty($res['status']))
echo '<div class="status">'.functions::status().'<div class="" style="float:right;"> '.functions::capbac2().'</div></br>
<img src="http://icons.iconarchive.com/icons/iconshock/real-vista-business/64/competitors-icon.png"  width="15px" height="15px" />'.functions::ketban().'| '.functions::tudo().'';
if ($set_user['avatar']){

echo '</td></tr></tbody></table></div>';}
/////////////
// FORUM
// MAIN
///////////////
}
else {if($set['caidat']==null) {
if ($res['rights'] == 0 ) {
$colornick['colornick'] = '4e387e';
$colornickk['colornick'] = '4e387e';
}
if ($res['rights'] == 1 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($res['rights'] == 2 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($res['rights'] == 3 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($res['rights'] == 4 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($res['rights'] == 5 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($res['rights'] == 6 ) {
$colornick['colornick'] = '43d6d8';
$colornickk['colornick'] = '43d6d8';
}
if ($res['rights'] == 7 ) {
$colornick['colornick'] = 'ff0000';
$colornickk['colornick'] = 'ff0000';
}
if ($res['rights'] == 9 ) {
$colornick['colornick'] = 'ff00ff';
$colornickk['colornick'] = 'ff00ff';
}
if ($res['rights'] == 10 ) {
$colornick['colornick'] = 'ff0000';
$colornickk['colornick'] = 'ff0000';
}
$user_rights = array(
0 => '<font color="gray">(</font><font
color="black">Member</font><font color="gray">)</font>',
1 => '<font color="gray">(</font><font
color="green">DMod</font><font color="gray">)</font>',
3 => '<font color="gray">(</font><font
color="green">Mod</font><font color="gray">)</font>',
4 => '<font color="gray">(</font><font
color="green>DMod</font><font color="gray">)</font>',
6 => '<font color="gray">(</font><font
color="blue">Smod</font><font color="gray">)</font>',
7 => '<font color="gray">(</font><font
color="red">Admin</font><font color="gray">)</font>',
9 => '<font color="gray">(</font><font color="red">Chủ Tịch Nước</font><font color="gray">)</font>',
10 => '<font color="gray">(</font><font color="red">Máy Chém
Tự Động</font><font color="gray">)</font>'
);
/////////////
} else {
//////////
if ($res['rights'] == 0 ) {
$colornick['colornick'] = ''.$chuoimaunick[4].'';
$colornickk['colornick'] = ''.$chuoimaunick[4].'';
}
if ($res['rights'] == 1 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($res['rights'] == 2 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($res['rights'] == 3 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($res['rights'] == 4 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($res['rights'] == 5 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($res['rights'] == 6 ) {
$colornick['colornick'] = ''.$chuoimaunick[2].'';
$colornickk['colornick'] = ''.$chuoimaunick[2].'';
}
if ($res['rights'] == 7 ) {
$colornick['colornick'] = ''.$chuoimaunick[1].'';
$colornickk['colornick'] = ''.$chuoimaunick[1].'';
}
if ($res['rights'] == 9 ) {
$colornick['colornick'] = ''.$chuoimaunick[0].'';
$colornickk['colornick'] = ''.$chuoimaunick[0].'';
}
if ($res['rights'] == 10 ) {
$colornick['colornick'] = ''.$chuoimaunick[1].'';
$colornickk['colornick'] = ''.$chuoimaunick[1].'';
}
$user_rights = array(
0 => '<font color="gray">(</font><font
color="black">'.$chuoichucvu[4].'</font><font color="gray">)</font>',
1 => '<font color="gray">(</font><font
color="green">'.$chuoichucvu[3].'</font><font color="gray">)</font>',
3 => '<font color="gray">(</font><font
color="green">'.$chuoichucvu[3].'</font><font color="gray">)</font>',
4 => '<font color="gray">(</font><font
color="green>'.$chuoichucvu[3].'</font><font color="gray">)</font>',
6 => '<font color="gray">(</font><font
color="blue">'.$chuoichucvu[2].'</font><font color="gray">)</font>',
7 => '<font color="gray">(</font><font
color="red">'.$chuoichucvu[1].'</font><font color="gray">)</font>',
9 => '<font color="gray">(</font><font color="red">'.$chuoichucvu[0].'</font><font color="gray">)</font>',
10 => '<font color="gray">(</font><font color="red">Máy Chém
Tự Động</font><font color="gray">)</font>'
);

}
$user_u = $res['user_id'];
$req_u2 = mysql_query("SELECT * FROM `users` WHERE `website` = '$website' AND `id` = '$user_u' LIMIT 1");
$res_u = mysql_fetch_assoc($req_u2);
mySQL_free_result($req_u2);



echo functions::forum(html_entity_decode($he_a['forum'],ENT_QUOTES,'utf-8'),1);


}
/*
-----------------------------------------------------------------
Вывод текста поста
-----------------------------------------------------------------
*/
echo '<div class="forumtxt">';
$text = $res['text'];
if ($set_forum['postcut']) {
// Если текст длинный, обрезаем и даем ссылку на полный вариант
switch ($set_forum['postcut']) {
case 2:
$cut = 10000;
break;

case 3:
$cut = 5000;
break;
default :
$cut = 1000;
}
}
if ($set_forum['postcut'] && mb_strlen($text) > $cut) {
$text = mb_substr($text, 0, $cut);
$text = functions::checkout($text, 1, 1);
$text = preg_replace('#\[c\](.*?)\[/c\]#si', '<div class="quote">\1</div>', $text);
if ($set_user['smileys'])
$text = functions::smileys($text, $res['rights'] ? 1 : 0);
if($user_id) {
$text = str_replace('[you]', $login, $text);
} else {
$text = str_replace('[you]', 'Khách', $text);
}
$checkthank = mysql_query('SELECT COUNT(*) FROM `forum_thank` WHERE `website` = "'.$website.'" AND `userthank` = "' . $user_id . '" and `topic` = "' . $res['id'] . '" and `user` = "' . $res['user_id'] . '"');

$thankcheck = mysql_result($checkthank, 0);
if($thankcheck < 1 && $user_id != $res['user_id']) {
$text = preg_replace('#\[thank\](.*?)\[/thank\]#si', '<div class="gmenu">Nội dung ẩn</div><div class="quote">Bạn vui lòng cảm ơn bài viết này để được xem nội dung ẩn</div>', $text);
} else {
$text = preg_replace('#\[thank\](.*?)\[/thank\]#si', '\1', $text);
}
echo bbcode::notags($text) . '...<br /><a href="'.$home.'/forum/' . functions::mikdaik($type1['text']) . '_p' . $res['id'] . '.html">' . $lng_forum['read_all'] . ' &gt;&gt;</a>';
} else {
// Или, обрабатываем тэги и выводим весь текст
$text = functions::checkout($text, 1, 1);
if ($set_user['smileys'])
$text = functions::smileys($text, $res['rights'] ? 1 : 0);
if($user_id) {
$text = str_replace('[you]', $login, $text);
} else {
$text = str_replace('[you]', 'Khách', $text);
}
$checkthank = mysql_query('SELECT COUNT(*) FROM `forum_thank` WHERE `website` = "'.$website.'" AND `userthank` = "' . $user_id . '" and `topic` = "' . $res['id'] . '" and `user` = "' . $res['user_id'] . '"');
$thankcheck = mysql_result($checkthank, 0);
if($thankcheck < 1 && $user_id != $res['user_id']) {
$text = preg_replace('#\[thank\](.*?)\[/thank\]#si', '<div class="gmenu">Nội dung ẩn</div><div class="quote">Bạn vui lòng cảm ơn bài viết này để được xem nội dung ẩn</div>', $text);
} else {
$text = preg_replace('#\[thank\](.*?)\[/thank\]#si', '\1', $text);
}
echo $text;

}
if ($res['kedit']) {
// Если пост редактировался, показываем кем и когда
echo '<br /><span class="gray"><small>' . $lng_forum['edited'] . ' <b>' . $res['edit'] . '</b> (' . functions::display_date($res['tedit']) . ') <b>[' . $res['kedit'] . ']</b></small></span>';
}
// Если есть прикрепленный файл, выводим его описание
$freq = mysql_query("SELECT * FROM `cms_forum_files` WHERE `website` = '$website' AND `post` = '" . $res['id'] . "'");
if (mysql_num_rows($freq) > 0) {
$fres = mysql_fetch_assoc($freq);
echo '<br /><span class="gray">' . $lng_forum['attached_file'] . ':';
$att_ext = strtolower(functions::format('./files/forum/attach/' . $fres['filename']));
$pic_ext = array(
'gif',
'jpg',
'jpeg',
'png'
);
$req43=mysql_query("SELECT `postforum`, `id` FROM `users` WHERE `website` = '$website' AND  id='$user_id' ");
$arr56=mysql_fetch_assoc($req43);
mySQL_free_result($req43);
$total_spent = 1;
$hammad= 3 - $arr56['postforum'];
if (in_array($att_ext, $pic_ext)) {



       
echo '<div><a href="index.php?act=file&amp;id=' . $fres['id'] . '">';
echo ' <a href="index.php?act=file&amp;id=' . $fres['id'] . '">' . $fres['filename'] . '</a>';


} else if (empty($user_id)){
echo '<b>Bạn cần <a href="../login.php"><font color="red">đăng nhập</font></a> mới tải được file đính kèm</b>';
}else if($total_spent < $arr56['postforum']){
            $checkthank = mysql_query('SELECT COUNT(*) FROM `forum_thank` WHERE `website` = "'.$website.'" AND `userthank` = "' . $user_id . '" and `topic` = "' . $res['id'] . '" and `user` = "' . $res['user_id'] . '"');
$thankcheck = mysql_result($checkthank, 0);
if($thankcheck < 1 && $user_id != $res['user_id']) {
                echo '<b>Bạn cần Cảm ơn mới tải được file đính kèm</b>';
            } else {
echo ' <a href="index.php?act=file&amp;id=' . $fres['id'] . '">' . $fres['filename'] . '</a>';
}
}  
else echo '<b>Bạn vui lòng post đủ 3 bài viết để tải tập tin đính kèm</b>';
echo ' (' . ceil($fres['size']/1024) . ' Kb.)<br/>';
echo $lng_forum['downloads'] . ': ' . $fres['dlcount'] . ' ' . $lng_forum['time'] . '</span>';
$file_id = $fres['id'];

}

#############Th?ng kê s? ngu?i thanks
// $total222 = mysql_result(mysql_query("SELECT COUNT(*) FROM `thongbao` where `website` = '$website' AND  `send` = '".$res['id']."' AND `loai`='4'"), 0);
// if($total222>0){

// echo '</br></br> Cùng với: ';
// $adf=mysql_query("SELECT * from `thongbao` where `website` = '$website' AND `send`='" . $res["id"] . "' AND `loai`='4'");
// while($shit=mysql_fetch_assoc($adf)){
// $ad2=mysql_query("SELECT * from `users` where `website` = '$website' AND `id`='" . $shit["user_id"] . "'");
// while($ad23=mysql_fetch_assoc($ad2)){
// echo '<a href="'.$home.'/'.$ad23['name_lat'].'">'.$ad23['name_lat'].'</a>,';
// }
// }
// mySQL_free_result($adf);

// }
$thongkethank = mysql_query("SELECT COUNT(*) from `forum_thank` where `website` = '$website' AND `topic`='" . $res["id"] . "'");
$thongkethanks = mysql_result($thongkethank, 0);
// $thongkethanks=mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_thank` WHERE `topic` = "' . $res['id'] . '"')), 0);
$thongkea= @mysql_query("select * from `forum_thank` where `website` = '$website' AND `topic` = '" . $res['id'] . "'");
$thongke=mysql_fetch_assoc($thongkea);
mySQL_free_result($thongkea);
$idthongke=trim($_GET['idthongke']);
if($thongkethanks>0&&(empty($_GET['idthongke'])))
{echo'<div id="'.$idthongke.'" class="menu"><font color="red"><img src="http://infoviet.net/images/thich.gif" /></font> ';
$thongkeaa= @mysql_query("select * from `forum_thank` where `website` = '$website' AND `topic` = '" . $res['id'] . "'");
while ($thongkea = mysql_fetch_assoc($thongkeaa))
{
{
$dentv=mysql_fetch_assoc(mysql_query('SELECT * FROM `users` WHERE `website` = "'.$website.'" AND `id` = "'.$thongkea['userthank'].'"'));
echo '<a href="/users/profile.php?id='.$thongkea['userthank'].'">'.$dentv['name'].'</a>, ';
}
++$f;
}
mySQL_free_result($thongkeaa);
echo'</div>';}
 
                   // Ket thuc
                                 
                    // Cap cua thanh vien 2
                    $user_rights = array(
                        0 => '<img src="http://img.infoviet.net/images/mem.gif"/>',
                        3 => '<img src="http://img.infoviet.net/images/mod.gif"/>',
                        6 => '<img src="http://img.infoviet.net/images/smod.gif"/>',
                        7 => '<img src="http://img.infoviet.net/images/admin.gif"/>',
                        9 => '<img src="http://img.infoviet.net/images/vip.gif"/>'
                    );
if($he_a['forum']==null){				
echo '<div style="text-align:right">' . $user_rights[$res['rights']] . '</div>';
if($res['chuki']){
echo '<div class="chuki">' .bbcode::tags($res['chuki']). '</div>';
}
}
else {
echo functions::forum(html_entity_decode($he_a['forum'],ENT_QUOTES,'utf-8'),2);

}
//g0i data thanks
$checkthank = mysql_query('SELECT COUNT(*) FROM `forum_thank` WHERE `website` = "'.$website.'" AND `userthank` = "' . $user_id . '" and `topic` = "' . $res['id'] . '" and `user` = "' . $res['user_id'] . '"');
//end
if($he_a['forum']==null){	                    
                        echo '<div class="forumb"><table cellpadding="0" cellspacing="0" width="100%"><tr><td width="auto" align="left"><a href="/users/profile.php?act=vipham&amp;user=' . $res['user_id'] . '"><font color="red"><img src="http://infoviet.net/images/vipham.gif" /></font></a>';


if ($user_id && $user_id != $res['user_id'] && (mysql_result($checkthank, 0) < 1)) {

						echo '<a href="index.php?id=' . $id . '&amp;thanks=' . $res['id'] . '&amp;user=' . $res['user_id'] . '&amp;start=' . $start . '&amp;thank#thanksyou"><img src="http://infoviet.net/images/thich.gif" /></a></td>';
						
						 }
						 echo (mysql_result($checkthank, 0) == 1 ? '<a href="index.php?id=' . $id . '&amp;unthanks=' . $res['id'] . '&amp;user=' . $res['user_id'] . '&amp;start=' . $start . '&amp;unthank"><b><img src="http://allfacebook.com/files/2010/06/dislike.png" height="20" width="20" /></b></a>' : ' ');

						 echo'
						<td width="auto" align="right"><a href="index.php?act=say&amp;id=' . $res['id'] . '&amp;start=' . $start . '"><img src="http://infoviet.net/images/reply.gif" /></a>';
						// if($user_id == $res['user_id'])
						// echo'<a href="index.php?act=danhdau&amp;id=' . $res['id'] . '"><img src="http://systemsync.googlecode.com/svn-history/r112/trunk/dnAdmin/WebContent/images/true-icon.png" /></a>&#160;';
						
                             echo                     '<a href="index.php?act=say&amp;id=' . $res['id'] . '&amp;start=' . $start . '&amp;cyt"><img src="http://infoviet.net/images/quocte.gif" /></a></td></tr></table></div>';
                   }//////////////////////
				   else{
				         $vipham= '<a href="/users/profile.php?act=vipham&amp;user=' . $res['user_id'] . '">';


if ($user_id && $user_id != $res['user_id'] && (mysql_result($checkthank, 0) < 1)) {

						$thank= '<a href="index.php?id=' . $id . '&amp;thanks=' . $res['id'] . '&amp;user=' . $res['user_id'] . '&amp;start=' . $start . '&amp;thank#thanksyou">';
						
						 }
						$unthank= (mysql_result($checkthank, 0) == 1 ? '<a href="index.php?id=' . $id . '&amp;unthanks=' . $res['id'] . '&amp;user=' . $res['user_id'] . '&amp;start=' . $start . '&amp;unthank">' : '');

						 $reply='<a href="index.php?act=say&amp;id=' . $res['id'] . '&amp;start=' . $start . '">';
						// if($user_id == $res['user_id'])
						// echo'<a href="index.php?act=danhdau&amp;id=' . $res['id'] . '"><img src="http://systemsync.googlecode.com/svn-history/r112/trunk/dnAdmin/WebContent/images/true-icon.png" /></a>&#160;';
						
                             $quote='<a href="index.php?act=say&amp;id=' . $res['id'] . '&amp;start=' . $start . '&amp;cyt">';
                   
				   $a=functions::forum(html_entity_decode($he_a['forum'],ENT_QUOTES,'utf-8'),3);
echo strtr($a, array(
'$thank' => $thank,
'$unthank' => $unthank,
'$vipham' => $vipham,
'$reply' => $reply,
'$quote' => $quote,
));
				   
				   }
                         echo '</div>';
                    if ((($rights == 3 || $rights >= 6 || $curator) && $rights >= $res['rights']) || ($res['user_id'] == $user_id && !$set_forum['upfp'] && ($start + $i) == $colmes && $res['time'] > time() - 300) || ($res['user_id'] == $user_id && $set_forum['upfp'] && $start == 0 && $i == 1 && $res['time'] > time() - 300)) {
// Ссылки на редактирование / удаление постов
                        $menu = array(
                            '<a href="index.php?act=editpost&amp;id=' . $res['id'] . '">' . $lng['edit'] . '</a>',
                            ($rights >= 7 && $res['close'] == 1 ? '<a href="index.php?act=editpost&amp;do=restore&amp;id=' . $res['id'] . '">' . $lng_forum['restore'] . '</a>' : ''),
                            ($res['close'] == 1 ? '' : '<a href="index.php?act=editpost&amp;do=del&amp;id=' . $res['id'] . '">' . $lng['delete'] . '</a>')
                        );
                        echo '<div class="sub">';
                        if ($rights == 3 || $rights >= 6)
                            echo '<input type="checkbox" name="delch[]" value="' . $res['id'] . '"/>&#160;';
                        echo functions::display_menu($menu);
                        if ($res['close']) {
                            echo '<div class="red">' . $lng_forum['who_delete_post'] . ': <b>' . $res['close_who'] . '</b></div>';
                        } elseif (!empty($res['close_who'])) {
                            echo '<div class="green">' . $lng_forum['who_restore_post'] . ': <b>' . $res['close_who'] . '</b></div>';
                        }
                      echo '</div>';
                    }
					echo '</div>';					
					
                    ++$i;
                }

if ($rights == 3 || $rights >= 6) {
echo '<div class="rmenu"><input type="submit" value=" ' . $lng['delete'] . ' "/></div>';
echo '</form>';
}
// Нижнее поле "Написат
echo '<br /><div class="mainblok">';
if($user_id){
echo '<div class="phdr"><b>Đăng bài viết</b></div><div class="gmenu"><form name="form2"
action="index.php?act=say&amp;id=' . $id . '" method="post">';
echo '<p>';
if (!$is_mobile)
echo bbcode::auto_bb('form2', 'msg');
echo '<textarea rows="' . $set_user['field_h'] . '"
name="msg"></textarea><br/></p><p><input type="checkbox" name="addfiles" value="1" />'.$lng_forum['add_file'].'';
echo '<br /><input type="checkbox" name="msgtrans" value="1" />
Telex (aa => â)';
echo '</p><p><input type="submit" name="submit" value="' .
$lng['write'] . '" style="width: 107px; cursor: pointer;"/> ' .
($set_forum['preview'] ? '<input type="submit" value="' .
$lng['preview'] . '" style="width: 107px; cursor: pointer;"/>' : '') .
'</p></form></div>';
} else {
echo '<div class="gmenu">Cần phải <a href="/login.php"><b>Đăng
Nhập</b></a> để được phép bình luận</form></div>';
}
echo '<div class="phdr"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td width="auto"><a name="down" id="down"></a><a href="#up">' .
'<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/up.png" alt="' . $lng['up'] . '" width="20" height="10" border="0"/></a><b>' .
'&#160;&#160;' . $lng['total'] . ': ' . $colmes . '</b></td><td align="right" width="auto"><b>Xem: '.$type1['view'].' lượt</b></td></tr></tbody></table></div>';
if ($colmes > $kmess) {
echo '<div class="topmenu">' . functions::display_pagination2(''.$home.'/forum/'.functions::mikdaik($type1["text"]).'_' . $id, $start, $colmes, $kmess) . '</div>' .
'<p><form action="index.php?id=' . $id . '" method="post">' .
'<input type="text" name="page" size="2"/>' .
'<input type="submit" value="' . $lng['to_page'] . ' &gt;&gt;"/>' .
'</form></p>';
} else {
}
echo '</div>';
                /*
                -----------------------------------------------------------------
                Chia se bv
                -----------------------------------------------------------------
                */


// Chia sẻ

function selfURL()
{
$s = empty($_SERVER["HTTPS"]) ? ''
: ($_SERVER["HTTPS"] == "on") ? "s"
: "";
$protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s;
$port = ($_SERVER["SERVER_PORT"] == "80") ? ""
: (":".$_SERVER["SERVER_PORT"]);
return $protocol."://".$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI'];
}
function strleft($s1, $s2)
{
return substr($s1, 0, strpos($s1, $s2));
}

function facebook()
{     
	  $l = selfURL();
	  
	  $social = "<a href=\"http://www.facebook.com/share.php?u=$l\"/><img src=\"http://giacmovn.com/icon/facebook.png\" align =\"middle\" border =\"0\" width =\"18\" height =\"18\" alt =\"facebook\" /></a>";
      return $social;
}

function zingme()
{     
	  $l = selfURL();
	  
	  $social = "<a href=\"http://link.apps.zing.vn/share?url=$l\"/><img src=\"http://img.forum.zing.vn/images/bluestyle/misc/bookmark_icons/zingme.png\" align =\"middle\" border =\"0\" width =\"20\" height =\"20\" alt =\"delicious\" /></a>";
      return $social;
}
echo '<div class="rmenu">chia sẻ bài viết này:'.facebook().'
'.zingme().'</br>


<textarea rows="' . $set_user['field_h'] . '">anh em xem bài viết này:'.$home.'/forum/index.php?id='.$id.'
'.$type1["text"].', cảm ơn </textarea>
</div></br>';//het

/*
-----------------------------------------------------------------
Ссылки на модераторские функции
-----------------------------------------------------------------
*/
if ($curators) {
$array = array();
foreach ($curators as $key => $value)
$array[] = '<a href="../users/profile.php?user=' . $key . '">' . $value . '</a>';
echo '<p><div class="func">' . $lng_forum['curators'] . ': ' . implode(', ', $array) . '</div></p>';
}
$req = mysql_query("SELECT `text`,`id` FROM `forum` WHERE `website` = '$website' AND `type`='t' AND `refid`='$type1[refid]' AND `id`!='$id' ORDER BY `vip` DESC, `time` DESC LIMIT 5");
$total = mysql_num_rows($req);
if($total!=0)
{
echo '<div class="mainblok"><div class="phdrblack"><b>Chủ đề có liên quan</b></div>';
while ($res = mysql_fetch_assoc($req)) {
echo '<div class="menub">+ <a href="'.$home.'/forum/' . functions::mikdaik($res['text']) . '_' . $res['id'] . '.html">' . bbcode::tags($res['text']) . '</a></div>';
++$i;
}

echo'</div>';
}
if ($rights == 3 || $rights >= 6) {
echo '<p><div class="func">';
if ($rights == 9){
if(empty($type1['portal'])) echo '<a href="index.php?act=portal&amp;id=' . $id . '&amp;portal" style="color:red;">Thêm tin tức Hot</a><br/>';else echo '<a href="index.php?act=portal&amp;id=' . $id . '&amp;khoiphuc" style="color:red;">Bỏ tin tức Hot</a><br/>';
}
echo '<a href="index.php?act=curators&amp;id=' . $id . '&amp;start=' . $start . '">' . $lng_forum['curators_of_the_topic'] . '</a><br />';
if(!$type1['kiemduyet']) {
echo "<a href='index.php?act=kiemduyet&amp;id=" . $id . "&amp;kiemduyet'>Kiểm duyệt</a> | <a href='index.php?act=deltema&amp;id=" . $id . "'>Loại bỏ</a><br/>";
}
echo isset($topic_vote) && $topic_vote > 0
? '<a href="index.php?act=editvote&amp;id=' . $id . '">' . $lng_forum['edit_vote'] . '</a><br/><a href="index.php?act=delvote&amp;id=' . $id . '">' . $lng_forum['delete_vote'] . '</a><br/>'
: '<a href="index.php?act=addvote&amp;id=' . $id . '">' . $lng_forum['add_vote'] . '</a><br/>';
echo '<a href="index.php?act=ren&amp;id=' . $id . '">' . $lng_forum['topic_rename'] . '</a><br/>';
// Закрыть - открыть тему
if ($type1['edit'] == 1)
echo '<a href="index.php?act=close&amp;id=' . $id . '">' . $lng_forum['topic_open'] . '</a><br/>';
else
echo '<a href="index.php?act=close&amp;id=' . $id . '&amp;closed">' . $lng_forum['topic_close'] . '</a><br/>';
// Удалить - восстановить тему
if ($type1['close'] == 1)
echo '<a href="index.php?act=restore&amp;id=' . $id . '">' . $lng_forum['topic_restore'] . '</a><br/>';
echo '<a href="index.php?act=deltema&amp;id=' . $id . '">' . $lng_forum['topic_delete'] . '</a><br/>';
if ($type1['vip'] == 1)
echo '<a href="index.php?act=vip&amp;id=' . $id . '">' . $lng_forum['topic_unfix'] . '</a>';
else
echo '<a href="index.php?act=vip&amp;id=' . $id . '&amp;vip">' . $lng_forum['topic_fix'] . '</a>';
echo '<br/><a href="index.php?act=per&amp;id=' . $id . '">' . $lng_forum['topic_move'] . '</a></div></p>';
}

if ($filter)
echo '<div class="list1"><a href="index.php?act=filter&amp;id=' . $id . '&amp;do=unset">' . $lng_forum['filter_cancel'] . '</a></div>';
else
echo '<div class="phdrblack"><a href="index.php?act=filter&amp;id=' . $id . '&amp;start=' . $start . '">' . $lng_forum['filter_on_author'] . '</a> |';
echo ' <a href="index.php?act=tema&amp;id=' . $id . '">' . $lng_forum['download_topic'] . '</a></div>';

break;
default:
/*
-----------------------------------------------------------------
Если неверные данные, показываем ошибку
-----------------------------------------------------------------
*/
break;
}
} else {
/*
-----------------------------------------------------------------
Список Категорий форума
-----------------------------------------------------------------
*/
$count = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_forum_files`" . ($rights >= 7 ? '' : " WHERE `website` = '$website' AND `del` != '1'")), 0);
echo '<p>' . counters::forum_new(1) . '</p>' .
'<div class="phdr"><b>' . $lng['forum'] . '</b></div>' .
'<div class="topmenu"><a href="search.php">' . $lng['search'] . '</a> | <a href="index.php?act=files">' . $lng_forum['files_forum'] . '</a> <span class="red">(' . $count . ')</span></div>';
$req = mysql_query("SELECT `id`, `text`, `soft` FROM `forum` WHERE `website` = '$website' AND `type`='f' ORDER BY `realid`");
$i = 0;
while (($res = mysql_fetch_assoc($req)) !== false) {
echo $i % 2 ? '<div class="list2">' : '<div class="list1">';
$count = mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website` = '$website' AND `type`='r' and `refid`='" . $res['id'] . "'"), 0);
echo '<a href="'.$home.'/forum/' . functions::mikdaik($res['text']) . '_' . $res['id'] . '.html">' . $res['text'] . '</a> [' . $count . ']';
if (!empty($res['soft']))
echo '<div class="sub"><span class="gray">' . $res['soft'] . '</span></div>';
echo '</div>';
++$i;
}
$online_u = mysql_result(mysql_query("SELECT COUNT(*) FROM `users` WHERE `website` = '$website' AND `lastdate` > " . (time() - 300) . " AND `place` LIKE 'forum%'"), 0);
$online_g = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `website` = '$website' AND `lastdate` > " . (time() - 300) . " AND `place` LIKE 'forum%'"), 0);
echo '<div class="phdr">' . ($user_id ? '<a href="index.php?act=who">' . $lng_forum['who_in_forum'] . '</a>' : $lng_forum['who_in_forum']) . '&#160;(' . $online_u . '&#160;/&#160;' . $online_g . ')</div>';
unset($_SESSION['fsort_id']);
unset($_SESSION['fsort_users']);
}

// Навигация внизу страницы
echo '<p>' . ($id ? '<a href="index.php">' . $lng['to_forum'] . '</a><br />' : '');
if (!$id) {
echo '<a href="../pages/faq.php?act=forum">' . $lng_forum['forum_rules'] . '</a><br/>';
echo '<a href="index.php?act=moders">' . $lng['moders'] . '</a>';
}
echo '</p>';
if (!$user_id) {
if ((empty($_SESSION['uppost'])) || ($_SESSION['uppost'] == 0)) {
echo '<a href="index.php?id=' . $id . '&amp;page=' . $page . '&amp;newup">' . $lng_forum['new_on_top'] . '</a>';
} else {
echo '<a href="index.php?id=' . $id . '&amp;page=' . $page . '&amp;newdown">' . $lng_forum['new_on_bottom'] . '</a>';
}
}
}

require_once('../incfiles/end.php');

?>