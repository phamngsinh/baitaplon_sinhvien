<?php
/**
* @package     JohnCMS
* @link        http://johncms.com
* @copyright   Copyright (C) 2008-2011 JohnCMS Community
* @license     LICENSE.txt (see attached file)
* @version     VERSION.txt (see attached file)
* @author      http://johncms.com/about
*/

defined('_IN_JOHNADM') or die('Error: restricted access');

// Проверяем права доступа
if ($rights < 9) {
    header('Location: http://johncms.com/?err');
    exit;
}
echo '<div class="phdr"><a href="index.php"><b>' . $lng['admin_panel'] . '</b></a> | ' . $lng['site_settings'] . '</div>';
if (isset($_POST['submit'])) {
    /*
    -----------------------------------------------------------------
    Сохраняем настройки системы
    -----------------------------------------------------------------
    */
    mysql_query("UPDATE `cms_settings` SET `skindef`='" . functions::check($_POST['skindef']) . "' where `website` = '$website' ")or die (mysql_error());
    mysql_query("UPDATE `cms_settings` SET `email`='" . mysql_real_escape_string(htmlspecialchars($_POST['madm'])) . "' where `website` = '$website'");
    mysql_query("UPDATE `cms_settings` SET `timeshift`='" . intval($_POST['timeshift']) . "' where `website` = '$website' ");
    mysql_query("UPDATE `cms_settings` SET `copyright`='" . addslashes($_POST['copyright']) . "' where `website` = '$website' ");
    mysql_query("UPDATE `cms_settings` SET `tiente`='" . addslashes($_POST['tiente']) . "' where `website` = '$website' ");
    mysql_query("UPDATE `cms_settings` SET `ban`='" . addslashes($_POST['ban']) . "' where `website` = '$website' ");
    mysql_query("UPDATE `cms_settings` SET `homeurl`='" . functions::check(preg_replace("#/$#", '', trim($_POST['homeurl']))) . "' where `website` = '$website' ");
    mysql_query("UPDATE `cms_settings` SET `flsz`='" . intval($_POST['flsz']) . "' where `website` = '$website' ");
    mysql_query("UPDATE `cms_settings` SET `gzip`='" . isset($_POST['gz']) . "' where `website` = '$website' ");
    mysql_query("UPDATE `cms_settings` SET `meta_key`='" . functions::check($_POST['meta_key']) . "' where `website` = '$website' ");
    mysql_query("UPDATE `cms_settings` SET `icon`='" . functions::check($_POST['icon']) . "' where `website` = '$website' ");
    mysql_query("UPDATE `cms_settings` SET `meta_desc`='" . functions::check($_POST['meta_desc']) . "' where `website` = '$website' ");
	mysql_query("UPDATE `cms_settings` SET `meta_ver`='" . htmlspecialchars($_POST['meta_ver']) . "' where `website` = '$website' ");
mysql_query("UPDATE `cms_settings` SET `closed`='" . intval($_POST['closed']) . "' where `website` = '$website'");       echo '<div class="rmenu">' . $lng['settings_saved'] . '</div>';
}
/*
-----------------------------------------------------------------
Форма ввода параметров системы
-----------------------------------------------------------------
*/
echo '<form action="index.php?act=settings" method="post"><div class="menu">';
// Общие настройки
echo '<p>' .
    '<h3>' . $lng['common_settings'] . '</h3>' .
    $lng['site_url'] . ':<br/>' . '<input type="text" name="homeurl" value="' . htmlentities($set['homeurl']) . '"/><br/>' .
    $lng['site_copyright'] . ':<br/>' . '<input type="text" name="copyright" value="' . htmlentities($set['copyright'], ENT_QUOTES, 'UTF-8') . '"/><br/>' .
   'Bản quyền Forum:<br/>' . '<input type="text" name="ban" value="' . htmlentities($set['ban'], ENT_QUOTES, 'UTF-8') . '"/><br/>' .
   'Đơn Vị Tiền Tệ:<br/>' . '<input type="text" name="tiente" value="' . htmlentities($set['tiente'], ENT_QUOTES, 'UTF-8') . '"/><br/>' .
    $lng['site_email'] . ':<br/>' . '<input name="madm" maxlength="50" value="' . htmlentities($set['email']) . '"/><br />' .
    $lng['file_maxsize'] . ' (kb):<br />' . '<input type="text" name="flsz" value="' . intval($set['flsz']) . '"/><br />' .
    '<input name="gz" type="checkbox" value="1" ' . ($set['gzip'] ? 'checked="checked"' : '') . ' />&#160;' . $lng['gzip_compress'] .
    '</p>';
// Настройка времени
echo '<p>' .
    '<h3>' . $lng['clock_settings'] . '</h3>' .
    '<input type="text" name="timeshift" size="2" maxlength="3" value="' . $set['timeshift'] . '"/> ' . $lng['time_shift'] . ' (+-12)<br />' .
    '<span style="font-weight:bold; background-color:#C0FFC0">' . date("H:i", time() + $set['timeshift'] * 3600) . '</span> ' . $lng['system_time'] .
    '<br /><span style="font-weight:bold; background-color:#FFC0C0">' . date("H:i") . '</span> ' . $lng['server_time'] .
    '</p>';
// META тэги
echo '<p>' .
    '<h3>' . $lng['meta_tags'] . '</h3>' .
    '&#160;' . $lng['meta_keywords'] . ':<br />&#160;<textarea rows="' . $set_user['field_h'] . '" name="meta_key">' . $set['meta_key'] . '</textarea><br />' .
    '&#160; favicon :<br />&#160;<textarea rows="' . $set_user['field_h'] . '" name="icon">' . $set['icon'] . '</textarea><br />' .
    '&#160;' . $lng['meta_description'] . ':<br />&#160;<textarea rows="' . $set_user['field_h'] . '" name="meta_desc">' . $set['meta_desc'] . '</textarea>' .
    '&#160;Ver Google:<br />&#160;<textarea rows="' . $set_user['field_h'] . '" name="meta_ver">' . $set['meta_ver'] . '</textarea>' .
    '</p>';
// Выбор темы оформления
echo '<p><h3>' . $lng['design_template'] . '</h3>&#160;<select name="skindef">';
$dir = opendir('../theme');
while ($skindef = readdir($dir)) {
    if (($skindef != '.') && ($skindef != '..') && ($skindef != '.svn')) {
        $skindef = str_replace('.css', '', $skindef);
        echo '<option' . ($set['skindef'] == $skindef ? ' selected="selected">' : '>') . $skindef . '</option>';
    }
}
closedir($dir);
echo '</select>';
echo '<br/><br/><input name="closed" type="checkbox" value="1" ' . ($set['closed'] ? 'checked="checked"': '') . ' />&nbsp;Đóng cửa';
echo'</p><p><input type="submit" name="submit" value="' . $lng['save'] . '"/></p></div></form>';
echo'<div class="phdr">&#160;</div>';
echo'<p><a href="index.php">' . $lng['admin_panel'] . '</a></p>';

?>