<?php
$is_mobile = mobile_detect();  
function mobile_detect()
    {
        if (isset($_SESSION['is_mobile'])) {
            return $_SESSION['is_mobile'] == 1 ? true : false;
        }
        $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
        $accept = strtolower($_SERVER['HTTP_ACCEPT']);
        if ((strpos($accept, 'text/vnd.wap.wml') !== false) || (strpos($accept, 'application/vnd.wap.xhtml+xml') !== false)) {
            $_SESSION['is_mobile'] = 1;
            return true;
        }
        if (isset($_SERVER['HTTP_X_WAP_PROFILE']) || isset($_SERVER['HTTP_PROFILE'])) {
            $_SESSION['is_mobile'] = 1;
            return true;
        }
        if (preg_match('/android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $user_agent)
            || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i', substr($user_agent, 0, 4))) {
            $_SESSION['is_mobile'] = 1;
            return true;
        }
        $_SESSION['is_mobile'] = 2;
        return false;
    }
	
function userAgent2() {



    $userAgent2= array(
        'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
        'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; GTB6; .NET CLR 2.0.50727)',
        'Mozilla/5.0 (Windows; U; Windows NT 6.0; vi; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; GTB6)',
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2',
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.13) Gecko/2009073022 Firefox/3.0.13',
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/530.5 (KHTML, like Gecko) Chrome/2.0.172.43 Safari/530.5',
        'Opera/9.80 (Windows NT 5.1; U; en) Presto/2.2.15 Version/10.00',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; InfoPath.2)',
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/531.0 (KHTML, like Gecko) Chrome/3.0.182.3 Safari/531.0',
    );



    return $userAgent2[rand(0, 10)];
}
function grab_link($url,$cookie='',$user_agent2='',$header='') {
if(function_exists('curl_init')){
$ch = curl_init();
$headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
$headers[] = 'Accept-Language: en-us,en;q=0.5';
//$headers[] = 'Accept-Encoding: gzip,deflate';
$headers[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7';
$headers[] = 'Keep-Alive: 300';
$headers[] = 'Connection: Keep-Alive';
$headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
curl_setopt($ch, CURLOPT_URL, $url);
if($user_agent)	curl_setopt($ch, CURLOPT_USERAGENT, $user_agent2);
else curl_setopt($ch, CURLOPT_USERAGENT, userAgent2());
if($header)
curl_setopt($ch, CURLOPT_HEADER, 1);
else
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com.vn/search?hl=vi&client=firefox-a&rls=org.mozilla:en-US:official&hs=hKS&q=video+clip&start=20&sa=N');
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
if(strncmp($url, 'https', 6)) curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
if($cookie)	curl_setopt($ch, CURLOPT_COOKIE, $cookie);
curl_setopt($ch, CURLOPT_TIMEOUT, 100);
$html = curl_exec($ch);
$mess_error = curl_error($ch);
curl_close($ch);
}
else {
$matches = parse_url($url);
$host = $matches['host'];
$link = (isset($matches['path'])?$matches['path']:'/').(isset($matches['query'])?'?'.$matches['query']:'').(isset($matches['fragment'])?'#'.$matches['fragment']:'');
$port = !empty($matches['port']) ? $matches['port'] : 80;
$fp=@fsockopen($host,$port,$errno,$errval,30);
if (!$fp) {
$html = "$errval ($errno)<br />\n";
} else {
$rand_ip = rand(1,254).".".rand(1,254).".".rand(1,254).".".rand(1,254);
$out  = "GET $link HTTP/1.1\r\n".
"Host: $host\r\n".
"Referer: http://www.google.com.vn/search?hl=vi&client=firefox-a&rls=org.mozilla:en-US:official&hs=hKS&q=video+clip&start=20&sa=N\r\n".
"Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5\r\n";
if($cookie) $out .= "Cookie: $cookie\r\n";
if($user_agent) $out .= "User-Agent: ".$user_agent2."\r\n";
else $out .= "User-Agent: ".userAgent2()."\r\n";
$out .= "X-Forwarded-For: $rand_ip\r\n".
"Via: CB-Prx\r\n".
"Connection: Close\r\n\r\n";
fwrite($fp,$out);
while (!feof($fp)) {
$html .= fgets($fp,4096);
}
fclose($fp);
}
}
return $html;
}
function laynoidung($noidung, $start, $stop) {
$bd = strpos($noidung, $start);
$kt = strpos(substr($noidung, $bd), $stop) + $bd;
$content = substr($noidung, $bd, $kt - $bd);
$content = $content.$stop;
return $content;
}
function repl($t)
{
$repl = str_replace('http://hinhnenhot.24h.com.vn/','',$repl);
return $repl;
}

