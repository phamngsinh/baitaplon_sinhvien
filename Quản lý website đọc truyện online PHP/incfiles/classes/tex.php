<?php
/**
* @package     JohnCMS
* @link        http://johncms.com
* @copyright   Copyright (C) 2008-2011 JohnCMS Community
* @license     LICENSE.txt (see attached file)
* @version     VERSION.txt (see attached file)
* @author      http://johncms.com/about
*/

defined('_IN_JOHNCMS') or die('Restricted access');

class functions extends core
{
/*
-----------------------------------------------------------------
Антифлуд
-----------------------------------------------------------------
Режимы работы:
1 - Адаптивный
2 - День / Ночь
3 - День
4 - Ночь
-----------------------------------------------------------------
*/
public static function create_keywords($story, $separator = ', ', $keyword_count = 20)
 { global $set; 
 $fastquotes = array ("\x22", "\x60", "\t", "\n", "\r", '"', "\\", '\r', '\n', "-", "{", "}", "[", "]" );
 $story1 = str_replace( $fastquotes, " ", $story ); 
 $story2 = preg_replace("/[^\w\x7F-\xFF\s]/", " ", $story1); 
 $story = preg_replace("/ {,1}/", " ", $story2); 
 $story3 = str_replace( $fastquotes, '', trim( strip_tags( str_replace( '<br />', ' ', stripslashes( $story1 ) ) ) ) ); 
 $story3 = preg_replace("/ {,1}/", " ", $story3);
 $set['meta_desc'] = mb_substr( $story3, 0, 190, 'UTF-8' ); 
 $arr = explode( " ", $story ); foreach ( $arr as $word ) { if( mb_strlen( $word, 'UTF-8') > 4 ) $newarr[] = $word; } 
 $arr = array_count_values( $newarr ); arsort( $arr ); 
 $arr = array_keys( $arr ); $total = count( $arr ); 
 $offset = 0; 
 $arr = array_slice( $arr, $offset, $keyword_count ); 
 $set['meta_key'] = implode( $separator, $arr ); }
public static function antiflood()
{
$default = array(
'mode' => 2,
'day' => 10,
'night' => 30,
'dayfrom' => 10,
'dayto' => 22
);
$af = isset(self::$system_set['antiflood']) ? unserialize(self::$system_set['antiflood']) : $default;
switch ($af['mode']) {
case 1:
// Адаптивный режим
$adm = mysql_result(mysql_query("SELECT COUNT(*) FROM `users` where `website` = '$website' AND  `rights` > 0 AND `lastdate` > " . (time() - 300)), 0);
$limit = $adm > 0 ? $af['day'] : $af['night'];
break;
case 3:
// День
$limit = $af['day'];
break;
case 4:
// Ночь
$limit = $af['night'];
break;
default:
// По умолчанию день / ночь
$c_time = date('G', time());
$limit = $c_time > $af['day'] && $c_time < $af['night'] ? $af['day'] : $af['night'];
}
if (self::$user_rights > 0)
$limit = 4; // Для Администрации задаем лимит в 4 секунды
$flood = self::$user_data['lastpost'] + $limit - time();
if ($flood > 0)
return $flood;
else
return false;
}

/*
-----------------------------------------------------------------
Маскировка ссылок в тексте
-----------------------------------------------------------------
*/
public static function antilink($var)
{
$var = preg_replace('~\\[url=(https?://.+?)\\](.+?)\\[/url\\]|(https?://(www.)?[0-9a-z\.-]+\.[0-9a-z]{2,6}[0-9a-zA-Z/\?\.\~&amp;_=/%-:#]*)~', '###', $var);
$replace = array(
'.ru' => '***',
'.com' => '***',
'.biz' => '***',
'.cn' => '***',
'.in' => '***',
'.net' => '***',
'.org' => '***',
'.info' => '***',
'.mobi' => '***',
'.wen' => '***',
'.kmx' => '***',
'.h2m' => '***'
);
return strtr($var, $replace);
}
//////////////////

public static function forum($var,$vitri)
{
global $res_u,$set,$colornick,$vip1,$res,$start,$i,$user_rights;
if($vitri==2)
{
$var=preg_replace("/\[duoi\](.+?)\[\/duoi\]/is", "\\1", $var);
$var=preg_replace("/\[than\](.+?)\[\/than\]/is", "", $var);
}
else {
$var=preg_replace("/\[than\](.+?)\[\/than\]/is", "\\1", $var);
$var=preg_replace("/\[duoi\](.+?)\[\/duoi\]/is", "", $var);}
/// Chưa Login 


$replace = array(
'[forum:avatar]' => functions::avatar(),
'[forum:status]' => functions::status(),
'[forum:on]' => functions::onoff(),
'[forum:like]' => $res_u['thank_duoc'],
'[forum:tien]' => $res_u['balans'],
'[forum:post]' => $res_u['postforum'],
'[forum:capbac]' => functions::capbac2(),
'[forum:time]' => functions::display_date($res['time']),
'[forum:bai]' => '#<a href="index.php?act=post&amp;id=' . $res['id']. '?#' .( $start + $i ). '"><b>' .( $start + $i ). '</b></a>',
'[forum:nick]' => '<a href="../users/profile.php?user=' . $res['user_id'] . '"><font color="#' . $colornick['colornick'] . '"><b>' . $res['from'] . '</b>' .$vip1['vip1']. '</a>',
'[forum:chuky]' => bbcode::tags($res['chuki']),
'[forum:chucvu]' => $user_rights[$res['rights']],

);

return strtr($var, $replace);
}





public static function func($var)
{
global $website,$start,$kmess,$rights,$login,$act,$headmod,$datauser,$user_id,$balans,$set,$set_user,$realtime,$user_id,$admp,$setdh;
$H=date("H")+7;
//// Đã Login
if($user_id)
{
$var=preg_replace("/\[login\](.+?)\[\/login\]/is", "\\1", $var);
$var=preg_replace("/\[khach\](.+?)\[\/khach\]/is", "", $var);

if ($rights >=1){
$var=preg_replace("/\[bqt\](.+?)\[\/bqt\]/is", "\\1", $var);

}
else {
$var=preg_replace("/\[bqt\](.+?)\[\/bqt\]/is", "", $var);}
/// Chưa Login 

} else {
$login2='<a href="' . $set['homeurl'] . '/login.php">Đăng nhập</a>';
$reg='<a href="' . $set['homeurl'] . '/registration.php">Đăng ký</a>';
$var=preg_replace("/\[login\](.+?)\[\/login\]/is", "", $var);
$var=preg_replace("/\[khach\](.+?)\[\/khach\]/is", "\\1", $var);
}

$replace = array(
'[func:date]' => $H.date(":i"),
'[func:login]' => $login2,
'[func:reg]' => $reg,
'[func:topx]' => functions::topx(),
'[func:tintuc]' => functions::tintuc(),
'[func:anh]' => functions::anh(),
'[func:tracnghiem]' => functions::tracngiem(),
'[func:member]' => functions::member(),
'[func:thongke]' => functions::thongke(),
'[func:blog]' => functions::blog(),
'[func:chuyenmuc]' => functions::chuyenmuc(),
'[func:chuadoc]' => counters::forum_new(1),
'[func:nick]' => $login,
'[func:tien]' => ''.$datauser['balans'].'<b> '.$set['tiente'].'</b>',
'[func:avt]' => functions::thanhvien('avt'),
'[func:chatbox]' => functions::chatbox(),
'[func:ds]' => functions::ds(),
'[func:capbac]' => functions::capbac(),
'[func:topforum]' => functions::get_top('postforum'),

);

return strtr($var, $replace);
}

public static function get_top($order = 'postforum') {
    global $lng,$website;
    $req = mysql_query("SELECT * FROM `users` where `website` = '$website' AND  `$order` > 0 ORDER BY `$order` DESC LIMIT 5");
    if (mysql_num_rows($req)) {
        $out = '';
		$out .='<div class="phdr">Top Member </div>';
        $i = 0;
        while ($res = mysql_fetch_assoc($req)) {
            $out .= $i % 2 ? '<div class="list2">' : '<div class="list1">';
            $out .= functions::display_user2($res, array ('header' => ('<b>' . $res[$order]) . '</b>')) . '</div>';
            ++$i;
        }
        return $out;
    } else {
        return '<div class="menu"><p>' . $lng['list_empty'] . '</p></div>';
    }
}

public static function avatar(){
global $res,$set_user;
if ($set_user['avatar']) {
if (file_exists(('../files/users/avatar/' . $res['user_id'] . '.png')))
$var.='<img src="../files/users/avatar/' . $res['user_id'] . '.png" width="32" height="32" alt="' . $res['from'] . '" />&#160;';
else
$var.= '<img src="http://img.infoviet.net/images/empty.png" width="32" height="32" alt="' . $res['from'] . '" />&#160;';
}
return $var;
}
public static function status(){
global $res,$set_user;
if (!empty($res['status']))
$var.= '<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/label.png" alt="" align="middle"/>&#160;' . $res['status'] . '';

return $var;
}

public static function onoff(){
global $res;
if ($res['sex'])
$var.= (time() > $res['lastdate'] + 300 ? '<font color="red"> • </font> ' : '<font color="green"> • </font> ');
else
$var.= '<img src="http://img.infoviet.net/images/del.png" width="12" height="12" align="middle" alt=""/>&#160;';
return $var;
}
public static function capbac(){
global $website,$set,$user_id,$user_u,$res,$parent,$type1;
if(!$user_u)
$req_u = mysql_query("SELECT * FROM `users` WHERE `website` = '$website' AND `id` = '$user_id' LIMIT 1");
else
$req_u = mysql_query("SELECT * FROM `users` WHERE `website` = '$website' AND `id` = '$user_u' LIMIT 1");
$res_u = mysql_fetch_array($req_u);
if ($set['danhhieu']==0){
if ($res_u['postforum'] != 0) {
$chucdanh= $res_u['postforum']/5;
if ($chucdanh<= 1)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/01.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 3)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/02.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 6)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/03.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 12)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/04.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 20)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/05.gif" width="32" height="32" align="middle"/> ';
elseif ($chucdanh<= 30)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/06.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 40)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/07.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 50)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/08.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 60)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/09.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 70)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/10.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 85)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/11.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 100)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/12.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 115)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/13.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 130)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/14.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 145)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/15.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 160)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/16.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 175)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/17.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 190)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/18.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 205)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/19.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 220)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/20.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 240)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/21.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 260)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/22.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 280)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/23.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 300)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/24.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 320)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/25.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 340)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/26.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 360)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/27.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 380)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/28.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 400)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/29.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 430)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/30.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 460)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/31.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 490)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/32.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 510)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/33.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 540)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/34.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 570)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/35.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 600)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/36.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 630)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/37.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 660)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/38.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 690)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/39.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 760)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/40.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 800)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/41.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 840)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/42.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 880)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/43.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 920)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/44.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 960)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/45.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1000)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/46.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1040)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/47.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1080)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/48.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1200)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/49.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1300)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/50.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1400)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/51.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1500)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/52.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1600)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/53.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1700)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/54.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1800)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/55.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1900)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/56.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2000)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/57.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2200)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/58.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2500)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/59.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 3000)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/60.png" width="32" height="32" align="middle"/>';
}
}
if ($set['danhhieu']==1){
$exp = $res_u['postforum']*50;
if ($exp >= 0 && $exp <3000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/gacon.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 3000 && $exp <5250)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/buago.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 5250 && $exp <8250)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/buagodoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 8250 && $exp <12750)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/buada.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 12750 && $exp <19500)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/buadadoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 19500 && $exp <31500)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riusat.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 31500 && $exp <46500)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riusatdoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 46500 && $exp <70500)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riubac.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 70500 && $exp <102000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riubacdoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 102000 && $exp <165000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuvang.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 165000 && $exp <240000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuvangdoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 240000 && $exp <330000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchiensat.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 330000 && $exp <435000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchiensatcham.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 435000 && $exp <585000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchienbac.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 585000 && $exp <765000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchienbaccham.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 765000 && $exp <1140000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchienvang.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 1140000 && $exp <1650000)
{

$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchienvangcham.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 1650000)
{

$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/vip.gif" width="25" height="15" />';
}
}
return $chucdanh;
}
public static function capbac2(){
global $website,$set,$user_id,$user_u,$res,$parent,$type1;
if(!$user_u)
$req_u = mysql_query("SELECT * FROM `users` WHERE `website` = '$website' AND `id` = '$user_id' LIMIT 1");
else
$req_u = mysql_query("SELECT * FROM `users` WHERE `website` = '$website' AND `id` = '$user_u' LIMIT 1");
$res_u = mysql_fetch_array($req_u);
if ($set['danhhieu']==0){
if ($res_u['postforum'] != 0) {
$chucdanh= $res_u['postforum']/5;
if ($chucdanh<= 1)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/01.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 3)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/02.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 6)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/03.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 12)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/04.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 20)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/05.gif" width="32" height="32" align="middle"/> ';
elseif ($chucdanh<= 30)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/06.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 40)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/07.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 50)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/08.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 60)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/09.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 70)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/10.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 85)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/11.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 100)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/12.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 115)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/13.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 130)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/14.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 145)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/15.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 160)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/16.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 175)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/17.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 190)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/18.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 205)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/19.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 220)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/20.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 240)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/21.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 260)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/22.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 280)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/23.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 300)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/24.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 320)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/25.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 340)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/26.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 360)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/27.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 380)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/28.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 400)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/29.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 430)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/30.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 460)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/31.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 490)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/32.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 510)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/33.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 540)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/34.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 570)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/35.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 600)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/36.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 630)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/37.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 660)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/38.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 690)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/39.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 760)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/40.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 800)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/41.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 840)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/42.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 880)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/43.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 920)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/44.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 960)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/45.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1000)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/46.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1040)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/47.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1080)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/48.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1200)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/49.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1300)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/50.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1400)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/51.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1500)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/52.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1600)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/53.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1700)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/54.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1800)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/55.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1900)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/56.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2000)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/57.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2200)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/58.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2500)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/59.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 3000)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/60.png" width="32" height="32" align="middle"/>';
}
}
if ($set['danhhieu']==1){
$exp = $res_u['postforum']*50;
if ($exp >= 0 && $exp <3000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/gacon.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 3000 && $exp <5250)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/buago.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 5250 && $exp <8250)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/buagodoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 8250 && $exp <12750)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/buada.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 12750 && $exp <19500)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/buadadoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 19500 && $exp <31500)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riusat.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 31500 && $exp <46500)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riusatdoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 46500 && $exp <70500)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riubac.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 70500 && $exp <102000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riubacdoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 102000 && $exp <165000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuvang.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 165000 && $exp <240000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuvangdoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 240000 && $exp <330000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchiensat.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 330000 && $exp <435000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchiensatcham.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 435000 && $exp <585000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchienbac.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 585000 && $exp <765000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchienbaccham.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 765000 && $exp <1140000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchienvang.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 1140000 && $exp <1650000)
{

$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchienvangcham.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 1650000)
{

$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/vip.gif" width="25" height="15" />';
}
}
return $chucdanh;
}

/////////////////////
public static function thanhvien($s){
global $website,$act,$headmod,$user_id,$set;
if($s=='avt')
$var.='<a href="' . $set['homeurl'] . '/users/profile.php?act=images&amp;mod=avatar&amp;user=' . $user_id . '">';

if (file_exists('' .
    (isset($_GET['err']) || $headmod != "mainpage" || ($headmod == 'mainpage' && $act) ? '../' : '') . 'files/users/avatar/' . $user_id . '.png')){
	                           $var.='<img class="avatar" src="../files/users/avatar/' . $user_id . '.png" width="32" height="32" alt="' . $user_id . '" align="top" />&#160;';
     } 
  else
{
 $var.= '<img src="../images/empty.png" width="32" height="32" alt="' . $user_id . '" align="top" />&#160;';
}
return $var;
}




// Chuyên mục
public static function chuyenmuc(){
global $website;
$var.= '<div class="mainblok">';
$sql =mysql_query("SELECT * FROM `forum` where `website`='$website' and `type` = 'f' order by `realid` ASC")or die (mysql_error());
while($row =mysql_fetch_assoc($sql)){

$var.= '<div class="phdr"> <a href="'.$home.'/forum/'.functions::mikdaik(notags($row['text'])).'_'.$row['id'].'.html"> '.$row['text'].' </a> </div>';

$fsql =mysql_query("select * from `forum` where `website`='$website' and  `type` = 'r' && `refid` = '".$row['id']."' order by `realid` ASC");
while($frow =mysql_fetch_array($fsql)){
$total_post =mysql_result(mysql_query("select count(*) from `forum` where `website`='$website' and  `type` = 't' && `refid` = '{$frow['id']}'"),0);

$var.= '<div class="list1">
<table width="100%" cellpadding="0" cellspacing="0"><tbody><tr><td width="auto"><img src="/1.png">
<a href="'.$home.'/forum/'.functions::mikdaik(notags($frow['text'])).'_'.$frow['id'].'.html"> '.$frow['text'].' </a></td><td width="auto" align="right">[<font color="green">'.$total_post.'</font>]</td></tr></tbody></table></div>';


}}

return $var;
}
public static function ds(){
global $website;
$var.='<form name="cngtng">
<select name="url"size="1"onChange="window.open(this.options [this.selectedIndex].value,\'_top\')">
<option value="">Chuyên Mục</option>
';

$sql5 =mysql_query("SELECT * FROM `forum` where `website`='$website' and `type` = 'f' order by `realid` ASC");
while($row =mysql_fetch_assoc($sql5)){

$var.= '<option value="'.$home.'/forum/'.functions::mikdaik(notags($row['text'])).'_'.$row['id'].'.html">'.$row['text'].'
</option>
';

$fsql4 =mysql_query("select * from `forum` where `website`='$website' and `type` = 'r' && `refid` = '".$row['id']."' order by `realid` ASC");
while($frow =mysql_fetch_array($fsql4)){


$var.= '<option value="'.$home.'/forum/'.functions::mikdaik(notags($frow['text'])).'_'.$frow['id'].'.html">->'.$frow['text'].'</option>';

}}

$var.= '</select></form>';

return $var;
}
public static function chatbox(){
global $website,$set_user,$realtime,$user_id,$admp;
if ($user_id) {
$var.= '<div class="mainblok"><div class="phdr">
 Shoutbox</div></div>';
$var.= '<div class="list1"><form name="form" action="/guestbook/index.php?act=say" method="post">' ;
$var.= '<input type="text" name="msg" rows="2">' ;
$var.= '<input type="submit" name="submit" value="Gửi"></form></div>' ;
$var.= '<div class="gmenu">';
require_once (''.$_SERVER['DOCUMENT_ROOT'].'/incfiles/shout.php');
$var.= '</div><div class="list1"><center><a href="">Refresh</a> | <a href="../pages/faq.php?act=smileys">Smiles</a> | <a href="../guestbook/index.php">Shoutbox</a></center></div>';
} else {
$var.= '<div class="mainblok"><div class="phdr">
 Shoutbox</div></div>';
$var.= '<div class="gmenu">';
require_once (''.$_SERVER['DOCUMENT_ROOT'].'/incfiles/shout.php');
$var.= '</div><div class="list1"><center><a href="">Refresh</a> | <a href="../pages/faq.php?act=smileys">Smiles</a> | <a href="../guestbook/index.php">Shoutbox</a></center></div>';
}

return $var;
}
////// Shout Box

// Blog
public static function blog(){
global $website,$start,$kmess;
$var.= '<div class="mainblok"><div class="phdr">Bài Viết mới</div>';
$req =@mysql_query("SELECT * FROM `forum` WHERE `website`='$website' and `type`='t' AND `close`!='1' ORDER BY rand() DESC LIMIT $start, $kmess");
$total=@mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website`='$website' and `type`='t' AND `close`!='1'"),0)or die (mysql_error());

if($total>0){
$i = 0;
while (($res =@mysql_fetch_assoc($req)) !== false) {
$rq=@mysql_query("SELECT `text` FROM `forum` WHERE `website`='$website' and `type`='m' AND `refid`='".$res['id']."' ORDER BY rand() LIMIT 1");
$rs=@mysql_fetch_array($rq);
$gettop = mysql_fetch_assoc(mysql_query("SELECT * FROM `forum` WHERE `website`='$website' and `type` = 'r' and id = '".$res['refid']."'"));
		$topic = $gettop['text'];
$text=mb_substr($rs['text'],0,200,'UTF-8');
$text=bbcode::tags($text);
$text=strip_tags($text,'<b><br>');
if(preg_match('#\[img\](https?://.+?)\[\/img\]#i', $rs['text'], $Img)) {
$img=$Img[1];
} else {
$img='http://infoviet.net/images/noimage.png';
}
$nikuser =@mysql_query("SELECT `from` FROM `forum` WHERE `website`='$website' and `type` = 'm' AND `close` != '1' AND `refid` = '" . $res['id'] . "' ORDER BY `time` DESC LIMIT 1");
$nam =@mysql_fetch_assoc($nikuser);
$colmes =@mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website`='$website' and `type`='m' AND `refid`='" . $res['id'] . "'" . ($rights >= 7 ? '' : " AND `close` != '1'"));
$colmes1 =@mysql_result($colmes, 0);
$cpg = ceil($colmes1 / $kmess);
$var.= '<div class="list1">';
$var.= '<table cellpdding="0" cellspacing="0" width="100%"><tr><td width="40" align="center">';
$var.= '<img class="image" src="'.$img.'" width="45px" height="55px"/>';


$var.= '</td><td width="auto" valign="top"><a href="'.$home.'/forum/'.functions::mikdaik(notags($res['text'])).'_'.$res['id'].'.html"> '.$res['text']. '</a><div class="sub"><span class="gray">' . functions::display_date($res['time']) . ' </span></div>';
$var.= ' Chuyên Mục: '.$topic.'';





$var.= '</td></tr></table>';
$var.= '</div>';
$i;
}
} else {
$var.= '<div class="menu"><p>Không có bài mới</p></div>';
}




$var.= '</div>';
return $var;
}
// Topx
public static function topx()
 {
 global $website,$start,$kmess,$set;
$var.= '<div class="mainblok"><div class="phdr"><a> Diễn đàn</a><a href="' . $set['homeurl'] . '/forum" style="float:right;"><b>+</b></a></div>';
if ($set['kiemduyet'] !=0) {
$req = mysql_query("SELECT * FROM `forum` where `website` = '$website'  AND `kiemduyet`='1' AND  `type` = 't' and kedit='0' AND `close`!='1' ORDER BY `time` DESC LIMIT $start, $kmess");
} else{
$req = mysql_query("SELECT * FROM `forum` where `website` = '$website'  AND  `type` = 't' and kedit='0' AND `close`!='1' ORDER BY `time` DESC LIMIT $start, $kmess");

}

$total = mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` where `website` = '$website' AND  `type` = 't' and kedit='0' AND `close`!='1'"), 0);


while ($arr = mysql_fetch_array($req)) {
$q3 = mysql_query("select `id`, `refid`, `text` from `forum` where `website` = '$website' AND  type='r' and id='" . $arr['refid'] . "'");
$razd = mysql_fetch_array($q3);
$q4 = mysql_query("select `id`, `refid`, `text` from `forum` where `website` = '$website' AND  type='f' and id='" . $razd['refid'] . "'");
$frm = mysql_fetch_array($q4);
$nikuser = mysql_query("SELECT `from`,`id`, `time` FROM `forum` where `website` = '$website' AND  `type` = 'm' AND `close` != '1' AND `refid` = '" . $arr['id'] . "'ORDER BY time DESC");
$colmes1 = mysql_num_rows($nikuser);
$cpg = ceil($colmes1 / $kmess);
$nam = mysql_fetch_array($nikuser);
$trang4 = mysql_query("SELECT * FROM `forum_thank` where `website` = '$website' AND  `topic` = '" . ($nam['id'] + 1) . "'");
$trang5 = mysql_num_rows($trang4);
$trangxinhdep2 = mysql_query("select `rights` from `users` where `website` = '$website' AND  id='" . $nam['user_id'] . "'");
$trang3 = mysql_fetch_array($trangxinhdep2);
$var.= is_integer($i / 2) ? '<div class="gmenu">' : '<div class="gmenu">';
$var.= '<img src="http://img.infoviet.net/images/' . ($arr['edit'] == 1 ? 'tz' : 'np') . '.gif" alt=""/>';
if ($arr['realid'] == 1)
$var.= '&nbsp;<img src="http://img.infoviet.net/images/rate.gif" alt=""/>';
if ($arr['realid'] == 1)
$var.= '&nbsp;<img src="http://img.infoviet.net/images/rate.gif" alt="Bình chọn"/>';
if ($arr['tiento'] ==1) {
$var.='<b><font color="red">[Share]</font></b>';
} elseif ($arr['tiento'] ==2) {
$var.='<b><font color="blue">[Giúp]</font></b
>';
} elseif ($arr['tiento'] ==3) {
$var.='<b><font color="red">[HOT]</font></b>';
} elseif ($arr['tiento'] ==4) {
$var.='<b><font color="red">[Thông báo]</font></b>';
} elseif ($arr['tiento'] ==5) {
$var.='<b><font color="green">[Thảo luận]</font></b>';
} elseif ($arr['tiento'] ==6) {
$var.='<b><font color="blue">[Hướng dẫn]</font></b>';
}
$var.= '&nbsp;<a href="'.$home.'/forum/'.functions::mikdaik($arr["text"]).'_' . $arr['id'] . ($cpg > 1 && $set_forum['upfp'] && $set_forum['postclip'] ? '_clip_' : '') . ($set_forum['upfp'] && $cpg > 1 ? '_p' . $cpg : '.html') . '">' . bbcode::tags($arr['text']) . '</a>';
if ($cpg <= 1){
$var.= '&nbsp;(' . $colmes1 . ')';
}
if ($cpg > 1)
$var.= '&nbsp;(<a href="'.$home.'/forum/'.functions::mikdaik($arr["text"]).'_' . $arr['id'] . (!$set_forum['upfp'] && $set_forum['postclip'] ? '_clip_' : '') . ($set_forum['upfp'] ? '' : '_p' . $cpg) . '.html">' . $colmes1 . '</a>)';


if ($trang3['rights'] == 0 ) {
$colornick['colornick'] = '4e387e';
$colornickk['colornick'] = '4e387e';
}
if ($trang3['rights'] == 3 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($trang3['rights'] == 6 ) {
$colornick['colornick'] = '43d6d8';
$colornickk['colornick'] = '43d6d8';
}
if ($trang3['rights'] == 7 ) {
$colornick['colornick'] = 'ff0000';
$colornickk['colornick'] = 'ff0000';
}
if ($trang3['rights'] == 9 ) {
$colornick['colornick'] = 'ff00ff';
$colornickk['colornick'] = 'ff00ff';
}


if (!empty ($nam['from'])) {
$var.= '&nbsp;<font color="#' . $colornick['colornick'] . '">' .$nam['from']. '</font>
';
}
$var.= '</div>';
$i++;
}

$var.= '</div>';

if ($total > $kmess) {
$var.= '<div class="list1">' . functions::display_pagination('index.php?', $start, $total, $kmess) . '</div>';
}
return $var;
}

public static function tintuc()
{
global $website,$start,$kmess;
$req = mysql_query("SELECT * FROM `forum` where `website` = '$website' AND  `type`='t' AND `portal`>'0' ORDER BY `portal` DESC LIMIT 3");
if(mysql_num_rows($req)!=0) {
$var.= '<div class="mainblok"><div class="phdr">
<table width="100%">
<tr>
<td> Tin Tức HOT</td>
<td align="right"><a href=".../forum"><b> </b> </a></td>
</tr>
</table>
</div></div>';}

$i = 0;
while($res = mysql_fetch_array($req)) {
$noidung = mysql_query("SELECT `id`, `refid`, `text` FROM `forum` where `website` = '$website' AND  `type`='m' AND `refid`='" . $res['id'] . "'");
$t = mysql_fetch_array($noidung);
$q3 = mysql_query("SELECT `id`, `refid`, `text` FROM `forum` where `website` = '$website' AND  `type`='r' AND `id`='" . $res['refid'] . "'");
$razd = mysql_fetch_array($q3);
$q4 = mysql_query("SELECT `text` FROM `forum` where `website` = '$website' AND  `type`='f' AND `id`='" . $razd['refid'] . "'");
$frm = mysql_fetch_array($q4);
$colmes = mysql_query("SELECT * FROM `forum` where `website` = '$website' AND  `refid` = '" . $res['id'] . "' AND `type` = 'm'" . ($rights >= 7 ? '' : " AND `close` != '1'") . " ORDER BY `time` DESC");
$colmes1 = mysql_num_rows($colmes);
$cpg = ceil($colmes1 / $kmess);

$var.= '<div class="pad5">';
$var.= '&#160;<a href="../forum/' . functions::mikdaik($res['text']) . '_' . $res['id'] . '.html"><b>' . bbcode::tags($res['text']) .'</b></a>&#160;<br />';
$var.= '<span class="gray">'.$res['from'].' - ' . functions::display_date($res['time']) . '</span><br/>';

/*
-----------------------------------------------------------------
Noi dung bai viet
-----------------------------------------------------------------
*/

$text = $t['text'];
$cut = '200';

$text = mb_substr($text, 0, $cut);
$text = functions::checkout($text, 1, 1);
if ($set_user['smileys'])
$text = functions::smileys($text);
$var.= $text;

$var.= '</div>';
++$i;
}
return $var;
}
// Func ảnh
public static function anh()
{
global $website;
$hammad4=mysql_query("SELECT `id`, `user_id`, `tmb_name`, `time` FROM `cms_album_files` where `website`='$website' order by rand() limit 3") or die (mysql_error());
if(mysql_num_rows($hammad4)!=0)
{
$var.='<div class="phdr"><b>Album Ảnh</b></div><div class="list2">';
while($ar34=mysql_fetch_array($hammad4)){
$var.='<img src="../files/users/album/'.$ar34['user_id'] .'/'.$ar34['tmb_name'] .'" alt="'.$ar34['id'] .'"
height="42" width="42"
/> ';
}
$var.='</div>';
}
return $var;
}
// Func Trắc Nghiệm

public static function tracngiem()
{
global $website;

$req = mysql_query("SELECT * FROM `quiz` where `website` = '$website' AND  `type`='q' ORDER BY `time` DESC LIMIT 5");
if(mysql_num_rows($req)!=0) {
$var.= '<div class="mainblok"><div class="phdr"><a> Trắc nghiệm</a><a href="users/quiz.php" style="float:right;"><b>+</b></a></div>';

}
$i = 0;
while($res = mysql_fetch_assoc($req)) {
$var.= $i % 2 ? '<div class="list1">' : '<div class="list1">';
$price = $res['price'];
$text = functions::checkout($res['text'], 1, 1);
$text = functions::smileys($text, 1);
$var.= '» <a href="users/quiz.php?quiz='.$res['id'].'"> '.$text.' ('.$price.'xu)'.($user_id ? ' </a>' : '</a>');
$var.= '</div>';
++$i;
}
return $var;
}
// Func Member

public static function member()
{
global $website;
$var.='<div class="phdr">Trực tuyến</div>';
$var.= '<div class="orangex">';
$onltime = '".time() - 300."';
$gbot = 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)';
$yanbot = 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)';
$bing = 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)';
$msn = 'msnbot/1.1 (+http://search.msn.com/msnbot.htm)';
$bd = 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)';
$DCM = 'DoCoMo/2.0 N905i(c100;TB;W24H16) (compatible; Googlebot-Mobile/2.1; +http://www.google.com/bot.html)';
$yahoo = 'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)';
$Ahrefs = 'Mozilla/5.0 (compatible; AhrefsBot/3.1; +http://ahrefs.com/robot/)';
$Sosos = 'Sosospider+(+http://help.soso.com/webspider.htm)';

$googlebot = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$gbot' AND `website` = '$website'"), 0);
$yandexbot = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$yanbot' AND `website` = '$website'"), 0);
$bingbot = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$bing' AND `website` = '$website'"), 0);
$baidu = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$msn' AND `website` = '$website'"), 0);
$msnbot = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$bd' AND `website` = '$website'"), 0);
$yahoobot = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$yahoo' AND `website` = '$website'"), 0);
$DoCoMo = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$DCM' AND `website` = '$website'"), 0);
$AhrefsBot = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$Ahrefs' AND `website` = '$website'"), 0);
$Sosospider = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$Sosos' AND `website` = '$website'"), 0);
$usere = mysql_result(mysql_query("SELECT COUNT(*) FROM `users` WHERE `website` = '$website' AND `lastdate` > '" . (time() - 300) . "'"), 0);
$tamune = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `website` = '$website' AND `lastdate` > '" . (time() - 300) . "'"), 0);
$total = $total+$googlebot+$msnbot+$yandexbot+$bingbot+$yahoobot+$baidu+$DoCoMo+$AhrefsBot+$Sosospider+$usere+$tamune;

if ($total) {
$var.= '•Online: '.($googlebot > 0 ? '<font color="red">Google[BOT]</font>, ' : '').''.($yandexbot > 0 ? '<font color="117cd6">Yandex[BOT]</font>, ' : '').''.($bingbot > 0 ? '<font color="red">Bing[BOT]</font>, ' : '').''.($baidu > 0 ? '<font color="1d942e">Baidu[BOT]</font>, ' : '').''.($msnbot > 0 ? '<font color="c4a916">MSN[BOT]</font>, ' : '').''.($yahoobot > 0 ? '<font color="b70db9">Yahoo[BOT]</font>, ' : '').''.($DoCoMo > 0 ? '<font color="red">DoCoMo[BOT]</font>, ' : '').''.($AhrefsBot > 0 ? '<font color="red">AhrefsBot[BOT]</font>, ' : '').''.($Sosospider > 0 ? '<font color="0d66b9">Soso[BOT]</font>, ' : '').'';
}
$online = array();
$hadir = @mysql_query("SELECT * FROM `users` WHERE `lastdate` >= '" . intval(time() - 300) . "' AND `website` = '$website' ORDER BY RAND() LIMIT 5 ;");
$hitung = mysql_num_rows($hadir);
while ($notal = mysql_fetch_array($hadir)){
$online[] = '<a href="' . core::$system_set['homeurl'] . '/users/profile.php?user=' . $notal['id'] . '">' . $notal['name'] . '</a>';
}
if (($usere >= 1) && ($usere <= 5)){
$var.= implode(', ',$online).'.';
} elseif ($usere >= 6){
$minus = $usere - 5;
$var.= implode(', ',$online).'.';
$var.= ' <a href="' . core::$system_set['homeurl'] . '/users/index.php?act=online">' . $minus . ' thêm</a>';
}
$var.= '<br/>•Khách: ';
if ($tamune >= 1)
$var.= '<a href="' . core::$system_set['homeurl'] . '/users/index.php?act=online&amp;mod=guest">' . $tamune . '</a>';
else
$var.= 'No Guest';
//new member//
$var.= '<br/>•T.v mới:&nbsp;';
$req = mysql_query("SELECT `id`, `name`, `datereg` FROM `users` WHERE (preg='1') AND `website` = '$website' ORDER BY `datereg` DESC LIMIT 1; ");
$arr = mysql_fetch_array($req);
$var.= '<a href="' . core::$system_set['homeurl'] . '/'.$arr['id'].'-'.$arr['name'].'">'.$arr['name'].'</a>';
$var.='</div>';
return $var;
}
public static function thongke()
{
global $website;
$var.= '<div class="phdr">Thống kê</div>';
$var.='<div class="orangex">';
$var.='<div class="list2">Chủ đề: <b>' . mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website`='$website' and `type` = 't' AND `close` != '1'"), 0) . '</b></div>';
$var.='<div class="list2">Bài viết: <b>' . mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website`='$website' and `type` = 'm' AND `close` != '1'"), 0) . '</b></div>';
$var.='<div class="list2">Thành viên: <b>' . mysql_result(mysql_query("SELECT COUNT(*) FROM `users` where `website`='$website'"), 0) . '</b></div>';
$var.='</div>';
return $var;
}






/////////////
public static function catt(){
global $website,$id,$_SESSION,$lng;
$req = mysql_query("SELECT `id`, `text`, `soft` FROM `forum` WHERE `website` = '$website' AND `type`='r' AND `refid`='$id' ORDER BY `realid`");
$total = mysql_num_rows($req);
if ($total) {
$i = 0;
while (($res = mysql_fetch_assoc($req)) !== false) {
$var.= $i % 2 ? '<div class="list2">' : '<div class="list1">';
$coltem = mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` WHERE `type` = 't' AND `website` = '$website' AND  `refid` = '" . $res['id'] . "'"), 0);
$var.= '<a href="'.$home.'/forum/' . functions::mikdaik($res['text']) . '_'.$res['id'].'.html">' . $res['text'] . '</a>';
if ($coltem)
$var.= " [$coltem]";
if (!empty($res['soft']))
$var.= '<div class="sub"><span class="gray">' . $res['soft'] . '</span></div>';
$var.= '</div>';
++$i;
}

unset($_SESSION['fsort_id']);
unset($_SESSION['fsort_users']);
} else {
$var.= '<div class="menu"><p>Trống</p></div>';
}
$var.= '<div class="phdr">'.$lng['total'].': ' . $total . '</div>';
return $var;
}








/*//////////

Chuyên mục con


/////////// */
public static function catr(){
global $website,$id,$user_id,$ban,$set_user,$lng_forum,$lng,$rights,$start,$kmess,$_SESSION,$lng_forum;
$total = mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website` = '$website' AND `type`='t' AND `refid`='$id'" . ($rights >= 7 ? '' : " AND `close`!='1'")), 0);
if (($user_id && !isset($ban['1']) && !isset($ban['11']) && $set['mod_forum'] != 3) || core::$user_rights) {
// Кнопка создания новой темы
$var.= '<div class="gmenu"><form action="index.php?act=nt&amp;id=' . $id . '" method="post"><input type="submit" value="' . $lng_forum['new_topic'] . '" /></form></div>';
}
if ($total) {
if($set['kiemduyet'] !=0)
$req = mysql_query("SELECT * FROM `forum` WHERE `website` = '$website' AND `kiemduyet`='1' AND `type`='t'" . ($rights >= 7 ? '' : " AND `close`!='1'") . " AND `refid`='$id' ORDER BY `vip` DESC, `time` DESC LIMIT $start, $kmess");
else 
$req = mysql_query("SELECT * FROM `forum` WHERE `website` = '$website' AND `type`='t'" . ($rights >= 7 ? '' : " AND `close`!='1'") . " AND `refid`='$id' ORDER BY `vip` DESC, `time` DESC LIMIT $start, $kmess");

$i = 0;
while (($res = mysql_fetch_assoc($req)) !== false) {
if ($res['close'])
$var.= '<div class="rmenu">';
else
$var.= $i % 2 ? '<div class="list2">' : '<div class="list1">';
$nikuser = mysql_query("SELECT `from` FROM `forum` WHERE `type` = 'm' AND `website` = '$website' AND  `close` != '1' AND `refid` = '" . $res['id'] . "' ORDER BY `time` DESC LIMIT 1");
$nam = mysql_fetch_assoc($nikuser);
$colmes = mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website` = '$website' AND `type`='m' AND `refid`='" . $res['id'] . "'" . ($rights >= 7 ? '' : " AND `close` != '1'"));
$colmes1 = mysql_result($colmes, 0);
$cpg = ceil($colmes1 / $kmess);
$np = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_forum_rdm` WHERE `website` = '$website' AND `time` >= '" . $res['time'] . "' AND `topic_id` = '" . $res['id'] . "' AND `user_id`='$user_id'"), 0);
// Значки
$icons = array(
($np ? (!$res['vip'] ? '<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/op.gif" alt=""/>' : '') : '<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/np.gif" alt=""/>'),
($res['vip'] ? '<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/pt.gif" alt=""/>' : ''),
($res['realid'] ? '<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/rate.gif" alt=""/>' : ''),
($res['edit'] ? '<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/tz.gif" alt=""/>' : '')
);
$var.= functions::display_menu($icons, '&#160;', '&#160;');
$var.= '<a href="'.$home.'/forum/' . functions::mikdaik($res['text']) . '_' . $res['id'] . '.html">' . bbcode::tags($res['text']) . '</a> [' . $colmes1 . ']';
if ($cpg > 1) {
$var.= '<a href="'.$home.'/forum/' . functions::mikdaik($res['text']) . '_' . $res['id'] . '_p' . $cpg . '.html">*&gt;&gt;</a>';
}
$var.= '<div class="sub">';
$var.= $res['from'];
if (!empty($nam['from'])) {
$var.= '&#160;/&#160;' . $nam['from'];
}
$var.= ' <span class="gray">(' . functions::display_date($res['time']) . ')</span></div></div>';
++$i;
}
unset($_SESSION['fsort_id']);
unset($_SESSION['fsort_users']);
} else {
$var.= '<div class="menu"><p>' . $lng_forum['topic_list_empty'] . '</p></div>';
}
$var.= '<div class="phdr">' . $lng['total'] . ': ' . $total . '</div>';
if ($total > $kmess) {
$var.= '<div class="topmenu">' . functions::display_pagination('index.php?id=' . $id . '&amp;', $start, $total, $kmess) . '</div>' .
'<p><form action="index.php?id=' . $id . '" method="post">' .
'<input type="text" name="page" size="2"/>' .
'<input type="submit" value="' . $lng['to_page'] . ' &gt;&gt;"/>' .
'</form></p>';
}




return $var;
}


/*/////////////


Bài viết

////////////////*/


public static function catb(){
global $website,$_SESSION,$user_id,$type1,$curators,$key,$lng_forum,$value,$realtime,$lng,$set_user,$id,$start,$set_forum,$kmess,$rights,$datauser;
$parent = $type1['refid'];
$filter = isset($_SESSION['fsort_id']) && $_SESSION['fsort_id'] == $id ? 1 : 0;
$sql = '';
if ($filter && !empty($_SESSION['fsort_users'])) {
// Подготавливаем запрос на фильтрацию юзеров
$sw = 0;
$sql = ' AND (';
$fsort_users = unserialize($_SESSION['fsort_users']);
foreach ($fsort_users as $val) {
if ($sw)
$sql .= ' OR ';
$sortid = intval($val);
$sql .= "`forum`.`user_id` = '$sortid'";
$sw = 1;
}
$sql .= ')';
}
if ($user_id && !$filter) {
// Фиксация факта прочтения топика
}
if ($rights < 7 && $type1['close'] == 1) {
$var.= '<div class="rmenu"><p>' . $lng_forum['topic_deleted'] . '<br/><a href="?id=' . $type1['refid'] . '">' . $lng_forum['to_section'] . '</a></p></div>';
require('../incfiles/end.php');
exit;
}
$checkthankdau = mysql_query('SELECT COUNT(*) FROM `forum_thank` WHERE `website` = "'.$website.'" AND `userthank` = "' . $user_id . '" and `topic` = "' . trim($_GET['thanks']) . '" and `user` = "' . trim($_GET['user']) . '"');
if ($user_id && $user_id != $_GET['user'] && (mysql_result($checkthankdau, 0) < 1)) {
if ((isset ($_GET['thank']))&&(isset ($_GET['user']))&&(isset ($_GET['thanks']))) {


$var.= '<div class="rmenu" id="thanksyou"><b>Bạn đã cảm ơn bài viết thành công.</b></div>';
mysql_query("INSERT INTO `forum_thank` SET
`user` = '".trim($_GET['user'])."',
`topic` = '".trim($_GET['thanks'])."' ,
`time` = '$realtime',
					`website` = '$website',
`userthank` = '$user_id',
`chude` = '".trim($_GET["id"])."'
");
$congcamon=mysql_fetch_array(mysql_query('SELECT * FROM `users` WHERE `website` = "'.$website.'" AND `id` = "' . trim($_GET['user']) . '"'));
mysql_query("UPDATE `users` SET `thank_duoc`='" . ($congcamon['thank_duoc'] + 1) . "' WHERE `website` = '$website' AND `id` = '" . trim($_GET['user']) . "'");
mysql_query("UPDATE `users` SET `thank_di`='" . ($datauser['thank_di'] + 1) . "' WHERE `website` = '$website' AND `id` = '" . $user_id . "'");
mysql_query("UPDATE `users` SET `balans`='" . ($congcamon['balans'] + 20) . "' WHERE `website` = '$website' AND `id` = '" . trim($_GET['user']) . "'");
}
}
// Счетчик постов темы
$colmes = mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website` = '$website' AND `type`='m'$sql AND `refid`='$id'" . ($rights >= 7 ? '' : " AND `close` != '1'")), 0);
if ($start > $colmes) $start = $colmes - $kmess;
// Выводим название топика
$var.= '<div class="mainblok"><div class="phdr"><a name="up" id="up"></a><a href="#down"><img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/down.png" alt="Вниз" width="20" height="10" border="0"/></a>&#160;&#160;<b>' . bbcode::tags($type1['text']) . '</b></div>';
if ($colmes > $kmess)
$var.= '<div class="topmenu">' . functions::display_pagination2(''.$home.'/forum/'.functions::mikdaik($type1["text"]).'_' . $id, $start, $colmes, $kmess) . '</div>';
$var.= '</div>';
// Метки удаления темы
if ($type1['close'])
$var.= '<div class="rmenu">' . $lng_forum['topic_delete_who'] . ': <b>' . $type1['close_who'] . '</b></div>';
elseif (!empty($type1['close_who']) && $rights >= 7)
$var.= '<div class="gmenu"><small>' . $lng_forum['topic_delete_whocancel'] . ': <b>' . $type1['close_who'] . '</b></small></div>';
if (!empty($type1['kiemduyet_who'])) {
$var.= '<div class="gmenu">Chủ đề được kiểm duyệt bởi: <b>' . $type1['kiemduyet_who'] . '</b></div>';
}
// Метки закрытия темы
if ($type1['edit'])
$var.= '<div class="rmenu">' . $lng_forum['topic_closed'] . '</div>';
/*
-----------------------------------------------------------------
Блок голосований
-----------------------------------------------------------------
*/
if ($type1['realid']) {
$clip_forum = isset($_GET['clip']) ? '&amp;clip' : '';
$vote_user = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_forum_vote_users` WHERE `website` = '$website' AND `user`='$user_id' AND `topic`='$id'"), 0);
$topic_vote = mysql_fetch_assoc(mysql_query("SELECT `name`, `time`, `count` FROM `cms_forum_vote` WHERE `website` = '$website' AND `type`='1' AND `topic`='$id' LIMIT 1"));
$var.= '<div  class="gmenu"><b>' . functions::checkout($topic_vote['name']) . '</b><br />';
$vote_result = mysql_query("SELECT `id`, `name`, `count` FROM `cms_forum_vote` WHERE `website` = '$website' AND `type`='2' AND `topic`='" . $id . "' ORDER BY `id` ASC");
if (!$type1['edit'] && !isset($_GET['vote_result']) && $user_id && $vote_user == 0) {
// Выводим форму с опросами
$var.= '<form action="index.php?act=vote&amp;id=' . $id . '" method="post">';
while (($vote = mysql_fetch_assoc($vote_result)) !== false) {
$var.= '<input type="radio" value="' . $vote['id'] . '" name="vote"/> ' . functions::checkout($vote['name'], 0, 1) . '<br />';
}
$var.= '<p><input type="submit" name="submit" value="' . $lng['vote'] . '"/><br /><a href="index.php?id=' . $id . '&amp;start=' . $start . '&amp;vote_result' . $clip_forum .
'">' . $lng_forum['results'] . '</a></p></form></div>';
} else {
// Выводим результаты голосования
$var.= '<small>';
while (($vote = mysql_fetch_assoc($vote_result)) !== false) {
$count_vote = $topic_vote['count'] ? round(100 / $topic_vote['count'] * $vote['count']) : 0;
$var.= functions::checkout($vote['name'], 0, 1) . ' [' . $vote['count'] . ']<br />';
$var.= '<img src="vote_img.php?img=' . $count_vote . '" alt="' . $lng_forum['rating'] . ': ' . $count_vote . '%" /><br />';
}
$var.= '</small></div><div class="bmenu">' . $lng_forum['total_votes'] . ': ';
if ($datauser['rights'] > 6)
$var.= '<a href="index.php?act=users&amp;id=' . $id . '">' . $topic_vote['count'] . '</a>';
else
$var.= $topic_vote['count'];
$var.= '</div>';
if ($user_id && $vote_user == 0)
$var.= '<div class="bmenu"><a href="index.php?id=' . $id . '&amp;start=' . $start . $clip_forum . '">' . $lng['vote'] . '</a></div>';
}
}
$curators = !empty($type1['curators']) ? unserialize($type1['curators']) : array();
$curator = false;
if ($rights < 6 && $rights != 3 && $user_id) {
if (array_key_exists($user_id, $curators)) $curator = true;
}
/*
-----------------------------------------------------------------
Фиксация первого поста в теме
-----------------------------------------------------------------
*/
if (($set_forum['postclip'] == 2 && ($set_forum['upfp'] ? $start < (ceil($colmes - $kmess)) : $start > 0)) || isset($_GET['clip'])) {
$postreq = mysql_query("SELECT `forum`.*, `users`.`sex`, `users`.`rights`, `users`.`lastdate`, `users`.`status`, `users`.`datereg`
FROM `forum` LEFT JOIN `users` ON `forum`.`user_id` = `users`.`id`
WHERE `forum`.`type` = 'm' AND `forum`.`refid` = '$id'" . ($rights >= 7 ? "" : " AND `forum`.`website` = '$website' AND  `forum`.`close` != '1'") . "
ORDER BY `forum`.`id` LIMIT 1");
$postres = mysql_fetch_assoc($postreq);
$var.= '<div class="topmenu"><p>';
if ($postres['sex'])
$var.= '<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/' . ($postres['sex'] == 'm' ? 'm' : 'w') . ($postres['datereg'] > time() - 86400 ? '_new.png" width="14"' : '.png" width="10"') . ' height="10"/>&#160;';
else
$var.= '<img src="http://img.infoviet.net/images/del.png" width="10" height="10" alt=""/>&#160;';
if ($user_id && $user_id != $postres['user_id']) {
$var.= '<a href="../users/profile.php?user=' . $postres['user_id'] . '&amp;fid=' . $postres['id'] . '"><b>' . $postres['from'] . '</b></a> ' .
'<a href="index.php?act=say&amp;id=' . $postres['id'] . '&amp;start=' . $start . '"> ' . $lng_forum['reply_btn'] . '</a> ' .
'<a href="index.php?act=say&amp;id=' . $postres['id'] . '&amp;start=' . $start . '&amp;cyt"> ' . $lng_forum['cytate_btn'] . '</a> ';
} else {
$var.= '<b>' . $postres['from'] . '</b> ';
}
$user_rights = array(
1 => 'Kil',
3 => 'Mod',
6 => 'Smd',
7 => 'Adm',
8 => 'SV'
);
$var.= @$user_rights[$postres['rights']];
$var.= (time() > $postres['lastdate'] + 300 ? '<span class="red"> [Off]</span>' : '<span class="green"> [ON]</span>');
$var.= ' <span class="gray">(' . functions::display_date($postres['time']) . ')</span><br/>';
if ($postres['close']) {
$var.= '<span class="red">' . $lng_forum['post_deleted'] . '</span><br/>';
}
$var.= functions::checkout(mb_substr($postres['text'], 0, 500), 0, 2);
if (mb_strlen($postres['text']) > 500)
$var.= '...<a href="index.php?act=post&amp;id=' . $postres['id'] . '">' . $lng_forum['read_all'] . '</a>';
$var.= '</p></div>';
}
if ($filter)
$var.= '<div class="rmenu">' . $lng_forum['filter_on'] . '</div>';
// Задаем правила сортировки (новые внизу / вверху)
if ($user_id)
$order = $set_forum['upfp'] ? 'DESC' : 'ASC';
else
$order = ((empty($_SESSION['uppost'])) || ($_SESSION['uppost'] == 0)) ? 'ASC' : 'DESC';
// Запрос в базу
                $req = mysql_query("SELECT `forum`.*, `users`.`sex`, `users`.`rights`, `users`.`chuki`, `users`.`lastdate`, `users`.`status`, `users`.`datereg`
                FROM `forum` LEFT JOIN `users` ON `forum`.`user_id` = `users`.`id`
                WHERE `forum`.`type` = 'm' AND `forum`.`website` = '$website' AND `forum`.`refid` = '$id'"
                                   . ($rights >= 7 ? "" : " AND `forum`.`close` != '1'") . "$sql ORDER BY `forum`.`id` $order LIMIT $start, $kmess");
// Верхнее поле "Написать"
if (($user_id && !$type1['edit'] && $set_forum['upfp'] && $set['mod_forum'] != 3) || ($rights >= 7 && $set_forum['upfp'])) {
$var.= '<div class="gmenu"><form name="form1" action="index.php?act=say&amp;id=' . $id . '" method="post">';
if ($set_forum['farea']) {
$var.= '<p>' .
(!$is_mobile ? bbcode::auto_bb('form1', 'msg') : '') .
'<textarea rows="' . $set_user['field_h'] . '" name="msg"></textarea></p>' .
'<p><input type="checkbox" name="addfiles" value="1" /> ' . $lng_forum['add_file'] .
($set_user['translit'] ? '<br /><input type="checkbox" name="msgtrans" value="1" /> ' . $lng['translit'] : '') .
'</p><p><input type="submit" name="submit" value="' . $lng['write'] . '" style="width: 107px; cursor: pointer;"/> ' .
($set_forum['preview'] ? '<input type="submit" value="' . $lng['preview'] . '" style="width: 107px; cursor: pointer;"/>' : '') .
'</p></form></div>';
} else {
$var.= '<p><input type="submit" name="submit" value="' . $lng['write'] . '"/></p></form></div>';
}
}
if ($rights == 3 || $rights >= 6)
$var.= '<form action="index.php?act=massdel" method="post">';
$i = 1;
while (($res = mysql_fetch_assoc($req)) !== false) {
if ($res['close'])
$var.= '<div class="rmenu">';
else
$var.= $i % 2 ? '<div class="mainblok">' : '<div class="mainblok">';
if ($set_user['avatar']) {

$var.= '<div class="phdr"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td width="auto"><img src="http://img.infoviet.net/images/op.gif"/> ' . functions::display_date($res['time']) . '</td><td align="right" width="auto">#<a href="index.php?act=post&amp;id=' . $res['id']. '?#' .( $start + $i ). '"><b>' .( $start + $i ). '</b></a></td></tr></tbody></table></div>';
$var.= '<div class="newsx"><table cellpadding="0" cellspacing="0" width="100%" style="margin-bottom:6px;"><tbody><tr><td width="32" style="text-align:center;">';
if (file_exists(('../files/users/avatar/' . $res['user_id'] . '.png')))
$var.= '<img src="../files/users/avatar/' . $res['user_id'] . '.png" width="32" height="32" alt="' . $res['from'] . '" />&#160;';
else
$var.= '<img src="http://img.infoviet.net/images/empty.png" width="32" height="32" alt="' . $res['from'] . '" />&#160;';
$var.= '</td><td style="text-align:left;">';
}
if ($res['sex'])
$var.= (time() > $res['lastdate'] + 300 ? '<font color="red"> • </font> ' : '<font color="green"> • </font> ');
else
$var.= '<img src="http://img.infoviet.net/images/del.png" width="12" height="12" align="middle" alt=""/>&#160;';
// Ник юзера и ссылка на его
if ($res['rights'] == 0 ) {
$colornick['colornick'] = '4e387e';
$colornickk['colornick'] = '4e387e';
}
if ($res['rights'] == 1 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($res['rights'] == 2 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($res['rights'] == 3 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($res['rights'] == 4 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($res['rights'] == 5 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($res['rights'] == 6 ) {
$colornick['colornick'] = '43d6d8';
$colornickk['colornick'] = '43d6d8';
}
if ($res['rights'] == 7 ) {
$colornick['colornick'] = 'ff0000';
$colornickk['colornick'] = 'ff0000';
}
if ($res['rights'] == 9 ) {
$colornick['colornick'] = 'ff00ff';
$colornickk['colornick'] = 'ff00ff';
}
if ($res['rights'] == 10 ) {
$colornick['colornick'] = 'ff0000';
$colornickk['colornick'] = 'ff0000';
}

// Метка должности
$user_rights = array(
0 => '<font color="gray">(</font><font
color="black">Member</font><font color="gray">)</font>',
1 => '<font color="gray">(</font><font
color="green">DMod</font><font color="gray">)</font>',
3 => '<font color="gray">(</font><font
color="green">Mod</font><font color="gray">)</font>',
4 => '<font color="gray">(</font><font
color="green>DMod</font><font color="gray">)</font>',
6 => '<font color="gray">(</font><font
color="blue">Smod</font><font color="gray">)</font>',
7 => '<font color="gray">(</font><font
color="red">Admin</font><font color="gray">)</font>',
9 => '<font color="gray">(</font><font color="red">Chủ Tịch Nước</font><font color="gray">)</font>',
10 => '<font color="gray">(</font><font color="red">Máy Chém
Tự Động</font><font color="gray">)</font>'
);








$user_u = $res['user_id'];
$req_u = mysql_query("SELECT * FROM `users` WHERE `website` = '$website' AND `id` = '$user_u' LIMIT 1");
$res_u = mysql_fetch_array($req_u);
	   // Ten Member
                    if ($user_id && $user_id != $res['user_id']) {
                        $var.= '<a href="../users/profile.php?user=' . $res['user_id'] . '"><font color="#' . $colornick['colornick'] . '"><b>' . $res['from'] . '</b>' .$vip1['vip1']. '</a> ';


} else {


$var.= '<b><span style="color:#' . $colornick['colornick'] . '">' .$vip['vip']. '' . $res['from'] . '' .$vip1['vip1']. '</b></span> ';


}				

                    $var.= '<span style="color:blue">['.$res_u['postforum'].']</span> | <span style="color:red">'.$res_u['balans'].''.$set['tiente'].'</span><div class="" style="float:right;"><b>Like: '.$res_u['thank_duoc'].'</b>
					<br/>'.functions::capbac($user_u).'</div> ';





// Статус юзера
if (!empty($res['status']))
$var.= '<div class="status"><img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/label.png" alt="" align="middle"/>&#160;' . $res['status'] . '<div class="" style="float:right;"> '.$chucdanh.' </div>';
if ($set_user['avatar']){

$var.= '</td></tr></tbody></table></div>';}
/*
-----------------------------------------------------------------
Вывод текста поста
-----------------------------------------------------------------
*/
$var.= '<div class="forumtxt">';
$text = $res['text'];
if ($set_forum['postcut']) {
// Если текст длинный, обрезаем и даем ссылку на полный вариант
switch ($set_forum['postcut']) {
case 2:
$cut = 10000;
break;

case 3:
$cut = 5000;
break;
default :
$cut = 1000;
}
}
if ($set_forum['postcut'] && mb_strlen($text) > $cut) {
$text = mb_substr($text, 0, $cut);
$text = functions::checkout($text, 1, 1);
$text = preg_replace('#\[c\](.*?)\[/c\]#si', '<div class="quote">\1</div>', $text);
if ($set_user['smileys'])
$text = functions::smileys($text, $res['rights'] ? 1 : 0);
if($user_id) {
$text = str_replace('[you]', $login, $text);
} else {
$text = str_replace('[you]', 'Khách', $text);
}
$checkthank = mysql_query('SELECT COUNT(*) FROM `forum_thank` WHERE `website` = "'.$website.'" AND `userthank` = "' . $user_id . '" and `topic` = "' . $res['id'] . '" and `user` = "' . $res['user_id'] . '"');

$thankcheck = mysql_result($checkthank, 0);
if($thankcheck < 1 && $user_id != $res['user_id']) {
$text = preg_replace('#\[thank\](.*?)\[/thank\]#si', '<div class="gmenu">Nội dung ẩn</div><div class="quote">Bạn vui lòng cảm ơn bài viết này để được xem nội dung ẩn</div>', $text);
} else {
$text = preg_replace('#\[thank\](.*?)\[/thank\]#si', '\1', $text);
}
$var.= bbcode::notags($text) . '...<br /><a href="'.$home.'/forum/' . functions::mikdaik($type1['text']) . '_p' . $res['id'] . '.html">' . $lng_forum['read_all'] . ' &gt;&gt;</a>';
} else {
// Или, обрабатываем тэги и выводим весь текст
$text = functions::checkout($text, 1, 1);
if ($set_user['smileys'])
$text = functions::smileys($text, $res['rights'] ? 1 : 0);
if($user_id) {
$text = str_replace('[you]', $login, $text);
} else {
$text = str_replace('[you]', 'Khách', $text);
}
$checkthank = mysql_query('SELECT COUNT(*) FROM `forum_thank` WHERE `website` = "'.$website.'" AND `userthank` = "' . $user_id . '" and `topic` = "' . $res['id'] . '" and `user` = "' . $res['user_id'] . '"');
$thankcheck = mysql_result($checkthank, 0);
if($thankcheck < 1 && $user_id != $res['user_id']) {
$text = preg_replace('#\[thank\](.*?)\[/thank\]#si', '<div class="gmenu">Nội dung ẩn</div><div class="quote">Bạn vui lòng cảm ơn bài viết này để được xem nội dung ẩn</div>', $text);
} else {
$text = preg_replace('#\[thank\](.*?)\[/thank\]#si', '\1', $text);
}
$var.= $text;

}
if ($res['kedit']) {
// Если пост редактировался, показываем кем и когда
$var.= '<br /><span class="gray"><small>' . $lng_forum['edited'] . ' <b>' . $res['edit'] . '</b> (' . functions::display_date($res['tedit']) . ') <b>[' . $res['kedit'] . ']</b></small></span>';
}
// Если есть прикрепленный файл, выводим его описание
$freq = mysql_query("SELECT * FROM `cms_forum_files` WHERE `website` = '$website' AND `post` = '" . $res['id'] . "'");
if (mysql_num_rows($freq) > 0) {
$fres = mysql_fetch_assoc($freq);
$fls = round(@filesize('../files/forum/attach/' . $fres['filename']) / 1024, 2);
$var.= '<br /><span class="gray">' . $lng_forum['attached_file'] . ':';
// Предпросмотр изображений
$att_ext = strtolower(functions::format('./files/forum/attach/' . $fres['filename']));
$pic_ext = array(
'gif',
'jpg',
'jpeg',
'png'
);
$req43=mysql_query("SELECT `postforum`, `id` FROM `users` WHERE `website` = '$website' AND  id='$user_id' ");
$arr56=mysql_fetch_array($req43);
$total_spent = 2;
$hammad= 3 - $arr56['postforum'];
if (in_array($att_ext, $pic_ext)) {
$var.= '<div><a href="index.php?act=file&amp;id=' . $fres['id'] . '">';
$var.= '<img src="thumbinal.php?file=' . (urlencode($fres['filename'])) . '" alt="' . $lng_forum['click_to_view'] . '" /></a></div><br/>';
} else if (empty($user_id)){
$var.= '<b>Bạn cần <a href="../login.php"><font color="red">đăng nhập</font></a> mới tải được file đính kèm</b>';
}else if($total_spent < $arr56['postforum']){
$var.= ' <a href="index.php?act=file&amp;id=' . $fres['id'] . '">' . $fres['filename'] . '</a>';
}
else $var.= '<b>Bạn vui lòng post đủ 3 bài viết để tải tập tin đính kèm</b>';
$var.= ' (' . $fls . ' Kb.)<br/>';
$var.= $lng_forum['downloads'] . ': ' . $fres['dlcount'] . ' ' . $lng_forum['time'] . '</span>';
$file_id = $fres['id'];

}

#############Th?ng kê s? ngu?i thanks
$thongkethank = mysql_query("SELECT COUNT(*) from `forum_thank` where `website` = '$website' AND `topic`='" . $res["id"] . "'");
$thongkethanks = mysql_result($thongkethank, 0);
// $thongkethanks=mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_thank` WHERE `topic` = "' . $res['id'] . '"')), 0);
$thongkea= @mysql_query("select * from `forum_thank` where `website` = '$website' AND `topic` = '" . $res['id'] . "'");
$thongke=mysql_fetch_array($thongkea);
$idthongke=trim($_GET['idthongke']);
if($thongkethanks>0&&(empty($_GET['idthongke'])))
{$var.='<div id="'.$idthongke.'" class="menu"><font color="red"><img src="http://img.infoviet.net/images/thich.gif" /></font> ';
$thongkeaa= @mysql_query("select * from `forum_thank` where `website` = '$website' AND `topic` = '" . $res['id'] . "'");while ($thongkea = mysql_fetch_array($thongkeaa))
{
{
$dentv=mysql_fetch_array(mysql_query('SELECT * FROM `users` WHERE `website` = "'.$website.'" AND `id` = "'.$thongkea['userthank'].'"'));
$var.= '<a href="/users/profile.php?id='.$thongkea['userthank'].'">'.$dentv['name'].'</a>, ';
}
++$f;
}
$var.='</div>';}
                   // Ket thuc
                                 
                    // Cap cua thanh vien 2
                    $user_rights = array(
                        0 => '<img src="http://img.infoviet.net/images/mem.gif"/>',
                        3 => '<img src="http://img.infoviet.net/images/mod.gif"/>',
                        6 => '<img src="http://img.infoviet.net/images/smod.gif"/>',
                        7 => '<img src="http://img.infoviet.net/images/admin.gif"/>',
                        9 => '<img src="http://img.infoviet.net/images/vip.gif"/>'
                    );
$var.= '<div style="text-align:right">' . $user_rights[$res['rights']] . '</div>';
if($res['chuki']){
$var.= '<div class="chuki">' .bbcode::tags($res['chuki']). '</div>';
}

//g0i data thanks
$checkthank = mysql_query('SELECT COUNT(*) FROM `forum_thank` WHERE `website` = "'.$website.'" AND `userthank` = "' . $user_id . '" and `topic` = "' . $res['id'] . '" and `user` = "' . $res['user_id'] . '"');
//end


                    
                        $var.= '<div class="forumb"><table cellpadding="0" cellspacing="0" width="100%"><tr><td width="auto" align="left"><a href="/users/profile.php?act=vipham&amp;user=' . $res['user_id'] . '"><font color="red"><img src="http://img.infoviet.net/images/vipham.gif" /></font></a>';


if ($user_id && $user_id != $res['user_id'] && (mysql_result($checkthank, 0) < 1)) {

						$var.= '<a href="index.php?id=' . $id . '&amp;thanks=' . $res['id'] . '&amp;user=' . $res['user_id'] . '&amp;start=' . $start . '&amp;thank#thanksyou"><img src="http://img.infoviet.net/images/thich.gif" /></a></td>';
						
						 }
						 $var.='
						<td width="auto" align="right"><a href="index.php?act=say&amp;id=' . $res['id'] . '&amp;start=' . $start . '"><img src="http://img.infoviet.net/images/reply.gif" /></a>&#160;' .
                                                  '<a href="index.php?act=say&amp;id=' . $res['id'] . '&amp;start=' . $start . '&amp;cyt"><img src="http://img.infoviet.net/images/quocte.gif" /></a></td></tr></table></div>';
                   
                         $var.= '</div>';
                    if ((($rights == 3 || $rights >= 6 || $curator) && $rights >= $res['rights']) || ($res['user_id'] == $user_id && !$set_forum['upfp'] && ($start + $i) == $colmes && $res['time'] > time() - 300) || ($res['user_id'] == $user_id && $set_forum['upfp'] && $start == 0 && $i == 1 && $res['time'] > time() - 300)) {
// Ссылки на редактирование / удаление постов
                        $menu = array(
                            '<a href="index.php?act=editpost&amp;id=' . $res['id'] . '">' . $lng['edit'] . '</a>',
                            ($rights >= 7 && $res['close'] == 1 ? '<a href="index.php?act=editpost&amp;do=restore&amp;id=' . $res['id'] . '">' . $lng_forum['restore'] . '</a>' : ''),
                            ($res['close'] == 1 ? '' : '<a href="index.php?act=editpost&amp;do=del&amp;id=' . $res['id'] . '">' . $lng['delete'] . '</a>')
                        );
                        $var.= '<div class="sub">';
                        if ($rights == 3 || $rights >= 6)
                            $var.= '<input type="checkbox" name="delch[]" value="' . $res['id'] . '"/>&#160;';
                        $var.= functions::display_menu($menu);
                        if ($res['close']) {
                            $var.= '<div class="red">' . $lng_forum['who_delete_post'] . ': <b>' . $res['close_who'] . '</b></div>';
                        } elseif (!empty($res['close_who'])) {
                            $var.= '<div class="green">' . $lng_forum['who_restore_post'] . ': <b>' . $res['close_who'] . '</b></div>';
                        }
                      $var.= '</div>';
                    }
					$var.= '</div>';
                    ++$i;
                }

if ($rights == 3 || $rights >= 6) {
$var.= '<div class="rmenu"><input type="submit" value=" ' . $lng['delete'] . ' "/></div>';
$var.= '</form>';
}
// Нижнее поле "Написат
$var.= '<br /><div class="mainblok">';
if($user_id){
$var.= '<div class="phdr"><b>Đăng bài viết</b></div><div class="gmenu"><form name="form2"
action="index.php?act=say&amp;id=' . $id . '" method="post">';
$var.= '<p>';
if (!$is_mobile)
$var.= bbcode::auto_bb('form2', 'msg');
$var.= '<textarea rows="' . $set_user['field_h'] . '"
name="msg"></textarea><br/></p>' 

;
$var.= '<br /><input type="checkbox" name="msgtrans" value="1" />
Telex (aa => â)';
$var.= '</p><p><input type="submit" name="submit" value="' .
$lng['write'] . '" style="width: 107px; cursor: pointer;"/> ' .
($set_forum['preview'] ? '<input type="submit" value="' .
$lng['preview'] . '" style="width: 107px; cursor: pointer;"/>' : '') .
'</p></form></div>';
} else {
$var.= '<div class="gmenu">Cần phải <a href="/login.php"><b>Đăng
Nhập</b></a> để được phép bình luận</form></div>';
}
$var.= '<div class="phdr"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td width="auto"><a name="down" id="down"></a><a href="#up">' .
'<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/up.png" alt="' . $lng['up'] . '" width="20" height="10" border="0"/></a><b>' .
'&#160;&#160;' . $lng['total'] . ': ' . $colmes . '</b></td><td align="right" width="auto"><b>Xem: '.$type1['view'].' lượt</b></td></tr></tbody></table></div>';
if ($colmes > $kmess) {
$var.= '<div class="topmenu">' . functions::display_pagination2(''.$home.'/forum/'.functions::mikdaik($type1["text"]).'_' . $id, $start, $colmes, $kmess) . '</div>' .
'<p><form action="index.php?id=' . $id . '" method="post">' .
'<input type="text" name="page" size="2"/>' .
'<input type="submit" value="' . $lng['to_page'] . ' &gt;&gt;"/>' .
'</form></p>';
} else {
}
$var.= '</div>';
                /*
                -----------------------------------------------------------------
                Chia se bv
                -----------------------------------------------------------------
                */


// Chia sẻ

function selfURL()
{
$s = empty($_SERVER["HTTPS"]) ? ''
: ($_SERVER["HTTPS"] == "on") ? "s"
: "";
$protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s;
$port = ($_SERVER["SERVER_PORT"] == "80") ? ""
: (":".$_SERVER["SERVER_PORT"]);
return $protocol."://".$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI'];
}
function strleft($s1, $s2)
{
return substr($s1, 0, strpos($s1, $s2));
}

function facebook()
{     
	  $l = selfURL();
	  
	  $social = "<a href=\"http://www.facebook.com/share.php?u=$l\"/><img src=\"http://giacmovn.com/icon/facebook.png\" align =\"middle\" border =\"0\" width =\"20\" height =\"20\" alt =\"facebook\" /></a>";
      return $social;
}

function zingme()
{     
	  $l = selfURL();
	  
	  $social = "<a href=\"http://link.apps.zing.vn/share?url=$l\"/><img src=\"http://img.forum.zing.vn/images/bluestyle/misc/bookmark_icons/zingme.png\" align =\"middle\" border =\"0\" width =\"20\" height =\"20\" alt =\"delicious\" /></a>";
      return $social;
}
$var.= '<div class="rmenu">chia sẻ bài viết này:'.facebook().'
'.zingme().'</br>

<b>BB Code</b>:</br>
<input type="text" size="17" value="anh em xem bài viết này:'.$type1["text"].', cảm ơn "/></br>
<b>Link:</b></br>
<input type="text" size="17" value="'.$home.'/forum/index.php?id='.$id.'"/></div></br>';//het

/*
-----------------------------------------------------------------
Ссылки на модераторские функции
-----------------------------------------------------------------
*/
if ($curators) {
$array = array();
foreach ($curators as $key => $value)
$array[] = '<a href="../users/profile.php?user=' . $key . '">' . $value . '</a>';
$var.= '<p><div class="func">' . $lng_forum['curators'] . ': ' . implode(', ', $array) . '</div></p>';
}
$req = mysql_query("SELECT * FROM `forum` WHERE `website` = '$website' AND `type`='t' AND `refid`='$type1[refid]' AND `id`!='$id' ORDER BY `vip` DESC, `time` DESC LIMIT 5");
$total = mysql_num_rows($req);
if($total!=0)
{
$var.= '<div class="mainblok"><div class="phdrblack"><b>Chủ đề có liên quan</b></div>';
while ($res = mysql_fetch_assoc($req)) {
$var.= '<div class="menub">+ <a href="'.$home.'/forum/' . functions::mikdaik($res['text']) . '_' . $res['id'] . '.html">' . bbcode::tags($res['text']) . '</a></div>';
++$i;
}

$var.='</div>';
}
if ($rights == 3 || $rights >= 6) {
$var.= '<p><div class="func">';
if ($rights == 9){
if(empty($type1['portal'])) $var.= '<a href="index.php?act=portal&amp;id=' . $id . '&amp;portal" style="color:red;">Thêm tin tức Hot</a><br/>';else $var.= '<a href="index.php?act=portal&amp;id=' . $id . '&amp;khoiphuc" style="color:red;">Bỏ tin tức Hot</a><br/>';
}
$var.= '<a href="index.php?act=curators&amp;id=' . $id . '&amp;start=' . $start . '">' . $lng_forum['curators_of_the_topic'] . '</a><br />';
if(!$type1['kiemduyet']) {
$var.= "<a href='index.php?act=kiemduyet&amp;id=" . $id . "&amp;kiemduyet'>Kiểm duyệt</a> | <a href='index.php?act=deltema&amp;id=" . $id . "'>Loại bỏ</a><br/>";
}
$var.= isset($topic_vote) && $topic_vote > 0
? '<a href="index.php?act=editvote&amp;id=' . $id . '">' . $lng_forum['edit_vote'] . '</a><br/><a href="index.php?act=delvote&amp;id=' . $id . '">' . $lng_forum['delete_vote'] . '</a><br/>'
: '<a href="index.php?act=addvote&amp;id=' . $id . '">' . $lng_forum['add_vote'] . '</a><br/>';
$var.= '<a href="index.php?act=ren&amp;id=' . $id . '">' . $lng_forum['topic_rename'] . '</a><br/>';
// Закрыть - открыть тему
if ($type1['edit'] == 1)
$var.= '<a href="index.php?act=close&amp;id=' . $id . '">' . $lng_forum['topic_open'] . '</a><br/>';
else
$var.= '<a href="index.php?act=close&amp;id=' . $id . '&amp;closed">' . $lng_forum['topic_close'] . '</a><br/>';
// Удалить - восстановить тему
if ($type1['close'] == 1)
$var.= '<a href="index.php?act=restore&amp;id=' . $id . '">' . $lng_forum['topic_restore'] . '</a><br/>';
$var.= '<a href="index.php?act=deltema&amp;id=' . $id . '">' . $lng_forum['topic_delete'] . '</a><br/>';
if ($type1['vip'] == 1)
$var.= '<a href="index.php?act=vip&amp;id=' . $id . '">' . $lng_forum['topic_unfix'] . '</a>';
else
$var.= '<a href="index.php?act=vip&amp;id=' . $id . '&amp;vip">' . $lng_forum['topic_fix'] . '</a>';
$var.= '<br/><a href="index.php?act=per&amp;id=' . $id . '">' . $lng_forum['topic_move'] . '</a></div></p>';
}

if ($filter)
$var.= '<div class="list1"><a href="index.php?act=filter&amp;id=' . $id . '&amp;do=unset">' . $lng_forum['filter_cancel'] . '</a></div>';
else
$var.= '<div class="phdrblack"><a href="index.php?act=filter&amp;id=' . $id . '&amp;start=' . $start . '">' . $lng_forum['filter_on_author'] . '</a> |';
$var.= ' <a href="index.php?act=tema&amp;id=' . $id . '">' . $lng_forum['download_topic'] . '</a></div>';

return $var;
}



















//////////////////////////////

/*
-----------------------------------------------------------------
Проверка переменных
-----------------------------------------------------------------
*/
public static function check($str)
{
$str = htmlentities(trim($str), ENT_QUOTES, 'UTF-8');
$str = nl2br($str);
$str = strtr($str, array(
chr(0) => '',
chr(1) => '',
chr(2) => '',
chr(3) => '',
chr(4) => '',
chr(5) => '',
chr(6) => '',
chr(7) => '',
chr(8) => '',
chr(9) => '',
chr(10) => '',
chr(11) => '',
chr(12) => '',
chr(13) => '',
chr(14) => '',
chr(15) => '',
chr(16) => '',
chr(17) => '',
chr(18) => '',
chr(19) => '',
chr(20) => '',
chr(21) => '',
chr(22) => '',
chr(23) => '',
chr(24) => '',
chr(25) => '',
chr(26) => '',
chr(27) => '',
chr(28) => '',
chr(29) => '',
chr(30) => '',
chr(31) => ''
));
$str = str_replace("'", "&#39;", $str);
$str = str_replace('\\', "&#92;", $str);
$str = str_replace("|", "I", $str);
$str = str_replace("||", "I", $str);
$str = str_replace("/\\\$/", "&#36;", $str);
$str = mysql_real_escape_string($str);
return $str;
}

/*
-----------------------------------------------------------------
Обработка текстов перед выводом на экран
-----------------------------------------------------------------
$br=1           обработка переносов строк
$br=2           подстановка пробела, вместо переноса
$tags=1         обработка тэгов
$tags=2         вырезание тэгов
-----------------------------------------------------------------
*/
public static function checkout($str, $br = 0, $tags = 0)
{
$str = htmlentities(trim($str), ENT_QUOTES, 'UTF-8');
if ($br == 1)
$str = nl2br($str);
elseif ($br == 2)
$str = str_replace("\r\n", ' ', $str);
if ($tags == 1)
$str = tags(url($str));
elseif ($tags == 2)
$str = notags($str);
$replace = array(
chr(0) => '',
chr(1) => '',
chr(2) => '',
chr(3) => '',
chr(4) => '',
chr(5) => '',
chr(6) => '',
chr(7) => '',
chr(8) => '',
chr(9) => '',
chr(11) => '',
chr(12) => '',
chr(13) => '',
chr(14) => '',
chr(15) => '',
chr(16) => '',
chr(17) => '',
chr(18) => '',
chr(19) => '',
chr(20) => '',
chr(21) => '',
chr(22) => '',
chr(23) => '',
chr(24) => '',
chr(25) => '',
chr(26) => '',
chr(27) => '',
chr(28) => '',
chr(29) => '',
chr(30) => '',
chr(31) => ''
);
return strtr($str, $replace);
}

/*
-----------------------------------------------------------------
Показ различных счетчиков внизу страницы
-----------------------------------------------------------------
*/
public static function display_counters()
{
global $headmod,$website;
$req = mysql_query("SELECT * FROM `cms_counters` where `website` = '$website' AND  `switch` = '1' ORDER BY `sort` ASC");
if (mysql_num_rows($req) > 0) {
while (($res = mysql_fetch_array($req)) !== false) {
$link1 = ($res['mode'] == 1 || $res['mode'] == 2) ? $res['link1'] : $res['link2'];
$link2 = $res['mode'] == 2 ? $res['link1'] : $res['link2'];
$count = ($headmod == 'mainpage') ? $link1 : $link2;
if (!empty($count))
echo $count;
}
}
}

/*
-----------------------------------------------------------------
Показываем дату с учетом сдвига времени
-----------------------------------------------------------------
*/
public static function display_date($var)
{
$shift = (self::$system_set['timeshift'] + self::$user_set['timeshift']) * 3600;

return date("d.m.Y / H:i", $var + $shift);
}

/*
-----------------------------------------------------------------
Сообщения об ошибках
-----------------------------------------------------------------
*/
public static function display_error($error = NULL, $link = NULL)
{
if (!empty($error)) {
return '<div class="rmenu"><p><b>' . self::$lng['error'] . '!</b><br />' .
(is_array($error) ? implode('<br />', $error) : $error) . '</p>' .
(!empty($link) ? '<p>' . $link . '</p>' : '') . '</div>';
} else {
return false;
}
}

/*
-----------------------------------------------------------------
Отображение различных меню
-----------------------------------------------------------------
$delimiter - разделитель между пунктами
$end_space - выводится в конце
-----------------------------------------------------------------
*/
public static function display_menu($val = array(), $delimiter = ' | ', $end_space = '')
{
return implode($delimiter, array_diff($val, array(''))) . $end_space;
}

/*
-----------------------------------------------------------------
Постраничная навигация
За основу взята аналогичная функция от форума SMF2.0
-----------------------------------------------------------------
*/
public static function display_pagination($base_url, $start, $max_value, $num_per_page)
{
$neighbors = 2;
if ($start >= $max_value)
$start = max(0, (int)$max_value - (((int)$max_value % (int)$num_per_page) == 0 ? $num_per_page : ((int)$max_value % (int)$num_per_page)));
else
$start = max(0, (int)$start - ((int)$start % (int)$num_per_page));
$base_link = '<a class="pagenav" href="' . strtr($base_url, array('%' => '%%')) . 'page=%d' . '">%s</a>';
$out[] = $start == 0 ? '' : sprintf($base_link, $start / $num_per_page, '&lt;&lt;');
if ($start > $num_per_page * $neighbors)
$out[] = sprintf($base_link, 1, '1');
if ($start > $num_per_page * ($neighbors + 1))
$out[] = '<span style="font-weight: bold;">...</span>';
for ($nCont = $neighbors; $nCont >= 1; $nCont--)
if ($start >= $num_per_page * $nCont) {
$tmpStart = $start - $num_per_page * $nCont;
$out[] = sprintf($base_link, $tmpStart / $num_per_page + 1, $tmpStart / $num_per_page + 1);
}
$out[] = '<span class="currentpage"><b>' . ($start / $num_per_page + 1) . '</b></span>';
$tmpMaxPages = (int)(($max_value - 1) / $num_per_page) * $num_per_page;
for ($nCont = 1; $nCont <= $neighbors; $nCont++)
if ($start + $num_per_page * $nCont <= $tmpMaxPages) {
$tmpStart = $start + $num_per_page * $nCont;
$out[] = sprintf($base_link, $tmpStart / $num_per_page + 1, $tmpStart / $num_per_page + 1);
}
if ($start + $num_per_page * ($neighbors + 1) < $tmpMaxPages)
$out[] = '<span style="font-weight: bold;">...</span>';
if ($start + $num_per_page * $neighbors < $tmpMaxPages)
$out[] = sprintf($base_link, $tmpMaxPages / $num_per_page + 1, $tmpMaxPages / $num_per_page + 1);
if ($start + $num_per_page < $max_value) {
$display_page = ($start + $num_per_page) > $max_value ? $max_value : ($start / $num_per_page + 2);
$out[] = sprintf($base_link, $display_page, '&gt;&gt;');
}
return implode(' ', $out);
}

/*
-----------------------------------------------------------------
Показываем местоположение пользователя
-----------------------------------------------------------------
*/
public static function display_place($user_id = '', $place = '')
{
global $headmod;
$place = explode(",", $place);
$placelist = parent::load_lng('places');
if (array_key_exists($place[0], $placelist)) {
if ($place[0] == 'profile') {
if ($place[1] == $user_id) {
return '<a href="' . self::$system_set['homeurl'] . '/users/profile.php?user=' . $place[1] . '">' . $placelist['profile_personal'] . '</a>';
} else {
$user = self::get_user($place[1]);
return $placelist['profile'] . ': <a href="' . self::$system_set['homeurl'] . '/users/profile.php?user=' . $user['id'] . '">' . $user['name'] . '</a>';
}
}
elseif ($place[0] == 'online' && isset($headmod) && $headmod == 'online') return $placelist['here'];
else return str_replace('#home#', self::$system_set['homeurl'], $placelist[$place[0]]);
}
else return '<a href="' . self::$system_set['homeurl'] . '/index.php">' . $placelist['homepage'] . '</a>';
}

/*
-----------------------------------------------------------------
Отображения личных данных пользователя
-----------------------------------------------------------------
$user          (array)     массив запроса в таблицу `users`
$arg           (array)     Массив параметров отображения
[lastvisit] (boolean)   Дата и время последнего визита
[stshide]   (boolean)   Скрыть статус (если есть)
[iphide]    (boolean)   Скрыть (не показывать) IP и UserAgent
[iphist]    (boolean)   Показывать ссылку на историю IP

[header]    (string)    Текст в строке после Ника пользователя
[body]      (string)    Основной текст, под ником пользователя
[sub]       (string)    Строка выводится вверху области "sub"
[footer]    (string)    Строка выводится внизу области "sub"
-----------------------------------------------------------------
*/
public static function display_user2($user = false, $arg = false)
{

global $rootpath, $mod,$website;
$out = false;

if (!$user['id']) {
$out = '<b>' . self::$lng['guest'] . '</b>';
if (!empty($user['name']))
$out .= ': ' . $user['name'];
if (!empty($arg['header']))
$out .= ' ' . $arg['header'];
} else {

if ($user['sex'])
$out .= '<div class="newsx" style="border-bottom:2px solid #cecece"><img src="http://img.infoviet.net/theme/' . self::$user_set['skin'] . '/images/' . ($user['sex'] == 'm' ? 'm' : 'w') . ($user['datereg'] > time() - 86400 ? '_new' : '')
. '.png" width="16" height="16" align="middle" alt="' . ($user['sex'] == 'm' ? 'М' : 'Ж') . '" />&#160;';
else
$out .= '<img src="http://img.infoviet.net/images/del.png" width="12" height="12" align="middle" />&#160;';
if ($user['rights'] == 0 ) {
$colornick['colornick'] = '4e387e';
$colornickk['colornick'] = '4e387e';
}
if ($user['rights'] == 1 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 2 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 3 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 4 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 5 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 6 ) {
$colornick['colornick'] = '43d6d8';
$colornickk['colornick'] = '43d6d8';
}
if ($user['rights'] == 7 ) {
$colornick['colornick'] = 'ff0000';
$colornickk['colornick'] = 'ff0000';
}
if ($user['rights'] == 9 ) {
$colornick['colornick'] = 'ff00ff';
$colornickk['colornick'] = 'ff00ff';
}
if ($user['rights'] == 10 ) {
$colornick['colornick'] = 'ff0000';
$colornickk['colornick'] = 'ff0000';
}
if ($user['rights'] == 10 ) {
$out .= !$user_id || $user_id == $user['id'] ? '<a
href="../users/profile.php?user=' . $user['id'] . '"><span style="color:
rgb(255, 0, 0); background: url(/images/6.gif) repeat scroll 0% 0%
transparent;" border="0"><b>' . $user['name'] . '
</b></span></a>' : '<a href="../users/profile.php?user=' .
$user['id'] . '"><span style="color: rgb(255, 0, 0); background:
url(/images/6.gif) repeat scroll 0% 0% transparent;"
border="0"><b>' . $user['name'] . ' </b></span></a>';
}else {

$out .= !$user_id || $user_id == $user['id'] ? '<a
href="../users/profile.php?user=' . $user['id'] . '"><span
style="color:#' . $colornick['colornick'] . '"><b>' . $user['name'] . '
</b></span></a>' : '<a href="../users/profile.php?user=' .
$user['id'] . '"><span style="color:#' . $colornickk['colornick'] .
'"><b>' . $user['name'] . ' </b></span></a>';
}
$rank = array(
0 => '<font color="gray">(</font><font
color="black">Member</font><font color="gray">)</font>',
1 => '<font color="gray">(</font><font
color="#9900ff">Auto</font><font color="gray">)</font>',
3 => '<font color="gray">(</font><font
color="green">Mod</font><font color="gray">)</font>',
4 => '<font color="gray">(</font><font
color="green>DMod</font><font color="gray">)</font>',
6 => '<font color="gray">(</font><font
color="blue">Smod</font><font color="gray">)</font>',
7 => '<font color="gray">(</font><font
color="red">Admin</font><font color="gray">)</font>',
9 => '<font color="gray">(</font><font
color="red">Người Sáng Lập</font><font color="gray">)</font>',
10 => '<font color="gray">(</font><font
color="red">Máy Chém Tự Động</font><font
color="gray">)</font>'
);
$out .= ' ' . $rank[$user['rights']];
//    $out .= '<a href="/user/perevod.php?id=' .$user['id'].
'><font color="#FFA500"><small>' .$user['balans']. '
Xu</font></small></a>';
$out .= (time() > $user['lastdate'] + 300 ? '<font
color="red"> [Off]</font>' : '<font color="green"> [ON]</font>');
if (!empty($arg['header']))
$out .= ' ' . $arg['header'];
if (!isset($arg['stshide']) && !empty($user['status']))
$out .= '</div>';
if (self::$user_set['avatar'])
$out .= '</td></tr></table>';
}
if (isset($arg['body']))
$out .= '<div>' . $arg['body'] . '</div>';
$ipinf = !isset($arg['iphide']) && (self::$user_rights || ($user['id'] && $user['id'] == self::$user_id)) ? 1 : 0;
$lastvisit = time() > $user['lastdate'] + 300 && isset($arg['lastvisit']) ? self::display_date($user['lastdate']) : false;
if ($ipinf || $lastvisit || isset($arg['sub']) && !empty($arg['sub']) || isset($arg['footer'])) {

}
return $out;
}

public static function display_user($user = false, $arg = false)
{

global $rootpath, $mod,$website;
$out = false;

if (!$user['id']) {
$out = '<b>' . self::$lng['guest'] . '</b>';
if (!empty($user['name']))
$out .= ': ' . $user['name'];
if (!empty($arg['header']))
$out .= ' ' . $arg['header'];
} else {
if (self::$user_set['avatar']) {
$out .= '<table cellpadding="0" cellspacing="0"><tr><td>';
if (file_exists(($rootpath . 'files/users/avatar/' . $user['id'] . '.png')))
$out .= '<img src="' . self::$system_set['homeurl'] . '/files/users/avatar/' . $user['id'] . '.png" width="32" height="32" alt="" />&#160;';
else
$out .= '<img src="http://img.infoviet.net/images/empty.png" width="32" height="32" alt="" />&#160;';
$out .= '</td><td>';
}
if ($user['sex'])
$out .= '<div class="newsx" style="border-bottom:2px solid #cecece"><img src="http://img.infoviet.net/theme/' . self::$user_set['skin'] . '/images/' . ($user['sex'] == 'm' ? 'm' : 'w') . ($user['datereg'] > time() - 86400 ? '_new' : '')
. '.png" width="16" height="16" align="middle" alt="' . ($user['sex'] == 'm' ? 'М' : 'Ж') . '" />&#160;';
else
$out .= '<img src="http://img.infoviet.net/images/del.png" width="12" height="12" align="middle" />&#160;';
if ($user['rights'] == 0 ) {
$colornick['colornick'] = '4e387e';
$colornickk['colornick'] = '4e387e';
}
if ($user['rights'] == 1 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 2 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 3 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 4 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 5 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 6 ) {
$colornick['colornick'] = '43d6d8';
$colornickk['colornick'] = '43d6d8';
}
if ($user['rights'] == 7 ) {
$colornick['colornick'] = 'ff0000';
$colornickk['colornick'] = 'ff0000';
}
if ($user['rights'] == 9 ) {
$colornick['colornick'] = 'ff00ff';
$colornickk['colornick'] = 'ff00ff';
}
if ($user['rights'] == 10 ) {
$colornick['colornick'] = 'ff0000';
$colornickk['colornick'] = 'ff0000';
}
if ($user['rights'] == 10 ) {
$out .= !$user_id || $user_id == $user['id'] ? '<a
href="../users/profile.php?user=' . $user['id'] . '"><span style="color:
rgb(255, 0, 0); background: url(/images/6.gif) repeat scroll 0% 0%
transparent;" border="0"><b>' . $user['name'] . '
</b></span></a>' : '<a href="../users/profile.php?user=' .
$user['id'] . '"><span style="color: rgb(255, 0, 0); background:
url(/images/6.gif) repeat scroll 0% 0% transparent;"
border="0"><b>' . $user['name'] . ' </b></span></a>';
}else {

$out .= !$user_id || $user_id == $user['id'] ? '<a
href="../users/profile.php?user=' . $user['id'] . '"><span
style="color:#' . $colornick['colornick'] . '"><b>' . $user['name'] . '
</b></span></a>' : '<a href="../users/profile.php?user=' .
$user['id'] . '"><span style="color:#' . $colornickk['colornick'] .
'"><b>' . $user['name'] . ' </b></span></a>';
}
$rank = array(
0 => '<font color="gray">(</font><font
color="black">Member</font><font color="gray">)</font>',
1 => '<font color="gray">(</font><font
color="#9900ff">Auto</font><font color="gray">)</font>',
3 => '<font color="gray">(</font><font
color="green">Mod</font><font color="gray">)</font>',
4 => '<font color="gray">(</font><font
color="green>DMod</font><font color="gray">)</font>',
6 => '<font color="gray">(</font><font
color="blue">Smod</font><font color="gray">)</font>',
7 => '<font color="gray">(</font><font
color="red">Admin</font><font color="gray">)</font>',
9 => '<font color="gray">(</font><font
color="red">Người Sáng Lập</font><font color="gray">)</font>',
10 => '<font color="gray">(</font><font
color="red">Máy Chém Tự Động</font><font
color="gray">)</font>'
);
$out .= ' ' . $rank[$user['rights']];
//    $out .= '<a href="/user/perevod.php?id=' .$user['id'].
'><font color="#FFA500"><small>' .$user['balans']. '
Xu</font></small></a>';
$out .= (time() > $user['lastdate'] + 300 ? '<font
color="red"> [Off]</font>' : '<font color="green"> [ON]</font>');
if (!empty($arg['header']))
$out .= ' ' . $arg['header'];
if (!isset($arg['stshide']) && !empty($user['status']))
$out .= '</div>';
if (self::$user_set['avatar'])
$out .= '</td></tr></table>';
}
if (isset($arg['body']))
$out .= '<div>' . $arg['body'] . '</div>';
$ipinf = !isset($arg['iphide']) && (self::$user_rights || ($user['id'] && $user['id'] == self::$user_id)) ? 1 : 0;
$lastvisit = time() > $user['lastdate'] + 300 && isset($arg['lastvisit']) ? self::display_date($user['lastdate']) : false;
if ($ipinf || $lastvisit || isset($arg['sub']) && !empty($arg['sub']) || isset($arg['footer'])) {
$out .= '<div class="sub">';
if (isset($arg['sub']))
$out .= '<div>' . $arg['sub'] . '</div>';
if ($lastvisit)
$out .= '<div><span class="gray">' . self::$lng['last_visit'] . ':</span> ' . $lastvisit . '</div>';
$iphist = '';
if ($ipinf) {
$out.='<div><span class="gray">' . self::$lng['ip_address'] . ':</span> ';
$hist = $mod == 'history' ? '&amp;mod=history' : '';
$ip = long2ip($user['ip']);
if (self::$user_rights && isset($user['ip_via_proxy']) && $user['ip_via_proxy']) {
$out .= '<b class="red"><a href="' . self::$system_set['homeurl'] . '/' . self::$system_set['admp'] . '/index.php?act=search_ip&amp;ip=' . $ip . $hist . '">' . $ip . '</a></b> / ';
$out .= '<a href="' . self::$system_set['homeurl'] . '/' . self::$system_set['admp'] . '/index.php?act=search_ip&amp;ip=' . long2ip($user['ip_via_proxy']) . $hist . '">' . long2ip($user['ip_via_proxy']) . '</a>';
} elseif (self::$user_rights) {
$out .= '<a href="' . self::$system_set['homeurl'] . '/' . self::$system_set['admp'] . '/index.php?act=search_ip&amp;ip=' . $ip . $hist . '">' . $ip . '</a>';
} else {
$out .= $ip . $iphist;
}
if (isset($arg['iphist'])) {
$iptotal = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_users_iphistory` where `website` = '$website' AND  `user_id` = '" . $user['id'] . "'"), 0);
$out .= '<div><span class="gray">' . self::$lng['ip_history'] . ':</span> <a href="' . self::$system_set['homeurl'] . '/users/profile.php?act=ip&amp;user=' . $user['id'] . '">[' . $iptotal . ']</a></div>';
}
$out .= '</div>';
}
if (isset($arg['footer']))
$out .= $arg['footer'];
$out .= '</div>';
}
return $out;
}

/*
-----------------------------------------------------------------
Форматирование имени файла
-----------------------------------------------------------------
*/
public static function format($name)
{
$f1 = strrpos($name, ".");
$f2 = substr($name, $f1 + 1, 999);
$fname = strtolower($f2);
return $fname;
}

/*
-----------------------------------------------------------------
Получаем данные пользователя
-----------------------------------------------------------------
*/
public static function get_user($id = false)
{
global $website;
if ($id && $id != self::$user_id) {
$req = mysql_query("SELECT * FROM `users` where `website` = '$website' AND  `id` = '$id'");
if (mysql_num_rows($req)) {
return mysql_fetch_assoc($req);
} else {
return false;
}
} else {
return self::$user_data;
}
}

/*
-----------------------------------------------------------------
Транслитерация с Русского в латиницу
-----------------------------------------------------------------
*/
public static function rus_lat($str)
{
$replace = array(
'а' => 'a',
'б' => 'b',
'в' => 'v',
'г' => 'g',
'д' => 'd',
'е' => 'e',
'ё' => 'e',
'ж' => 'j',
'з' => 'z',
'и' => 'i',
'й' => 'i',
'к' => 'k',
'л' => 'l',
'м' => 'm',
'н' => 'n',
'о' => 'o',
'п' => 'p',
'р' => 'r',
'с' => 's',
'т' => 't',
'у' => 'u',
'ф' => 'f',
'х' => 'h',
'ц' => 'c',
'ч' => 'ch',
'ш' => 'sh',
'щ' => 'sch',
'ъ' => "",
'ы' => 'y',
'ь' => "",
'э' => 'ye',
'ю' => 'yu',
'я' => 'ya'
);
return strtr($str, $replace);
}

/*
-----------------------------------------------------------------
Обработка смайлов
-----------------------------------------------------------------
*/
public static function smileys($str, $adm = false)
{
global $rootpath;
static $smileys_cache = array();
if (empty($smileys_cache)) {
$file = $rootpath . 'files/cache/smileys.dat';
if (file_exists($file) && ($smileys = file_get_contents($file)) !== false) {
$smileys_cache = unserialize($smileys);
return strtr($str, ($adm ? array_merge($smileys_cache['usr'], $smileys_cache['adm']) : $smileys_cache['usr']));
} else {
return $str;
}
} else {
return strtr($str, ($adm ? array_merge($smileys_cache['usr'], $smileys_cache['adm']) : $smileys_cache['usr']));
}
}

/*
-----------------------------------------------------------------
Функция пересчета на дни, или часы
-----------------------------------------------------------------
*/
public static function timecount($var)
{
global $lng;
if ($var < 0) $var = 0;
$day = ceil($var / 86400);
if ($var > 345600) return $day . ' ' . $lng['timecount_days'];
if ($var >= 172800) return $day . ' ' . $lng['timecount_days_r'];
if ($var >= 86400) return '1 ' . $lng['timecount_day'];
return date("G:i:s", mktime(0, 0, $var));
}

/*
-----------------------------------------------------------------
Транслитерация текста
-----------------------------------------------------------------
*/

/*
-----------------------------------------------------------------
Транслитерация текста
-----------------------------------------------------------------
*/
public static function mikdaik($text)
{
$text = html_entity_decode(trim($text), ENT_QUOTES, 'UTF-8');
$text=str_replace(" ","-", $text);$text=str_replace("--","-", $text);
$text=str_replace("@","-",$text);$text=str_replace("/","-",$text);
$text=str_replace("\\","-",$text);$text=str_replace(":","",$text);
$text=str_replace("\"","",$text);$text=str_replace("'","",$text);
$text=str_replace("<","",$text);$text=str_replace(">","",$text);
$text=str_replace(",","",$text);$text=str_replace("?","",$text);
$text=str_replace(";","",$text);$text=str_replace(".","",$text);
$text=str_replace("[","",$text);$text=str_replace("]","",$text);
$text=str_replace("(","",$text);$text=str_replace(")","",$text);
$text=str_replace("́","", $text);
$text=str_replace("̀","", $text);
$text=str_replace("̃","", $text);
$text=str_replace("̣","", $text);
$text=str_replace("̉","", $text);
$text=str_replace("*","",$text);$text=str_replace("!","",$text);
$text=str_replace("$","-",$text);$text=str_replace("&","-and-",$text);
$text=str_replace("%","",$text);$text=str_replace("#","",$text);
$text=str_replace("^","",$text);$text=str_replace("=","",$text);
$text=str_replace("+","",$text);$text=str_replace("~","",$text);
$text=str_replace("`","",$text);$text=str_replace("--","-",$text);
$text = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $text);
$text = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $text);
$text = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $text);
$text = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $text);
$text = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $text);
$text = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $text);
$text = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $text);
$text = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $text);
$text = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $text);
$text = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $text);
$text = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $text);
$text = preg_replace("/(đ)/", 'd', $text);
$text = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $text);
$text = preg_replace("/(đ)/", 'd', $text);
$text = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $text);
$text = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $text);
$text = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $text);
$text = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $text);
$text = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $text);
$text = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $text);
$text = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $text);
$text = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $text);
$text = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $text);
$text = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $text);
$text = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $text);
$text = preg_replace("/(Đ)/", 'D', $text);
$text = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $text);
$text = preg_replace("/(Đ)/", 'D', $text);
$text=strtolower($text);
return $text;
}
////////////////////////
public static function display_pagination2($base_url, $start, $max_value, $num_per_page)
{
$neighbors = 2;
if ($start >= $max_value)
$start = max(0, (int)$max_value - (((int)$max_value % (int)$num_per_page) == 0 ? $num_per_page : ((int)$max_value % (int)$num_per_page)));
else
$start = max(0, (int)$start - ((int)$start % (int)$num_per_page));
$base_link = '<a class="pagenav" href="' . strtr($base_url, array('%' => '%%')) . '_p%d.html' . '">%s</a>';
$out[] = $start == 0 ? '' : sprintf($base_link, $start / $num_per_page, '&lt;&lt;');
if ($start > $num_per_page * $neighbors)
$out[] = sprintf($base_link, 1, '1');
if ($start > $num_per_page * ($neighbors + 1))
$out[] = '<span style="font-weight: bold;">...</span>';
for ($nCont = $neighbors; $nCont >= 1; $nCont--)
if ($start >= $num_per_page * $nCont) {
$tmpStart = $start - $num_per_page * $nCont;
$out[] = sprintf($base_link, $tmpStart / $num_per_page + 1, $tmpStart / $num_per_page + 1);
}
$out[] = '<span class="currentpage"><b>' . ($start / $num_per_page + 1) . '</b></span>';
$tmpMaxPages = (int)(($max_value - 1) / $num_per_page) * $num_per_page;
for ($nCont = 1; $nCont <= $neighbors; $nCont++)
if ($start + $num_per_page * $nCont <= $tmpMaxPages) {
$tmpStart = $start + $num_per_page * $nCont;
$out[] = sprintf($base_link, $tmpStart / $num_per_page + 1, $tmpStart / $num_per_page + 1);
}
if ($start + $num_per_page * ($neighbors + 1) < $tmpMaxPages)
$out[] = '<span style="font-weight: bold;">...</span>';
if ($start + $num_per_page * $neighbors < $tmpMaxPages)
$out[] = sprintf($base_link, $tmpMaxPages / $num_per_page + 1, $tmpMaxPages / $num_per_page + 1);
if ($start + $num_per_page < $max_value) {
$display_page = ($start + $num_per_page) > $max_value ? $max_value : ($start / $num_per_page + 2);
$out[] = sprintf($base_link, $display_page, '&gt;&gt;');
}
return implode(' ', $out);
}
public static function trans($str)
{
$replace = array(
"A"=>"A","a"=>"a",
"B"=>"B","b"=>"b",
"C"=>"C","c"=>"c",
"D"=>"D","d"=>"d",
"E"=>"E","e"=>"e",
"F"=>"F","f"=>"f",
"G"=>"G","g"=>"g",
"H"=>"H","h"=>"h",
"I"=>"I","i"=>"i",
"J"=>"J","j"=>"j",
"K"=>"K","k"=>"k",
"L"=>"L","l"=>"l",
"M"=>"M","m"=>"m",
"N"=>"N","n"=>"n",
"O"=>"O","o"=>"o",
"P"=>"P","p"=>"p",
"R"=>"R","r"=>"r",
"S"=>"S","s"=>"s",
"T"=>"T","t"=>"t",
"U"=>"U","u"=>"u",
"V"=>"V","v"=>"v",
"W"=>"W","w"=>"w",
"Y"=>"Y","y"=>"y",
"Z"=>"Z","z"=>"z",
"As"=>"Á","Ax"=>"Ã","Aj"=>"Ạ","Af"=>"À","Ar"=>"Ả",
"Es"=>"É","Ex"=>"Ẽ","Ej"=>"Ẹ","Ef"=>"È","Er"=>"Ẻ",
"Ys"=>"Ý","Yx"=>"Ỹ","Yj"=>"Ỵ","Yf"=>"Ỳ","Yr"=>"Ỷ",
"Us"=>"Ú","Ux"=>"Ũ","Uj"=>"Ụ","Uf"=>"Ù","Ur"=>"Ủ",
"Os"=>"Ó","Ox"=>"Õ","Oj"=>"Ọ","Of"=>"Ò","Or"=>"Ỏ",
"Is"=>"Í","Ix"=>"Ĩ","Ij"=>"Ị","If"=>"Ì","Ir"=>"Ỉ",
"Aas"=>"Ấ","Aax"=>"Ẫ","Aaj"=>"Ậ","Aaf"=>"Ầ","Aar"=>"Ẩ",
"Ees"=>"Ế","Eex"=>"Ễ","Eej"=>"Ệ","Eef"=>"Ề","Eer"=>"Ể",
"Oos"=>"Ố","Oox"=>"Ỗ","Ooj"=>"Ộ","Oof"=>"Ồ","Oor"=>"Ổ",
"Ows"=>"Ớ","Owx"=>"Ớ","Owj"=>"Ợ","Owf"=>"Ờ","Owr"=>"Ở",
"Aws"=>"Ẵ","Awx"=>"Ẵ","Awj"=>"Ặ","Awf"=>"Ằ","Awr"=>"Ẳ",
"Uws"=>"Ứ","Uwx"=>"Ữ","Uwj"=>"Ự","Uwf"=>"Ừ","Uwr"=>"Ử",
"as"=>"á","ax"=>"ã","aj"=>"ạ","af"=>"à","ar"=>"ả",
"es"=>"é","ex"=>"ẽ","ej"=>"ẹ","ef"=>"è","er"=>"ẻ",
"ys"=>"ý","yx"=>"ỹ","yj"=>"ỵ","yf"=>"ỳ","yr"=>"ỷ",
"us"=>"ú","ux"=>"ũ","uj"=>"ụ","uf"=>"ù","ur"=>"ủ",
"os"=>"ó","ox"=>"õ","oj"=>"ọ","of"=>"ò","or"=>"ỏ",
"is"=>"í","ix"=>"ĩ","ij"=>"ị","if"=>"ì","ir"=>"ỉ",
"aas"=>"ấ","aax"=>"ẫ","aaj"=>"ậ","aaf"=>"ầ","aar"=>"ẩ",
"ees"=>"ế","eex"=>"ễ","eej"=>"ệ","eef"=>"ề","eer"=>"ể",
"oos"=>"ố","oox"=>"ỗ","ooj"=>"ộ","oof"=>"ồ","oor"=>"ổ",
"ees"=>"ế","eex"=>"ễ","eej"=>"ệ","eef"=>"ề","eer"=>"ể",
"ows"=>"ớ","owx"=>"ớ","owj"=>"ợ","owf"=>"ờ","owr"=>"ở",
"aws"=>"ắ","awx"=>"ẵ","awj"=>"ặ","awf"=>"ằ","awr"=>"ẳ",
"uws"=>"ứ","uwx"=>"ữ","uwj"=>"ự","uwf"=>"ừ","uwr"=>"ử",
"uw"=>"ư","aw"=>"ă","aa"=>"â","oo"=>"ô", "ee"=>"ê",
"ow"=>"ơ", "dd"=>"đ","uw"=>"ư",
"lon"=>"l*n", "dit"=>"d*t", "dyt"=>"d*t",
"djt"=>"d*t", "địt"=>"đ*t", "lồn"=>"l*n", "dcm"=>"đập con
muỗi", "dkm"=>"đập con muỗi", "fuck"=>"fuc*"
);
return strtr($str, $replace);
}
    public static function display_htrang($user = false, $arg = false)
    {
        global $rootpath, $mod;
        $out = false;
            $out .= !self::$user_id || self::$user_id == $user['id'] ? '<b>' . $user['name'] . '</b>' : '<a href="' . self::$system_set['homeurl'] . '/users/profile.php?user=' . $user['id'] . '"><b>' . $user['name'] . '</b></a>';
        return $out;
    }
}
?>
