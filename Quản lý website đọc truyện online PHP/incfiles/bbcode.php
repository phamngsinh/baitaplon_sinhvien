<?php
/**
* @package     JohnCMS
* @link        http://johncms.com
* @copyright   Copyright (C) 2008-2011 JohnCMS Community
* @license     LICENSE.txt (see attached file)
* @version     VERSION.txt (see attached file)
* @author      http://johncms.com/about
*/

defined('_IN_JOHNCMS') or die('Error: restricted access');

function tags($var = '') {
  global $user_id;
  global $login;
  $var = preg_replace(array ('#\[php\](.+?)\[\/php\]#se'), array ("''.highlight('$1').''"), str_replace("]\n", "]", $var));
  $var = preg_replace('#@([\w\d]{2,})#si', '@<a href="../@$1">$1</a>', $var);
  $var = preg_replace_callback('#\[hide\](.*?)\[/hide\]#si', 'bbHide', $var);
  $var = preg_replace('#\[hr\]#si', '<hr style="margin:2px;border:0;border-bottom:1px dotted #aaa"/>', $var);
  $var = preg_replace_callback('#\[code\](.*?)\[/code\]#si', 'showcode', $var);
//  $var = preg_replace('#\[code\](.*?)\[/code\]#si', '<div class="phdr">Mã</div><div class="gmenu">\1</div>', $var);
  $var = preg_replace('#\[text\](.*?)\[/text\]#si', 'TEXT:<br><input type="text" value"\1"><br>', $var);
  $var = preg_replace('#\[vblue\](.*?)\[/vblue\]#si', '<span style="text-shadow: 1px 3px 9px blue;">\1</span>', $var);
  $var = preg_replace('#\[vgreen\](.*?)\[/vgreen\]#si', '<span style="text-shadow: 1px 3px 9px green;">\1</span>', $var);
  $var = preg_replace('#\[vred\](.*?)\[/vred\]#si', '<span style="text-shadow: 1px 3px 9px red;">\1</span>', $var);
  $var = preg_replace('#\[b\](.*?)\[/b\]#si', '<span style="font-weight: bold;">\1</span>', $var);
  $var = str_replace('[br]', '<br>', $var);
  $var = preg_replace('#\[i\](.*?)\[/i\]#si', '<span style="font-style:italic;">\1</span>', $var);
  $var = preg_replace('#\[u\](.*?)\[/u\]#si', '<span style="text-decoration:underline;">\1</span>', $var);
  $var = preg_replace('#\[s\](.*?)\[/s\]#si', '<span style="text-decoration: line-through;">\1</span>', $var);
  $var = preg_replace('#\[center\](.+?)\[/center\]#is', '<div align="center">\1</div>', $var );
  $var = preg_replace('#\[CENTER\](.+?)\[/CENTER\]#is', '<div align="center">\1</div>', $var );
  $var = preg_replace('#\[LEFT\](.+?)\[/LEFT\]#is', '<div align="left">\1</div>', $var );
  $var = preg_replace('#\[left\](.+?)\[/left\]#is', '<div align="left">\1</div>', $var );
  $var = preg_replace('#\[right\](.+?)\[/right\]#is', '<div align="right">\1</div>', $var );
  $var = preg_replace('#\[RIGHT\](.+?)\[/RIGHT\]#is', '<div align="right">\1</div>', $var );
  $var = preg_replace('#\[red\](.*?)\[/red\]#si', '<span style="color:red">\1</span>', $var);
  $var = preg_replace('#\[orange\](.*?)\[/orange\]#si', '<font color="ffa500">\1</font>', $var);
  $var = preg_replace('#\[black\](.*?)\[/black\]#si', '<font color="000000">\1</font>', $var);
  $var = preg_replace('#\[green\](.*?)\[/green\]#si', '<span style="color:green">\1</span>', $var);
  $var = preg_replace('#\[blue\](.*?)\[/blue\]#si', '<span style="color:blue">\1</span>', $var);
  $var = preg_replace('#\[c\](.*?)\[/c\]#si', '<div class="bbcode_container"><div class="bbcode_quote"><div class="quote_container"><div class="bbcode_quote_container"></div>$1</div></div></div>', $var);
  $var = preg_replace('#\[quote=(.*?)\](.*?)\[/quote\]#si', '<div class="phdr">\1 đã viết</div><div class="gmenu">\2</div>', $var);
  $var = preg_replace('#\[img=(.+?)\]#is', '<a href="http://\1"><img src="http://\1" alt="click" border="0" width="110" onerror="this.src=/images/no.gif"/></a>', $var);
  $var = preg_replace('#\[img](.+?)\[/img]#is', '<a href="http://\1"><img src="http://\1" alt="click" border="0" width="110" onerror="this.src=/images/no.gif"/></a>', $var);
  $var = preg_replace('#\[url](.+?)\[/url]#is', '\1<br/>', $var);
  $var = preg_replace('#\[img=(.+?)\][/img]#is', '<a href="http://\1"><img src="http://\1" alt="click" border="0" width="110" onerror="this.src=/images/no.gif"/></a>', $var);
  $var = preg_replace('#\[COLOR=(.+?)\](.+?)\[/COLOR\]#is', '<font style="color:\1;">\2</font>', $var );
  $var = preg_replace('#\[color=(.+?)\](.+?)\[/color\]#is', '<font style="color:\1;">\2</font>', $var );
  $var = preg_replace('#\[SIZE=(.+?)\](.+?)\[/SIZE\]#is', '<font style="font-size:\1;">\2</font>', $var );
  $var = preg_replace('#\[size=(.+?)\](.+?)\[/size\]#is', '<font style="font-size:\1;">\2</font>', $var );
  $var = preg_replace('#\[FONT=(.+?)\](.+?)\[/FONT\]#is', '<font face="\1">\2</font>', $var );
  $var = preg_replace('#\[font=(.+?)\](.+?)\[/font\]#is', '<font face="\1">\2</font>', $var );
  $var = preg_replace("#\[url=(.+?)\](.+?)\[/url\]#is", "".("<a href=\"http://\\1\">\\2</a>")."", $var );
  $var = preg_replace("#\[URL=(.+?)\](.+?)\[/URL\]#is", "".("<a href=\"http://\\1\">\\2</a>")."", $var );
  $var = preg_replace("#\[spoiler=(?:&quot;|\"|')?(.*?)[\"']?(?:&quot;|\"|')?\](.*?)\[\/spoiler\](\r\n?|\n?)#si", "<div style=\"margin: 5px 5px 5px 5px;\"><div class=\"smallfont\" style=\"margin-bottom:2px\"><b>Spoiler</b> for <i>$1</i>: <input type=\"button\" style=\"margin: 0px; padding: 0px; width: 45px; font-size: 10px;\" onClick=\"if (this.parentNode.parentNode.getElementsByTagName('div')[1].getElementsByTagName('div')[0].style.display != '') { this.parentNode.parentNode.getElementsByTagName('div')[1].getElementsByTagName('div')[0].style.display = ''; this.innerText = ''; this.value = 'Hide'; } else { this.parentNode.parentNode.getElementsByTagName('div')[1].getElementsByTagName('div')[0].style.display = 'none'; this.innerText = ''; this.value = 'Show'; }\" value=\"Show\"></div><div style=\"border: 1px inset; background-color: whitesmoke; margin: 0px; padding: 2px;\"><div style=\"display: none;\">$2</div></div></div>", $var);
  $var = preg_replace("#\[spoiler\](.*?)\[\/spoiler\](\r\n?|\n?)#si", "<div style=\"margin: 5px 5px 5px 5px;\"><div class=\"smallfont\" style=\"margin-bottom:2px\"><b>Spoiler</b>: <input type=\"button\" style=\"margin: 0px; padding: 0px; width: 45px; font-size: 10px;\" onClick=\"if (this.parentNode.parentNode.getElementsByTagName('div')[1].getElementsByTagName('div')[0].style.display != '') { this.parentNode.parentNode.getElementsByTagName('div')[1].getElementsByTagName('div')[0].style.display = ''; this.innerText = ''; this.value = 'Hide'; } else { this.parentNode.parentNode.getElementsByTagName('div')[1].getElementsByTagName('div')[0].style.display = 'none'; this.innerText = ''; this.value = 'Show'; }\" value=\"Show\"></div><div style=\"border: 1px inset; background-color: whitesmoke; margin: 0px; padding: 2px;\"><div style=\"display: none;\">$1</div></div></div>", $var);
  $var = str_replace('[you]',($user_id ? $login : 'kháck'), $var);
  $var = preg_replace('#\[youtube](.+?)\[/youtube]#is', '<div class="gmenu"><center><iframe src="http://www.youtube.com/embed/\1" allowfullscreen="" frameborder="0" height="230" width="240"></iframe></center></div>', $var); 
$var = preg_replace('#\[YOUTUBE](.+?)\[/YOUTUBE]#is', '<div class="gmenu"><center><iframe src="http://www.youtube.com/embed/\1" allowfullscreen="" frameborder="0" height="230" width="240"></iframe></center></div>', $var);
 return $var;
}
/*
-----------------------------------------------------------------
Служебная функция подсветки PHP кода
-----------------------------------------------------------------
*/
function highlight($php) {
    $php = strtr($php, array (
        '<br />' => '',
        '\\' => 'slash_JOHNCMS'
    ));
    $php = html_entity_decode(trim($php), ENT_QUOTES, 'UTF-8');
    $php = substr($php, 0, 2) != "<?" ? $php = "<?php\n" . $php . "\n?>" : $php;
    $php = highlight_string(stripslashes($php), true);
    $php = strtr($php, array (
        'slash_JOHNCMS' => '&#92;',
        ':' => '&#58;',
        '[' => '&#91;',
        '&nbsp;' => ' '
    ));
    return '<div class="phpcode">' . $php . '</div>';
}

function showcode($box) {
    $codebox = str_replace('<br />',"\n",$box[1]);
    return '<div class="code"><textarea  name="codebox[]">'.$codebox.'</textarea></div>';
}

function bbHide($hide){
    global $user_id;
    if(!$user_id){
        return '<div style="background:#EAF1F4;border:1px solid #84BEE6;color:#2383A3;margin:2px;padding:2px">Bạn không phải thành viên nên không được xem nội dung</div>';
    }else{
        return '' . $hide[1] . '';
    }
}

/*
-----------------------------------------------------------------
Служебная функция парсинга URL
-----------------------------------------------------------------
*/
function url($var)
    {
        if (!function_exists('process_url')) {
            function process_url($url)
            {
                if (!isset($url[3])) {
                    $tmp = parse_url($url[1]);
                    if ('http://' . $tmp['host'] == core::$system_set['homeurl'] || isset(core::$user_set['direct_url']) && core::$user_set['direct_url']) {
                        return '<a href="' . $url[1] . '">' . $url[2] . '</a>';
                    } else {
                        return '<a href="' . core::$system_set['homeurl'] . '/go.php?url=' . base64_encode($url[1]) . '">' . $url[2] . '</a>';
                    }
                } else {
                    $tmp = parse_url($url[3]);
                    $url[3] = str_replace(':', '&#58;', $url[3]);
                    if ('http://' . $tmp['host'] == core::$system_set['homeurl'] || isset(core::$user_set['direct_url']) && core::$user_set['direct_url']) {
                        return '<a href="' . $url[3] . '">' . $url[3] . '</a>';
                    } else {
                        return '<a href="' . core::$system_set['homeurl'] . '/go.php?url=' . base64_encode($url[3]) . '">' . $url[3] . '</a>';
                    }
                }
            }
        }
$var = str_replace('[img=http://', '[img=', $var);
$var = str_replace('[url=http://', '[url=', $var);
$var = str_replace('[img]http://', '[img]', $var);
return preg_replace_callback('~\\[url=(https?://.+?)\\](.+?)\\[/url\\]|(https?://(www.)?[0-9a-z\.-]+\.[0-9a-z]{2,6}[0-9a-zA-Z/\?\.\~&amp;_=/%-:#]*)~', 'process_url', $var);
}
/*
-----------------
BBCode Notag
-----------------
*/
function notags($var = '') {
    $var = preg_replace('#\[color=(.+?)\](.+?)\[/color]#si', '$2', $var);
    $var = preg_replace('!\[bg=(#[0-9a-f]{3}|#[0-9a-f]{6}|[a-z\-]+)](.+?)\[/bg]!is', '$2', $var);
    $var = strtr($var, array (
        '[orange]' => '',
        '[/orange]' => '',
        '[black]' => '',
   '[/black]' => '',
   '[green]' => '',
   '[/green]' => '',
   '[/vgreen]' => '',
   '[/vred]' => '',
   '[/vblue]' => '',
   '[vgreen]' => '',
   '[vred]' => '',
   '[vblue]' => '',
   '[red]' => '',
   '[/red]' => '',
   '[blue]' => '',
   '[/blue]' => '',
   '[b]' => '',
   '[/b]' => '',
   '[i]' => '',
   '[/i]' => '',
   '[u]' => '',
   '[/u]' => '',
   '[s]' => '',
   '[/s]' => '',
   '[c]' => '',
   '[/c]' => '',
   '[br]' => ''
));

return $var;
}
?>