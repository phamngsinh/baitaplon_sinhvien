function Logout() {
    window.location.href = '?page=dangxuat';
}

function Profile() {
    window.location.href = '';
}

function EditProfile() {
    window.location.href = '/thay-doi-thong-tin';
}

function Register() {
    window.location.href = '?page=dangky';
}

function ChangeService(obj) {
    if (obj.value) {
        window.location.href = obj.value;
    }
}

function ChangeIcoinAmount(obj) {
  document.getElementById('exchangeAmount').innerHTML = obj.value;
}

function ChangeWinxuAmount(obj) {
  document.getElementById('exchangeAmount').innerHTML = obj.value * 8 / 10;
}

$(function(){
    var url = window.location.href;
    if (url.indexOf("trang-chu")>-1) {
        $('#trang-chu').attr("class", "active");
    } else {
        $('#span-trang-chu').attr("class", "nativ");
    }
    if (url.indexOf('nap-winxu')>-1) {
        $('#nap-winxu').attr("class", "active");
    } else {
        $('#span-nap-winxu').attr("class", "nativ");
    }
    if (url.indexOf('doi-winxu')>-1) {
        $('#doi-winxu').attr("class", "active");
    } else {
        $('#span-doi-winxu').attr("class", "nativ");
    }
    if (url.indexOf('ho-tro')>-1) {
        $('#ho-tro').attr("class", "active");
    } else {
        $('#span-ho-tro').attr("class", "nativ");
    }
    if (url.indexOf('lien-he')>-1) {
        $('#lien-he').attr("class", "active");
    } else {
        $('#span-lien-he').attr("class", "nativ");
    }
});

