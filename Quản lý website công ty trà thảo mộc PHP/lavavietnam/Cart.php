<?php
function themSP(cart,pid,soluong){
	if (cart.Exists(pid)){
		cart.Item(pid) = cart.Item(pid) + soluong;
	}
	else {
		cart.Add(pid,soluong);
	}
}

function xoaSP(cart,pid){
	if (cart.Exists(pid)){
		cart.Remove(pid);
	}
}

function capnhatSP(cart,pid,soluong){
	if (cart.Exists(pid)){
		if (soluong==0) {
			cart.Remove(pid);
		}
		else {
			cart.Item(pid)=soluong;
		}
	}
}

function xoatatSP(cart){
	cart.RemoveAll();
}
?>