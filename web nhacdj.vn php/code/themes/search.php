<?php
$page = $_REQUEST['p'];
if(preg_match("/\D/",$page)) {
		err_index("NUMBER FORMAT EXCEPTION!");
} else {
$limit=20;
if (empty($page)) {
    $offset=0;
}
else {
	$offset = ($page-1)*$limit;
}
require_once('ksecurity.php');
$security = new K_Security;
$kw = $_REQUEST["q"];
$kw = stripslashes(str_replace('-',' ',$kw));
$kw = $security->cleanXSS($kw);
?>	
<div class="left-column-cat">
            <div class="column-bit breadcrumbs">
<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" class="item">
    <a href="./" title="Nghe Nhac San, DJ Nonstop, Nhac DJ cuc manh, Nhac San cuc manh" itemprop="url">
    <span itemprop="title">Nhạc sàn</span>
    </a>
</div>
<h2 itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" class="item last-child">
    <span itemprop="title" title="Tìm kiếm">Tìm kiếm</span>
</h2>
</div>
<?if (empty($kw)){?>
<div class="column-bit" id="video-block">
<div class="column-title video-cat"><i class="icon-search"></i> Tìm kiếm</h2></div>
<div class="column-content">
<div class="alert error">Bạn cần nhập từ khóa tìm kiếm ! Hoặc bạn có thể thử tìm kiếm với kết quả của Google bên dưới !</div>
<style>
/**
 * Default theme
 *
 */
/* Slight reset to make the preview have ample padding. */
.cse .gsc-control-cse,
.gsc-control-cse {
  padding: 1em;
  width: auto;
}
.cse .gsc-control-wrapper-cse,
.gsc-control-wrapper-cse {
  width: 100%;
}
.cse .gsc-branding,
.gsc-branding {
  display: none;
}
.cse .gsc-control-cse div,
.gsc-control-cse div {
  position: normal;
}
/* Selector for entire element. */
.cse .gsc-control-cse,
.gsc-control-cse {
  background:url("../images/misc/bg.jpg") repeat;
  border: 1px solid #222;
  border-radius:2px;
}
.cse .gsc-control-cse:after,
.gsc-control-cse:after {
  content:".";
  display:block;
  height:0;
  clear:both;
  visibility:hidden;
}
.cse .gsc-resultsHeader,
.gsc-resultsHeader {
  border: block;
}
table.gsc-search-box td.gsc-input {
margin-top:-30px;
}
input.gsc-input {
  border-color: red;
  font-size: 13px;
	margin-top:-30px;
}
/* Search button */
.cse input.gsc-search-button,
input.gsc-search-button {
  font-family: inherit;

}
/* Inactive tab */
.cse .gsc-tabHeader.gsc-tabhInactive,
.gsc-tabHeader.gsc-tabhInactive {
  border-bottom: none;
  color: #666666;
  background-color: #e9e9e9;
  border: 1px solid;
  border-color: #e9e9e9;
  border-bottom: none;
}
/* Active tab */
.cse .gsc-tabHeader.gsc-tabhActive,
.gsc-tabHeader.gsc-tabhActive {
  background-color: #222;
  border: 1px solid;
  border-top: 2px solid;
  border-color: #e9e9e9;
  border-top-color: #FF9900;
  border-bottom: none;
}
/* This is the tab bar bottom border. */
.cse .gsc-tabsArea,
.gsc-tabsArea {
  margin-top: 1em;
  border-bottom: 1px solid #e9e9e9;
}
/* Inner wrapper for a result */
.cse .gsc-webResult.gsc-result,
.gsc-webResult.gsc-result,
.gsc-imageResult-column,
.gsc-imageResult-classic {
  padding: .25em;
  border: 1px solid;
  border-color: #222;
  margin-bottom: 1em;
}
/* Result hover event styling */
.cse .gsc-webResult.gsc-result:hover,
.gsc-webResult.gsc-result:hover,
.gsc-webResult.gsc-result.gsc-promotion:hover,
.gsc-results .gsc-imageResult-classic:hover,
.gsc-results .gsc-imageResult-column:hover {
  border: 1px solid;
  border-color: #FFFFFF;
}
/*Promotion Settings*/
/* The entire promo */
.cse .gsc-webResult.gsc-result.gsc-promotion,
.gsc-webResult.gsc-result.gsc-promotion {
  background-color: #222;
  border-color: #336699;
}
/* Promotion links */
.cse .gs-promotion a.gs-title:link,
.gs-promotion a.gs-title:link,
.cse .gs-promotion a.gs-title:link *,
.gs-promotion a.gs-title:link *,
.cse .gs-promotion .gs-snippet a:link,
.gs-promotion .gs-snippet a:link {
  color: #0000CC;
}
.cse .gs-promotion a.gs-title:visited,
.gs-promotion a.gs-title:visited,
.cse .gs-promotion a.gs-title:visited *,
.gs-promotion a.gs-title:visited *,
.cse .gs-promotion .gs-snippet a:visited,
.gs-promotion .gs-snippet a:visited {
  color: #0000CC;
}
.cse .gs-promotion a.gs-title:hover,
.gs-promotion a.gs-title:hover,
.cse .gs-promotion a.gs-title:hover *,
.gs-promotion a.gs-title:hover *,
.cse .gs-promotion .gs-snippet a:hover,
.gs-promotion .gs-snippet a:hover {
  color: #0000CC;
}
.cse .gs-promotion a.gs-title:active,
.gs-promotion a.gs-title:active,
.cse .gs-promotion a.gs-title:active *,
.gs-promotion a.gs-title:active *,
.cse .gs-promotion .gs-snippet a:active,
.gs-promotion .gs-snippet a:active {
  color: #0000CC;
}
/* Promotion snippet */
.cse .gs-promotion .gs-snippet,
.gs-promotion .gs-snippet,
.cse .gs-promotion .gs-title .gs-promotion-title-right,
.gs-promotion .gs-title .gs-promotion-title-right,
.cse .gs-promotion .gs-title .gs-promotion-title-right *,
.gs-promotion .gs-title .gs-promotion-title-right * {
  color: #000000;
}
/* Promotion url */
.cse .gs-promotion .gs-visibleUrl,
.gs-promotion .gs-visibleUrl {
  color: #008000;
}
/* Style for auto-completion table
 * .gsc-completion-selected : styling for a suggested query which the user has moused-over
 * .gsc-completion-container : styling for the table which contains the completions
 */
.gsc-completion-selected {
  background: #EEE;
}
.gsc-completion-container {
  font-family: Arial, sans-serif;
  font-size: 13px;
  background: white;
  border: 1px solid #CCC;
  border-top-color: #D9D9D9;
  margin-left: 0;
  margin-right: 0;
  /* The top, left, and width are set in JavaScript. */
}
.gsc-completion-title {
  color: #0000CC;
}
.gsc-completion-snippet {
  color: #000000;
}

/* Full URL */
.gs-webResult div.gs-visibleUrl-short,
.gs-promotion div.gs-visibleUrl-short {
  display: none;
}
.gs-webResult div.gs-visibleUrl-long,
.gs-promotion div.gs-visibleUrl-long {
  display: block;
}

.gsc-context-box {
  font-size: 83%;
  margin-top: 3px;
  border-collapse: collapse;
}

.gsc-context-box .gsc-col {
  padding:1px 0;
  white-space: nowrap;
  vertical-align: middle;
}

.gsc-context-box .gsc-facet-label {
  width: 65px;
  padding-left: 2px;
  text-decoration: underline;
  color: #15C;
  cursor: pointer;
}

.gsc-context-box .gsc-chart {
  width: 32em;
  padding: 3px;
  border-left: 1px solid #6A9CF3;
  border-right: 1px solid #6A9CF3;
}

.gsc-context-box .gsc-top {
  border-top: 1px solid #6A9CF3;
}

.gsc-context-box .gsc-bottom {
  border-bottom: 1px solid #6A9CF3;
}

.gsc-context-box .gsc-chart div {
  background: #6A9CF3;
  height: 9px;
}

.gsc-context-box .gsc-facet-result {
  color: #15C;
  width: 30px;
  text-align: right;
  padding-right: 5px;
}
</style>
<script>
  (function() {
    var cx = '011216670257953542017:6_fgepiqwyw';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search><center>Đang tải thanh công cụ tìm kiếm ...</center></gcse:search>

<br></br>
<div class="seperator-line1"></div>
<div class="seperator-line2"></div>
<?}else{?>
<div class="column-bit" id="video-block">
<div class="column-title video-cat"><i class="icon-music"></i> Nhạc DJ</h2></div>
<div class="column-content">
 <div id="list-video-cat">
 <div class="song-list">
<?
$querythumuc = "SELECT * FROM song AS a LEFT OUTER JOIN category AS b ON a.catID = b.catID LEFT OUTER JOIN singer AS c ON a.singerID = c.singerID WHERE a.sKw LIKE '%$kw%' AND sType!='youtube' AND sStatus != '0'";
$numresults = mysql_query ($querythumuc);
$numrows=mysql_num_rows($numresults);
$pages=intval($numrows/$limit);
if ($pages < ($numrows/$limit)){
$pages=($pages + 1);


	} else {
$query = "SELECT a.*,b.catID,b.catName,c.singerID,c.singerName,c.singerKw FROM song AS a LEFT OUTER JOIN category AS b ON a.catID = b.catID LEFT OUTER JOIN singer AS c ON a.singerID = c.singerID WHERE a.sKw LIKE '%$kw%' AND a.sType!='youtube' AND sStatus != '0' ORDER BY sKw limit $offset,$limit";
$result = mysql_query ($query);
$link_web="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; 
?>								
<?
$i = 0;
	while ($row=@mysql_fetch_array($result))
		{
			$i++;
			$sID = $row["sID"];
			$sName = $row["sName"];
			$sType = $row["sType"];
			$sView = $row["sView"];
			$sDownload = $row["sDownload"];
			$id_album = $row["albumID"];
			$catID = $row["catID"];
			$catName = $row["catName"];
            $ngay_dang = $row["m_date"];
			$date = date_create($ngay_dang); 
			$ngaydang = date_format($date, 'd/m/Y');
			$Member_id = $row["mID"];
			$singerID = $row["singerID"];
			$singerName = $row["singerName"];
			$id_cate = $row["catID"];;
			$id_user = $row["mID"];;
			$sStatus = $row["sStatus"];
			$hit = $row["sHot"];
			$new = $row["sNew"];	
			$mID = $row["mID"];

$query_cat = "SELECT * FROM category WHERE catID='$id_cate'";
$result_cat = mysql_query ($query_cat);
$row_cat=@mysql_fetch_array($result_cat);
$the_loai= $row_cat["catName"];

$query_singer = "SELECT * FROM singer WHERE singerID='$id_singer'";
$result_singer = mysql_query ($query_singer);
$row_singer=@mysql_fetch_array($result_singer);
$the_hien= $row_singer["singerName"];

$query_member = "SELECT * FROM member WHERE mID='$mID'";
$result_member = mysql_query ($query_member);
$row_member=@mysql_fetch_array($result_member);
$img = $row_member['link_avatar'];
$nguoidang = $row_member['mUsername'];
?>
<div class="song-bit">
<span class="song-icon"></span>
<div class="song-name"><h3><a href="<?php echo $server; ?><?php echo CovertVn($sName);?>-s<?php echo $sID; ?>.html" title="<?=$sName?>"><?=$sName?></a></h3></div>
<div class="no-display"></div>
<div class="add-to-playlist"><a href="javascript:void(0)" title="Thêm vào playlist nhạc" <?if($_SESSION['login'] == true){?> onclick="showAddPlaylist(<?=$sID?>)"<?} else {?> onclick="alert('Đăng nhập đê');"<?}?>></a></div>
<div class="song-listen"><?=$sView?> lượt</div>
</div>
<?}?>	
</div>
<div class="pagination">
<?
if( $pages > 1 ) {
printf("<a class='page' href=\"tim-kiem.html?q=$kw&p=1\">Đầu</a>");	
if( $page <= 5 ) {
$spage = 1;
if($pages > 8 ) {
$epage = 8;
} else {
$epage = $pages;
}
} else if ( $page <= $pages - 4 ) {
$spage = $page - 4;
$epage = $page + 4;
} else {
$spage = $pages - 8;
$epage = $pages;
}
if($spage < 1) $spage = 1;
for ($i = $spage; $i <= $epage; $i++) { 
$newoffset = $limit*($i-1); 	
if ($newoffset == $offset) { 
print " <a class='page active'>$i</a>"; 
} else {
print "<a class='page' href=\"tim-kiem.html?q=$kw&p=$i\">$i</a>"; 
}} 	
echo "<a class='page' href=\"tim-kiem.html?q=$kw&p=$epage\">Sau</a>";
}
?>
    </nav>
  </section> 
<!-- Ket thuc phan trang -->
<center>Tổng số <?=$numrows?> bài hát trong <?=$pages?> trang.</center>			
<?}?>
</div>
<div class="clear"></div>
</div>
</div>
</div>


<div class="column-bit" id="video-block">
<div class="column-title video-cat"><i class="icon-film"></i> Video</h2></div>
<div class="column-content">
 <div id="list-video-cat">

<?
$query_video_search = "SELECT a.*,c.singerID,c.singerName,c.singerKw FROM song AS a LEFT OUTER JOIN singer AS c ON a.singerID = c.singerID WHERE a.sKw LIKE '%$kw%' AND sType='youtube' AND sStatus != '0' ORDER BY sName limit 6";
$result_video_search = mysql_query ($query_video_search);
if(@mysql_num_rows($result_video_search)>0){
?>
 <div id="list-video-cat">
 <?
while ($row_video_search=@mysql_fetch_array($result_video_search))
{
$video = $row_video_search['sLink'];
$sID_video = $row_video_search["sID"];
$sName_video = $row_video_search["sName"];
$singerName_video = $row_video_search["singerName"];
?> 
<div class="video-bit">
<div class="video-thumb"><a href="<?php echo $server; ?><?php echo CovertVn($sName_video);?>-s<?php echo $sID; ?>.html" title="<?=$sName?>">
<img src="http://img.youtube.com/vi/<?=get_youtubeid($video)?>/mqdefault.jpg" alt="<?=$sName?>" /><div class="play-video-icon"></div></a></div>
<div class="video-title"><h3>
<a href="<?php echo $server; ?><?php echo CovertVn($sName);?>-s<?php echo $sID; ?>.html" title="<?=$sName?>"><?=mb_substr($sName_video, 0,60,'UTF-8')?></a></h3>
</div>
</div>
<?}?>
</div>
<?}else{?>
<h3 align='center'>Không tìm thấy Video nào !</h3>
<?}?>
</div>
<div class="clear"></div>
</div>
</div>
				
<div class="column-bit" id="video-block">
<div class="column-title video-cat"><i class="icon-book"></i> Album</h2></div>
<div class="column-content">
 <div id="list-video-cat">
<?
$query_album_search = "SELECT * FROM album  WHERE albumKw LIKE '%$kw%' ORDER BY albumName limit 6";
$result_album_search = mysql_query ($query_album_search);
if(@mysql_num_rows($result_album_search)>0){
?>
 <div id="list-video-cat">
<?
while ($row_album_search=@mysql_fetch_array($result_album_search)){
	$albumID_search = $row_album_search["albumID"];
	$albumName_search = $row_album_search["albumName"];
	$albumPic_search = $row_album_search["albumPicture"];

?>
<div class="video-bit">
<div class="video-thumb"><a href="<?php echo $server; ?>album/<?php echo $albumID_search; ?>/<?php echo CovertVn($albumName_search);?>.html" title="<?=$albumName_search?>">
<img src="<?=$albumPic_search?>" alt="<?=$albumName_search?>" /><div class="play-video-icon"></div></a></div>
<div class="video-title"><h3>
<a href="<?php echo $server; ?>album/<?php echo $albumID_search; ?>/<?php echo CovertVn($albumName_search);?>.html" title="<?=$albumName_search?>"><?=mb_substr($albumName_search, 0,60,'UTF-8')?></a></h3>
</div>
</div>
<?}?>
</div>
<?}else{?>
<h3 align='center'>Không tìm thấy Album nào !</h3>
<?}?>
</div>
<div class="clear"></div>
</div>
</div>
				
<div class="column-bit" id="video-block">
<div class="column-title video-cat"><i class="icon-user"></i> Thành viên</h2></div>
<div class="column-content">
<div id="list-video-cat">
<?
$query_member_search = "SELECT * FROM member  WHERE mUsername LIKE '%$kw%' OR mFullname LIKE '%$kw%' OR mID LIKE '%$kw%' ORDER BY mID limit 6";
$result_member_search = mysql_query ($query_member_search);
if(@mysql_num_rows($result_member_search)>0){
?>
 <div id="list-video-cat">
<?
while ($row_member_search=@mysql_fetch_array($result_member_search)){
	$memberID_search = $row_member_search["mID"];
	$memberName_search = $row_member_search["mUsername"];
	$memberPic_search = $row_member_search["link_avatar"];

?>
<div class="video-bit">
<div class="video-thumb"><a href="<?php echo $server; ?><?php echo CovertVn($memberName_search);?>-urs<?php echo $memberID_search; ?>.html" title="<?=$memberName_search?>">
<img onerror=this.src="<?=$homelink?>images/default-avatar.png" src="<?=$memberPic_search?>" alt="<?=$albumName_search?>" /><div class="play-video-icon"></div></a></div>
<div class="video-title"><h3>
<a href="<?php echo $server; ?><?php echo CovertVn($memberID_search);?>-urs<?php echo $albumID_search; ?>.html" title="<?=$memberName_search?>"><?=mb_substr($memberName_search, 0,60,'UTF-8')?></a></h3>
</div>
</div>
<?}?>
</div>
<?}else{?>
<h3 align='center'>Không tìm thấy thành viên nào !</h3>
<?}?>
<div class="clear"></div>
</div>
<?}?>
<?}?>
</div>
</div>
</div>



