<?php

class paging_ajax
{
    public $data; // DATA
    public $per_page = 1; // SỐ RECORD TRÊN 1 TRANG
    public $page; // SỐ PAGE 
    public $text_sql; // CÂU TRUY VẤN
    
    //	THÔNG SỐ SHOW HAY HIDE 
    public $show_pagination = true;
    public $show_goto = false;
    public $show_total = true;
    
    // TÊN CÁC CLASS
    public $class_pagination; 
    public $class_active;
    public $class_inactive;
    public $class_go_button;
    public $class_text_total;
    public $class_txt_goto;    
    
    private $cur_page;	// PAGE HIỆN TẠI
    private $num_row; // SỐ RECORD
    
    // PHƯƠNG THỨC LẤY KẾT QUẢ CỦA TRANG 
    public function GetResult()
    {
        global $db; // BIỀN $db TOÀN CỤC
        
        // TÌNH TOÁN THÔNG SỐ LẤY KẾT QUẢ
        $this->cur_page = $this->page;
        $this->page -= 1;
        $this->per_page = $this->per_page;
        $start = $this->page * $this->per_page;
        
        // TÍNH TỔNG RECORD TRONG BẢNG
		$result = mysql_query($this->text_sql);
        $this->num_row = mysql_num_rows($result);
        
        // LẤY KẾT QUẢ TRANG HIỆN TẠI
        return mysql_query($this->text_sql." LIMIT $start, $this->per_page");
    }
    
    // PHƯƠNG THỨC XỬ LÝ KẾT QUẢ VÀ HIỂN THỊ PHÂN TRANG
    public function Load()
    {
        // KHÔNG PHÂN TRANG THÌ TRẢ KẾT QUẢ VỀ
        if(!$this->show_pagination)
            return $this->data;
        
        // SHOW CÁC NÚT NEXT, PREVIOUS, FIRST & LAST
        $previous_btn = true;
        $next_btn = true;
        $first_btn = true;
        $last_btn = true;    
        
        // GÁN DATA CHO CHUỖI KẾT QUẢ TRẢ VỀ 
        $msg = $this->data;
        
        // TÍNH SỐ TRANG
        $count = $this->num_row;
        $no_of_paginations = ceil($count / $this->per_page);
        
        // TÍNH TOÁN GIÁ TRỊ BẮT ĐẦU & KẾT THÚC VÒNG LẶP
        if ($this->cur_page >= 10) {
            $start_loop = $this->cur_page - 3;
            if ($no_of_paginations > $this->cur_page + 3)
                $end_loop = $this->cur_page + 3;
            else if ($this->cur_page <= $no_of_paginations && $this->cur_page > $no_of_paginations - 6) {
                $start_loop = $no_of_paginations - 6;
                $end_loop = $no_of_paginations;
            } else {
                $end_loop = $no_of_paginations;
            }
        } else {
            $start_loop = 1;
            if ($no_of_paginations > 10)
                $end_loop = 10;
            else
                $end_loop = $no_of_paginations;
        }
        
        // NỐI THÊM VÀO CHUỖI KẾT QUẢ & HIỂN THỊ NÚT FIRST 
        $msg .= "<div style='clear:both;'><div class='$this->class_pagination'><ul>";
        if ($first_btn && $this->cur_page > 1) {
            $msg .= "<li p='1' class='active'>Đầu</li>";
        } else if ($first_btn) {
            $msg .= "<li p='1' class='$this->class_inactive'>Đầu</li>";
        }
    
        // HIỂN THỊ NÚT PREVIOUS
        if ($previous_btn && $this->cur_page > 1) {
            $pre = $this->cur_page - 1;
            $msg .= "<li p='$pre' class='active'>Trước</li>";
        } else if ($previous_btn) {
            $msg .= "<li class='$this->class_inactive'>Cuối</li>";
        }
        for ($i = $start_loop; $i <= $end_loop; $i++) {
        
            if ($this->cur_page == $i)
                $msg .= "<li p='$i' class='actived'>{$i}</li>";
            else
                $msg .= "<li p='$i' class='active'>{$i}</li>";
        }
        
        // HIỂN THỊ NÚT NEXT
        if ($next_btn && $this->cur_page < $no_of_paginations) {
            $nex = $this->cur_page + 1;
            $msg .= "<li p='$nex' class='active'>Sau</li>";
        } else if ($next_btn) {
            $msg .= "<li class='$this->class_inactive'>Sau</li>";
        }
        
        // HIỂN THỊ NÚT LAST
        if ($last_btn && $this->cur_page < $no_of_paginations) {
            $msg .= "<li p='$no_of_paginations' class='$this->class_active'>Cuối</li>";
        } else if ($last_btn) {
            $msg .= "<li p='$no_of_paginations' class='$this->class_inactive'>Cuối</li>";
        }
        
        // SHOW TEXTBOX ĐỂ NHẬP PAGE KO ? 
        if($this->show_goto)
            $goto = "<input type='text' id='goto' class='$this->class_txt_goto' size='1' style='margin-top:-1px;margin-left:40px;margin-right:10px'/><input type='button' id='go_btn' class='$this->class_go_button' value='Đến'/>";
        if($this->show_total)
            $total_string = "<span id='total' class='$this->class_text_total' a='$no_of_paginations'>Trang <b>" . $this->cur_page . "</b>/<b>$no_of_paginations</b></span>";
        $stradd =  $goto . $total_string;
        
        // TRẢ KẾT QUẢ TRỞ VỀ
        return $msg . "</ul>" . $stradd . "</div></div>";  // Content for pagination
    }     
            
}

?>
<script type="text/javascript" language="javascript">
                                function showinfo(id)
                                {
                                    if($('#player'+id).is(':visible')) $('#row'+id).fadeOut('fast');
                                    else{
                                    var url = $('#song'+id).val();  
                                    genplayer(url,'player'+id);                            
                                    $('#row'+id).show();
                                    }                                                           
                                }
								function hideplayer(id)
                                {
                                    $('#'+id).fadeOut();
                                }
                            </script>
<?php

// KIỂM TRA TỒN TẠI PAGE HAY KHÔNG
if(isset($_POST["page"]))
{
	// ĐƯA 2 FILE VÀO TRANG & KHỞI TẠO CLASS
	include "../../XBTeam/se7en.php";
	require_once "../../XBTeam/functions.php";
	$paging = new paging_ajax();
	
	
	// ĐẶT CLASS CHO THÀNH PHẦN PHÂN TRANG THEO Ý MUỐN
	$paging->class_pagination = "pagination";
	$paging->class_active = "active";
	$paging->class_inactive = "inactive";
	$paging->class_go_button = "go_button";
	$paging->class_text_total = "total";
	$paging->class_txt_goto = "txt_go_button";

	// KHỞI TẠO SỐ PHẦN TỬ TRÊN TRANG
    $paging->per_page = 10; 	
    
    // LẤY GIÁ TRỊ PAGE THÔNG QUA PHƯƠNG THỨC POST
    $paging->page = $_POST["page"];
    
    // VIẾT CÂU TRUY VẤN & LẤY KẾT QUẢ TRẢ VỀ
    $paging->text_sql = "SELECT * FROM song WHERE sStatus!=3 AND sStatus!=0 ORDER BY m_time DESC";
    $result_pag_data = $paging->GetResult();

	
	// DUYỆT MẢNG LẤY KẾT QUẢ
	if(@mysql_num_rows($result_pag_data)>0){
	while ($row = mysql_fetch_array($result_pag_data)) {
	 			$i++;
			$sID = $row["sID"];
			$sName = $row["sName"];
			$tenbhkodau=CovertVn($sName);
			$sType = $row["sType"];
			$sView = $row["sView"];
			$sDownload = $row["sDownload"];
			$catID = $row["catID"];
			$catName = $row["catName"];
			$ngay_dang = $row["m_date"];
			$date = date_create($ngay_dang); 
			$ngaydang = date_format($date, 'd/m/Y');
			$singerID = $row["singerID"];
			$singerName = $row["singerName"];
			$sStatus = $row["sStatus"];
			$id_user = $row["mID"];;
			$sLink = $row["sLink"];
			$nguoiduyet = $row["nguoiduyet"];
			$hit = $row["sHot"];
            $mID = $row["mID"];	
            $sHot= $row["sHot"];			
			$query_top50 = "SELECT * FROM top50 WHERE sID='$sID'";
			$resultt = mysql_query ($query_top50);
			$top50=@mysql_num_rows($resultt);
			$query_css = "SELECT * FROM member WHERE mID='$id_user'";
			$result2 = mysql_query ($query_css);
			$numrows2=mysql_num_rows($result2);
			while ($row2=mysql_fetch_array($result2))
			{$username= $row2["mUsername"];}
			if($row['up']!=0 || $row['down']!=0){
			$up_value=intval($row['up']);
			$down_value=intval($row['down']);
			$total=$up_value+$down_value;

			$up_per=($up_value*100)/$total;
			$down_per=($down_value*100)/$total;
			}
			$sql_name_mem1 = mysql_query("SELECT * FROM comment WHERE sID='".$sID."'");
			$numpl = @mysql_num_rows($sql_name_mem1);
			if($sHot == '1') {
				$imgsHot = "<img src='".$server."Images/icon/hot-music.gif'>";
			}else {
				$imgsHot = "";
			} 
			$query_cs = "SELECT * FROM category WHERE catID='$catID'";
		$result1 = mysql_query ($query_cs);
		$row1=@mysql_fetch_array($result1);
		$the_loai= $row1["catName"];
		
		$query_cs1 = "SELECT * FROM singer WHERE singerID='$catID'";
		$result12 = mysql_query ($query_cs1);
		$row12=@mysql_fetch_array($result12);
		$deejay= $row12["singerName"];
		
		$query_cs3 = "SELECT * FROM member WHERE mID='$mID'";
		$result3 = mysql_query ($query_cs3);
		$row3=@mysql_fetch_array($result3);
		$linkavatar = $row3['link_avatar'];
		$nguoidang = $row3['mUsername'];
		
		if ( $sStatus == 1 ) {
		$status = "<a onclick='setInvisible(".$sID.")' style='CURSOR: hand'><img src='img/ok.png'/></a>";
		} else if ( $sStatus == 0 ) {
		$status = "<a onclick='setAvailable(".$sID.")' style='CURSOR: hand'><img src='img/clock.png'/></span></a>";
		} else {
		$status = "<a onclick='setAvailable(".$sID.")' style='CURSOR: hand'><img src='img/ok.png'/></a>";
		}
		if ( $sHot == 1 ) {
		$sethot="<div ><input type='checkbox' value='".$sID."' checked='checked' onclick='setnotHot(".$sID.")' /><b> HOT</b></div> ";
		} else if ( $sHot == 0 ) {
		$sethot="<div><input type='checkbox' value='".$sID."'  onclick='setHot(".$sID.")' /><b> HOT</b></div>"; 
        } else {
		$sethot="<a onclick='setHot(".$sID.")' style='CURSOR: hand'><img src='./Images/icon-acp/lock.gif' border='0' /></a>";
		}
		if ( $top50 == 1 ) {
        $settop="<div ><input type='checkbox' value=".$sID."' checked='checked' onclick='huyTop(".$sID.")' /><b> TOP<b></div> ";
        } else if ( $top50 == 0 ) {
		$settop="<div ><input type='checkbox' value='".$sID."'   onclick='setTop(".$sID.")' /><b> TOP</b></div>"; 
        } else {
		$settop="<a onclick='setTop(".$sID.")' style='CURSOR: hand'><img src='./Images/icon-acp/lock.gif' border='0' /></a>";
        }

			
		$message .= '
		
							<tr id="songbody"'.$sID.'">
					<td><label class="checkbox"><input type="checkbox" name="element[]" value="'.$sID.'" id="element"></label></td>
					<td><img src="'.$homelink.'/AdminCP/img/'.$sType.'.gif" width="20" height="20"></td>
					<td><b> <a target="_blank" href="'.$server.''.CovertVn($sName).'-s'.$sID.'.html">'.$sName.''.$imgsHot.'</a></b></td>	
					<td><b>'.$the_loai.'</b></td>
					<td><b>'.$sView.'/'.$sDownload.'</b></td>
					<td><b>'.$ngaydang.'</b></td>
                    <td><b><a title="Gửi tin nhắn cho '.$username.'" rel="facebox" href="Client/SendMess.php?uid=<?=$mID?>">'.$username.'</a></b></td>																								
                    <td><b>'.$nguoiduyet.'</b></td>																								
					
					<td class="center">
					'.$status.'
			        
					<td>
                    <div><b>'.$row["up"].' / '.$row["down"].'</b></div>
                    <div class="like-dis-bar">
                    <div class="likebar" style="width: '.$up_per.'%;"></div>
                    <div class="dislikebar" style="width: '.$down_per.'%;"></div>
                    </div>
                    </td>
					
					<td>
					'.$sethot.'
					'.$settop.'                    
					</td>                                          		
                    
					<td>
                    <a title="Đưa lên đầu trang" class="actionbutton" onclick="setdautrang('.$sID.')"><img src="img/top.png" width="16px" height="16px" /></a>&nbsp;
                    <a title="Nghe nhanh" class="actionbutton" onclick="showinfo('.$sID.')"><img src="img/embed.png" width="16px" height="16px" /></a>&nbsp;
                    <a title="Sửa" class="actionbutton" target="_blank" href="edit-song-'.$sID.'.html"><img src="img/Edit.png" /></a>&nbsp;
                    <a title="Xóa kèm lý do" rel="facebox" href="xoa-tam-'.$sID.'.html"><img src="img/document-delete.png" width="16px" height="16px"></span></a>
					<a href="javascript: void(0)" title="Xóa vĩnh viễn" class="songdelete" id="'.$sID.'"><img width="16px" height="16px" src="img/Delete.png" /></a>
					</td>	
					<td><label class="checkbox"><input type="checkbox" name="element2[]" value="'.$sID.'" id="element2"></label></td>
					
					
                    
					<tr id="row'.$sID.'" style="display: none;background-color: #F5F5F5;">
                    <td colspan="10">
                    <input type="hidden" id="song'.$sID.'" value="'.$sLink.'" />                                    
                    <div id="player'.$sID.'">
                    </div>
                    <div onclick="hideplayer" style="min-height: 20px;background-color: #C1C1C1;border: solid 1px #7E7E7E;margin-top: 5px;cursor: pointer;">Ẩn</div>
                    </td>
                    </tr>
</tr>
		
		';
		}} else {	
		$message .= '<center>Chưa có dữ liệu cho BXH Nhạc DJ Đề cử !</center>';
		}

	// ĐƯA KẾT QUẢ VÀO PHƯƠNG THỨC LOAD() TRONG LỚP PAGING_AJAX
	$paging->data = "<div class='data'>
	
	<form action='".$server."He-thong.html' name='xoaFrm' method='POST'>
	<input type='hidden' name='act' value='removeSong' />
	<input type='hidden' name='back' value='<?=$qString?>&p=<?=$page?>' />
	<input type='hidden' name='ID' />
</form> 

<form action='".$server."He-thong.html' name='StatusFrm' method='POST'>
	<input type='hidden' name='act' value='' />
	<input type='hidden' name='ID' />
</form>
	<table class='tabledata' cellspacing='0' cellpadding='0' align='center' width='100%'>
<thead>
<tr>
<th>Check Album</th>
<th>Link</th>
<th>Tên</th>
<th>Thể loại</th>
<th>Nghe/Tải</th>
<th>Ngày Đăng</th>
<th>Người gửi</th>							
<th>Người duyệt</th>							
<th>Trạng thái</th>
<th>Like / Dislike</th>	
<th style='width: 70px;'>Công cụ</th>						
<th style='width: 110px;'>Thao tác</th>
<th><input class='button' type='submit' onClick='return confirm('Thím có chắc chắn muốn xóa những bài vừa chọn khỏi cơ sở dữ liệu ?');' value='Xóa' name='DelSongSelect' >
<input type='checkbox' value='<?=$sID?>' name='element2' id='element2' onclick='toggleChecked(this)' />
</th>
</tr>
</thead>
<tbody>
	" . $message . "</tbody>
</table></div>"; // Content for Data    
	echo $paging->Load();  // KẾT QUẢ TRẢ VỀ
		
} 

