var ManagerSong = 
{
    DeleteSelect : function()
    {
        var a = $('.tabledata #checkitem');
        var item = new Array();
        var i = 0;
        $.each(a,function(index,value){
           if($(this).is(':checked')) {item[i] = $(this).val();i++; } 
                                         
        });        
        if (confirm("Bạn có chắc chắn xóa ?")) {
            
            $.ajax({
            url: "ModelAjax.php?method=deleteselect",
            type: "POST",
            data: {"sID" : item},
            cache: false,
            success: function (data) {
                if(data>0)
                {
                    $.each(a,function(index,value){
                        
                       if($(this).is(':checked')) {
                        var id= $(this).val();
                        $('#item'+id).hide();
                        $('#row'+id).hide();
                        }                                 
                    }); 
                }
                else
                {
                    alert('Bạn không có quyền sử dụng chức năng này !');
                }
            },
            error : function()
            {
                alert('Loi');
            }
            });     
          }
    },
    ChangeStatus :function(songid,status)
    {        
        $('#loading').fadeIn();
        $.ajax({
            url: "ModelAjax.php?method=changestatus",
            type: "POST",
            data: {"songid" : songid,"status": status},
            cache: false,
            success: function (data) {
                $('#loading').fadeOut();
                if(data != 0){
                if(status==1) $('#status'+songid).html('<a onclick="return changestatus(\'deselect\','+songid+')" href="#" ><img src ="http://nhacdj.vn/Images/Icon/Tick.png" /></a>');
                else $('#status'+songid).html('<a onclick="return changestatus(\'select\','+songid+')" href="#" ><img src ="http://nhacdj.vn/Images/Icon/Deselect.png" /></a>');
                }
            },
            error : function()
            {
                alert('Loi');
            }
        }); 
    },
    ChangeHit : function(id,valimg)
    {
        $.ajax({
            url: "ModelAjax.php?method=changehit",
            type: "POST",
            data: {"img" : valimg,"id":id},
            cache: false,
            success: function (data) {                
                 if(data==1) {
                    if(valimg.length>=5)$('#sname'+id).append(' <img src="'+valimg+'" />');
                    else $('#sname'+id).children('img').remove();
                 }
            },
            error : function()
            {
                alert('Loi');
            }
        }); 
    },
    ChangeTop : function(id,top)
    {
        $.ajax({
            url: "ModelAjax.php?method=changetop",
            type: "POST",
            data: {"top" : top,"id":id},
            cache: false,
            success: function (data) {                
                 if(data==1) {
                    if(top>=1)$('#top'+id).show();
                    else $('#top'+id).hide();                    
                 }
            },
            error : function()
            {
                alert('Loi');
            }
        }); 
    },
    ChangeSort : function(id,sort,a,i)
    {
        
        $.ajax({
            url: "ModelAjax.php?method=changesort",
            type: "POST",
            data: {"sort" : sort,"id":id},
            cache: false,
            success: function (data) {
                var percent = (100/a)*(i+1);                
           	    $( "#progressbar" ).progressbar({
        			value: percent
        		});
                if(percent=100)
                {
                  
                  $('#bbar').html('<div class="message success closeable"><p><strong>Thành công !</strong> Sắp xếp thành công</p></div>');  
                }
                
            },
            error : function()
            {
               $('#bbar').html('<div class="message error closeable"><p><strong>Thất bại !</strong> Có lỗi trong quá trình sắp xếp</p></div>');
            }
        }); 
    }
    
}