﻿function xoa(n) {
	if(confirm("Bạn muốn xoá thật không?")) {
		xoaFrm.ID.value=n;
		xoaFrm.submit();
	}
}
function huyxoatam(n) {
	if(confirm("Bạn muốn khôi phục bài hát này?")) {
		xoaFrm.ID.value=n;
		xoaFrm.submit();
	}
}

function setInvisible(n) {
	if(confirm("Bài hát này sẽ không hiện trên trang chủ?")) {
		StatusFrm.act.value="setInvisible";
		StatusFrm.ID.value=n;
		StatusFrm.submit();
	}
}

function setAvailable(n) {
	if(confirm("Bài hát này sẽ được hiện trên trang chủ?")) {
		StatusFrm.act.value="setAvailable";
		StatusFrm.ID.value=n;
		StatusFrm.submit();
	}
}
function setnotHot(n) {
	if(confirm("Hủy HOT cho ca khúc này ?")) {
		StatusFrm.act.value="setnotHot";
		StatusFrm.ID.value=n;
		StatusFrm.submit();
	}
}

function setHot(n) {
	if(confirm("Thêm HOT cho ca khúc này ?")) {
		StatusFrm.act.value="setHot";
		StatusFrm.ID.value=n;
		StatusFrm.submit();
	}
	
}
function huyNew(n) {
	if(confirm("Hủy NEW cho ca khúc này ?")) {
		StatusFrm.act.value="huyNew";
		StatusFrm.ID.value=n;
		StatusFrm.submit();
	}
}

function setNew(n) {
	if(confirm("Thêm NEW cho ca khúc này ?")) {
		StatusFrm.act.value="setNew";
		StatusFrm.ID.value=n;
		StatusFrm.submit();
	}
	
}
function huyTop(n) {
	if(confirm("Hủy TOP cho ca khúc này ?")) {
		StatusFrm.act.value="huyTop";
		StatusFrm.ID.value=n;
		StatusFrm.submit();
	}
}

function setTop(n) {
	if(confirm("Thêm TOP cho ca khúc này ?")) {
		StatusFrm.act.value="setTop";
		StatusFrm.ID.value=n;
		StatusFrm.submit();
	}
	
}
function catInvisible(n) {
	if(confirm("Thể loại này sẽ không hiện trên trang chủ?")) {
		StatusFrm.act.value="catInvisible";
		StatusFrm.ID.value=n;
		StatusFrm.submit();
	}
}

function catAvailable(n) {
	if(confirm("Thể loại này sẽ được hiện trên trang chủ?")) {
		StatusFrm.act.value="catAvailable";
		StatusFrm.ID.value=n;
		StatusFrm.submit();
	}
}
function setdautrang(n) {
	if(confirm("Bài hát này sẽ được hiện lên đầu trang chủ ?")) {
		StatusFrm.act.value="setdautrang";
		StatusFrm.ID.value=n;
		StatusFrm.submit();
	}
}
function toggleChecked(oElement) 
      { 
        oForm = oElement.form; 
        oElement = oForm.elements[oElement.name]; 
        if(oElement.length) 
        { 
          bChecked = oElement[0].checked; 
          for(i = 1; i < oElement.length; i++) 
            oElement[i].checked = bChecked; 
        } 
      }
function showinfo(id)
{
	if($('#player'+id).is(':visible')) $('#row'+id).fadeOut('fast');
	else{
	var url = $('#song'+id).val();  
	genplayer(url,'player'+id);                            
	$('#row'+id).show();
	}                                                           
}
function hideplayer(id)
{
	$('#'+id).fadeOut();
}