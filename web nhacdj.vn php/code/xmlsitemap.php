<?
/*===   GOOGLE XML SITEMAP GENERATOR  ===*/
/*===  Tweaked BY Jeff Dunsmore  ===*/
/*===   www.webhostingelite.com    ===*/
/*===== CUSTOMIZE THE VALUES BELOW (check your spelling!) ===*/
/*===========================================================*/
//Your website url including http:// prefix and trailing slash /
$siteUrl = "http://www.webhostingelite.com/";

//Change below to all the pages of your website that you want inluded in your google sitemap, Then after the =>, insert the update frequency of that page.
//They must be in single quotes seperated by commas and relative to your site root......
//eg. if the file is http://www.webhostingelite.com/contactform/contact.php, you would write "contactform/contact.php"
//The order you place the values, the higher the priority it will be.
$pages = array('default.cfm' => 'monthly','webhostingelite/web_hosting.cfm' => 'monthly','webhostingelite/web_design.cfm' => 'monthly','webhostingelite/coldfusion_web_designers.cfm' => 'monthly','webhostingelite/database_design.cfm' => 'monthly','webhostingelite/advertising.cfm' => 'monthly','phpxmlsitemapcreator.cfm' => 'monthly','webhostingelite/customersupport.cfm' => 'monthly','webhostingelite/contact.cfm' => 'monthly');




/*===============================================================================================*/
/*=======================        NOTHING IS EDITABLE BELOW THIS LINE        =====================*/
/*===============================================================================================*/

//Initialize the xml
$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns=\"http://www.google.com/schemas/sitemap/0.84\">";

//Scroll through the pages
$count = 0;
foreach ($pages as $page => $freq) {
	if (file_exists($_SERVER['DOCUMENT_ROOT']."/".$page)) {

		$time = date('Y-m-d',filemtime($_SERVER['DOCUMENT_ROOT']."/".$page));

		$xml .= "<url>\n";
		$xml .= "<loc>{$siteUrl}{$page}</loc>\n";
		$xml .= "<lastmod>{$time}</lastmod>\n";
		$xml .= "<changefreq>{$freq}</changefreq>\n";
		if ($count < 3) {
			$min = "0.";
			$min .= $count*2;
			$priority = number_format(1.0 - $min,1,".",",");
		} else {
			$priority = 0.5;
		}
		$xml .= "<priority>$priority</priority>";
		$xml .= "</url>\n";

	}
	$count++;
}
$xml .=  "</urlset>";

header ("content-type: text/xml");
echo $xml;
?>