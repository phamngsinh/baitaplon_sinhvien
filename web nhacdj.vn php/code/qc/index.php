﻿
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta name="description" content="">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Quảng cáo tại KenhDJ.Vn</title>
<link rel="stylesheet" type="text/css" href="style.css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
</script>
</head>
<body id="portfolio_page">
<div id="container"><div id="background_top">

	<!-- Start banner -->
    <div id="banner">
    	<div id="nav">
        	<ul>
            	<li><a class="current" href="index.html"><strong><img src="images/intro.png" alt=""> Introduction</strong><span>Giới thiệu về KenhDJ.Vn</span></a></li>
                <li><a  href="thong-ke.html"><strong><img src="images/statistics.png" alt=""> Statistics</strong><span>Thống kê truy cập</span></a></li>
                <li><a  href="bang-gia.html"><strong><img src="images/adv.png" alt=""> Advertising</strong><span>Vị trí quảng cáo</span></a></li>
                <li><a  href="thanh-toan.html"><strong><img src="images/payment.png" alt=""> Payment</strong><span>Cách thức thanh toán</span></a></li>
                <li><a  href="lien-he.html"><strong><img src="images/contact.png" alt=""> Contact</strong><span>Liên hệ</span></a></li>
            </ul>
        </div>
        <a href="index.html" id="header_logo"><img src="images/logo.png" alt="logoDj"></a>
    </div>
    <!-- End banner -->
    
    <div id="content">
    
    
    
<div class="top_boxes" style="display: block;">
    <a class="top_box" href="#">
    <h3>Giao diện thân thiện, tính năng hữu ích</h3>
    <img style="float: left; margin-left: 10px;" alt="" src="images/twitter-dj-icon.png" width="80" />
    <span style="display: block; padding: 10px 5px 0 80px; text-align: left; line-height: 180%;">Với giao diện đơn giản, tính năng thiết thực, chúng tôi mong muốn đem lại sự thoải mái nhất cho các bạn</span>
    </a>
    
    <a class="top_box middle" href="#">
    <h3>Hỗ trợ hầu hết các trình duyệt hiện nay</h3>
    <span class="top_image">
    <img alt="" src="images/top_browsers.png" />
    </span>
    </a>
    
    <a class="top_box" style="float: right;" href="#">
    <h3>Lượng truy cập ổn định, nội dung cập nhật</h3>
    <img style="float: left; margin-left: 10px;" alt="" src="images/Virtual-DJ-icon.png" width="80" />
    <span style="display: block; padding: 10px 8px 0 80px; text-align: left; line-height: 180%;">Nội dung website được cập nhật hàng ngày nên đảm bảo được lượng truy cập ổn định.</span>
    </a>
    </div>
       
    	<div class="wrap">

<div style="height: 0px; overflow: hidden; line-height: 0px;"><img src="custom_overlay.htm" alt=""></div>
<div class="padding" style="margin: 15px;">
<h2>Giới thiệu chung về Website</h2><br />

<div id="block-left">
<font color="white"><b>GIỚI THIỆU VỀ KenhDJ.Vn:</b></font>
<ul>
<li>- KenhDJ.Vn là Website nghe nhạc trực tuyến chất lượng cao và hoàn toàn miễn phí.</li>
<li>- Các thể loại nhạc chính : Nonstop , Việt Remix , Vietnam DJ Producer , House , Trance ,...</li>
<li>- Website là nơi giao lưu , học hỏi giữa các DJ . Cùng nhau trao đổi , đánh giá , góp ý để hoàn thiện bản mix .</li>
<li>- Nội dung website phong phú, lành mạnh, không vi phạm thuần phong mỹ tục Việt Nam, cập nhật hàng ngày...</li>
</ul>
</div>

<div id="block-right">
<font color="white"><b>QUY CÁCH CHUNG:</b></font>
<ul>
<li>- Website phát triển trước hết để phục vụ nhu cầu của các bạn yêu thích thể loại nhạc này .</li>
<li>- Chúng tôi sẵn sàng để vị trí quảng cáo của bạn với mức giá rẻ nhất .</li>
<li>- Banner và website có nội dung lành mạnh, không vi phạm thuần phong mỹ tục Việt Nam.</li>
<li>- Chúng tôi có quyền gỡ bỏ quảng cáo của bạn nếu phát hiện website của bạn vi phạm những quy định về pháp luật Việt Nam, hoặc banner gây ảnh hưởng xấu đến website của chúng tôi.<br />(Khi đó chúng tôi sẽ có thông báo đến cho bạn)</li>
</ul>
</div>

<div style="clear: both;"></div>




</div>

</div></div></div></div></div></div></div></div>

<div id="footer_links">

	

</div>

   
</div></div>


</div></body></html>