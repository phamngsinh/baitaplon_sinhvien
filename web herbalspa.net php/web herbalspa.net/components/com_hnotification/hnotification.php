<?php
/*------------------------------------------------------------------------
# hnotification.php - hnotification Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Set the component css/js
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_hnotification/assets/css/hnotification.css');

// Require helper file
JLoader::register('HnotificationHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'hnotification.php');

// import joomla controller library
jimport('joomla.application.component.controller');

// Get an instance of the controller prefixed by Hnotification
$controller = JController::getInstance('Hnotification');

// Perform the request task
$controller->execute(JRequest::getCmd('task'));

// Redirect if set by the controller
$controller->redirect();
?>