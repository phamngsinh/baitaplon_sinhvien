<?php
/*------------------------------------------------------------------------
# default.php - hcontact Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

?>

<script type="text/javascript" src="components/com_hcontact/jquery.placeholder.js" ></script>
<style>
	#hcontact-content input{
		width:200px;
		height:24px;
		margin-top:5px;
		margin-left:15px;
	}
	#hcontact-content textarea{
		width:200px;
		margin-left:15px;
	}
	.cform-title{
		padding: 10px 15px;
		text-align:center;
		color:#3a2313;
		font-size:18px;
		font-weight: bold;
	}
	.button{
		width:100px !important;
		float:right;
		border-radius: 0px;
		background:#3a2215;
		color:#FFFFFF;
	}
</style>

<div id="hcontact-content">
	<div class="contact-banner">
		<?php echo $this->item->banner; ?>
	</div>
	<div class="contact-box">
		<form method="post" action="index.php?option=com_hcontact&task=sendMail" >
		<table>
			<tr>
				<td colspan="2">
					<div class="cform-title">
					Đặt Hàng Online <br/>
					Liên Hệ - Góp Ý
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" id="name" name="name" placeholder="Họ & Tên"/>
				</td>
				<td>
					<input type="text" id="subject" name="subject"  placeholder="Tiêu đề"/>
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" id="phone" name="phone" placeholder="Số điện thoại"/>
				</td>
				<td rowspan="3">
					<textarea id="content"  name="content" placeholder="Nội dung" rows="9">
						
					</textarea>
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" id="address"  name="address" placeholder="Địa chỉ"/>
				</td>				
			</tr>
			<tr>
				<td>
					<input type="email"  name="email" placeholder="Email"/>
				</td>				
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" class="button" value="GỬI ĐI"/>
				</td>				
			</tr>
		</table>
		<input type="hidden" name="receipt" value="<?php echo $this->item->email; ?>" />
	</form>
	</div>
	<div class="map-box">{mosmap} </div>
	
</div>
	<div class="bank-acc-box">
		<div class="acc-name-box">
			<p>Thông tin chuyển khoản</p>
			<p><strong> <?php echo $this->item->name; ?></strong></p>
		</div>
		<div class="acc-detail-box">
			<?php echo $this->item->accounts; ?>
		</div>
	</div>
	


<script>
	jQuery(function($){
  $('input, textarea').placeholder();
});
</script>