<?php
/*------------------------------------------------------------------------
# default.php - dich vu Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Connect to database
$db = JFactory::getDBO();
jimport('joomla.filter.output');
?>
<div id="hdichvu-hdichvu">
	<?php foreach($this->items as $item){ ?>
		<?php
		$item->category = $db->setQuery('SELECT #__categories.title FROM #__categories WHERE #__categories.id = "'.$item->category.'"')->loadResult();
		if(empty($item->alias)){
			$item->alias = $item->name;
		};
		$item->alias = JFilterOutput::stringURLSafe($item->alias);
		$item->linkURL = JRoute::_('index.php?option=com_hdichvu&view=edit&id='.$item->id.':'.$item->alias);
		?>
		<p><strong>Name</strong>: <a href="<?php echo $item->linkURL; ?>"><?php echo $item->name; ?></a></p>
		<p><strong>Price</strong>: <?php echo $item->price; ?></p>
		<p><strong>Description</strong>: <?php echo $item->description; ?></p>
		<p><strong>Images</strong>: <?php echo $item->images; ?></p>
		<p><strong>Times</strong>: <?php echo $item->times; ?></p>
		<p><strong>Category</strong>: <?php echo $item->category; ?></p>
		<p><strong>Link URL</strong>: <a href="<?php echo $item->linkURL; ?>">Go to page</a> - <?php echo $item->linkURL; ?></p>
		<br /><br />
	<?php }; ?>
</div>
