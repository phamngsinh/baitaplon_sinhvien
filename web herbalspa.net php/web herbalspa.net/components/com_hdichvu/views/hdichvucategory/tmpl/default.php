<?php
/*------------------------------------------------------------------------
# default.php - dich vu Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

?>
<script type="text/javascript" src="components/com_hdichvu/fancybox/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
	<script type="text/javascript" src="components/com_hdichvu/fancybox/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<link rel="stylesheet" type="text/css" href="components/com_hdichvu/fancybox/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<script>

jQuery(document).ready(function(){
			jQuery(".sp").fancybox({
				'padding'			: 0,
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			});
		});
</script>

<style>
	.pop-contain{
		width:900px;
		height:600px;
		background:#FFFFFF;
		border-radius:5px;
		border:solid 1px #cdcdcd;
	}
	#fancybox-outer {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0) !important;}
    .pop-contain .thumb{
    	width:160px;
    	float:left;
    	height:560px;
    	margin:15px 20px 20px 20px;
    }
    .pop-contain .thumb p{margin-top:0px;}
   .pop-contain .thumb img{
    	width:160px;
    }
    .pop-contain .img{
    	width:350px;
    	float:left;
    	height:560px;
    }
     .pop-contain .img img{
    	width:350px;
    
    }
    .pop-contain .content{
    	width:310px;
    	float:left;
    	height:560px;
    	margin:20px;
    }
     .pop-contain  .pop-desc{
     	float:left;
     	text-align:justify;
     	max-height:450px;
     	overflow-x: hidden;
     	overflow-y:auto;
     	width: 100%;
     }
</style>

<div class="pro-left">
	<div class="pro-pagination">
		<a class="page-button" href="index.php/dich-vu?id=<?php echo $this->cat;?>&page=<?php echo ($this->page + 1);?>"></a>
		<a class="page-button" href="index.php/dich-vu?id=<?php echo $this->cat;?>&page=<?php echo ($this->page - 1);?>"></a>
	</div>
	<div id="category-hsanpham-content">
	<?php foreach($this->items as $item){ ?>
		<div class="sanpham <?php echo $counter%3 == 0?"no-margin-left":"" ?>">
			<a class="sp" href="#id<?php echo $counter;?>"><?php echo $item->images; ?></a>
			<div>
				<span class="pro-name">
				<?php echo $item->name; ?>
				</span>
				<div class="pro-short-desc" >
					
					<?php 
					$desc = explode('<hr id="system-readmore" />', $item->description); 
					echo $desc[0];
				?>
				</div>
				<div class="pro-price-container">
					<div class="time-value">
						Thời gian: <?php echo $item->times; ?>
					</div>
					<div class="price-value">
						<?php echo $item->price; ?>
					</div>
					
				</div>
			</div>
		</div>
		<div style="display: none;">
			
			<div id="id<?php echo $counter;?>" class="fancy">
				<div class="pop-contain">
					<div class="thumb">
						<?php echo $item->images; ?>
					</div>
					<div class="img">
						<?php echo $item->images; ?>
					</div>
					<div class="content">
						<span class="pro-name" style="display: block;width:100%;float:left;font-size: 34px;height:auto;padding-bottom:5px;">
						<?php echo $item->name; ?>
						</span>
						<div class="price-value" style="float:left;">
						<?php echo $item->price; ?>
						</div>
						<div class="pop-desc">
							<?php echo $desc[1]; ?>
						</div>
						
					</div>
				
				
				</div>
			</div>
			
		</div>
	<?php $counter++;} ?>
	</div>
</div>
<div class="pro-right">
	<h3 class="title">Dịch Vụ Của Tháng</h3>
	<div class="mpro-container">
		<div id="slider" style="height: <?php echo count($this->mItem)*155; ?>px ">
		<?php $spc = 0;foreach($this->mItem as $mitem){ ?>
		<div class="sanpham ">
			<a class="sp" href="#mid<?php echo $spc;?>">
			<?php echo $mitem->images; ?>
			</a>
			<div>
				<div class="pro-name">
				<?php echo $mitem->name; ?>
				</div>
				
				<div class="pro-price-container">
					<div class="price-value">
						<?php echo $mitem->price; ?>
					</div>
					
				</div>
			</div>
		</div>
			<div style="display: none;">
			<div id="mid<?php echo $spc;?>" class="fancy">
				
				
				<div class="pop-contain">
					<div class="thumb">
						<?php echo $mitem->images; ?>
					</div>
					<div class="img">
						<?php echo $mitem->images; ?>
					</div>
					<div class="content">
						<span class="pro-name" style="display: block;width:100%;float:left;font-size: 34px;height:auto;padding-bottom:5px;">
						<?php echo $mitem->name; ?>
						</span>
						<div class="price-value" style="float:left;">
						<?php echo $mitem->price; ?>
						</div>
						<div class="pop-desc">
							<?php $mdesc =  explode('<hr id="system-readmore" />',$mitem->description); echo $mdesc[1];?>
				
						</div>
						
					</div>
				
				
				</div>
			</div>
			
		</div>
	<?php $spc ++;} ?>
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function(){
		
			jQuery(".sp").find("p").hide();
			jQuery(".sp").find("p:first-child").show();
			
			jQuery(".sanpham").find("p").hide();
			jQuery(".sanpham").find("p:first-child").show();
			jQuery(".img").find("p").hide();
			jQuery(".img").find("p:first-child").show();
			
			jQuery(".thumb").find("img").click(function(){
				var img = jQuery(this).parent().html();
				jQuery(this).parent().parent().parent().find("div.img").html("<p>"+img+"</p>");
			});
			
	});
</script>
<?php if(count($this->mItem) > 3){ ?>
<script>

		

var kk = 155;
var longs ="<?php echo count($this->mItem)*155 ?>";
var l = 498 - parseInt(longs);
	jQuery(document).ready(function(){
		
			setInterval(function(){
				
			if(kk == 11){
				jQuery("#slider").animate({"top":""+kk+"px"},1000);
			}else{
				jQuery("#slider").animate({"top":"-"+kk+"px"},1000);
			}
				if((-kk) == l || (-kk) < l){
				
					kk= 11;
						
				}else{
					if(kk == 11){
						kk+=140;
					}else{
						kk+=165;
					}
					
				}
			},5000);
			
	
	});
	

</script>
<?php } ?>