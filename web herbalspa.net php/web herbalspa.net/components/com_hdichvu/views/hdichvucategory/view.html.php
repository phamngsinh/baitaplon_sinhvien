<?php
/*------------------------------------------------------------------------
# view.html.php - dich vu Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML Hdichvu Category View class for the Hdichvu Component
 */
class HdichvuViewhdichvucategory extends JView
{
	// Overwriting JView display method
	function display($tpl = null)
	{
		$this->items = JModel::getInstance("hdichvucategory","HdichvuModel")->getItems();
        $this->page = JRequest::getVar("page")?JRequest::getVar("page"):1;
		$this->cat= JRequest::getInt('id');
		$this->mItem = JModel::getInstance("hdichvucategory","HdichvuModel")->getMItems();
		// Check for errors.
		if (count($errors = $this->get('Errors'))){
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		};

		// Display the view
		parent::display($tpl);
	}
}
?>