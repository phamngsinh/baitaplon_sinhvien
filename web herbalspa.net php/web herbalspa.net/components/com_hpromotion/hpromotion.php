<?php
/*------------------------------------------------------------------------
# hpromotion.php - hpromotion Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Set the component css/js
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_hpromotion/assets/css/hpromotion.css');

// Require helper file
JLoader::register('HpromotionHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'hpromotion.php');

// import joomla controller library
jimport('joomla.application.component.controller');

// Get an instance of the controller prefixed by Hpromotion
$controller = JController::getInstance('Hpromotion');

// Perform the request task
$controller->execute(JRequest::getCmd('task'));

// Redirect if set by the controller
$controller->redirect();
?>