<?php
/*------------------------------------------------------------------------
# hdichvu.php - dich vu Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Hdichvu component helper.
 */
abstract class HdichvuHelper
{
	/**
	 *	Configure the Linkbar.
	 */
	public static function addSubmenu($submenu) 
	{
		JSubMenuHelper::addEntry(JText::_('Hdichvu'), 'index.php?option=com_hdichvu&view=hdichvu', $submenu == 'hdichvu');
		JSubMenuHelper::addEntry(JText::_('Categories'), 'index.php?option=com_categories&view=categories&extension=com_hdichvu', $submenu == 'categories');

		// set some global property
		$document = JFactory::getDocument();
		if ($submenu == 'categories'){
			$document->setTitle(JText::_('Categories - Hdichvu'));
		};
	}

	/**
	 *	Get the actions
	 */
	public static function getActions($Id = 0)
	{
		jimport('joomla.access.access');

		$user	= JFactory::getUser();
		$result	= new JObject;

		if (empty($Id)){
			$assetName = 'com_hdichvu';
		} else {
			$assetName = 'com_hdichvu.message.'.(int) $Id;
		};

		$actions = JAccess::getActions('com_hdichvu', 'component');

		foreach ($actions as $action){
			$result->set($action->name, $user->authorise($action->name, $assetName));
		};

		return $result;
	}
}
?>