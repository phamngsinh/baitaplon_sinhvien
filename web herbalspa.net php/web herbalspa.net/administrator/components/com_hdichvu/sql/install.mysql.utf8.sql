CREATE TABLE IF NOT EXISTS `#__hdichvu_edit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `price` varchar(256) NOT NULL,
  `description` TEXT NOT NULL DEFAULT '',
  `images` TEXT NOT NULL DEFAULT '',
  `times` varchar(256) NOT NULL,
  `category` int(11) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL,
  `checked_out_time` DATETIME NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;