<?php
/*------------------------------------------------------------------------
# hsanphamzero.php - san pham Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the list field type
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

/**
 * name Form Field class for the Hsanpham component
 */
class JFormFieldhsanphamzero extends JFormFieldList
{
	/**
	 * The name field type.
	 *
	 * @var		string
	 */
	protected $type = 'hsanphamzero';

	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return	array		An array of JHtml options.
	 */
	protected function getOptions()
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('#__hsanpham_edit.id as id, #__hsanpham_edit.name as name');
		$query->from('#__hsanpham_edit');
		$db->setQuery((string)$query);
		$items = $db->loadObjectList();
		$options = array();
		if($items){
			foreach($items as $item){
				$options[] = JHtml::_('select.option', $item->id, ucwords($item->name));
			};
		};
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>