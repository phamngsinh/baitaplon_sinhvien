<?php
/*------------------------------------------------------------------------
# hsanpham.php - san pham Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Hsanpham component helper.
 */
abstract class HsanphamHelper
{
	/**
	 *	Configure the Linkbar.
	 */
	public static function addSubmenu($submenu) 
	{
		JSubMenuHelper::addEntry(JText::_('Hsanpham'), 'index.php?option=com_hsanpham&view=hsanpham', $submenu == 'hsanpham');
		JSubMenuHelper::addEntry(JText::_('Categories'), 'index.php?option=com_categories&view=categories&extension=com_hsanpham', $submenu == 'categories');

		// set some global property
		$document = JFactory::getDocument();
		if ($submenu == 'categories'){
			$document->setTitle(JText::_('Categories - Hsanpham'));
		};
	}

	/**
	 *	Get the actions
	 */
	public static function getActions($Id = 0)
	{
		jimport('joomla.access.access');

		$user	= JFactory::getUser();
		$result	= new JObject;

		if (empty($Id)){
			$assetName = 'com_hsanpham';
		} else {
			$assetName = 'com_hsanpham.message.'.(int) $Id;
		};

		$actions = JAccess::getActions('com_hsanpham', 'component');

		foreach ($actions as $action){
			$result->set($action->name, $user->authorise($action->name, $assetName));
		};

		return $result;
	}
}
?>