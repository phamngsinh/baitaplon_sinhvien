CREATE TABLE IF NOT EXISTS `#__hcontact_edit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `accounts` TEXT NOT NULL DEFAULT '',
  `banner` TEXT NOT NULL DEFAULT '',
  `email` varchar(256) NOT NULL,
  `lmap` varchar(256) NOT NULL,
  `lamap` varchar(256) NOT NULL,
  `checked_out` int(11) NOT NULL,
  `checked_out_time` DATETIME NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;