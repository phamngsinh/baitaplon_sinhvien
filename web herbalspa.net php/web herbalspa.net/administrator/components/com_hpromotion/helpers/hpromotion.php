<?php
/*------------------------------------------------------------------------
# hpromotion.php - hpromotion Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Hpromotion component helper.
 */
abstract class HpromotionHelper
{
	/**
	 *	Configure the Linkbar.
	 */
	public static function addSubmenu($submenu) 
	{
		JSubMenuHelper::addEntry(JText::_('Hpromotion'), 'index.php?option=com_hpromotion&view=hpromotion', $submenu == 'hpromotion');
		JSubMenuHelper::addEntry(JText::_('Categories'), 'index.php?option=com_categories&view=categories&extension=com_hpromotion', $submenu == 'categories');

		// set some global property
		$document = JFactory::getDocument();
		if ($submenu == 'categories'){
			$document->setTitle(JText::_('Categories - Hpromotion'));
		};
	}

	/**
	 *	Get the actions
	 */
	public static function getActions($Id = 0)
	{
		jimport('joomla.access.access');

		$user	= JFactory::getUser();
		$result	= new JObject;

		if (empty($Id)){
			$assetName = 'com_hpromotion';
		} else {
			$assetName = 'com_hpromotion.message.'.(int) $Id;
		};

		$actions = JAccess::getActions('com_hpromotion', 'component');

		foreach ($actions as $action){
			$result->set($action->name, $user->authorise($action->name, $assetName));
		};

		return $result;
	}
}
?>