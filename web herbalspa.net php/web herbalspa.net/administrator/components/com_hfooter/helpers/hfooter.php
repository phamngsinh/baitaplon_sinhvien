<?php
/*------------------------------------------------------------------------
# hfooter.php - herbal footer Component
# ------------------------------------------------------------------------
# author    vuguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Hfooter component helper.
 */
abstract class HfooterHelper
{
	/**
	 *	Configure the Linkbar.
	 */
	public static function addSubmenu($submenu) 
	{
		JSubMenuHelper::addEntry(JText::_('Hfooter'), 'index.php?option=com_hfooter&view=hfooter', $submenu == 'hfooter');
	}

	/**
	 *	Get the actions
	 */
	public static function getActions($Id = 0)
	{
		jimport('joomla.access.access');

		$user	= JFactory::getUser();
		$result	= new JObject;

		if (empty($Id)){
			$assetName = 'com_hfooter';
		} else {
			$assetName = 'com_hfooter.message.'.(int) $Id;
		};

		$actions = JAccess::getActions('com_hfooter', 'component');

		foreach ($actions as $action){
			$result->set($action->name, $user->authorise($action->name, $assetName));
		};

		return $result;
	}
}
?>