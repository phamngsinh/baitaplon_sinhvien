<?php 
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.filesystem.file');

JHtml::_('behavior.framework', true);
$app				= JFactory::getApplication();
$doc				= JFactory::getDocument();
$templateparams		= $app->getTemplate(true)->params;
$doc->addStyleSheet($this->baseurl.'/templates/'.$this->template.'/css/style.css', $type = 'text/css', $media = 'screen,projection');

function check_user_agent ( $type = NULL ) {
        $user_agent = strtolower ( $_SERVER['HTTP_USER_AGENT'] );
        if ( $type == 'bot' ) {
                // matches popular bots
                if ( preg_match ( "/googlebot|adsbot|yahooseeker|yahoobot|msnbot|watchmouse|pingdom\.com|feedfetcher-google/", $user_agent ) ) {
                        return true;
                        // watchmouse|pingdom\.com are "uptime services"
                }
        } else if ( $type == 'browser' ) {
                // matches core browser types
                if ( preg_match ( "/mozilla\/|opera\//", $user_agent ) ) {
                        return true;
                }
        } else if ( $type == 'mobile' ) {
                // matches popular mobile devices that have small screens and/or touch inputs
                // mobile devices have regional trends; some of these will have varying popularity in Europe, Asia, and America
                // detailed demographics are unknown, and South America, the Pacific Islands, and Africa trends might not be represented, here
                if ( preg_match ( "/phone|iphone|itouch|ipod|symbian|android|htc_|htc-|palmos|blackberry|opera mini|iemobile|windows ce|nokia|fennec|hiptop|kindle|mot |mot-|webos\/|samsung|sonyericsson|^sie-|nintendo/", $user_agent ) ) {
                        // these are the most common
                        return true;
                } else if ( preg_match ( "/mobile|pda;|avantgo|eudoraweb|minimo|netfront|brew|teleca|lg;|lge |wap;| wap /", $user_agent ) ) {
                        // these are less common, and might not be worth checking
                        return true;
                }
        }
        return false;
}
$ismobile = check_user_agent('mobile');
if($ismobile) {
?>
<style>
#footer .custom {
   
    padding-left: 8px !important;
    padding-right: 8px !important;
}
</style>
<?php
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
<head>
<jdoc:include type="head" />
</head>

<?php 

$menu = $app->getMenu();
if ($menu->getActive() == $menu->getDefault()) { 
		$db = JFactory::getDBO();
		$query = "Select * FROM #__hnotification_edit limit 0,1";
		$db->setQuery($query);
		$result = $db->loadObject();
?>
		<script type="text/javascript" src="components/com_hdichvu/fancybox/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
		<script type="text/javascript" src="components/com_hdichvu/fancybox/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<link rel="stylesheet" type="text/css" href="components/com_hdichvu/fancybox/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
		<style>
			#top_banner p {
			    z-index:8;
			    width: 970px;
			    height:410px;
			    position:absolute;
			    top:5px;
			    left:5px;
			}
			#top_banner p img{
			   width: 970px;
			    height:410px;
			}
			#top_banner p.active {
			    z-index:10;
			}
			
			#top_banner p.last-active {
			    z-index:9;
			}
			#popup-notification{
			width:900px;
			height:600px;
			background:#FFFFFF;
			border-radius:5px;
			border:solid 1px #cdcdcd;
			text-align: center;
			}
			#fancybox-outer {
		    background: none repeat scroll 0 0 rgba(0, 0, 0, 0) !important;
		    }
	
		</style>
		<script>
		jQuery(document).ready(function(){
		jQuery("#top_banner").find("p:first-child").addClass("active");
		
			
		setInterval( "slideSwitch()", 4000 );
	});
			function slideSwitch() {
			    var $active = jQuery('#top_banner p.active');
			
			    if ( $active.length == 0 ) $active = jQuery('#top_banner p:last');
			
			    var $next =  $active.next().length ? $active.next()
			        : jQuery('#top_banner p:first');
			
			    $active.addClass('last-active');
			        
			    $next.css({opacity: 0.0})
			        .addClass('active')
			        .animate({opacity: 1.0}, 1000, function() {
			            $active.removeClass('active last-active');
			        });
			}
		</script>
		<?php if($result->content != ""){ ?>
		<script>
			jQuery(document).ready(function(){
			jQuery("#show-notification").fancybox({
				'padding'			: 0,
				'autoScale'			: false,
				'width'				: '75%',
				'height'			: '75%',
				'transitionIn'	: 'elastic',
				'transitionOut'	: 'none'
			});
			jQuery("#show-notification").click();
		});
		</script>
	
		<div style="display:none;">
			<div id="popup-notification" class="fancy">
				<?php echo $result->content; ?>
				
			</div>
			<a id="show-notification" href="#popup-notification"></a>
		</div>
		<?php 
		} 
} ?>
<body>

	<div id="main-container">
		<div id="header">
			<a href="index.php" class="logo">
				<img src="<?php echo $this->baseurl.'/templates/'.$this->template.'/images/logo.png';?>">
			</a>
			<div id="top_menu">
				<jdoc:include type="modules" name="position-menu"   />
			</div>
			 	<?php if($this->countModules('position-banner')){?>
			<div id="top_banner">
				<jdoc:include type="modules" name="position-banner"   />
			</div>
			<?php } ?>
		</div>
		
		<div id="content_wap">
				<?php if($this->countModules('position-user1')){?>
			<div id="top_content">
				<jdoc:include type="modules" name="position-user1"   />
			</div>
			<?php } ?>
			<div class="no_collumns">
				  <jdoc:include type="message" />
                  <jdoc:include type="component" />
			</div>
			
		</div>
		<div id="footer">
			<jdoc:include type="modules" name="position-footer"   />
		</div>
		
	</div>


</body>