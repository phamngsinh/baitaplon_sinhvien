<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_custom
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;


?>


<div class="customm<?php echo $moduleclass_sfx ?>" <?php if ($params->get('backgroundimage')): ?> style="background-image:url(<?php echo $params->get('backgroundimage');?>)"<?php endif;?> >
	<?php 
	if($module->content == "<p>homeadv</p>"){
		$db = JFactory::getDBO();
		$query = "Select * FROM #__homeadv_edit ORDER BY `id` DESC";
		$db->setQuery($query);
		$result = $db->loadObjectList();
		
		foreach($result as $item){
			echo "<div class='custom'>". $item->content."</div>";
		}
		
	}else if($module->content == "<p>footeradv</p>"){
		$db = JFactory::getDBO();
		$query = "Select * FROM #__hfooter_edit ORDER BY `id`";
		$db->setQuery($query);
		$result = $db->loadObjectList();
		
		foreach($result as $item){
			echo "<div class='custom'><h3>".$item->title."</h3>". $item->content."</div>";
		}
		
	}else if($module->content == "<p>hbanner</p>"){
		$db = JFactory::getDBO();
		$query = "Select * FROM #__hbanner_edit where id = 1";
		$db->setQuery($query);
		$result = $db->loadObject();
		
		echo $result->banner; ?>

		<?php
		
	}else{
		echo $module->content;
	}
	?>
</div>


