<!--
// Copyright  2005 Jandus Technologies - www.smartwebby.com - All Rights Reserved.
// This script in part or whole is not to be used without taking prior written permission from SmartWebby.com and Jandus Technologies.
 var bName = navigator.appName;
 var bVer = parseInt(navigator.appVersion);
 var bAgent=navigator.userAgent.toLowerCase();
 indOP=bAgent.indexOf("opera");
 verOP=parseInt(bAgent.charAt(indOP+6));
 indNS=bAgent.indexOf("netscape");
 verNS=bAgent.charAt(indNS+9);
 if (!isInteger(verNS)) verNS=parseInt(bAgent.charAt(indNS+8));
 else verNS=parseInt(verNS);
 var FF = (bAgent.indexOf("firefox")!=-1);
 var GB = (bAgent.indexOf("gecko")!=-1);
 var OP = (indOP!=-1);
 var OP7 = (indOP!=-1 && verOP<8);
 var OP8 = (indOP!=-1 && verOP>=8);
 var NS = (indNS!=-1);
 var NS4 = (indNS!=-1 && verNS < 5);
 var NS6 = (indNS!=-1 && verNS >= 6);
 var IE = (bVer >= 4 && bAgent.indexOf("msie")!=-1 && !OP);
 var IE7 = (bVer >= 4 && bAgent.indexOf("msie 7")!=-1 && !OP);
 var IE6 = (bVer >= 4 && bAgent.indexOf("msie 6")!=-1 && !OP);
 var IE5 = (bVer >= 4 && bAgent.indexOf("msie 5")!=-1 && !OP);
 var IE4 = (bVer < 4 && bAgent.indexOf("msie")!=-1 && !OP); 
 if (NS4 || IE4) alert ("Thank You for visiting SmartWebby.com! Your browser cannot view our website properly due to its inability to fully support CSS. Please upgrade it to the latest version or try viewing our pages with another browser. Thanks for your co-operation!");
function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
		if (((c < "0") || (c > "9"))) return false;
    }
    return true;
	

}
// SET BACKGROUND Image 
function setBgImage(obj, bit){
	id=obj.id;
	if (bit==1){	
		if (NS6 || IE || OP || FF || GB) eval('document.getElementById("'+id+'").style.backgroundPosition="0 -28px"');
		else eval('document.all.'+id+'.style.backgroundPosition="0 -28px"');
	}
	else {	
		if (NS6 || IE || OP || FF || GB) eval('document.getElementById("'+id+'").style.backgroundPosition="0 0"');
		else eval('document.all.'+id+'.style.backgroundPosition="0 0"');
	}
}
var arrImg=new Array;
function preloadImages(img,i) {
  var d=document; 
  if(d.images){
    var x,iName,temp;
	if (img.indexOf(".")!=-1){
		iName=img.substring(img.lastIndexOf("/")+1,img.lastIndexOf(".")); 
		arrImg[i]=new Image; 
		arrImg[i].src=img;
		arrImg[i].name=iName;
	}
  }
}

var photos=new Array();
var iWidth=150;
var iHeight=75;

//Paths to Photos:
photos[0]="images/rightswap0.jpg";
photos[1]="images/rightswap1.jpg";
photos[2]="images/rightswap2.jpg";
photos[3]="images/rightswap3.jpg";
photos[4]="images/rightswap4.jpg";
photos[5]="images/rightswap5.jpg";
photos[6]="images/rightswap6.jpg";
photos[7]="images/rightswap7.jpg";
photos[8]="images/rightswap8.jpg";
photos[9]="images/rightswap9.jpg";
photos[10]="images/rightswap10.jpg";
photos[11]="images/rightswap11.jpg";
photos[12]="images/rightswap12.jpg";
photos[13]="images/rightswap13.jpg";
photos[14]="images/rightswap14.jpg";	
photos[15]="images/rightswap15.jpg";
photos[16]="images/rightswap16.jpg";
photos[17]="images/rightswap17.jpg";
photos[18]="images/rightswap18.jpg";

//Photo Album Functions:
function applyeffect(){
	if (document.all && photo_slideshow.filters)
	{
		photo_slideshow.filters.revealTrans.Transition=Math.floor(Math.random()*23);
		photo_slideshow.filters.revealTrans.stop();
		photo_slideshow.filters.revealTrans.apply();
		document.images.photo_slideshow.src=arrImg[cursor].src;
		document.images.photo_slideshow.name=arrImg[cursor].name;
		photo_slideshow.filters.revealTrans.play();
	}
		document.images.photo_slideshow.src=arrImg[cursor].src;
}

function next_photo(){
	if(blnRound2==true && cursor>=iRnd) return;
	else if (cursor<photos.length-1) cursor++;
	else {
		blnRound2=true;
		cursor=0;
	}
	applyeffect();
	play_album();
}

function play_album() {
	timeOn = setTimeout("next_photo()",4000);
}

var blnRound2=false;
var cursor=0;
var iRnd=Math.floor(Math.random()*photos.length);
for (i=0;i<photos.length;i++){
	preloadImages(photos[i],i);
}

var intDivWidth=520
var intLeft=3
var intTop=22

//font color, font hilight color, background color, hilight background color
var hexFColor="#000000"
var hexFHColor="#ffffff"
var hexBgColor="#99CC33"
var hexBgHColor="#6699CC"

function divMenu(img){
	return imgPos(img)-imgPos('services')+intLeft;
}
//-->