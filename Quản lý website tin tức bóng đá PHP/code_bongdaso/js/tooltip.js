function mouseX(evt) {if (!evt) evt = window.event; if (evt.pageX) return evt.pageX; else if (evt.clientX)return evt.clientX + (document.documentElement.scrollLeft ?  document.documentElement.scrollLeft : document.body.scrollLeft); else return 0;} 
function mouseY(evt) {if (!evt) evt = window.event; if (evt.pageY) return evt.pageY; else if (evt.clientY)return evt.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop); else return 0;} 
function pageWidth() {return window.innerWidth != null? window.innerWidth: document.body != null? document.body.clientWidth:null;} 
function pageHeight() {return window.innerHeight != null? window.innerHeight: document.body != null? document.body.clientHeight:null;} 
function posLeft() {return typeof window.pageXOffset != 'undefined' ? window.pageXOffset:document.documentElement.scrollLeft? document.documentElement.scrollLeft:document.body.scrollLeft? document.body.scrollLeft:0;} 
function posTop() {return typeof window.pageYOffset != 'undefined' ? window.pageYOffset:document.documentElement.scrollTop? document.documentElement.scrollTop: document.body.scrollTop?document.body.scrollTop:0;} 
function posRight() {return posLeft()+pageWidth();} 
function posBottom() {return posTop()+pageHeight();} 
                    
var cX = 0; var cY = 0;
var tooltipObj = '';
function UpdateCursorPosition(evt)
{ 
	cX = mouseX(evt);
	cY = mouseY(evt);
	if (tooltipObj!='')
		AlignTooltip();
}
function AlignTooltip()
{
    var it = document.getElementById(tooltipObj);
    if (!it)
		return;
    x=cX+3;
    y=cY+2;    
    if (cY+it.offsetHeight>posBottom()-20)
    {		
		y=cY-it.offsetHeight-2;
	}
    it.style.top = y + 'px';
    it.style.left = x + 'px';        
}

document.onmousemove = UpdateCursorPosition;


function xstooltip_show(tooltipId, parentId)
{
    var it = document.getElementById(tooltipId);
    if (!it)
		return;
	tooltipObj = tooltipId;
    if ((it.style.top == '' || it.style.top == 0) 
        && (it.style.left == '' || it.style.left == 0))
    {
        // need to fixate default size (MSIE problem)
        it.style.width = it.offsetWidth + 'px';
        it.style.height = it.offsetHeight + 'px';
        
        img = document.getElementById(parentId); 
            
		x=cX+3;
		y=cY+2;    
		if (cY+it.offsetHeight>posBottom()-20)
		{		
			y=cY-it.offsetHeight-2;
		}
        it.style.top = y + 'px';
        it.style.left = x + 'px';        
    }
    
    it.style.visibility = 'visible'; 
}

function xstooltip_show2(tooltipId, parentId, toparrowId, bottomarrowId)
{
    var it = document.getElementById(tooltipId);
    if (!it)
		return;
	tooltipObj = tooltipId;
    if ((it.style.top == '' || it.style.top == 0) 
        && (it.style.left == '' || it.style.left == 0))
    {
        // need to fixate default size (MSIE problem)
        it.style.width = it.offsetWidth + 'px';
        it.style.height = it.offsetHeight + 'px';
        
        img = document.getElementById(parentId); 
        
		var topArrowNode = document.getElementById(toparrowId);
		var bottomArrowNode = document.getElementById(bottomarrowId);
            
		x=cX+3;
		y=cY+2;    
		if (cY+it.offsetHeight>posBottom()-20)
		{		
			y=cY-it.offsetHeight-2;
			topArrowNode.style.display = "none";
			bottomArrowNode.style.display = "inline";	
		}
		else
		{
			topArrowNode.style.display = "inline";
			bottomArrowNode.style.display = "none";			
		}
        it.style.top = y + 'px';
        it.style.left = x + 'px';        
    }
    
    it.style.visibility = 'visible'; 
}

function xstooltip_show_fixed(tooltipId, parentId, posX, posY)
{
    var it = document.getElementById(tooltipId);
    if (!it)
		return;
    
    if ((it.style.top == '' || it.style.top == 0) 
        && (it.style.left == '' || it.style.left == 0))
    {
        // need to fixate default size (MSIE problem)
        it.style.width = it.offsetWidth + 'px';
        it.style.height = it.offsetHeight + 'px';
        
        img = document.getElementById(parentId); 
            
        x = findPosX(img) + img.offsetWidth + posX;
        y = findPosY(img) + posY;
        
        it.style.top = y + 'px';
        it.style.left = x + 'px';
    }
    
    it.style.visibility = 'visible'; 
}

function xstooltip_hide(id)
{
    var it = document.getElementById(id); 
    if (!it)
		return;
	tooltipObj = '';
    it.style.visibility = 'hidden'; 
}

