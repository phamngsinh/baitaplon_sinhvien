<?php

function deleteNumber($str)
{
	$str = str_replace("0", "", $str);
	$str = str_replace("1", "", $str);
	$str = str_replace("2", "", $str);
	$str = str_replace("3", "", $str);
	$str = str_replace("4", "", $str);
	$str = str_replace("5", "", $str);
	$str = str_replace("6", "", $str);
	$str = str_replace("7", "", $str);
	$str = str_replace("8", "", $str);
	$str = str_replace("9", "", $str);
	return $str;
}
//các file upload được để trong 1 thư mục riêng
$data_upload = './doc_upload/';
$upload_dir = $data_upload;

if($_GET['type'] == 'cv')
	$upload_dir = './cv_upload/';

//lấy tên file cần download từ URL
$filename = isset($_GET['name'])?$_GET['name']:'';

//thực hiện quá trình kiểm tra
$bOK = true;
if (( !preg_match('/^[a-z0-9\_\-][a-z0-9\_\-\. ]*$/i', $filename) )|| !is_file($upload_dir.$filename) || !is_readable($upload_dir.$filename) ) 
{
	$bOK = false;
	echo "<script type='text/javascript'> alert(\"He thong hien chua cap nhat an pham nay!\")</script>";
	echo "<script type='text/javascript'> history.go(-1);</script>";	
} //end if

//mở file để đọc với chế độ nhị phân (binary)
if($bOK)
{
	$fp = fopen($upload_dir.$filename, "rb");

	//gởi header đến cho browser
	header('Content-type: application/octet-stream');
	header('Content-disposition: attachment; filename="'.deleteNumber($filename).'"');
	header('Content-length: ' . filesize($upload_dir.$filename));

	//đọc file và trả dữ liệu về cho browser
	fpassthru($fp);
	fclose($fp); 
}
else
{
	exit(1);
}
?>