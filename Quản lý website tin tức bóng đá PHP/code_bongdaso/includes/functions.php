<?php
/******************************************************
Company: ECOPRO Co., Ltd
Project: SO XAY DUNG DIEN BIEN
Author: Vu Thanh Toan
Email: vttoan@gmail.com
Phone: 84913319908
Commen Day: 12/8/2006 9:38 AM
*******************************************************/
 
 /////////////////
 
function get_array_sub_id($id, $cat_table = "tblcat_info")
{
//Author: Written By Vu Thanh Toan. For reusing, please contact me via phone(84913319908) or mail(vttoan@gmail.com)
//Used for: Function for getting array id of sub node from id of node.
//Return Value: Return string of id of sub node

	$select = "SELECT * FROM ".$cat_table." WHERE parent_id = ".$id;
	
	$select = mysql_query($select) or die(mysql_error());
	$menu = '';
	while($sql = mysql_fetch_array($select))
	{
		$menu .= $sql['id'].", ";
		$menu .= get_array_sub_id($sql['id'], $cat_table);
	}
	return $menu;	
}
////////////
function get_root_id($id,$cat_table = "tblcat_info")
{
//Author: Written By Vu Thanh Toan. For reusing, please contact me via phone(84913319908) or mail(vttoan@gmail.com)
//Used for: Function for getting root id from id of node in tree
//Return Value: Return id of root

	$select = "SELECT * FROM ".$cat_table." WHERE id = ".$id;
	$select = mysql_query($select) or die(mysql_error());
	$menu = 0;
	while($sql = mysql_fetch_array($select))
	{
		$menu = $id;
		if($sql['parent_id'] != '' && $sql['parent_id'] != 0)
			$menu = (int)$sql['parent_id'];
		
		$temp = (int)get_root_id($sql['parent_id'], $cat_table);
		if($temp != '' && $temp != 0)
			$menu = (int)get_root_id($sql['parent_id'], $cat_table);
	}
	return $menu;	
}

function check_select($number, $nember_array)
{//Ham thuc hien kiem tra cac quyen cua user. Written by Vu Thanh Toan//
	foreach ($nember_array as $authority_id )
	{
		if($authority_id == $number)return true;
	}
	return false;
}


// Kiem tra form them moi administrator
function checkNewUser(&$bOK, &$strMsg)
{
	global $strUsername;
	if ( !empty($strUsername) )
	{
		$bOK = true;
	}
	else
	{
		$bOK = false;
		$strMsg = 'Tên đăng nhập không được để trống.';
	}
}

// Upload cac loai file len server
function upload_files_doc(&$file_name, &$file_type, $file_tmp, $dir_upload)
{
	$date = getdate();	
	
	$time_update = $date[0];
	$file_name = $time_update;
	
	//$file_type = substr($file_type, -strpos($file_type, '/'));

	switch ($file_type) {
		case 'text/plain':
			$file_type = '.txt';
		break;
		
		case 'application/msword':
			$file_type = '.doc';
		break;
		
		case 'application/vnd.ms-excel':
			$file_type = '.xls';
		break;
		
		case 'application/x-zip-compressedp':
			$file_type = '.zip';
		break;		
	}
	
	if ( !is_dir($dir_upload) )
	{
		//echo 'Can not found the folder: <b>'.$dir_upload.'</b>';		
		return false;
	}
	
	if ( $file_name == '' or !$file_name or $file_name == 'none')
	{
		//echo 'Can not found the file';
		//exit;
		return false;
	}
	
	// Copy file can upload len 1 thu muc tren SERVER
	chmod($dir_upload, 0777);
	if ( !@move_uploaded_file($file_tmp, $dir_upload . '/' . $file_name . $file_type) )
	{
		//echo 'Upload failed';
		//exit;
		return false;
	}
	else
		@chmod($dir_upload . '/' . $file_name . $file_type , 0777);
	chmod($dir_upload, 0755);
	return true;
}

// UpLoad file anh len Server
function upload_files(&$file_name, &$file_type, $file_tmp, $dir_upload)
{
	$date = getdate();	
	
	$time_update = $date[0];
	$file_name .= $time_update;
	
	$file_type = substr($file_type, -strpos($file_type, '/'));

	switch ($file_type) {
		case 'jpeg':
		case 'pjpeg':
		case 'jpg':
			$file_type = '.jpg';
		break;
		
		case 'e/gif':
			$file_type = '.gif';
		break;
		
		case 'e/pgn':
			$file_type = '.pgn';
		break;
		
		case 'e/bmp':
			$file_type = '.bmp';
		break;		
	}
	
	if ( !is_dir($dir_upload) )
	{
		echo 'Can not found the folder: <b>'.$dir_upload.'</b>';		
	}
	
	if ( $file_name == '' or !$file_name or $file_name == 'none')
	{
		echo 'Can not found the file';
		exit;
	}
	
	// Copy file can upload len 1 thu muc tren SERVER
	chmod($dir_upload, 0777);
	if ( !@move_uploaded_file($file_tmp, $dir_upload . '/' . $file_name . $file_type) )
	{
		echo 'Upload failed';
		exit;
	}
	else
		@chmod($dir_upload . '/' . $file_name . $file_type , 0777);
	chmod($dir_upload, 0755);
}


// Xoa file anh tren server
function delete_files($file_name, $dir_upload) {
	if ( file_exists( $dir_upload . '/' . $file_name) ) {
		unlink( $dir_upload . '/' . $file_name);
	}
	return;
}

// Kiem tra cau truc email nhap vao
function is_email($EmailAddr)
{
	if ( ! preg_match('/^[a-z0-9&\'\.\-_\+]+@[a-z0-9\-]+\.([a-z0-9\-]+\.)*?[a-z]+$/is', $EmailAddr))
		return false;
	else return true;
}

/////////
//Written By Vu Thanh Toan
function transform_date($date_)
{
	$day = substr($date_, 0,2);
	$mon = substr($date_, 3,2);
	$year = substr($date_, 6,4);
	$date_change = $year."-".$mon."-".$day;
	return $date_change;
}
//Written By Vu Thanh Toan
function transform_date_back($date_)
{
	$year = substr($date_, 0,4);
	$mon = substr($date_, 5,2);
	$day = substr($date_, 8,2);
	$hour = substr($date_, 11,2);
	$minutes = substr($date_, 14,2);
	$gi = substr($date_, 17,2);
	$date_change = $day."-".$mon."-".$year." ".$hour.":".$minutes;
	return $date_change;
}
//Written By Vu Thanh Toan
function transform_date_back_only($date_)
{
	$year = substr($date_, 0,4);
	$mon = substr($date_, 5,2);
	$day = substr($date_, 8,2);
	
	$date_change = $day."-".$mon."-".$year;
	return $date_change;
}
function transform_phone_back_only($phone_)
{
	$ma = substr($phone_, 1,2);
	
	$phone_change = $ma;
	return $phone_change;
}
function transform_mang_back_only($mang_)
{
	$mang = substr($mang_, 3,1);
	
	$mang_change = $mang;
	return $mang_change;
}
function transform_so_back_only($so_)
{
	$so = substr($so_, 4,8);
	
	$so_change = $so;
	return $so_change;
}

//stripslashes
//addslashes
//htmlspecialchars
//htmlspecialchars("<a href='test'>Test</a>", ENT_QUOTES);
//html_entity_decode

function convertHTML($strInput) //Convert to html special code
{
	//$strInput = htmlspecialchars($strInput, ENT_QUOTES);
	$strInput = str_replace('"', '&quot;', $strInput);
	$strInput = str_replace("'", "&#039;", $strInput);
	return $strInput;
}

function unconvertHTML($strInput)//Convert html special code to standart form
{
	//$strInput = html_entity_decode($strInput);
	$strInput = str_replace('&quot;', '"', $strInput);
	$strInput = str_replace("&#039;", "'", $strInput);
	return $strInput;
}

function insertData($strInput)//Use In inserting or updating data into database
{//Written By Vu Thanh Toan
	$strInput = addslashes(unconvertHTML($strInput));
	return $strInput;
}

function displayData_DB($strInput)
{//Written By Vu Thanh Toan
	$strInput = stripslashes($strInput);
	$strInput = str_replace(chr(10), '<br>', $strInput);
	return $strInput;
}

function displayData_DB_Content($strInput)
{//Written By Vu Thanh Toan
	$strInput = stripslashes($strInput);
//	$strInput = str_replace(chr(10), '<br>', $strInput);
	return $strInput;
}

function displayData_Textbox($strInput)
{//Written By Vu Thanh Toan
	$strInput = convertHTML(stripslashes($strInput));
	return $strInput;
}

function convertcharstoupper($str)
{//Written By Vu Thanh Toan
	return mb_convert_case($str, MB_CASE_UPPER, "UTF-8");
}

function cat_travel_law($id)
{//Written By Vu Thanh Toan. For reusing, please contact me via phone(84913319908) or mail(vttoan@gmail.com)
	$select = "SELECT * FROM tblcat_law WHERE parent_id = ".$id;
	$select = mysql_query($select) or die(mysql_error());
	$menu = "";
	while($sql = mysql_fetch_array($select))
	{
		$menu .= "'".$sql['id']."', ";
		$menu .= cat_travel_law($sql['id']);
	}
	return $menu;	
}

function private_unset()
{
	unset($_SESSION['sr_name']);
	unset($_SESSION['sr_start']);
	unset($_SESSION['sr_end']);
	unset($_SESSION['sr_type']);
	unset($_SESSION['sr_code']);
	unset($_SESSION['sr_area']);
	unset($_SESSION['sr_prime_key']);
	unset($_SESSION['sr_dept']);
	unset($_SESSION['sr_signer']);
	unset($_SESSION['sr_order']);
	unset($_SESSION['sr_numofpage']);
}


function check_injection($text, $level = 1)
{
	//Exam
	$text = strtolower($text);
	if(substr_count($text, 'insert into') > 0) $insert_exists = true;
	if(substr_count($text, 'update tbl') > 0) $update_exists = true;
	if(substr_count($text, 'update `tbl`') > 0) $update_exists = true;
	if(substr_count($text, 'delete from') > 0) $delete_exists = true;
	
	if(substr_count($text, 'drop database') > 0) return false;
	if(substr_count($text, 'drop table') > 0) return false;
	
	if(substr_count($text, 'insert into') > 1) return false;
	if(substr_count($text, 'update tbl') > 1) return false;
	if(substr_count($text, 'update `tbl`') > 1) return false;
	if(substr_count($text, 'delete from') > 1) return false;

	if($level == 1)
	{
		if($insert_exists)
		{
			return false;
		}
		elseif(delete_exists)
		{
			return false;
		}
		elseif($update_exists)
		{
			return false;
		}

	}
	else
	{
		if($insert_exists && $update_exists)
		{
			return false;
		}
		elseif($insert_exists && $delete_exists)
		{
			return false;
		}
		elseif($update_exists && $delete_exists)
		{
			return false;
		}	
		
	}
	return true;
}
function compareDate ($i_sFirstDate)
{
//Break the Date strings into seperate components

$arrFirstDate = explode ("-", $i_sFirstDate);

//$today = date("Y-n-d");
$today = "2007-03-01";
$arrToDay = explode ("-", $today);

$intFirstDay = (int)$arrFirstDate[2];
$intFirstMonth = (int)$arrFirstDate[1];
$intFirstYear = (int)$arrFirstDate[0];

$intSecondDay = (int)$arrToDay[2];
$intSecondMonth = (int)$arrToDay[1];
$intSecondYear = (int)$arrToDay[0];

 
// Calculate the diference of the two dates and return the number of days.


//$intDate1Jul = gregoriantojd($intFirstMonth, $intFirstDay, $intFirstYear);
//$intDate2Jul = gregoriantojd($intSecondMonth, $intSecondDay, $intSecondYear);
if($intFirstYear > $intSecondYear)
{
	return 1;
}
elseif($intFirstYear == $intSecondYear && $intFirstMonth > $intSecondMonth)
{
	 return 1;
}
elseif($intFirstYear == $intSecondYear && $intFirstMonth == $intSecondMonth && $intFirstDay > $intSecondDay)
{
	 return 1;
}
else
{
 return -1;
}



//return $intDate1Jul - $intDate2Jul;

}//end Compare Date
function merstrrever($str, $str1)
{
//Author: Written By Vu Thanh Toan. For reusing, please contact me via phone(84913319908) or mail(vttoan@gmail.com)
//Used for: Function for merrging two string to one string
//Return Value: Return compine string

	return $str1.$str;
}

function get_array_link($id, $cat_table = "tblcat_info", $act = "info")
{
//Author: Written By Vu Thanh Toan. For reusing, please contact me via phone(84913319908) or mail(vttoan@gmail.com)
//Used for: Function for getting url link from child node to root node on tree
//Return Value: Return string of url
	if(!isset($id)) $id = 0;
	$select = "SELECT * FROM ".$cat_table." WHERE id = ".$id;
	$select = mysql_query($select) or die(mysql_error());
	//echo $id."-";
	$menu = '';
	while($sql = mysql_fetch_array($select))
	{
		$select1 = "SELECT * FROM ".$cat_table." WHERE id = ".$sql['parent_id'];
		$select1 = mysql_query($select1) or die(mysql_error());
		while($sql1 = mysql_fetch_array($select1))
		{
			$menu = "<a href='./?act=".$act."&cat_id=".$sql1['id']."'><font class='textes10' color='#D03107'><b>".displayData_DB($sql1['name_vn'])."</b></font></a> <font class='textes11'> > </font> ";
			$menu = merstrrever($menu,get_array_link($sql1['id'], $cat_table, $act));
		}
	}
	/*
	// 21/06/07
	// Them thuoc tinh class =Function vao phan function dang duoc hien thi
	// Lamlh
	*/
	return $menu;	
}
function get_array_linken($id, $cat_table = "tblcat_info", $act = "info")
{
//Author: Written By Vu Thanh Toan. For reusing, please contact me via phone(84913319908) or mail(vttoan@gmail.com)
//Used for: Function for getting url link from child node to root node on tree
//Return Value: Return string of url
	if(!isset($id)) $id = 0;
	$select = "SELECT * FROM ".$cat_table." WHERE id = ".$id;
	$select = mysql_query($select) or die(mysql_error());
	//echo $id."-";
	$menu = '';
	while($sql = mysql_fetch_array($select))
	{
		$select1 = "SELECT * FROM ".$cat_table." WHERE id = ".$sql['parent_id'];
		$select1 = mysql_query($select1) or die(mysql_error());
		while($sql1 = mysql_fetch_array($select1))
		{
			$menu = "<a href='./?act=".$act."&cat_id=".$sql1['id']."'><font class='textes10' color='#D03107'><b>".displayData_DB($sql1['name_en'])."</b></font></a> <font class='textes11'> > </font> ";
			$menu = merstrrever($menu,get_array_linken($sql1['id'], $cat_table, $act));
		}
	}
	/*
	// 21/06/07
	// Them thuoc tinh class =Function vao phan function dang duoc hien thi
	// Lamlh
	*/
	return $menu;	
}
function display_tree($id, $cat_table, &$template, $parent_id, $block = "GROUP", $type="")
{
//Author: Written By Vu Thanh Toan. For reusing, please contact me via phone(84913319908) or mail(vttoan@gmail.com)
//Used for: Function for displaying data in tree form.
//Return Value: Return No value.
	$select = "SELECT * FROM ".$cat_table." WHERE parent_id = ".$id.$type." AND name_vn <> '' ORDER BY priority_order DESC, id DESC";
	$select = mysql_query($select) or die(mysql_error());
	$menu = '';
	$prex = "";
	while($sql = mysql_fetch_array($select))
	{
		while($sql['level'])
		{
			if($sql['level'] > 1)
				$prex .= "&nbsp;&nbsp;&nbsp;";
			else
				$prex .= "|__";
			$sql['level'] --;	
		}
		$template->assign_block_vars($block, array(
			'id'  => $sql['id'],
			'name' 	=> $prex.displayData_Textbox($sql['name_vn']),
			'_selected' => ($sql['id'] == $parent_id)?'selected':''	
		));
		display_tree($sql['id'], $cat_table, $template, $parent_id, $block, $type);
		$prex = "";
	}
	
}
function display_tree_en($id, $cat_table, &$template, $parent_id, $block = "GROUP", $type="")
{
//Author: Written By Vu Thanh Toan. For reusing, please contact me via phone(84913319908) or mail(vttoan@gmail.com)
//Used for: Function for displaying data in tree form.
//Return Value: Return No value.
	$select = "SELECT * FROM ".$cat_table." WHERE parent_id = ".$id.$type." AND name_en <> '' ORDER BY priority_order DESC, id DESC";
	$select = mysql_query($select) or die(mysql_error());
	$menu = '';
	$prex = "";
	while($sql = mysql_fetch_array($select))
	{
		while($sql['level'])
		{
			if($sql['level'] > 1)
				$prex .= "&nbsp;&nbsp;&nbsp;";
			else
				$prex .= "|__";
			$sql['level'] --;	
		}
		$template->assign_block_vars($block, array(
			'id'  => $sql['id'],
			'name' 	=> $prex.displayData_Textbox($sql['name_en']),
			'_selected' => ($sql['id'] == $parent_id)?'selected':''	
		));
		display_tree($sql['id'], $cat_table, $template, $parent_id, $block, $type);
		$prex = "";
	}
	
}
?>