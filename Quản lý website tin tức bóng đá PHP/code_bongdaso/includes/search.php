<?php

$template -> set_filenames(array('search'	=> $dir_template . 'search.tpl'));

// Lay cac bien _POST va _GET
if(isset($_POST['txt_keyword']))
{
	$txt_keyword = isset($_POST['txt_keyword'])?$_POST['txt_keyword']:'';
}
else
{
	$txt_keyword = isset($_GET['txt_keyword'])?$_GET['txt_keyword']:'';
}
$ra_searchin = isset($_POST['ra_searchin'])?$_POST['ra_searchin']:0;
$cbDocType = isset($_POST['cbDocType'])?$_POST['cbDocType']:'';
$cbNewsType = isset($_POST['cbNewsType'])?$_POST['cbNewsType']:'';

// Neu bam vao mot van ban de xem chi tiet hoac bam chuyen trang thi se lay cac bien sau

$doc_id = isset($_GET['id'])?$_GET['id']:'';
$doccat_id = isset($_GET['doccat_id'])?$_GET['doccat_id']:'';
$txt_keyword = $txt_keyword==''?(isset($_GET['keyword'])?$_GET['keyword']:''):$txt_keyword;
$ra_searchin = isset($_GET['searchin'])?$_GET['searchin']:$ra_searchin;
$cbDocType = $cbDocType==''?(isset($_GET['doctype'])?$_GET['doctype']:''):$cbDocType;
$cbNewsType	= $cbNewsType==''?(isset($_GET['newstype'])?$_GET['newstype']:''):$cbNewsType;
	
display_tree(0, 'tblcat_info', $template, $cbDocType, "CATNEWS", "");

$template -> assign_vars(array(
		'keyword'		=> $txt_keyword,
		'txt_keyword'	=> 'Từ khóa',
		'txt_search_in'	=> 'Loại',
		'cbnewstypedisplay'	=> $ra_searchin==0?'':'none',
		'cbdoctypedisplay'	=> $ra_searchin==1?'':'none',
		'checked_news'	=> $ra_searchin==0?'checked':'',
		'checked_docs'	=> $ra_searchin==1?'checked':'',
		'txt_news'		=> "Tin tức",
		'txt_helplogin'	=> 'Bạn hãy đăng nhập để có thể xem được thông tin',
		'txt_helpsearch'=> 'Hãy nhập từ khóa để tìm các loại văn bản tại đây',
		'txt_catdoc'	=> 'Danh mục ảnh',
		'txt_catnews'	=> 'Loại tin tức',
		'txt_browsing' 	=> 'Đang duyệt: ',
		'function'=>' <b>Tìm kiếm nâng cao</b>',
		'txt_result'	=> 'Kết quả tìm kiếm',
		'txt_order' 	=> 'STT',
		'txt_name' 		=> 'Tin tức',
		'txt_update' 	=> 'Cập nhật',
		'img_search' 	=> 'search_vn.gif',
		'img_submit'	=> 'login_vn.gif',
));

$sql_search = "SELECT count(*) FROM tblinfo WHERE title_vn <> '' AND title_vn LIKE '%$txt_keyword%'".($cbNewsType!=''?" AND cat_id=$cbNewsType":'');
$sql_search = $db->sql_query($sql_search) or die(mysql_error());
$nRows = $db->sql_fetchrow($sql_search);
$nRows = $nRows[0];
if($nRows==0)
{
	$template -> assign_vars(array(
		'search_msg' => $typelang=='vn'?"Không tìm thấy kết quả phù hợp với yêu cầu!":($typelang=='en'?"No result suitable!":'')
	));
}

$p = isset($_GET['p']) ? $_GET['p'] : 0;
// Set lai $p neu $p >= $nRows
$p = ( ($p >= $nRows) || !is_numeric($p) || ($p < 0)) ? 0 : $p;
// So trang dang hien thi
$link_current_page = ($p > 0) ? '&p=' . $p : '';
$nNewsSize = 20;

$sql_search = "SELECT * FROM tblinfo WHERE title_vn <> '' AND title_vn LIKE '%$txt_keyword%'".($cbNewsType!=''?" AND cat_id=$cbNewsType":'')
			." ORDER BY create_time DESC";
$sql_search .= " LIMIT $p, $nNewsSize";
$sql_search = $db->sql_query($sql_search) or die(mysql_error());
$nCounter = 0;
while ( $doc_rows = $db->sql_fetchrow($sql_search))
{
	$nCounter++;
	
		$template -> assign_block_vars('DOC', array(
			'order'				=> $p+$nCounter,
			'act'				=> './?act=info&cat_id='.$doc_rows['cat_id'].'&id='.$doc_rows['id'].'',
			'doc_name'			=> displayData_DB($doc_rows["title_vn"]),
			'date'				=> transform_date_back_only($doc_rows['update_time']),
		));
	
}
// Hien thi phan trang
$url = "./?act=search&keyword=$txt_keyword&searchin=$ra_searchin&doctype=$cbDocType&newstype=$cbNewsType&doccat_id=$doccat_id";
$page = generate_pagination($url, $nRows, $nNewsSize, $p);
$template -> assign_vars(array(
	'txt_page'	=> $page
));
//*******************
$template -> pparse('search');
?>