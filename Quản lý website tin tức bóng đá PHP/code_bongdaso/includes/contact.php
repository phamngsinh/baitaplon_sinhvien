<?php
$template -> set_filenames(array(
	'contact'	=> $dir_template . 'contact.tpl')
);
if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'vn')
{
	$template -> assign_vars(array(
		'txt_browsing'	=> 'Đang duyệt:&nbsp;',
		'function'	=> '<b>Liên hệ</b>',
		'txt_company_name' => '<br><br>VIỆN ĐỊA CHẤT - Viện khoa học và công nghệ Việt Nam',
		'txt_company_info' => 'Địa chỉ : 84 phố Chùa Láng, Láng Thượng, Đống Đa, Hà Nội<br>Điện thoại: (+84.4) 774 4798<br>Fax: (+84.4) 774 4797<br>Email :contact@igsvn.ac.vn<br>Website: www.igsvn.ac.vn',
		'txt_please' => '<br>Bạn vui lòng liên hệ với VIỆN ĐỊA CHẤT thông qua địa chỉ trên <br>hoặc qua form liên hệ sau',
		'txt_name'	=> 'Họ tên: ',
		'txt_phone'	=> 'Điện thoại: ',
		'txt_add'	=> 'Địa chỉ: ',
		'txt_content'	=> 'Nội dung: ',
		'check_name' => 'Bạn hãy nhập đầy đủ họ tên!',
		'check_phone' => 'Bạn chưa nhập số điện thoại!',
		'check_email_empty' => 'Bạn chưa nhập email!',
		'check_email' => 'Địa chỉ email không hợp lệ!',
		'check_reques' => 'Bạn chưa nhập nội dung liên hệ',
		'yes_no' 	=> 'Bạn có đồng ý gửi thông tin trên đến VIỆN ĐỊA CHẤT ?',
		'img_send' => 'submit_vn.gif',
		'img_reset' => 'reset_vn.gif',		
	));
}
else // Ngon ngu tieng Anh
{
	$template -> assign_vars(array(
		'txt_browsing'	=> 'Browsing:&nbsp;',
		'function'=>'<b>Contact</b>',
		'txt_company_name' => ' VIEN DIA CHAT TRADING AND ENGINEERING SERVICES, LTD',
		'txt_company_info' => 'Address : 84 phố Chùa Láng, Láng Thượng, Đống Đa, Hà Nội. <br>Phone: (+84.4) 688 7980<br>Fax: (+84.4) 774 4798<br>Fax: (+84.4) 774 4797<br>Email :contact@igsvn.ac.vn<br>Website: www.igsvn.ac.vn',
		'txt_please' => 'Please contact us with infomation above, <br>or fill out the contacting forms below:',
		'txt_name'	=> 'Full Name: ',
		'txt_phone'	=> 'Phone: ',
		'txt_add'	=> 'Address: ',
		'txt_content'	=> 'content: ',
		'check_name' => 'please writed full name!',
		'check_phone' => 'please writed phone!',
		'check_email_empty' => 'please writed email!',
		'check_email' => 'Địa chỉ email không hợp lệ!',
		'check_reques' => 'please writed content contact us',
		'yes_no' 	=> ' You want concur send content to VIEN DIA CHAT ?',
		'img_send' => 'submit_en.gif',
		'img_reset' => 'reset_en.gif',
	));
}

if(isset($_POST['submit_x']))
{
	$strFullname	= $HTTP_POST_VARS['txtFullName'];
	$strAddress 	= $HTTP_POST_VARS['txtAdd'];
	$strEmail 		= $HTTP_POST_VARS['txtEmail'];
	$strTel			= $HTTP_POST_VARS['txtPhone'];
	$strContent		= $HTTP_POST_VARS['txtContent'];
	
	$mail_content = '<br> My Information: <br><br>';
	$mail_content .= 'Full Name:'.$strFullname.'<br>';
	$mail_content .= 'Email: '.$strEmail.'<br>';
	$mail_content .= 'Address:'.$strAddress.'<br>';
	$mail_content .= 'Phone Number: '.$strTel.'<br>';
	$mail_content .= 'Content of this contact: '.$strContent.'<br>';
	// Send mail
	$to = 'vongoc_an@yahoo.com';

	$headers  = "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=\"utf-8\"\r\n";
	$headers .= "From: " . $strEmail;
	mail($to, 'Contact', $mail_content, $headers);
}
$template -> pparse('contact');
?>