<?php

  $max_size = 500000;
  
  include ('include_fns.php');
  session_start();

  // kiem tra user co phai admin (chi admin moi co the upload file)
  if(!check_admin_user())
  {
    echo 'You do not seem to be authorized to use this page.';
    exit;
  }
  
  // thiet lap cac chuc nang cua admin
  $buttons = array();
  $buttons[0] = 'change-password';
  $buttons[1] = 'create-list';
  $buttons[2] = 'create-mail';
  $buttons[3] = 'view-mail';
  $buttons[4] = 'log-out';
  $buttons[5] = 'show-all-lists';
  $buttons[6] = 'show-my-lists';
  $buttons[7] = 'show-other-lists';
  
  do_html_header('Newsletters - Upload Files');
  
  display_toolbar($buttons);

  // kiem tra viec thuc hien cac yeu cau
  if(!$HTTP_POST_FILES['userfile']['name'][0] 
     ||!$HTTP_POST_FILES['userfile']['name'][1]
     ||!$HTTP_POST_VARS['subject']||!$HTTP_POST_VARS['list'])
  {
      echo 'Loi: Ban khong dien day du cac truong. Moi mot tin can mot file Text, mot
      mot file HTML va hinh anh (neu co).';
      do_html_footer();
      exit;
  }
  $list = $HTTP_POST_VARS['list'];
  $subject = $HTTP_POST_VARS['subject'];

  if(!db_connect())
  {
     echo '<p>Khong the ket noi den CSDL</p>';
     do_html_footer();
     exit;
  }
  
  // luu thong tin mail vao CSDL
  $query = "insert into mail values (NULL, 
                                     '".$HTTP_SESSION_VARS['admin_user']."',
                                     '".$subject."',
                                     '".$list."',
                                     'STORED', NULL, NULL)";
  $result = mysql_query($query);
  if(!$result)   
  {   
    do_html_footer();  
    exit; 
  }
  
  //nhan id MySQL gan cho thu chon
  $mailid = mysql_insert_id();
        
  if(!$mailid)   
  {   
    do_html_footer();  
    exit; 
  }

  @ mkdir('archive/'.$list, 0700);

  if(!mkdir('archive/'.$list."/$mailid", 0700))
  {   
    do_html_footer();  
    exit; 
  }

  $i = 0;
  while ($HTTP_POST_FILES['userfile']['name'][$i]&&
         $$HTTP_POST_FILES['userfile']['name'][$i]!='none')
  {
    echo '<p>Da upload '.$HTTP_POST_FILES['userfile']['name'][$i].' - ';
    echo $HTTP_POST_FILES['userfile']['size'][$i].' bytes.</p>';
    if ($HTTP_POST_FILES['userfile']['size'][$i]==0)
    {
      echo 'Loi: File'.$HTTP_POST_FILES['userfile']['name'][$i].
           ' rong';
      $i++;
      continue;  
    }
  
    if ($HTTP_POST_FILES['userfile']['size'][$i]>$max_size)
    {
      echo 'Loi: File '.$HTTP_POST_FILES['userfile']['name'][$i].' qua lon'
            .$max_size.' bytes';
      $i++;
      continue;  
    }

    // kiem tra file anh bang ham getimagesize()
    if($i>1&&!getimagesize($HTTP_POST_FILES['userfile']['tmp_name'][$i]))
    {
      echo 'Loi: File '.$HTTP_POST_FILES['userfile']['name'][$i].
           ' khong phai la mot file anh';
      $i++;
      continue;  
    }
  
    // file 0 cho text and file 1 cho html
    if($i==0) 
      $destination = "archive/$list/$mailid/text.txt";
    else if($i == 1)
      $destination = "archive/$list/$mailid/index.html";
    else
    {
      $destination = "archive/$list/$mailid/"
                     .$HTTP_POST_FILES['userfile']['name'][$i];
      $query = "insert into images values ($mailid, 
                             '".$HTTP_POST_FILES['userfile']['name'][$i]."',
                             '".$HTTP_POST_FILES['userfile']['type'][$i]."')";
      $result = mysql_query($query);
    }
    if (!is_uploaded_file($HTTP_POST_FILES['userfile']['tmp_name'][$i]))
    { 
      echo 'Co loi voi File '
           .$HTTP_POST_FILES['userfile']['name'].', khong upload duoc.';
      do_html_footer();
      exit;
    }
    
    move_uploaded_file($HTTP_POST_FILES['userfile']['tmp_name'][$i], 
                       $destination);
    $i++;
  }
  
  display_preview_button($list, $mailid, 'preview-html');
  display_preview_button($list, $mailid, 'preview-text');
  display_button('send', "&id=$mailid");
  
  echo '<br /><br /><br /><br /><br />';
  do_html_footer();
?>  
