<?php
 
// No direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
// Require the base controller
 
require_once( JPATH_COMPONENT.DS.'controller.php' );
@include_once(JPATH_COMPONENT.DS.'toolbar.html.php');
@include_once(JPATH_COMPONENT.DS.'toolbar.php');
JTable::addIncludePath(JPATH_COMPONENT.DS.'tables'); 
 
// Require specific controller if requested
if($controller = JRequest::getWord('controller')) {
    $path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
    if (file_exists($path)) {
        require_once $path;
    } else {
        $controller = '';
    }
}
$controllerName = JRequest::getCmd( 'task', '' );
if ($controllerName == 'listCat')
{
    JSubMenuHelper::addEntry(JText::_('Sản phẩm'), 'index.php?option=com_pplshop');
    JSubMenuHelper::addEntry(JText::_('Danh mục sản phẩm'), 'index.php?option=com_pplshop&task=listCat', true);
    JSubMenuHelper::addEntry(JText::_('Đơn đặt hàng'), 'index.php?option=com_pplshop&task=viewOrder'); 
}
else if ($controllerName == 'viewOrder' || $controllerName == 'orderDetail')
{
    JSubMenuHelper::addEntry(JText::_('Sản phẩm'), 'index.php?option=com_pplshop');
    JSubMenuHelper::addEntry(JText::_('Danh mục sản phẩm'), 'index.php?option=com_pplshop&task=listCat');
    JSubMenuHelper::addEntry(JText::_('Đơn đặt hàng'), 'index.php?option=com_pplshop&task=viewOrder', true); 
}else {
   JSubMenuHelper::addEntry(JText::_('Sản phẩm'), 'index.php?option=com_pplshop', true );
   JSubMenuHelper::addEntry(JText::_('Danh mục sản phẩm'), 'index.php?option=com_pplshop&task=listCat');
   JSubMenuHelper::addEntry(JText::_('Đơn đặt hàng'), 'index.php?option=com_pplshop&task=viewOrder');  
}
    
// Create the controller
$classname    = 'pplshopController'.$controller;
$controller   = new $classname( );
 
// Perform the Request task
$controller->execute( JRequest::getVar( 'task' ) );
 
// Redirect if set by the controller
$controller->redirect();
