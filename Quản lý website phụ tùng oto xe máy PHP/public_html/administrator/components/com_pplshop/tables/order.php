<?php

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Hello Table class
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class TableOrder extends JTable
{

    var $id = null;
    var $name = null;
    var $email = null;
    var $phone= null;
    var $city= null;
    var $address = null;
    var $total = null;  
    var $payment_menthod= null;  
    var $order_time= null; 
    var $ipaddress = null;
    var $name_rev = null; 
    var $email_rev= null;
    var $phone_rev = null;
    var $city_rev = null;
    var $address_rev= null;
    var $status = null;
    var $tax = null; 
    var $archive = null;   

    function TableOrder(& $db) {
        parent::__construct('#__pplshop_orders', 'id', $db);
    }
}