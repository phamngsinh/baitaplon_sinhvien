<?php
 
// No direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.model' );
require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'helpers'.DS.'images.php' ); 
 
/**
 * Hello Model
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class pplshopModelProduct extends JModel
{
    /**
    * Gets the greeting
    * @return string The greeting to be displayed to the user
    */
    function getListParent() {
        $query = "SELECT * FROM #__pplshop_category WHERE parentid = '0' ORDER BY ordering ASC";
        $this->_db->setQuery($query);
        $listParent = $this->_db->loadObjectList();
        return $listParent; 
    }
    
    function getChildCategory($parentid) {
        $query = "SELECT * FROM #__pplshop_category WHERE parentid = '$parentid' ORDER BY ordering ASC";
        $this->_db->setQuery($query);
        $listChild = $this->_db->loadObjectList();
        return $listChild;
    }
    
    function saveProduct(){
        $row = JTable::getInstance('Product', 'Table');
        $data = JRequest::get('POST');
        if(!$row->bind($data)) {
                echo "<script> alert('".$row->getError()."');
                window.history.go(-1); </script>\n";
                exit();  
        }

        if($row->created == null) {
           $row->created = date( 'Y-m-d H:i:s' );  
        }
        $row->description = JRequest::getVar('description', '', 'post', 'string', JREQUEST_ALLOWRAW);
        $images1 = JRequest::getVar('images1', null, 'files', 'array');
        $name1 = $this->change($data['product_name']).'-01';    
        if($images1['size'] != 0) {
            jimport('joomla.filesystem.file');  
            $imagename1 = JFile::makeSafe($images1['name']);
            $src = $images1['tmp_name'];
            $dest = JPATH_COMPONENT . DS . 'images'.DS.'products' . DS . $name1.'.'.JFile::getExt($imagename1);
          //  die ($dest);
            JFile::upload($src, $dest);
            $row->image1 = $name1.'.'.JFile::getExt($imagename1);        
        }
        $images2 = JRequest::getVar('images2', null, 'files', 'array');
        $name2 = $this->change($data['product_name']).'-02';    
        if($images2['size'] != 0) {
            jimport('joomla.filesystem.file');  
            $imagename2 = JFile::makeSafe($images2['name']);
            $src2 = $images2['tmp_name'];
            $dest2 = JPATH_COMPONENT . DS . 'images'.DS.'products' . DS . $name2.'.'.JFile::getExt($imagename2);
            
            JFile::upload($src2, $dest2);
            $row->image2 = $name2.'.'.JFile::getExt($imagename2);        
        }
        if(!$row->store()) {
               echo "<script> alert('".addslashes($this->_db->stderr())."');
                window.history.go(-1); </script>\n";
                exit(); 
        } else {
            return true;
        }
    }
    
    function change($text) {

        $text = str_replace(
        array(' ','%','/','\'','"','?','<','>',"#","^","`","'","=","!",":" ,",,","..","*","&","__","▄"),
        array(' ','' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'-','' ,'-','' ,'' ,'' , "_" ,"" ,""),
        $text);
       
        $chars = array("a","A","e","E","o","O","u","U","i","I","d", "D","y","Y");
       
        $uni[0] = array("á","à","ạ","ả","ã","â","ấ","ầ", "ậ","ẩ","ẫ","ă","ắ","ằ","ặ","ẳ","� �");
        $uni[1] = array("Á","À","Ạ","Ả","Ã","Â","Ấ","Ầ", "Ậ","Ẩ","Ẫ","Ă","Ắ","Ằ","Ặ","Ẳ","� �");
        $uni[2] = array("é","è","ẹ","ẻ","ẽ","ê","ế","ề" ,"ệ","ể","ễ");
        $uni[3] = array("É","È","Ẹ","Ẻ","Ẽ","Ê","Ế","Ề" ,"Ệ","Ể","Ễ");
        $uni[4] = array("ó","ò","ọ","ỏ","õ","ô","ố","ồ", "ộ","ổ","ỗ","ơ","ớ","ờ","ợ","ở","� �");
        $uni[5] = array("Ó","Ò","Ọ","Ỏ","Õ","Ô","Ố","Ồ", "Ộ","Ổ","Ỗ","Ơ","Ớ","Ờ","Ợ","Ở","� �");
        $uni[6] = array("ú","ù","ụ","ủ","ũ","ư","ứ","ừ", "ự","ử","ữ");
        $uni[7] = array("Ú","Ù","Ụ","Ủ","Ũ","Ư","Ứ","Ừ", "Ự","Ử","Ữ");
        $uni[8] = array("í","ì","ị","ỉ","ĩ");
        $uni[9] = array("Í","Ì","Ị","Ỉ","Ĩ");
        $uni[10] = array("đ");
        $uni[11] = array("Đ");
        $uni[12] = array("ý","ỳ","ỵ","ỷ","ỹ");
        $uni[13] = array("Ý","Ỳ","Ỵ","Ỷ","Ỹ");
       
        for($i=0; $i<=13; $i++) {
            $text = str_replace($uni[$i],$chars[$i],$text);
            $text = str_replace(' ', '_', $text);
        }

        return $text;
    }
    
    function getListProduct() {
        global $mainframe;
        jimport('joomla.html.pagination');
        $limit = JRequest::getVar('limit', 10, '', 'int'); 
        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
        
        $category_id = JRequest::getInt('category_id');
        $listId = $this->getAllCatId($category_id);  
        if($category_id == 0){
           $query = "SELECT * FROM #__pplshop_product";  
        } else {
            $query = "SELECT * FROM #__pplshop_product WHERE category_id IN $listId";
        }
        
        $this->_db->setQuery($query);
        $listProduct = $this->_db->loadObjectList();
        
        $total = sizeof($listProduct);
        $items['Nav'] = new JPagination($total, $limitstart, $limit);
        $items['listProduct'] = $this->_getList($query, $limitstart, $limit);
        $items['category_id'] = $category_id;
        
        return $items;
    }
    
    function getCatName($catid) {
        $query = "SELECT category_name FROM #__pplshop_category WHERE id = '$catid'";
        $this->_db->setQuery($query);
        $catName = $this->_db->loadResult();
        return $catName;
    }
    
    function getProductInfo() {
        $array = JRequest::getVar('cid',  0, '', 'array');
        $productid = $array[0];
        $query = "SELECT * FROM #__pplshop_product WHERE id = '$productid'";
        $this->_db->setQuery($query);
        $product = $this->_db->loadObject();
        return $product;
    }
    
    function delProduct() {
        $array = JRequest::getVar('cid',  0, '', 'array'); 
        $row = JTable::getInstance('Product', 'Table');     
        for ($i=0; $i<=count($array); $i++)  {
            $productid = $array[$i];

            $row->delete( $productid ) ;
            /*
             if (!$row->delete( $productid )) {
                $this->setError( $row->getErrorMsg() );
                return false;
            } else {
               
            }  */   
            
        }
        return true;
    }
    
    function getAllCatId($catid) {
        $query = "SELECT id FROM #__pplshop_category WHERE parentid = '$catid'";
        $this->_db->setQuery($query);
        $listId = $this->_db->loadResultArray();
        $list_temp = implode(',', $listId);
        if(count($listId) > 0) {
            $list = '('.$catid.','.$list_temp.')';
        } else if(count($listId == 0)) {
            $list = '('.$catid.')';
        }
        return $list; 
    }
   
}
