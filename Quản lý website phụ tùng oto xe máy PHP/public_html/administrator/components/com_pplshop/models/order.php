<?php
 
// No direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.model' );
require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'helpers'.DS.'images.php' ); 
 
/**
 * Hello Model
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class pplshopModelOrder extends JModel
{
    /**
    * Gets the greeting
    * @return string The greeting to be displayed to the user
    */   
    function getListOrder() {
        global $mainframe;
        jimport('joomla.html.pagination');
        $limit = JRequest::getVar('limit', 10, '', 'int'); 
        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');

           $query = "SELECT * FROM #__pplshop_orders ORDER BY order_time DESC";  

        
        $this->_db->setQuery($query);
        $listOrder = $this->_db->loadObjectList();
        
        $total = sizeof($listOrder);
        $items['Nav'] = new JPagination($total, $limitstart, $limit);
        $items['listOrder'] = $this->_getList($query, $limitstart, $limit);
        $items['category_id'] = $category_id;
        
        return $items;
    }
    
    function getCatName($catid) {
        $query = "SELECT category_name FROM #__pplshop_category WHERE id = '$catid'";
        $this->_db->setQuery($query);
        $catName = $this->_db->loadResult();
        return $catName;
    }
    
    function getOrderInfo() {
        $array = JRequest::getVar('cid',  0, '', 'array');
        $Orderid = $array[0];
        $queryup = "UPDATE #__pplshop_orders SET status = 1 WHERE (id = '$Orderid') AND (status = 0)";
        $this->_db->setQuery($queryup);
        $this->_db->query();
        $items = array();
        $query = "SELECT * FROM #__pplshop_orders WHERE id = '$Orderid'";
        $this->_db->setQuery($query);
        $items['order'] = $this->_db->loadObject();
        $query2 = "SELECT * FROM #__pplshop_order_details as a, #__pplshop_product as b WHERE a.prod_id = b.id AND  a.orderid = '$Orderid'";
        $this->_db->setQuery($query2);
        $items['order_detail'] = $this->_db->loadObjectList();
        return $items;
    }
    
    function delOrder() {
        $array = JRequest::getVar('cid',  0, '', 'array');
        $Orderid = $array[0];
        $row = JTable::getInstance('Order', 'Table');
        if (!$row->delete($Orderid )) {
            $this->setError( $row->getErrorMsg() );
            return false;
        } else {
            return true;
        }
    }
    

   
}
