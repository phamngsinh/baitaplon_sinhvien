-- phpMyAdmin SQL Dump
-- version 2.11.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 08, 2009 at 08:35 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `web_basic`
--

-- --------------------------------------------------------

--
-- Table structure for table `ifd_ip_category`
--

DROP TABLE IF EXISTS `ifd_ip_category`;
CREATE TABLE IF NOT EXISTS `ifd_ip_category` (
  `id` int(11) NOT NULL auto_increment,
  `category_name` varchar(250) NOT NULL,
  `parentid` int(11) NOT NULL default '0',
  `ordering` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `ifd_ip_category`
--

INSERT INTO `ifd_ip_category` (`id`, `category_name`, `parentid`, `ordering`) VALUES
(1, 'Điện Tử - Điện Lạnh', 0, 3),
(3, 'Thức Uống', 0, 2),
(10, 'Bia', 3, 1),
(5, 'Ti Vi', 1, 3),
(11, 'Nước Ngọt', 3, 2),
(14, 'Máy Tính - Điện Thoại', 0, 1),
(12, 'Tủ Lạnh', 1, 2),
(13, 'Máy Giặt', 1, 1),
(15, 'Máy Tính', 14, 1),
(16, 'Điện Thoại', 14, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ifd_ip_product`
--

DROP TABLE IF EXISTS `ifd_ip_product`;
CREATE TABLE IF NOT EXISTS `ifd_ip_product` (
  `id` int(11) NOT NULL auto_increment,
  `product_name` varchar(250) NOT NULL,
  `manufacturer` varchar(250) NOT NULL,
  `category_id` int(11) NOT NULL,
  `price` int(20) NOT NULL,
  `image1` varchar(250) NOT NULL,
  `image2` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL default '1',
  `created` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `ifd_ip_product`
--

INSERT INTO `ifd_ip_product` (`id`, `product_name`, `manufacturer`, `category_id`, `price`, `image1`, `image2`, `description`, `published`, `created`) VALUES
(2, 'Tủ Lạnh', '', 5, 3000000, '_big.jpg', '_small.jpg', 'tủ lạnh đời mới', 1, '0000-00-00 00:00:00'),
(3, 'Máy Tính', '', 3, 7500000, '0_big.jpg', '0_small.jpg', 'ddd', 1, '0000-00-00 00:00:00'),
(11, 'werqwear', '', 11, 3333, '', '', 'werqwe54r', 1, '0000-00-00 00:00:00'),
(6, '33333', '', 1, 0, '', '', '345et', 1, '0000-00-00 00:00:00'),
(7, '2222', '', 1, 454, '', '', '', 0, '0000-00-00 00:00:00'),
(8, 'Bia 333', '', 10, 15000, '_big.jpg', '_small.jpg', '3333', 0, '0000-00-00 00:00:00'),
(9, 'Tủ Lạnh Samsung', '', 12, 5, 'Tu_Lanh_Samsung_big.jpg', 'Tu_Lanh_Samsung_small.jpg', '<p>Tủ lạnh xịn,e</p>\r\n<p> </p>\r\n<p><strong>mới zing</strong></p>', 1, '0000-00-00 00:00:00'),
(10, 'Chảo Không Dính', '', 1, 3333333, 'Bep_ko_dinh_big.jpg', 'Bep_ko_dinh_small.jpg', '<p><strong>qư34s jjkl; </strong></p>\r\n<p> </p>\r\n<p> </p>\r\n<p><span style="background-color: #ffffff;"><span style="color: #ff0000;"><strong> jhk; sdxs</strong></span></span></p>', 1, '0000-00-00 00:00:00'),
(12, 'Máy In Cực vip', '', 1, 3333333, 'May_In_Vip_big.jpg', 'May_In_Vip_small.jpg', 'áertawer', 1, '2009-05-05 21:00:52'),
(13, 'HP Compaq 6530s', '', 15, 3333333, 'HP_big.jpg', 'HP_small.jpg', '<p><strong>HP Compaq 6530s là dòng laptop phổ thông 14,1 inch được hãng máy tính số một thế giới nhắm tới các doanh nghiệp nhỏ và vừa. Thế mạnh của dòng máy này nằm ở thế hệ CPU Core 2 Duo 45nm mới, card đồ họa rời, thiết kế độc đáo và giá bán hợp lý.</strong></p>\r\n<p> </p>\r\n<p class="Normal">HP Compaq 6530s được ứng dụng công nghệ di động Centrino 2 của Intel, với nhiều ưu điểm vượt trội dựa trên nền bộ vi xử lý Intel Core 2 Duo 45nm, chipset Intel PM45, hệ điều hành Windows Vista và kết nối mạng không dây Intel PRO/Wireless. Nhờ đó, máy vừa tiết kiệm điện, ít tỏa nhiệt trong khi vẫn đảm bảo hiệu suất làm việc. Bộ nhớ hệ thống có thể nâng cấp tối đa lên tới 8 GB, cao hơn hẳn so với các dòng laptop trước đây.</p>\r\n<p class="Normal">Khả năng đồ họa của 6530s cũng khá mạnh mẽ, bởi ngoài lựa chọn đồ họa tích hợp Intel GMA X4500 có khả năng giải mã tín hiệu HD, chiếc máy này còn được trang bị card rời ATI Radeon HD 3430, cho phép sử dụng thêm một phần bộ nhớ hệ thống để nâng cao năng lực đồ họa. Nhờ thế, HP Compaq 6530s xử lý tốt các hiệu ứng đồ họa Aero Glass của Windows Vista và đảm nhận hoàn hảo việc trình chiếu phim HD cũng như các ứng dụng 3D hạng nhẹ.</p>\r\n<p class="Normal">Ngoài ổ ghi DVD Dual Layer tích hợp sẵn, HP Compaq 6530s còn có lựa chọn ổ Blu-ray dành cho những người muốn thỏa mãn các nhu cầu giải trí cao cấp. Máy có các mức dung lượng ổ cứng 120, 160 và 250 GB (5400rpm) hoặc 120 GB (7200rpm), hỗ trợ công nghệ chống sốc 3D Guard, giúp bảo vệ dữ liệu lưu trong ổ trước những va đập mạnh. Ngoài ra, HP Compaq 6530s cũng được trang bị đầu đọc thẻ nhớ SD/MMC và ExpressCard.</p>', 1, '2009-05-05 09:28:14'),
(14, 'Asus F80S', 'Asus', 15, 14000000, 'Asus_F80S_big.jpg', 'Asus_F80S_small.jpg', '<p><strong>Asus F80S có nhiều cấu hình và mức giá, dao động từ 13 đến 15,5 triệu đồng. Đây là mẫu laptop 14 inch thiết kế hiện đại.</strong></p>\r\n<p>Asus F80S xuất hiện trên thị trường từ cuối năm ngoái và được đánh giá cao nhờ thiết kế trẻ trung, hiện đại.</p>\r\n<p>Vỏ máy màu đen nhẵn bóng với nhiều vân kẻ ô nhỏ và đường cong lớn giao thoa mềm mại ở chính giữa. Nhờ sử dụng công nghệ sơn bề mặt tân tiến Infusion Technology, Asus khẳng định vỏ máy bền hơn và có khả năng chống trầy xước. Nếu để ý kỹ một chút, người sử dụng chắc chắn sẽ nhận ra những điểm tương đồng trong thiết kế của F80s với những chiếc Pavilion dv của HP - bề mặt tráng bóng, dễ để lại dấu vân tay. Điều này khiến người dùng ít nhiều khó chịu vì sẽ phải “vệ sinh” máy thường xuyên hơn.</p>\r\n<p>Asus F80S có màn hình 14 inch, kích thước vừa phải, với các chiều rộng, sâu và dày tương ứng là 34 x 24 x 4 cm. Trọng lượng khá nhẹ 2,39 kg (cả pin). Bàn phím rất thoải mái khi sử dụng nhưng vẫn bị phàn nàn là các phím “mũi tên” quá nhỏ. Những phím quan trọng như “Ctrl trái”, “Alt trái” hay “Windows” cũng bị thu nhỏ trong khi 2 phím chuột phía rìa máy lại lớn hơn so với bình thường. Tuy nhiên, đánh giá chung, bàn phím của Asus F80S nhạy, và khi dùng có thể cảm nhận được chiều sâu tạo ra sự thoải mái khi dùng lâu.<br />Phần không gian quanh touchpad được trang trí với các họa tiết nhỏ. Ảnh: Asus.</p>\r\n<p>Trong khi phần không gian bao được trang trí với các họa tiết nhỏ lấm tấm nhám thì bề mặt bàn di chuột cảm ứng (touchpad) lại phẳng nhẵn, nhưng không vì vậy mà trơn trượt. Khi sử dụng người dùng sẽ thấy độ ma sát tốt và khá nhạy. Ngược lại, 2 phím chuột thì hơi “cứng” cần nhiều lực nhấn khi sử dụng.</p>\r\n<p>Màn hình của Asus F80S dạng gương, độ sáng cao nhưng không bị bắt sáng như ở nhiều màn gương bóng ở các mẫu máy khác. Hình ảnh trên đó thể hiện được màu sắc sống động và rất phù hợp với xem phim hay chơi game. Việc hiển thị văn bản cũng rõ ràng với góc nhìn rộng.</p>\r\n<p>Về kết nối, cũng giống như nhiều mẫu máy khác thuộc phân khúc giá này, Asus F80S được trang bị đầy đủ kết nối cần thiết và hiện đại, như Wi-Fi, Bluetooth, LAN, Modem, đầu đọc thẻ 8 trong 1 (8-in-1), ngõ xuất hình ảnh tiêu chuẩn VGA và HDMI đời mới, Webcam 1,3 Megapixel tích hợp, 3 cổng USB, ngõ cắm Headphone/Microphone và ổ ghi DVD. Tuy nhiên, các ngõ cắm và cổng xuất này được ASUS bố trí hơi khác ở cả 4 mặt máy trên F80S.</p>', 1, '2009-05-06 08:21:15');
