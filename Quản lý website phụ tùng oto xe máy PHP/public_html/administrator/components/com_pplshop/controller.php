<?php

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');
JTable::addIncludePath(JPATH_COMPONENT.DS.'tables');

/**
 * Hello World Component Controller
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class pplshopController extends JController
{
    /**
     * Method to display the view
     *
     * @access    public
     */
    var $_db = null;
    function __construct()
    {
        parent::__construct();
        $this->registerTask( 'editCat','addCat' );
        $this->registerTask( 'editProduct','addProduct' );
        $this->registerTask( 'unpublish', 'publish' );     
        $this->_db = JFactory::getDBO();
    }
    
    function display()
    {
        JRequest::setVar( 'view', 'product' );
        JRequest::setVar('layout', 'default');
        parent::display();
    }
    
    function listCat() {
        JRequest::setVar( 'view', 'category' );
        JRequest::setVar( 'layout', 'default'  );
        parent::display();
    }
    
    function addCat(){
        JRequest::setVar( 'view', 'category' ); 
        JRequest::setVar( 'layout', 'editCat' );
        parent::display(); 
    }
    
    function saveCat() {
        $model = $this->getModel('category');
        if($model->saveCat()){
            $link ="index.php?option=com_pplshop&task=listCat";
            $msg="Lưu thành công";
            $this->setRedirect($link, $msg); 
        }
    }
    
    function cancelCat(){
        $link ="index.php?option=com_pplshop&task=listCat";
        $this->setRedirect($link, $msg);
    }
    
    function delCat(){
        $model = $this->getModel('category');
        if($model->delCat()){
            $link ="index.php?option=com_pplshop&task=listCat";
            $msg="Xóa thành công";
        } else {
            $link ="index.php?option=com_pplshop&task=listCat";
            $msg="Không thể xóa Danh Mục này";
        }
        $this->setRedirect($link, $msg); 
    }
    
    /*  Product Function  */
    function addProduct() {
        JRequest::setVar( 'view', 'product' ); 
        JRequest::setVar( 'layout', 'editProduct' );
        parent::display();
    }
    
    function saveProduct() {
        $model = $this->getModel('Product');
        if($model->saveProduct()){
            $link ="index.php?option=com_pplshop";
            $msg="Lưu thành công";
            $this->setRedirect($link, $msg); 
        }
    }
    function delProduct(){
        $model = $this->getModel('product');
        if($model->delProduct()){
            $link ="index.php?option=com_pplshop";
            $msg="Đã xóa xong";
        } else {
            $link ="index.php?option=com_pplshop";
            $msg="Có lỗi trong quá trình xóa sản phẩm";
        }
        $this->setRedirect($link, $msg); 
    }
    
    
    function publish(){
        global $option;
        $cid = JRequest::getVar( 'cid', array(), '', 'array' );
        $category_id = JRequest::getInt('category_id');
        if( $this->_task == 'publish'){
            $publish = 1;
        } else {
            $publish = 0;
        }
        $row =& JTable::getInstance('Product', 'Table');
        $row->publish($cid, $publish);
        $this->setRedirect( 'index.php?option=' . $option .'&category_id='.$category_id);
    }
    
    function saveOrder() {
        global $mainframe;
        $db =& JFactory::getDBO();
        $Orderid = JRequest::getVar('id');
        $status = JRequest::getVar('status');
        $notes = JRequest::getVar('notes', '', 'post', 'string', JREQUEST_ALLOWRAW);
        $queryup = "UPDATE #__pplshop_orders SET status = $status, notes = '$notes' WHERE (id = '$Orderid')";
       // die($queryup);
        $this->_db->setQuery($queryup);
        $this->_db->query();
        $lt = JRequest::getInt('limitstart');
        $msg = "Cập nhật Đơn đặt hàng thành công.";
        $link = "index.php?option=com_pplshop&task=viewOrder&limitstartr=".$lt;
        $mainframe->redirect($link,$msg);
    }
    function viewOrder()
    {
        JRequest::setVar( 'view', 'order' );
        JRequest::setVar( 'layout', 'default'  );
        parent::display();
    }
    function orderDetail()
    {
        JRequest::setVar( 'view', 'order' );
        JRequest::setVar( 'layout', 'orderdetail'  );
        parent::display();
    }
    function delOrder(){
        $model = $this->getModel('order');
        $lt = JRequest::getInt('limitstart');
        if($model->delOrder()){
            $link ="index.php?option=com_pplshop&task=viewOrder&limitstart=".$lt;
            $msg="Xóa thành công";
        } else {
            $link ="index.php?option=com_pplshop&task=viewOrde&limitstartr=".$lt;
            $msg="Không thể xóa Đơn đặt hàng này";
        }
        $this->setRedirect($link, $msg); 
    }
     
    function cancelOrder(){
    
        $lt = JRequest::getInt('limitstart');
        $link ="index.php?option=com_pplshop&task=viewOrder&limitstart=".$lt;
        $this->setRedirect($link, $msg);
    }
}