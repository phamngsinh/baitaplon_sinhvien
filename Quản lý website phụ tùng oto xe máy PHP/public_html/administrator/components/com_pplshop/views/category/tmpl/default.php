<?php
    defined('_JEXEC') or die('Restricted access');
    $listCategory = $this->items;
    $model = $this->getModel();
?>
<form action="index.php" method="post" name="adminForm">
<div id="editcell">
    <table class="adminlist">
    <thead>
        <tr>
            <th width="5">
                <?php echo JText::_( 'ID' ); ?>
            </th>
            <th width="20">
               <!-- <input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $listCategory ); ?>);" />    -->
            </th>            
            <th>
                <?php echo JText::_( 'Tên Danh mục' ); ?>
            </th>
            <th>
                <?php echo JText::_( 'Sắp xếp' );//JHTML::_('grid.order',  $listCategory ); ?> 
            </th>

        </tr>
    </thead>
    <?php
    $k = 0;
    for ($i=0, $n=count( $listCategory ); $i < $n; $i++)    {
        $row = &$listCategory[$i];
        $checked     = JHTML::_('grid.id',   $i, $row->id );
        $link         = JRoute::_( 'index.php?option=com_pplshop&task=editCat&cid='. $row->id );
        ?>
      <!--  <input type="hidden" name="cid[]" value="" /> -->
        <tr class="<?php echo "row$k"; ?>">
            <td>
                <?php echo $row->id; ?>
            </td>
            <td>
                <?php echo $checked; ?>
            </td>
            <td style="font-weight:bold">
                <a href="<?php echo $link; ?>"><?php echo $row->category_name; ?></a>
            </td>
            <td class="order">
                    <?php echo $row->ordering; ?>
                </td>
        </tr>
        <?php
        // If category have child
        $listChildCategory = $model->getChildCategory($row->id);
        if(count($listChildCategory) > 0) {
           for ($j=0, $m=count($listChildCategory); $j<$m; $j++){
               $row = &$listChildCategory[$j];
               $checked     = JHTML::_('grid.id',   $j, $row->id );
               $link         = JRoute::_( 'index.php?option=com_pplshop&task=editCat&cid[]='. $row->id );
               $k = 1 - $k; 
               ?>
               <!--<input type="hidden" name="cid[]" value="<?php echo $row->id ?>" />-->
               <tr class="<?php echo "row$k"; ?>">
                <td>
                    <?php echo $row->id; ?>
                </td>
                <td>
                 <?php echo $checked; ?>
                </td>
                <td style="text-indent:20px">
                    <a href="<?php echo $link; ?>"><?php echo $row->category_name; ?></a>
                </td>
                <td class="order">
                    <?php echo $row->ordering; ?>
                </td>
                </tr>
               <?
               
           } 
        }
        $k = 1 - $k;
    }
    ?>
    </table>
</div>

<input type="hidden" name="option" value="com_pplshop" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
</form>
