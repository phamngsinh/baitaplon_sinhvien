<?php
    defined('_JEXEC') or die('Restricted access');
    $category = $this->items['category'];
    $listParent = $this->items['listParent'];
?>

<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="col100">
    <fieldset class="adminform">

        <table class="admintable">
        <tr>
            <td width="100" align="right" class="key">
                <label for="category_name">
                    <?php echo JText::_( 'Tên danh mục' ); ?>:
                </label>
            </td>
            <td>
                <input class="text_area" type="text" name="category_name" id="category_name" size="40" maxlength="250" value="<?php echo $category->category_name;?>" />
            </td>
        </tr>
         <tr>
            <td width="100" align="right" class="key">
                <label for="category_name">
                    <?php echo JText::_( 'Thứ tự' ); ?>:
                </label>
            </td>
            <td>
                <input class="text_area" type="text" name="ordering" id="ordering" size="10" maxlength="250" value="<?php echo $category->ordering;?>" />
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="parentid">
                    <?php echo JText::_( 'Danh mục cha' ); ?>:
                </label>
            </td>
            <td>
                <select name="parentid" id="parentid">
                <option value="0"> Là danh mục cha</option>
                <?php
                    foreach($listParent as $parent) {
                        if($parent->id == $category->parentid){
                           echo '<option value ="'.$parent->id.'" selected>'.$parent->category_name.'</option>';  
                        } else {
                           echo '<option value ="'.$parent->id.'">'.$parent->category_name.'</option>';  
                        }
                        
                    }
                ?>
                </select>
            </td>
        </tr>
    </table>
    </fieldset>
</div>
<div class="clr"></div>

<input type="hidden" name="option" value="com_pplshop" />
<input type="hidden" name="id" value="<?php echo $category->id; ?>" />
<input type="hidden" name="task" value="save" />
<input type="hidden" name="controller" value="" />
</form>
