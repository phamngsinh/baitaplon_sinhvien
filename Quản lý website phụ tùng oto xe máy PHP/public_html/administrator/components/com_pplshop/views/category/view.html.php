<?php
/**
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://docs.joomla.org/Developing_a_Model-View-Controller_Component_-_Part_1
 * @license    GNU/GPL
*/
 
// no direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.view');
 
/**
 *
 */
 
class pplshopViewCategory extends JView
{
    function display($tpl = null)
    {
        $model = $this->getModel();
        $layout = $this->getLayout();
        if($layout == 'default') {
            $items = $model->getListParent();
        } else if($layout == 'editCat') {
            $items['category'] = $model->getCategoryInfo();
            $items['listParent'] = $model->getListParent();
        }
        $this->assignRef( 'items', $items );
        parent::display($tpl);
    }
}
