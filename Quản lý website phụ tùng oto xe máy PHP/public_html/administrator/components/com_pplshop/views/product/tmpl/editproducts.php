<?php
    defined('_JEXEC') or die('Restricted access');
    $product = $this->items['product'];
    $listParent = $this->items['listParent'];
    $model = $this->getModel();
?>
<script>
function CalcKeyCode(aChar) {
  var character = aChar.substring(0,1);
  var code = aChar.charCodeAt(0);
  return code;
}

function checkNumber(val) {
  var strPass = val.value;
  var strLength = strPass.length;
  var lchar = val.value.charAt((strLength) - 1);
  var cCode = CalcKeyCode(lchar);

  if (cCode < 48 || cCode > 57 ) {
    var myNumber = val.value.substring(0, (strLength) - 1);
    val.value = myNumber;
  }
  return false;
}

function zoomPic(){
    if (document.getElementById("viewProduct").style.display==""){
        document.getElementById("viewProduct").style.display="none";
        } else {
            document.getElementById("viewProduct").style.display="";
        }
    }

</script>
<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<div class="col100">
    <fieldset class="adminform">
       
        <table>
            <tr>
                <td>
        
                        <table class="admintable">
                        <tr height="40">
                            <td width="100" align="right" class="key">
                                <label for="product_name">
                                    <?php echo JText::_( 'Tên Sản Phẩm' ); ?>:
                                </label>
                            </td>
                            <td>
                                <input class="text_area" type="text" name="product_name" id="product_name" size="60" maxlength="250" value="<?php echo $product->product_name;?>" />
                            </td>
                        </tr>
                        <tr height="40">
                            <td width="100" align="right" class="key">
                                <label for="product_name">
                                    <?php echo JText::_( 'Nhà Sản Xuất' ); ?>:
                                </label>
                            </td>
                            <td>
                                <input class="text_area" type="text" name="manufacturer" id="manufacturer" size="60" maxlength="250" value="<?php echo $product->manufacturer;?>" />
                            </td>
                        </tr>
                        <tr height="40">  
                            <td width="100" align="right" class="key">
                                <label for="parentid">
                                    <?php echo JText::_( 'Danh Mục' ); ?>:
                                </label>
                            </td>
                            <td>
                                <select name="category_id" id="category_id">
                                <?php
                                    foreach($listParent as $parent) {
                                        if($parent->id == $product->category_id){
                                           echo '<option value ="'.$parent->id.'" selected>'.$parent->category_name.'</option>';  
                                        } else {
                                           echo '<option value ="'.$parent->id.'">'.$parent->category_name.'</option>';  
                                        }
                                        $listChild = $model->getChildCategory($parent->id);
                                        if(count($listChild) > 0) {
                                            
                                            foreach($listChild as $child){
                                                if($child->id == $product->category_id){
                                                    echo '<option value ="'.$child->id.'" selected> &nbsp;&nbsp;&nbsp;'.$child->category_name.'</option>';
                                                } else {
                                                    echo '<option value ="'.$child->id.'"> &nbsp;&nbsp;&nbsp;'.$child->category_name.'</option>';
                                                }
                                                
                                            }
                                        }
                                        
                                    }
                                ?>
                                </select>
                            </td>
                        </tr>
                        <tr height="40">  
                            <td width="100" align="right" class="key">
                                <label for="price">
                                    <?php echo JText::_( 'Giá cũ' ); ?>:
                                </label>
                            </td>
                            <td>
                                <input class="price" type="text" name="price" id="price" size="30" maxlength="250" value="<?php echo $product->price;?>" onKeyUp="checkNumber(adminForm.price);" /> 
                            </td>
                        </tr>
                        <tr height="40">  
                            <td width="100" align="right" class="key">
                                <label for="price">
                                    <?php echo JText::_( 'Giá mới' ); ?>:
                                </label>
                            </td>
                            <td>
                                <input class="price" type="text" name="price_new" id="price" size="30" maxlength="250" value="<?php echo $product->price_new;?>" onKeyUp="checkNumber(adminForm.price);" /> 
                            </td>
                        </tr> 
                        <tr><td width="100" align="right" class="key">
                            <label for="price">
                                    <?php echo JText::_( 'Chi tiết giá' ); ?>:
                             </label>
                        </td><td><input class="price" type="text" name="price_detail" id="price_detail" size="30" maxlength="250" value="<?php echo $product->price_detail;?>"  /> (đ/hộp; vnđ/thùng/24lon, ...)</td></tr> 
                        <tr height="40">  
                            <td width="100" align="right" class="key">
                                <label for="price">
                                    <?php echo JText::_( 'Hàng giảm giá' ); ?>:
                                </label>
                            </td>
                            <td>
                                <input type="checkbox" name="giamgia" value="1" <?if($product->giamgia==1){echo 'checked="checked"';}?>>
                            </td>
                        </tr>
                        <tr><td width="100" align="right" class="key"><label for="price">
                                    <?php echo JText::_( 'Giới thiệu' ); ?>:
                                </label></td>
                        
                        <td> <textarea name="intro" id="intro" cols="50" rows="3"><?php echo $product->intro;?></textarea></td></tr>                                               
                        <tr height="40">  
                            <td width="100" align="right" class="key">
                                <label for="price">
                                    <?php echo JText::_( 'Hình' ); ?>:
                                </label>
                            </td>
                            <td>
                                <input type="file" name="images" id="images" style="width:100px" size="47" /> 
                            </td>
                        </tr>
                        </table>

                    </td>
                    <td>
        
        
                            <table class="admintable">
                            <tr>
                                <td align="center">
                                <?php 
                                    $urlimage = 'components/com_pplshop/images/products/'.$product->image2;
                                    $urlimage1 = 'components/com_pplshop/images/products/'.$product->image1; 
                                ?>
                                <?php if(count($product) > 0) { ?><img onclick="zoomPic();" src="<?php echo $urlimage; ?>"></img> <? } ?>
                                </td>
                            </tr> 
                            </table>
        
                    </td>
                </tr>
          </table> 


        <table class="admintable" style="padding-left:3px"> 
        <tr>
            <td width="100" align="right" valign="top" class="key">
                <label for="description">
                    <?php echo JText::_( 'Thông tin về Sản Phẩm' ); ?>:
                </label>
            </td>
            <td>
               <?php
                $editor =& JFactory::getEditor();
                echo $editor->display('description', $product->description, '100%', '200', '20', '15', false);
        ?>
            </td>
        </tr>
    </table>
    </fieldset>
</div>


<div class="clr"></div>
<input type="hidden" name="created" id="created" value="<?=$product->created?>" />   
<input type="hidden" name="option" value="com_pplshop" />
<input type="hidden" name="id" value="<?php echo $product->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="" />
</form>


<div id="viewProduct" align="center" style="text-align:center; display:none; right:50%-200px; width:400px; position: absolute; top:200px; right:50px; z-index:3; background-color:#f3f3f3;">
    <img src="<?php echo $urlimage1; ?>"></img>    
</div>
