<?php
    defined('_JEXEC') or die('Restricted access');
    $product = $this->items['product'];
    $listParent = $this->items['listParent'];
    $model = $this->getModel();
?>
<script>
function CalcKeyCode(aChar) {
  var character = aChar.substring(0,1);
  var code = aChar.charCodeAt(0);
  return code;
}

function checkNumber(val) {
  var strPass = val.value;
  var strLength = strPass.length;
  var lchar = val.value.charAt((strLength) - 1);
  var cCode = CalcKeyCode(lchar);

  if (cCode < 48 || cCode > 57 ) {
    var myNumber = val.value.substring(0, (strLength) - 1);
    val.value = myNumber;
  }
  return false;
}

function zoomPic(){
    if (document.getElementById("viewProduct").style.display==""){
        document.getElementById("viewProduct").style.display="none";
        } else {
            document.getElementById("viewProduct").style.display="";
        }
    }

</script>
<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<div class="col100">
    <fieldset class="adminform">
       
        <table>
            <tr>
                <td>
        
                        <table class="admintable">
                        <tr>
                            <td width="100" align="right" class="key">
                                <label for="product_name">
                                    <?php echo JText::_( 'Tên Sản Phẩm' ); ?>:
                                </label>
                            </td>
                            <td>
                                <input class="text_area" type="text" name="product_name" id="product_name" size="60" maxlength="250" value="<?php echo $product->product_name;?>" />
                            </td>
                        </tr>
                        <tr>
                            <td width="100" align="right" class="key">
                                <label for="product_name">
                                    <?php echo JText::_( 'Nhà Sản Xuất' ); ?>:
                                </label>
                            </td>
                            <td>
                                <input class="text_area" type="text" name="manufacturer" id="manufacturer" size="60" maxlength="250" value="<?php echo $product->manufacturer;?>" />
                            </td>
                        </tr>
                        <tr>  
                            <td width="100" align="right" class="key">
                                <label for="parentid">
                                    <?php echo JText::_( 'Danh Mục' ); ?>:
                                </label>
                            </td>
                            <td>
                                <select name="category_id" id="category_id">
                                <?php
                                    foreach($listParent as $parent) {
                                        if($parent->id == $product->category_id){
                                           echo '<option value ="'.$parent->id.'" selected>'.$parent->category_name.'</option>';  
                                        } else {
                                           echo '<option value ="'.$parent->id.'">'.$parent->category_name.'</option>';  
                                        }
                                        $listChild = $model->getChildCategory($parent->id);
                                        if(count($listChild) > 0) {
                                            
                                            foreach($listChild as $child){
                                                if($child->id == $product->category_id){
                                                    echo '<option value ="'.$child->id.'" selected> &nbsp;&nbsp;&nbsp;'.$child->category_name.'</option>';
                                                } else {
                                                    echo '<option value ="'.$child->id.'"> &nbsp;&nbsp;&nbsp;'.$child->category_name.'</option>';
                                                }
                                                
                                            }
                                        }
                                        
                                    }
                                ?>
                                </select>
                            </td>
                        </tr>
                        <tr>  
                            <td width="100" align="right" class="key">
                                <label for="price">
                                    <?php echo JText::_( 'Giá cũ' ); ?>:
                                </label>
                            </td>
                            <td>
                                <input class="price" type="text" name="price" id="price" size="30" maxlength="250" value="<?php echo $product->price;?>" onKeyUp="checkNumber(adminForm.price);" /> 
                                  <label for="price">
                                    <?php echo JText::_( 'Giá mới' ); ?>:
                                </label>
                            
                                <input class="price" type="text" name="priceb" id="priceb" size="30" maxlength="250" value="<?php echo $product->priceb;?>" onKeyUp="checkNumber(adminForm.price2);" /> 
                                
                            </td>
                        </tr>
                                               
                    <!--    <tr>  
                            <td width="100" align="right" class="key">
                                <label for="price">
                                    <?php echo JText::_( 'Chiều rộng' ); ?>:
                                </label>
                            </td>
                            <td>
                                <input class="width" type="text" name="width" id="width" size="30" maxlength="250" value="<?php echo $product->width;?>" /> 
                            </td>
                        </tr>
                        <tr>  
                            <td width="100" align="right" class="key">
                                <label for="price">
                                    <?php echo JText::_( 'Chiều cao' ); ?>:
                                </label>
                            </td>
                            <td>
                                <input class="height" type="text" name="height" id="price" size="30" maxlength="250" value="<?php echo $product->height;?>" /> 
                            </td>
                        </tr> 
                        <tr><td width="100" align="right" class="key">
                            <label for="price">
                                    <?php echo JText::_( 'Đường kính' ); ?>:
                             </label>
                        </td><td><input class="diameter" type="text" name="diameter" id="diameter" size="30" maxlength="250" value="<?php echo $product->diameter;?>"  /></td></tr> 
                        <tr><td width="100" align="right" class="key">
                            <label for="price">
                                    <?php echo JText::_( 'Chất liệu' ); ?>:
                             </label>
                        </td><td><input class="material" type="text" name="material" id="material" size="30" maxlength="250" value="<?php echo $product->material;?>"  /></td>
                        </tr> 
                        <tr><td width="100" align="right" class="key">
                            <label for="price">
                                    <?php echo JText::_( 'Công suất' ); ?>:
                             </label>
                        </td><td><input class="capacity" type="text" name="capacity" id="capacity" size="30" maxlength="250" value="<?php echo $product->capacity;?>"  /></td>
                        </tr> -->
                        <tr><td width="100" align="right" class="key"><label for="price">
                                    <?php echo JText::_( 'Bảo hành' ); ?>:
                                </label></td>
                        
                        <td> <input class="warranty" type="text" name="warranty" id="warranty" size="30" maxlength="250" value="<?php echo $product->warranty;?>"  /> tháng</td></tr>                                               
                        <tr>  
                            <td width="100" align="right" class="key">
                                <label for="price">
                                    <?php echo JText::_( 'Sản phẩm nổi bật' ); ?>:
                                </label>
                            </td>
                            <td>
                                <input type="checkbox" name="hoted" value="1" <?if($product->hoted==1){echo 'checked="checked"';}?>>
                            </td>
                        </tr>
                        <tr>
                         <td width="100" align="right" class="key">
                                <label for="greeting">
                                    <?php echo JText::_( 'Hiển thị' ); ?>:
                                </label>
                            </td>
                            <td>
                            <input type="radio" class="inputbox" <?if($product->id) {if($product->published==1) echo 'checked="checked"';} else echo 'checked="checked"'?> value="1" id="published1" name="published">
                            <label for="published1">Có</label>
                            <input type="radio" class="inputbox" value="0" <?if($product->id) {if($product->published==0) echo 'checked="checked"';}?> id="published0" name="published">
                            <label for="published0">Không</label>
                            </td>
                        </tr>
                        
                        <tr>  
                            <td width="100" align="right" class="key">
                                <label for="price">
                                    <?php echo JText::_( 'Hình 1' ); ?>:
                                </label>
                            </td>
                            <td>
                                <input type="file" name="images1" id="images1" style="width:100px" size="47" /> 
                            </td>
                        </tr> 
                         <tr>  
                            <td width="100" align="right" class="key">
                                <label for="price">
                                    <?php echo JText::_( 'Hình 2' ); ?>:
                                </label>
                            </td>
                            <td>
                                <input type="file" name="images2" id="images2" style="width:100px" size="47" /> 
                            </td>
                        </tr> 

                    </td>
                    <td>
        
        
                            <table class="admintable">
                            <tr>
                                <td align="center">
                                <?php 
                                    $urlimage = 'components/com_pplshop/images/products/'.$product->image2;
                                    $urlimage1 = 'components/com_pplshop/images/products/'.$product->image1; 
                                ?>
                                <?php if(count($product) > 0) { ?><img onclick="zoomPic();" src="<?php echo $urlimage; ?>"></img> <? } ?>
                                </td>
                            </tr> 
                            </table>
        
                    </td>
                </tr>
          </table> 


        <table class="admintable" style="padding-left:3px"> 
        <tr>
            <td width="100" align="right" valign="top" class="key">
                <label for="description">
                    <?php echo JText::_( 'Thông tin về Sản Phẩm' ); ?>:
                </label>
            </td>
            <td>
               <?php
                $editor =& JFactory::getEditor();
                echo $editor->display('description', $product->description, '100%', '300', '20', '15', false);
        ?>
            </td>
        </tr>
    </table>
    </fieldset>
</div>


<div class="clr"></div>
<input type="hidden" name="created" id="created" value="<?=$product->created?>" />   
<input type="hidden" name="option" value="com_pplshop" />
<input type="hidden" name="id" value="<?php echo $product->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="" />
</form>


<div id="viewProduct" align="center" style="text-align:center; display:none; right:50%-200px; width:158px; position: absolute; top:200px; right:292px; z-index:3; background-color:#f3f3f3; height:184px">
    <img src="<?php echo $urlimage1; ?>"></img>    
</div>
