<?php
    defined('_JEXEC') or die('Restricted access');
    $product = $this->items['product'];
    $listProduct = $product['listProduct'];
    $nav = $product['Nav'];
    $category_id = $product['category_id'];
    $listParent = $this->items['listParent']; 
    $model = $this->getModel();
    
    
    
?>

<form action="index.php" method="post" name="adminForm">
<div id="editcell">
    <table>
        <tbody>
            <tr>
                <td width="100%"></td>
                <td nowrap="nowrap">
                    <select id="category_id" name="category_id" size="1" onchange="document.adminForm.submit()">
                        <option value="0"> Chọn Danh mục Sản phẩm </option>
                        <?php
                            foreach($listParent as $parent) {
                                if($parent->id == $category_id){
                                   echo '<option value ="'.$parent->id.'" selected>'.$parent->category_name.'</option>'; 
                                } else {
                                   echo '<option value ="'.$parent->id.'">'.$parent->category_name.'</option>'; 
                                }

                                $listChild = $model->getChildCategory($parent->id);
                                if(count($listChild) > 0) {
                                    foreach($listChild as $child){
                                        if($child->id == $category_id){
                                            echo '<option value ="'.$child->id.'" selected> &nbsp;&nbsp;&nbsp;'.$child->category_name.'</option>';
                                        } else {
                                            echo '<option value ="'.$child->id.'"> &nbsp;&nbsp;&nbsp;'.$child->category_name.'</option>';
                                        }
                                    }
                                }
                            }
                        ?>
                    </select>
                </td>  
                
            </tr>
        </tbody>
    </table>
    <table class="adminlist">
    <thead>
        <tr>
            <th width="5">
                <?php echo JText::_( 'ID' ); ?>
            </th>
            <th width="20">
                <input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $listProduct ); ?>);" />
            </th>            
            <th>
                <?php echo JText::_( 'Tên Sản phẩm' ); ?>
            </th>
            <th width="80" align="center">
                <?php echo JText::_( 'Đã được bật' ); ?>
            </th>
            <th>
                <?php echo JText::_( 'Đơn Giá (VNĐ)' ); ?>
            </th>
            <th>
                <?php echo JText::_( 'Ngày Đăng' ); ?>
            </th>
            <th>
                <?php echo JText::_( 'Danh Mục' ); ?>
            </th>
            <th>
                <?php echo JText::_( 'Lần xem' ); ?>
            </th>
        </tr>
    </thead>
    <?php
    jimport('joomla.filter.output');
    $k = 0;
    for ($i=0, $n=count( $listProduct ); $i < $n; $i++)    {
        $row = &$listProduct[$i];
        $checked     = JHTML::_('grid.id',   $i, $row->id );
        $published = JHTML::_('grid.published', $row, $i );
        $link         = JRoute::_( 'index.php?option=com_pplshop&task=editProduct&cid[]='. $row->id );
        $urlimage = 'components/com_pplshop/images/products/'.$row->image2;
        ?>
        <tr class="<?php echo "row$k"; ?>">
            <td>
                <?php echo $row->id; ?>
            </td>
            <td>
                <?php echo $checked; ?>
            </td>
            <td>
                <a href="<?php echo $link; ?>"><?php echo $row->product_name; ?></a>
            </td>
            <td align="center">
                <?php echo $published; ?>
            </td>
            <td align="right">
                <?php echo number_format($row->priceb, 0, '', '.'); ?>
            </td>
            <td align="center">
                <?php echo JHTML::date($row->created, '%d/%m/%Y') ?></td>
            </td>
            <td>
                <?php echo $model->getCatName($row->category_id)?>
            </td>
            <td>
                <?php echo number_format($row->hits, 0, '', '.'); ?>
            </td>
            
        </tr>
        <?php
        $k = 1 - $k;
    } if($nav->getPagesLinks() != null) {
    ?>
        <tr class="row1"> 
             <td colspan="8">
                <?=$nav->getListFooter(); ?>
              </td> 
        </tr>
       <?php } ?> 
    </table>
</div>

<input type="hidden" name="option" value="com_pplshop" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
</form>
