<?php
/**
* @version		$Id: content.php 14401 2010-01-26 14:10:00Z louis $
* @package		Joomla.Framework
* @subpackage	Table
* @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is within the rest of the framework
defined('JPATH_BASE') or die();


/**
 * Content table
 *
 * @package 	Joomla.Framework
 * @subpackage		Table
 * @since	1.0
 */
class JTableContent extends JTable
{
	/** @var int Primary key */
	var $id					= null;
	/** @var string */
	var $title				= null;
	/** @var string */
	var $alias				= null;
	/** @var string */
	var $title_alias			= null;
	/** @var string */
	var $introtext			= null;
	/** @var string */
	var $fulltext			= null;
	/** @var int */
	var $state				= null;
	/** @var int The id of the category section*/
	var $sectionid			= null;
	/** @var int DEPRECATED */
	var $mask				= null;
	/** @var int */
	var $catid				= null;
	/** @var datetime */
	var $created				= null;
	/** @var int User id*/
	var $created_by			= null;
	/** @var string An alias for the author*/
	var $created_by_alias		= null;
	/** @var datetime */
	var $modified			= null;
	/** @var int User id*/
	var $modified_by			= null;
	/** @var boolean */
	var $checked_out			= 0;
	/** @var time */
	var $checked_out_time		= 0;
	/** @var datetime */
	var $publish_up			= null;
	/** @var datetime */
	var $publish_down		= null;
	/** @var string */
	var $images				= null;
	/** @var string */
	var $urls				= null;
	/** @var string */
	var $attribs				= null;
	/** @var int */
	var $version				= null;
	/** @var int */
	var $parentid			= null;
	/** @var int */
	var $ordering			= null;
	/** @var string */
	var $metakey				= null;
	/** @var string */
	var $metadesc			= null;
	/** @var string */
	var $metadata			= null;
	/** @var int */
	var $access				= null;
	/** @var int */
	var $hits				= null;

	/**
	* @param database A database connector object
	*/
	function __construct( &$db ) {
		parent::__construct( '#__content', 'id', $db );
	}

	/**
	 * Overloaded check function
	 *
	 * @access public
	 * @return boolean
	 * @see JTable::check
	 * @since 1.5
	 */
	function check()
	{
		/*
		TODO: This filter is too rigorous,need to implement more configurable solution
		// specific filters
		$filter = & JFilterInput::getInstance( null, null, 1, 1 );
		$this->introtext = trim( $filter->clean( $this->introtext ) );
		$this->fulltext =  trim( $filter->clean( $this->fulltext ) );
		*/


		if(empty($this->title)) {
			$this->setError(JText::_('Article must have a title'));
			return false;
		}

		if(empty($this->alias)) {
			//$this->alias = $this->title;
            $this->alias = $this->vnit_change_unicode($this->title);
		}
		//$this->alias = JFilterOutput::stringURLSafe($this->alias);

		/*if(trim(str_replace('-','',$this->alias)) == '') {
			$datenow =& JFactory::getDate();
			$this->alias = $datenow->toFormat("%Y-%m-%d-%H-%M-%S");
		}
              */
		if (trim( str_replace( '&nbsp;', '', $this->fulltext ) ) == '') {
			$this->fulltext = '';
		}

		if(empty($this->introtext) && empty($this->fulltext)) {
			$this->setError(JText::_('Article must have some text'));
			return false;
		}

		// clean up keywords -- eliminate extra spaces between phrases
		// and cr (\r) and lf (\n) characters from string
		if(!empty($this->metakey)) { // only process if not empty
			$bad_characters = array("\n", "\r", "\"", "<", ">"); // array of characters to remove
			$after_clean = JString::str_ireplace($bad_characters, "", $this->metakey); // remove bad characters
			$keys = explode(',', $after_clean); // create array using commas as delimiter
			$clean_keys = array(); 
			foreach($keys as $key) {
				if(trim($key)) {  // ignore blank keywords
					$clean_keys[] = trim($key);
				}
			}
			$this->metakey = implode(", ", $clean_keys); // put array back together delimited by ", "
		}
		
		// clean up description -- eliminate quotes and <> brackets
		if(!empty($this->metadesc)) { // only process if not empty
			$bad_characters = array("\"", "<", ">");
			$this->metadesc = JString::str_ireplace($bad_characters, "", $this->metadesc);
		}

		return true;
	}

	/**
	* Converts record to XML
	* @param boolean Map foreign keys to text values
	*/
	function toXML( $mapKeysToText=false )
	{
		$db =& JFactory::getDBO();

		if ($mapKeysToText) {
			$query = 'SELECT name'
			. ' FROM #__sections'
			. ' WHERE id = '. (int) $this->sectionid
			;
			$db->setQuery( $query );
			$this->sectionid = $db->loadResult();

			$query = 'SELECT name'
			. ' FROM #__categories'
			. ' WHERE id = '. (int) $this->catid
			;
			$db->setQuery( $query );
			$this->catid = $db->loadResult();

			$query = 'SELECT name'
			. ' FROM #__users'
			. ' WHERE id = ' . (int) $this->created_by
			;
			$db->setQuery( $query );
			$this->created_by = $db->loadResult();
		}

		return parent::toXML( $mapKeysToText );
	}
   function vnit_change_unicode($value){
    #---------------------------------a^
    $value = str_replace("ấ", "a", $value);
    $value = str_replace("ầ", "a", $value);
    $value = str_replace("ẩ", "a", $value);
    $value = str_replace("ẫ", "a", $value);
    $value = str_replace("ậ", "a", $value);
    #---------------------------------A^
    $value = str_replace("Ấ", "A", $value);
    $value = str_replace("Ầ", "A", $value);
    $value = str_replace("Ẩ", "A", $value);
    $value = str_replace("Ẫ", "A", $value);
    $value = str_replace("Ậ", "A", $value);
    #---------------------------------a(
    $value = str_replace("ắ", "a", $value);
    $value = str_replace("ằ", "a", $value);
    $value = str_replace("ẳ", "a", $value);
    $value = str_replace("ẵ", "a", $value);
    $value = str_replace("ặ", "a", $value);
    #---------------------------------A(
    $value = str_replace("Ắ", "A", $value);
    $value = str_replace("Ằ", "A", $value);
    $value = str_replace("Ẳ", "A", $value);
    $value = str_replace("Ẵ", "A", $value);
    $value = str_replace("Ặ", "A", $value);
    #---------------------------------a
    $value = str_replace("á", "a", $value);
    $value = str_replace("à", "a", $value);
    $value = str_replace("ả", "a", $value);
    $value = str_replace("ã", "a", $value);
    $value = str_replace("ạ", "a", $value);
    $value = str_replace("â", "a", $value);
    $value = str_replace("ă", "a", $value);
    #---------------------------------A
    $value = str_replace("Á", "A", $value);
    $value = str_replace("À", "A", $value);
    $value = str_replace("Ả", "A", $value);
    $value = str_replace("Ã", "A", $value);
    $value = str_replace("Ạ", "A", $value);
    $value = str_replace("Â", "A", $value);
    $value = str_replace("Ă", "A", $value);
    #---------------------------------e^
    $value = str_replace("ế", "e", $value);
    $value = str_replace("ề", "e", $value);
    $value = str_replace("ể", "e", $value);
    $value = str_replace("ễ", "e", $value);
    $value = str_replace("ệ", "e", $value);
    #---------------------------------E^
    $value = str_replace("Ế", "E", $value);
    $value = str_replace("Ề", "E", $value);
    $value = str_replace("Ể", "E", $value);
    $value = str_replace("Ễ", "E", $value);
    $value = str_replace("Ệ", "E", $value);
    #---------------------------------e
    $value = str_replace("é", "e", $value);
    $value = str_replace("è", "e", $value);
    $value = str_replace("ẻ", "e", $value);
    $value = str_replace("ẽ", "e", $value);
    $value = str_replace("ẹ", "e", $value);
    $value = str_replace("ê", "e", $value);
    #---------------------------------E
    $value = str_replace("É", "E", $value);
    $value = str_replace("È", "E", $value);
    $value = str_replace("Ẻ", "E", $value);
    $value = str_replace("Ẽ", "E", $value);
    $value = str_replace("Ẹ", "E", $value);
    $value = str_replace("Ê", "E", $value);
    #---------------------------------i
    $value = str_replace("í", "i", $value);
    $value = str_replace("ì", "i", $value);
    $value = str_replace("ỉ", "i", $value);
    $value = str_replace("ĩ", "i", $value);
    $value = str_replace("ị", "i", $value);
    #---------------------------------I
    $value = str_replace("Í", "I", $value);
    $value = str_replace("Ì", "I", $value);
    $value = str_replace("Ỉ", "I", $value);
    $value = str_replace("Ĩ", "I", $value);
    $value = str_replace("Ị", "I", $value);
    #---------------------------------o^
    $value = str_replace("ố", "o", $value);
    $value = str_replace("ồ", "o", $value);
    $value = str_replace("ổ", "o", $value);
    $value = str_replace("ỗ", "o", $value);
    $value = str_replace("ộ", "o", $value);
    #---------------------------------O^
    $value = str_replace("Ố", "O", $value);
    $value = str_replace("Ồ", "O", $value);
    $value = str_replace("Ổ", "O", $value);
    $value = str_replace("Ô", "O", $value);
    $value = str_replace("Ộ", "O", $value);
    #---------------------------------o*
    $value = str_replace("ớ", "o", $value);
    $value = str_replace("ờ", "o", $value);
    $value = str_replace("ở", "o", $value);
    $value = str_replace("ỡ", "o", $value);
    $value = str_replace("ợ", "o", $value);

    #---------------------------------O*
    $value = str_replace("Ớ", "O", $value);
    $value = str_replace("Ờ", "O", $value);
    $value = str_replace("Ở", "O", $value);
    $value = str_replace("Ỡ", "O", $value);
    $value = str_replace("Ợ", "O", $value);
    #---------------------------------u*
    $value = str_replace("ứ", "u", $value);
    $value = str_replace("ừ", "u", $value);
    $value = str_replace("ử", "u", $value);
    $value = str_replace("ữ", "u", $value);
    $value = str_replace("ự", "u", $value);
    #---------------------------------U*
    $value = str_replace("Ứ", "U", $value);
    $value = str_replace("Ừ", "U", $value);
    $value = str_replace("Ử", "U", $value);
    $value = str_replace("Ữ", "U", $value);
    $value = str_replace("Ự", "U", $value);
    #---------------------------------y
    $value = str_replace("ý", "y", $value);
    $value = str_replace("ỳ", "y", $value);
    $value = str_replace("ỷ", "y", $value);
    $value = str_replace("ỹ", "y", $value);
    $value = str_replace("ỵ", "y", $value);
    #---------------------------------Y
    $value = str_replace("Ý", "Y", $value);
    $value = str_replace("Ỳ", "Y", $value);
    $value = str_replace("Ỷ", "Y", $value);
    $value = str_replace("Ỹ", "Y", $value);
    $value = str_replace("Ỵ", "Y", $value);
    #---------------------------------DD
    $value = str_replace("Đ", "D", $value);
    $value = str_replace("đ", "d", $value);
    #---------------------------------o
    $value = str_replace("ó", "o", $value);
    $value = str_replace("ò", "o", $value);
    $value = str_replace("ỏ", "o", $value);
    $value = str_replace("õ", "o", $value);
    $value = str_replace("ọ", "o", $value);
    $value = str_replace("ô", "o", $value);
    $value = str_replace("ơ", "o", $value);
    #---------------------------------O
    $value = str_replace("Ó", "O", $value);
    $value = str_replace("Ò", "O", $value);
    $value = str_replace("Ỏ", "O", $value);
    $value = str_replace("Õ", "O", $value);
    $value = str_replace("Ọ", "O", $value);
    $value = str_replace("Ô", "O", $value);
    $value = str_replace("Ơ", "O", $value);
    #---------------------------------u
    $value = str_replace("ú", "u", $value);
    $value = str_replace("ù", "u", $value);
    $value = str_replace("ủ", "u", $value);
    $value = str_replace("ũ", "u", $value);
    $value = str_replace("ụ", "u", $value);
    $value = str_replace("Ụ", "U", $value);
    $value = str_replace("ư", "u", $value);
    #---------------------------------U
    $value = str_replace("Ú", "U", $value);
    $value = str_replace("Ù", "U", $value);
    $value = str_replace("Ủ", "U", $value);
    $value = str_replace("Ũ", "U", $value);
    $value = str_replace("Ụ", "U", $value);
    $value = str_replace("Ư", "U", $value);
    $value = str_replace(" ", "-", $value);
    $search = array ('&amp;', '&#039;', '&quot;', '&lt;', '&gt;', '&#x005C;', '&#x002F;', '&#40;', '&#41;', '&#42;', '&#91;', '&#93;', '&#33;', '&#x3D;', '&#x23;', '&#x25;', '&#x5E;', '&#x3A;', '&#x7B;', '&#x7D;', '&#x60;', '&#x7E;' );
    $value = str_replace ( $search, " ", $value );
    
    $value = preg_replace( "/([^a-z0-9-\s])/is", "", $value );
    $value = preg_replace( "/[\s]+/", " ", $value );
    $value = preg_replace( "/\s/", "-", $value );
    $value = preg_replace( '/(\-)$/', '', $value );
    $value = preg_replace( '/^(\-)/', '', $value );
    $value = preg_replace( '/[\-]+/', '-', $value );
    
    return $value;
}

}
