<?php
class modJVAMenuHelper {
	var $moduleId;
	function __construct($moduleId){
		$this->moduleId = $moduleId;
	}

	/*
	 * Function using singleton pattern
	 * @Created by joomvision
	 */
	function &getInstance($moduleId){
		static $instance = null;
		if( !$instance ){
			$instance = new modJVAMenuHelper($moduleId);
		}
		return $instance;
	}
	//End function init singleton pattern

	/*
	 * Function show menu from menutype
	 * @Created by joomvision
	 */
	function showMenu($params){
		$menu = & JSite::getMenu();
		$rows = $menu->getItems('menutype', $params->get('menutype'));
		$children = array();
		if(is_array($rows) && count($rows)) {
			foreach ($rows as $v) {
				$pt = $v->parent;
				$list = @ $children[$pt] ? $children[$pt] : array ();
				array_push($list, $v);
				$children[$pt] = $list;
			}
		}
		if($params->get('event_type') == 0){
			$this->mosHoverEventRecurseMenu(0, 0, $children, $params);
		} else {
			$this->mosClickEventRecurseMenu(0,0,$children,$params);
		}
	}
	//End function

	/*
	 * Function check menu whether contain child menu
	 * @Created by joomvision
	 */
	function haveChidren($itemId){
		$db =& JFactory::getDBO();
		$sql = "SELECT COUNT(*) AS total FROM #__menu WHERE parent=".$itemId." AND parent >0  AND published = 1";
		$db->setQuery( $sql );
		return $db->loadResult();
	}
	//End function

	/*
	 * Function get menu and all child of menu using recurse function
	 * @Created by joomvision
	 */
	function mosHoverEventRecurseMenu($id, $level, & $children, & $params){
		if (@ $children[$id]) {		
			if($this->haveChidren($id)  > 0 || $id == 0){
				if($id == 0){
					echo "<ul class=\"jv_maccordion\">";
				} else {
					echo "<ul class=\"jv_amenu_items\" style=\"display: none;\">";
				}
			} else {
				echo "<ul class=\"jv_amenu_items\" style=\"display: none;\">";
			}
			foreach ($children[$id] as $row) {				
				$url = $this->getUrlFromMenuItem($row);
				if($url == '') $url ="#";	
				$itemId  = "jv_amenu".$this->moduleId."_".$row->id;				
				echo "<li id=\"$itemId\" class=\"jv_amenu_item\"><a style=\"display:block;\" class=\"trigger\" href=\"$url\">".$row->name."</a>";							
				$this->mosHoverEventRecurseMenu($row->id,$level+1,$children,$params);
				echo "</li>";
			}
			echo "</ul>";
		}
	}
	//End function

	/*
	 * Function get menu and all child when selecting event click type
	 * @Created by joomvision
	 */
	function mosClickEventRecurseMenu($id, $level, & $children, & $params){
		if (@ $children[$id]) {			
			if($this->haveChidren($id)  > 0 || $id == 0){
				if($id == 0){
					echo "<ul class=\"jv_maccordion\">";
				} else {
					echo "<ul class=\"jv_amenu_items\" style=\"display: none;\">";
				}
			} else {
				echo "<ul class=\"jv_amenu_items\" style=\"display: none;\">";
			}
			foreach ($children[$id] as $row) {
				$isHasChild = $this->haveChidren($row->id);
				$url = $this->getUrlFromMenuItem($row);
				if($url == '') $url ="#";
				$itemId  = "jv_amenu".$this->moduleId."_".$row->id;				
				if($isHasChild){
					echo "<li id=\"$itemId\" class=\"jv_amenu_item\"><div class=\"wrap_link\"><a style=\"display:block;\" class=\"readmore\" href=\"$url\">".$row->name."</a><a class=\"trigger\" href=\"$url\"></a></div>";
				} else {
					echo "<li id=\"$itemId\" class=\"jv_amenu_item last-child\"><a style=\"display:block;\" href=\"$url\">".$row->name."</a>";
				}
				echo "<div class=\"clear\"></div>";
				$this->mosClickEventRecurseMenu($row->id,$level+1,$children,$params);
				echo "</li>";
			}
			echo "</ul>";
		}
	}
	//End
	
	/*
	 * Function get link from row menu item
	 * @Created by joomvision
	 */
	function getUrlFromMenuItem($row){
		$menu = &JSite::getMenu();
		$iParams = new JParameter($row->params);
		switch($row->type){
			case 'separator':
				return '';
				break;
			case 'url':
				if ((strpos($row->link, 'index.php?') === 0) && (strpos($row->link, 'Itemid=') === false)) {
					$url = $row->link.'&amp;Itemid='.$row->id;
				} else {
					$url = $row->link;
				}
				break;
			default :
				$router = JSite::getRouter();
				$url = $router->getMode() == JROUTER_MODE_SEF ? 'index.php?Itemid='.$row->id : $row->link.'&Itemid='.$row->id;
				break;	
		}		
		if($url !=''){//If url not null or empty
			// Handle SSL links
			$iSecure = $iParams->def('secure', 0);
			if ($row->home == 1) {
				$url = JURI::base();
			} elseif (strcasecmp(substr($url, 0, 4), 'http') && (strpos($row->link, 'index.php?') !== false)) {
				$url = JRoute::_($url, true, $iSecure);
			} else {
				$url = str_replace('&', '&amp;', $url);
			}
		}		
		return $url;
	}
	//End get link
}

?>