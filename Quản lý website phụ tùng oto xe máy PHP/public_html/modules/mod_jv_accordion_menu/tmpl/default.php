<?php
JHTML::_('script','jquery.min.js','modules/mod_jv_accordion_menu/assets/js/');
JHTML::_('script','jquery.nestedAccordion.js','modules/mod_jv_accordion_menu/assets/js/');
if($isActiveExpand == 1) JHTML::_('script','amenu_load.js','modules/mod_jv_accordion_menu/assets/js/');
$eventType = $params->get('event_type');
if($eventType == 0) {
	JHTML::_('stylesheet','style_hover.css','modules/mod_jv_accordion_menu/assets/css/');
} else{
	JHTML::_('stylesheet','style_click.css','modules/mod_jv_accordion_menu/assets/css/');
} 
?>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.noConflict();
<?php if($eventType == 0) { ?>
(function($){
	$(function(){
		 $("#jv_amenu_side<?php echo $module->id; ?>").accordion({	
			 event : "hover",		 			
			 initShow : "ul.current",
			 objClass:".jv_maccordion",
			 slide:<?php echo $params->get('is_slide'); ?>			
			});
	});
})(jQuery); 
<?php } else { ?>
(function($){
	$(function(){
		 $("#jv_amenu_side<?php echo $module->id; ?>").accordion({						
			 initShow : "ul.current",
			 objClass:".jv_maccordion",
			 slide:<?php echo $params->get('is_slide'); ?>
			 });
	});
})(jQuery); 	
<?php }?>
<?php if($isActiveExpand == 1){ ?>
(function($){
	$(function(){
		 $("#jv_amenu_side<?php echo $module->id; ?>").aMenuLoad({
			 activeItemId:<?php echo $actItemId; ?>,
			 moduleId:<?php echo $module->id; ?>,
			eventType:<?php echo $eventType; ?>				
	});
});			
})(jQuery);
<?php } ?>
//--><!]]>
</script>
<div style="display: none;">Developed by <a href="http://www.joomvision.com" title="Joomla Templates, Joomla Extentions">JoomVision.com</a></div>
<?php if($eventType == 0) {?>
<div class="jv_ahovermenu_wrap">
<?php }else {?>
<div class="jv_aclickmenu_wrap">
<?php }?>
	<div id="jv_amenu_side<?php echo $module->id; ?>">
		<?php $jvAMenuHelper->showMenu($params); ?>
	</div>
</div>