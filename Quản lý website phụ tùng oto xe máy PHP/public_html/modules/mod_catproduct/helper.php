<?php
/**
* @version		$Id: helper.php 10857 2008-08-30 06:41:16Z willebil $
* @package		dmt
* @copyright	Phan Phuoc Long.
* @license		DanangServices
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');

class modcatproductHelper
{
	 function getChildCategory($parentid) {
        $db =& JFactory::getDBO();
        $query = "SELECT * FROM #__pplshop_category WHERE parentid = '$parentid' ORDER BY ordering ASC";
        $db->setQuery($query);
        $listChild = $db->loadObjectList();
        return $listChild;    
    } 
}
