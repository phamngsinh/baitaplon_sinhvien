<?php 
/**
* @version 1.5.x
* @package JoomVision Project
* @copyright (C) 2008 http://www.JoomVision.com. All rights reserved.
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
JHTML::_('stylesheet','jvslidesmooth_css.php?id='.$module->id.'&amp;width='.$params->get('jv2_width').'&amp;height='.$params->get('jv2_height'),'modules/mod_jv_headline/assets/css/');
JHTML::_('script','jd.gallery.js','modules/mod_jv_headline/assets/js/');
JHTML::_('script','jd.gallery.transitions.js','modules/mod_jv_headline/assets/js/');
$modWidthCss = "width:".$params->get('jv2_width')."px";
$modHeightCss = "height:".$moduleHeight."px";
?>
<script type="text/javascript">
function startSlideshow<?php echo $module->id; ?>() {
    var mySlideshow<?php echo $module->id; ?> = new gallery($('mySlideshow<?php echo $module->id; ?>'), {
        timed:<?php echo $autoRun; ?>,        
        defaultTransition:'<?php echo $params->get('jv2_effect'); ?>',       
        baseClass: 'jdSlideshow',       
        showCarousel:false,
        embedLinks:true      
    });
}
window.addEvent('load',startSlideshow<?php echo $module->id; ?>);
</script>
<div style="display: none;">Developed by <a href="http://www.joomvision.com" title="Joomla Templates, Joomla Extentions">JoomVision.com</a></div>
<div style="<?php echo $modHeightCss.";".$modWidthCss; ?>" id="mySlideshow<?php echo $module->id; ?>">
<?php foreach($slides as $slide) : ?>
<div class="imageElement">
    <h3><a href="<?php echo $slide->link; ?>"><?php echo $slide->title; ?></a></h3>
    <p><?php echo $slide->introtext; ?></p>    
    <a href="<?php echo $slide->link; ?>" class="open" title="<?php echo $slide->title; ?>"></a> 
    <?php if($slide->thumbl) { ?> 
    <img src="<?php echo $slide->thumbl; ?>" class="full" alt="<?php echo $slide->title; ?>" />   
    <?php } else { ?>
   <img alt="<?php echo $slide->title; ?>" class="full" src ="<?php echo JURI::base().'modules/mod_jv_headline/assets/images/nophoto.png'; ?>" />
    <?php } ?>
</div>
<?php endforeach; ?>
</div>