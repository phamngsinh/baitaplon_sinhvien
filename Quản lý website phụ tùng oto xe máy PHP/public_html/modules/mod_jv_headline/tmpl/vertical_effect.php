<?php 
/**
* @version 1.5.x
* @package JoomVision Project
* @email webmaster@joomvision.com
* @copyright (C) 2008 http://www.JoomVision.com. All rights reserved.
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.mootools');
$document = &JFactory::getDocument();
$css = JURI::base().'modules/mod_jv_headline/assets/css/vertical.css';
$document->addStyleSheet($css);
JHTML::_('stylesheet','jvslidesmooth_css.php?id='.$module->id.'&amp;width='.$slide_width.'&amp;height='.$moduleHeight,'modules/mod_jv_headline/assets/css/');
//Add lib js of smooth gallery
JHTML::_('script','jd.gallery.js','modules/mod_jv_headline/assets/js/');
JHTML::_('script','jd.gallery.transitions.js','modules/mod_jv_headline/assets/js/');
?>
<script type="text/javascript">
  function startSlideshow<?php echo $module->id; ?>() {
    var mySlideshow<?php echo $module->id; ?> = new gallery($('mySlideshow<?php echo $module->id; ?>'), {       
        defaultTransition:'<?php echo $params->get('lago_animation'); ?>',
        timed:true,       
        baseClass: 'jdSlideshow',              
        embedLinks:false,        
        showCarousel: false,       
        showInfopane:true,               
        slideBarItem:'<?php echo $slideBarItem; ?>',    
        fadeDuration:<?php echo $params->get('trans_duration',500); ?>,
        showArrows:<?php if($showButNext == 1) : echo "true"; else : echo "false"; endif; ?>,        
        delay:'<?php echo $slideDelay; ?>',
        isNormalSmooth:0     
    });
}
window.addEvent('domready',startSlideshow<?php echo $module->id; ?>);
</script>
<div style="display: none;">Developed by <a href="http://www.joomvision.com" title="Joomla Templates, Joomla Extentions">JoomVision.com</a></div>
<div id="<?php echo $divHeadLine; ?>" class="jv_lagoheadline_wrap">
<div class="jv-maskslide" style=" <?php  echo $css_width; ?>;<?php echo $css_height; ?>">
<div id="slide_wrapper<?php echo $module->id; ?>">
<div id="slide_outer<?php echo $module->id; ?>" class="slide_outer">
  <div id="mySlideshow<?php echo $module->id; ?>">
     <?php foreach($slides as $item) { ?>
    <div class="imageElement">
        <h3><a class="smooth_link" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a></h3>
        <p><?php echo $item->introtext; ?></p>   
      <?php if($item->thumbl) { ?>      
       <img src="<?php echo $item->thumbl; ?>" class="full" alt="<?php echo $item->title; ?>" /> 
       <img src="<?php echo $item->thumbl; ?>" class="thumbnail" width="100" height="100" alt="<?php echo $item->title; ?>" />
       <?php } else { ?>
       <img border="0" alt="" class="full" src ="<?php echo JURI::base().'modules/mod_jv_headline/assets/images/nophoto.png'; ?>" />
       <?php } ?>
    </div>
    <?php } ?>
  </div>
</div></div>
</div>
<div id="jv_pagislide<?php echo $module->id; ?>" class="jv-pagislide" style="<?php echo $css_height; ?>;<?php echo $css_slidebar_width; ?>">
<?php foreach($slides as $item) { ?>
    <div class="slideitem" style="<?php echo $css_item_heigth; ?>">
       <?php if($item->thumbs !=''){ ?>       
      <img border="0" alt="thumbnail" src ="<?php echo JURI::base().$item->thumbs; ?>" />
       <?php } ?>
       <a class="slidetitle" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>     
       <p class="slidedes"><?php echo $item->introtext; ?></p>
    </div>
<?php } ?>
</div>    
</div>
