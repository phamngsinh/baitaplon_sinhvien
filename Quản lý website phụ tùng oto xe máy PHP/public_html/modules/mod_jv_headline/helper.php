<?php
/**
 * @version 1.5.x
 * @package JoomVision Project
 * @email webmaster@joomvision.com
 * @copyright (C) 2008 http://www.JoomVision.com. All rights reserved.
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

$user 		= &JFactory::getUser();
$db 		= &JFactory::getDBO();
$menu 		= &JSite::getMenu();
$document	= &JFactory::getDocument();

//..Adding Css to Header
//JHTML::_('stylesheet','jvslidecontent.css','modules/mod_jvheadline/assets/css/');
//JHTML::_('script','jvheadline.js','modules/mod_jvheadline/assets/js/');
//... These params I get from the author of Phoca Gallery, no need in this module. Leave them as default ...........
$moduleclass_sfx = $params->get( 'moduleclass_sfx', 0 );
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');

class modJVHeadLineHelper
{
	function createdDirThumb($comp='com_content',$folderImage=''){
		$thumbImgParentFolder = JPATH_BASE.DS.'images'.DS.'stories'.DS.'thumbs'.DS.$comp.$folderImage;
		if(!JFolder::exists($thumbImgParentFolder)){
			JFolder::create($thumbImgParentFolder);
		}
	}
	function grabData($source_to_grab, $delimiter_start, $delimiter_stop, $str_to_replace='', $str_replace='', $extra_data='')
	{
		$result='';
		$fd = "";
		$start_pos = $end_pos = 0;
		$source_to_grab = $source_to_grab;
		while(true)
		{
			if($end_pos > $start_pos)
			{
				$result = substr($fd, $start_pos, $end_pos-$start_pos);
				$result .= $delimiter_stop;
				break;
			}//10
			$data = @fread($source_to_grab, 8192);
			//echo $data;
			if(strlen($data) == 0) break;
			$fd .= $data;
			if(!$start_pos) $start_pos = strpos($fd, $delimiter_start);
			if($start_pos) $end_pos = strpos(substr($fd, $start_pos), $delimiter_stop) + $start_pos;
		}
		//echo $result;
		return str_replace($str_to_replace, $str_replace, $extra_data.$result);
	}
	/*
	 * Function get thumbnail size
	 * @Created by joomvision
	 */
	function getThumnailSize($params,&$thumbWidth,&$thumbHeight){
		switch($params->get('layout_style')){			
			case "jv_slide4":
				$thumbHeight = $params->get('sello2_thumb_height');
				$thumbWidth = $params->get('sello2_thumb_width');
				break;
			case "jv_slide3":				
				$thumbHeight = $params->get('lago_thumb_height');
				$thumbWidth = $params->get('lago_thumb_width');			
				break;
			case "jv_slide6":
				$thumbHeight = $params->get('sello1_thumb_height');
				$thumbWidth = $params->get('sello1_thumb_width');
				break;	
			case "jv_slide5":
				$thumbHeight = $params->get('maju_thumb_height');
				$thumbWidth = $params->get('maju_thumb_width');	
				break;
			case "jv_slide8":
				$thumbHeight = $params->get('pedon_thumb_height');
				$thumbWidth = $params->get('pedon_thumb_width');	
				break;			
		}
	}
	//End get thumbnail size

	/*
	 *Function get large thumbnail
	 *@Created by joomvision
	 */
	function getLargeThumbSize($params,&$thumbWidth,&$thumbHeight){
		switch($params->get('layout_style')){			
			case "jv_slide8":
				$thumbHeight = $params->get('jv_pedon_height');
				$thumbWidth = $params->get('jv_pedon_width');
				break;
			case "jv_slide6":
				$thumbWidth = $params->get('sello1_imgslide_width');
				$thumbHeight = $params->get('sello1_imgslide_height');			
				break;
			case "jv_slide7":
				$thumbWidth = $params->get('jv7_main_width');
				$thumbHeight = $params->get('jv7_height');
				break;
			case "jv_slide3":
				$thumbHeight = $params->get('jv_lago_height');
				$thumbWidth = $params->get('jv_lago_main_width');
				break;
			case "jv_slide2":
				$thumbWidth = $params->get('jv2_width');
				$thumbHeight = $params->get('jv2_height');
				break;
			case "jv_slide5":
				$thumbWidth = $params->get('jv_maju_width');
				$thumbHeight = $params->get('jv_maju_height');
				break;	
			case "jv_slide1":
				$thumbWidth= $params->get('news_thumb_width');
				$thumbHeight = $params->get('news_thumb_height');
				break;	
				
		}
	}
	//End get large thumbnail	
	function getImageSizes($file) {
		return getimagesize($file);
	}

	function checkImage($file) {
		preg_match("/\<img.+?src=\"(.+?)\".+?\/>/", $file, $matches);
		if(count($matches)){
			return $matches[1];
		} else {return '';}
	}

	function FileExists($file) {
		if(file_exists($file))
		return true;
		else
		return false;
	}

	function FileDifferentExists($file) {
		$check = @fopen($file, "r");
		if(strpos($check, "Resource id") !== false)
		return true;
		else
		return false;
	}	
	function getThumb($text, $extraname, $tWidth,$tHeight, $reflections=false,$id=0,$isSmall = 1){
		preg_match("/\<img.+?src=\"(.+?)\".+?\/>/", $text, $matches);
		$paths = array();
		$showbug = true;
		if (isset($matches[1])) {
			$image_path = $matches[1];
			//joomla 1.5 only
			$full_url = JURI::base();
			//remove any protocol/site info from the image path
			$parsed_url = parse_url($full_url);
			$paths[] = $full_url;
			if (isset($parsed_url['path']) && $parsed_url['path'] != "/") $paths[] = $parsed_url['path'];
			foreach ($paths as $path) {
				if (strpos($image_path,$path) !== false) {
					$image_path = substr($image_path,strpos($image_path, $path)+strlen($path));
				}
			}
			// remove any / that begins the path
			if (substr($image_path, 0 , 1) == '/') $image_path = substr($image_path, 1);
			//if after removing the uri, still has protocol then the image
			//is remote and we don't support thumbs for external images
			if (strpos($image_path,'http://') !== false ||
			strpos($image_path,'https://') !== false) {
				return false;
			}
			// create a thumb filename
			$file_div = strrpos($image_path,'.');
			$thumb_ext = substr($image_path, $file_div);
			$thumb_prev = substr($image_path, 0, $file_div);
			$thumb_path = '';
			if($isSmall == 1){
				$thumb_path = 'images/stories/thumbs/com_content/'.$id.'/thumbs_'.$tWidth.'x'.$tHeight.$thumb_ext;
			} else {
				$thumb_path = 'images/stories/thumbs/com_content/'.$id.'/thumbl_'.$tWidth.'x'.$tHeight.$thumb_ext;
			}
			// check to see if this file exists, if so we don't need to create it
			if ($thumb_path !='' && function_exists("gd_info") && !file_exists($thumb_path)) {
				// file doens't exist, so create it and save it
				include_once('thumbnail.inc.php');
				$thumb = new JVThumbnail($image_path);
				if ($thumb->error) {
					if ($showbug)   echo "JV Image ERROR: " . $thumb->errmsg . ": " . $image_path;
					return false;
				}
				//$thumb->resize($size);
				$thumb->resize_image($tWidth,$tHeight);
				if ($reflections) {
					$thumb->createReflection(30,30,60,false);
				}
				if (!is_writable(dirname($thumb_path))) {
					$thumb->destruct();
					return false;
				}
				$thumb->save($thumb_path);
				$thumb->destruct();
			}
			return ($thumb_path);
		} else {
			return false;
		}
	}
	function introContent( $text, $length=200, $htmltag=0 ) {
		$text = preg_replace( "'<script[^>]*>.*?</script>'si", "", $text );
		$text = preg_replace( '/{.+?}/', '', $text);
		if($htmltag)
		$text = strip_tags(preg_replace( "'<(br[^/>]*?/|hr[^/>]*?/|/(div|h[1-6]|li|p|td))>'si", ' ', $text ),'<a><strong><span><small>');
		else
		$text = strip_tags(preg_replace( "'<(br[^/>]*?/|hr[^/>]*?/|/(div|h[1-6]|li|p|td))>'si", ' ', $text ));
		if (strlen($text) > $length) {
			$text = substr($text, 0, strpos($text, ' ', $length)) . "..." ;
		}
		return $text;
	}
}

class modJVHeadlineCommonHelper{
	function getSlideContent($params){
		global $mainframe;
		$db         =& JFactory::getDBO();
		$user       =& JFactory::getUser();
		$userId     = (int) $user->get('id');

		$categories = ( array )$params->get('categories',array());
		if(count($categories)){
			$strCatId = implode(',',$categories);
			$categoriesCondi = " AND cc.id IN ($strCatId)";
		}
		$intro_lenght = intval($params->get( 'intro_lenght', 200) );		
		//$count      = (int) $params->get('count', 0);
		$count = modJVHeadlineCommonHelper::getNoItemByStyle($params);
		$catid      = trim( $params->get('catid') );
		$secid      = trim( $params->get('secid') );
		$aid        = $user->get('aid', 0);
		//Init width and height of small or large thumbnail
		$sThumbWidth = '';
		$sThumbHeight = '';
		$lThumbWidth ='';
		$lThumbHeight ='';
		//End init
		$layoutStyle = $params->get('layout_style');
		if($layoutStyle !='jv_slide2' && $layoutStyle !='jv_slide1')
		modJVHeadLineHelper::getThumnailSize($params,$sThumbWidth,$sThumbHeight);//Get small thumb size
		if($layoutStyle != 'jv_slide4') modJVHeadLineHelper::getLargeThumbSize($params,$lThumbWidth,$lThumbHeight);

		$htmltag    = $params->get('htmltag');
		$contentConfig = &JComponentHelper::getParams( 'com_content' );
		$access     = !$contentConfig->get('shownoauth');
		$nullDate   = $db->getNullDate();

		$date =& JFactory::getDate();
		$now = $date->toMySQL();
		$where      = 'a.state = 1'
		. ' AND ( a.publish_up = '.$db->Quote($nullDate).' OR a.publish_up <= '.$db->Quote($now).' )'
		. ' AND ( a.publish_down = '.$db->Quote($nullDate).' OR a.publish_down >= '.$db->Quote($now).' )'
		;
		// Ordering
		$ordering       = 'a.id DESC';

		if ($catid)
		{
			$ids = explode( ',', $catid );
			JArrayHelper::toInteger( $ids );
			$catCondition = ' AND (cc.id=' . implode( ' OR cc.id=', $ids ) . ')';
		}
		if ($secid)
		{
			$ids = explode( ',', $secid );
			JArrayHelper::toInteger( $ids );
			$secCondition = ' AND (s.id=' . implode( ' OR s.id=', $ids ) . ')';
		}
		$show_readmore_link = false;
		$catidcount = count(explode( ',', $catid ));
		$secidcount = count(explode( ',', $secid ));
		if (($catidcount==1) && ($secidcount==1)) {
			$show_readmore_link = true;
		}
		$params->set('show_readmore_link', $show_readmore_link);
		// Content Items only
		$query = 'SELECT a.*,a.id as key1, cc.id as key2, cc.title as cat_title, ' .
            ' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug,'.
            ' CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(":", cc.id, cc.alias) ELSE cc.id END as catslug'.
            ' FROM #__content AS a' .             
            ' INNER JOIN #__categories AS cc ON cc.id = a.catid' .
            ' INNER JOIN #__sections AS s ON s.id = a.sectionid' .
            ' WHERE '. $where .' AND s.id > 0' .
		($access ? ' AND a.access <= ' .(int) $aid. ' AND cc.access <= ' .(int) $aid. ' AND s.access <= ' .(int) $aid : '').
		(count($categories) ? $categoriesCondi:'').
            ' AND s.published = 1' .
            ' AND cc.published = 1' .
            ' ORDER BY '. $ordering;
		$db->setQuery($query, 0, $count);
		$rows = $db->loadObjectList();
		$i      = 0;
		$lists  = array();
		$article_count = count($rows);
		modJVHeadLineHelper::createdDirThumb();
		foreach ( $rows as $row )
		{
			$imageurl = modJVHeadLineHelper::checkImage($row->introtext);
			$folderImg = DS.$row->id;
			$lists[$i]->thumb_diff = '';
			modJVHeadLineHelper::createdDirThumb('com_content',$folderImg);
			if(modJVHeadLineHelper::FileExists($imageurl)) {
				$lists[$i]->title = $row->title;
				$lists[$i]->alias = $row->alias;
				$lists[$i]->link = JRoute::_(ContentHelperRoute::getArticleRoute($row->slug, $row->catslug, $row->sectionid));
				$lists[$i]->introtext = modJVHeadLineHelper::introContent($row->introtext, $intro_lenght, $htmltag);
				if($layoutStyle !='jv_slide2' && $layoutStyle !='jv_slide1') {
					$lists[$i]->thumbs = modJVHeadLineHelper::getThumb($row->introtext, '_headline', $sThumbWidth,$sThumbHeight,false,$row->id);
					$images_size = modJVHeadLineHelper::getImageSizes($lists[$i]->thumbs);
					if($images_size[0] != $sThumbWidth || $images_size[1] != $sThumbHeight) {
						@unlink($lists[$i]->thumbs);
						$lists[$i]->thumbs = modJVHeadLineHelper::getThumb($row->introtext, '_headline', $sThumbWidth,$sThumbHeight,false,$row->id);
					}
					if(!modJVHeadLineHelper::FileExists($lists[$i]->thumbs))
					$lists[$i]->thumb_diff = $imageurl;
				}
				if($params->get('layout_style') != 'jv_slide4') {
					$lists[$i]->thumbl = modJVHeadLineHelper::getThumb($row->introtext, '_headline', $lThumbWidth,$lThumbHeight,false,$row->id,0);
					$imagel_size = modJVHeadLineHelper::getImageSizes($lists[$i]->thumbl);
					if($imagel_size[0] !=$lThumbWidth || $imagel_size[1] !=$lThumbHeight){
						@unlink($lists[$i]->thumbl);
						$lists[$i]->thumbl = modJVHeadLineHelper::getThumb($row->introtext, '_headline', $lThumbWidth,$lThumbHeight,false,$row->id,0);
					}
				}
				$i++;
			} else {
				$lists[$i]->title = $row->title;
				$lists[$i]->alias = $row->alias;
				$lists[$i]->link = JRoute::_(ContentHelperRoute::getArticleRoute($row->slug, $row->catslug, $row->sectionid));
				$lists[$i]->introtext = modJVHeadLineHelper::introContent($row->introtext, $intro_lenght, $htmltag);
				$lists[$i]->thumb_diff = $imageurl;
				$lists[$i]->thumbs = '';
				$lists[$i]->thumbl = '';
				$i++;
			}
		}
		return $lists;
	}
	/*
	 * Function get no of item default of each style
	 * @Created by joomvision
	 */	
	function getNoItemByStyle($params){
			switch($params->get('layout_style')){
				case "jv_slide3":
					return (int)$params->get('lago_no_item',3);
					break;
				case "jv_slide2":
					return (int) $params->get('jv2_no_item',5);	
					break;
				case "jv_slide4":
					return (int) $params->get('sello2_no_item',10);
					break;
				case "jv_slide6":
					return (int) $params->get('sello1_no_item',8);
					break;
				case "jv_slide5":
					return (int)$params->get('maju_no_item',4);
					break;	
				case "jv_slide7":
					return (int)$params->get('jv7_no_item',10);
					break;
				case "jv_slide8":
					return (int)$params->get('pedon_no_item',6);
					break;	
				case "jv_slide1":
					return (int)$params->get('news_no_item');
					break;				
				default:
					return (int)$params->get('count',0);
					break;	
			}
	}
	//End functiion

}
class modJVSlide7 {
	function getSlideContent($params){
		global $mainframe;
		$db         =& JFactory::getDBO();
		$user       =& JFactory::getUser();
		$userId     = (int) $user->get('id');

		$categories = ( array )$params->get('categories',array());
		if(count($categories)){
			$strCatId = implode(',',$categories);
			$categoriesCondi = " AND cc.id IN ($strCatId)";
		}
		$intro_lenght = intval($params->get( 'intro_lenght', 200) );
		$count      = modJVHeadlineCommonHelper::getNoItemByStyle($params);
		$catid      = trim( $params->get('catid') );
		$secid      = trim( $params->get('secid') );
		$aid        = $user->get('aid', 0);
		//$thumbs  = $params->get('main_item_width');
		modJVHeadLineHelper::getLargeThumbSize($params,$width,$height);
		$htmltag    = $params->get('htmltag');

		$contentConfig = &JComponentHelper::getParams( 'com_content' );
		$access     = !$contentConfig->get('shownoauth');

		$nullDate   = $db->getNullDate();

		$date =& JFactory::getDate();
		$now = $date->toMySQL();

		$where      = 'a.state = 1'
		. ' AND ( a.publish_up = '.$db->Quote($nullDate).' OR a.publish_up <= '.$db->Quote($now).' )'
		. ' AND ( a.publish_down = '.$db->Quote($nullDate).' OR a.publish_down >= '.$db->Quote($now).' )'
		;
		// Ordering
		$ordering       = 'a.id DESC';

		if ($catid)
		{
			$ids = explode( ',', $catid );
			JArrayHelper::toInteger( $ids );
			$catCondition = ' AND (cc.id=' . implode( ' OR cc.id=', $ids ) . ')';
		}
		if ($secid)
		{
			$ids = explode( ',', $secid );
			JArrayHelper::toInteger( $ids );
			$secCondition = ' AND (s.id=' . implode( ' OR s.id=', $ids ) . ')';
		}
		$show_readmore_link = false;
		$catidcount = count(explode( ',', $catid ));
		$secidcount = count(explode( ',', $secid ));
		if (($catidcount==1) && ($secidcount==1)) {
			$show_readmore_link = true;
		}
		$params->set('show_readmore_link', $show_readmore_link);
		// Content Items only
		$query = 'SELECT a.*,a.id as key1, cc.id as key2, cc.title as cat_title, ' .
            ' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug,'.
            ' CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(":", cc.id, cc.alias) ELSE cc.id END as catslug'.
            ' FROM #__content AS a' .             
            ' INNER JOIN #__categories AS cc ON cc.id = a.catid' .
            ' INNER JOIN #__sections AS s ON s.id = a.sectionid' .
            ' WHERE '. $where .' AND s.id > 0' .
		($access ? ' AND a.access <= ' .(int) $aid. ' AND cc.access <= ' .(int) $aid. ' AND s.access <= ' .(int) $aid : '').
		(count($categories) ? $categoriesCondi:'').
            ' AND s.published = 1' .
            ' AND cc.published = 1' .
            ' ORDER BY '. $ordering;
		$db->setQuery($query, 0, $count);
		$rows = $db->loadObjectList();
		$i      = 0;
		$lists  = array();
		$article_count = count($rows);
		modJVHeadLineHelper::createdDirThumb();
		foreach ( $rows as $row )
		{
			$imageurl = modJVHeadLineHelper::checkImage($row->introtext);
			$folderImg = DS.$row->id;
			modJVHeadLineHelper::createdDirThumb('com_content',$folderImg);
			if(modJVHeadLineHelper::FileExists($imageurl)) {
				$lists[$i]->title = $row->title;
				$lists[$i]->alias = $row->alias;
				$lists[$i]->link = JRoute::_(ContentHelperRoute::getArticleRoute($row->slug, $row->catslug, $row->sectionid));
				$lists[$i]->introtext = modJVHeadLineHelper::introContent($row->introtext, $intro_lenght, $htmltag);
				$lists[$i]->thumb = modJVHeadLineHelper::getThumb($row->introtext, '_headline', $width,$height,false,$row->id);
				$image_size = modJVHeadLineHelper::getImageSizes($lists[$i]->thumb);
				if(!modJVHeadLineHelper::FileExists($lists[$i]->thumb))
				$lists[$i]->thumb_diff = $imageurl;
				$i++;
			} else {
				$lists[$i]->title = $row->title;
				$lists[$i]->alias = $row->alias;
				$lists[$i]->link = JRoute::_(ContentHelperRoute::getArticleRoute($row->slug, $row->catslug, $row->sectionid));
				$lists[$i]->introtext = modJVHeadLineHelper::introContent($row->introtext, $intro_lenght, $htmltag);
				$lists[$i]->thumb_diff = $imageurl;
				$lists[$i]->thumb = '';
				$i++;
			}
		}
		return $lists;
	}	
}
?>