<?php

/* Yahoo Online Support Version: 1.10

* 07 July 2008

* Author : Cristi Teodoreanu - WIS.ro

* mod_support.php, v 1.0

*

*  Copyright (C) Cristi Teodoreanu - WIS.ro

*  Yahoo Online Support is free software distributed under the terms

*  and conditions of the GNU General Public License

*  http://www.gnu.org/copyleft/gpl.html GNU/GPL

*

*/



// no direct access

if(method_exists($mainframe,'registerEvent')){

	defined( '_JEXEC' ) or die( 'Restricted access' );

    $module_path = "/modules/mod_support";

}else{

	defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

    $module_path = "/modules";

}

global $mosConfig_cachepath, $mosConfig_live_site, $mosConfig_absolute_path;

$module_path = $mosConfig_live_site . $module_path;



$out = "<TABLE border=0 cellpadding=2 cellspacing=0 width=100%><tr>";



$support 		= array();

$support_text 	= array();

$support_alt	 	= array();



if($params->get( 'support_display', 1 )) {

	array_push($support,		$params->get( 'support_id' ));

	array_push($support_text, 	$params->get( 'support_text' , 'Technical Support'));

	array_push($support_alt, 	$params->get( 'support_alt' , 'Online Technical Support'));

}

if($params->get( 'contact_display', 1 )) {

	array_push($support, 	$params->get( 'contact_id' ));

	array_push($support_text, 	$params->get( 'contact_text' , 'Customer Support'));

	array_push($support_alt, 	$params->get( 'contact_alt' , 'Online Customer Support'));

}

$width=100/count($support);

for($i=0;$i<count($support);$i++) {

  if(!$support[$i])

	continue;

  $handle = @fsockopen("opi.yahoo.com", 80, $errno, $errstr, 5);

  if (!$handle) {

       echo "Unavailable for the moment<br>";

	continue;

       //echo "$errstr ($errno)<br>\n";

  }

  else {

    @fputs($handle, "GET /online?u=" . $support[$i] . "&m=h&t=1 HTTP/1.0\r\n");

    @fputs($handle, "Host: opi.yahoo.com\r\n");

    @fputs($handle, "Referer: http://opi.yahoo.com/online?u=" . $support[$i] . "&m=h&t=1\r\n");

    @fputs($handle, "User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)\r\n\r\n");

	while (!@feof($handle)){

		$contact = @fgets($handle);

	}

  }

  @fclose($handle);

  $img_online  = "$module_path/mod_support/online.jpg";



  $img_offline = "$module_path/mod_support/offline.jpg";



  if($params->get( 'online_icon'))

    $img_online = "http://opi.yahoo.com/online?u=" . $support[$i] . "&m=g&t=" . $params->get( 'online_icon');	



  if($params->get( 'offline_icon'))

    $img_online = "http://opi.yahoo.com/online?u=" . $support[$i] . "&m=g&t=" . $params->get( 'offline_icon');	

	

  if($contact=="01")

    $img_support= $img_online;

  else

    $img_support= $img_offline;

 if($params->get( 'overall_display')) $img_sep = ""; else $img_sep = "<BR>";

  $out .= "<td align=center width='".$width."%'><a title='".$support_alt[$i]."' href='ymsgr:sendIM?" . $support[$i] . "&m=" . $mosConfig_live_site . "&raquo;'><img 

border='0' valign='absmiddle' src='" . $img_support  . "' alt='".$support_alt[$i]."' align=absmiddle hspace=3>$img_sep".html_entity_decode($support_text[$i])."</a></td>";

 if($params->get( 'overall_display'))

	$out .= "</tr><tr>";

}//for

$c=1;if(!$params->get( 'overall_display')) $c++;

$out .= "</tr> <tr>

          <td colspan=$c>

           <!--Thank you for using the Yahoo support module, please keep the link intact -->

               <div align='center'>

			

		    </div>

              </td>

        </tr>

</table>";

echo $out;

?>



