<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 
JHTML::_('behavior.tooltip');

$Itemid =  "&Itemid=".trim( $params->get( 'menu_id' )); 
$widthimg = trim( $params->get( 'chieurong' ));
$heightimg = trim( $params->get( 'chieucao' ));
$type                     =     trim( $params->get( 'type' )); 
$tocdo                     =     trim( $params->get( 'tocdo' )); 
?>
 <link rel="stylesheet"  href="modules/mod_sanphamnoibat/tmpl/spnoibat.css" type="text/css"    />
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
 <script src="modules/mod_sanphamnoibat/tmpl/slides.min.jquery.js"></script>

  <script>
       jQuery.noConflict();
       $(function(){
            $('#slide<?php echo $type;?>s').slides({
                preload: true,
                preloadImage: 'loading.gif',
                play: <?php echo $tocdo;?>,
                pause: 2500,
                hoverPause: true,
                animationStart: function(current){
                    $('.caption').animate({
                        bottom:-35
                    },100);
                    if (window.console && console.log) {
                        // example return of current slide number
                        console.log('animationStart on slide: ', current);
                    };
                },
                animationComplete: function(current){
                    $('.caption').animate({
                        bottom:0
                    },200);
                    if (window.console && console.log) {
                        // example return of current slide number
                        console.log('animationComplete on slide: ', current);
                    };
                },
                slidesLoaded: function() {
                    $('.caption').animate({
                        bottom:0
                    },200);
                }
            });
        });
    </script>
     <style type="text/css" media="screen">
            #slide<?php echo $type;?>s {
                position: relative;
                z-index:100;
            }
            #slide<?php echo $type;?>s .slides_container {
                width:<?php echo ($widthimg+5);?>px;
                overflow:hidden;
                position:relative;
                display:none;
            }

            /*
                Each slide
                Important:
                Set the width of your slides
                If height not specified height will be set by the slide content
                Set to display block
            */

            #slide<?php echo $type;?>s .slides_container div.slide {
                width:<?php echo ($widthimg+2);?>px;
                height:<?php echo $heightimg;?>px;
                display:block;
                overflow: hidden;
            }


            /*
                Next/prev buttons
            */

            #slide<?php echo $type;?>s .next,  #slide<?php echo $type;?>s .prev {
                position:absolute;
                top:107px;
                left:-5px;
                width:24px;
                height:43px;
                display:block;
                z-index:101;
            }

            #slide<?php echo $type;?>s .next {
                left:211px;
            }

            /*
                Pagination
            */

            .pagination {
                margin:5px auto 0;
                width:<?php echo ($widthimg+2);?>px;
                position: absolute;
                top: 0px;
                left: 0px;
                z-index: 110;
            }

             .pagination li {
                float:left;
                margin:0 1px !important;
                list-style:none;
            }

          
            /*
                Caption
            */

           #slide<?php echo $type;?>s .caption {
                z-index:500;
                position:absolute;
                width: <?php echo ($widthimg-2);?>px;
                left:0;
                height: 25px;
                padding:5px 5px 0 5px;
                background:#000;
                background:rgba(0,0,0,.5);
                width:210;
                font-size:12px;
                line-height:1.33;
                color:#fff;
                border-top:1px solid #000;
                text-shadow:none;
            }
        </style>
 <div id="slide<?php echo $type;?>s">
    <div class="slides_container">
<?php 
      $n=count($list);
      if ($n > 0)
       {
          for ($i=0; $i < $n; $i++)
            {
               $row =& $list[$i];
               $link = 'index.php?option=com_pplshop&task=viewDetails&catid='.$row->category_id.'&cid='. $row->id . $Itemid ; 
               $link1         = JRoute::_( 'index.php?option=com_pplshop&task=addtocart&prod_id='. $rs1->id.$Itemid); 
               $link = JRoute::_($link);
               echo "<div class=\"slide\">";
               echo "<a href='".$link."'><img src=\"administrator/components/com_pplshop/images/products/".$row->image1."\" width=\"".$widthimg."px\">";
               echo "<div class=\"caption\" style=\"bottom:0\">";
               echo "<p>".$row->product_name."</p>";
               echo "</div>";
               echo "</a></div>";
             }
        }
       else
        echo "Chưa có sản phẩm nào.";               
           
?>
    </div>
</div>