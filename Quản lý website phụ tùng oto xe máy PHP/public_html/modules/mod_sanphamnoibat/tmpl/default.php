<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 
JHTML::_('behavior.tooltip');

$Itemid =  "&Itemid=".trim( $params->get( 'menu_id' )); 
$widthimg = trim( $params->get( 'chieurong' ));
$heightimg = trim( $params->get( 'chieucao' ));
$countdis = (int) $params->get('countdis', 2);
$type     =     trim( $params->get( 'type' )); 
$timelen = (int) $params->get('timelen', 5000);
$timelen2 = (int) $params->get('timelen2', 1000);
$dircect = (int) $params->get('dircect', 1000);
?>
<script type="text/javascript" src="modules/mod_sanphamnoibat/tmpl/js/jquery-latest.pack.js" ></script>
<script type="text/javascript" src="modules/mod_sanphamnoibat/tmpl/js/jcarousellite_1.0.1c4.js" ></script>
<script type="text/javascript">
jQuery.noConflict();
jQuery(function() {
    jQuery(".newsticker-jcarousellite<?php echo $type;?>").jCarouselLite({
        <?php echo ($dircect) ? 'vertical: true,' : '' ?>
        hoverPause:true,
        visible: <?php echo $countdis; ?>,
        auto:<?php echo $timelen; ?>,
        speed:<?php echo $timelen2; ?>
    });
});
</script>
<style type="">
.newsticker-jcarousellite<?php echo $type;?> ul li{ display:block; padding:0px; margin:0; }
.newsticker-jcarousellite<?php echo $type;?> ul li a{ padding: 0 !important; margin: 0 !important; border: none !important; display: inline !important; }
.newsticker-jcarousellite<?php echo $type;?> ul li img { margin: 0px !important; padding: 0px !important; border: none !important;}
</style>
<div style="clear: both;">
<div class="newsticker-jcarousellite<?php echo $type;?>">
 <ul>
<?php 
          for ($i=0; $i < count($list); $i++)
            {
               $row =& $list[$i];
               $link = 'index.php?option=com_pplshop&task=viewDetails&catid='.$row->category_id.'&cid='. $row->id . $Itemid ; 
               $link1         = JRoute::_( 'index.php?option=com_pplshop&task=addtocart&prod_id='. $row->id.$Itemid); 
               $link = JRoute::_($link);
               echo "<li>";
               echo "<div id=\"pplshop_img\" style='width:".($widthimg+10)."px !important;'>";
               echo "<div style='float:left; height:".$heightimg."px;'><a href='".$link."'><img src=\"administrator/components/com_pplshop/images/products/".$row->image1."\" width=\"".$widthimg."px\" height=\"".$heightimg."px\" /></a>";
               echo "</div><div class=\"productname\" style=\"margin-top:-30px !important; margin-left:0 !important; width:".($widthimg)."px !important; border: none; \">";
               echo "<a href='".$link."'>".$row->product_name."</a>";
               echo "</div>";
               ?>
               <div id="pplshop_des">               
                    <div style="text-align: center;"><b><?php echo JText::_('PRICE');?>: </b><?php if($row->priceb!=0){ if (JRequest::getVar('lang')=='en')
                        echo $row->priceb; 
                        else echo number_format($row->priceb, 0 ,'', '.');}else{if (JRequest::getVar('lang')=='en')
                        echo $row->price;  else
                         echo number_format($row->price, 0 ,'', '.');}?> <?php echo JText::_('USD');?></div>
                     <div id="pplshop_button">
                        <a href="<?=$link1?>"><span id="pplshop_btn_order"><?php echo JText::_('ORDERPR');?></span> </a>  <a href="<?=$link?>"><span id="pplshop_btn_order"><?php echo JText::_('DETAILS');?></span> </a>
                     </div>                                       
                 </div>
               <?php
               echo "</div>";
               echo "</li>";
             }              
           
?>
    </ul>
</div>
</div>