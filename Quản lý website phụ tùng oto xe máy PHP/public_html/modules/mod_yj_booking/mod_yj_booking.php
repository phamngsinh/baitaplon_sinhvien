<?php
/*======================================================================*\
|| #################################################################### ||
|| # Youjoomla LLC - YJ- Licence Number 1777BP315
|| # Licensed to - Chien Du
|| # ---------------------------------------------------------------- # ||
|| # Copyright (C) 2006-2009 Youjoomla LLC. All Rights Reserved.        ||
|| # This file may not be redistributed in whole or significant part. # ||
|| # ---------------- THIS IS NOT FREE SOFTWARE ---------------- #      ||
|| # http://www.youjoomla.com | http://www.youjoomla.com/license.html # ||
|| #################################################################### ||
\*======================================================================*/
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.');
require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');
/**
 * Scripts and stylesheets
 */
JHTML::_('behavior.mootools');
$document = &JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'modules/mod_yj_booking/files/form_css.css');
$document->addScript(JURI::base() . 'modules/mod_yj_booking/files/SendForm.js');

$document->addScriptDeclaration("
	window.addEvent('domready', function(){
		new AjaxForm({
			dirname: '".JURI::base()."modules/mod_yj_booking/',
			captchaImageId: 'captcha_image',
			formId: 'ReservationForm',
			sendTo: '".JURI::base()."modules/mod_yj_booking/ajax_validate.php',
			messages: 'send_responses',
			loadingMessage:'".JText::_('Thanks for the valuable feedback information for us.')."',
			captchaRefresh: 'captcha_refresh'
		});
	}.bind(this))
");

/**
 * Browser specific CSS tweaks
 */
$who = strtolower($_SERVER['HTTP_USER_AGENT']);

if(eregi("Chrome", $who)) 
{
	$document->addCustomTag('
		<style type="text/css">
		div.reservation fieldset.personal-data{
			height:202px;
			overflow:hidden;
		}
		</style>
	');
}
if(eregi("opera", $who)) 
{
	$document->addCustomTag('
		<style type="text/css">
		div.reservation fieldset.personal-data{
			height:202px;
			overflow:hidden;
		}
		</style>
	');
}
if(eregi("MSIE 6.0" || "MSIE 7.0", $who)) 
{
	$document->addCustomTag('<!--[if lt IE 8]> <style type="text/css">
		div.reservation fieldset.personal-data{
			height:213px;
			overflow:hidden;
		}
		div.reservation fieldset textarea{
			margin:3px 0 0 0;
		}
		</style>
		<![endif]-->
	');
}


/**
 * Parameters
 */
$module_layout = $params->get( 'module_layout', 2); 
$your_email = $params->get( 'your_email');
$SMTP_email = $params->get( 'SMTP_email');
$email_subject = $params->get( 'email_subject');

$sentmsg1 =  $params->get ( 'sentmsg1');
$sentmsg2 =  $params->get ( 'sentmsg2');
/**
 * END OF PARAMETERS
 */

$parts_width = $module_layout ==1 ? '100%' : '25%';
  
/**
 * Get options
 */
$get_items      = $params->get   ('get_items',1);
$nitems         = $params->get   ('nitems',4);
$ordering       = $params->get   ('ordering',3);// 1 = ordering | 2 = popular | 3 = random 
$specificitems 	= $params->get   ('specificitems','');		

switch ($ordering)
{
	case 1:
		$order = 'ordering';
	break;

	case 2:
		$order = 'hits';
	break;
	
	case 3:
		$order = 'RAND()';
	break;
}

$db	=& JFactory::getDBO();
$user =& JFactory::getUser();
$userId = (int) $user->get('id');
$aid = $user->get('aid', 0);
$contentConfig = &JComponentHelper::getParams( 'com_content' );
$access = !$contentConfig->get('shownoauth');
$nullDate = $db->getNullDate();
$date =& JFactory::getDate();
$now = $date->toMySQL(); //date('Y-m-d H:i:s');
$where =  'a.state = 1'
		. ' AND ( a.publish_up = '.$db->Quote($nullDate).' OR a.publish_up <= '.$db->Quote($now).' )'
		. ' AND ( a.publish_down = '.$db->Quote($nullDate).' OR a.publish_down >= '.$db->Quote($now).' )';
$where.= !empty($specificitems) ? ' AND a.id IN ('.$specificitems.')' : ' AND cc.id = '.$get_items.'';
$sql =  'SELECT a.*, ' .
		' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug,'. 
		' CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(":", cc.id, cc.alias) ELSE cc.id END as catslug,'.
		'cc.title as cattitle,'.
		's.title as sectitle'.
		' FROM #__content AS a' .
		' INNER JOIN #__categories AS cc ON cc.id = a.catid' .
		' INNER JOIN #__sections AS s ON s.id = a.sectionid' .
		' WHERE '. $where .'' .
		($access ? ' AND a.access <= ' .(int) $aid. ' AND cc.access <= ' .(int) $aid. ' AND s.access <= ' .(int) $aid : '').
		' AND s.published = 1' .
		' AND cc.published = 1' .
		' ORDER BY '.$order .' LIMIT 0,'.$nitems.'';
$db->setQuery( $sql );
$load_items = $db->loadObjectList();

$booking = array();
foreach ( $load_items as $row ) {
	$book = array(
		'link' => ContentHelperRoute::getArticleRoute($row->slug, $row->catslug, $row->sectionid),
		'title' => $row->title
	);
	$booking[] = $book;
} 
/* fields that need to be filled. verification is done in getmail.php */
$neccesary_fields = array(
	JText::_('FIRST NAME')	=>'first_name',
	JText::_('LAST NAME')	=>'surname',
	JText::_('EMAIL')		=>'email',
	JText::_('PHONE')		=>'phone',
	JText::_('GUESTS')		=>'number_of_guests'	
);
include ("modules/mod_yj_booking/files/getmail.php");
?>

<div id="yj_booking">
	<div id="send_responses" class="messages"></div>
  	<!-- reservation form begin -->
	<form id="ReservationForm" name="yjbooking"  method="post" action="<?php echo stayThere(); ?>">
           <div style="width: 100%; padding: 4px;">
               <table cellpadding="3" cellspacing="3" width="95%">
               
               
                <tr>
                    <td>
                    <textarea style="width:99%; text-align: left; padding: 1px;" rows="4" id="phone" name="phone">  
                    </textarea></td>
                </tr>
                <tr>
                    <td>
                    
                     <div class="captch_img">
                         <img src="<?php echo JURI::base() ;?>modules/mod_yj_booking/captcha.php" alt="verification" id="captcha_image" /> 
                         <span style="width:10px; float: left; height: 20px; font-weight: bold; color: #000;"> = </span>
                         <input type="text"  name="captcha_ver" id="captcha_ver"  value="" class="required validate-numeric" />
                         <a href="#" title="refresh captcha" id="captcha_refresh"></a>
                     </div>                     
                    </td>
                </tr> 
                <tr>
                    <td>
                         <div class="form_butt">    
                             <input type="submit" class="button" name="salji" id="submit_button" value="<?php echo JText::_('SUBMIT') ?>" />        
                         </div>                    
                    </td>
                </tr>       
               </table>  

             </div>           

          <div class="display-none">
				<input type="hidden" id="date" />
				<input type="hidden" name="Subject" value="RESERVATION INFORMATION" />
				<input type="text" id="generatedantispamcode" name="generatedantispamcode"/>
				<input type="text" id="submittedantispamcode" name="submittedantispamcode"/>
				<textarea id="reservation_information" name="RESERVATION_INFORMATION" cols="" rows=""></textarea>
		  </div>

	</form>  
</div>