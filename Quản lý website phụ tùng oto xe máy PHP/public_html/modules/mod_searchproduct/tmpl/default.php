<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 
JHTML::_('behavior.tooltip');

$Itemid =  trim( $params->get( 'menu_id' )); 
$giatus = explode( ',', $params->get( 'giatu' ) );
$giadens = explode( ',', $params->get( 'giaden' ) );
$task = trim($params->get( 'task' ) );
?>
<div style="text-align: center;">
<form action="san-pham.html" method="get">
<input type="hidden" name="option" value="com_pplshop">
<input type="hidden" name="task" value="<?php echo $task;?>">
<div style="padding: 3px 0;">
<input class="textbox" name="keyword" type="text" style="width: 90%; padding: 3px 0;" />
</div>
<div style="padding: 3px 0;">
<select name="catid" style="width: 90%; padding: 3px 0;">
<option value="0"> - <?php echo JText::_('PRODUCT CATEGORIES');?> - </option>
<?php 
      $n=count($list);
          for ($i=0; $i < $n; $i++)
            {
               $row =& $list[$i];
               $selected = ($row->id == JRequest::getInt('catid')) ? " selected = 'selected' " : "";
               echo "<option value='".$row->id."' ".$selected.">".$row->category_name."</option>";
             }             
           
?>
</select>
</div>
<div style="padding: 3px 0;">
<select name="giatu" style="width: 90%; padding: 3px 0;">
<option value="0"> - <?php echo JText::_('PRICE FROM');?> - </option>
 <?php 
      
          for ($j=0; $j < count($giatus); $j++)
            {
               $rowj =& $giatus[$j];
               $selectedgiatu = ($rowj == JRequest::getVar('giatu')) ? " selected = 'selected' " : "";
               echo "<option value='".$rowj."' ".$selectedgiatu.">".number_format($rowj, 0 ,'', '.')." ".JText::_('USD')."</option>";
             }             
           
?>
</select>
</div>
<div style="padding: 3px 0;">
<select name="giaden" style="width: 90%; padding: 3px 0;">
<option value="0"> - <?php echo JText::_('PRICE TO');?> - </option>
 <?php 
      
          for ($jj=0; $jj < count($giadens); $jj++)
            {
               $rowjj =& $giadens[$jj];
               $selectedgiaden = ($rowjj == JRequest::getVar('giaden')) ? " selected = 'selected' " : "";
               echo "<option value='".$rowjj."' ".$selectedgiaden.">".number_format($rowjj, 0 ,'', '.')." ".JText::_('USD')."</option>";
             }             
           
?>
</select>
</div>
<div style="padding: 3px 0;">
<input class="button" type="submit" value="<?php echo JText::_('SEARCH');?>" />
</div>
<input type="hidden" name="Itemid" value="<?=$Itemid?>">

</form>
</div>