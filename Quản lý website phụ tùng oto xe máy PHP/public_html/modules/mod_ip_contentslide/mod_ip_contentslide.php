<?php


if (!defined ('_JEXEC')) {
	define( '_JEXEC', 1 );
	define( 'DS', DIRECTORY_SEPARATOR );

	$path = dirname(__FILE__);
	$path = str_replace ('\\', '/', $path);
	if ($pos = strpos ($path, '/modules/')) {
		$path = substr($path, 0, $pos);
	}
	define('JPATH_BASE', $path );

	require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
	require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );

	JDEBUG ? $_PROFILER->mark( 'afterLoad' ) : null;

	/**
	 * CREATE THE APPLICATION
	 *
	 * NOTE :
	 */
	$mainframe =& JFactory::getApplication('site');

	$mainframe->initialise();

	// trigger the onAfterStart events
	$modid = JArrayHelper::getValue($_GET,'modid',0);
	$query = "SELECT params FROM #__modules WHERE id = $modid;";

	$database = JFactory::getDBO();
	$database->setQuery($query);
	$params = $database->loadResult();
	$params = new JParameter ($params);
	/** get the information about the current user from the sessions table */
	$my = $mainframe->getUser();

}

defined( '_JEXEC' ) or die( 'Restricted access' );
require_once(JPATH_SITE.'/modules/mod_ip_contentslide/mod_ip_contentslide/application.php');

$xheight 		= 	$params->get('xheight',120);
$xwidth 		= 	$params->get('xwidth',160);
$iheight 		= 	$params->get('iheight',80);
$iwidth 		= 	$params->get('iwidth',120);
$numElem 		= 	$params->get('numElem',4);
$catid 			= 	$params->get('catid','');
$showtitle	 	= 	$params->get('showtitle',1);
$showimages 	= 	$params->get('showimages',1);
$showreadmore 	= 	$params->get('showreadmore',0);
$showintrotext 	= 	$params->get('showintrotext',1);
$link_titles 	= 	$params->get('link_titles',1);
$numChar 		= 	$params->get('numchar',0);

$auto 			= 	$params->get('auto',0);
$direction 		=	$params->get('direction','left');
$delaytime 		= 	$params->get('delaytime',5000);
$animationtime 	= 	$params->get('animationtime',1000);
$mootools 		= 	$params->get('mootools',1);

$numberjump 	= 	1;
$useajax 		= 	$params->get('useajax',0);
$usezip			= 	$params->get('usezip',0);


$currentItem 	= 	JArrayHelper::getValue($_REQUEST,'news',0);
$total  		= 	JArrayHelper::getValue($_REQUEST,'total',0);
$database		= 	&JFactory::getDBO();

if($catid)	$catid = "'".implode("','",explode(',',$catid))."'";
$loadajax  		= 	JArrayHelper::getValue($_REQUEST,'loadajax',0);

$jactslide = new JA_ContentSlide($params, $database, $showtitle,$showreadmore,$showintrotext,$link_titles,$numElem,$numberjump,$numChar,$iwidth,$iheight,$showimages);
if(!$total) $total = $jactslide->getTotal($catid);
if($total){
	if($loadajax){
		$jactslide->genHTML_AJAX($catid,$currentItem,$numberjump);
	}
	else{
		$ja_modid = $module->id;
		//Insert link to head (onece)
		JHTML::stylesheet('ja_contentslide.css', JURI::base().'/modules/mod_ip_contentslide/mod_ip_contentslide/');
	?>
		<?php if ($mootools) { ?><script type="text/javascript" src="<?php echo JURI::base(); ?>modules/mod_ip_contentslide/mod_ip_contentslide/mootools.v1.1.js"></script> <?php } ?>
		<script type="text/javascript" src="<?php echo JURI::base(); ?>modules/mod_ip_contentslide/mod_ip_contentslide/ja_contentslide<?php if($usezip) echo ".pak"; ?>.js"></script>
		<script type="text/javascript">
		var options={
			w: <?php echo $xwidth; ?>,
			h: <?php echo $xheight; ?>,
			num_elem: <?php echo $numElem; ?>,
			mode: 'horizontal', //horizontal or virtical
			direction: '<?php echo $direction; ?>', //horizontal: left or right; virtical: up or down
			total: <?php echo $total; ?>,
			url: '<?php echo JURI::base(); ?>modules/mod_ip_contentslide/mod_ip_contentslide.php',
			wrapper: 'ja-contentslider-center',
			duration: <?php echo $animationtime; ?>,
			interval: <?php echo $delaytime; ?>,
			modid: <?php echo $ja_modid;?>,
			running: false,
			auto: <?php echo $auto; ?>

		};
		var jscontentslider = null;
		</script>

		<script type="text/javascript">
		//<!--[CDATA[
		function contentSliderInit () {
			jscontentslider = new JS_ContentSlider(options);
			elems = $('ja-contentslider-center').getElementsByClassName ('content_element');
			for(i=0;i<elems.length;i++){
				jscontentslider.update (elems[i].innerHTML, i);
			}
			jscontentslider.setPos(null);
			if(jscontentslider.options.auto){
				jscontentslider.nextRun();
			}

			$("ja-contentslide-left-img").onmouseover = function(){setDirection('left',0);};
			$("ja-contentslide-left-img").onmouseout = function(){setDirection('left',1);};
			$("ja-contentslide-right-img").onmouseover = function(){setDirection('right',0);};
			$("ja-contentslide-right-img").onmouseout = function(){setDirection('right',1);};
		}

		window.addEvent( 'load', contentSliderInit);

		function setDirection(direction,ret){
			jscontentslider.options.direction = direction;
			if(ret){
				$('ja-contentslide-'+direction+'-img').src = '<?php echo JURI::base(); ?>modules/mod_ip_contentslide/mod_ip_contentslide/re-'+direction+'.gif';
				jscontentslider.options.interval = <?php echo $delaytime; ?>;
				jscontentslider.options.duration = <?php echo $animationtime; ?>;
				jscontentslider.options.auto = <?php echo $auto; ?>;
				jscontentslider.nextRun();
			}
			else{
				$('ja-contentslide-'+direction+'-img').src = '<?php echo JURI::base(); ?>modules/mod_ip_contentslide/mod_ip_contentslide/re-'+direction+'-hover.gif';
				jscontentslider.options.interval = 500;
				jscontentslider.options.duration = 500;
				jscontentslider.options.auto = 1;
				jscontentslider.nextRun();
			}
		}
		//]]-->
		</script>

		<div id="ja-contentslider" class="clearfix" >
		<div id="ja-contentslider-left" style="height: <?php echo $xheight; ?>px; line-height: <?php echo $xheight; ?>px;"><img id="ja-contentslide-left-img" src="<?php echo JURI::base(); ?>modules/mod_ip_contentslide/mod_ip_contentslide/re-left.gif" alt="Left direction" title="Left direction" /></div>
		<div id="ja-contentslider-center">
		<?php $jactslide->genHTML($catid,0,$numElem,$useajax); 		?>


		</div>
		<div id="ja-contentslider-right" style="height: <?php echo $xheight; ?>px;"><img id="ja-contentslide-right-img" src="<?php echo JURI::base(); ?>modules/mod_ip_contentslide/mod_ip_contentslide/re-right.gif" alt="Right direction" title="Right direction" /></div>
		</div>

	<?php
	}
}
else{
	echo '<div id="ja-contentslide-error">JA Content Slide Error: There is not any content in this category </div>';
}
?>

