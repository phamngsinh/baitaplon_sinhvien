<?php
/*------------------------------------------------------------------------
# ------------------------------------------------------------------------
# Copyright (C) 2004-2006 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
# @license - Copyrighted Commercial Software
# Author: J.O.O.M Solutions Co., Ltd
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

class JA_ContentSlide{

	var $_params = '';
	var $_db = '';
	var $_show_price = 0;
	var $_show_addtocart = 0;
	var $_typeproduct = 0;
	var $_numElem = 0;
	var	$_numberjump = 1;
	var	$_total = 0;
	var $_listPro = '';
	var $_now 		= '';
	var $_access 	= '';
	var $_nullDate 	='';
	var $_numChar = '';
	var $_link_titles = '';
	var $_width 	 = '';
	var $_height 	 = '';
	var $_showimages  = '';
    var $_catid  = '';  
	//var $_listPro = array();
	////////////

	function JA_ContentSlide($params, $database, $showtitle,$showreadmore,$showintrotext,$link_titles ,$numElem,$numberjump,$numChar,$xwidth,$xheight,$showimages){
		$mainframe =& JFactory::getApplication('site');
		$this->_params 			= 	$params;
		$this->_db 				= 	$database;
		$this->_showtitle 		= 	$showtitle;
		$this->_showreadmore	= 	$showreadmore;
		$this->_showintrotext 	= 	$showintrotext;
		$this->_link_titles 	= 	$link_titles;
		$this->_numElem 		= 	$numElem;
		$this->_numberjump 		= 	$numberjump;
		$this->_numChar 		= 	$numChar;
		$this->_width 			= 	$xwidth;
		$this->_height 			= 	$xheight;
		$this->_showimages 		= 	$showimages;
       // $this->_catid         =     $catid;   

		$this->_now 			= date( 'Y-m-d H:i' );
		$this->_access 			= !$mainframe->getCfg( 'shownoauth' );
		$this->_nullDate 		= $this->_db->getNullDate();

		$this->_params->set( 'image', 		1 );
		$this->_params->set( 'intro_only', 		1 );
		$this->_params->set( 'hide_author', 		1 );
		$this->_params->set( 'hide_createdate', 	0 );
		$this->_params->set( 'hide_modifydate', 	1 );
		$this->_params->set( 'link_titles', 		1 );

	}
    /*
	function getTotal($catid){
		$my = &JFactory::getUser();

		$query 	= 	" SELECT COUNT(a.id)"
					. "\n FROM #__content AS a"
					. "\n WHERE a.state = 1"
					. "\n AND ( a.publish_up = " . $this->_db->Quote( $this->_nullDate ) . " OR a.publish_up <= " . $this->_db->Quote( $this->_now ) . " )"
					. "\n AND ( a.publish_down = " . $this->_db->Quote( $this->_nullDate ) . " OR a.publish_down >= " . $this->_db->Quote( $this->_now ) . " )"
					. ( $this->_access ? "\n AND a.access <= " . (int) $my->gid : '' )
					;
		if($catid) $query .= ' AND a.catid IN ('.$catid.') ';
		$this->_db->setQuery($query);
		$this->_total = $this->_db->loadResult();
		return $this->_total;
	}
     */
    function getTotal($catid){
        $my = &JFactory::getUser($catid);

        $query     =     " SELECT COUNT(a.id)"
                    . "\n FROM #__pplshop_product AS a"
                    . "\n WHERE a.published = 1"
                   // . "\n AND ( a.publish_up = " . $this->_db->Quote( $this->_nullDate ) . " OR a.publish_up <= " . $this->_db->Quote( $this->_now ) . " )"
                   // . "\n AND ( a.publish_down = " . $this->_db->Quote( $this->_nullDate ) . " OR a.publish_down >= " . $this->_db->Quote( $this->_now ) . " )"
                   // . ( $this->_access ? "\n AND a.access <= " . (int) $my->gid : '' )
                    ;
        if($catid) $query .= ' AND a.category_id IN ('.$catid.') ';
        $this->_db->setQuery($query);
        $this->_total = $this->_db->loadResult();
        return $this->_total;
    }
     /* 
	function loadContents($catid,$start=0,$numberE=1,$useajax = 1){
		$my = &JFactory::getUser();
		$query 	= 	" SELECT a.*"
					. "\n FROM #__content AS a"
					. "\n WHERE a.state = 1"
					. "\n AND ( a.publish_up = " . $this->_db->Quote( $this->_nullDate ) . " OR a.publish_up <= " . $this->_db->Quote( $this->_now ) . " )"
					. "\n AND ( a.publish_down = " . $this->_db->Quote( $this->_nullDate ) . " OR a.publish_down >= " . $this->_db->Quote( $this->_now ) . " )"
					. ( $this->_access ? "\n AND a.access <= " . (int) $my->gid : '' )
					;
		if($catid) $query .= ' AND a.catid IN ('.$catid.') ';
		if($useajax) $query .=  ' LIMIT '.$start.','.$numberE;

		$this->_db->setQuery($query);
		return $this->_listPro = $this->_db->loadObjectlist();
	}
    */
    function loadContents($catid,$start=0,$numberE=1,$useajax = 1){
        $my = &JFactory::getUser();
        $query     =     " SELECT a.*"
                    . "\n FROM #__pplshop_product AS a"
                    . "\n WHERE a.published = 1"
                    //. "\n AND ( a.publish_up = " . $this->_db->Quote( $this->_nullDate ) . " OR a.publish_up <= " . $this->_db->Quote( $this->_now ) . " )"
                    //. "\n AND ( a.publish_down = " . $this->_db->Quote( $this->_nullDate ) . " OR a.publish_down >= " . $this->_db->Quote( $this->_now ) . " )"
                   // . ( $this->_access ? "\n AND a.access <= " . (int) $my->gid : '' )
                    ;
        if($catid) $query .= ' AND a.category_id IN ('.$catid.')';
        $query .= " ORDER BY a.created DESC";
        if($useajax) $query .=  ' LIMIT '.$start.','.$numberE;
        $this->_db->setQuery($query);
        return $this->_listPro = $this->_db->loadObjectlist();
    }
      
	function genHTML($catid, $start = 0 ,$numElem = 4,$useajax){
		$mainframe =& JFactory::getApplication('site');
		//JPluginHelper::importPlugin( 'content' );

		$contents = $this->loadContents($catid,$start,$numElem,$useajax);
		$i=0;
		foreach($contents as $contn){
			echo "<div class=\"content_element\" style=\"float:left;\">";

				// get Itemid
				$Itemid =  $mainframe->getItemid( $contn->id );
				// Blank itemid checker for SEF
				/*if ($Itemid == NULL) {
					$Itemid = '';
				} else {
					$Itemid = '&amp;Itemid='. $Itemid;
				}*/
                
				$link = JRoute::_( 'index.php?option=com_pplshop&task=viewDetails&cid[]='. $contn->id .'&Itemid='.JRequest::getVar('Itemid' ));
				

				$contn->text 	= $contn->description;
				$mainframe->triggerEvent( 'onPrepareContent', array( &$contn, &$this->_params, 0 ) , true );
				$contn->introtext = $contn->text;
				//$image = $this->replaceImage ($contn, $this->_numChar,$this->_showimages, $this->_width, $this->_height);////////////////
                $image = "<img src='administrator/components/com_pplshop/images/products/".$contn->image2."' width='".$this->_width."' height= '".$this->_height."' />";

				if($this->_showimages) {
				echo "<div class=\"ja_slideimages clearfix\" align=\"center\">";
					echo $image;
				echo "</div>";
				}
                if($this->_showtitle) {
                    echo "<div class=\"ja_slidetitle\" style=\"min-height: 30px\">";
                    echo ($this->_link_titles) ? '<a href="'.$link.'">'.$contn->product_name.'</a>' : $contn->product_name;
                    echo "</div>";
                }

				/*if($this->_showintrotext) {
				echo "<div class=\"ja_slideintro\">";
					//echo ($this->_numChar) ?$contn->introtext1 : $contn->introtext;
                    echo "<b>Giá:</b> ".number_format($contn->price, 0 , '', '.'). " VNĐ";
                    echo "<br/>";
                    echo "<b>NSX</b>: ".$contn->manufacturer;
				echo "</div>";   
				}                */
			echo "</div>";
			$i++;
		}
	}

	function genHTML_AJAX($catid,$currentItem,$numberjump = 1){
		$mainframe =& JFactory::getApplication('site');

		//JPluginHelper::importPlugin( 'content' );

		$contn = $this->loadContents($catid,$currentItem,$numberjump);
			// get Itemid
			$Itemid = $mainframe->getItemid( $contn[0]->id);
			// Blank itemid checker for SEF
			if ($Itemid == NULL) {
				$Itemid = '';
			} else {
				$Itemid = '&amp;Itemid='. $Itemid;
			}
		//	$link = JRoute::_( 'index.php?option=com_pplshop&task=viewDetails&cid='. $contn[0]->id .'&Itemid='.JRequest::getVar('Itemid') );
            $link = JRoute::_( 'index.php?option=com_pplshop&task=viewDetails&cid[]='. $contn[0]->id .'&Itemid='.JRequest::getVar('Itemid' ));

			$contn[0]->text 	= $contn[0]->description;
			$mainframe->triggerEvent( 'onPrepareContent', array( &$contn[0], &$this->_params, 0 ) , true );
			$contn[0]->introtext = $contn[0]->text;
			//$image = $this->replaceImage ($contn[0], $this->_numChar,$this->_showimages, $this->_width, $this->_height);////////////////
            $image = "<img src='administrator/components/com_pplshop/images/products/".$contn[0]->image2."' width='".$this->_width."' height= '".$this->_height."' />";

			if($this->_showimages && $image) {
			echo "<div class=\"ja_slideimages clearfix\" align=\"center\">";
				echo $image;
			echo "</div>";
			}
            if($this->_showtitle) {
            echo "<div class=\"ja_slidetitle\">";
                echo ($this->_link_titles) ? '<a href="'.$link.'">'.$contn[0]->product_name.'</a>' : $contn[0]->product_name;
            echo "</div>";
            }
            /*
			if($this->_showintrotext) {
			echo "<div class=\"ja_slideintro\">";
				//echo ($this->_numChar) ?$contn[0]->introtext1 : $contn[0]->introtext;
                echo "<b>Giá:</b> ".number_format($contn[0]->price, 0 , '', '.'). " VNĐ";
                    echo "<br/>";
                    echo "<b>NSX</b>: ".$contn[0]->manufacturer;
			echo "</div>";   
			}                  */ 
	}

	function replaceImage( &$row, $maxchars, $showimage, $width = 0, $height = 0 ) {
		//global $database, $_MAMBOTS, $current_charset;

		// expression to search for
		$regex = '/{mosimage\s*.*?}/i';

		$image = $showimage?$this->processImage ( $row, $width, $height ):"";
		if ($image) {
			$row->introtext = trim($row->introtext);
			$row->introtext = preg_replace( $regex, '', $row->introtext );
		} else {
			$regex = "/\<img.*\/\>/";
			preg_match ($regex, $row->introtext, $matches);
			$row->introtext = trim($row->introtext);
			$row->introtext = preg_replace ($regex, '', $row->introtext);

			$image = $showimage && count($matches)?$matches[0]:"";
			if ($image) {
				if ($width) {
					if (is_numeric ($width)) $width = $width;
					$regex = '/(width\s*=\s*\"?)([^\s,^\"]*)/';
					if (preg_match ($regex, $image))
						$image = preg_replace ($regex, '${1}'.$width, $image);
					else {
						$image = substr ($image, 0, -2) . " width=\"$width\" />";
					}
				}
				if ($height) {
					if (is_numeric ($height)) $height = $height;
					$regex = '/(height\s*=\s*\"?)([^\s,^\"]*)/';
					if (preg_match ($regex, $image))
						$image = preg_replace ($regex, '${1}'.$height, $image);
					else {
						$image = substr ($image, 0, -2) . " heigh=\"$height\" />";
					}
				}
			}
		}

		$row->introtext1 = strip_tags($row->introtext);
		if ($maxchars && strlen ($row->introtext) > $maxchars) {
			$row->introtext1 = substr ($row->introtext1, 0, $maxchars) . "...";
		}
		// clean up globals
		return $image;
	}

	function processImage ( &$row, $width, $height ) {
		/* for 1.5 - don't need to use image parameter */
		return 0;
		/* End 1.5 */
	}

}
?>
