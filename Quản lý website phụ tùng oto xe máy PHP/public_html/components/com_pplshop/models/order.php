<?php
 
// No direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.model' );
require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'helpers'.DS.'images.php' ); 
 
/**
 * Hello Model
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class pplshopModelOrder extends JModel
{
    /**
     * Gets the greeting
     * @return string The greeting to be displayed to the user
     */
    function getListProduct()
    {
        jimport('joomla.html.pagination'); 
        $category_id = JRequest::getVar('catid');
        $listcat = $this->getAllCatId($category_id);
        
        $db =& JFactory::getDBO();
        $limit = 12; 
        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
        
        $query = "SELECT * FROM #__pplshop_product WHERE published ='1'";
        if ($category_id) $query .= " AND category_id IN $listcat";
        $query .=" order by id DESC";

        $db->setQuery( $query );
        $listProduct = $db->loadObjectList();
        $total = count($listProduct);
        
        $items['Nav'] = new JPagination($total, $limitstart, $limit);
        $items['listProduct'] = $this->_getList($query, $limitstart, $limit);
        return $items;
    }
    
    function getAllCatId($catid) {
        $query = "SELECT id FROM #__pplshop_category WHERE parentid = '$catid'";
        $this->_db->setQuery($query);
        $listId = $this->_db->loadResultArray();
        $list_temp = implode(',', $listId);
        if(count($listId) > 0) {
            $list = '('.$catid.','.$list_temp.')';
        } else if(count($listId == 0)) {
            $list = '('.$catid.')';
        }
        return $list; 
    }
    
   function getCart()
    {
       $db=& JFactory::getDBO();
       session_start();
       if (is_array($_SESSION['pplCart']) && count($_SESSION['pplCart'])>0)
       {
          foreach($_SESSION['pplCart'] as $key=>$value)
          {
             $item[]=$key;
          }
           $str=implode(",",$item);
       
       }
       else   $str='';
       
       $query ="SELECT * FROM #__pplshop_product WHERE id in ($str)";
       $db->setQuery( $query );
       $Product = $db->loadObjectList();
       return $Product;
    } 
    
    function getidOrder()
    {
        $db =& JFactory::getDBO();
        $query = "SELECT MAX(id) FROM #__pplshop_orders";
        $db->setQuery($query);
        return $db->loadResult(); 
    }      
    
	function getData()
	{
		// Load the Category data
		if ($this->_loadData())
		{
			// Initialize some variables
			$user	=& JFactory::getUser();

			// raise errors
		}

		return $this->_data;
	}
    function getOrder()
    {
        $db=& JFactory::getDBO();
       $resultAll = array();
       $contactInfo;
       $contactRevInfo;
       $menthod = JRequest::getInt('menthod');
       $contactInfo['name'] =  JRequest::getVar('name');
       $contactInfo['email'] =  JRequest::getVar('email');
       $contactInfo['phone'] =  JRequest::getVar('phone');
       $contactInfo['address'] =  JRequest::getVar('address');
       $contactInfo['city'] =  JRequest::getVar('city');  
       $nhanhang = JRequest::getInt('nhanhang');
       if ($nhanhang==1)
       {
           $contactRevInfo = $contactInfo;
       }
       else
       {
           $contactRevInfo['name'] =  JRequest::getVar('name_rev');
           $contactRevInfo['email'] =  JRequest::getVar('email_rev');
           $contactRevInfo['phone'] =  JRequest::getVar('phone_rev');
           $contactRevInfo['address'] =  JRequest::getVar('address_rev');
           $contactRevInfo['city'] =  JRequest::getVar('city_rev'); 
       }  
       // Insert data
 
       session_start();
       if (is_array($_SESSION['pplCart']) && count($_SESSION['pplCart'])>0)
       {
          foreach($_SESSION['pplCart'] as $key=>$value)
          {
             $item[]=$key;
          }
           $str=implode(",",$item);                                                           
       
       }
       else   $str='';
       
       $query ="SELECT * FROM #__pplshop_product WHERE id in ($str)";
       $db->setQuery( $query );

       $resultAll['orderDetail'] = $db->loadObjectList();

       $queryorderdetail = "INSERT INTO #__pplshop_order_details (orderid, prod_id, unitprice, qty, total, shorttext) VALUES ";
       $total=0; 
       $order_id = $this->getidOrder() + 1;
       for ($i=0, $n=count( $resultAll['orderDetail'] ); $i < $n; $i++){           
                 $rs1 =& $resultAll['orderDetail'][$i]; 
                 $total += ($rs1->priceb*$_SESSION['pplCart'][$rs1->id]);
                 $queryorderdetail .= "(".$order_id.",".$rs1->id.",".$rs1->priceb.",".$_SESSION['pplCart'][$rs1->id].", ".($rs1->priceb*$_SESSION['pplCart'][$rs1->id]).", '".$rs1->product_name."'),";
                        
           } 
       $queryorderdetail = substr($queryorderdetail, 0 , strlen($queryorderdetail)-1);
       $queryin  = "INSERT INTO #__pplshop_orders(id, name, email, phone, address, city, total, payment_menthod, name_rev, email_rev, phone_rev, address_rev, city_rev, order_time )";
       $queryin .= " VALUE('".$order_id."','".$contactInfo['name']."','".$contactInfo['email']."','".$contactInfo['phone']."','".$contactInfo['address']."','".$contactInfo['city']."',".$total.",".$menthod;
       $queryin .= " , '".$contactRevInfo['name']."','".$contactRevInfo['email']."','".$contactRevInfo['phone']."','".$contactRevInfo['address']."','".$contactRevInfo['city']."','".date('Y-m-d H:i:s')."' )";
       $db->setQuery($queryin); 
       $db->query();
       
       //Insert data Order details  

       $db->setQuery($queryorderdetail); 
       $db->query();                                                                       
       $resultAll['contactInfo'] = $contactInfo;
       $resultAll['contactRevInfo'] = $contactRevInfo;
       $resultAll['menthod'] = $menthod;
       return $resultAll;
    }
    
    function getCheckout()
    {
        $db=& JFactory::getDBO();
        $resultAll = array();
       session_start();
       if (is_array($_SESSION['pplCart']) && count($_SESSION['pplCart'])>0)
       {
          foreach($_SESSION['pplCart'] as $key=>$value)
          {
             $item[]=$key;
          }
           $str=implode(",",$item);
       
       }
       else   $str='';
       
       $query ="SELECT * FROM #__pplshop_product WHERE id in ($str)";
       $db->setQuery( $query );
       $resultAll['detailOrder'] = $db->loadObjectList();
       return $resultAll;
    }
    
}
