<?php
/**
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://docs.joomla.org/Developing_a_Model-View-Controller_Component_-_Part_1
 * @license    GNU/GPL
*/
 
// no direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.view');
 
/**
 * HTML View class for the HelloWorld Component
 *
 * @package    HelloWorld
 */
 
class pplshopViewProduct extends JView
{
     
    function display($tpl = null)
    {
        $model = $this->getModel();
        $layout = $this->getLayout();
       
        //$layout     = JRequest::getCmd('layout');
        //$task        = JRequest::getCmd('task');

       
            
               if ($layout == 'viewProduct') {
                  $items =$model->getListProduct();
               } else if  ( $layout == 'viewproducten') {
                   $items =$model->getListProductEn();
               }else if  ( $layout == 'viewDetails') {
                $items =$model->getProduct(); 
               }    
                    
        
        
    
        $this->assignRef( 'items', $items );
        parent::display($tpl);
        
    }
    function is_odd($num) {
      if ($num % 2 == 0) {
      return false;
     } else {
        return true;
      }
    }
       
   
}