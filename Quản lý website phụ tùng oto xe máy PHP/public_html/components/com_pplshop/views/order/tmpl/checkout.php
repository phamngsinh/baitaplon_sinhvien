	<link rel="stylesheet"  href="components/com_pplshop/views/product/tmpl/style/pplshop.css" type="text/css"    />
<?php 
    defined('_JEXEC') or die('Restricted access');
   $listProduct = $this->items['listProduct'];
   $Nav = $this->items['Nav']; 
       
?>
    <script>
    function dispInfo(i)
    {
        if (i==1)
         document.getElementById('infoRev').style.display = 'block';
        else if (i==0)
         document.getElementById('infoRev').style.display = 'none';
    }
    function checkInfo()
    {
        frmcheck = document.order;
        if (frmcheck.name.value == '')
         {
             alert('Nhập thông tin Tên người đặt hàng');
             frmcheck.name.focus();
             
         }
        else if (frmcheck.email.value == '')
         {
             alert('Nhập Email người đặt hàng');
             frmcheck.email.focus();
             
         }
        else if (frmcheck.phone.value == '')
         {
             alert('Nhập Số điện thoại người đặt hàng');
             frmcheck.phone.focus();
             
         }
        else if (frmcheck.address.value == '')
         {
             alert('Nhập thông tin Địa chỉ người đặt hàng');
             frmcheck.address.focus();
             
         }
        else if (document.getElementById('infoRev').style.display == 'block')
         {
             if (frmcheck.name_rev.value == '')
             {
                 alert('Nhập thông tin Họ tên người nhận hàng');
                 frmcheck.name_rev.focus();
                 
             }
            else if (frmcheck.phone_rev.value == '')
             {
                 alert('Nhập Số điện thoại người nhận hàng');
                 frmcheck.phone_rev.focus();
                 
             }
            else if (frmcheck.address_rev.value == '')
             {
                 alert('Nhập thông tin Địa chỉ người nhận hàng');
                 frmcheck.address_rev.focus();
                 
             }
            else frmcheck.submit(); 
         }                             
        else frmcheck.submit();
    }
    </script>

    <div class="content_pplshop">                
        <div class="breakline">
            <div style="padding: 4px;">
            <?php
            if(isset($_SESSION['pplCart']) && count($_SESSION['pplCart'])>0){  
                ?>
                    <form method="post" name="order" onsubmit="return checkInfo();">
                     <div id="frmContact">
                      <div id="pplshop_com">THÔNG TIN NGƯỜI ĐẶT HÀNG</div>
                        <table width="100%" cellpadding="2" cellspacing="5" style="border: 1px solid green">
                        <tr><td width="150px">Họ và tên (*)</td><td><input type="text" name="name" size="40" /></td></tr>     
                        <tr><td width="150px">Email (*)</td><td><input type="text" name="email" size="40" /></td></tr>
                        <tr><td width="150px">Điện thoại (*)</td><td><input type="text" name="phone" size="40" /></td></tr>
                        <tr><td width="150px">Tỉnh / Thành phố</td><td>
                            <select name='city' style="width:200px">
                                 <option value="TP HCM">TP Hồ Chí Minh</option>
                                <option value="H� N&#7897;i">Hà Nội</option>
                                <option value="&#272;� N&#7861;ng">Đà Nẵng</option>
								<option value="An Giang">An Giang</option>
								<option value="Bà Rịa Vũng Tầu">Bà Rịa Vũng Tầu</option>
								<option value="B�nh D&#432;&#417;ng">Bình Dương</option>
								<option value="B�nh Ph&#432;&#7899;c">Bình Phước</option>
								<option value="B�nh Thu&#7853;n">Bình TThuận</option>
								<option value="B�nh &#272;&#7883;nh">Bình Định</option>
								<option value="B&#7841;c Li�u">Bạc Liêu</option>
								<option value="B&#7855;c C&#7841;n">Bắc Cạn</option>
								<option value="B&#7855;c Giang">Bắc Giang</option>
								<option value="B&#7855;c Ninh">Bắc Ninh</option>
								<option value="B&#7871;n Tre">Bến Tre</option>
								<option value="Cao B&#7857;ng">Cao Bằng</option>
								<option value="C� Mau">Cà Mau</option>
								<option value="C&#7847;n Th&#417;">Cần Thơ</option>
								<option value="Gia Lai">Gia Lai</option>
								<option value="H� Giang">Hà Giang</option>
								<option value="H� Nam">Hà Nam</option>
								<option value="H� T�y">Hà Tây</option>
								<option value="H� T&#297;nh">Hà Tĩnh</option>
								<option value="H�a B�nh">Hòa Binh</option>
								<option value="H&#432;ng Y�n">Hưng Yên</option>
								<option value="H&#7843;i D&#432;&#417;ng">Hải Dương</option>
								<option value="H&#7843;i Ph�ng">Hải Phòng</option>
								<option value="Kh�nh H�a">Khánh Hòa</option>
								<option value="Ki�n Giang">Kiên Giang</option>
								<option value="Kontum">Kontum</option>
								<option value="Lai Ch�u">Lai Châu</option>
								<option value="Long An">Long An</option>
								<option value="L�o Cai">Lào Cai</option>
								<option value="L�m &#272;&#7891;ng">Lâm Đồng</option>
								<option value="L&#7841;ng S&#417;n">L&#7841;ng S&#417;n</option>
								<option value="Nam &#272;&#7883;nh">Nam &#272;&#7883;nh</option>
								<option value="Ngh&#7879; An">Ngh&#7879; An</option>
								<option value="Ninh B�nh">Ninh Bình</option>
								<option value="Ninh Thu&#7853;n">Ninh Thu&#7853;n</option>
								<option value="Ph� Th&#7885;">Phú Thọ</option>
								<option value="Ph� Y�n">Phú Yên</option>
								<option value="Qu&#7843;ng B�nh">Quảng Bình</option>
								<option value="Qu&#7843;ng Nam">Quảng Nam</option>
								<option value="Qu&#7843;ng Ng�i">Quảng Ngãi</option>
								<option value="Qu&#7843;ng Ninh">Quảng Ninh</option>
								<option value="Qu&#7843;ng Tr&#7883;">Quảng Trị</option>
								<option value="S�c Tr&#259;ng">Sóc Trăng</option>
								<option value="S&#417;n La">Sơn La</option>
								<option value="Thanh H�a">Thanh Hóa</option>
								<option value="Th�i B�nh">Thái Bình</option>
								<option value="Th�i Nguy�n">Thái Nguyên</option>
								<option value="Th&#7915;a Thi�n Hu�">Thừa Thiên Huế</option>
								<option value="Ti&#7873;n Giang">Tiền Giang</option>
								<option value="Tr� Vinh">Trà Vinh</option>
								<option value="Tuy�n Quang">Tuyên Quang</option>
								<option value="T�y Ninh">Tây Linh</option>
								<option value="V�nh Long">Vĩnh Long</option>
								<option value="V&#297;nh Ph�c">Vĩnh Phúc</option>
								<option value="Y�n B�i">Yên Bái</option>
								<option value="&#272;&#7855;k L&#7855;k">&#272;&#7855;k L&#7855;k</option>
								<option value="&#272;&#7891;ng Nai">Đồng Nai</option>
								<option value="&#272;&#7891;ng Th�p">Đồng Tháp</option>
                            </select>
                        </td></tr>
                        <tr><td width="150px">Địa chỉ (*)</td><td><textarea name="address" cols="30"></textarea></td></tr>     
                        </table>
                          
                     </div>
                      <div id="frmContact">
                      <div id="pplshop_com"><span style="padding: 0 80px 0 0">THÔNG TIN NGƯỜI NHẬN HÀNG</span><span style="padding: 0 10px 0 0"><input type="radio" name="nhanhang" checked="checked" value="1" onclick="dispInfo(0);">Là người đặt hàng</span><span><input type="radio" name="nhanhang" onclick="dispInfo(1);" value="0">Khác</span></div>
                        <div id='infoRev' style="display: none">
                        <table width="100%" cellpadding="2" cellspacing="5" style="border: 1px solid green">
                        <tr><td width="150px">Họ và tên (*)</td><td><input type="text" name="name_rev" size="40" /></td></tr>     
                         <tr><td width="150px">Email </td><td><input type="text" name="email_rev" size="40" /></td></tr>
                        <tr><td width="150px">Điện thoại (*)</td><td><input type="text" name="phone_rev" size="40" /></td></tr>
                        <tr><td width="150px">Tỉnh / Thành phố</td><td>
                            <select name='city_rev' style="width:200px">
                                 <option value="TP HCM">TP Hồ Chí Minh</option>
                                <option value="H� N&#7897;i">Hà Nội</option>
                                <option value="&#272;� N&#7861;ng">Đà Nẵng</option>
								<option value="An Giang">An Giang</option>
								<option value="Bà Rịa Vũng Tầu">Bà Rịa Vũng Tầu</option>
								<option value="B�nh D&#432;&#417;ng">Bình Dương</option>
								<option value="B�nh Ph&#432;&#7899;c">Bình Phước</option>
								<option value="B�nh Thu&#7853;n">Bình TThuận</option>
								<option value="B�nh &#272;&#7883;nh">Bình Định</option>
								<option value="B&#7841;c Li�u">Bạc Liêu</option>
								<option value="B&#7855;c C&#7841;n">Bắc Cạn</option>
								<option value="B&#7855;c Giang">Bắc Giang</option>
								<option value="B&#7855;c Ninh">Bắc Ninh</option>
								<option value="B&#7871;n Tre">Bến Tre</option>
								<option value="Cao B&#7857;ng">Cao Bằng</option>
								<option value="C� Mau">Cà Mau</option>
								<option value="C&#7847;n Th&#417;">Cần Thơ</option>
								<option value="Gia Lai">Gia Lai</option>
								<option value="H� Giang">Hà Giang</option>
								<option value="H� Nam">Hà Nam</option>
								<option value="H� T�y">Hà Tây</option>
								<option value="H� T&#297;nh">Hà Tĩnh</option>
								<option value="H�a B�nh">Hòa Binh</option>
								<option value="H&#432;ng Y�n">Hưng Yên</option>
								<option value="H&#7843;i D&#432;&#417;ng">Hải Dương</option>
								<option value="H&#7843;i Ph�ng">Hải Phòng</option>
								<option value="Kh�nh H�a">Khánh Hòa</option>
								<option value="Ki�n Giang">Kiên Giang</option>
								<option value="Kontum">Kontum</option>
								<option value="Lai Ch�u">Lai Châu</option>
								<option value="Long An">Long An</option>
								<option value="L�o Cai">Lào Cai</option>
								<option value="L�m &#272;&#7891;ng">Lâm Đồng</option>
								<option value="L&#7841;ng S&#417;n">L&#7841;ng S&#417;n</option>
								<option value="Nam &#272;&#7883;nh">Nam &#272;&#7883;nh</option>
								<option value="Ngh&#7879; An">Ngh&#7879; An</option>
								<option value="Ninh B�nh">Ninh Bình</option>
								<option value="Ninh Thu&#7853;n">Ninh Thu&#7853;n</option>
								<option value="Ph� Th&#7885;">Phú Thọ</option>
								<option value="Ph� Y�n">Phú Yên</option>
								<option value="Qu&#7843;ng B�nh">Quảng Bình</option>
								<option value="Qu&#7843;ng Nam">Quảng Nam</option>
								<option value="Qu&#7843;ng Ng�i">Quảng Ngãi</option>
								<option value="Qu&#7843;ng Ninh">Quảng Ninh</option>
								<option value="Qu&#7843;ng Tr&#7883;">Quảng Trị</option>
								<option value="S�c Tr&#259;ng">Sóc Trăng</option>
								<option value="S&#417;n La">Sơn La</option>
								<option value="Thanh H�a">Thanh Hóa</option>
								<option value="Th�i B�nh">Thái Bình</option>
								<option value="Th�i Nguy�n">Thái Nguyên</option>
								<option value="Th&#7915;a Thi�n Hu�">Thừa Thiên Huế</option>
								<option value="Ti&#7873;n Giang">Tiền Giang</option>
								<option value="Tr� Vinh">Trà Vinh</option>
								<option value="Tuy�n Quang">Tuyên Quang</option>
								<option value="T�y Ninh">Tây Linh</option>
								<option value="V�nh Long">Vĩnh Long</option>
								<option value="V&#297;nh Ph�c">Vĩnh Phúc</option>
								<option value="Y�n B�i">Yên Bái</option>
								<option value="&#272;&#7855;k L&#7855;k">&#272;&#7855;k L&#7855;k</option>
								<option value="&#272;&#7891;ng Nai">Đồng Nai</option>
								<option value="&#272;&#7891;ng Th�p">Đồng Tháp</option>
                            </select>
                        </td></tr>            
                        <tr><td width="150px">Địa chỉ (*)</td><td><textarea name="address_rev" cols="30"></textarea></td></tr>     
                        </table>
                        </div>  
                     </div>
                     <div id="frmPayment">
                       <div id="pplshop_com">PHƯƠNG THỨC THANH TOÁN</div>
                        <table width="100%" cellpadding="2" cellspacing="5" style="border: 1px solid green">
                        <tr><td width="10px"><input type="radio" name="menthod" value="1"></td><td>Ghi nợ </td></tr>     
                        <tr><td width="10px"><input type="radio" name="menthod" checked="checked" value="2"></td><td>Chuyển tiền vào tài khoản ngân hàng</td></tr>
                        <tr><td width="10px"><input type="radio" name="menthod" value="3"></td><td>Thanh toán khi nhận hàng </td></tr>
                        
                        </table>
                     </div>
                     <div id="frmVanchuyen">
                     </div>
                       <div>Những phần đánh dấu (*) là thông tin bắt buộc nhập.</div> 
                       <div style="text-align:right; padding:10px 5px 0"><a href="javascript:history.go(-1);"><span id="pplshop_btn_order"> Xem lại đơn hàng</span></a><a href="javascript:checkInfo();"><span id="pplshop_btn_order">Hoàn tất đơn hàng</span></a></div> 
                       <input type="hidden" name="option" value="com_pplshop" />
                       <input type="hidden" name="task" value="vieworder" />
                       <input type="hidden" name="Itemid" value="<?=JRequest::getInt('Itemid')?>" />
                    </form>
                    
             <?php
            }else echo "Bạn chưa chọn sản phẩm để đặt hàng.";                                                                                                                                                                             
             ?>
             </div>
    </div>        
    </div>

