<?php
  defined('_JEXEC') or die();
  jimport('joomla.application.component.view');
  
  class pplshopViewCategory extends JView{
      function display($tpl=null){
          $model = $this->getModel();
          $cat = $model->getAllCat();
          $this->assignRef('cat',$cat);
          parent::display($tpl);
      }
  }
?>
