var current;
var current_hover;
var t1;
function startDropLine (listmenu) {
	listmenu.each(function(el) {
			var _submenu = $E('ul',el);
			if (_submenu) {
				//-- Is active ? --
				if ($E("a.active",el)) {
					current = el;
				}
				el.addEvent('mouseenter', function() {
					current_hover = el;
					showDropLine(el,listmenu);
				});
				el.addEvent('mouseleave', function() {
					current_hover = '';
					showActive(listmenu);
				});
			}
		});
		show_active();
}
function show_active() {
	if (current) {
		$E("ul",current).setStyle('display','block');
	}
}
function showActive(listmenu) {
	if (t1) clearTimeout(t1);
	t1 = setTimeout(function() {
		hide_all(listmenu);
		show_active();
	},700);
}
function hide_all(listmenu) {
	listmenu.each(function(el) {
			if ($E("ul",el)) $E("ul",el).setStyle('display','none');
		});
}	
function showDropLine(el,listmenu) {
	if (t1) clearTimeout(t1);
	hide_all(listmenu);
	$E("ul",el).setStyle('display','block');
}
window.addEvent('load',function() {
	var _listmenu = $$("#menusys_dropline li");
	startDropLine(_listmenu);
});