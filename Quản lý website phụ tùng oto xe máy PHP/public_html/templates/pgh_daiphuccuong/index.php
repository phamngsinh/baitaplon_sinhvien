<?php
/**
* @version 1.5.x
* @package JoomVision Project
* @email webmaster@joomvision.com
* @copyright (C) 2009 http://www.JoomVision.com. All rights reserved.
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
include_once (dirname(__FILE__).DS.'jv_tools.php');


// Javascript
    unset($this->_scripts[$this->baseurl . '/media/system/js/mootools.js']);
    unset($this->_scripts[$this->baseurl . '/media/system/js/caption.js']);
    
    if($gzip == "true") :
    $this->_scripts = array_merge(array($jvTools->templateurl() . 'js/jv.script.js.php' => 'text/javascript'), $this->_scripts);
     else:
	//$this->_scripts = array_merge(array($jvTools->templateurl() . 'js/jv.collapse.js' => 'text/javascript'), $this->_scripts);
	$this->_scripts = array_merge(array($jvTools->templateurl() . 'js/jv.script.js' => 'text/javascript'), $this->_scripts);
    $this->_scripts = array_merge(array($jvTools->templateurl() . 'js/mootools.js' => 'text/javascript'), $this->_scripts);
    endif;
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>">
<head>
<jdoc:include type="head" />
<meta name="google-site-verification" content="u4U0SLCLXJd0oz91keyd__um65C2hPudO1tqUxNWigo" />
<?php JHTML::_('behavior.mootools'); ?>

	<?php if($gzip == "true") : ?>
    <link rel="stylesheet" href="<?php echo $jvTools->templateurl(); ?>css/template.css.php" type="text/css" />
	<?php else: ?>
    <link rel="stylesheet" href="<?php echo $jvTools->templateurl(); ?>css/default.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $jvTools->templateurl(); ?>css/template.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $jvTools->templateurl(); ?>css/typo.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $jvTools->templateurl(); ?>css/k2.css" type="text/css" />  
	<link rel="stylesheet" href="<?php echo $jvTools->templateurl(); ?>css/jvtabs.css" type="text/css" />    
	<link rel="stylesheet" href="<?php echo $jvTools->baseurl(); ?>templates/system/css/system.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $jvTools->baseurl(); ?>templates/system/css/general.css" type="text/css" />	
	<?php endif; ?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-20701503-20']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body id="bd" class="fs<?php echo $jvTools->getParam('jv_font'); ?> <?php echo $jvTools->getParam('jv_display'); ?> <?php echo $jvTools->browser();?>" >
<div id="jv-wrapper" class="clearfix">

	<div id="jv-containerwrap" class="clearfix">
<div>
<?php if($this->countModules('banner')): ?>
					<div id="jv-banner">
						<jdoc:include type="modules" name="banner" style="rounded" />
					</div>
					<?php endif;?>
<!-- <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="986" height="300">
  <param name="movie" value="images/banner.swf" />
  <param name="quality" value="high" />
  <param name="allowScriptAccess" value="always" />
  <param name="wmode" value="transparent">
     <embed src="images/banner.swf"
      quality="high"
      type="application/x-shockwave-flash"
      WMODE="transparent"
      width="986"
      height="300"
      pluginspage="http://www.macromedia.com/go/getflashplayer"
      allowScriptAccess="always" />
</object> -->
</div>    
 <div id="jv-mainmenu" class="clearfix">
            <?php $menu->show(); ?>
            <div style='float:right;height: 0px;'><jdoc:include type="modules" name="user4" /></div>
        </div>
        <?php if ( ( ($jv_menustyle == 'split')  ) || ($jv_menustyle == 'dropline') ):  ?>
        <div id="jv-submenu" class="clearfix">
            <?php if ( $jv_menustyle == 'split' ) : ?>
            <?php $menu->show(1,1); ?>
            <?php endif; ?>
        </div>
        <?php endif; ?>
		<div id="jv-container<?php echo $jv_width; ?>" class="clearfix">

			<?php if($this->countModules('left')) : ?>
			<div class="jv-left-wrapper">
				<div id="jv-left">
					<jdoc:include type="modules" name="left" style="jvrounded" />
				
				</div>
			</div>
			<?php endif; ?>

			<div class="jv-content-wrapper">
				<jdoc:include type="modules" name="slideshow" style="jvrounded" />
				<div>
                    
					<jdoc:include type="modules" name="user2" style="jvrounded" />
                </div>
                <div id="jv-padding">	
					<jdoc:include type="message" />
					
					<jdoc:include type="component" />		
				</div>
                <div>
                    
                    <jdoc:include type="modules" name="user22" style="jvrounded" />
                </div>
			</div>

			<?php if($this->countModules('right')) : ?>
            <div class="jv-right-wrapper">
                <div id="jv-right">
                    <jdoc:include type="modules" name="right" style="jvrounded" />
                
                </div>
            </div>
            <?php endif; ?> 
        
		</div>
            
	</div>
	<!-- BEGIN: User5, User6, user8, user10 -->
		
        
        <?php
	        $spotlight = array ('user5','user6','user8', 'user10');
	        $botsl = $jvTools->calSpotlight ($spotlight,$jvTools->isOP()?100:99,'%');
	        if( $botsl ) {
	    ?>
	    <div class="jv-colspan clearfix" style="height:1px !important; width: 100%;">	
	      
	      	<?php if( $this->countModules('user5') ): ?>
	       		<div class="jv-box<?php echo $botsl['user5']['class']; ?>" style="width: <?php echo $botsl['user5']['width']; ?>;">
				<jdoc:include type="modules" name="user5" style="jvrounded" />
	         	</div>
         	<?php endif; ?>	
	        <?php if( $this->countModules('user6') ): ?>
       			<div class="jv-box<?php echo $botsl['user6']['class']; ?>" style="width: <?php echo $botsl['user6']['width']; ?>;">
				<jdoc:include type="modules" name="user6"  style="jvrounded" />
         		</div>
	        <?php endif; ?>
	        <?php if( $this->countModules('user8') ): ?>
       			<div class="jv-box<?php echo $botsl['user8']['class']; ?>" style="width: <?php echo $botsl['user8']['width']; ?>;">
				<jdoc:include type="modules" name="user8" style="jvrounded" />
         		</div>
	        <?php endif; ?>
	        <?php if( $this->countModules('user10') ): ?>
       			<div class="jv-box<?php echo $botsl['user10']['class']; ?>" style="width: <?php echo $botsl['user10']['width']; ?>;">
				<jdoc:include type="modules" name="user10" style="jvrounded" />
         		</div>
	        <?php endif; ?>
	    </div>
	   	<?php } ?>
	    <!-- END: User5, User6, user8, user10 -->
        <?php if($this->countModules('user3')) : ?>
            
            <div class="copyright"> <jdoc:include type="modules" name="user3" /></div>
            <?php endif;?>    
        
</div>
			
	</div>


</body>
</html>