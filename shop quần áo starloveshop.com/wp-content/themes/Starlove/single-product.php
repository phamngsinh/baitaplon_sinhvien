<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( '' ); ?>

<div id="single-product-content">
	
	<?php wc_print_notices();?>
	<?php while ( have_posts() ) : the_post(); ?>
	<div id="single-image-container">
		 <?php do_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 ); ?>
		 <?php do_action( 'woocommerce_product_thumbnails'); ?>	<?php 			$availability = $product->get_availability();			 			if ($availability['availability']) :				echo apply_filters( 'woocommerce_stock_html', '<div class="index-attribute attribute-stock"><span class="index-attr' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span></div>', $availability['availability'] );			endif;				?>
	</div>
	
		
	
	<div id="single-product-information">
		<h1 class="single-product-title"><?php the_title();?></h1>
		<div class="single-product-attribute">Giá sản phẩm: <span class="price-attr"><?php echo $product->get_price_html();?></span></div>
		<div class="single-product-attribute">Mã sản phẩm: <span class="serial-attr"><?php echo $product->get_sku();?></span></div>
			
		<div class="product-checking">
			<div class="excerpt">
				<?php the_excerpt();?>
			</div>
			<form class="cart" method="post" enctype="multipart/form-data">
			
				<div class="quantity buttons_added" style="width: 100px; float: left;">
					<input type="button" value="-" class="minus" style="width: 25px;">
					<input type="number" step="1" min="1" name="quantity" value="1" title="Qty" class="input-text qty text" size="4" style="width: 35px;">
					<input type="button" value="+" class="plus" style="width: 25px;">
				</div>
				<input type="hidden" name="add-to-cart" value="<?php echo $post->ID;?>">

				<button type="submit" class="single_add_to_cart_button button alt" style="padding: 2px 7px 3px 7px;margin: 0;">Thêm vào giỏ</button>

			</form>
			<div id="cam-ket-chat-luong" class="addition-info">
				<?php echo get_field('thong-tin-chuyen-muc-san-pham-chan-vay');?>
				<?php //echo get_field('thong-tin-chuyen-muc-vay-dam-cong-so');?>
			</div>
		</div>
	
	</div>
	<div id="single-product-ads">
		<img src="http://starloveshop.com/wp-content/uploads/2014/08/quang-cao-ads.jpg">
		<div itemscope="" itemtype="http://schema.org/Recipe">
			<span itemprop="name" style="margin-left: 10px"><?php the_title();?></span>
			<img itemprop="image" width="70" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>" style="margin-right: 40px; float: right;">
			<div itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating" style="margin-left: 10px">
				<span itemprop="ratingValue"><?php echo(rand(7,10));?></span>/<span itemprop="bestRating">10</span>
				<span itemprop="reviewCount"><?php echo(rand(100,1000));?></span> người đánh giá
			</div>
		</div>
	</div>
	

</div>

	<div id="single-product-write">
		<div id="single-product-write-left">
			<?php get_sidebar();?>
		</div>
		<div id="single-product-write-content">
			<h2 class="entry-title"><?php the_title();?></h2>
			<div class="breadcrumbs">
				<?php if(function_exists('bcn_display'))
				{
					bcn_display();
				}?>
			</div>
			<span class="author_mt hp_meta">Thời gian: <?php the_time('l, F jS, Y') ?> </span>
			<?php the_content();?>
			<?php echo $product->get_rating_html(); ?>
			<?php if(function_exists('stt_terms_list')) echo stt_terms_list() ;?>
			<?php woocommerce_product_loop_tags();?>
			<div class="fb-comments" data-href="<?php the_permalink();?>" data-numposts="5" data-colorscheme="light"></div>
			<?php edit_post_link('Sửa bài viết', '<p>', '</p>'); ?>
			<!-- begin custom related loop, isa -->
			<?php echo do_shortcode('[related_products per_page="12"]');?>
			<!-- end custom related loop, isa -->
		</div>
		<?php include 'sidebar-right.php';?>
	</div>
	<?php endwhile; // end of the loop. ?>

<?php get_footer(''); ?>