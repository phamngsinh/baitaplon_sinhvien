<?php
	
	

	add_theme_support( $feature, $arguments );
	add_theme_support( 'post-thumbnails' );

	register_nav_menu('main-menu', __('Menu Chính'));
	register_nav_menu('sub-menu', __('Menu Phụ'));
	register_nav_menu('footer-menu', __('Menu Footer'));
	register_nav_menu('socials-menu', __('Menu MXH'));
	register_nav_menu('tabbed-menu', __('Menu Góc Ẩm Thực'));
	register_sidebar(

	array(

	'name' => __('Sidebar Widget'),

	'before_widget' => '<div id="%1$s" class="widget %2$s">',

	'after_widget' => '</div>',

	'before_title' => '<h4 class="widget-title">',

	'after_title' => '</h4>'

	)

	);
function disqus_embed()
{
    wp_enqueue_script('disqus_embed', 'http://' . $disqus_shortname . '.disqus.com/embed.js');
    echo '<div id="disqus_thread"></div>
    <script type="text/javascript">
      var disqus_shortname = "' . $disqus_shortname . '";
      var disqus_title = "' . $post->post_title . '";
      var disqus_url = "' . get_permalink($post->ID) . '";
      var disqus_identifier = "' . $disqus_shortname . '-' . $post->ID . '";
    </script>';
}
 
function disqus_count($disqus_shortname, $return = false) {
    wp_enqueue_script('disqus_count','http://'.$disqus_shortname.'.disqus.com/count.js');
    if ($return) return '';
    echo '';
}
function disqus_shortname() {
    echo '<script type="text/javascript">// <![CDATA[
var disqus_shortname = "minhnhutblog";
// ]]></script>'; } add_action('wp_head', 'disqus_shortname');

	// Add save percent next to sale item prices.
	add_filter( 'woocommerce_sale_price_html', 'woocommerce_custom_sales_price', 10, 2 );
	function woocommerce_custom_sales_price( $price, $product ) {
		$percentage = round( ( ( $product->regular_price - $product->sale_price ) / $product->regular_price ) * 100 );
		if(is_single()) return $price;
		else return $price . sprintf( __('<span class="percent-sale">%s</span>', 'woocommerce' ), '-' .$percentage .'%' );
	}

	
	function custom_excerpt_length( $length ) {
		return 50;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

	/* ********************
	* Limit post excerpts. Within theme files used as
	* print string_limit_words(get_the_excerpt(), 20);
	******************************************************************** */
	function string_limit_words($string, $word_limit) {
		$words = explode(' ', $string, ($word_limit + 1));
		if(count($words) > $word_limit)
		array_pop($words);
		return implode(' ', $words);
	}

	
	add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_product_loop_tags', 5 );

	function woocommerce_product_loop_tags() {
		global $post, $product;
		$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );
		echo $product->get_tags( ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', $tag_count, 'woocommerce' ) . ' ', '.</span>' );
	}
	add_action('init', 'kraft_woo_publicize');
	function kraft_woo_publicize() {
	  add_post_type_support( 'product', 'publicize' );
	}
	add_filter( ‘clean_url’, ‘defer_parsing_of_js’, 11, 1 );
	
		function getPostViews($postID){
			$count_key = 'post_views_count';
			$count = get_post_meta($postID, $count_key, true);
			if($count==''){
				delete_post_meta($postID, $count_key);
				add_post_meta($postID, $count_key, '0');
				return "0";
		}
		return $count;
	}
	function setPostViews($postID) {
		$count_key = 'post_views_count';
		$count = get_post_meta($postID, $count_key, true);
		if($count==''){
			$count = 0;
			delete_post_meta($postID, $count_key);
			add_post_meta($postID, $count_key, '0');
		}else{
			$count++;
			update_post_meta($postID, $count_key, $count);
		}
	}
	// Remove issues with prefetching adding extra views
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);