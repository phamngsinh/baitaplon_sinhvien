<?php get_header()?>

	
	
	<section class="title-breadcrumbs">
		<div class="inside clear">							
		Chủ đề: <h1 class="title_tour"><?php single_tag_title(); ?></h1>
			<div class="breadcrumbs" style="margin-top: 15px; font-size: 10px;color: #bb0000;">
				<?php 
					if(function_exists('bcn_display'))
					{
						bcn_display();
					}
				?>
			</div>
		
		</div>
	</section>
	
	<section id="main-content">
		<div id="category-list">
			<?php 
				query_posts($query_string. "&order=DESC");
				if(have_posts()) : while (have_posts()) : the_post(); 	
			?>	
			<div class="category-item">
					
				<a href="<?php the_permalink();?>"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" title="<?php printf(the_title_attribute( 'echo=0' ) ); ?>"></a>
				<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
				
				<div class="infomation-item">
					<div class="meta">
					<span class="meta-info">Đăng bởi: <?php the_author();?></span>
					</div>
					<div class="meta">
					<span class="meta-info">Thời gian: <?php the_time('l, F jS, Y') ?></span>
					</div>
					<div class="meta-category">
					Chuyên mục : <?php the_category(' &gt; '); ?>
					</div>
				</div>
				
				
				<div class="excerpt-item">
					<?php the_excerpt();?>
				</div>
				
				<div class="more-list-button">
					<a href="<?php the_permalink();?>" rel="nofollow"><span class="more_item">Xem Chi Tiết</span></a>
				</div>
			
			
			</div>
				
			<?php endwhile; //end first news ?>
			<?php else : ?>
				<p><?php _e('Dữ  liệu đang được cập nhật...'); ?></p>
			<?php endif;?>
				
				<?php wp_reset_query(); ?>
		</div>
		<?php get_sidebar();?>
	</section>

<?php get_footer();?>