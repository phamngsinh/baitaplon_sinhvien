<?php
define('WPLANG', 'vi_VN');
define('WP_MEMORY_LIMIT', '128M');

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'root.vn');

/** MySQL database username */
define('DB_USER', 'root.vn');

/** MySQL database password */
define('DB_PASSWORD', 'root.vn');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'BvBu?mHE,sr2$`Ir[s!W@* jBTOek:)q%J[6L!gEuh8jK}2;?%NQVN[^sarBd=5~');
define('SECURE_AUTH_KEY',  '=8RD+`(4af-X51&:L;z+_w<p@4uXe`#| dv5AA$0{ICRMA8Ht(V$(jx^:Ng SV@I');
define('LOGGED_IN_KEY',    '0ln]H@jJ3a6/~9DzIg%EG+V-JUS:s2|`(vpQLE&N#4Zk;,S- LvyS:-r}*0NZX@~');
define('NONCE_KEY',        '1`J@-.ios?W|li8U3#^B%t-^$y&mwv`Ah[$YX;9TUHy R`%Zdx|C*w-ejw-RMN/^');
define('AUTH_SALT',        '[53Ro[_$JT{[F~-4<?ONd9%n@5bhm7F9xc=fZpt 27rkV/QpC4V^0[k |ihn:VxN');
define('SECURE_AUTH_SALT', '`yCNK8jeQ9-e b]Y7/,5]C^aBCsF1.x$W$[UNq)+(m]~~~x(-[j$Zxqr<T7fw+E]');
define('LOGGED_IN_SALT',   'i0{y.WAoq~6zuc6`DZ@3mf|yVr}>_t+ZB`w8tK=N|N91o]prB2Zd|IN(4a|b[*2`');
define('NONCE_SALT',       'yUFdJhu|h*o71cf0C+ l{^1KM[I5yP7h;aHQ,dp]u0STV=>P5)e*+%-%/|-%& ip');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'vi_VN');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	define('WP_MEMORY_LIMIT', '64M');
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');