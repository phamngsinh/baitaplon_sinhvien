<?php
	class Home extends CI_Controller
	{
		function __construct(){
			parent::__construct();
			$this->load->model('general_model');
		}
		function index()
		{
			$time_now = time();
			$time_out = 60;
			$ip_address = $_SERVER['REMOTE_ADDR'];
			if (!mysql_num_rows(mysql_query("SELECT `ip_address` FROM `online` WHERE UNIX_TIMESTAMP(`last_visit`) + $time_out > $time_now AND `ip_address` = '$ip_address'")))
				mysql_query("INSERT INTO `online` VALUES ('$ip_address', NOW())");
			$lang_id = 1;
			$req = $this->uri->segment(1);
			$req=str_replace(".html","",$req);
			if(!empty($req)){
				$rs = $this->general_model->get_controller($req,$lang_id);
				if(empty($rs))redirect(base_url());
				switch($rs["object"]){
					case "lien-he":
						include(APPPATH.'/controllers/front/general.php');
						$my_controller = new general();
						$my_controller->lien_he($req);
						unset($my_controller); 
						break;
					case "gioi-thieu":
						include(APPPATH.'/controllers/front/general.php');
						$my_controller = new general();
						$my_controller->gioi_thieu($req);
						unset($my_controller); 
						break;
					case "about":
						include(APPPATH.'/controllers/front/about.php');
						$my_controller = new about();
						$my_controller->about_details($rs["object_id"]);
						unset($my_controller); 
						break;
					case "product":
						include(APPPATH.'/controllers/front/product.php');
						$my_controller = new product();
						$my_controller->product_details($rs["object_id"]);
						unset($my_controller); 
						break;
					default : $this->load->view('front/404error');break;
				}
			} else {
				//$data['mainhover'] = '';
				$this->load->model('m_setting');
				$chi_tiet_cau_hinh = $this->m_setting->edit_setting(1);
				$data['title_page'] = $chi_tiet_cau_hinh->title_page;
				$data['description_page'] = $chi_tiet_cau_hinh->description_page;
				$data['keyword_page'] = $chi_tiet_cau_hinh->keyword_page;
				$data['chi_tiet_cau_hinh'] = $chi_tiet_cau_hinh;
				
				//lấy màu nền
				$this->load->model('m_background');
				$data['background'] = $this->m_background->edit_background(1);	
				//lấy menu
				$this->load->model('m_menu');
				$data['menu'] = $this->m_menu->ds_menu();
				//lấy danh sách dịch vụ
				$this->load->model('m_category');
				$data['category'] = $this->m_category->category_list();			
				$this->load->view('front/home',$data);
			}
		}
	}
?>