<?php	
	class General extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->model('general_model');
		}
		function lien_he($seo)
		{
			$seo=$this->general_model->db_seo($seo);
			$data['description_page']=$seo[0]['description_page'];
			$data['keyword_page']=$seo[0]['keyword_page'];
			$data['title_page']=$seo[0]['title_page'];
			$this->load->model('m_setting');
			$chi_tiet_cau_hinh = $this->m_setting->edit_setting(1);
			$data['chi_tiet_cau_hinh']=$chi_tiet_cau_hinh;
			
			if(isset($_POST['action']) && $_POST['action'] == 'Gửi tin'){
				$tendaydu = trim($_POST['tendaydu']);
				$email = trim($_POST['email']);
				$dienthoai = trim($_POST['dienthoai']);
				$nd = trim($_POST['noidung']);
				$this->load->model("m_setting");
				$cauhinh = $this->m_setting->edit_setting(1);
				$this->load->library('email');
				$config['protocol'] = 'smtp';
				$config['smtp_host'] = 'ssl://smtp.gmail.com';
				$config['smtp_port'] = '465';
				$config['smtp_timeout'] = '7';
				$config['smtp_user'] = $cauhinh->smtp_user; 
				$config['smtp_pass'] = $cauhinh->smtp_pass; // nhập pass mail
				$config['charset'] = 'utf-8';
				$config['newline'] = "\r\n";
				$config['mailtype'] = "html";
				$config['validation'] = TRUE;
				$this->email->initialize($config);
				$this->email->from($email,$tendaydu);
				$this->email->to($cauhinh->receive_email); //mail người nhận
				$this->email->subject('Gia công mỹ phẩm trọn gói');
				$noidung = "<strong>Họ tên người liên hệ: </strong>" . $tendaydu . '<br>';
				$noidung.='<strong>Email: </strong>' . $email . '<br><br>';
				$noidung.='<strong>Nội dung: </strong><br>' . $nd;
				$this->email->message($noidung);
				if(!$this->email->send())
					$data['warning'] = 'Có lỗi xảy ra, vui lòng thực hiện lại';
				else{
					$data['success'] = 'Thông tin liên hệ của bạn đã được gửi đi!';
					$this->general_model->add_message($tendaydu,$email,$dienthoai,$nd);	
				}
			}
			//lấy menu
			$this->load->model('m_menu');
			$data['menu'] = $this->m_menu->ds_menu();
			//lấy danh sách dịch vụ
			$this->load->model('m_category');
			$data['category'] = $this->m_category->category_list();
			//lấy màu nền
			$this->load->model('m_background');
			$data['background'] = $this->m_background->edit_background(1);
			$this->load->view('front/contact',$data);
		}
		function gioi_thieu($seo)
		{
			//$data['menuhover'] = 'gioi-thieu.html';
			$seo=$this->general_model->db_seo($seo);	
			$data['description_page']=$seo[0]['description_page'];
			$data['keyword_page']=$seo[0]['keyword_page'];
			$data['title_page']=$seo[0]['title_page'];
			$this->load->model('m_setting');
			$chi_tiet_cau_hinh = $this->m_setting->edit_setting(1);
			$data['chi_tiet_cau_hinh'] = $chi_tiet_cau_hinh;
				
			//lấy menu
			$this->load->model('m_menu');
			$data['menu'] = $this->m_menu->ds_menu();
			//lấy danh sách dịch vụ
			$this->load->model('m_category');
			$data['category'] = $this->m_category->category_list();	
			//lấy màu nền
			$this->load->model('m_background');
			$data['background'] = $this->m_background->edit_background(1);	
			$this->load->view('front/about',$data);
		}
	}
?>