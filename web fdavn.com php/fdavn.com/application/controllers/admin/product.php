<?php class Product extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_product');
	}
	function product_list()
	{
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			$data['pro_active'] = '';
			$data['product']=$this->m_product->product_list();
			$this->load->model('m_category');
			$data['category'] = $this->m_category->category_list();
			$data['path'] = 'admin/product/v_product_list';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
	function add_product()
	{	
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			if(isset($_POST['action']) && $_POST['action'] == 'Add product'){
				$pro_name = trim($this->input->post('pro_name'));
				$pro_brief = trim($this->input->post('pro_brief'));
				$pro_details = trim($this->input->post('pro_details'));
				$pro_price = trim($this->input->post('pro_price'));
				$catp_id = $this->input->post('catp_id');
				$kt = $this->m_product->check_pro_name($pro_name,$catp_id);
				if($kt == 1){
					$data['pro_name_error'] = '<div class="n_warning"><p>Tên sản phẩm đã có. Vui lòng nhập tên khác!</p></div>';
				}else{
					//upload product images
					$this->load->library('upload');
					$config['upload_path'] = './public/product_images/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']	= '20000';
					$config['max_width']  = '1920';
					$config['max_height']  = '1600';
					$hinh_luu = '';
					$so_hinh = $this->input->post('so_hinh');
					for($i = 0; $i < $so_hinh; $i++){
						$ten = 'file'.$i;
						$hinh = time() . str_replace(' ','',$_FILES[$ten]['name']);
						$config['file_name'] = $hinh;
						$hinh_luu .= $hinh . '|';
						$this->upload->initialize($config);
						if(!$this->upload->do_upload($ten)){
							$this->session->set_flashdata('mss', '<div class="n_warning"><p>Hình không hợp lệ. Vui lòng chọn hình khác!</p></div>');
							redirect('cm-admin/add-product');
						}
						$hinh = "";
					}
					$hinh_luu = substr($hinh_luu,0,strlen($hinh_luu)-1);
					//end upload product images
					$kq = $this->m_product->add_product($pro_name,$hinh_luu,$pro_brief,$pro_details,$pro_price,$catp_id);
					$id = mysql_insert_id();
					$this->load->view('front/include/Functions');
					$khongdau = khongdau(strip_tags($pro_name));
					$data=array('lang_id'=>'1','object'=>'product','object_id'=>$id,'alas'=>$khongdau);
					$this->db->insert('seo_name',$data);
					if($kq){
						$this->session->set_flashdata('mss', '<div class="n_ok"><p>Thêm thành công</p></div>');
					}else{
						$this->session->set_flashdata('mss', '<div class="n_error"><p>Thêm không thành công</p></div>');
					}
					redirect('cm-admin/add-product');
				}
			}
			$data['pro_active'] = '';
			$this->load->model('m_category');
			$data['category'] = $this->m_category->category_list();
			$data['path'] = 'admin/product/v_add_product';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
	function edit_product($pro_id)
	{
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			$data['pro_active'] = '';
			$data['product_details']=$this->m_product->edit_product($pro_id);
			if(isset($_POST['action']) && $_POST['action'] == 'Edit product'){
				$pro_name = trim($this->input->post('pro_name'));
				$pro_brief = trim($this->input->post('pro_brief'));
				$pro_details = trim($this->input->post('pro_details'));
				$pro_price = trim($this->input->post('pro_price'));
				$catp_id = $this->input->post('catp_id');
				$this->load->view('front/include/Functions');
				$khongdau = khongdau(strip_tags($pro_name));
				$kt = $this->m_product->check_pro_name_upt($pro_id,$pro_name,$catp_id);
				if($kt == 1){
					$data['pro_name_error'] = '<div class="n_warning"><p>Tên sản phẩm đã có. Vui lòng nhập tên khác!</p></div>';
				}else{
					if($_FILES['file0']['name'] != NULL){
						$this->load->library('upload');
						$config['upload_path'] = './public/product_images/';
						$config['allowed_types'] = 'gif|jpg|png|jpeg';
						$config['max_size']	= '20000';
						$config['max_width']  = '1920';
						$config['max_height']  = '1600';
						$hinh_luu = '';
						$so_hinh = $this->input->post('so_hinh');
						for($i=0;$i<$so_hinh;$i++){
							$ten = 'file'.$i;
							$hinh = time() . str_replace(' ','',$_FILES[$ten]['name']);
							$config['file_name'] = $hinh;
							$hinh_luu .= $hinh . '|';
							$this->upload->initialize($config);
							if(!$this->upload->do_upload($ten)){
								$this->session->set_flashdata('mss', '<div class="n_warning"><p>Hình không hợp lệ. Vui lòng chọn hình khác!</p></div>');
								redirect('cm-admin/edit-product/'.$pro_id);
							}
							$hinh = "";
						}
						$hinh_luu = substr($hinh_luu,0,strlen($hinh_luu)-1);
						$kq = $this->m_product->change_product_with_image($pro_id,$hinh_luu,$pro_name,$pro_brief,$pro_details,$pro_price,$catp_id);
						$chuoi_hinh_cu = $this->input->post('hidden_hinh');
						$mang_hinh_cu = explode('|',$chuoi_hinh_cu);
						for($j=0;$j<count($mang_hinh_cu);$j++){
							$path='./public/product_images/'.$mang_hinh_cu[$j];
							if(file_exists($path)){
								unlink($path);
							}	
						}						
						$data = array('alas'=>$khongdau);
						$this->db->where('object_id',$pro_id);
						$this->db->where('object','product');
						$this->db->update('seo_name',$data);
						if($kq){
							$this->session->set_flashdata('mss', '<div class="n_ok"><p>Cập nhậtthành công</p></div>');
						}else{
							$this->session->set_flashdata('mss', '<div class="n_error"><p>Cập nhật không thành công</p></div>');
						}
						redirect('cm-admin/edit-product/'.$pro_id);
					}else{
						$kq = $this->m_product->change_product($pro_id,$pro_name,$pro_brief,$pro_details,$pro_price,$catp_id);
						if($kq){
							$this->session->set_flashdata('mss', '<div class="n_ok"><p>Cập nhật thành công</p></div>');
						}else{
							$this->session->set_flashdata('mss', '<div class="n_error"><p>Cập nhật không thành công</p></div>');
						}							
						$data = array('alas'=>$khongdau);
						$this->db->where('object_id',$pro_id);
						$this->db->where('object','product');
						$this->db->update('seo_name',$data);
						redirect('cm-admin/edit-product/'.$pro_id);
					}
				}
			}
			$this->load->model('m_category');
			$data['category'] = $this->m_category->category_list();
			$data['path'] = 'admin/product/v_edit_product';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
	function delete_product($pro_id)
	{
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			$data['pro_active'] = '';
			$data['product'] = $this->m_product->edit_product($pro_id);
			if(isset($_POST['action']) && $_POST['action'] == 'Delete product'){
				$xoa = $this->input->post('delete');
				if($xoa == 'yes'){
					$this->m_product->delete_product($pro_id);
					$chuoi_hinh_cu = $this->input->post('hidden_hinh');
					$mang_hinh_cu = explode('|',$chuoi_hinh_cu);
					for($j=0;$j<count($mang_hinh_cu);$j++){
						$path='./public/product_images/'.$mang_hinh_cu[$j];
						if(file_exists($path)){
							unlink($path);
						}	
					}
					$this->db->delete('seo_name', array('object_id' => $pro_id,'object' => 'product'));
					redirect('cm-admin/product-list');
				}else{
					redirect('cm-admin/product-list');
				}
			}
			$data['path'] = 'admin/product/v_delete_product';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
	function load_product_by_catid($catp_id){
		$this->load->model('m_category');
		$data['category'] = $this->m_category->category_list();
		$data['catp_id'] = $catp_id;
		if($catp_id != 0){
			$data['product_by_cat'] = $this->m_product->list_product_for_parent($catp_id);
		}else{
			$data['product_by_cat'] = $this->m_product->product_list();
		}
		$this->load->view('front/product/load_product',$data);
	}
	function delete_all_product()
	{
		$case = $_POST['case'];
		//print_r($case);
		if(count($case)>0){
			foreach($_POST['case'] as $id)
			{
				$product_details = $this->m_product->edit_product($id);
				$mang_hinh = explode('|',$product_details->pro_picture);
				$this->m_product->delete_product($id);
				for($j=0;$j<count($mang_hinh);$j++){
					$path='./public/product_images/'.$mang_hinh[$j];
					if(file_exists($path)){
						unlink($path);
					}	
				}
				$this->db->delete('seo_name', array('object_id' => $id,'object' => 'product'));
			}
		}
		redirect('cm-admin/product-list');
	}
}?>