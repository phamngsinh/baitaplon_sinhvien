<?php class Setting extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_setting');
	}
	function index($id=1){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			if(isset($_POST['action']) && $_POST['action'] == 'Thiết lập'){
				$title_page = trim($this->input->post('title_page'));
				$description_page = trim($this->input->post('description_page'));
				$keyword_page = trim($this->input->post('keyword_page'));
				$analytics = trim($this->input->post('analytics'));
				$facebook_acc = trim($this->input->post('facebook_acc'));
				$twitter_acc = trim($this->input->post('twitter_acc'));
				$google_acc = trim($this->input->post('google_acc'));
				$phone = trim($this->input->post('phone'));	
				$smtpuser = trim($this->input->post('smtpuser'));
				$smtppass = trim($this->input->post('smtppass'));
				$receive_email = trim($this->input->post('receive_email'));
				$about = trim($this->input->post('about'));
				$about1 = trim($this->input->post('about1'));
				$about2 = trim($this->input->post('about2'));
				$footer = trim($this->input->post('footer'));
				$kq = $this->m_setting->change_setting($id,$title_page,$description_page,$keyword_page,$analytics,$facebook_acc,$twitter_acc,$google_acc,$phone,$smtpuser,$smtppass,$receive_email,$about,$about1,$about2,$footer);
				if($kq){
					$this->session->set_flashdata('mss', '<div class="n_ok"><p>Cập nhật thành công</p></div>');
				}else{
					$this->session->set_flashdata('mss', '<div class="n_error"><p>Cập nhật không thành công</p></div>');
				}
				redirect('cm-admin/setting/1');
			}
			$data['edit_setting'] = $this->m_setting->edit_setting($id);
			$data['path'] = 'admin/setting/v_setting';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
}?>
