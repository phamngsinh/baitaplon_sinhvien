<?php class Dang_nhap extends CI_Controller{
	function index()
	{
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			$data['path'] = 'admin/includes/v_trang_chu';
			$this->load->view('admin/layout',$data);
		}else{
			$this->load->view("admin/login");
		}
	}
	function forget_password()
	{
		if(isset($_POST['email']) && $_POST['email'] != null){
			$email = trim($_POST['email']);
			$this->load->model('m_nguoi_dung');
			$kq = $this->m_nguoi_dung->check_email($email);
			if ($kq->num_rows > 0) {
				$r = $kq->result();
				$user = $r[0];
				$test = $this->resetpassword($user);
				$this->session->set_flashdata('mss','<div class="n_ok" style="margin: 0px 0px 10px 0px"><p>Mật khẩu mới đã được gửi đến email của bạn</p></div>');
			}else{
				$this->session->set_flashdata('mss','<div class="warning">Email này không tồn tại</div>');
			}
			redirect('cm-admin/forget-password');
		}
    	$this->load->view('admin/forget_password');
	}
	private function resetpassword($user)
	{
		$this->load->helper('string');
		$mat_khau = random_string('alnum', 16);
		$ma_nguoi_dung = $user->ma_nguoi_dung;
		$this->m_nguoi_dung->reset_password($ma_nguoi_dung,$mat_khau);
		$this->load->model("m_setting");
		$cauhinh = $this->m_setting->edit_setting(1);
		$this->load->library('email');
		$config['protocol'] = 'smtp';
		$config['smtp_host'] = 'ssl://smtp.gmail.com';
		$config['smtp_port'] = '465';
		$config['smtp_timeout'] = '7';
		$config['smtp_user'] = $cauhinh->smtp_user; 
		$config['smtp_pass'] = $cauhinh->smtp_pass;
		$config['charset'] = 'utf-8';
		$config['newline'] = "\r\n";
		$config['mailtype'] = "html";
		$config['validation'] = TRUE;
		$this->email->initialize($config);
		$this->email->from($cauhinh->receive_email,$cauhinh->title_page);
		$this->email->to($user->email);
		$this->email->subject('Mật khẩu mới tại ' . $cauhinh->title_page);
		$this->email->message('Bạn vừa yêu cầu gửi lại mật khẩu mới từ webiste Gia công mỹ phẩm.<br/><br/>Đây là mật khẩu mới của bạn: ' . $mat_khau);
		$this->email->send();
	}
	function kiem_tra_dang_nhap()
	{
		$tendn = $this->input->post('tendn');
		$matkhau = md5($this->input->post('matkhau'));
		$this->load->model('m_nguoi_dung');
		$nguoi_dung = $this->m_nguoi_dung->lay_nguoi_dung($tendn,$matkhau);
		if(!$nguoi_dung){
			$this->session->set_flashdata('mss','<div class="warning">Đăng nhập không thành công</div>');
			redirect('cm-admin');
		}else{
			$arr_session = array(
				'ma_nguoi_dung' => $nguoi_dung['ma_nguoi_dung'],
				'ten_nguoi_dung' => $nguoi_dung['ten_nguoi_dung'],
				'quyen' => $nguoi_dung['quyen']
			);
			$this->session->set_userdata($arr_session);
			redirect('cm-admin/he-thong-quan-tri');	
		}
	}
	function he_thong_quan_tri()
	{
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			$data['path'] = 'admin/includes/v_trang_chu';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}	
	}
	function thoat()
	{
		$this->session->sess_destroy();
		redirect('cm-admin');	
	}
}?>