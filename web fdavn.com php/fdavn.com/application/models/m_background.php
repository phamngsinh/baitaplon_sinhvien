<?php class M_background extends CI_Model{
	function edit_background($id){
		$this->db->where('id',$id);
		$query = $this->db->get('tbl_background');
		if($query->num_rows>0){
			return $query->row();
		}else{
			return false;
		}
	}
	function change_background($id,$home,$about1,$about2,$contact,$my_pham,$chai_lo,$thiet_ke,$phap_ly,$footer1){
		$arr = array(
			'home' => $home,
			'about1' => $about1,
			'about2' => $about2,
			'contact' => $contact,
			'my_pham' => $my_pham,
			'chai_lo' => $chai_lo,
			'thiet_ke' => $thiet_ke,
			'phap_ly' => $phap_ly,
			'footer' => $footer1
		);
		$this->db->where('id',$id);
		return $this->db->update('tbl_background',$arr);	
	}
	function change_background_with_img($id,$home,$home_img,$about_img,$about1,$about2,$contact,$contact_img,$my_pham,$chai_lo,$thiet_ke,$phap_ly,$footer1){
		$arr = array(
			'home' => $home,
			'home_img' => $home_img,
			'about_img' => $about_img,
			'about1' => $about1,
			'about2' => $about2,
			'contact' => $contact,
			'contact_img' => $contact_img,
			'my_pham' => $my_pham,
			'chai_lo' => $chai_lo,
			'thiet_ke' => $thiet_ke,
			'phap_ly' => $phap_ly,
			'footer' => $footer1
		);
		$this->db->where('id',$id);
		return $this->db->update('tbl_background',$arr);	
	}
	function delete_home_img($obj)
	{
		$this->db->where('id',1);
		if($obj == 'home_img')
			return $this->db->update('tbl_background',$arr = array('home_img' => ''));
		if($obj == 'about_img')
			return $this->db->update('tbl_background',$arr = array('about_img' => ''));
		if($obj == 'contact_img')
			return $this->db->update('tbl_background',$arr = array('contact_img' => ''));
	}
}?>