<?php class General_model extends CI_Model{
	function db_seo($page_name){
		$this->db->where('page_name',$page_name);
		$result=$this->db->get('seo');
		return $result->result_array();
	}
	function get_controller($alas,$lang_id){
		$sql = "select object, object_id ,lang_id from ".$this->db->dbprefix("seo_name")." where lang_id = $lang_id and alas = ".$this->db->escape($alas);
		return $this->db->query($sql)->row_array();
	}
	function ds_tinh_thanh(){
		$this->db->order_by("matinhthanh", "desc");
		$query = $this->db->get('tinhthanh');
		if($query->num_rows()>0){
			$arr=array();
			foreach ($query->result() as $row){
				$arr[]=$row;
			}
			return $arr;
		}else{
			return false;	
		}
	}
	function check_email($femail)
	{
		$this->db->select('email');
		$this->db->where('email',$femail);
		$query = $this->db->get('email_list');
		if($query->num_rows()>0){
			return $query->num_rows();
		}else{
			return false;
		}
	}
	function insert_email($femail)
	{
		$arr = array(
			'email' => $femail
		);
		return $this->db->insert('email_list',$arr);	
	}
	function get_all()
	{
		$query = $this->db->get('email_list');
		if($query->num_rows()>0){
			$arr=array();
			foreach ($query->result() as $row){
				$arr[]=$row;
			}
			return $arr;
		}else{
			return false;	
		}
	}
	function add_message($tendaydu,$email,$dienthoai,$nd)
	{
		$arr = array(
			'fullname' => $tendaydu,
			'email' => $email,
			'phone' => $dienthoai,
			'content' => $nd
		);
		return $this->db->insert('contact',$arr);
	}
}?>