<?php class M_product extends CI_Model{
	function product_list(){
		$this->db->join('category','category.cat_id = product.catp_id');
		$this->db->join('seo_name','seo_name.object_id = product.pro_id');
		$this->db->where('seo_name.object','product');			
		$this->db->order_by("pro_id", "desc");
		$query = $this->db->get("product");
		if($query->num_rows()>0){
			$arr=array();
			foreach ($query->result() as $row){
				$arr[]=$row;
			}
			return $arr;
		}else{
			return false;	
		}
	}
	function check_pro_name($pro_name,$catp_id){
		$this->db->select('pro_name');
		$this->db->where('pro_name', $pro_name);
		$this->db->where('catp_id', $catp_id);
		$query = $this->db->get('product');
		if($query->num_rows()>0){
			return $query->num_rows();
		}else{
			return false;
		}
	}
	function add_product($pro_name,$hinh_luu,$pro_brief,$pro_details,$pro_price,$catp_id){
		$arr = array(
			'pro_name' => $pro_name,
			'pro_picture' => $hinh_luu,
			'pro_brief' => $pro_brief,
			'pro_details' => $pro_details,
			'pro_price' => $pro_price,
			'catp_id' => $catp_id
		);
		return $this->db->insert('product',$arr);	
	}

	function edit_product($pro_id){
		$this->db->where('pro_id',$pro_id);
		$bv = $this->db->get('product');
		if($bv->num_rows>0){
			return $bv->row();
		}else{
			return false;
		}
	}
	function check_pro_name_upt($pro_id,$pro_name,$catp_id){
		$this->db->select('pro_name');
		$this->db->where('pro_name', $pro_name);
		$this->db->where('catp_id', $catp_id);
		$this->db->where('pro_id !=', $pro_id);
		$query = $this->db->get('product');
		if($query->num_rows()>0){
			return $query->num_rows();
		}else{
			return false;
		}
	}
	function change_product($pro_id,$pro_name,$pro_brief,$pro_details,$pro_price,$catp_id){
		$arr = array(
			'pro_name' => $pro_name,
			'pro_brief' => $pro_brief,
			'pro_details' => $pro_details,
			'pro_price' => $pro_price,
			'catp_id' => $catp_id
		);
		$this->db->where('pro_id',$pro_id);
		return $this->db->update('product',$arr);		
	}
	function change_product_with_image($pro_id,$hinh_luu,$pro_name,$pro_brief,$pro_details,$pro_price,$catp_id){
		$arr = array(
			'pro_name' => $pro_name,
			'pro_picture' => $hinh_luu,
			'pro_brief' => $pro_brief,
			'pro_details' => $pro_details,
			'pro_price' => $pro_price,
			'catp_id' => $catp_id
		);
		$this->db->where('pro_id',$pro_id);
		return $this->db->update('product',$arr);		
	}
	function delete_product($pro_id){
		return $this->db->delete('product', array('pro_id' => $pro_id)); 
	}
	
	//front
	function list_product_for_parent($cat_id){
		$this->db->join('category','category.cat_id = product.catp_id');
		$this->db->join('seo_name','seo_name.object_id = product.pro_id');
		$this->db->where('seo_name.object','product');
		$this->db->where('product.catp_id', $cat_id);			
		$this->db->order_by("pro_id", "desc");
		$query = $this->db->get("product");
		if($query->num_rows()>0){
			$arr=array();
			foreach ($query->result() as $row){
				$arr[]=$row;
			}
			return $arr;
		}else{
			return false;	
		}
	}
	function older_products($pro_id,$catp_id){
		$this->db->select('pro_id,seo_name.alas as alas');
		$this->db->from('product');
		$this->db->join('seo_name','seo_name.object_id = product.pro_id');
		$this->db->order_by("pro_id", "desc");
		$this->db->where('pro_id <',$pro_id);
		$this->db->where('catp_id',$catp_id);
		$this->db->limit(1,0);
		$query = $this->db->get();
		if($query->num_rows()>0){
			$arr=array();
			foreach ($query->result() as $row){
				$arr[]=$row;
			}
			return $arr;
		}else{
			return false;	
		}
	}
	function newer_products($pro_id,$catp_id){
		$this->db->select('pro_id,seo_name.alas as alas');
		$this->db->from('product');
		$this->db->join('seo_name','seo_name.object_id = product.pro_id');
		$this->db->order_by("pro_id", "asc");
		$this->db->where('pro_id >',$pro_id);
		$this->db->where('catp_id',$catp_id);
		$this->db->limit(1,0);
		$query = $this->db->get();
		if($query->num_rows()>0){
			$arr=array();
			foreach ($query->result() as $row){
				$arr[]=$row;
			}
			return $arr;
		}else{
			return false;	
		}
	}
}?>