<?php class M_tai_khoan extends CI_Model{
	function ds_tai_khoan(){
		$this->db->where('quyen !=', 1);
		$this->db->order_by("ma_nguoi_dung", "desc");
		$query = $this->db->get('nguoi_dung');
		if($query->num_rows()>0){
			$arr=array();
			foreach ($query->result() as $row){
				$arr[]=$row;
			}
			return $arr;
		}else{
			return false;	
		}
	}
	function kiem_tra_ten_dang_nhap($tendangnhap){
		$this->db->select('tendn');
		$this->db->where('tendn', $tendangnhap);
		$query = $this->db->get('nguoi_dung');
		if($query->num_rows()>0){
			return $query->num_rows();
		}else{
			return false;
		}
	}
	function kiem_tra_email($email){
		$this->db->select('email');
		$this->db->where('email', $email);
		$query = $this->db->get('nguoi_dung');
		if($query->num_rows()>0){
			return $query->num_rows();
		}else{
			return false;
		}
	}
	function kiem_tra_ten_dang_nhap_cap_nhat($manguoidung,$tendangnhap){
		$this->db->select('tendn');
		$this->db->where('tendn', $tendangnhap);
		$this->db->where('ma_nguoi_dung !=', $manguoidung);
		$query = $this->db->get('nguoi_dung');
		if($query->num_rows()>0){
			return $query->num_rows();
		}else{
			return false;
		}
	}
	function kiem_tra_email_cap_nhat($manguoidung,$email){
		$this->db->select('email');
		$this->db->where('email', $email);
		$this->db->where('ma_nguoi_dung !=', $manguoidung);
		$query = $this->db->get('nguoi_dung');
		if($query->num_rows()>0){
			return $query->num_rows();
		}else{
			return false;
		}
	}
	function them_tai_khoan($tendangnhap,$matkhau,$hoten,$email,$dienthoai,$diachi,$cmnd,$quyen,$ngaysinh){
		$arr = array(
			'tendn' => $tendangnhap,
			'mat_khau' => $matkhau,
			'ten_nguoi_dung' => $hoten,
			'ngay_sinh' => $ngaysinh,
			'dia_chi' => $diachi,
			'email' => $email,
			'cmnd' => $cmnd,
			'dien_thoai' => $dienthoai,
			'quyen' => $quyen
		);
		return $this->db->insert('nguoi_dung',$arr);	
	}
	function chi_tiet_tai_khoan($manguoidung){
		$this->db->where('ma_nguoi_dung',$manguoidung);
		$query = $this->db->get('nguoi_dung');
		if($query->num_rows>0){
			return $query->row();
		}else{
			return false;
		}
	}
	function cap_nhat_tai_khoan($manguoidung,$tendangnhap,$email,$hoten,$dienthoai,$diachi,$ngaysinh,$cmnd,$quyen){
		$arr = array(
			'tendn' => $tendangnhap,
			'ten_nguoi_dung' => $hoten,
			'ngay_sinh' => $ngaysinh,
			'dia_chi' => $diachi,
			'email' => $email,
			'cmnd' => $cmnd,
			'dien_thoai' => $dienthoai,
			'quyen' => $quyen
		);
		$this->db->where('ma_nguoi_dung',$manguoidung);
		return $this->db->update('nguoi_dung',$arr);	
	}
	function cap_nhat_tai_khoan_mk($manguoidung,$tendangnhap,$matkhau,$email,$hoten,$dienthoai,$diachi,$ngaysinh,$cmnd,$quyen){
		$arr = array(
			'tendn' => $tendangnhap,
			'mat_khau' => $matkhau,
			'ten_nguoi_dung' => $hoten,
			'ngay_sinh' => $ngaysinh,
			'dia_chi' => $diachi,
			'email' => $email,
			'cmnd' => $cmnd,
			'dien_thoai' => $dienthoai,
			'quyen' => $quyen
		);
		$this->db->where('ma_nguoi_dung',$manguoidung);
		return $this->db->update('nguoi_dung',$arr);	
	}
	function xoa_tai_khoan($manguoidung){
		return $this->db->delete('nguoi_dung', array('ma_nguoi_dung' => $manguoidung)); 
	}
}?>