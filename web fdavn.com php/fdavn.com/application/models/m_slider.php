<?php class M_slider extends CI_Model{
	function slider_list($catp_id){
		$this->db->join('category','category.cat_id = slider.catp_id');
		if($catp_id!=0)
			$this->db->where('catp_id',$catp_id);
		$this->db->order_by("sl_id", "desc");
		$slider = $this->db->get('slider');
		if($slider->num_rows()>0){
			$arr=array();
			foreach ($slider->result() as $row){
				$arr[]=$row;
			}
			return $arr;
		}else{
			return false;	
		}
	}
	function add_slider($sl_title,$hinh,$sl_link,$catp_id){
		$arr = array(
			'sl_image' => $hinh,
			'sl_title' => $sl_title,
			'sl_link' => $sl_link,
			'catp_id' => $catp_id
		);
		return $this->db->insert('slider',$arr);	
	}
	function edit_slider($sl_id){
		$this->db->where('sl_id',$sl_id);
		$query = $this->db->get('slider');
		if($query->num_rows>0){
			return $query->row();
		}else{
			return false;
		}
	}
	function change_slider($sl_id,$sl_title,$sl_link,$catp_id){
		$arr = array(
			'sl_title' => $sl_title,
			'sl_link' => $sl_link,
			'catp_id' => $catp_id
		);
		$this->db->where('sl_id',$sl_id);
		return $this->db->update('slider',$arr);		
	}
	function change_slider_with_img($sl_id,$sl_title,$hinh,$sl_link,$catp_id){
		$arr = array(
			'sl_title' => $sl_title,
			'sl_image' => $hinh,
			'sl_link' => $sl_link,
			'catp_id' => $catp_id
		);
		$this->db->where('sl_id',$sl_id);
		return $this->db->update('slider',$arr);		
	}
	function delete_slider($sl_id){
		return $this->db->delete('slider', array('sl_id' => $sl_id)); 
	}
	//front
	/*function ds_slider_limit($limit){
		$this->db->order_by("id", "desc");
		$this->db->limit($limit);
		$slider = $this->db->get('slider');			
		if($slider->num_rows()>0){
			$arr=array();
			foreach ($slider->result() as $row){
				$arr[]=$row;
			}
			return $arr;
		}else{
			return false;	
		}
	}*/
}?>