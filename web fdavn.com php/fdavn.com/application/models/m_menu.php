<?php class M_menu extends CI_Model{
	function ds_menu(){
		$this->db->order_by("thutu", "asc");
		$query = $this->db->get("menu");		
		if($query->num_rows()>0){
			$arr=array();
			foreach ($query->result() as $row){
				$arr[]=$row;
			}
			return $arr;
		}else{
			return false;	
		}
	}
	function tong_so_mau_tin(){
		return $this->db->count_all('menu');
	}
	function chi_tiet_menu($idmenu){
		$this->db->where('idmenu',$idmenu);
		$query = $this->db->get('menu');
		if($query->num_rows>0){
			return $query->row();
		}else{
			return false;
		}
	}
	function cap_nhat_menu($idmenu,$tenmenu,$link,$trangthai,$thutu){
		$arr = array(
			'tenmenu' => $tenmenu,
			'link' => $link,
			'trangthai' => $trangthai,
			'thutu' => $thutu
		);
		$this->db->where('idmenu',$idmenu);
		return $this->db->update('menu',$arr);		
	}
	function xoa_menu($idmenu){
		return $this->db->delete('menu', array('idmenu' => $idmenu)); 
	}
}?>