<?php class M_contact extends CI_Model{
	function contact_list(){
		$this->db->order_by("id", "desc");
		$query = $this->db->get('contact');
		if($query->num_rows()>0){
			$arr=array();
			foreach ($query->result() as $row){
				$arr[]=$row;
			}
			return $arr;
		}else{
			return false;	
		}
	}
	function edit_contact($id){
		$this->db->where('id',$id);
		$query = $this->db->get('contact');
		if($query->num_rows>0){
			return $query->row();
		}else{
			return false;
		}
	}
	function delete_contact($id){
		return $this->db->delete('contact', array('id' => $id)); 
	}
}?>