<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<?php echo base_url(); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?php echo base_url('public/css_front/images/favicon.ico'); ?>">
<title><?php echo @$title_page; ?></title>
<meta content="<?php echo @$keyword_page; ?>" name="keywords" />
<meta content="<?php echo @$description_page; ?>" name="description" />
<link rel="stylesheet" href="public/css_front/home.css" type="text/css" />
<link rel='stylesheet' type='text/css' href='public/css_front/menu.css' />
<link rel='stylesheet' type='text/css' href='public/css_front/style.css' />
<script type="text/javascript" src="<?php echo base_url('public/js_admin/jquery-1.7.2.min.js'); ?>"></script>
<script src="<?php echo base_url('public/js_admin/jquery.validate.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function () {
	   $(".c-form").validate({
		   rules: {
			   tendaydu:{
				   required:true
			   },
			   email:{
				   required:true,
				   email:true
			   },
			   dienthoai:{
				   required:true,
				   number:true,
				   rangelength:[8,12]
			   },
			   noidung:{
				   required:true,
				   rangelength:[10,400]
			   }
			},
		   messages:{
			   tendaydu:{required:"Vui lòng nhập họ tên"},
			   email:{required:"Vui lòng nhập địa chỉ email",email:"Vui lòng nhập đúng định dạng email"},
			   dienthoai:{required:"Vui lòng nhập điện thoại",number:"Điện thoại phải là số",rangelength:"Điện thoại phải từ 8-12 ký tự"},
			   noidung:{required:"Vui lòng nhập nội dung liên hệ",rangelength:"Vui lòng nhập từ 10-400 ký tự"}
		   }
	   });
	});
</script><script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', '<?php if($chi_tiet_cau_hinh->analytics != '') echo $chi_tiet_cau_hinh->analytics; ?>', 'giacongmyphamtrongoi.com'); ga('send', 'pageview');</script>
<style type="text/css">
	.c-form p {
		position:relative;
	}
	.c-form label.error {
		position:absolute;
		top:-30px;
		right:-60px;
		margin:0;
		background:red;
		filter:alpha(opacity=70);
		color:#fff;
		-moz-box-shadow:0 0 7px #000;
		-webkit-box-shadow:0 0 7px #000;
		box-shadow:0 0 7px #000;
		border:1px solid #fff;
		padding:5px 10px;
	}
	.c-form label.error:before {
		content:url(<?php echo base_url('public/css_front/images/triangle.png'); ?>);
		position:absolute;
		bottom:-36px;
		height:36px;
	}
</style>
</head>

<body style="background: <?php if($background->contact != '') echo '#'.$background->contact; ?>">
	<div id="wrapper">
    	<div id="header">
    		<div id="logo">
            	<h1><a href="">Gia công mỹ phẩm trọn gói</a></h1>
            </div><!-- end #logo -->
            <div id="nav">
				<?php if(isset($menu) && $menu != null){ ?>
                <ul id="main-nav">
                    <?php foreach($menu as $row){ ?>
                        <li><a <?php if($row->link != '') echo 'href="'.base_url($row->link).'"'; ?>><?php echo $row->tenmenu; ?></a>
                            <?php if(isset($category) && $category != null && $row->idmenu == 5){ ?>
                                <ul>
                                    <?php
                                        $this->load->view('front/include/Functions');
                                        foreach($category as $row){
                                            $khongdau = khongdau($row->cat_name);
                                    ?>
                                        <li><a href="<?php echo 'dich-vu/'.$khongdau.'-'.$row->cat_id; ?>"><?php echo $row->cat_name; ?></a></li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </li>
                    <?php } ?>
                </ul>
                <?php } ?>
                
            </div><!-- end #nav -->
    	</div><!-- end #header -->
        <div class="clear"></div>
        <div id="contact" <?php if($background->contact_img != ''){$url=base_url('public/css_front/images/'.$background->contact_img); echo 'style="background: url('.$url.') no-repeat 95px 1px;"';}?>>
        	
        
        	<div class="contact-form">
            	<p class="c-title">Gửi tin nhắn cho chúng tôi</p>
                <?php if(isset($success)){ ?>
                <div style="width:98.5%;background:#039; border:1px solid #999; padding:10px 0px 10px 8px; color:#fff; margin-bottom:15px">Nội dung liên hệ của bạn đã được gửi đi.</div>
                <?php } ?>
                <?php if(isset($warning)){ ?>
                <div style="width:98.5%;background:#FCC; border:1px solid #999; padding:10px 0px 10px 8px; color:red; margin-bottom:15px"><?php echo $warning; ?></div>
                <?php } ?>
                
                <form method="post" action="" class="c-form">
                	<p><input name="tendaydu" type="text" placeholder="Họ tên" /></p>
                    <p><input name="email" type="email" placeholder="Địa chỉ email" /></p>
                    <p><input name="dienthoai" type="text" placeholder="Số điện thoại" /></p>
                    <p><textarea name="noidung" placeholder="Nội dung"></textarea></p>
                    <input name="action" type="submit" value="Gửi tin" />
                </form>
            </div><!-- end #contact-form -->
            <div class="contact-direct">
            	<p class="c-title">Liên hệ trực tiếp với chúng tôi</p>
                <div class="c-phone c-info">
                	<?php
						$phone_arr = explode('#',$chi_tiet_cau_hinh->phone);
						$phone='';
						for($i=0;$i<count($phone_arr);$i++){
							$phone = $phone.$phone_arr[$i].'<br/>';
						}
						$phone = substr($phone, 0, strlen($phone)-5);
						echo $phone;
					?>
                </div>
                <div class="c-email c-info">
                	<?php echo $chi_tiet_cau_hinh->receive_email; ?>
                </div>
                <div class="c-facebook c-info">
                	www.facebook.com/<br/>
                	<?php
						$facebook_arr = explode('/',$chi_tiet_cau_hinh->facebook_acc);
						$i= count($facebook_arr)-1;
						echo $facebook_arr[$i];
					?>

                </div>
            </div><!-- end #contact-direct -->
        </div><!-- end #contact -->
    </div><!-- end #wrapper -->
    <div class="clear"></div>
    <div id="footer" style="background: <?php if($background->footer != '') echo '#'.$background->footer; ?>">
    	<div id="main-foot">
            <div id="logo-foot">
                <img src="public/css_front/images/logo-foot-white.png" />
            </div><!-- end #logo-foot -->
            <div class="footer-content">
            	<?php echo $chi_tiet_cau_hinh->footer; ?>
            </div><!-- end #footer-content -->
            <div class="social">
            	<a href="<?php echo $chi_tiet_cau_hinh->facebook_acc; ?>"><img src="public/css_front/images/facebook.png" /></a>
                <a href="<?php echo $chi_tiet_cau_hinh->google_acc; ?>"><img src="public/css_front/images/google.png" /></a>
                <a href="<?php echo $chi_tiet_cau_hinh->twitter_acc; ?>"><img src="public/css_front/images/twitter.png" /></a>
               </div><!-- end #social -->

     <div class="author">Designed by FDA Corp</div>
           
            <div class="clear"></div>
        </div>
        
    </div><!-- end #footer -->
    <script type="text/javascript">
$(function(){
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) $('#goTop').fadeIn();
		else $('#goTop').fadeOut();
	});
	$('#goTop').click(function () {
		$('body,html').animate({scrollTop: 0}, 'slow');
	});
});
</script>
 <div id="goTop"><img src="public/css_front/images/gotop.png" alt="về đầu trang" title='về đầu trang' /></div>
</body>
</html>
