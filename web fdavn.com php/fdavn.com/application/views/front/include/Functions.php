<?php
	function Tach($chuoi)
	{
		$arr=explode ("|", $chuoi);
		return $arr;
	}
	function TachURL($chuoi)
	{
		$arr=explode ("/", $chuoi);
		return $arr;
	}
    function gioihantu($chuoi)
    {
        $tam=str_split($chuoi,100);
        return $tam[0].'...';      
    }
	function khongdau($str){
		$str = trim($str);
		$str = mb_strtolower($str);
		
		$mang_kytu = array('~','- ',"'",'!','@','#','$','%','^','&','*','(',')','_','+','*','/','.',',',';','"',':','?','“','”');
		foreach($mang_kytu as $gt)
			$str = str_replace($gt,'',$str);

		$str = str_replace(' ','-',$str);
		
		$mang_a = array('ạ','á','à','ả','ã','ă','ắ','ằ','ẳ','ẵ','ặ','â','ậ','ấ','ầ','ẩ','ẫ');
		foreach($mang_a as $gt)
			$str = str_replace($gt,'a',$str);
			
		$str = str_replace('đ','d',$str);
		
		$mang_e = array('ẹ','é','è','ẻ','ẽ','ê','ế','ề','ể','ễ','ệ');
		foreach($mang_e as $gt)
			$str = str_replace($gt,'e',$str);
		
		$mang_i = array('ị','í','ì','ỉ','ĩ');
		foreach($mang_i as $gt)
			$str = str_replace($gt,'i',$str);
			
		$mang_y = array('ỵ','ý','ỳ','ỷ','ỹ');
		foreach($mang_y as $gt)
			$str = str_replace($gt,'y',$str);
		
		$mang_o = array('ó','ò','ỏ','õ','ọ','ố','ô','ồ','ổ','ỗ','ộ','ơ','ớ','ờ','ở','ỡ','ợ');
		foreach($mang_o as $gt)
			$str = str_replace($gt,'o',$str);
		
		$mang_u = array('ú','ù','ủ','ũ','ụ','ư','ứ','ừ','ử','ữ','ự');
		foreach($mang_u as $gt)
			$str = str_replace($gt,'u',$str);
		return $str;
	}
	function GetIPAd() 
	{    
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet    
		{      
			 $ip=$_SERVER['HTTP_CLIENT_IP'];    
		}     
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy   
		{    
			 $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];  
		}   
		else   
		{      
			 $ip=$_SERVER['REMOTE_ADDR'];    
		}
		return $ip; 
	}
	function getToday()
	{
		$date = '';
		if(date('w')==0) echo $date."Chủ nhật";
		if(date('w')==1) echo $date."Thứ hai";
		if(date('w')==2) echo $date."Thứ ba";
		if(date('w')==3) echo $date."Thứ tư";
		if(date('w')==4) echo $date."Thứ năm";
		if(date('w')==5) echo $date."Thứ sáu";
		if(date('w')==6) echo $date."Thứ bảy";
		$date .= date(', d/m/Y');
		return $date;
	}
?>
