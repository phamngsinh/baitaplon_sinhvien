<div id="vnt-container">
    <div class="mid-banner-bg">
        <div class="mid-banner-wrapper">
            <div class="mid-banner">
                <div id="slider">
                	<?php if(isset($slider) && $slider != null){ ?>
                    <ul>
                    	<?php foreach($slider as $sl){ ?>
                        <li><img src="<?php echo base_url('public/slide_images/'.$sl->hinh); ?>" alt="<?php echo $sl->tieude; ?>" /></li>;
                        <?php } ?>
                    <ul>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
	<div class="clear"></div>
<div id="vnt-content">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
    	<tr>
          <td id="vnt-sidebar"><div class="box_datetime">&nbsp;</div>
        <div class="box">
              <div class="box-title">
            <h1> DANH MỤC</h1>
          </div>
              <div class="box-content">
            <div class="box_category">
            	<?php if(isset($product_cat) && $product_cat != null){ ?>
                  <ul >
					  <?php 
                        foreach($product_cat as $row){
                            $nkhongdau = khongdau($row->catp_name).'-'.$row->catp_id.'.html';
                            echo '<li><a href="'.base_url('san-pham/'.$nkhongdau).'"><span>'.$row->catp_name.'</span></a></li>';
                        ?>                
                  	<?php } ?>
                  </ul>
              <?php } ?>
                </div>
          </div>
            </div></td>
          <td  id="vnt-main"><div class="box_mid">
              <div class="boxM-tl fixPNG">
              <div class="boxM-tr fixPNG">
                  <div class="boxM-t"></div>
                </div>
            </div>
              <div class="boxM">
              <div class="mid-title">
                  <div class="titleL">
                  <h1>Thông tin mới</h1>
                </div>
                  <div class="titleR">
                  <div class="more"></div>
                </div>
                  <div class="clear"></div>
                </div>
              <div class="mid-content">
                  <div class="boxInfo">
                  <div class="boxNews">
                      <div class="img"><a href="/vn/news.html" title="Tin tức mới"><img src="<?php echo base_url('public/css_front/images'); ?>/tin-tuc-moi.gif" width="195" height="70" /></a></div>
                      <h2><a href="/vn/news.html" title="Tin tức mới">Tin tức mới</a></h2>
                    <?php if(isset($news) && $news != null){ ?>  
                    <ul>
                    	<?php foreach($news as $row){ ?>
                      <li><a href="<?php echo base_url($row->alas); ?>" title="<?php echo $row->tieu_de; ?>"><?php echo $row->tieu_de; ?></a></li>
                      <?php } ?>
                     
                    </ul>
                    <?php } ?>
                    </div>
                  <div class="boxNews">
                      <div class="img"><a href="/vn/shareholder.html" title="Tin cổ đông"><img src="<?php echo base_url('public/css_front/images'); ?>/tin-co-dong.gif" width="195" height="70" /></a></div>
                      <h2><a href="/vn/shareholder.html" title="Tin cổ đông">Tin cổ đông</a></h2>
                      <?php if(isset($shareholder) && $shareholder != null){ ?>  
                      <ul>
                      <?php foreach($shareholder as $row){ ?>
                      <li><a href="<?php echo base_url($row->alas); ?>" title="<?php echo $row->tieu_de; ?>"><?php echo $row->tieu_de; ?></a></li>
                      <?php } ?>
                    </ul>
                    <?php } ?>
                    </div>
                  <div class="boxNews last">
                      <div class="img"><a href="/vn/about.html" title="Giới thiệu công ty "><img src="<?php echo base_url('public/css_front/images'); ?>/tin-cong-ty.gif" width="195" height="70" /></a></div>
                      <h2><a href="/vn/about.html" title="Giới thiệu công ty ">Giới thiệu công ty </a></h2>
                      <?php
						$abb = mysql_query("select about_box from cau_hinh");
						$abb1 = mysql_fetch_assoc($abb);
						if($abb1!=null){
					?>
					<ul>
						<?php
							$ab_arr = explode(',',$abb1['about_box']);
							for($i=0;$i<count($ab_arr);$i++){
								$ab_id = $ab_arr[$i];
								$ab = mysql_query("select ab_title, seo_name.alas as alas from about inner join seo_name ON about.ab_id = seo_name.object_id where seo_name.object = 'about' and ab_id = '$ab_id'");
								$ab1 = mysql_fetch_assoc($ab);
								
							
						?>
						  <li><a href="<?php echo base_url($ab1['alas']); ?>"  title='<?php echo $ab1['ab_title'] ?>'><?php echo $ab1['ab_title'] ?></a></li>
						  <?php } ?>
						</ul>
						<?php } ?>
                    
                    </div>
                  <div class="clear"></div>
                </div>
                </div>
              <br />
              <div class="mid-title">
                  <div class="titleL">
                  <h1>Sản phẩm mới</h1>
                </div>
                  <div class="titleR">
                  <div class="more"></div>
                </div>
                  <div class="clear"></div>
                </div>
              <div class="mid-content">
                  <div class="FocusProduct">
                  <div class="marquee" id="mycrawler">
                      <table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                      <?php if(isset($new_products) && $new_products != null){ ?>
                      	<?php foreach($new_products as $row){ ?>
                        	<?php
								$manghinh = explode('|',$row->pro_picture);
								$url = base_url('public/product_images/'.$manghinh[0]);
							?>
                          <td class="item" style="overflow:hidden" width="170px">
                          	<div class='boxImg' style="width:170px">
                              <table width='170px' border=0 cellspacing=0 cellpadding=0 align='center'>
                              <tr>
                                  <td align='center' class='img'><a href='<?php echo base_url($row->alas); ?>' title='<?php echo $row->pro_name; ?>'><img width="150px" height="130px" src="<?php echo $url; ?>" alt="<?php echo $row->pro_name; ?>"/></a></td>
                                <tr>
                            </table>
                           
                            </div>
                             <div style="width:170px"><h3><a href='<?php echo base_url($row->alas); ?>' title='<?php echo $row->pro_name; ?>'><?php echo $row->pro_name; ?></a></h3></div>
                          </td>
                          <?php } ?>
                      <?php } ?>    
                        </tr>
                    </table>
                    </div>
                  <script type="text/javascript">
						marqueeInit({
							uniqueid: 'mycrawler',
							style: {
								'padding': '0px',
								'width': '640px',
								'height': '200px'
							},
							inc: 10,  
							mouse: 'cursor driven', 
							moveatleast: 2,
							neutral: 150,
							savedirection: true
						});
						</script> </div>
                  <div class="clear"></div>
                </div>
            </div>
              <div class="boxM-bl fixPNG">
              <div class="boxM-br fixPNG">
                  <div class="boxM-b"></div>
                </div>
            </div>
            </div></td>
        </tr>
  </table>
      <div class="clear"></div>
    </div>
</div>