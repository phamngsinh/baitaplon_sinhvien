<div class="project-slide">

  <div class="project-info">
    <h2><?php echo $product_details->pro_name; ?></h2>

    <div class="project-info-text">


      <?php echo $product_details->pro_details; ?>

    </div>

   <!----- <?php if($product_details->pro_price != ''){ ?><h3>Giá thành: <?php echo number_format($product_details->pro_price); ?> VND</h3><?php } ?>--->

    <!-- .manufactured --> 

  </div>

  <!-- .project-info -->

  

  <div class="project-cycle-wrap">

    <div class="csc-textpic csc-textpic-center csc-textpic-above">

      <div class="project-cycle-wrap">

        <ul class="project-cycle">

          <?php

		  	$img_arr = explode('|',$product_details->pro_picture);

			for($i=2;$i<count($img_arr);$i++){

				if($img_arr[$i] != '')

					echo '<li><img src="'.base_url('public/product_images/'.$img_arr[$i]).'" width="641" height="385" alt="" /></li>';	

			}

		  ?>

        </ul>

      </div>

    </div>

    <div class="csc-textpic-clear"><!-- --></div>

  </div>

  <!-- .project-cycle --> 

  

</div>

<!-- .project-slide --> 



<a href="#" class="project-slider-close">Close</a>

<?php

	if(isset($product_details_older)){	

		echo '<a href="'.base_url($product_details_older).'" class="project-slider-prev">Previous Project</a>';

	}

?>

<?php

	if(isset($product_details_newer)){

		echo '<a href="'.base_url($product_details_newer).'" class="project-slider-next">Next Project</a>';

	}

?>