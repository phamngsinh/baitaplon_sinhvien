<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<base href="<?php echo base_url(); ?>" />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><?php echo @$title_page; ?></title>

<link rel="shortcut icon" href="<?php echo base_url('public/css_front/images/favicon.ico'); ?>" />

<meta content="<?php echo @$keyword_page; ?>" name="keywords" />

<meta content="<?php echo @$description_page; ?>" name="description" />

<meta name="viewport" content="width=device-width" />

<link rel="stylesheet" href="public/css_front/home.css" type="text/css" />

<script src="public/js_front/jquery-1.6.2.min.js" type="text/javascript"></script>

<script type="text/javascript" src="public/js_front/jquery.cycle.all.js"></script>

<script type="text/javascript">

	$(document).ready(function(){

		$('#slide1').cycle({ fx: 'fade', timeout: Math.floor(Math.random()*8000 + 100),speed: 800});

		$('#slide2').cycle({ fx: 'fade', timeout: Math.floor(Math.random()*8000 + 100),speed: 800});

		$('#slide3').cycle({ fx: 'fade', timeout: Math.floor(Math.random()*8000 + 100),speed: 800});

		$('#slide4').cycle({ fx: 'fade', timeout: Math.floor(Math.random()*8000 + 100),speed: 800});

	});

</script>
<script>

 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', '<?php if($chi_tiet_cau_hinh->analytics != '') echo $chi_tiet_cau_hinh->analytics; ?>', 'giacongmyphamtrongoi.com');

 ga('send', 'pageview');

</script>
<link rel='stylesheet' type='text/css' href='public/css_front/menu.css' />
<link rel='stylesheet' type='text/css' href='public/css_front/style.css' />
<script type="text/javascript">
$(function(){
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) $('#goTop').fadeIn();
		else $('#goTop').fadeOut();
	});
	$('#goTop').click(function () {
		$('body,html').animate({scrollTop: 0}, 'slow');
	});
});
</script>
</head>

<body style="background: <?php if($background->home_img != '') echo 'url(public/css_front/images/'.$background->home_img.') top center no-repeat'; ?> <?php if($background->home != '') echo '#'.$background->home; ?>">

	<div id="colorFooter"></div>



	<div id="wrapper">

    	<div id="header">

    		<div id="logo">

            	<h1><a href="">Gia công mỹ phẩm trọn gói</a></h1>

            </div><!-- end #logo -->

            <div id="nav">

            	<?php if(isset($menu) && $menu != null){ ?>

            

            	<ul id="main-nav">

                	<?php foreach($menu as $row){ ?>

                		<li><a <?php if($row->link != '') echo 'href="'.base_url($row->link).'"'; ?>><?php echo $row->tenmenu; ?></a>

                        	<?php if(isset($category) && $category != null && $row->idmenu == 5){ ?>

                        		<ul>

                                	<?php

										$this->load->view('front/include/Functions');

                                    	foreach($category as $row){

											$khongdau = khongdau($row->cat_name);

									?>

                                		<li><a href="<?php echo 'dich-vu/'.$khongdau.'-'.$row->cat_id.'.html'; ?>"><?php echo $row->cat_name; ?></a></li>

                                    <?php } ?>

                                </ul>

                        	<?php } ?>

                        </li>

                	<?php } ?>

                </ul>

                <?php } ?>

                

            </div><!-- end #nav -->

    	</div><!-- end #header -->

        <div class="clear"></div>

        <div id="container">

        	<?php if(isset($category) && $category != null){ ?>

    		<ul	class="services-list">

            	<?php

					$count=0; 

					foreach($category as $row){

						$count++;

						$pic_arr = explode('|',$row->cat_pic);

						$khongdau = khongdau($row->cat_name);

				?>

            	<li>

                	<div class="top-ser">

                    	<a href="<?php echo 'dich-vu/'.$khongdau.'-'.$row->cat_id.'.html'; ?>">

                            <div class="sl" id="slide<?php echo $count; ?>">	

                            	<?php

									for($i=0;$i<count($pic_arr);$i++)

									{

										echo '<img width="200px" height="200px" src="public/category_images/'.$pic_arr[$i].'" alt="'.$row->cat_name.'"/>';	

									}

								?>

                           </div>

                       </a>

                       <h2><a href="<?php echo 'dich-vu/'.$khongdau.'-'.$row->cat_id.'.html'; ?>"><?php echo $row->cat_name; ?></a></h2>

                    </div>

                    

                    <div class="main-ser">

                    	<?php echo $row->cat_brief; ?>

                    </div>

				</li>

                <?php } ?>        

            </ul>

            <?php } ?>

    	</div><!-- end #container -->

    </div><!-- end #wrapper -->

    <div class="clear"></div>

    <div id="footer" style="background: <?php if($background->footer != '') echo '#'.$background->footer; ?>">

    	<div id="main-foot">

            <div id="logo-foot">

                <img src="public/css_front/images/logo-foot-white.png" />

            </div><!-- end #logo-foot -->

            <div class="footer-content">

            	<?php echo $chi_tiet_cau_hinh->footer; ?>

            </div><!-- end #footer-content -->

            <div class="social">

            	<a href="<?php echo $chi_tiet_cau_hinh->facebook_acc; ?>"><img src="public/css_front/images/facebook.png" /></a>

                <a href="<?php echo $chi_tiet_cau_hinh->google_acc; ?>"><img src="public/css_front/images/google.png" /></a>

                <a href="<?php echo $chi_tiet_cau_hinh->twitter_acc; ?>"><img src="public/css_front/images/twitter.png" /></a>

            </div><!-- end #social -->

     <div class="author">Designed by FDA Corp</div>

            

        </div>

    </div><!-- end #footer -->

    <div class="clear"></div>




 <div id="goTop"><img src="public/css_front/images/gotop.png" alt="về đầu trang" title='về đầu trang' /></div>
</body>

</html>

