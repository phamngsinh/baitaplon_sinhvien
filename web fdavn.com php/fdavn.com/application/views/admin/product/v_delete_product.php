<div class="full_w">
    <div class="h_title h_product">Xóa sản phẩm: <?php echo $product->pro_name; ?></div>
    <?php echo form_open(''); ?>
        <div class="element">
        	<input type="hidden" name="hidden_hinh" value="<?php echo $product->pro_picture; ?>"/>
            <label for="delete">Bạn thật sự muốn xóa sản phẩm này?</label>
            <?php echo form_radio('delete', 'no', TRUE); ?> Không
            <?php echo form_radio('delete', 'yes', FALSE); ?> Có 
        </div>
        <div class="entry" style="margin-top:10px">
            <button type="submit" class="btnUpdate">Xóa</button> 
            <button type="button" class="cancel" onClick="window.history.back();">Hủy bỏ</button>
        </div>
        <input type="hidden" name="action" value="Delete product"/>
    <?php echo form_close(); ?>
    </form>
</div>