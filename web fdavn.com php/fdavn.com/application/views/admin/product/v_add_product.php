<script type="text/javascript" src="<?php echo base_url('ckeditor/ckeditor.js'); ?>" language="javascript"></script>
<script src="<?php echo base_url('public/js_admin/jquery.validate.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function () {
	   $("#insertform").validate({
		   rules: {
			   pro_name:{
				   required:true,
				   rangelength:[0,200]
			   },
			   pro_brief:{rangelength:[0,140]},
			   pro_price:{
				   rangelength:[0,50]
			   },
			   pro_details:{required:true},
			   catp_id:{min:1}
			},
			messages:{
				pro_name:{rangelength:"Chỉ nhập tối đa 200 ký tự"},
				pro_brief:{rangelength:"Chỉ nhập tối đa 140 ký tự"},
				pro_price:{rangelength:"Chỉ nhập tối đa 50 ký tự"},
			    catp_id:{min:"Vui lòng chọn danh mục."}
			}
	   });
	});
</script>
<div class="full_w">
    <div class="h_title h_product">Thêm sản phẩm</div>
    <?php 
		if(isset($pro_name_error)){echo $pro_name_error;}
		echo $this->session->flashdata('mss');
		$attributes = array('id' => 'insertform');
		echo form_open_multipart('',$attributes);
	?>
    	<div class="element">
            <label><span class="red"><strong>Lưu ý:</strong> Sử dụng &lt;br/&gt; để xuống dòng</span></label>
        </div>
        <div class="element">
            <label for="pro_name">Tên sản phẩm <span class="red">(*)</span></label>
            <?php
				$data=array('name'=>'pro_name','id'=>'pro_name','class'=>'text','value'=>set_value('pro_name',''));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="pro_brief">Mô tả ngắn <span class="red">(* Tối đa 140 ký tự)</span></label>
            <?php
				$data=array('name'=>'pro_brief','id'=>'pro_brief','rows'=>3,'value'=>set_value('pro_brief',''));
				echo form_textarea($data);
			?>
        </div>
        
        <div class="element">
            <label for="pro_details">Chi tiết sản phẩm <span class="red">(*)</span></label>
            <?php
				$data=array('name'=>'pro_details','id'=>'pro_details','value'=>set_value('pro_details',''));
				echo form_textarea($data);
			?>  
			<script type="text/javascript">CKEDITOR.replace('pro_details'); </script>
        </div>
        <div class="element">
            <label for="pro_price">Giá bán</label>
            <?php
				$data=array('name'=>'pro_price','id'=>'pro_price','class'=>'text','value'=>set_value('pro_price',''));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="catp_id">Thuộc dịch vụ <span class="red">(*)</span></label>
            <select name="catp_id">
                <option value="0">-- Chọn dịch vụ -- </option>
                <?php foreach($category as $cat){ ?>
                    <option value="<?php echo $cat->cat_id; ?>"><?php echo $cat->cat_name; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="element">
            <label for="pro_picture">Hình ảnh sản phẩm <span class="red">(*)</span></label>
            <script language="javascript" type="text/javascript">
                function tao_files_upload()
                {
                    so_hinh=document.getElementById("so_hinh").value;
                    if(so_hinh>0)
                    {
                        chuoi="";
                        for(i=0;i<so_hinh;i++)
                        {
                            chuoi+= "<p><input type='file' name='file" + i + "' size='41' /></p>";
                        }
                        document.getElementById("tao_file").innerHTML=chuoi;
                    }
                    else
                        document.getElementById("tao_file").innerHTML="<p><input type='file' name='file0' size='41' /></p>";
                }
            </script>
            <div style="border:1px solid #ccc; padding: 0px 10px 20px 60px">
            	<p style="line-height:1.6; color:#FF5252">
                	Lưu ý: Nên upload ít nhất 3 ảnh.<br/>
                    -- 2 hình ảnh đầu tiên sẽ được sử dụng để làm Ảnh đại diện (Kích thước tốt nhất <strong>204px - 190px</strong>).<br/>
                    -- Các hình ảnh còn lại sẽ được sử dụng để làm Slide ảnh (Kích thước tốt nhất <strong>641px - 385px</strong>).
                </p>
                <p style="margin-top:20px">Số hình cần upload:
                <input type="text" name="so_hinh" id="so_hinh" value="1" style="width: 130px; height:22px; margin-right:5px;">
                <input type="button" onClick="tao_files_upload()" value="Tạo"></p>
                <div id="tao_file" name="tao_file">
                    <input type="file" name="file0" size="41" />
                </div>
            </div>
        </div>
        <div class="entry" style="margin-top:10px">
        	<input type="hidden" name="action" value="Add product"/>
            <button type="submit" class="add">Thêm</button> 
            <button type="button" class="cancel" onClick="window.history.back();">Hủy bỏ</button>
        </div>
    <?php echo form_close(); ?>
    </form>
</div>