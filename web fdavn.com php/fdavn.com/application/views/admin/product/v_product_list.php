<script type="text/javascript">
    function loadbaiviet(str){
        var xmlhttp;
        if (window.XMLHttpRequest){
            xmlhttp=new XMLHttpRequest();
        }else{
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function(){
            if (xmlhttp.readyState==4 && xmlhttp.status==200){
                //document.getElementById("post-box").ht.innerHTML = xmlhttp.responseText;
				
				$('#post-box').html(xmlhttp.responseText);
            }
        }
        xmlhttp.open("GET","<?php echo base_url('cm-admin/load-product-by-catid'); ?>/"+str,true);
        xmlhttp.send();
    }
</script>
<div class="full_w">
    <div class="h_title h_product">Danh sách sản phẩm</div>  
    <div id="post-box">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css_admin/jquery.dataTables.css'); ?>"/>
	<script type="text/javascript" src="<?php echo base_url('public/js_admin/jquery.dataTables.js'); ?>"></script>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
          $('table').dataTable({
			  "sPaginationType": "full_numbers",
			  "bSort": false,
			"aaSortingFixed": [[0,'asc']]
			
		});
        } );
    </script>
    <?php if(count($product)>0 && $product != null){ ?>
	<SCRIPT language="javascript">
		$(function(){
			$("#selectall").click(function () {
				  $('.case').attr('checked', this.checked);
			});
			$(".case").click(function(){
				if($(".case").length == $(".case:checked").length) {
					$("#selectall").attr("checked", "checked");
				} else {
					$("#selectall").removeAttr("checked");
				}
		 
			});
		});
    </SCRIPT>
    <form action="<?php echo base_url('cm-admin/delete-all-product') ?>" method="post">
    	<div class="entry">
            <a class="button add" href="<?php echo base_url('cm-admin/add-product'); ?>">Thêm sản phẩm</a>
            <button type="submit" class="btnUpdate">Xóa sản phẩm chọn</button>
            <select name="loaicha" style="float: right; margin: 3px 0px 0 0; padding: 5px 10px; font-size: 16px; width: 185px;" onchange="loadbaiviet(this.value)">
                <option value="0">Tất cả sản phẩm</option>
                <?php 
                    foreach($category as $row){
                        echo '<option value="'.$row->cat_id.'">'.$row->cat_name.'</option>';	
                    }
                ?>
            </select>
            <div class="sep"></div>
        </div>
        <table id="table">
            <thead>
                <tr>
                	<th><input type="checkbox" id="selectall"/></th>
                    <th scope="col">Hình ảnh</th>
                    <th scope="col" style="width:190px">Tên sản phẩm</th>
                    <th scope="col">Mô tả ngắn</th>
                    <th scope="col" style="width:150px">Thuộc dịch vụ</th>
                    <th scope="col" style="width: 70px;"></th>
                </tr>
            </thead>
            <tbody>
                <?php $count = 0; ?>
                <?php foreach($product as $p){
                     $count += 1;
                ?>          
                <tr>
                	<td class="align-center"><input type="checkbox" class="case" name="case[]" value="<?php echo $p->pro_id; ?>"></td>
                    <td class="align-center">
                        <?php
                            $manghinh = explode('|',$p->pro_picture);
                            $url = base_url('public/product_images/'.$manghinh[0]);
                        ?>
                        <img src="<?php echo $url; ?>" width="30px"/>
                    </td>
                    <td><?php echo $p->pro_name; ?></td>
                    <td><?php echo $p->pro_brief; ?></td>
                    <td style="padding-left:20px;"><?php echo $p->cat_name; ?></td>
                    <td style="text-align:center">
                        <a href="<?php echo base_url('cm-admin/edit-product/'.$p->pro_id); ?>" class="table-icon edit" title="Chỉnh sửa" style="margin-right:5px"></a>
                        <a href="<?php echo base_url('cm-admin/delete-product/'.$p->pro_id); ?>" class="table-icon delete" title="Xóa"></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php }else{
            echo '<div class="no-entry">Chưa có sản phẩm nào</div>';
        } ?>
    <div style="clear:both"></div>
    <div class="entry">
        <div class="sep"></div>		
        <a class="button add" href="<?php echo base_url('cm-admin/add-product'); ?>">Thêm sản phẩm</a>

        <button onclick="return confirm('Bạn có chắc chắn xóa tất cả các sản phẩm đang được chọn?');" type="submit" class="btnUpdate">Xóa sản phẩm chọn</button>
    </div>
    </form>
    </div>
</div>
