<script src="<?php echo base_url('public/js_admin/jquery.validate.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function () {
	   $("#insertform").validate({
		   rules:{
			   home:{maxlength:10},
			   home_img:{accept:"gif|jpg|png|jpeg"},
			   about1:{maxlength:10},
			   about2:{maxlength:10},
			   contact:{maxlength:10},
			   my_pham:{maxlength:10},
			   chai_lo:{maxlength:10},
			   thiet_ke:{maxlength:10},
			   phap_ly:{maxlength:10},
			   footer1:{maxlength:10}
		   },
		   messages:{
			   home:{maxlength:"Vui lòng nhập ít hơn 10 ký tự."},
			   home_img:{accept:"Vui lòng upload các hình ảnh có định dang gif, jpg, png, jpeg."},
			   about1:{maxlength:"Vui lòng nhập ít hơn 10 ký tự."},
			   about2:{maxlength:"Vui lòng nhập ít hơn 10 ký tự."},
			   contact:{maxlength:"Vui lòng nhập ít hơn 10 ký tự."},
			   my_pham:{maxlength:"Vui lòng nhập ít hơn 10 ký tự."},
			   chai_lo:{maxlength:"Vui lòng nhập ít hơn 10 ký tự."},
			   thiet_ke:{maxlength:"Vui lòng nhập ít hơn 10 ký tự."},
			   phap_ly:{maxlength:"Vui lòng nhập ít hơn 10 ký tự."},
			   footer1:{maxlength:"Vui lòng nhập ít hơn 10 ký tự."},
			}
	   });
	});
</script>
<script src="<?php echo base_url('public/js_admin/colpick.js'); ?>" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo base_url('public/css_admin/colpick.css'); ?>" type="text/css"/>
<script type="text/javascript">
	$(document).ready(function(){
		$('#home').colpick({
			onChange:function(hsb,hex,rgb,fromSetColor){if(!fromSetColor) $('#home').val(hex).css('border-color','#'+hex);}
		});
		$('#home').keyup(function(){$(this).colpickSetColor(this.value);});
		$('#about1').colpick({
			onChange:function(hsb,hex,rgb,fromSetColor){if(!fromSetColor) $('#about1').val(hex).css('border-color','#'+hex);}
		});
		$('#about1').keyup(function(){$(this).colpickSetColor(this.value);});
		$('#about2').colpick({
			onChange:function(hsb,hex,rgb,fromSetColor){if(!fromSetColor) $('#about2').val(hex).css('border-color','#'+hex);}
		});
		$('#about2').keyup(function(){$(this).colpickSetColor(this.value);});
		$('#contact').colpick({
			onChange:function(hsb,hex,rgb,fromSetColor){if(!fromSetColor) $('#contact').val(hex).css('border-color','#'+hex);}
		});
		$('#contact').keyup(function(){$(this).colpickSetColor(this.value);});
		$('#my_pham').colpick({
			onChange:function(hsb,hex,rgb,fromSetColor){if(!fromSetColor) $('#my_pham').val(hex).css('border-color','#'+hex);}
		});
		$('#my_pham').keyup(function(){$(this).colpickSetColor(this.value);});
		$('#chai_lo').colpick({
			onChange:function(hsb,hex,rgb,fromSetColor){if(!fromSetColor) $('#chai_lo').val(hex).css('border-color','#'+hex);}
		});
		$('#chai_lo').keyup(function(){$(this).colpickSetColor(this.value);});
		$('#thiet_ke').colpick({
			onChange:function(hsb,hex,rgb,fromSetColor){if(!fromSetColor) $('#thiet_ke').val(hex).css('border-color','#'+hex);}
		});
		$('#thiet_ke').keyup(function(){$(this).colpickSetColor(this.value);});
		$('#phap_ly').colpick({
			onChange:function(hsb,hex,rgb,fromSetColor){if(!fromSetColor) $('#phap_ly').val(hex).css('border-color','#'+hex);}
		});
		$('#phap_ly').keyup(function(){$(this).colpickSetColor(this.value);});
		$('#footer1').colpick({
			onChange:function(hsb,hex,rgb,fromSetColor){if(!fromSetColor) $('#footer1').val(hex).css('border-color','#'+hex);}
		});
		$('#footer1').keyup(function(){$(this).colpickSetColor(this.value);});
	});
</script>
<style>a:hover{text-decoration:none}</style>
<script>
	function delete1(img,col)
	{
		//alert(img+'-'+col);
		var xmlhttp;
		if (window.XMLHttpRequest){
			xmlhttp=new XMLHttpRequest();
		}
		else{
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function(){
			if (xmlhttp.readyState==4 && xmlhttp.status==200){
				location.reload();
			}
		}
		xmlhttp.open("GET","<?php echo base_url('cm-admin/delete-bg-img'); ?>/"+img+"/"+col,true);
		xmlhttp.send();
	}
</script>
<div class="full_w">
    <div class="h_title h_color">Thiết lập màu sắc giao diện</div>
    <?php 
		if(isset($hinh_error)){echo $hinh_error;}
		echo $this->session->flashdata('mss');
		$attributes = array('id' => 'insertform');
		echo form_open_multipart('',$attributes);
	?>
    	<input type="hidden" name="hidden_hinh_home" value="<?php echo $edit_background->home_img; ?>" /> 
        <div class="element">
            <label for="home">Màu nền Trang chủ</label>
            #<input name="home" type="text" id="home" style="border-right:20px solid #<?php echo $edit_background->home; ?>;" value="<?php echo $edit_background->home; ?>" />
        </div>
        <div class="element">
            <label for="home_img">Hình nền "Trang chủ"</label>
            <?php if($edit_background->home_img != ''){ ?>
            <img style="float:left; margin-right:15px" src="<?php echo base_url('public/css_front/images/'.$edit_background->home_img); ?>" height="80px"/>
            <a onclick="delete1('<?php echo $edit_background->home_img; ?>','home_img')" style="line-height:50px; padding:2px 6px; border:1px solid #ccc; background:#f2f2f2; cursor:pointer">Xóa ảnh nền</a><br/>
            
            <?php } ?>
            <?php
				$data=array('name'=>'home_img','id'=>'home_img','value'=>set_value('home_img',''));
				echo form_upload($data);
			?>  
        </div>
        
        <input type="hidden" name="hidden_hinh_about" value="<?php echo $edit_background->about_img; ?>" />
        <div class="element">
            <label for="about_img">Hình nền "Trang giới thiệu" (Chiều ngang cho phép 470px, nếu ảnh vượt quá kích thước này sẽ bị mất)</label>
            <?php if($edit_background->about_img != ''){ ?>
            <img style="float:left; margin-right:15px" src="<?php echo base_url('public/css_front/images/'.$edit_background->about_img); ?>" height="80px"/>
            <a onclick="delete1('<?php echo $edit_background->about_img; ?>','about_img')" style="line-height:50px; padding:2px 6px; border:1px solid #ccc; background:#f2f2f2; cursor:pointer">Xóa ảnh nền</a><br/>
            
            <?php } ?>
            <?php
				$data=array('name'=>'about_img','id'=>'about_img','value'=>set_value('about_img',''));
				echo form_upload($data);
			?>  
        </div>
        <div class="element">
            <label for="about1">Màu nền Khung giới thiệu 2</label>
            #<input name="about1" type="text" id="about1" style="border-right:20px solid #<?php echo $edit_background->about1; ?>;" value="<?php echo $edit_background->about1; ?>" />
        </div>  
        <div class="element">
            <label for="about2">Màu nền Khung giới thiệu 3</label>
            #<input name="about2" type="text" id="about2" style="border-right:20px solid #<?php echo $edit_background->about2; ?>;" value="<?php echo $edit_background->about2; ?>" />
        </div>
        <div class="element">
            <label for="contact">Màu nền trang "Liên hệ"</label>
            #<input name="contact" type="text" id="contact" style="border-right:20px solid #<?php echo $edit_background->contact; ?>;" value="<?php echo $edit_background->contact; ?>" />
        </div>
        <input type="hidden" name="hidden_hinh_contact" value="<?php echo $edit_background->contact_img; ?>" />
        <div class="element">
            <label for="contact_img">Hình nền "Trang liên hệ"</label>
            <?php if($edit_background->contact_img != ''){ ?>
            <img style="float:left; margin-right:15px" src="<?php echo base_url('public/css_front/images/'.$edit_background->contact_img); ?>" height="80px"/>
            <a onclick="delete1('<?php echo $edit_background->contact_img; ?>','contact_img')" style="line-height:50px; padding:2px 6px; border:1px solid #ccc; background:#f2f2f2; cursor:pointer">Xóa ảnh nền</a><br/>
            
            <?php } ?>
            <?php
				$data=array('name'=>'contact_img','id'=>'contact_img','value'=>set_value('contact_img',''));
				echo form_upload($data);
			?>  
        </div>
        <div class="element">
            <label for="my_pham">Màu nền "Cung cấp mỹ phẩm"</label>
            #<input name="my_pham" type="text" id="my_pham" style="border-right:20px solid #<?php echo $edit_background->my_pham; ?>;" value="<?php echo $edit_background->my_pham; ?>" />
        </div>  
        <div class="element">
            <label for="chai_lo">Màu nền "Cung cấp chai lọ"</label>
            #<input name="chai_lo" type="text" id="chai_lo" style="border-right:20px solid #<?php echo $edit_background->chai_lo; ?>;" value="<?php echo $edit_background->chai_lo; ?>" />
        </div>
        <div class="element">
            <label for="thiet_ke">Màu nền "Thiết kế"</label>
            #<input name="thiet_ke" type="text" id="thiet_ke" style="border-right:20px solid #<?php echo $edit_background->thiet_ke; ?>;" value="<?php echo $edit_background->thiet_ke; ?>" />
        </div>
        <div class="element">
            <label for="phap_ly">Màu nền "Pháp lý"</label>
            #<input name="phap_ly" type="text" id="phap_ly" style="border-right:20px solid #<?php echo $edit_background->phap_ly; ?>;" value="<?php echo $edit_background->phap_ly; ?>" />
        </div>  
        <div class="element">
            <label for="footer1">Màu nền "Footer"</label>
            #<input name="footer1" type="text" id="footer1" style="border-right:20px solid #<?php echo $edit_background->footer; ?>;" value="<?php echo $edit_background->footer; ?>" />
        </div>
        
        
        <div class="entry" style="margin-top:10px">
        	<input type="hidden" name="action" value="Thiết lập"/>
            <button type="submit" class="update">Thiết lập</button> 
            <button type="button" class="cancel" onClick="window.history.back();">Hủy bỏ</button>
        </div>
    <?php echo form_close(); ?>
    </form>
</div>