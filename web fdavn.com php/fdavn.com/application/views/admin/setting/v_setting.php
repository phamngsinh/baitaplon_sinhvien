<script type="text/javascript" src="<?php echo base_url('ckeditor/ckeditor.js'); ?>" language="javascript"></script>
<script src="<?php echo base_url('public/js_admin/jquery.validate.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function () {
	   $("#insertform").validate({
		   rules:{
			   title_page:{maxlength:255},
			   description_page:{maxlength:255},
			   keyword_page:{maxlength:255},
			   analytics:{maxlength:255},
		   		smtpuser:"required",
				smtppass:"required",
				receive_email:"required"
		   },
		   messages:{
			   title_page:{maxlength:"Vui lòng nhập ít hơn 255 ký tự."},
			   description_page:{maxlength:"Vui lòng nhập ít hơn 255 ký tự."},
			   keyword_page:{maxlength:"Vui lòng nhập ít hơn 255 ký tự."},
			   analytics:{maxlength:"Vui lòng nhập ít hơn 255 ký tự."}
			}
	   });
	});
</script>
<style type="text/css">
	.checkbox{
		margin:5px 10px;	
	}
</style>
<div class="full_w">
    <div class="h_title h_setting">Thiết lập cấu hình hệ thống</div>
    <?php 
		echo $this->session->flashdata('mss');
		$attributes = array('id' => 'insertform');
		echo form_open('',$attributes);
	?>
        <div class="element">
            <label for="title_page">Tiêu đề trang</label>
            <?php
				$data=array('name'=>'title_page','id'=>'title_page','class'=>'text','value'=>set_value('title_page',$edit_setting->title_page));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="description_page">Mô tả trang</label>
            <?php
				$data=array('name'=>'description_page','id'=>'description_page','class'=>'text','value'=>set_value('description_page',$edit_setting->description_page));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="keyword_page">Từ khóa trang</label>
            <?php
				$data=array('name'=>'keyword_page','id'=>'keyword_page','class'=>'text','value'=>set_value('keyword_page',$edit_setting->keyword_page));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="analytics">Google Analytics </label>
            <?php
                $data=array('name'=>'analytics','id'=>'analytics','rows'=>'3','value'=>set_value('analytics',$edit_setting->analytics));
                echo form_textarea($data);
            ?>
        </div>
        <div class="element">
            <label for="facebook_acc">Tài khoản Facebook</label>
            <?php
				$data=array('name'=>'facebook_acc','id'=>'facebook_acc','class'=>'text','value'=>set_value('facebook_acc',$edit_setting->facebook_acc));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="twitter_acc">Tài khoản Twitter</label>
            <?php
				$data=array('name'=>'twitter_acc','id'=>'twitter_acc','class'=>'text','value'=>set_value('twitter_acc',$edit_setting->twitter_acc));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="google_acc">Tài khoản Google +</label>
            <?php
				$data=array('name'=>'google_acc','id'=>'google_acc','class'=>'text','value'=>set_value('google_acc',$edit_setting->google_acc));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="phone">Số điện thoại (Các số điện thoại cách nhau bởi dấu #. VD: 0932145202#0932125205)</label>
            <?php
				$data=array('name'=>'phone','id'=>'phone','class'=>'text','value'=>set_value('phone',$edit_setting->phone));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="smtpuser">Email đại diện gửi (SMTP User) - Chỉ sử dụng GMAIL <span class="red">(*)</span></label>
            <?php
				$data=array('name'=>'smtpuser','id'=>'smtpuser','class'=>'text','value'=>set_value('smtpuser',$edit_setting->smtp_user));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="smtppass">Mật khẩu (SMTP Pass) <span class="red">(*)</span></label>
            <?php
				$data=array('name'=>'smtppass','id'=>'smtppass','class'=>'text','value'=>set_value('smtppass',$edit_setting->smtp_pass));
				echo form_password($data);
			?>
        </div>
        <div class="element">
            <label for="receive_email">Email nhận liên hệ <span class="red">(*)</span></label>
            <?php
				$data=array('name'=>'receive_email','id'=>'receive_email','class'=>'text','value'=>set_value('receive_email',$edit_setting->receive_email));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="about">Nội dung giới thiệu 1</label>
            <?php
				$data=array('name'=>'about','id'=>'about','value'=>set_value('about',$edit_setting->about));
				echo form_textarea($data);
			?>  
			<script type="text/javascript">CKEDITOR.replace('about'); </script>  
        </div>
        <div class="element">
            <label for="about1">Nội dung giới thiệu 2</label>
            <?php
				$data=array('name'=>'about1','id'=>'about1','value'=>set_value('about1',$edit_setting->about1));
				echo form_textarea($data);
			?>  
			<script type="text/javascript">CKEDITOR.replace('about1'); </script>  
        </div>
        <div class="element">
            <label for="about2">Nội dung giới thiệu 3</label>
            <?php
				$data=array('name'=>'about2','id'=>'about2','value'=>set_value('about2',$edit_setting->about2));
				echo form_textarea($data);
			?>  
			<script type="text/javascript">CKEDITOR.replace('about2'); </script>  
        </div>
        <div class="element">
            <label for="footer">Nội dung footer</label>
            <?php
				$data=array('name'=>'footer','id'=>'footer','value'=>set_value('footer',$edit_setting->footer));
				echo form_textarea($data);
			?>  
			<script type="text/javascript">CKEDITOR.replace('footer'); </script>  
        </div>
        <div class="entry" style="margin-top:10px">
        	<input type="hidden" name="action" value="Thiết lập"/>
            <button type="submit" class="add">Thiết lập</button> 
            <button type="button" class="cancel" onClick="window.history.back();">Hủy bỏ</button>
        </div>
    <?php echo form_close(); ?>
    </form>
</div>