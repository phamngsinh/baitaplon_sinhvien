<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css_admin/jquery.dataTables.css'); ?>"/>
<script type="text/javascript" src="<?php echo base_url('public/js_admin/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#table').dataTable({"sPaginationType": "full_numbers"});
	} );
</script>
<div class="full_w">
        <div class="h_title h_slider">Danh sách slider</div>
        <?php if(count($slider)>0 && $slider!= null){ ?>
        <table id="table">
            <thead>
                <tr>
                    <th scope="col">STT</th>
                    <th scope="col">Hình ảnh</th>
                    <th scope="col">Tiêu đề</th>
                    <th scope="col">Liên kết</th>
                    <th scope="col">Thuộc dịch vụ</th>
                    <th scope="col" style="width: 70px;"></th>
                </tr>
            </thead>
            <tbody>
            	<?php $count = 0; ?>
            	<?php foreach($slider as $sl){ ?> 
                <?php $count += 1; ?>          
                <tr>
                    <td class="align-center"><?php echo $count; ?></td>
                    <td align="center"><img src="<?php echo base_url("public/slide_images/$sl->sl_image"); ?>" width="200px"></td>
                    <td style="padding-left:20px;"><?php echo $sl->sl_title; ?></td>
                    <td style="padding-left:20px;"><?php echo $sl->sl_link; ?></td>
                    <td style="padding-left:20px;"><?php echo $sl->cat_name; ?></td>
                    <td style="text-align:center">
                        <a href="<?php echo base_url('cm-admin/edit-slider/'.$sl->sl_id); ?>" class="table-icon edit" title="Chỉnh sửa" style="margin-right:5px"></a>
                        <a href="<?php echo base_url('cm-admin/delete-slider/'.$sl->sl_id); ?>" class="table-icon delete" title="Xóa"></a>
                    </td>
                </tr>
                <?php }	?>
            </tbody>
        </table>
        <div style="clear:both"></div>
        <div class="entry">
            <div class="sep"></div>		
            <a class="button add" href="<?php echo base_url('cm-admin/add-slider'); ?>">Thêm slider</a>
        </div>
        <?php }else{
				echo '<div class="no-entry">Chưa có slider nào</div>';
			} ?>
    </div>