<script src="<?php echo base_url('public/js_admin/jquery.validate.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function () {
	   $("#insertform").validate({
		   rules: {hinh:{accept:"gif|jpg|png|jpeg"},sl_link:{url:true},catp_id:{min:1}},
		   messages:{
			   hinh:{accept:"Vui lòng upload các hình ảnh có định dang gif, jpg, png, jpeg."},
			   sl_link:{url:"Vui lòng nhập liên kết hợp lệ"},
			   catp_id:{min:"Vui lòng chọn dịch vụ."}
		   }
	   });
	});
</script>
<div class="full_w">
    <div class="h_title h_slider">Cập nhật slider</div>
    <?php 
		if(isset($hinh_error)){echo $hinh_error;}
		echo $this->session->flashdata('mss');
		$attributes = array('id' => 'insertform');
		echo form_open_multipart('',$attributes);
	?>
    	<div class="element">
            <label><span class="red">-- Kích thước ảnh tốt nhất là <strong>1196px - 342px</span></label>
        </div>
        <div class="element">
            <input type="hidden" name="hidden_hinh" value="<?php echo $chi_tiet_slider->sl_image; ?>" />
            <label for="hinhanh">Hình ảnh <span class="red">(*)</span></label>
            <img src="<?php echo base_url('public/slide_images/'.$chi_tiet_slider->sl_image); ?>" width="400px"/><br/><br/>
            <?php
				$data=array('name'=>'hinh','id'=>'hinh','value'=>set_value('hinh',''));
				echo form_upload($data);
			?>
        </div>
        <div class="element">
            <label for="sl_title">Tiêu đề</label>
            <?php
				$data=array('name'=>'sl_title','id'=>'sl_title','class'=>'text','value'=>set_value('tieude',$chi_tiet_slider->sl_title));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="sl_link">Liên kết</label>
            <?php
				$data=array('name'=>'sl_link','id'=>'sl_link','class'=>'text','value'=>set_value('sl_link',$chi_tiet_slider->sl_link));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="catp_id">Thuộc dịch vụ <span class="red">(*)</span></label>
            <select name="catp_id">
                <option value="0">-- Chọn dịch vụ -- </option>
                <?php foreach($category as $cat){ ?>
                    <option <?php if($chi_tiet_slider->catp_id == $cat->cat_id) echo 'selected="selected"'; ?> value="<?php echo $cat->cat_id; ?>"><?php echo $cat->cat_name; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="entry" style="margin-top:10px">
        	<input type="hidden" name="action" value="Edit slider"/>
            <button type="submit" class="update">Cập nhật</button> 
            <button type="button" class="cancel" onClick="window.history.back();">Hủy bỏ</button>
        </div>
    <?php echo form_close(); ?>
    </form>
</div>