<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css_admin/jquery.dataTables.css'); ?>"/>
<script type="text/javascript" src="<?php echo base_url('public/js_admin/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#table').dataTable(
			{"sPaginationType": "full_numbers"}
		);
	} );
</script>
<div class="full_w">
    <div class="h_title h_service">Danh sách dịch vụ</div>
    <?php if(count($category)>0 && $category != null){ ?>
    <table id="table">
        <thead>
            <tr>
                <th scope="col" style="width: 50px;">STT</th>
                <th scope="col">Tên dịch vụ</th>
                <th scope="col">Trạng thái</th>
                <th scope="col">Vị trí</th>
                <th scope="col" style="width: 50px;"></th>
            </tr>
        </thead>
        <tbody>
            <?php $count = 0; ?>
			<?php foreach($category as $cat){ ?> 
            <?php $count += 1; ?>          
            <tr>
                <td class="align-center"><?php echo $count; ?></td>
                <td><?php echo $cat->cat_name; ?></td>
                <td class="align-center">
					<?php 
                        if($cat->status == 1) echo 'Hiện';
                        else echo 'Ẩn';
                    ?>
                </td>
                <td align="center"><?php echo $cat->position; ?></td>
                <td style="text-align:center">
                    <a href="<?php echo base_url('cm-admin/edit-category/'.$cat->cat_id); ?>" class="table-icon edit" title="Chỉnh sửa" style="margin-left:10px"></a>
                    <!-- <a href="<?php echo base_url('cm-admin/delete-category/'.$cat->cat_id); ?>" class="table-icon delete" title="Xóa"></a> -->
                </td>
            </tr>
            <?php }	?>
        </tbody>
    </table>
    <?php }else{
            echo '<div class="no-entry">Chưa có dịch vụ nào</div>';
        } ?>
    <!-- <div style="clear:both"></div>
    <div class="entry">
        <div class="sep"></div>		
        <a class="button add" href="<?php echo base_url('cm-admin/add-category'); ?>">Thêm dịch vụ</a>
    </div> -->
</div>