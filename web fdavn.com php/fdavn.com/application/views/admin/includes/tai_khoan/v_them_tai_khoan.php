<script src="<?php echo base_url('public/js_admin/jquery.validate.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function () {
	   $("#insertform").validate({
		   rules: {
			   tendangnhap:{required:true,rangelength:[4,20]},
			   matkhau:{required:true,rangelength:[6,20]},
			   xacnhanmatkhau:{required:true,equalTo:"#txtPass"},
			   hoten:"required",
			   email:{email:true},
			   cmnd:{number:true,rangelength:[12,12]},
			   dienthoai:{number:true,rangelength:[8,12]}
		   },
		   messages:{
			  tendangnhap:{rangelength:"Vui lòng nhập 4-20 ký tự"},
			  matkhau:{rangelength:"Vui lòng nhập 6-20 ký tự"},
			  xacnhanmatkhau:{equalTo:"Xác nhận mật khẩu không đúng"},
			  email:{email:"Phải nhập email hợp lệ"},
			  cmnd:{number:"Chứng minh nhân dân phải là số",rangelength:"CMND phải có 12 số"},
			  dienthoai:{number:"Điện thoại phải là số",rangelength:"Điện thoại phải từ 8-12 số"},
		   }
	   });
	});
</script>
<div class="full_w">
    <div class="h_title h_account">Thêm tài khoản</div>
    <?php 
		if(isset($errors) && $errors != null){
			foreach($errors as $er){
				echo $er;
			}	
		}
		echo $this->session->flashdata('mss');
		$attributes = array('id' => 'insertform');
		echo form_open('',$attributes);
	?>
        <div class="element">
            <label for="tendangnhap">Tên đăng nhập <span class="red">(*)</span></label>
            <?php
				$data=array('name'=>'tendangnhap','id'=>'tendangnhap','class'=>'text','value'=>set_value('tendangnhap',''));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="matkhau">Mật khẩu <span class="red">(*)</span></label>
            <?php
				$data=array('name'=>'matkhau','id'=>'txtPass','class'=>'text','value'=>set_value('matkhau',''));
				echo form_password($data);
			?>
        </div>
        <div class="element">
            <label for="xacnhanmatkhau">Xác nhận mật khẩu: <span class="red">(*)</span></label>
            <?php
				$data=array('name'=>'xacnhanmatkhau','id'=>'xacnhanmatkhau','class'=>'text','value'=>set_value('xacnhanmatkhau',''));
				echo form_password($data);
			?>
        </div>
        <div class="element">
            <label for="hoten">Họ tên: <span class="red">(*)</span></label>
            <?php
				$data=array('name'=>'hoten','id'=>'hoten','class'=>'text','value'=>set_value('hoten',''));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="ngaysinh">Ngày sinh:</label>
            <?php
				$data=array('name'=>'ngaysinh','id'=>'datepicker','class'=>'text','value'=>set_value('ngaysinh',''));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="diachi">Địa chỉ:</label>
            <?php
				$data=array('name'=>'diachi','id'=>'diachi','class'=>'text','value'=>set_value('diachi',''));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="email">Email:</label>
            <?php
				$data=array('name'=>'email','id'=>'email','class'=>'text','value'=>set_value('email',''));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="cmnd">CMND:</label>
            <?php
				$data=array('name'=>'cmnd','id'=>'cmnd','class'=>'text','value'=>set_value('cmnd',''));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="dienthoai">Điện thoại:</label>
            <?php
				$data=array('name'=>'dienthoai','id'=>'dienthoai','class'=>'text','value'=>set_value('dienthoai',''));
				echo form_input($data);
			?>
        </div>
        <div class="entry" style="margin-top:10px">
        	<input type="hidden" name="action" value="Thêm tài khoản"/>
            <button type="submit" class="add">Thêm</button> 
            <button type="button" class="cancel" onClick="window.history.back();">Hủy bỏ</button>
        </div>
    <?php echo form_close(); ?>
    </form>
</div>
<link type="text/css" href="<?php echo base_url('public/css_admin/jquery.datepick.css'); ?>" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url('public/js_admin/jquery.datepick.js'); ?>"></script>
<script type="text/javascript">
$(function() {
	$('#datepicker').datepick({dateFormat: 'dd/mm/yyyy'});
});
</script>