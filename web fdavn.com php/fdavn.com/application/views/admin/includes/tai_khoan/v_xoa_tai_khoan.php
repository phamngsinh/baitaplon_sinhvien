<div class="full_w">
	<?php if($tai_khoan != null){ ?>
        <div class="h_title h_account">Xóa tài khoản: <?php echo $tai_khoan->tendn; ?></div>
        <?php echo form_open(''); ?>
            <div class="element">
                <label for="delete">Bạn thật sự muốn xóa tài khoản này?</label>
                <?php echo form_radio('delete', 'no', TRUE); ?> Không
                <?php echo form_radio('delete', 'yes', FALSE); ?> Có 
            </div>
            <div class="entry" style="margin-top:10px">
                <input type="hidden" name="action" value="Xóa tài khoản"/>
                <button type="submit" class="btnUpdate">Xóa</button> 
                <button type="button" class="cancel" onClick="window.history.back();">Hủy bỏ</button>
            </div>
        <?php echo form_close(); ?>
        </form>
	<?php }else{ ?>
    	<div class="h_title">Xóa tài khoản</div>
    	<div class="exist"><label>Tài khoản này không tồn tại</label></div>
    <?php } ?>
</div>