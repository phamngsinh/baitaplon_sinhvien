<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css_admin/jquery.dataTables.css'); ?>"/>
<script type="text/javascript" src="<?php echo base_url('public/js_admin/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#table').dataTable({"sPaginationType": "full_numbers"});
	} );
</script>
<div class="full_w">
        <div class="h_title h_account">Danh sách tài khoản</div>
        <?php if(count($tai_khoan)>0 && $tai_khoan != null){ ?>
        <table id="table">
            <thead>
                <tr>
                    <th scope="col" style="width: 100px;">STT</th>
                    <th scope="col">Tài khoản</th>
                    <th scope="col">Email</th>
                    <th scope="col">Địa chỉ</th>
                    <th scope="col">Điện thoại</th>
                    <th scope="col" style="width: 70px;"></th>
                </tr>
            </thead> 
            <tbody>
            	<?php $count = 0; ?>
            	<?php foreach($tai_khoan as $tk){ ?> 
                <?php $count += 1; ?>          
                <tr>
                    <td class="align-center"><?php echo $count; ?></td>
                    <td style="padding-left:10px;"><?php echo $tk->tendn; ?></td>
                    <td style="padding-left:10px;"><?php echo $tk->email; ?></td>
                    <td style="padding-left:10px;"><?php echo $tk->dia_chi; ?></td>
                    <td style="padding-left:10px;"><?php echo $tk->dien_thoai; ?></td>
                    <td style="text-align:center">
                        <a href="<?php echo base_url('cm-admin/chi-tiet-tai-khoan/'.$tk->ma_nguoi_dung); ?>" class="table-icon edit" title="Chỉnh sửa" style="margin-right:5px"></a>
                        <a href="<?php echo base_url('cm-admin/xoa-tai-khoan/'.$tk->ma_nguoi_dung); ?>" class="table-icon delete" title="Xóa"></a>
                    </td>
                </tr>
                <?php }	?>
            </tbody>
        </table>
        <div style="clear:both"></div>
        <div class="entry">
            <div class="sep"></div>		
            <a class="button add" href="<?php echo base_url('cm-admin/them-tai-khoan'); ?>">Thêm tài khoản</a>
        </div>
        <?php }else{
				echo '<div class="no-entry">Chưa có tài khoản nào</div>';
			} ?>
    </div>