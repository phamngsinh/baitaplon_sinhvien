<?php
	$time_now = time();
	$time_out = 60;
	$ip_address = $_SERVER['REMOTE_ADDR'];
	$online = mysql_num_rows(mysql_query("SELECT `ip_address` FROM `online` WHERE UNIX_TIMESTAMP(`last_visit`) + $time_out > $time_now"));
	$day = mysql_num_rows(mysql_query("SELECT `ip_address` FROM `online` WHERE DAYOFYEAR(`last_visit`) = " . (date('z') + 1) . " AND YEAR(`last_visit`) = " . date('Y')));
	$week = mysql_num_rows(mysql_query("SELECT `ip_address` FROM `online` WHERE WEEKOFYEAR(`last_visit`) = " . date('W') . " AND YEAR(`last_visit`) = " . date('Y')));
	$month = mysql_num_rows(mysql_query("SELECT `ip_address` FROM `online` WHERE MONTH(`last_visit`) = " . date('n') . " AND YEAR(`last_visit`) = " . date('Y')));
	$year = mysql_num_rows(mysql_query("SELECT `ip_address` FROM `online` WHERE YEAR(`last_visit`) = " . date('Y')));
	$visit = mysql_num_rows(mysql_query("SELECT `ip_address` FROM `online`"));
?>
<div class="full_w" style="border:none">
	<div class="statics" style="background: url(<?php echo base_url(); ?>public/css_admin/img/statistics.png) no-repeat 10px 21px #fa3031">
         <div class="statics-number">
         	<strong style="font-size:30px"><?php echo $day; ?></strong><br />
            Hôm nay
         </div>
    </div>
    
    <div class="statics" style="background: url(<?php echo base_url(); ?>public/css_admin/img/statistics.png) no-repeat 10px 21px #43c83c; width:13%">
         <div class="statics-number">
         	<strong style="font-size:30px"><?php echo $month; ?></strong><br />
            Tháng này
         </div>
    </div>
    
    <div class="statics" style="background: url(<?php echo base_url(); ?>public/css_admin/img/statistics.png) no-repeat 10px 21px #52b9e9;width: 15%">
         <div class="statics-number">
         	<strong style="font-size:30px"><?php echo $year; ?></strong><br />
            Năm nay
         </div>
    </div>
    
    <div class="statics" style="background: url(<?php echo base_url(); ?>public/css_admin/img/statistics.png) no-repeat 10px 21px #932ab6; width:18%">
         <div class="statics-number">
         	<strong style="font-size:30px"><?php echo $visit; ?></strong><br />
            Tổng lượt truy cập
         </div>
    </div>
  
</div>
<div class="full_w">
    <div class="h_title h_bdk">Bảng điều khiển</div>
    <style type="text/css">
		#cpanel{
			margin:25px 0px 0px 25px; width:1076px; overflow:hidden;
		}
		#cpanel .icon{
			text-align: center;
			margin-right: 25px;
			float: left;
			margin-bottom: 25px;	
		}
		#cpanel .icon a{	
			background-color: #d74f2a;
			background-position: -30px;
			display: block;
			float: left;
			height: 110px;
			width: 240px;
			color: #fff;
			vertical-align: middle;
			text-decoration: none;
			-webkit-transition-property: background-position, -webkit-border-bottom-left-radius, -webkit-box-shadow;
			-moz-transition-property: background-position, -moz-border-radius-bottomleft, -moz-box-shadow;
			-webkit-transition-duration: 0.8s;
			-moz-transition-duration: 0.8s;	
		}
		#cpanel .icon a img{
			padding: 10px 0;
			margin: 5px auto;
			box-shadow:#999;
		}
		#cpanel .icon a span{
			display: block;
			text-align: center;
			color: #fff;
			font-size:14px;
		}
		table{margin:20px 10px 10px 10px}
		table td{padding:0px; padding-bottom:7px}
	</style>
    
	<div id="cpanel">
		<div class="icon">
			<a style="background:#1ba1e2" href="<?php echo base_url('cm-admin/them-tai-khoan'); ?>">
				<img src="<?php echo base_url('public/css_admin/img/icon-48-user-add.png'); ?>" alt="">	
				<span>Thêm tài khoản</span>
			</a>
		</div>
		<div class="icon">
			<a style="background:#a05000" href="<?php echo base_url('cm-admin/danh-sach-menu'); ?>">
				<img src="<?php echo base_url('public/css_admin/img/icon-48-menumgr.png'); ?>" alt="">	
				<span>Menu</span>
			</a>
		</div>
        <div class="icon">
			<a style="background:#a2c139" href="<?php echo base_url('cm-admin/add-product'); ?>">
				<img src="<?php echo base_url('public/css_admin/img/icon-48-product-add.png'); ?>" alt="">	
				<span>Thêm sản phẩm</span>
			</a>
		</div>
        <div class="icon">
			<a style="background:#f3d240" href="<?php echo base_url('cm-admin/category-list'); ?>">
				<img src="<?php echo base_url('public/css_admin/img/icon-48-chat.png'); ?>" alt="">	
				<span>Dịch vụ</span>
			</a>
		</div>
        <div class="icon">
			<a style="background:#d80073" href="<?php echo base_url('cm-admin/add-slider'); ?>">
				<img src="<?php echo base_url('public/css_admin/img/icon-48-album-add.png'); ?>" alt="">	
				<span>Thêm slider</span>
			</a>
		</div>
        <div class="icon">
			<a style="background:#f09609" href="<?php echo base_url('cm-admin/background/1'); ?>">
				<img src="<?php echo base_url('public/css_admin/img/icon-48-media-add.png'); ?>" alt="">	
				<span>Thiết lập màu nền</span>
			</a>
		</div>
        <div class="icon">
			<a style="background:#00aba9" href="<?php echo base_url('cm-admin/contact-list'); ?>">
				<img src="<?php echo base_url('public/css_admin/img/icon-48-contact.png'); ?>" alt="">	
				<span>Tin nhắn liên hệ</span>
			</a>
		</div>
		<div class="icon">
			<a style="background:#a51400" href="<?php echo base_url('cm-admin/setting/1'); ?>">
				<img src="<?php echo base_url('public/css_admin/img/icon-48-setting.png'); ?>" alt="">	
				<span>Cấu hình hệ thống</span>
			</a>
		</div>
	</div>
</div>
    
    
    