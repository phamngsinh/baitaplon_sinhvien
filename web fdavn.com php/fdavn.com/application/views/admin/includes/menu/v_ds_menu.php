<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css_admin/jquery.dataTables.css'); ?>"/>
<script type="text/javascript" src="<?php echo base_url('public/js_admin/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#table').dataTable({"sPaginationType": "full_numbers"});
	} );
</script>
<div class="full_w">
        <div class="h_title h_menu">Danh sách menu</div>
        <?php if(count($menu)>0 && $menu != null){ ?>
        <table id="table">
            <thead>
                <tr>
                    <th scope="col" style="width: 50px;">STT</th>
                    <th scope="col">Tên menu</th>
                    <th scope="col">Liên kết</th>
                    <th scope="col">Trạng thái</th>
                    <th scope="col">Vị trí</th>
                    <th scope="col" style="width: 50px;"></th>
                </tr>
            </thead>
            <tbody>
            	<?php $count = 0; ?>
            	<?php foreach($menu as $m){ ?> 
                <?php $count += 1; ?>          
                <tr>
                    <td class="align-center"><?php echo $count; ?></td>
                    <td><?php echo $m->tenmenu; ?></td>
                    <td><?php echo $m->link; ?></td>
                    <td class="align-center">
						<?php 
							if($m->trangthai == 1) echo 'Hiện';
							else echo 'Ẩn';
						?>
                    </td>
                    <td align="center"><?php echo $m->thutu; ?></td>
                    <td style="text-align:center">
                        <a href="<?php echo base_url('cm-admin/chi-tiet-menu/'.$m->idmenu); ?>" class="table-icon edit" title="Chỉnh sửa" style="margin-left:10px"></a>
                    </td>
                </tr>
                <?php }	?>
            </tbody>
        </table>
        <div style="clear:both"></div>
        <?php }else{
				echo '<div class="no-entry">Chưa có menu nào</div>';
			} ?>
    </div>