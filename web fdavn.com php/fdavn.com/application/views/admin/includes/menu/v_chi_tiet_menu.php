<script type="text/javascript" src="<?php echo base_url('ckeditor/ckeditor.js'); ?>" language="javascript"></script>
<script src="<?php echo base_url('public/js_admin/jquery.validate.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function () {
	   $("#insertform").validate({
		   rules: {tenmenu:"required",link:"required"}
	   });
	});
</script>
<div class="full_w">
    <div class="h_title h_menu">Cập nhật menu: <?php echo $menu->tenmenu; ?></div>
    <?php 
		echo $this->session->flashdata('mss');
		$attributes = array('id' => 'insertform');
		echo form_open_multipart('',$attributes);
	?>
        <div class="element">
            <label for="tenmenu">Tên menu <span class="red">(bắt buộc)</span></label>
            <?php
				$data=array('name'=>'tenmenu','id'=>'tenmenu','class'=>'text','readonly'=>'readonly','value'=>set_value('tenmenu',$menu->tenmenu));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="link">Liên kết <span class="red">(bắt buộc)</span></label>
            <?php
				$data=array('name'=>'link','id'=>'link','class'=>'text','readonly'=>'readonly','value'=>set_value('link',$menu->link));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="trangthai">Trạng thái</label>
            <select name="trangthai">
                <option value="1" <?php if($menu->trangthai == 1) echo 'selected="selected"'; ?> >Hiện</option>
                <option value="0" <?php if($menu->trangthai == 0) echo 'selected="selected"'; ?>>Ẩn</option>
            </select>
        </div>
        <div class="element">
            <label for="thutu">Thứ tự <span class="red">(bắt buộc)</span></label>
            <select name="thutu">
            	<option value="0">Chọn thứ tự</option>
				<?php
                    for($i=1;$i<=$thutu;$i++){
                        echo '<option value="'.$i.'"';
							if($menu->thutu == $i) echo 'selected="selected"';
						echo '>'.$i.'</option>';
                    }
                ?>
            </select>
        </div>
        <div class="entry" style="margin-top:10px">
        	<input type="hidden" name="action" value="Cập nhật menu"/>
            <button type="submit" class="update">Cập nhật</button> 
            <button type="button" class="cancel" onClick="window.history.back();">Hủy bỏ</button>
        </div>
    <?php echo form_close(); ?>
    </form>
</div>