<div class="full_w">
    <div class="h_title h_contact">Tin nhắn liên hệ</div>
    <?php 
		echo $this->session->flashdata('mss');
		$attributes = array('id' => 'insertform');
		echo form_open_multipart('',$attributes);
	?>
        <div class="element">
            <label for="fullname">Họ tên</label>
            <?php
				$data=array('name'=>'fullname','id'=>'fullname','class'=>'text','readonly'=>'readonly','value'=>set_value('fullname',$edit_contact->fullname));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="email">Email</label>
            <?php
				$data=array('name'=>'email','id'=>'email','class'=>'text','readonly'=>'readonly','value'=>set_value('email',$edit_contact->email));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="phone">Số điện thoại</label>
            <?php
				$data=array('name'=>'phone','id'=>'phone','class'=>'text','readonly'=>'readonly','value'=>set_value('phone',$edit_contact->phone));
				echo form_input($data);
			?>
        </div>
        <div class="element">
            <label for="content">Nội dung liên hệ </label>
            <?php
                $data=array('name'=>'content','id'=>'content','rows'=>'3','readonly'=>'readonly','value'=>set_value('content',$edit_contact->content));
                echo form_textarea($data);
            ?>
        </div>
    <?php echo form_close(); ?>
    </form>
</div>