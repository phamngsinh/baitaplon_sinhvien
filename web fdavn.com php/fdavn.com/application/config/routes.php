<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['cm-admin/category-list'] = "admin/category/category_list";
$route['cm-admin/add-category'] = "admin/category/add_category";
$route['cm-admin/edit-category/(:any)'] = "admin/category/edit_category/$1";
$route['cm-admin/delete-category/(:any)'] = "admin/category/delete_category/$1";
$route['cm-admin/slider-list'] = "admin/slider/slider_list";
$route['cm-admin/add-slider'] = "admin/slider/add_slider";
$route['cm-admin/edit-slider/(:any)'] = "admin/slider/edit_slider/$1";
$route['cm-admin/delete-slider/(:any)'] = "admin/slider/delete_slider/$1";
$route['cm-admin/setting/(:any)'] = "admin/setting/index/$1";
$route['cm-admin/about/(:any)'] = "admin/about/index/$1";
$route['cm-admin/background/(:any)'] = "admin/background/index/$1";
$route['cm-admin/delete-bg-img/(:any)/(:any)'] = "admin/background/delete_bg_img/$1/$2";
$route['cm-admin/contact-list'] = "admin/contact/contact_list";
$route['cm-admin/edit-contact/(:any)'] = "admin/contact/edit_contact/$1";
$route['cm-admin/delete-contact/(:any)'] = "admin/contact/delete_contact/$1";


$route['cm-admin/product-list'] = "admin/product/product_list";
$route['cm-admin/add-product'] = "admin/product/add_product";
$route['cm-admin/edit-product/(:any)'] = "admin/product/edit_product/$1";
$route['cm-admin/delete-product/(:any)'] = "admin/product/delete_product/$1";
$route['cm-admin/load-product-by-catid/(:any)'] = "admin/product/load_product_by_catid/$1";
$route['cm-admin/delete-all-product'] = "admin/product/delete_all_product";
//front
$route['dich-vu/(:any)'] = "front/product/product_list_cat";



$route['cm-admin/danh-sach-menu'] = "admin/menu/ds_menu";
$route['cm-admin/chi-tiet-menu/(:any)'] = "admin/menu/chi_tiet_menu/$1";
$route['cm-admin/danh-sach-bai-viet'] = "admin/bai_viet/ds_bai_viet";
$route['cm-admin/them-bai-viet'] = "admin/bai_viet/them_bai_viet";
$route['cm-admin/chi-tiet-bai-viet/(:any)'] = "admin/bai_viet/chi_tiet_bai_viet/$1";
$route['cm-admin/xoa-bai-viet/(:any)'] = "admin/bai_viet/xoa_bai_viet/$1";
$route['cm-admin/cat-product-list'] = "admin/cat_product/cat_product_list";
$route['cm-admin/add-cat-product'] = "admin/cat_product/add_cat_product";
$route['cm-admin/edit-cat-product/(:any)'] = "admin/cat_product/edit_cat_product/$1";
$route['cm-admin/delete-cat-product/(:any)'] = "admin/cat_product/delete_cat_product/$1";
$route['cm-admin/document'] = "admin/document";
$route['cm-admin/document/delete/(:any)'] = "admin/document/delete_one_document/$1";
//front
$route['tin-tuc/(:any)'] = "front/bai_viet/news_list_cat/$1";
$route['san-pham/(:any)'] = "front/product/product_list_cat/$1";
$route['tim-kiem'] = "front/general/tim_kiem";





$route['cm-admin/danh-sach-tai-khoan'] = "admin/tai_khoan/ds_tai_khoan";
$route['cm-admin/them-tai-khoan'] = "admin/tai_khoan/them_tai_khoan";
$route['cm-admin/chi-tiet-tai-khoan/(:any)'] = "admin/tai_khoan/chi_tiet_tai_khoan/$1";
$route['cm-admin/xoa-tai-khoan/(:any)'] = "admin/tai_khoan/xoa_tai_khoan/$1";
$route['cm-admin/kiem-tra-dang-nhap'] = "admin/dang_nhap/kiem_tra_dang_nhap";
$route['cm-admin/he-thong-quan-tri'] = "admin/dang_nhap/he_thong_quan_tri";
$route['cm-admin/thoat'] = "admin/dang_nhap/thoat";
$route['default_controller'] = "front/home";
$route['(:any)'] = "front/home/index";
$route['cm-admin'] = "admin/dang_nhap";
$route['cm-admin/forget-password'] = "admin/dang_nhap/forget_password";
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */