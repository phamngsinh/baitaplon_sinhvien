<?
 session_start();
session_register(ten);
$table_width = "780";
function reformat_date($datetime)
{
  // put date in US format, discard seconds
  list($year, $month, $day, $hour, $min, $sec) = split( '[: -]', $datetime );
  return "$hour:$min $month/$day/$year";
}

function display_tree($expanded, $row = 0, $start = 0)
{
  // display the tree view of conversations

  global $table_width;
  echo "<table width = $table_width>";

  // see if we are displaying the whole list or a sublist
  if($start>0)
    $sublist = true;
  else
    $sublist = false;

  // construct tree structure to represent conversation summary 
  $tree = new treenode($start, '', '', '', 1, true, -1, $expanded, $sublist);

  // tell tree to display itself
  $tree->display($row, $sublist);
  
  echo "</table>";
}

function do_html_header($title = '')
{
  // print an HTML header including cute logo :)

  global $table_width;
?>
  <html>
  <head>
    <title><?=$title?></title>
    <style>
      h1 { font-family: 'Times New Roman', Times,  serif; font-size: 32; 
           font-weight: normal; color:  white; margin-bottom: 0}
      b { font-family: 'Times New Roman', Times,  serif; font-size: 18; 
          font-weight: normal; color: black }
      body, li, td { font-family: Arial, Helvetica, sans-serif; 
                     font-size: 15px; margin = 5px }
      a { color: #000000 }
    </style>
  </head>
  <body leftmargin=0 topmargin=0>
  <embed src=images\diendan.swf width=780 height=70>

<?
}

function do_html_footer()
{
  // print an HTML footer

  global $table_width;
?>
  <table id="AutoNumber9" style="BORDER-COLLAPSE: collapse" borderColor="#111111" cellSpacing="0" cellPadding="0" width="780" border="0" height="76">
  <tr>
    <td align="middle" width="100%" background="images/se_footer_left_bgr.gif" bgColor="#ffffff" height="12">
      <p align="left"><img src="images/se_footer_left.gif" border="0" width="101" height="12"></p>
    </td>
  </tr>
  <tr>
    <td align="middle" width="100%" bgColor="#20a799" height="65">
      <p align="center"><font face="Times New Roman" size="3"><a href="TRANG%20CHU.htm">Trang
      ch&#7911;</a> | <a href="http://localhost/trangdangky.php">Di&#7877;n
      &#273;�n</a> |<a href="truyencuoi.htm"> Vui c&#432;&#7901;i</a> |<a HREF="TRANGGAME.HTM">
      Game</a> |<a href="miniclip.html"> MiniClip&nbsp;</a>|<a href="http://localhost/admin.php">Qu&#7843;n
      tr&#7883;</a><br>
      � 2003 Tr&#432;&#7901;ng K&#7929; Thu&#7853;t C&#417; Kh� Luy&#7879;n
      Kim S�i G�n., Addr:21-23 Nguyen Bieu St, 5-Dist, Ho Chi Minh City
      WebSite: www.TKTCKLKSG.com.vn</font></p>
    </td>
  </tr>
 </table>
  </body>
  </html>
<?
}

function display_replies_line()
{
  global $table_width;
?>
  <table width = <?=$table_width?> cellpadding = 4 i
         cellspacing = 0 bgcolor = "#cccccc">
  <tr><td><b>Replies to this message<b></td></tr>
  </table>
<?
}

function display_index_toolbar()
{
  global $table_width;
?>
  <table width = <?=$table_width?> cellpadding = 4 cellspacing = 0>
  <tr>
    <td bgcolor = "#006666" width=40><embed src=images\td.swf width = 40 height = 40></td>	
    <td bgcolor = "#cccccc" align = right><a href = "new_post.php?parent=0"><b>B�i m&#7899;i</b></a>
    <a  href = "index.php?expand=all"><b>Hi&#7875;n th&#7883; t&#7845;t c&#7843;</b></a>
    <a href = "index.php?collapse=all"><b>Hi&#7875;n th&#7883; b�i g&#7903;i ch�nh</b></a></td>
  </tr>
  </table>
<?
}

function display_post($post)
{
  global $table_width;
 
  if(!$post)
    return;
?>
  <table width = <?=$table_width?> cellpadding = 4 cellspacing = 0>
  <tr>
    <td bgcolor = '#cccccc'>
      <b>Nguoi goi: <?=$post['poster'];?></b><br>
      <b>Goi vao luc: <?=$post['posted'];?></b>
    </td>
    <td bgcolor = '#cccccc' align = right>
      <a href = 'new_post.php?parent=0'><b>B�i m&#7899;i</b></a>
       <a  href = 'new_post.php?parent=<?=$post['postid'];?>'><b>Tr&#7843; l&#7901;i</b></a>
       <a  href = 'index.php?expanded=<?=$post['postid'];?>'><b>Tr&#7903; v&#7873; di&#7877;n &#273;�n</b></a>
  </td>
  </tr>
  <tr><td colspan = 2>
  <? echo nl2br($post['message']);?>
  </td></tr>
  </table>
<?
}

function display_new_post_form($parent = 0, $area = 1, $title='', $message='')
{
  global $table_width;
?>
  <table cellpadding = 0 cellspacing = 0 border = 0 width = <?=$table_width?>>
   <form action = "store_new_post.php?expand=<?=$parent;?>#<?=$parent;?>" 
        method = post>
  <tr>
    <td bgcolor = "#cccccc">
        </td>
    <td bgcolor = "#cccccc"> 
         <input type = text name = poster value ="<?=$poster?>"  size = 20 maxlength = 20></td>
    
</tr>
  <tr>
  <td bgcolor = "#cccccc">
      Chu de:
    </td>
   <td bgcolor = "#cccccc">
   <input type = text name = title value = "<?=$title?>" 
             size = 20 maxlength = 20></td>
  </tr>
  <tr>
 <td bgcolor = "#cccccc">
      Noi dung:
    </td>
  <td colspan = 2 >
  <textarea name = message rows = 10 cols = 55><?=$message?></textarea>
  </td>
  </tr>
  <tr>
  <td colspan = 2 align = center bgcolor = "#cccccc">
  <input type = image name = post src = "images/post.gif" 
             alt = "Post Message" width =50 height = 27>
  </td>
    <input type = hidden name = parent value = <?=$parent;?> >
  <input type = hidden name = area value = <?=$area;?> >
  </tr>
  </form>
  </table>
<?
}
?>