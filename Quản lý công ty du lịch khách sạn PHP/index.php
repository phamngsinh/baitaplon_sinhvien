<?php
session_start();
include("./includes/config.php");
include("./includes/functions.php");
include("./includes/va_db.php");

if ($_GET["lang"]){
	$_SESSION["lg"]=$_GET["lang"];
} else {
	if (!isset($_SESSION["lg"])) $_SESSION["lg"]="vn";
}
include ("./language/".$_SESSION["lg"].".php");

$do   = isset($_GET["do"])  ? $_GET["do"] :'main';
$act   = isset($_GET["act"])  ? $_GET["act"]  : "";
$page   = isset($_GET["page"])  ? $_GET["page"]  : '1';//for paging

$str_replace = "
";

$link=$_SERVER['QUERY_STRING'];
$pos=strpos($link,'&lang=');
if ($pos!=0&&$pos) $link=substr($link,0,$pos);
elseif ($pos==0 && $pos) $link='';

$sql="select id, title_".$_SESSION['lg']." as title from services where menu=1 order by `order`, id";
$menuservices=$db->getAll($sql);

$sql="select id, title_".$_SESSION['lg']." as title, nick, type from `nickchat` order by `order`, id";
$nickchat=$db->getAll($sql);

if (file_exists("./sources/site/".$do.".php"))
	require("./sources/site/".$do.".php");
else {
	$msg="Không có chức năng này.";
	$p='./index.php';
	page_transfer($msg,$p);
}

include("./templates/site/header.tpl");
include("./templates/site/".$tpl.".tpl");
include("./templates/site/right.tpl");
include("./templates/site/footer.tpl");

?>  