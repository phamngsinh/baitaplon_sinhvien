<?php
$lang=array();

$lang['DATE_FORMAT'] = 'd-m-Y';
$lang['language']='Ngôn ngữ';

//header
$lang['home']='Trang chủ';
$lang['introduction']='Giới thiệu';
$lang['gallery']='Sưu tập ảnh';
$lang['news']='Tin tức';
$lang['event']='Khuyến mãi';
$lang['contact']='Liên hệ';

$lang['search']='Tìm kiếm';
$lang['google_search']='Tìm kiếm trên Google';
$lang['online_support']='Hỗ trợ trực tuyến';
$lang['advertisement']='Quảng cáo';

$lang['visitors']='Lượt truy cập';
$lang['visitorsonline']='Online';
$lang['weblink']='Liên kết website';
$lang['booking_online']='Đặt phòng trực tuyến';

//booking
$lang['roomtypes']='Loại phòng';
$lang['roomrates']='Giá phòng';
$lang['booking']='Đặt phòng';
$lang['personal_info']='Thông tin khách hàng';
$lang['roomtype']='Loại phòng';
$lang['select']='Chọn';
$lang['book_info']='Thông tin đặt phòng';
$lang['arrival_date']='Ngày đến';
$lang['departure_date']='Ngày đi';
$lang['rooms']='Số lượng phòng';
$lang['adults']='Số lượng người lớn';
$lang['child']='Số lượng trẻ em';
$lang['price_info']='Khung giá';
$lang['from_rate']='Từ giá';
$lang['to_rate']='Đến giá';
$lang['other_info']='Yêu cầu khác';
$lang['booksuc']='Cảm ơn quí khách đã đăng ký đặt phòng.<br>Chúng tôi sẽ gọi lại trong thời gian sớm nhất.';
$lang['error_roomtype']='Vui lòng chọn loại phòng.';
$lang['error_arrival_date']='Vui lòng nhập ngày đến của bạn.';
$lang['error_rooms']='Vui lòng nhập số lượng phòng.';
 
//more
$lang['send']='Gửi đi';
$lang['save']='Lưu';
$lang['reset']='Nhập lại';
$lang['submit']='Chấp nhận';
$lang['cancel']="Hủy bỏ";
$lang['continue']="Tiếp tục";
$lang['finish']="Hoàn tất";
$lang['top']='Đầu trang';
$lang['back']='Về trang trước';
$lang['field']='thông tin bắt buộc';
$lang['error_field']='Vui lòng nhập đầy đủ các thông tin bắt buộc.';
$lang['validfield']='Vui lòng không nhập < và >.';
$lang['sendsuc']='<b>Chân thành cảm ơn.</b><br>Thông tin yêu cầu của bạn đã được gửi đến chúng tôi.<br>Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.';

$lang['othernews']='Các tin khác';
$lang['otherarticles']='Các bài viết khác';

$lang['search_results']='Kết quả tìm kiếm';
$lang['updating']='Đang cập nhật';
$lang['detail']='chi tiết';
$lang['viewmore']='Xem tiếp';
$lang['viewall']='Xem tất cả';

//contact
$lang['send_email']='Gửi email';
$lang['name']="Họ tên";
$lang['firstname']='Họ';
$lang['middlename']='Tên đệm';
$lang['lastname']='Tên';
$lang['company']='Công ty';
$lang['address']="Địa chỉ";
$lang['city']="Tỉnh/Thành phố";
$lang['country']="Quốc gia";
$lang['tel']='Điện thoại';
$lang['email']='Email';
$lang['subject']='Tiêu đề';
$lang['content']='Nội dung';
$lang['error_name']='Vui lòng nhập họ tên của bạn.';
$lang['error_firstname']='Vui lòng nhập họ của bạn.';
$lang['error_lastname']='Vui lòng nhập tên của bạn.';
$lang['error_email']='Vui lòng nhập email của bạn.';
$lang['invalidemail']='Địa chỉ email không hợp lệ.';
$lang['error_company']='Vui lòng nhập công ty của bạn.';
$lang['error_address']='Vui lòng nhập địa chỉ của bạn.';
$lang['error_city']='Vui lòng nhập tỉnh/thành phố.';
$lang['error_country']='Vui lòng chọn quốc gia.';
$lang['error_tel']='Vui lòng nhập điện thoại của bạn.';
$lang['error_subject']='Vui lòng nhập tiêu đề.';
$lang['error_content']='Vui lòng nhập nội dung.';
$lang['sended_email']='Email của bạn đã được gửi đến chúng tôi.';

?>