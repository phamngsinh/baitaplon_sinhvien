<?php
$lang=array();

$lang['DATE_FORMAT'] = 'n-j-Y';
$lang['language']='Language';

//header
$lang['home']='Home';
$lang['introduction']='Introduction';
$lang['gallery']='Photo Gallery';
$lang['news']='News';
$lang['event']='Promotions';
$lang['contact']='Contact';

$lang['search']='Search';
$lang['google_search']='Google search';
$lang['online_support']='Online Support';
$lang['advertisement']='Advertisement';

$lang['visitors']='Visitors';
$lang['visitorsonline']='Online';
$lang['weblink']='Website links';
$lang['booking_online']='Booking online';

//booking
$lang['roomtypes']='ACCOMMODATION';
$lang['roomrates']='ROOM RATES';
$lang['booking']='Reservation';
$lang['personal_info']='Your Information';
$lang['roomtype']='Room Categories';
$lang['select']='Select';
$lang['book_info']='Booking Information';
$lang['arrival_date']='Arrival Date';
$lang['departure_date']='Departure Date';
$lang['rooms']='Number of Rooms';
$lang['adults']='Number of Adults';
$lang['child']='Number of Children';
$lang['price_info']='Rate Range';
$lang['from_rate']='From Rate';
$lang['to_rate']='To Rate';
$lang['other_info']='Other Requests';
$lang['booksuc']='Thank you for your booking.<br>Our Reservation staff will contact you as soon as possible.';
$lang['error_roomtype']='Please select room categories.';
$lang['error_arrival_date']='Please enter your arrival date.';
$lang['error_rooms']='Please enter number of rooms.';

//more
$lang['send']='Send';
$lang['save']='Save';
$lang['reset']='Reset';
$lang['submit']='Submit';
$lang['cancel']="Cancel";
$lang['continue']="Continue";
$lang['finish']="Finish";
$lang['top']='Top';
$lang['back']='Back';
$lang['field']='required information';
$lang['error_field']='Please enter all required information.';
$lang['validfield']='Please not enter < and >.';
$lang['sendsuc']='<b>Thank you.</b><br>Your request information have been sent to us.<br>We will contact with you as soon as possible.';

$lang['othernews']='Other News';
$lang['otherarticles']='Other articles';

$lang['search_results']='Search Results';
$lang['updating']='In the process of updating';
$lang['detail']='detail';
$lang['viewmore']='View more';
$lang['viewall']='View all';

//contact
$lang['send_email']='Send Email';
$lang['name']="Full Name";
$lang['firstname']='First Name';
$lang['middlename']='Middle Name';
$lang['lastname']='Last Name';
$lang['company']='Company';
$lang['address']="Address";
$lang['city']="City";
$lang['country']="Country";
$lang['tel']='Phone';
$lang['email']='Email';
$lang['subject']='Subject';
$lang['content']='Content';
$lang['error_name']='Please enter full name.';
$lang['error_firstname']='Please enter first name.';
$lang['error_lastname']='Please enter last name.';
$lang['error_email']='Please enter email.';
$lang['invalidemail']='Sorry, invalid email.';
$lang['error_company']='Please enter company.';
$lang['error_address']='Please enter address.';
$lang['error_city']='Please enter city.';
$lang['error_country']='Please select country.';
$lang['error_tel']='Please enter telephone.';
$lang['error_subject']='Please enter subject.';
$lang['error_content']='Please enter content.';
$lang['sended_email']='Your email has been sent to us.';

?>