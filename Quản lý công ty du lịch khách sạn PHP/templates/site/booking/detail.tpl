			<div id="contentleft">
      	<? include("./templates/site/top_main.tpl"); ?>
        <div id="contentleft_cell">
        	<div class="title1"><?=$lang['booking']?></div>
          <div class="space_content">
          	<? if ($_GET['send']){?>
            <div align="center" class="red12"><?=$lang['booksuc']?></div>
            <? } else { ?>
            <div class="black11"><span class="red11">(*)</span> <?=$lang['field']?></div>
            <div id="line4"><img src="images/spacer.gif" /></div>
            <div>
              <form name="frmbook" action="?do=booking&act=booksm" method="post" onSubmit="return chkselect(roomtype, '<?=$lang['error_roomtype']?>') && nospace(firstname, '<?=$lang['error_firstname']?>') && nospace(lastname, '<?=$lang['error_lastname']?>') && nospace(address, '<?=$lang['error_address']?>') && nospace(city, '<?=$lang['error_city']?>') && chkselect(country, '<?=$lang['error_country']?>') && nospace(email, '<?=$lang['error_email']?>') && goodEMail(email, '<?=$lang['invalidemail']?>') && nospace(arrival_date, '<?=$lang['error_arrival_date']?>') && nospace(rooms, '<?=$lang['error_rooms']?>');" style="margin:0px;">
              <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr><td colspan="2" class="booktitle"><?=$lang['personal_info']?></td></tr>
                <tr><td height="5"></td></tr>
                <tr>
                  <td width="150" align="right"><span class="red11">(*)</span> <?=$lang['roomtype']?> :</td>
                  <td>
                  <select name="roomtype">
                  	<option value=""><?=$lang['select']?></option>
                  	<?
                    $sql="select id, title_".$_SESSION['lg']." as title from `roomtypes` order by `order`, id";
										$roomtypes=$db->getAll($sql);
                  	if ($roomtypes) {
                    for ($i=0;$i<count($roomtypes);$i++) {
                  	?>
                    <option value="<?=stripslashes($roomtypes[$i]['title'])?>"><?=stripslashes($roomtypes[$i]["title"])?></option>
                 		<? } } ?>								
                  </select>
                  </td>
                </tr>
                <tr>
                  <td align="right"><span class="red11">(*)</span> <?=$lang['firstname']?> :</td>
                  <td><input type="text" name="firstname" maxlength="50" size="45"></td>
                </tr>
                <tr>
                  <td align="right"><?=$lang['middlename']?> :</td>
                  <td><input type="text" name="middlename" maxlength="50" size="45"></td>
                </tr>
                <tr>
                  <td align="right"><span class="red11">(*)</span> <?=$lang['lastname']?> :</td>
                  <td><input type="text" name="lastname" maxlength="50" size="45"></td>
                </tr>
                <tr>
                  <td align="right"><span class="red11">(*)</span> <?=$lang['address']?> :</td>
                  <td><input type="text" name="address" maxlength="250" size="80"></td>
                </tr>
                <tr>
                  <td align="right"><span class="red11">(*)</span> <?=$lang['city']?> :</td>
                  <td><input type="text" name="city" maxlength="100" size="45"></td>
                </tr>
                <tr>
                  <td align="right"><span class="red11">(*)</span> <?=$lang['country']?> :</td>
                  <td>
                  <select name="country">
                  	<option value=""><?=$lang['select']?></option>
                  	<?
                    $sql="select * from countries order by name";
                    $countries=$db->getAll($sql);
                    if ($countries) {
                    for ($i=0;$i<count($countries);$i++) {
                  	?>
                    <option value="<?=$countries[$i]['name']?>"><?=$countries[$i]['name']?></option>
                 		<? } } ?>								
                  </select>                  
                  </td>
                </tr>
                <tr>
                  <td align="right"><?=$lang['tel']?> :</td>
                  <td><input type="text" name="tel" maxlength="150" size="45"></td>
                </tr>
                <tr>
                  <td align="right"><span class="red11">(*)</span> <?=$lang['email']?> :</td>
                  <td><input type="text" name="email" maxlength="250" size="45"></td>
                </tr>
                <tr><td height="10"></td></tr>
                <tr><td colspan="2" class="booktitle"><?=$lang['book_info']?></td></tr>
                <tr><td height="5"></td></tr>
                <tr>
                  <td align="right"><span class="red11">(*)</span> <?=$lang['arrival_date']?> :</td>
                  <td><input type="text" name="arrival_date" size="15">
									<script language="JavaScript">
                  new tcal ({
                    // form name
                    'formname': 'frmbook',
                    // input name
                    'controlname': 'arrival_date'
                  });                
                  </script>
                  </td>
                </tr>
                <tr>
                  <td align="right"><?=$lang['departure_date']?> :</td>
                  <td><input type="text" name="departure_date" size="15">
									<script language="JavaScript">
                  new tcal ({
                    // form name
                    'formname': 'frmbook',
                    // input name
                    'controlname': 'departure_date'
                  });                
                  </script>                
                  </td>
                </tr>
                <tr>
                  <td align="right"><span class="red11">(*)</span> <?=$lang['rooms']?> :</td>
                  <td><input type="text" name="rooms" size="15"></td>
                </tr> 
                <tr>
                  <td align="right"><?=$lang['adults']?> :</td>
                  <td><input type="text" name="adults" size="15"></td>
                </tr>
                <tr>
                  <td align="right"><?=$lang['child']?> :</td>
                  <td><input type="text" name="child" size="15"></td>
                </tr>
                <tr><td height="10"></td></tr>
                <tr><td colspan="2" class="booktitle"><?=$lang['price_info']?></td></tr>
                <tr><td height="5"></td></tr>    
              	<tr>
                  <td align="right"><?=$lang['from_rate']?> :</td>
                  <td><input type="text" name="from_rate" size="15"> USD</td>
                </tr>
                <tr>
                  <td align="right"><?=$lang['to_rate']?> :</td>
                  <td><input type="text" name="to_rate" size="15"> USD</td>
                </tr>
                <tr><td height="10"></td></tr>
                <tr><td colspan="2" class="booktitle"><?=$lang['other_info']?></td></tr>
                <tr><td height="5"></td></tr>
                <tr>
                  <td align="right"></td>
                  <td><textarea name="other_info" cols="80" rows="8"></textarea></td>
                </tr>               
                <tr><td height="10"></td></tr>
                <tr>
                  <td></td>
                  <td>
                  <input type="submit" value=" <?=$lang['send']?> " class="button">
                  <input type="reset" value=" <?=$lang['reset']?> " class="button">
                  </td>
                </tr>               
              </table>
              </form>
            </div>
            <? } ?>
          </div>
          
          <div id="line1"><img src="images/spacer.gif" /></div>
            
          <div align="right"><img src="images/top.gif" align="absmiddle"> <a href="#top" class="green11"><?=$lang['top']?></a></div>	 
        </div>
			</div>