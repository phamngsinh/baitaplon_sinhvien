			<div id="contentleft">
      	<? include("./templates/site/top_main.tpl"); ?>
        <div id="contentleft_cell">
        	<div class="title1"><?=$lang['gallery']?></div>
          <div class="space_content">
          <? if ($gallery) { ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <? 
            for ($j=0;$j<count($gallery);$j++) { 
              if ($j % 3 == 0) echo '<tr>';
            ?>
                <td align="center"><a href="upload/gallery/<?=$gallery[$j]['img']?>" class="highslide" onclick="return hs.expand(this)"><img src="upload/gallery/<?=$gallery[$j]['img']?>" width="180" height="120" border="0" style="margin:0px 0px 5px 0px;" class="imgbor"></a><br /><?=stripslashes($gallery[$j]['title'])?></a></td>
            <?
                if ($j % 3 == 2) echo '</tr><tr><td height="15"></td></tr>';
              }				
              if ($j % 3 != 0) echo '</tr><tr><td height="15"></td></tr>';
            ?>
            </table>
            
            <? if ($plpage) { ?>
            <div align="center" style="padding-bottom:15px;"><?=$plpage?></div>
            <? } ?>
            
            <div align="right"><img src="images/top.gif" align="absmiddle"> <a href="#top" class="green11"><?=$lang['top']?></a></div>
          <? } ?>
          </div>
        </div>
			</div>