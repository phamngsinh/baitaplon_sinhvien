<?php
/**
* @package Autor
* @author Duong Khoan Giam
* @website http://www.tccomputervn.com
* @email duongkhoangiam@gmail.com
* @copyright Jimmy
* @license For Jimmy 0978904060
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

require_once(dirname(__FILE__).DS.'helper.php');

$folder = modpetroslideHelper::getFolder($params);
$images = modpetroslideHelper::getImages($params,$folder);
if (!count($images)) {
	echo JText::_('No Images To Display');
	return;
}
$document =& JFactory::getDocument();
$document->addScript('modules/mod_petroslide/js/jquery-1.6.1.min.js');
$document->addScript('modules/mod_petroslide/js/mooslide.js');
$document->addStyleSheet('modules/mod_petroslide/css/mooslide.css');

$sitelink = JURI::base();
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
require JModuleHelper::getLayoutPath('mod_petroslide');
?>
