<?php
/**
* @package Autor
* @author Duong Khoan Giam
* @website http://www.tccomputervn.com
* @email duongkhoangiam@gmail.com
* @copyright Jimmy
* @license For Jimmy 0978904060
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

class modpetroslideHelper
{
	static function getFolder(&$params)
	{
		$folder	= $params->get('folder');

		$LiveSite	= JURI::base();

		// if folder includes livesite info, remove
		if (JString::strpos($folder, $LiveSite) === 0) {
			$folder = str_replace($LiveSite, '', $folder);
		}
		// if folder includes absolute path, remove
		if (JString::strpos($folder, JPATH_SITE) === 0) {
			$folder= str_replace(JPATH_BASE, '', $folder);
		}
		$folder = str_replace('\\', DS, $folder);
		$folder = str_replace('/', DS, $folder);

		return $folder;
	}
	static function getImages(&$params, $folder)
	{
		$type		= $params->get('type', 'jpg');

		$files	= array();
		$images	= array();

		$dir = JPATH_BASE.DS.'images/'.$folder;

		// check if directory exists
		if (is_dir($dir))
		{
			if ($handle = opendir($dir)) {
				while (false !== ($file = readdir($handle))) {
					if ($file != '.' && $file != '..' && $file != 'CVS' && $file != 'index.html') {
						$files[] = $file;
					}
				}
			}
			closedir($handle);

			$i = 0;
			foreach ($files as $img)
			{
				if (!is_dir($dir .DS. $img))
				{
					//if (preg_match('/'.$type.'/', $img)) {
						$images[$i] = new stdClass;
						
						$images[$i]->name	= $img;
						$images[$i]->folder	= $folder;
						$images[$i]->width = $params->get('width');
						$images[$i]->height = $params->get('height');
						$i++;
					//}
				}
			}
		}

		return $images;
	}
	static function Resize(&$params,$img)
	{
		if($params->get('resize') == 0)
		{
			return $img;
		}
		$width = $params->get('width');
		$height = $params->get('height');
		return;
	}
}

?>