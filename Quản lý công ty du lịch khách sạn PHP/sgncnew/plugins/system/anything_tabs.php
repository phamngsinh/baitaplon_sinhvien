<?php
/**
* @copyright	Copyright (C) 2010 Pixel Point Creative. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
*/

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );


/**
 * Anything tabs Plugin
 *
 * @package		Joomla
 * @subpackage	System
 * @since 		1.5
 */
class plgSystemAnything_tabs extends JPlugin
{
	var $p_type = '';
	var $p_name = '';
	
	/**
	 * Constructor
	 *
	 * For php4 compatability we must not use the __constructor as a constructor for plugins
	 * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
	 * This causes problems with cross-referencing necessary for the observer design pattern.
	 *
	 * @param object $subject The object to observe
	 * @param object $params  The object that holds the plugin parameters
	 * @since 1.5
	 */
	function plgSystemAnything_tabs( &$subject, $params )
	{
		$this->p_type = $params['type'];
		$this->p_name = $params['name'];
		parent::__construct( $subject, $params );
	}
	
	function onAfterRender(){
		global $mainframe;
		
		if($mainframe->isAdmin()){
			return true;
		}
		
		// Get plugin info
		$plugin =& JPluginHelper::getPlugin($this->p_type, $this->p_name);
		$pluginParams = new JParameter( $plugin->params );
		
		ob_start();
		
		//tab one
		if ($pluginParams->get('tabone') == '1') {
?>

<div style="position:fixed;right:0;bottom:220px;width:37px;z-index:1000;" id="tabone">
<a target="<?php echo $pluginParams->get('window'); ?>" href="<?php echo $pluginParams->get('tabonelink'); ?>">
		<img border="0" src="<?php echo JURI::Base()?>plugins/system/anything_tabs/tabs/<?php echo $pluginParams->get('tabonetheme'); ?>/<?php echo $pluginParams->get('taboneimage'); ?>" width="37" height="37" title="<?php echo $pluginParams->get('tabonetitle'); ?>" alt="<?php echo $pluginParams->get('tabonealt'); ?>" />
	</a>
</div>
<?php
		}
		
		//tab two
		if ($pluginParams->get('tabtwo') == '1') {
?>
<div style="position:fixed;right:0;bottom:180px;width:37px;z-index:1000;" id="tabtwo">
	<a target="<?php echo $pluginParams->get('window2'); ?>" href="<?php echo $pluginParams->get('tabtwolink'); ?>">
		<img border="0" src="<?php echo JURI::Base()?>plugins/system/anything_tabs/tabs/<?php echo $pluginParams->get('tabtwotheme'); ?>/<?php echo $pluginParams->get('tabtwoimage'); ?>" width="37" height="37" title="<?php echo $pluginParams->get('tabtwotitle'); ?>" alt="<?php echo $pluginParams->get('tabtwoalt'); ?>" />
	</a>
</div>
<?php
		}
		
		//tab three
		if ($pluginParams->get('tabthree') == '1') {
?>
<div style="position:fixed;right:0;bottom:140px;width:37px;z-index:1000;" id="tabthree">
	<a target="<?php echo $pluginParams->get('window3'); ?>" href="<?php echo $pluginParams->get('tabthreelink'); ?>">
		<img border="0" src="<?php echo JURI::Base()?>plugins/system/anything_tabs/tabs/<?php echo $pluginParams->get('tabthreetheme'); ?>/<?php echo $pluginParams->get('tabthreeimage'); ?>" width="37" height="37" title="<?php echo $pluginParams->get('tabthreetitle'); ?>" alt="<?php echo $pluginParams->get('tabthreealt'); ?>" />
	</a>
</div>
<?php
		}
		
		//tab four
		if ($pluginParams->get('tabfour') == '1') {
?>
<div style="position:fixed;right:0;bottom:100px;width:37px;z-index:1000;" id="tabfour">
	<a target="<?php echo $pluginParams->get('window4'); ?>" href="<?php echo $pluginParams->get('tabfourlink'); ?>">
		<img border="0" src="<?php echo JURI::Base()?>plugins/system/anything_tabs/tabs/<?php echo $pluginParams->get('tabfourtheme'); ?>/<?php echo $pluginParams->get('tabfourimage'); ?>" width="37" height="37" title="<?php echo $pluginParams->get('tabfourtitle'); ?>" alt="<?php echo $pluginParams->get('tabfouralt'); ?>" />
	</a>
</div>
<?php
		}
		
		//tab five
		if ($pluginParams->get('tabfive') == '1') {
?>
<div style="position:fixed;right:0;bottom:60px;width:37px;z-index:1000;" id="tabfive">
	<a target="<?php echo $pluginParams->get('window5'); ?>" href="<?php echo $pluginParams->get('tabfivelink'); ?>">
		<img border="0" src="<?php echo JURI::Base()?>plugins/system/anything_tabs/tabs/<?php echo $pluginParams->get('tabfivetheme'); ?>/<?php echo $pluginParams->get('tabfiveimage'); ?>" width="37" height="37" title="<?php echo $pluginParams->get('tabfivetitle'); ?>" alt="<?php echo $pluginParams->get('tabfivealt'); ?>" />
	</a>
</div>
<?php
		}
		
		//tab six
		if ($pluginParams->get('tabsix') == '1') {
?>
<div style="position:fixed;right:0;bottom:20px;width:37px;z-index:1000;" id="tabsix">
	<a target="<?php echo $pluginParams->get('window6'); ?>" href="<?php echo $pluginParams->get('tabsixlink'); ?>">
		<img border="0" src="<?php echo JURI::Base()?>plugins/system/anything_tabs/tabs/<?php echo $pluginParams->get('tabsixtheme'); ?>/<?php echo $pluginParams->get('tabsiximage'); ?>" width="37" height="37" title="<?php echo $pluginParams->get('tabsixtitle'); ?>" alt="<?php echo $pluginParams->get('tabsixalt'); ?>" />
	</a>
</div>
<?php
		}
		$str_html = ob_get_contents();
		ob_end_clean();
		
		$body = JResponse::getBody();
		$body = str_replace('</body>', $str_html.'</body>', $body);
		JResponse::setBody($body);
		
		return true;
	}

}

