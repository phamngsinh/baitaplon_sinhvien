<?php
/**
* YOOgallery Joomla! Plugin
*
* @author    yootheme.com
* @copyright Copyright (C) 2008 YOOtheme Ltd. & Co. KG. All rights reserved.
* @license	 GNU/GPL
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

Changelog
------------

1.5.1
+ Added PHP GD image processing lib check
+ Added smooth image resampling
+ Added thumbnail resize option
+ Added custom lightbox rel parameter
+ Added thumbnail ordering option

1.5.0
- Initial Release