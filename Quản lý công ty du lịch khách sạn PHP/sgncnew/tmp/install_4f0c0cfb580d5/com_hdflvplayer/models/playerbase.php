<?php
/**
 * @version		$Id: playerbase.php 1.4 2010-11-30 $
 * @package		Joomla
 * @subpackage	hdflvplayer
 * @copyright Copyright (C) 2010-2011 Contus Support
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
defined('_JEXEC') or die();

jimport( 'joomla.application.component.model' );


class hdflvplayerModelplayerbase extends JModel
{
    /**
     * Gets the greeting
     *
     * @return string The greeting to be displayed to the user
     */
    function playerskin()
    {
        //$playerpath = JURI::base().'components/com_hdflvplayer/hdflvplayer/hdplayer.swf';
        $playerpath = realpath(dirname(__FILE__) . '/../hdflvplayer/hdplayer.swf');
        $this->showplayer($playerpath);
       
    }

    function showplayer($playerpath)
    {
        
        ob_clean();
        header("content-type:application/x-shockwave-flash");
        readfile($playerpath);
       //require('components/com_hflvplayer/hdflvplayer/hdplayer.swf');//includes the swf player file.
        exit();
        
    }
}