<?php
/**
 * @version		$Id: googleadd.php 1.4 2010-11-30 $
 * @package		Joomla
 * @subpackage	hdflvplayer
 * @copyright Copyright (C) 2010-2011 Contus Support
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport( 'joomla.application.component.model' );


class hdflvplayerModelgoogleadd extends JModel
{
	/**
	 * Gets the greeting
	 * 
	 * @return string The greeting to be displayed to the user
	 */
	function googlescript()
	{
            global $db;
            $db =& JFactory::getDBO();
            $query1 = "select * from #__hdflvaddgoogle where publish='1' and id='1'";
            $db->setQuery( $query1 );
            $fields = $db->loadObjectList();
            echo html_entity_decode(stripcslashes($fields[0]->code));
            exit();
	}
}