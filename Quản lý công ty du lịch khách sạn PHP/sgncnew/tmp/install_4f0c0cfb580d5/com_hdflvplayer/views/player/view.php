<?php
/**
 * @version		$Id: view.php 1.4 2010-11-30 $
 * @package		Joomla
 * @subpackage	hdflvplayer
 * @copyright Copyright (C) 2010-2011 Contus Support
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');



class hdflvplayerViewplayer extends JView
{

	function displayplayer()
	{
        $model =& $this->getModel();
		$detail = $model->showhdplayer();
		$this->assignRef('detail', $detail);
        $this->setLayout('playerlayout');
		parent::display();
	}

}
?>   
