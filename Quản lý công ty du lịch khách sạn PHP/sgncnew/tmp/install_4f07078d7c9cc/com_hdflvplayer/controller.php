<?php
/**
 * @version		$Id: controller.php 1.4 2010-11-30 $
 * @package		Joomla
 * @subpackage	hdflvplayer
 * Copyright (c) 2010 Contus Support - support@hdflvplayer.net
 * License: GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
 
// no direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport('joomla.application.component.controller');
 

class hdflvplayerController extends JController
{
	/**
	 * Method to display the view
	 *
	 * @access    public
	 */
    function display()
	{
        $view = & $this->getView('player');
      
        if ($model = & $this->getModel('player')) {
			//Push the model into the view (as default)
			//Second parameter indicates that it is the default model for the view
			$view->setModel($model, true);
		}
        $view->displayplayer();
		
	}
    function configxml()
    {
        $view = & $this->getView('configxml');
      
        if ($model = & $this->getModel('configxml')) {
			//Push the model into the view (as default)
			//Second parameter indicates that it is the default model for the view
			$view->setModel($model, true);
		}
        $view->configget();
     
    }

    function  playxml()
    {
        $view = & $this->getView('playxml');

        if ($model = & $this->getModel('playxml')) {
			//Push the model into the view (as default)
			//Second parameter indicates that it is the default model for the view
			$view->setModel($model, true);
		}
        $view->playget();

    }

     function  videourl()
    {
        
        $view = & $this->getView('videourl');

        if ($model = & $this->getModel('videourl')) {
			//Push the model into the view (as default)
			//Second parameter indicates that it is the default model for the view
			$view->setModel($model, true);
		}
        $view->getvideourl();

    }


    function  player()
    {
        
        $view = & $this->getView('playerbase');

        if ($model = & $this->getModel('playerbase')) {
			//Push the model into the view (as default)
			//Second parameter indicates that it is the default model for the view
			$view->setModel($model, true);
		}
        $view->loadplayer();

    }




    function  adsxml()
    {
        $view = & $this->getView('adsxml');

        if ($model = & $this->getModel('adsxml')) {
			//Push the model into the view (as default)
			//Second parameter indicates that it is the default model for the view
			$view->setModel($model, true);
		}
        $view->getads();

    }


     function email()
    {
        $view = & $this->getView('email');

        if ($model = & $this->getModel('email')) {
			//Push the model into the view (as default)
			//Second parameter indicates that it is the default model for the view
			$view->setModel($model, true);
		}
        $view->emailplayer();

    }
function googleadd()
    {

        $view = & $this->getView('googleadd');

        if ($model = & $this->getModel('googleadd')) {
			//Push the model into the view (as default)
			//Second parameter indicates that it is the default model for the view
			$view->setModel($model, true);
		}
        $view->googlescript();

    }

    function  languagexml()
    {
        $view = & $this->getView('language');

        if ($model = & $this->getModel('languagexml')) {
			//Push the model into the view (as default)
			//Second parameter indicates that it is the default model for the view
			$view->setModel($model, true);
		}
        
        $view->language();

    }
     function  addview()
    {
        $view = & $this->getView('addcount');

        if ($model = & $this->getModel('addview')) {
			//Push the model into the view (as default)
			//Second parameter indicates that it is the default model for the view
			$view->setModel($model, true);
		}

        $view->getaddview();

    }
      function  impressionclicks()
    {
        $view = & $this->getView('impressionclicks');

        if ($model = & $this->getModel('impressionclicks')) {
			//Push the model into the view (as default)
			//Second parameter indicates that it is the default model for the view
			$view->setModel($model, true);
		}

        $view->impressionclicks();

    }
   
}
?>