<?php
switch ($act){
	case "detail":
		showdetail();
		$tpl="news/detail";
		break;
	default:
		shownews();
		$tpl="news/list";
		break;	
}

function shownews() {
	global $db,$news,$page,$plpage,$set_per_page;
	$set_per_page=5;
	
	$sqlstmt="select id, title_".$_SESSION['lg']." as title, img, desc_".$_SESSION['lg']." as shortdesc, special, new, date from `news` where 1=1";
	$sqlstmt.=" order by `order` desc, id desc";
	
	$plpage = plpage($sqlstmt,$page,$set_per_page);
	$sqlstmt = sqlmod($sqlstmt,$page,$set_per_page);
	$news = $db->getAll($sqlstmt);
}

function showdetail() {
	global $db,$news,$others;
	$id=$_GET['id'];
	
	$sqlstmt="select id, title_".$_SESSION['lg']." as title, img, desc_".$_SESSION['lg']." as shortdesc, content_".$_SESSION['lg']." as content, special, new, date from news where id=$id";	
	$news = $db->getRow($sqlstmt);
	if (!$news) page_transfer2("?do=news");
		
	$sql="select id, title_".$_SESSION['lg']." as title, special, new, date from news where id<>".$news['id']." order by `order` desc, id desc limit 0,5";
	$others=$db->getAll($sql);
}

?>