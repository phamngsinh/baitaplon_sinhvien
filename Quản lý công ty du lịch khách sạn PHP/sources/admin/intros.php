<?php
switch($act){
	case("add"):
	case("edit"):
		if($act=='edit') show_edit();
		$tpl ="intros/edit";			
		break;
	case("addsm"):
	case("editsm"):
		add_edit();
		break;	
	case "del":
		del();
		break;
	case "delselect":
		delselect();
		break;
	default:
		showintros();
		$tpl ="intros/list";
		break;
}

function showintros() {
	global $db,$intros;
	$sqlstmt="select id, title_vn from `intro` order by id";
	$intros=$db->getAll($sqlstmt);
}

function show_edit(){
	global $db,$intros;
	$id=$_GET['id'];
	$sqlstmt="select * from `intro` where id=$id";
	$intros=$db->getRow($sqlstmt);
	if (!$intros) page_transfer2("?do=intros");
}

function add_edit(){
	global $db,$act;
	$arr['title_vn']=htmlspecialchars($_POST['title_vn'], ENT_QUOTES);
	$arr['title_en']=htmlspecialchars($_POST['title_en'], ENT_QUOTES);
	$arr['title_ru']=htmlspecialchars($_POST['title_ru'], ENT_QUOTES);
	$arr['content_vn']=addslashes($_POST['content_vn']);
	$arr['content_en']=addslashes($_POST['content_en']);
	$arr['content_ru']=addslashes($_POST['content_ru']);
	
	if($act=='addsm'){
		vaInsert('intro',$arr);		
		$msg="Thêm mới thành công";
	}	elseif($act=='editsm'){
		$id=$_POST['id'];		
		vaUpdate('intro',$arr,'id='.$id);
		$msg="Cập nhật thành công";
	}		
	$page="?do=intros";
	page_transfer($msg,$page);
}

function del(){
	global $db;
	$id=$_GET["id"];
	$sqldel="delete from intro where id=$id";
	$db->query($sqldel);
	$page="?do=intros";
	$msg="Xóa thành công";
	page_transfer($msg,$page);
}

function delselect(){
	global $db;
	$arrid=$_POST['checkid'];
	for ($i=0;$i<count($arrid);$i++){
		$id=$arrid[$i];
		$sqldel="delete from intro where id=$id";
		$db->query($sqldel);
	}
	$msg="Xóa thành công";	
	$page="?do=intros";
	page_transfer($msg,$page);
}

?>