<?php
switch($act){
	case "log_out":	
		log_out();	
		break;
	case "loginsm":	
		login();
		break;
	default:
		$tpl="login";
}

function log_out() {
	session_unregister("admin_login");
	session_unregister("admin_id");
	session_unregister("right");
	$msg = "Thoát thành công";             
	$page = "admin.php";             
	page_transfer($msg,$page);        
}

function login() {
	global $db;
   $username = isset($_POST["username"]) ? $_POST["username"] : '';
   $password = isset($_POST["password"]) ? $_POST["password"] : '';
	//-------------------------------------------------
	$sql_select = "select * from admin where username='" . $username . "'";
	$result=$db->getRow($sql_select);
	
	if (!$result) {
		$msg = "Người dùng này không có trong hệ thống";
		$page = "admin.php?do=login&error=1";
		page_transfer($msg,$page);
	}
	
	if (md5($password)!=$result["password"]) {
		$msg = "Mật khẩu không chính xác";
		$page = "admin.php?do=login&error=1";
		page_transfer($msg,$page);
	}

	if (session_unregister("admin_login")) {
		session_register("admin_login");
		$_SESSION["admin_login"]    = $username;
		session_register("admin_id");
		$_SESSION["admin_id"]    = $result['id'];
		session_register("right");
		$_SESSION["right"]    = $result['right'];
		$msg = "Đăng nhập thành công";
		$page = "admin.php";
		page_transfer($msg,$page);
	}
}

?>