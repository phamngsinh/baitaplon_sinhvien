<?php
switch($act){
	case("editsm"):
		edit();
		break;
	default:
		showintro();			
		$tpl ="intro/edit";
		break;
}

function showintro() {
	global $db,$intro;
	$id=$_GET['id'];
	$sqlstmt="select * from `intro` where id=$id";
	$intro=$db->getRow($sqlstmt);
	if (!$intro) page_transfer2("?do=main");			
}

function edit(){	
	global $db;
	$arr['email']=htmlspecialchars($_POST['email'], ENT_QUOTES);
	$arr['content_vn']=addslashes($_POST['content_vn']);
	$arr['content_en']=addslashes($_POST['content_en']);
	$arr['content_ru']=addslashes($_POST['content_ru']);
	$id=$_POST['id'];			
	vaUpdate('intro',$arr,'id='.$id);
	$msg="Cập nhật thành công";
	$page="?do=intro&id=$id";
	page_transfer($msg,$page);
}

?>