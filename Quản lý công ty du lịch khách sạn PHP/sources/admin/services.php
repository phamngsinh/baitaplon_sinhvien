<?php
switch($act){
	case("add"):
	case("edit"):
		if($act=='edit') show_edit();
		$tpl ="services/edit";			
		break;
	case("addsm"):
	case("editsm"):
		add_edit();
		break;
	case "move":
		move();
		break;
	case "del":
		del();
		break;
	case "delselect":
		delselect();
		break;
	default:
		showservices();
		$tpl ="services/list";
		break;
}

function showservices() {
	global $db,$services;
	$sqlstmt="select id, title_vn, menu, `order` from `services` order by `order`, id";
	$services=$db->getAll($sqlstmt);
}

function show_edit(){
	global $db,$services;
	$id=$_GET['id'];
	$sqlstmt="select * from `services` where id=$id";
	$services=$db->getRow($sqlstmt);
	if (!$services) page_transfer2("?do=services");
}

function add_edit(){
	global $db,$act;
	$arr['title_vn']=htmlspecialchars($_POST['title_vn'], ENT_QUOTES);
	$arr['title_en']=htmlspecialchars($_POST['title_en'], ENT_QUOTES);
	$arr['title_ru']=htmlspecialchars($_POST['title_ru'], ENT_QUOTES);	
	$arr['content_vn']=addslashes($_POST['content_vn']);
	$arr['content_en']=addslashes($_POST['content_en']);
	$arr['content_ru']=addslashes($_POST['content_ru']);
	$arr['menu']=$_POST['menu']?$_POST['menu']:0;
	
	if($act=='addsm'){	
		$sqlmaxvt="select max(`order`) as maxorder from services";		
		$remaxvt=$db->getRow($sqlmaxvt);
		$arr['order']=$remaxvt['maxorder']+1;	
		vaInsert('services',$arr);		
		$msg="Thêm mới thành công";
	}	elseif($act=='editsm'){
		$id=$_POST['id'];		
		vaUpdate('services',$arr,'id='.$id);
		$msg="Cập nhật thành công";
	}		
	$page="?do=services";
	page_transfer($msg,$page);
}

function move(){
	global $db;
	$id1=$_GET['id1'];
	$id2=$_GET['id2'];
	$pos1=$_GET['pos1'];
	$pos2=$_GET['pos2'];
	$update1="UPDATE services SET `order` = ".$pos1." WHERE id=".$id1;
	$db->query($update1);
	$update2="UPDATE services SET `order` = ".$pos2." WHERE id=".$id2;
	$db->query($update2);	
	$page="?do=services";
	page_transfer2($page);		
}

function del(){
	global $db;
	$id=$_GET["id"];
	$sqldel="delete from services where id=$id";
	$db->query($sqldel);
	$page="?do=services";
	$msg="Xóa thành công";
	page_transfer($msg,$page);
}

function delselect(){
	global $db;
	$arrid=$_POST['checkid'];
	for ($i=0;$i<count($arrid);$i++){
		$id=$arrid[$i];
		$sqldel="delete from services where id=$id";
		$db->query($sqldel);
	}
	$msg="Xóa thành công";	
	$page="?do=services";
	page_transfer($msg,$page);
}

?>