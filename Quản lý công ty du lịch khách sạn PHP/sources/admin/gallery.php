<?php
switch($act){
	case("add"):
	case("edit"):
		if($act=='edit') show_edit();		
		$tpl ="gallery/edit";			
		break;
	case("addsm"):
	case("editsm"):
		add_edit();
		break;
	case "move":
		move();
		break;
	case "del":
		del();
		break;
	case "delselect":
		delselect();
		break;
	default:
		showgallery();			
		$tpl ="gallery/list";
		break;
}

function showgallery() {
	global $db,$gallery,$page,$plpage,$set_per_page;
	$set_per_page=10;
	$key=trim($_GET['key']);
	$sqlstmt="select id, title_vn, img, special, new, `order`, date from `gallery` where 1=1";
	if ($key) $sqlstmt.=" and (title_vn like '%".$key."%' or title_en like '%".$key."%' or title_ru like '%".$key."%')";
	$sqlstmt.=" order by special desc, `order` desc, id desc";	
	$plpage = plpage($sqlstmt,$page,$set_per_page);
	$sqlstmt = sqlmod($sqlstmt,$page,$set_per_page);
	$gallery = $db->getAll($sqlstmt);
}

function show_edit(){
	global $db,$gallery;
	$id=$_GET['id'];
	$sqlstmt="select * from `gallery` where id=$id";
	$gallery = $db->getRow($sqlstmt);
	if (!$gallery) page_transfer2("?do=gallery");			
}

function add_edit(){
	global $db,$act;
	$arr['title_vn']=htmlspecialchars($_POST['title_vn'], ENT_QUOTES);
	$arr['title_en']=htmlspecialchars($_POST['title_en'], ENT_QUOTES);
	$arr['title_ru']=htmlspecialchars($_POST['title_ru'], ENT_QUOTES);
	$arr['desc_vn']=addslashes($_POST['desc_vn']);
	$arr['desc_en']=addslashes($_POST['desc_en']);
	$arr['desc_ru']=addslashes($_POST['desc_ru']);
	$arr['new']=$_POST['new']?$_POST['new']:0;
	$arr['special']=$_POST['special']?$_POST['special']:0;
	
	$img = isset($_FILES['img']['name']) ? $_FILES['img']['name'] : '';
	$sizeimg = $_FILES['img']['size'];
	$filename='';
	//--------------------------------------------------
	if(!empty($img)&& $sizeimg>0){
		$start = strpos($img,".");
		$type = substr($img,$start,strlen($img));
		$filename = "img_".time().$type;
		$filename = strtolower($filename);
		copy($_FILES['img']['tmp_name'], "./upload/gallery/" . $filename) ;
		$arr['img']=$filename;
  }
	
	if($act=='addsm'){
		$arr['date']=date("Y-m-d H:i:s");
		$sqlmaxvt="select max(`order`) as maxorder from gallery";		
		$remaxvt=$db->getRow($sqlmaxvt);
		$arr['order']=$remaxvt['maxorder']+1;	
		vaInsert('gallery',$arr);		
		$msg="Thêm mới thành công";	
	}	elseif($act=='editsm'){
		$id=$_POST['id'];
		if ($_POST['noimage'] || $sizeimg>0){		
			$sqlstmt="select img from `gallery` where id=$id";
			$r = $db->getRow($sqlstmt);
			$img_old=$r['img'];	
			if($_POST['noimage']) $arr['img']='';
			if($sizeimg>0) $arr['img']=$filename;
			$delfile = "./upload/gallery/".$img_old;
			if(file_exists($delfile) &&$img_old!="") unlink($delfile);
		}
		vaUpdate('gallery',$arr,'id='.$id);
		$msg="Cập nhật thành công";
	}		
	$page="?do=gallery&page=".$_POST['page'];
	page_transfer($msg,$page);
}

function move(){
	global $db;
	$id1=$_GET['id1'];
	$id2=$_GET['id2'];
	$pos1=$_GET['pos1'];
	$pos2=$_GET['pos2'];
	$update1="UPDATE gallery SET `order` = ".$pos1." WHERE id=".$id1;
	$db->query($update1);
	$update2="UPDATE gallery SET `order` = ".$pos2." WHERE id=".$id2;
	$db->query($update2);	
	$page="?do=gallery&page=".$_GET['page'];
	page_transfer2($page);		
}

function del(){
	global $db;
	$id=$_GET["id"];
	
	$sqlstmt="select img from `gallery` where id=$id";
	$r = $db->getRow($sqlstmt);
	$img_old=$r['img'];
	$delfile = "./upload/gallery/".$img_old;
	if(file_exists($delfile) && $img_old!="") unlink($delfile);
			
	$sqldel="delete from gallery where id=$id";
	$db->query($sqldel);
	
	$page="?do=gallery&page=".$_GET['page'];
	$msg="Xóa thành công";
	page_transfer($msg,$page);
}

function delselect(){
	global $db;
	$arrid=$_POST['checkid'];
	
	for ($i=0;$i<count($arrid);$i++){
		$id=$arrid[$i];
			
		$sqlstmt="select img from `gallery` where id=$id";
		$r = $db->getRow($sqlstmt);
		$img_old=$r['img'];
		$delfile = "./upload/gallery/".$img_old;
		if(file_exists($delfile) && $img_old!="") unlink($delfile);
		
		$sqldel="delete from gallery where id=$id";
		$db->query($sqldel);
	}
	
	$page="?do=gallery&page=".$_GET['page'];
	$msg="Xóa thành công";
	page_transfer($msg,$page);
}

?>