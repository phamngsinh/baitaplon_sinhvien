var reInt=/^\d+$/;
var reFloat=/^\d+(\.\d+)?$/;
var reUser=/^\w+$/;
var reChar=/^[\w\s\-\&\#]+$/;
var reEMail=/^\w[\w\-\.]+\@\w[\w\-]+(\.[\w\-]+)+$/;
var reURL=/^\w[\w\-]+(\.\w[\w\-]+)+([\/\%\?\&\+\#\.\w\-\=]+)*$/;
var rePrice=/^[\d\.]+$/;

function nospace(field, msg) { //Check not empty
	if (field.value=="")
		{
		    alert(msg);
		    field.select();
		    field.focus();
		    return false;
		}

	return true;
}

function nospace2(field, msg) { //Check not empty
	if (field.value=="")
		{
		    alert(msg);
				field.focus();
		    return false;
		}

	return true;
}

function chkselect(field, msg) { //Check not empty
	if (field.value=="")
		{
		    alert(msg);
			field.focus();
		    return false;
		}

	return true;
}

function chkimagetype(field, msg)
{
	var ext = field.value;	
	if (ext!='')
	{
		ext = ext.substring(ext.length-3,ext.length);
		ext = ext.toLowerCase();
		if(ext != 'gif' && ext != 'jpg') 
		{
			alert (msg);         
			return false; 
		}
	} 
	return true; 
}

function _checkIt(re, field, msg) {
  if (field.value=='')
  	return true;
  else
  	{
	  if (!re.test(field.value)) {
	    alert(msg);
	    field.select();
	    field.focus();
	    return false;
	  }
	  return true;
  	}
}

function maxlen(n, m, field, msg) {
	if ((field.value.length<n) || (field.value.length>m))
		{
		    alert(msg);
		    field.select();
		    field.focus();
		    return false;
		}

	return true;
}

function goodChar(field, msg) {
  return _checkIt(reChar, field, msg);
}

function goodUser(field, msg) {
  return _checkIt(reUser, field, msg);
}

function goodEMail(field, msg) {
  return _checkIt(reEMail, field, msg);
}

function goodInt(field, msg) {
  return _checkIt(reInt, field, msg);
}

function goodFloat(field, msg) {
  return _checkIt(reFloat, field, msg);
}

function confirmpass(pass1, pass2, msg) {
	if (pass1.value!=pass2.value)
		{
		    alert(msg);
		    pass2.select();
		    pass2.focus();
		    return false;
		}

	return true;
}

function rpChar(f) {
  var df=f;

  df=df.replace(/\\/g, '\\\\');
  df=df.replace(/\//g, '\\\/');
  df=df.replace(/\[/g, '\\\[');
  df=df.replace(/\]/g, '\\\]');
  df=df.replace(/\(/g, '\\\(');
  df=df.replace(/\)/g, '\\\)');
  df=df.replace(/\{/g, '\\\{');
  df=df.replace(/\}/g, '\\\}');
  df=df.replace(/\</g, '\\\<');
  df=df.replace(/\>/g, '\\\>');
  df=df.replace(/\|/g, '\\\|');
  df=df.replace(/\*/g, '\\\*');
  df=df.replace(/\?/g, '\\\?');
  df=df.replace(/\+/g, '\\\+');
  df=df.replace(/\^/g, '\\\^');
  df=df.replace(/\$/g, '\\\$');

  return df;
}

function reDate(f) {
  var df=rpChar(f);

  df=df.replace(/dd/gi, '\\d\\d');
  df=df.replace(/mm/gi, '\\d\\d');
  df=df.replace(/yyyy/gi, '\\d\\d\\d\\d');

  return new RegExp('^'+df+'$');
}

function goodDate(df, field, msg) {
  if (field.value=='')
  	return true;
  else
  	{  
	  if (_checkIt(reDate(df), field, msg)) {
		var di=field.value;
		var y4=df.search(/yyyy/i), y=di.substring(y4, y4+4)-0;
		var m2=df.search(/mm/i), m=di.substring(m2, m2+2)-1;
		var d2=df.search(/dd/i), d=di.substring(d2, d2+2)-0;
	
		var dd=new Date(y, m, d);
		if (y==dd.getFullYear() && m==dd.getMonth() && d==dd.getDate()) {
		  return true;
		}
		else {
		  alert(msg);
	
		  field.select();
		  field.focus();
		}
	  }	
	  return false;
	}
}

function _stringIt(df, d) {
  var y4=df.search(/yyyy/i), m2=df.search(/mm/i), d2=df.search(/dd/i);
  return d.substring(y4, y4+4)+d.substring(m2, m2+2)+d.substring(d2, d2+2);
}

function goodDateRange(df, field1, field2, msg) {
  if (goodDate(df, field1, msg) && goodDate(df, field2, msg)) {
    if (_stringIt(df, field1.value)>_stringIt(df, field2.value)) {
      alert(msg);
      field2.focus();
    }
    else {
      return true;
    }
  }

  return false;
}

function check() {
	if (confirm("Bạn có muốn xóa dòng này?")) return true;
	return false;
}

function confirmdelete(msg) {
	if (confirm(msg)) return true;
	return false;
}

function nofile(field, msg) { //Check not empty
	if (field.value=="")
		{
		    alert(msg);
		    return false;
		}

	return true;
}

function checkAll(field) {
	var value = field.checked;
	var form = field.form;
	var items = form.elements['checkid[]'];
	if (items.length) {
		var i;
		for (i = 0; i < items.length; i++)
			items[i].checked = value;
	} else {
		items.checked=value;
	}
}

function checkboxClicked(form) {
	var selectAllBox = form.elements['checkall'];
	var items = form.elements['checkid[]'];
	selectAllBox.checked = allCheckboxesAreChecked(items);
}

function allCheckboxesAreChecked(checkboxes) {
	if (checkboxes.length) {
		var allChecked = true;
		for (var i = 0; i < checkboxes.length; i++) {
			if (!checkboxes[i].checked)
				return false;
		}
		return true;
	}
	return checkboxes.checked;
}

function goodPrice(field, msg) {	
	if (!rePrice.test(field.value) || field.value<0) {
		alert(msg);
		field.select();
		field.focus();
		return false;
	}
	return true;  
}

function validText(field, msg) {
	var value = field.value;
	var chaos = new Array ("<",">");
	var sum = chaos.length;
	for (var i in chaos) {if (!Array.prototype[i]) {sum += value.lastIndexOf(chaos[i])}}
	if (sum) {
		alert(msg);
		field.select();
		field.focus();
		return false;
	}
	return true;
}